<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk|comment>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <chapter|Reactions and Other Chemical Processes><label|Chap. 11><label|c11>

  <subindex|Process|chemical><index|Chemical process>This chapter discusses
  the thermodynamics of mixing processes and processes described by reaction
  equations (chemical equations). It introduces the important concepts of
  molar mixing and reaction quantities, advancement, and the thermodynamic
  equilibrium constant. The focus is on chemical processes that take place in
  closed systems at constant pressure, with no work other than expansion
  work. Under these conditions, the enthalpy change is equal to the heat (Eq.
  <reference|dH=dq (dp=0)>). The processes either take place at constant
  temperature, or have initial and final states of the same temperature.

  Most of the processes to be described involve mixtures and have
  intermediate states that are nonequilibrium states. At constant temperature
  and pressure, these processes proceed spontaneously with decreasing Gibbs
  energy (Sec. <reference|5-combining>).<footnote|Processes in which <math|G>
  decreases are sometimes called <index|Exergonic process><em|exergonic>.>
  When the rates of change are slow enough for thermal and mechanical
  equilibrium to be maintained, the spontaneity is due to lack of transfer
  equilibrium or reaction equilibrium. An equilibrium phase transition of a
  pure substance, however, is a special case: it is a reversible process of
  constant Gibbs energy (Sec. <reference|8-phase transitions>).

  <section|Mixing Processes><label|11-mixing><label|c11 sec mp>

  A <index|Mixing process><subindex|Process|mixing><newterm|mixing process>
  is a process in which a mixture is formed from pure substances. In the
  initial state the system has two or more separate phases, each containing a
  different pure substance at the same temperature and pressure. The final
  state is a single-phase mixture at this temperature and pressure.

  The process is illustrated schematically in Fig. <vpageref|fig:11-mixing
  process>.<\float|float|hb>
    <\framed>
      <\big-figure|<image|11-SUP/DEL-MIX.eps|230pt|72pt||>>
        <label|fig:11-mixing process>Initial state (left) and final state
        (right) of mixing process for liquid substances A and B.
      </big-figure>
    </framed>
  </float> When the partition is withdrawn, the two pure liquids mix
  spontaneously at constant pressure to form a single homogeneous phase. If
  necessary, heat transfer is used to return the phase to the initial
  temperature.

  <subsection|Mixtures in general><label|11-mixing in general><label|c11 sec
  mp-general>

  First let us consider changes in the Gibbs energy <math|G>. Since this is
  an extensive property, <math|G> in the initial state 1 is the sum of
  <math|G> for each pure phase:

  <\equation>
    G<rsub|1>=<big|sum><rsub|i>n<rsub|i>*\<mu\><rsub|i><rsup|\<ast\>>
  </equation>

  Here <math|\<mu\><rsub|i><rsup|\<ast\>>> is the chemical potential (i.e.,
  the molar Gibbs energy) of pure substance <math|i> at the initial
  temperature and pressure. For the final state 2, we use the
  <index|Additivity rule>additivity rule for a mixture

  <\equation>
    G<rsub|2>=<big|sum><rsub|i>n<rsub|i>*\<mu\><rsub|i>
  </equation>

  where <math|\<mu\><rsub|i>> is the chemical potential of <math|i> in the
  mixture at the same temperature and pressure as the initial state. The
  overall change of <math|G>, the <index-complex|<tuple|gibbs
  energy|mixing>|||<tuple|Gibbs energy|of mixing>><newterm|Gibbs energy of
  mixing>, is then

  <\equation>
    <label|del(mix)G=G2-G1><Del>G<mix>=G<rsub|2>-G<rsub|1>=<big|sum><rsub|i>n<rsub|i>*<around|(|\<mu\><rsub|i>-\<mu\><rsub|i><rsup|\<ast\>>|)>
  </equation>

  The <index-complex|<tuple|gibbs energy|mixing|molar>|||<tuple|Gibbs
  energy|of mixing|molar>><newterm|molar Gibbs energy of mixing> is the Gibbs
  energy of mixing per amount of mixture formed; that is,
  <math|<Del>G<m><mix>=<Del>G<mix>/n>, where <math|n> is the sum
  <math|<big|sum><rsub|i>n<rsub|i>>. Dividing both sides of Eq.
  <reference|del(mix)G=G2-G1> by <math|n>, we obtain

  <\equation>
    <label|DelGm(mix)=sum(x_i)(mu_i-mu_i*)><Del>G<m><mix>=<big|sum><rsub|i>x<rsub|i>*<around|(|\<mu\><rsub|i>-\<mu\><rsub|i><rsup|\<ast\>>|)>
  </equation>

  where <math|x<rsub|i>> is the mole fraction of substance <math|i> in the
  final mixture.

  Following the same procedure for an extensive state function <math|X>, we
  derive the following general relation for its molar mixing quantity:

  <\equation>
    <label|Del X_m(mix)=><Del>X<m><mix>=<big|sum><rsub|i>x<rsub|i>*<around|(|X<rsub|i>-X<rsub|i><rsup|\<ast\>>|)>
  </equation>

  <subsection|Ideal mixtures><label|11-ideal mixts><label|c11 sec mp-ideal>

  <subindex|Ideal mixture|mixing process>When the mixture formed is an ideal
  mixture (gas, liquid, or solid), and the pure constituents have the same
  physical state as the mixture, the expressions for various molar mixing
  quantities are particularly simple. An ideal molar mixing quantity will be
  indicated by a superscript \Pid\Q as in <math|<Del>G<m><mix>>. The general
  definition of an ideal molar mixing quantity, analogous to Eq.
  <reference|Del X_m(mix)=>, is

  <\equation>
    <label|Del X_m(id)(mix)=><Del>X<m><mix>=<big|sum><rsub|i>x<rsub|i>*<around|(|X<rsub|i>-X<rsub|i><rsup|\<ast\>>|)>
  </equation>

  The chemical potential of constituent <math|i> of an ideal mixture is
  related to the mole fraction <math|x<rsub|i>> by the relation (Eq.
  <reference|ideal mixture>)

  <\equation>
    \<mu\><rsub|i>=\<mu\><rsub|i><rsup|\<ast\>>+R*T*ln x<rsub|i>
  </equation>

  By combining this relation with Eq. <reference|DelGm(mix)=sum(x_i)(mu_i-mu_i*)>,
  we find the <index-complex|<tuple|gibbs energy|mixing|ideal
  mixture>|||<tuple|Gibbs energy|of mixing|to form an ideal mixture>>molar
  Gibbs energy of mixing to form an ideal mixture is given by

  <\equation>
    <label|del(mix)Gm(id)=RT*sum(x_i)ln(x_i)><Del>G<m><mix>=R*T*<big|sum><rsub|i>x<rsub|i>*ln
    x<rsub|i>
  </equation>

  Since each mole fraction is less than one and the logarithm of a fraction
  is negative, it follows that <math|<Del>G<m><mix>> is negative for every
  composition of the mixture.

  We obtain expressions for other molar mixing quantities by substituting
  formulas for partial molar quantities of constituents of an ideal mixture
  derived in Sec. <reference|9-partial molar, id mixts> into Eq.
  <reference|Del X_m(mix)=>. From <math|S<rsub|i>=S<rsub|i><rsup|\<ast\>>-R*ln
  x<rsub|i>> (Eq. <reference|S_i=S_i^*-R*ln x_i>), we obtain
  <index-complex|<tuple|entropy|mixing|form>|||<tuple|Entropy|of mixing|to
  form an ideal mixture>>

  <\equation>
    <label|del(mix)Sm(id)=-R*sum(x_i)ln(x_i)><Del>S<m><mix>=-R*<big|sum><rsub|i>x<rsub|i>*ln
    x<rsub|i>
  </equation>

  This quantity is positive.

  <\quote-env>
    \ Although the molar entropy of mixing to form an <em|ideal> mixture is
    positive, this is not true for some nonideal mixtures.
    McGlashan<footnote|Ref. <cite|mcglashan-79>, p. 241.> cites the
    <index-complex|<tuple|entropy|mixing|negative value>|||<tuple|Entropy|of
    mixing|negative value>><em|negative> value <math|<Del>S<m><mix>=-8.8
    <text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>> for an
    equimolar mixture of diethylamine and water at <math|322<K>>.
  </quote-env>

  From <math|H<rsub|i>=H<rsub|i><rsup|\<ast\>>> (Eq. <reference|H_i=H_i^*>)
  and <math|U<rsub|i>=U<rsub|i><rsup|\<ast\>>> (Eq. <reference|U_i=U_i^*>),
  we have <index-complex|<tuple|enthalpy|mixing>|||<tuple|Enthalpy|of mixing
  to form an ideal mixture>>

  <\equation>
    <label|del(mix)Hm(id)=0><Del>H<m><mix>=0
  </equation>

  and<index-complex|<tuple|internal energy|mixing>|||<tuple|Internal
  energy|of mixing to form an ideal mixture>>

  <\equation>
    <label|del(mix)Um(id)=0><Del>U<m><mix>=0
  </equation>

  Thus, the mixing of liquids that form an ideal mixture is an
  <index|Athermal process><em|athermal> process, one in which no heat
  transfer is needed to keep the temperature constant.

  From <math|V<rsub|i>=V<rsub|i><rsup|\<ast\>>> (Eq. <reference|V_i=V_i^*>),
  we get<index-complex|<tuple|volume|mixing>|||<tuple|Volume|of mixing to
  form an ideal mixture>>

  <\equation>
    <label|del(mix)Vm(id)=0><Del>V<m><mix>=0
  </equation>

  showing that the ideal molar volume of mixing is zero. Thus an ideal
  mixture has the same volume as the sum of the volumes of the pure
  components at the same <math|T> and <math|p>.<footnote|From the fact
  mentioned on p.<nbsp><pageref|water-methanol mixt> that the volume of a
  mixture of water and methanol is different from the sum of the volumes of
  the pure liquids, we can deduce that this mixture is nonideal, despite the
  fact that water and methanol mix in all proportions.>

  Figure <vpageref|fig:11-mixing quantities><\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/ID-MIX.eps|181pt|198pt||>>
        <label|fig:11-mixing quantities>Molar mixing quantities for a binary
        ideal mixture at <math|298.15<K>>.
      </big-figure>
    </framed>
  </float> shows how <math|<Del>G<m><mix>>, <math|T<Del>S<m><mix>>, and
  <math|<Del>H<m><mix>> depend on the composition of an ideal mixture formed
  by mixing two pure substances. Although it is not obvious in the figure,
  the curves for <math|<Del>G<m><mix>> and <math|T<Del>S<m><mix>> have slopes
  of <math|+\<infty\>> or <math|-\<infty\>> at <math|x<A|=>0> and
  <math|x<A|=>1>.

  <subsection|Excess quantities><label|11-excess quantities><label|c11 sec
  mp-excess>

  An <subindex|Excess|quantity><newterm|excess quantity> <math|X<E>> of a
  mixture is defined as the difference between the value of the extensive
  property <math|X> of the real mixture and <math|X<rsup|<text|id>>>, the
  value for a hypothetical ideal mixture at the same temperature, pressure,
  and composition.

  An <subindex|Molar|excess quantity><newterm|excess molar quantity>
  <math|X<m><E>> is the excess quantity divided by <math|n>, the total amount
  of all constituents of the mixture. Examining the dependence of excess
  molar quantities on composition is a convenient way to characterize
  deviations from ideal-mixture behavior.

  Excess molar quantities are related to molar mixing quantities as follows:

  <\eqnarray*>
    <tformat|<table|<row|<cell|X<m><E>=<around|(|X-X<rsup|<text|id>>|)>/n>|<cell|=>|<cell|<around*|(|<big|sum><rsub|i>n<rsub|i>*X<rsub|i>-<big|sum><rsub|i>n<rsub|i>*X<rsub|i><rsup|<text|id>>|)>/n>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|i>x<rsub|i>*<around*|(|X<rsub|i>-X<rsub|i><rsup|<text|id>>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|i>x<rsub|i>*<around|(|X<rsub|i>-X<rsub|i><rsup|\<ast\>>|)>-<big|sum><rsub|i>x<rsub|i>*<around|(|X<rsub|i><rsup|<text|id>>-X<rsub|i><rsup|\<ast\>>|)>>>|<row|<cell|>|<cell|=>|<cell|<Del>X<m><mix>-<Del>X<m><rsup|<text|id>><mix><eq-number><label|Xm(E)=>>>>>
  </eqnarray*>

  By substituting expressions for <math|<Del>X<m><mix>> from Eqs.
  <reference|del(mix)Gm(id)=RT*sum(x_i)ln(x_i)>\U<reference|del(mix)Vm(id)=0>
  in Eq. <reference|Xm(E)=>, we obtain the following expressions for the
  excess molar Gibbs energy, entropy, enthalpy, internal energy, and volume:

  <\eqnarray*>
    <tformat|<table|<row|<cell|G<m><E>>|<cell|=>|<cell|<Del>G<m><mix>-R*T*<big|sum><rsub|i>x<rsub|i>*ln
    x<rsub|i><eq-number><label|Gm^E defn>>>|<row|<cell|S<m><E>>|<cell|=>|<cell|<Del>S<m><mix>+R*<big|sum><rsub|i>x<rsub|i>*ln
    x<rsub|i><eq-number>>>|<row|<cell|H<m><E>>|<cell|=>|<cell|<Del>H<m><mix><eq-number>>>|<row|<cell|U<m><E>>|<cell|=>|<cell|<Del>U<m><mix><eq-number>>>|<row|<cell|V<m><E>>|<cell|=>|<cell|<Del>V<m><mix><eq-number>>>>>
  </eqnarray*>

  By substitution from Eqs. <reference|act coeff, mixt> and
  <reference|DelGm(mix)=sum(x_i)(mu_i-mu_i*)> in Eq. <reference|Gm^E defn>,
  we can relate the excess molar Gibbs energy to the activity coefficients of
  the mixture constituents based on pure-liquid reference states:

  <\equation>
    <label|Gm(E)=RT sum[x(i)ln(gamma(i)]>G<m><E>=R*T*<big|sum><rsub|i>x<rsub|i>*ln
    <g><rsub|i>
  </equation>

  It is also possible to derive the useful relation

  <\equation>
    <label|dnGm(E)/dn(i)=RTln(gamma(i))><bPd|<around*|(|n*G<m><E>|)>|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>=R*T*ln
    <g><rsub|i>
  </equation>

  <\quote-env>
    \ To derive Eq. <reference|dnGm(E)/dn(i)=RTln(gamma(i))>, consider
    infinitesimal changes in the mixture composition at constant <math|T> and
    <math|p>. From Eq. <reference|Gm(E)=RT sum[x(i)ln(gamma(i)]>, we write

    <\equation>
      <label|d(nG^E)=><dif><around*|(|n*G<m><E>|)>=R*T*<big|sum><rsub|i><dif><around|(|n<rsub|i>*ln
      <g><rsub|i>|)>=R*T*<big|sum><rsub|i>n<rsub|i><dif>ln
      <g><rsub|i>+R*T*<big|sum><rsub|i><around|(|ln
      <g><rsub|i>|)>*<dif>n<rsub|i>
    </equation>

    From <math|\<mu\><rsub|i>=\<mu\><rsub|i><rsup|\<ast\>>+R*T*ln
    <around|(|<g><rsub|i>*x<rsub|i>|)>>, we have
    <math|<dif>\<mu\><rsub|i>=R*T*<around|(|<dif>ln
    <g><rsub|i>+<dx><rsub|i>/x<rsub|i>|)>>. Substitution in the
    <index|Gibbs--Duhem equation>Gibbs\UDuhem equation,
    <math|<big|sum><rsub|i>x<rsub|i>*<dif>\<mu\><rsub|i>=0>, gives

    <\equation>
      <label|sum(x_i)dln(ac_i)+sum(dx_i)=0><big|sum><rsub|i>x<rsub|i>*<dif>ln
      <g><rsub|i>+<big|sum><rsub|i><dx><rsub|i>=0
    </equation>

    In Eq. <reference|sum(x_i)dln(ac_i)+sum(dx_i)=0>, we set the sum
    <math|<big|sum><rsub|i><dx><rsub|i>> equal to zero (because
    <math|<big|sum><rsub|i>x<rsub|i>> equals 1) and multiply by the total
    amount, <math|n>, resulting in <math|<big|sum><rsub|i>n<rsub|i>*<dif>ln
    <g><rsub|i>=0>. This turns Eq. <reference|d(nG^E)=> into

    <\equation>
      <dif><around*|(|n*G<m><E>|)>=R*T*<big|sum><rsub|i><around|(|ln
      <g><rsub|i>|)>*<dif>n<rsub|i>
    </equation>

    from which Eq. <reference|dnGm(E)/dn(i)=RTln(gamma(i))> follows.
  </quote-env>

  <subsection|The entropy change to form an ideal gas mixture><label|c11
  sec-mp-entropy-ideal>

  <index-complex|<tuple|entropy|mixing|form>|||<tuple|Entropy|of mixing|to
  form an ideal mixture>>When pure ideal gases mix at constant <math|T> and
  <math|p> to form an ideal gas mixture, the molar entropy change
  <math|<Del>S<m><rsup|<text|id>><mix>=-R*<big|sum><rsub|i>y<rsub|i>*ln
  y<rsub|i>> (Eq. <reference|del(mix)Sm(id)=-R*sum(x_i)ln(x_i)>) is positive.

  Consider a pure ideal-gas phase. Entropy is an extensive property, so if we
  divide this phase into two subsystems with an internal partition, the total
  entropy remains unchanged. The reverse process, the removal of the
  partition, must also have zero entropy change. Despite the fact that the
  latter process allows the molecules in the two subsystems to intermingle
  without a change in <math|T> or <math|p>, it cannot be considered
  \Pmixing\Q because the entropy does not increase. The essential point is
  that the <em|same> substance is present in both of the subsystems, so there
  is no macroscopic change of state when the partition is removed.

  From these considerations, one might conclude that the fundamental reason
  the entropy increases when pure ideal gases mix is that different
  substances become intermingled. This conclusion would be mistaken, as we
  will now see.

  The partial molar entropy of constituent <math|i> of an ideal gas mixture
  is related to its partial pressure <math|p<rsub|i>> by Eq.
  <reference|S_i=S_io-R*ln(p_i/po)>:

  <\equation>
    <label|S_i=>S<rsub|i>=S<rsub|i><st>-R*ln <around|(|p<rsub|i>/p<st>|)>
  </equation>

  But <math|p<rsub|i>> is equal to <math|n<rsub|i>*R*T/V> (Eq.
  <reference|p_i=n_i*RT/V>). Therefore, if a fixed amount of <math|i> is in a
  container at a given temperature, <math|S<rsub|i>> depends only on the
  <em|volume> of the container and is unaffected by the presence of the other
  constituents of the ideal gas mixture.

  When Eq. <reference|S_i=> is applied to a <em|pure> ideal gas, it gives an
  expression for the molar entropy

  <\equation>
    <label|S_i*=>S<rsub|i><rsup|\<ast\>>=S<rsub|i><st>-R*ln
    <around|(|p/p<st>|)>
  </equation>

  where <math|p> is equal to <math|n*R*T/V>.

  From Eqs. <reference|S_i=> and <reference|S_i*=>, and the fact that the
  entropy of a mixture is given by the <index|Additivity rule>additivity rule
  <math|S=<big|sum><rsub|i>n<rsub|i>*S<rsub|i>>, we conclude that <em|the
  entropy of an ideal gas mixture equals the sum of the entropies of the
  unmixed pure ideal gases, each pure gas having the same temperature and
  occupying the same volume as in the mixture>.

  We can now understand why the entropy change is positive when ideal gases
  mix at constant <math|T> and <math|p>: Each substance occupies a greater
  <em|volume> in the final state than initially. Exactly the same entropy
  increase would result if the volume of each of the pure ideal gases were
  increased isothermally without mixing.

  The reversible mixing process depicted in Fig. <vpageref|fig:11-ideal gas
  mixing> illustrates this principle.<\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/id-gas-mix.eps|361pt|81pt||>>
        <label|fig:11-ideal gas mixing>Reversible mixing process for ideal
        gases A and B confined in a cylinder. Piston 1 is permeable to A but
        not B; piston 2 is permeable to B but not A.

        <\enumerate-alpha>
          <item>Gases A and B are in separate phases at the same temperature
          and pressure.

          <item>The pistons move apart at constant temperature with negative
          reversible work, creating an ideal gas mixture of A and B in
          continuous transfer equilibrium with the pure gases.

          <item>The two gases are fully mixed at the initial temperature and
          pressure.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float> The initial state shown in Fig. <reference|fig:11-ideal gas
  mixing>(a) consists of volume <math|V<rsub|1><around*|(|<text|A>|)>> of
  pure ideal gas A and volume <math|V<rsub|1><around*|(|<text|B>|)>> of pure
  ideal gas B, both at the same <math|T> and <math|p>. The hypothetical
  semipermeable pistons are moved apart reversibly and isothermally to create
  an ideal gas mixture, as shown in Fig. <reference|fig:11-ideal gas
  mixing>(b). According to an argument in Sec. <reference|9-partial molar, id
  gas mixts>, transfer equilibrium across the semipermeable pistons requires
  partial pressure <math|p<A>> in the mixture to equal the pressure of the
  pure A at the left, and partial pressure <math|p<B>> in the mixture to
  equal the pressure of the pure B at the right. Thus in intermediate states
  of the process, gas A exerts no net force on piston 1, and gas B exerts no
  net force on piston 2.

  In the final state shown in Fig. <reference|fig:11-ideal gas mixing>(c),
  the gases are fully mixed in a phase of volume
  <math|V<rsub|2>=V<rsub|1><around*|(|<text|A>|)>+V<rsub|1><around*|(|<text|B>|)>>.
  The movement of piston 1 has expanded gas B with the same reversible work
  as if gas A were absent, equal to <math|-n<B>R*T*ln
  <around|[|V<rsub|2>/V<rsub|1><around*|(|<text|B>|)>|]>>. Likewise, the
  reversible work to expand gas A with piston 2 is the same as if B were
  absent: <math|-n<A>R*T*ln <around|[|V<rsub|2>/V<rsub|1><around*|(|<text|A>|)>|]>>.
  Because the initial and final temperatures and pressures are the same, the
  mole fractions in the final mixture are
  <math|y<A>=V<rsub|1><around*|(|<text|A>|)>/V<rsub|2>> and
  <math|y<B>=V<rsub|1><around*|(|<text|B>|)>/V<rsub|2>>. The total work of
  the reversible mixing process is therefore <math|w=n<A>R*T*ln
  y<A>+n<B>R*T*ln y<B>>, the heat needed to keep the internal energy constant
  is <math|q=-w>, and the entropy change is

  <\equation>
    <label|Del S (A+B)><Del>S=q/T=-n<A>R*ln y<A>-n<B>R*ln y<B>
  </equation>

  It should be clear that isothermal expansion of both pure gases from their
  initial volumes to volume <math|V<rsub|2>> without mixing would result in
  the same total work and the same entropy change.

  When we divide Eq. <reference|Del S (A+B)> by <math|n=n<A>+n<B>>, we obtain
  the expression for the molar entropy of mixing given by Eq.
  <reference|del(mix)Sm(id)=-R*sum(x_i)ln(x_i)> with <math|x<rsub|i>>
  replaced by <math|y<rsub|i>> for a gas.

  <subsection|Molecular model of a liquid mixture><label|11-mol model of id
  mixt><label|c11 sec-mp-molecular-model>

  We have seen that when two pure liquids mix to form an ideal liquid mixture
  at the same <math|T> and <math|p>, the total volume and internal energy do
  not change. A simple molecular model of a binary liquid mixture will
  elucidate the energetic molecular properties that are consistent with this
  macroscopic behavior. The model assumes the excess molar entropy, but not
  necessarily the excess molar internal energy, is zero. The model is of the
  type sometimes called the <index|Quasicrystalline lattice
  model><em|quasicrystalline lattice model>, and the mixture it describes is
  sometimes called a <subindex|Mixture|simple><index|Simple
  mixture><em|simple> mixture. Of course, a molecular model like this is
  outside the realm of classical thermodynamics.

  The model is for substances A and B in gas and liquid phases at a fixed
  temperature. Let the standard molar internal energy of pure gaseous A be
  <math|U<A><st><gas>>. This is the molar energy in the absence of
  intermolecular interactions, and its value depends only on the molecular
  constitution and the temperature. The molar internal energy of pure liquid
  A is lower because of the attractive intermolecular forces in the liquid
  phase. We assume the energy difference is equal to a sum of pairwise
  nearest-neighbor interactions in the liquid. Thus, the molar internal
  energy of pure liquid A is given by

  <\equation>
    U<A><rsup|\<ast\>>=U<A><st><gas>+k<rsub|<text|A>\<nocomma\><text|A>>
  </equation>

  where <math|k<rsub|<text|A>\<nocomma\><text|A>>> (approximately the
  negative of the molar internal energy of vaporization) is the interaction
  energy per amount of A due to A\UA interactions when each molecule of A is
  surrounded only by other molecules of A.

  Similarly, the molar internal energy of pure liquid B is given by

  <\equation>
    <label|U_B=UBo(g)+k(BB)>U<B><rsup|\<ast\>>=U<B><st><gas>+k<rsub|<text|B>\<nocomma\><text|B>>
  </equation>

  where <math|k<rsub|<text|B>\<nocomma\><text|B>>> is for B\UB interactions.

  We assume that in a liquid mixture of A and B, the numbers of
  nearest-neighbor molecules of A and B surrounding any given molecule are in
  proportion to the mole fractions <math|x<A>> and <math|x<B>>.<footnote|This
  assumption requires the molecules of A and B to have similar sizes and
  shapes and to be randomly mixed in the mixture. <subindex|Statistical
  mechanics|mixture theory>Statistical mechanics theory shows that the
  molecular sizes must be approximately equal if the excess molar entropy is
  to be zero.> Then the number of A\UA interactions is proportional to
  <math|n<A>x<A>>, the number of B\UB interactions is proportional to
  <math|n<B>x<B>>, and the number of A\UB interactions is proportional to
  <math|n<A>x<B>+n<B>x<A>>. The internal energy of the liquid mixture is then
  given by

  <\equation>
    <label|U(mixt)>U<around*|(|<text|mixt>|)>=n<A>*U<A><st><gas>+n<B>*U<B><st><gas>+n<A>*x<A>*k<rsub|<text|A>\<nocomma\><text|A>>+n<B>*x<B>*k<rsub|<text|B>\<nocomma\><text|B>>+<around|(|n<A>*x<B>+n<B>*x<A>|)>*k<rsub|<text|A>\<nocomma\><text|B>>
  </equation>

  where <math|k<rsub|<text|A>\<nocomma\><text|B>>> is the interaction energy
  per amount of A when each molecule of A is surrounded only by molecules of
  B, or the interaction energy per amount of B when each molecule of B is
  surrounded only by molecules of A.

  The internal energy change for mixing amounts <math|n<A>> of liquid A and
  <math|n<B>> of liquid B is now

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\>*U<around*|(|<text|mix>|)>>|<cell|=>|<cell|U<around*|(|<text|mixt>|)>-n<A>*U<A><rsup|\<ast\>>-n<B>*U<B><rsup|\<ast\>>>>|<row|<cell|>|<cell|=>|<cell|n<A>*x<A>*k<rsub|<text|A>\<nocomma\><text|A>>+n<B>*x<B>*k<rsub|<text|B>\<nocomma\><text|B>>+<around|(|n<A>*x<B>+n<B>*x<A>|)>*k<rsub|<text|A>\<nocomma\><text|B>>-n<A>*k<rsub|<text|A>\<nocomma\><text|A>>-n<B>*k<rsub|<text|B>\<nocomma\><text|B>>>>|<row|<cell|>|<cell|=>|<cell|n<A>*<around|(|x<A>-1|)>*k<rsub|<text|A>\<nocomma\><text|A>>+n<B>*<around|(|x<B>-1|)>*k<rsub|<text|B>\<nocomma\><text|B>>+<around|(|n<A>*x<B>+n<B>*x<A>|)>*k<rsub|<text|A>\<nocomma\><text|B>><eq-number>>>>>
  </eqnarray*>

  With the identities <math|x<A>-1=-x<B>>, <math|x<B>-1=-x<A>>, and
  <math|n<A>x<B>=n<B>x<A>=n<A>n<B>/n> (where <math|n> is the sum
  <math|n<A>+n<B>>), we obtain

  <\equation>
    <label|delU(mix) - model><Del>U<mix>=<frac|n<A>*n<B>|n>*<around|(|2*k<rsub|<text|A>\<nocomma\><text|B>>-k<rsub|<text|A>\<nocomma\><text|A>>-k<rsub|<text|B>\<nocomma\><text|B>>|)>
  </equation>

  If the internal energy change to form a mixture of any composition is to be
  zero, as it is for an ideal mixture, the quantity
  <math|<around|(|2*k<rsub|<text|A>\<nocomma\><text|B>>-k<rsub|<text|A>\<nocomma\><text|A>>-k<rsub|<text|B>\<nocomma\><text|B>>|)>>
  must be zero, which means <math|k<rsub|<text|A>\<nocomma\><text|B>>> must
  equal <math|<around|(|k<rsub|<text|A>\<nocomma\><text|A>>+k<rsub|<text|B>\<nocomma\><text|B>>|)>/2>.
  Thus, one requirement for an ideal mixture is that <em|an A\UB interaction
  equals the average of an A\UA interaction and a B\UB interaction>.

  If we write Eq. <reference|U(mixt)> in the form

  <\equation>
    U<around*|(|<text|mixt>|)>=n<A>*U<A><st><gas>+n<B>*U<B><st><gas>+<frac|1|n<A>+n<B>>*<around|(|n<A><rsup|2>*k<rsub|<text|A>\<nocomma\><text|A>>+2*n<A>*n<B>*k<rsub|<text|A>\<nocomma\><text|B>>+n<B><rsup|2>*k<rsub|<text|B>\<nocomma\><text|B>>|)>
  </equation>

  we can differentiate with respect to <math|n<B>> at constant <math|n<A>> to
  evaluate the partial molar internal energy of B. The result can be
  rearranged to the simple form

  <\equation>
    <label|U_B(molecular model)>U<B>=U<B><rsup|\<ast\>>+<around*|(|2*k<rsub|<text|A>\<nocomma\><text|B>>-k<rsub|<text|A>\<nocomma\><text|A>>-k<rsub|<text|B>\<nocomma\><text|B>>|)>*<around|(|1-x<B>|)><rsup|2>
  </equation>

  where <math|U<B><rsup|\<ast\>>> is given by Eq.
  <reference|U_B=UBo(g)+k(BB)>. Equation <reference|U_B(molecular model)>
  predicts that the value of <math|U<B>> decreases with increasing
  <math|x<B>> if <math|k<rsub|<text|A>\<nocomma\><text|B>>> is less negative
  than the average of <math|k<rsub|<text|A>\<nocomma\><text|A>>> and
  <math|k<rsub|<text|B>\<nocomma\><text|B>>>, increases for the opposite
  situation, and is equal to <math|U<B><rsup|\<ast\>>> in an ideal liquid
  mixture.

  When the excess molar volume and entropy are set equal to zero, the model
  describes what is called a <index|Regular
  solution><subindex|Solution|regular><em|regular solution>.<footnote|Ref.
  <cite|hildebrand-62>.> The excess molar Gibbs energy of a mixture is
  <math|G<m><E>=U<m><E>+p*V<m><E>-T*S<m><E>>. Using the expression of Eq.
  <reference|delU(mix) - model> with the further assumptions that
  <math|V<m><E>> and <math|S<m><E>> are zero, this model predicts the excess
  molar Gibbs energy is given by

  <\equation>
    G<m><E>=<frac|<Del>U<mix>|n>=x<A>*x<B>*<around*|(|2*k<rsub|<text|A>\<nocomma\><text|B>>-k<rsub|<text|A>\<nocomma\>A>-k<rsub|<text|B>\<nocomma\><text|B>>|)>
  </equation>

  This is a symmetric function of <math|x<A>> and <math|x<B>>. It predicts,
  for example, that coexisting liquid layers in a binary system (Sec.
  <reference|11-phase sep>) have the same value of <math|x<A>> in one phase
  as the value of <math|x<B>> in the other.

  Molar excess Gibbs energies of real liquid mixtures are often found to be
  unsymmetric functions. To represent them, a more general function is
  needed. A commonly used function for a binary mixture is the
  <index|Redlich--Kister series><newterm|Redlich\UKister series> given by

  <\equation>
    G<m><E>=x<A>*x<B>*<around*|[|a+b*<around|(|x<A>-x<B>|)>+c*<around|(|x<A>-x<B>|)><rsup|2>+\<cdots\>|]>
  </equation>

  where the parameters <math|a,b,c,\<cdots\>> depend on <math|T> and <math|p>
  but not on composition. This function satisfies a necessary condition for
  the dependence of <math|G<m><E>> on composition: <math|G<m><E>> must equal
  zero when either <math|x<A>> or <math|x<B>> is zero.<footnote|The reason
  for this condition can be seen by looking at Eq. <vpageref|Gm(E)=RT
  sum[x(i)ln(gamma(i)]>. For a binary mixture, this equation becomes
  <math|G<m><E>=R*T*<around|(|x<A>*ln <g><A>+x<B>*ln <g><B>|)>>. When
  <math|x<A>> is zero, <math|<g><B>> is 1 and <math|ln <g><B>> is zero. When
  <math|x<B>> is zero, <math|<g><A>> is 1 and <math|ln <g><A>> is zero. Thus
  <math|G<m><E>> must be zero in both cases.>

  For many binary liquid systems, the measured dependence of <math|G<m><E>>
  on composition is reproduced reasonably well by the two-parameter
  Redlich\UKister series

  <\equation>
    <label|2-par Redlich-Kister>G<m><E>=x<A>*x<B>*<around*|[|<space|0.17em>a+b*<around|(|x<A>-x<B>|)><space|0.17em>|]>
  </equation>

  in which the parameters <math|a> and <math|b> are adjusted to fit the
  experimental data. The activity coefficients in a mixture obeying this
  equation are found, from Eq. <reference|dnGm(E)/dn(i)=RTln(gamma(i))>, to
  be given by

  <\equation>
    R*T*ln <g><A>=x<B><rsup|2>*<around|[|<space|0.17em>a+<around|(|3-4*x<B>|)>*b<space|0.17em>|]>*<space|2em>R*T*ln
    <g><B>=x<A><rsup|2>*<around|[|<space|0.17em>a+<around|(|4*x<A>-3|)>*b<space|0.17em>|]>
  </equation>

  <subsection|Phase separation of a liquid mixture><label|11-phase
  sep><label|c11 sec-mp-liquid-phase-sep>

  <index-complex|<tuple|phase|separation of a liquid mixture>||c11
  sec-mp-liquid-phase-sep idx1|<tuple|Phase|separation of a liquid mixture>>A
  binary liquid mixture in a system maintained at constant <math|T> and
  <math|p> can spontaneously separate into two liquid layers if any part of
  the curve of a plot of <math|<Del>G<m><mix>> versus <math|x<A>> is concave
  downward. To understand this phenomenon, consider Fig.
  <vpageref|fig:11-phase separation>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/PHASESEP.eps|152pt|160pt||>>
        <label|fig:11-phase separation>Molar Gibbs energy of mixing as a
        function of the composition of a binary liquid mixture with
        spontaneous phase separation. The inflection points are indicated by
        filled circles.
      </big-figure>
    </framed>
  </float> This figure is a plot of <math|<Del>G<m><mix>> versus <math|x<A>>.
  It has the form needed to evaluate the quantities
  <math|<around|(|\<mu\><A>-\<mu\><A><rsup|\<ast\>>|)>> and
  <math|<around|(|\<mu\><B>-\<mu\><B><rsup|\<ast\>>|)>> by the variant of the
  method of intercepts described on page <pageref|Del Xm(mix) plot>. On this
  plot, the tangent to the curve at any given composition has intercepts
  equal to <math|<around|(|\<mu\><B>-\<mu\><B><rsup|\<ast\>>|)>> at
  <math|x<A|=>=0> and <math|<around|(|\<mu\><A>-\<mu\><A><rsup|\<ast\>>|)>>
  at <math|x<A|=>=1>.

  In order for two binary liquid phases to be in transfer equilibrium,
  <math|\<mu\><A>> must be the same in both phases and <math|\<mu\><B>> must
  also be the same in both phases. The dashed line in the figure is a common
  tangent to the curve at the points labeled <math|<pha>> and <math|<phb>>.
  These two points are the only ones having a common tangent, and what makes
  the common tangent possible is the downward concavity (negative curvature)
  of a portion of the curve between these points. Because the tangents at
  these points have the same intercepts, phases <math|<pha>> and <math|<phb>>
  of compositions <math|x<A><aph>> and <math|x<A><bph>> can be in equilibrium
  with one another: the necessary conditions
  <math|\<mu\><A><aph>=\<mu\><A><bph>> and
  <math|\<mu\><B><aph>=\<mu\><B><bph>> are satisfied.

  Now consider point <math|1> on the curve. A phase of this composition is
  unstable. It will spontaneously separate into the two phases of
  compositions <math|x<A><aph>> and <math|x<A><bph>>, because the Gibbs
  energy per total amount then decreases to the extent indicated by the
  vertical arrow from point 1 to point 2. We know that a process in which
  <math|G> decreases at constant <math|T> and <math|p> in a closed system,
  with expansion work only, is a spontaneous process (Sec.
  <reference|5-combining>).

  <\quote-env>
    \ To show that the arrow in Fig. <reference|fig:11-phase separation>
    represents the change in <math|G/n> for phase separation, we let <math|y>
    represent the vertical ordinate and write the equation of the dashed line
    through points <math|<pha>> and <math|<phb>> (<math|y> as a function of
    <math|x<A>>):

    <\equation>
      <label|y= (common tangent)>y=y<aph>+<around*|(|<frac|y<bph>-y<aph>|x<A><bph>-x<A><aph>>|)>*<around*|(|x<A>-x<A><aph>|)>
    </equation>

    In the system both before and after phase separation occurs, <math|x<A>>
    is the mole fraction of component A in the system as a whole. When phases
    <math|<pha>> and <math|<phb>> are present, containing amounts
    <math|n<aph>> and <math|n<bph>>, <math|x<A>> is given by the expression

    <\equation>
      x<A>=<frac|x<A><aph>*n<aph>+x<A><bph>*n<bph>|n<aph>+n<bph>>
    </equation>

    By substituting this expression for <math|x<A>> in Eq. <reference|y=
    (common tangent)>, after some rearrangement and using
    <math|n<aph>+n<bph>=n>, we obtain

    <\equation>
      y=<frac|1|n>*<around*|(|n<aph>*y<aph>+n<bph>*y<bph>|)>
    </equation>

    which equates <math|y> for a point on the dashed line to the Gibbs energy
    change for mixing pure components to form an amount <math|n<aph>> of
    phase <math|<pha>> and an amount <math|n<bph>> of phase <math|<phb>>,
    divided by the total amount <math|n>. Thus, the difference between the
    values of <math|y> at points 1 and 2 is the decrease in <math|G/n> when a
    single phase separates into two equilibrated phases.
  </quote-env>

  Any mixture with a value of <math|x<A>> between <math|x<A><aph>> and
  <math|x<A><bph>> is unstable with respect to separation into two phases of
  compositions <math|x<A><aph>> and <math|x<A><bph>>. Phase separation occurs
  only if the curve of the plot of <math|<Del>G<m><mix>> versus <math|x<A>>
  is concave downward, which requires the curve to have at least two
  inflection points. The compositions of the two phases are not the
  compositions at the inflection points, nor in the case of the curve shown
  in Fig. <reference|fig:11-phase separation> are these compositions the same
  as those of the two local minima.

  By varying the values of parameters in an expression for the excess molar
  Gibbs energy, we can model the onset of phase separation caused by a
  temperature change. Figure <reference|fig:11-Redlich-Kister><\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/RED-KIST.eps|359pt|180pt||>>
        <label|fig:11-Redlich-Kister>Binary liquid mixtures at <math|1<br>>.
        The curves are calculated from the two-parameter Redlich\UKister
        series using the following parameter values.

        <\indent>
          Curve 1: <math|a=b=0> (ideal liquid mixture).

          Curve 2: <math|a/R*T=1.8>, <math|b/R*T=0.36>.

          Curve 3: <math|a/R*T=2.4>, <math|b/R*T=0.48>.
        </indent>

        <\enumerate-alpha>
          <item>Molar Gibbs energy of mixing as a function of composition.

          <item>Activity of component A (using a pure-liquid standard state)
          as a function of composition.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float> shows the results of using the two-parameter Redlich\UKister
  series (Eq. <reference|2-par Redlich-Kister>).

  If the properties of the mixture are such that <math|G<m><E>> is positive
  at each mixture composition (except at the extremes <math|x<A|=>0> and
  <math|x<A|=>1> where it must be zero), and no portion of the curve of
  <math|<Del>G<m><mix>> versus <math|x<A>> is concave downward, there can be
  no phase separation and the activity <math|a<A>> increases monotonically
  with <math|x<A>>. This case is illustrated by curve 2 in Figs.
  <reference|fig:11-Redlich-Kister>(a) and
  <reference|fig:11-Redlich-Kister>(b).

  If a portion of the <math|<Del>G<m><mix>>\U<math|x<A>> curve is concave
  downward, the condition needed for phase separation, then a maximum appears
  in the curve of <math|a<A>> versus <math|x<A>>. This case is illustrated by
  curve 3, and the compositions of the coexisting phases are indicated by
  open circles. The difference of the compositions at the two circles is a
  <index-complex|<tuple|miscibility gap|binary system>|||<tuple|Miscibility
  gap|in a binary system>><em|miscibility gap>. The portion of curve 3
  between these compositions in Fig. <reference|fig:11-Redlich-Kister>(b) is
  dashed to indicate it describes unstable, nonequilibrium states. Although
  the two coexisting phases have different compositions, the activity
  <math|a<A>> is the same in both phases, as indicated in Fig.
  <reference|fig:11-Redlich-Kister>(b) by the horizontal dashed line. This is
  because component A has the same standard state and the same chemical
  potential in both phases.

  Coexisting liquid phases will be discussed further in Secs.
  <reference|12-l-l eqm> and <reference|13-l-l
  eqm>.<index-complex|<tuple|phase|separation of a liquid mixture>||c11
  sec-mp-liquid-phase-sep idx1|<tuple|Phase|separation of a liquid mixture>>

  <section|The Advancement and Molar Reaction Quantities><label|c11 sec amrq>

  Many of the processes of interest to chemists can be described by balanced
  <subindex|Reaction|equation><subindex|Equation|reaction>reaction equations,
  or <index|Chemical equation><subindex|Equation|chemical>chemical equations,
  for the conversion of reactants into products. Thus, for the vaporization
  of water we write

  <\equation*>
    <text|H<rsub|2>O><around*|(|<text|l>|)><arrow><text|H<rsub|2>O><around*|(|g|)>
  </equation*>

  For the dissolution of sodium chloride in water, we write

  <\equation*>
    <text|NaCl><around*|(|s|)><arrow><text|Na><rsup|+><around*|(|<text|aq>|)>+<text|Cl><rsup|-><around*|(|<text|aq>|)>
  </equation*>

  For the Haber synthesis of ammonia, the reaction equation can be written

  <\equation*>
    <text|N<rsub|2>><around*|(|<text|g>|)>+3*<text|H<rsub|2>><around*|(|<text|g>|)><arrow>2*<text|NH<rsub|3>><around*|(|<text|g>|)>
  </equation*>

  The essential feature of a reaction equation is that equal amounts of each
  element and equal net charges appear on both sides; the equation is said to
  be <em|balanced>. Thus, matter and charge are conserved during the process,
  and the process can take place in a closed system. The species to the left
  of a single arrow are called <index|Reactant><em|reactants>, the species to
  the right are called <index|Product><em|products>, and the arrow indicates
  the <em|forward> direction of the process.

  A reaction equation is sometimes written with right and left arrows

  <\equation*>
    <text|N<rsub|2>><around*|(|<text|g>|)>+3*<text|H<rsub|2>><around*|(|<text|g>|)><arrows>2*<text|NH<rsub|3>><around*|(|<text|g>|)>
  </equation*>

  to indicate that the process is at reaction equilibrium. It can also be
  written as a <subindex|Stoichiometric|equation><subindex|Equation|stoichiometric><em|stoichiometric
  equation> with an equal sign:

  <\equation*>
    <text|N<rsub|2>><around*|(|<text|g>|)>+3*<text|H<rsub|2>><around*|(|<text|g>|)>=2*<text|NH<rsub|3>><around*|(|<text|g>|)>
  </equation*>

  A reaction equation shows stoichiometric relations among the reactants and
  products. It is important to keep in mind that it specifies neither the
  initial and final states of a chemical process, nor the change in the
  amount of a reactant or product during the process. For example, the
  reaction equation N<rsub|<math|2>><space|0.17em>+<space|0.17em>3<space|0.17em>H<rsub|<math|2>><space|0.17em><ra><space|0.17em>2<space|0.17em>NH<rsub|<math|3>>
  does not imply that the system initially contains only N<rsub|<math|2>> and
  H<rsub|<math|2>>, or that only NH<rsub|<math|3>> is present in the final
  state; and it does not mean that the process consists of the conversion of
  exactly one mole of N<rsub|<math|2>> and three moles of H<rsub|<math|2>> to
  two moles of NH<rsub|<math|3>> (although this is a possibility). Instead,
  the reaction equation tells us that a change in the amount of
  N<rsub|<math|2>> is accompanied by three times this change in the amount of
  H<rsub|<math|2>> and by twice this change, with the opposite sign, in the
  amount of NH<rsub|<math|3>>.

  <subsection|An example: ammonia synthesis><label|11-NH3 example><label|c11
  sec amrq-ex-ammonia>

  It is convenient to indicate the progress of a chemical process with a
  variable called the <em|advancement>. The reaction equation
  N<rsub|<math|2>><space|0.17em>+<space|0.17em>3<space|0.17em>H<rsub|<math|2>><space|0.17em><ra><space|0.17em>2<space|0.17em>NH<rsub|<math|3>>
  for the synthesis of ammonia synthesis will serve to illustrate this
  concept. Let the system be a gaseous mixture of N<rsub|<math|2>>,
  H<rsub|<math|2>>, and NH<rsub|<math|3>>.

  If the system is <em|open> and the intensive properties remain uniform
  throughout the gas mixture, there are five independent variables. We can
  choose them to be <math|T>, <math|p>, and the amounts of the three
  substances. We can write the total differential of the enthalpy, for
  instance, as

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dif>H>|<cell|=>|<cell|<Pd|H|T|p,<allni>>*<dif>T+<Pd|H|p|T,<allni>>*<difp>>>|<row|<cell|>|<cell|>|<cell|+H<rsub|<text|N<rsub|2>>>*<dvar|n<rsub|<text|N<rsub|2>>>>+H<rsub|<text|H<rsub|2>>>*<dvar|n<rsub|<text|H<rsub|2>>>>+H<rsub|<text|NH<rsub|3>>>*<dvar|n<rsub|<text|NH<rsub|3>>>><eq-number><label|dH=H(N2)dn(N2)+...>>>>>
  </eqnarray*>

  The notation <math|<allni>> stands for the set of amounts of all substances
  in the mixture, and the quantities <math|H<rsub|<text|N<rsub|2>>>>,
  <math|H<rsub|<text|H<rsub|2>>>>, and <math|H<rsub|<text|NH<rsub|3>>>> are
  partial molar enthalpies. For example, <math|H<rsub|<text|N<rsub|2>>>> is
  defined by

  <\equation>
    H<rsub|<text|N<rsub|2>>>=<Pd|H|n<rsub|<text|N<rsub|2>>>|T,p,n<rsub|<text|H><rsub|2>>,n<rsub|<text|NH><rsub|3>>>
  </equation>

  If the system is <em|closed>, the amounts of the three substances can still
  change because of the reaction N<rsub|<math|2>><space|0.17em>+<space|0.17em>3<space|0.17em>H<rsub|<math|2>><space|0.17em><ra><space|0.17em>2<space|0.17em>NH<rsub|<math|3>>,
  and the number of independent variables is reduced from five to three. We
  can choose them to be <math|T>, <math|p>, and a variable called
  advancement.

  The <index|Advancement><newterm|advancement> (or <index|Extent of
  reaction>extent of reaction), <math|\<xi\>>, is the amount by which the
  reaction defined by the reaction equation has advanced in the forward
  direction from specified initial conditions. The quantity <math|\<xi\>> has
  dimensions of amount of substance, the usual unit being the mole.

  Let the initial amounts be <math|n<rsub|<text|N<rsub|2>>,0>>,
  <math|n<rsub|<text|H<rsub|2>,0>>>, and <math|n<rsub|<text|NH<rsub|3>,0>>>.
  Then at any stage of the reaction process in the closed system, the amounts
  are given by

  <\equation>
    <label|n(N2)=...>n<rsub|<text|N<rsub|2>>>=n<rsub|<text|N<rsub|2>,0>>-\<xi\>*<space|2em>n<rsub|<text|H<rsub|2>>>=n<rsub|<text|H<rsub|2>>,0>-3*\<xi\>*<space|2em>n<rsub|<text|NH<rsub|3>>>=n<rsub|<text|NH<rsub|3>>,0>+2*\<xi\>
  </equation>

  These relations come from the stoichiometry of the reaction as expressed by
  the stoichiometric coefficients in the reaction equation. The second
  relation, for example, expresses the fact that when one mole of reaction
  has occurred (<math|\<xi\>=1<mol>>), the amount of H<rsub|<math|2>> in the
  closed system has decreased by three moles.

  Taking the differentials of Eqs. <reference|n(N2)=...>, we find that
  infinitesimal changes in the amounts are related to the change of
  <math|\<xi\>> as follows:

  <\equation>
    <dif>n<rsub|<text|N<rsub|2>>>=-<dif>\<xi\><space|2em><dif>n<rsub|<text|H<rsub|2>>>=-3<dif>\<xi\><space|2em><dif>n<rsub|<text|NH<rsub|3>>>=2<dif>\<xi\>
  </equation>

  These relations show that in a closed system, the changes in the various
  amounts are not independent. Substitution in Eq.
  <reference|dH=H(N2)dn(N2)+...> of the expressions for
  <math|<dif>n<rsub|<text|N<rsub|2>>>>, <math|<dif>n<rsub|<text|H<rsub|2>>>>,
  and <math|<dif>n<rsub|<text|NH<rsub|3>>>> gives

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dvar|H>>|<cell|=>|<cell|<Pd|H|T|p,\<xi\>>*<dvar|T>+<Pd|H|p|T,\<xi\>>*<dvar|p>>>|<row|<cell|>|<cell|>|<cell|+<around*|(|-H<rsub|<text|N<rsub|2>>>-3*H<rsub|<text|H<rsub|2>>>+2*H<rsub|<text|NH<rsub|3>>>|)>*<dvar|\<xi\><htab|5mm><tabular*|<tformat|<table|<row|<cell|<eq-number>>>|<row|<cell|<text|(closed
    system)>>>>>>><label|dH=()dxi>>>>>
  </eqnarray*>

  (The subscript <math|<allni>> on the partial derivatives has been replaced
  by <math|\<xi\>> to indicate the same thing: that the derivative is taken
  with the amount of each species held constant.)

  Equation <reference|dH=()dxi> gives an expression for the total
  differential of the enthalpy with <math|T>, <math|p>, and <math|\<xi\>> as
  the independent variables. The coefficient of <math|<dif>\<xi\>> in this
  equation is called the <subindex|Enthalpy|molar reaction><newterm|molar
  reaction enthalpy>, or molar enthalpy of reaction,
  <math|\<Delta\><rsub|<text|r>>*H>:

  <\equation>
    <label|del(r)Hm=-H(N2)...>\<Delta\><rsub|<text|r>>*H=-H<rsub|<text|N<rsub|2>>>-3*H<rsub|<text|H<rsub|2>>>+2*H<rsub|<text|NH<rsub|3>>>
  </equation>

  We identify this coefficient as the partial derivative

  <\equation>
    <label|del(r)Hm=dH/dxi>\<Delta\><rsub|<text|r>>*H=<Pd|H|\<xi\>|T,p>
  </equation>

  That is, the molar reaction enthalpy is the rate at which the enthalpy
  changes with the advancement as the reaction proceeds in the forward
  direction at constant <math|T> and <math|p>.

  <\quote-env>
    \ The partial molar enthalpy of a species is the enthalpy change per
    amount of the species added to an <em|open> system. To see why the
    particular combination of partial molar enthalpies on the right side of
    Eq. <reference|del(r)Hm=-H(N2)...> is the rate at which enthalpy changes
    with advancement in the <em|closed> system, we can imagine the following
    process at constant <math|T> and <math|p>: An infinitesimal amount
    <math|<dif>n> of N<rsub|<math|2>> is removed from an open system, three
    times this amount of H<rsub|<math|2>> is removed from the same system,
    and twice this amount of NH<rsub|<math|3>> is added to the system. The
    total enthalpy change in the open system is
    <math|<dif>H=<around|(|-H<rsub|<text|N<rsub|2>>>-3*H<rsub|<text|H<rsub|2>>>+2*H<rsub|<text|NH<rsub|3>>>|)>*<dif>n>.
    The net change in the state of the system is equivalent to an advancement
    <math|<dif>\<xi\>=<dif>n> in a closed system, so
    <math|<dif>H/<dif>\<xi\>> in the closed system is equal to
    <math|<around|(|-H<rsub|<text|N<rsub|2>>>-3*H<rsub|<text|H<rsub|2>>>+2*H<rsub|<text|NH<rsub|3>>>|)>>
    in agreement with Eqs. <reference|del(r)Hm=-H(N2)...> and
    <reference|del(r)Hm=dH/dxi>.
  </quote-env>

  Note that because the advancement is defined by how we write the reaction
  equation, the value of <math|\<Delta\><rsub|<text|r>>*H> also depends on
  the reaction equation. For instance, if we change the reaction equation for
  ammonia synthesis from N<rsub|<math|2>><space|0.17em>+<space|0.17em>3<space|0.17em>H<rsub|<math|2>><space|0.17em><ra><space|0.17em>2<space|0.17em>NH<rsub|<math|3>>
  to

  <\equation*>
    <tfrac|1|2>*<text|N<rsub|2>>+<tfrac|3|2>*<text|H<rsub|2>><arrow><text|NH<rsub|3>>
  </equation*>

  then the value of <math|\<Delta\><rsub|<text|r>>*H> is halved.

  <subsection|Molar reaction quantities in general><label|11-molar rxn
  quantities in general><label|c11 sec-amrq-general>

  Now let us generalize the relations of the preceding section for any
  chemical process in a closed system. Suppose the stoichiometric equation
  has the form

  <\equation>
    a*<text|A>+b*<text|B>=d*<text|D>+e*<text|E>
  </equation>

  where A and B are reactant species, D and E are product species, and
  <math|a>, <math|b>, <math|d>, and <math|e> are the corresponding
  <subindex|Stoichiometric|coefficient>stoichiometric coefficients. We can
  rearrange this equation to

  <\equation>
    0=-a*<text|A>-b*<text|B>+d*<text|D>+e*<text|E>
  </equation>

  In general, the stoichiometric relation for any chemical process is

  <\equation>
    <label|stoich reln>0=<big|sum><rsub|i>\<nu\><rsub|i>*<text|A><rsub|i>
  </equation>

  where <math|\<nu\><rsub|i>> is the <subindex|Stoichiometric|number><newterm|stoichiometric
  number> of species A<rsub|<math|i>>, a dimensionless quantity taken as
  negative for a reactant and positive for a product. In the ammonia
  synthesis example of the previous section, the stoichiometric relation is
  <math|0=-<text|N<rsub|2>>-3*<text|H<rsub|2>>+2*<text|NH<rsub|3>>> and the
  stoichiometric numbers are <math|\<nu\><rsub|<text|N<rsub|2>>>=-1>,
  <math|\<nu\><rsub|<text|H<rsub|2>>>=-3>, and
  <math|\<nu\><rsub|<text|NH<rsub|3>>>=+2>. In other words, each
  stoichiometric number is the same as the stoichiometric coefficient in the
  reaction equation, except that the sign is negative for a reactant.

  The amount of reactant or product species <math|i> present in the closed
  system at any instant depends on the advancement at that instant, and is
  given by

  <equation-cov2|<label|n_i=n_(i,0)+(nu_i)xi>n<rsub|i>=n<rsub|i,0>+\<nu\><rsub|i>*\<xi\>|(closed
  system)>

  The infinitesimal change in the amount due to an infinitesimal change in
  the advancement is

  <equation-cov2|<label|dn_i=(nu_i)dxi><dif>n<rsub|i>=\<nu\><rsub|i><dif>\<xi\>|(closed
  system)>

  In an open system, the total differential of extensive property <math|X> is

  <\equation>
    <label|dX=.dT+.dp+sum><dif>X=<Pd|X|T|p,<allni>><dif>T+<Pd|X|p|T,<allni>><difp>+<big|sum><rsub|i>X<rsub|i><dif>n<rsub|i>
  </equation>

  where <math|X<rsub|i>> is a partial molar quantity. We restrict the system
  to a closed one with <math|T>, <math|p>, and <math|\<xi\>> as the
  independent variables. Then, with the substitution
  <math|<dif>n<rsub|i>=\<nu\><rsub|i><dif>\<xi\>> from Eq.
  <reference|dn_i=(nu_i)dxi>, the total differential of <math|X> becomes

  <equation-cov2|<label|dX=()dT+()dp+del(r)Xm*dxi><dif>X=<Pd|X|T|p,<space|0.17em>\<xi\>>*<dif>T+<Pd|X|p|T,<space|0.17em>\<xi\>>*<difp>+\<Delta\><rsub|<text|r>>*X*<dif>\<xi\>|(closed
  system)>

  where the coefficient <math|\<Delta\><rsub|<text|r>>*X> is the
  <subindex|Molar|reaction quantity><subindex|Reaction
  quantity|molar><newterm|molar reaction quantity> defined by

  <\equation>
    <label|del(r)Xm=sum(nu_i)X_i>\<Delta\><rsub|<text|r>>*X<defn><big|sum><rsub|i>\<nu\><rsub|i>*X<rsub|i>
  </equation>

  Equation <reference|dX=()dT+()dp+del(r)Xm*dxi> allows us to identify the
  molar reaction quantity as a partial derivative:

  <equation-cov2|<label|del(r)Xm=dX/dxi>\<Delta\><rsub|<text|r>>*X=<Pd|X|\<xi\>|T,p>|(closed
  system)>

  It is important to observe the distinction between the notations
  <math|<Del>X>, the finite change of <math|X> during a process, and
  <math|\<Delta\><rsub|<text|r>>*X>, a differential quantity that is a
  property of the system in a given state. The fact that both notations use
  the symbol <math|<Del>> can be confusing. Equation
  <reference|del(r)Xm=dX/dxi> shows that we can think of
  <math|\<Delta\><rsub|<text|r>>> as an <em|operator>.

  In dealing with the change of an extensive property <math|X> as
  <math|\<xi\>> changes, we must distinguish between molar integral and molar
  differential reaction quantities.

  <\itemize>
    <item><math|<Del>X/<Del>\<xi\>> is a <subindex|Molar|integral reaction
    quantity><subindex|Reaction quantity|molar integral>molar <em|integral>
    reaction quantity, the ratio of two finite differences between the final
    and initial states of a process. These states are assumed to have the
    same temperature and the same pressure. This book will use a notation
    such as <math|<Del>H<m><rxn>> for a molar integral reaction enthalpy:

    <equation-cov2|<Del>H<m><rxn>=<frac|<Del>H<rxn>|<Del>\<xi\>>=<frac|H<around|(|\<xi\><rsub|2>|)>-H<around|(|\<xi\><rsub|1>|)>|\<xi\><rsub|2>-\<xi\><rsub|1>>|(<math|T<rsub|2>=T<rsub|1>>,
    <math|p<rsub|2>=p<rsub|1>>)>

    <item><math|\<Delta\><rsub|<text|r>>*X> is a <subindex|Molar|differential
    reaction quantity><subindex|Reaction quantity|molar differential>molar
    <em|differential> reaction quantity. Equation <reference|del(r)Xm=dX/dxi>
    shows that <math|\<Delta\><rsub|<text|r>>*X> is the rate at which the
    extensive property <math|X> changes with the advancement in a closed
    system at constant <math|T> and <math|p>. The value of
    <math|\<Delta\><rsub|<text|r>>*X> is in general a function of the
    independent variables <math|T>, <math|p>, and <math|\<xi\>>.
  </itemize>

  The notation for a molar differential reaction quantity such as
  <math|\<Delta\><rsub|<text|r>>*H> includes a subscript following the
  <math|<Del>> symbol to indicate the kind of chemical process. The subscript
  \Pr\Q denotes a reaction or process in general. The meanings of \Pvap,\Q
  \Psub,\Q \Pfus,\Q and \Ptrs\Q were described in Sec. <reference|8-molar
  trans quantities>. Subscripts for specific kinds of reactions and processes
  are listed in Sec. <reference|app:abbrev-processes> of Appendix
  <reference|app:abbrev> and are illustrated in sections to follow.

  For certain kinds of processes, it may happen that a partial molar quantity
  <math|X<rsub|i>> remains constant for each species <math|i> as the process
  advances at constant <math|T> and <math|p>. If <math|X<rsub|i>> remains
  constant for each <math|i>, then according to Eq.
  <reference|del(r)Xm=sum(nu_i)X_i> the value of
  <math|\<Delta\><rsub|<text|r>>*X> must also remain constant as the process
  advances. Since <math|\<Delta\><rsub|<text|r>>*X> is the rate at which
  <math|X> changes with <math|\<xi\>>, in such a situation <math|X> is a
  linear function of <math|\<xi\>>. This means that the molar integral
  reaction quantity <math|<Del>X<m><rxn>> defined by
  <math|<Del>X/<Del>\<xi\>> is equal, for any finite change of <math|\<xi\>>,
  to <math|\<Delta\><rsub|<text|r>>*X>.<label|delX_m(rxn)=del(r)X>

  An example is the partial molar enthalpy <math|H<rsub|i>> of a constituent
  of an ideal gas mixture, an ideal condensed-phase mixture, or an
  ideal-dilute solution. In these ideal mixtures, <math|H<rsub|i>> is
  independent of composition at constant <math|T> and <math|p> (Secs.
  <reference|9-partial molar, id gas mixts>, <reference|9-partial molar, id
  mixts>, and <reference|9-partial molar, id dil sln>). When a reaction takes
  place at constant <math|T> and <math|p> in one of these mixtures, the molar
  differential reaction enthalpy <math|\<Delta\><rsub|<text|r>>*H> is
  constant during the process, <math|H> is a linear function of
  <math|\<xi\>>, and <math|\<Delta\><rsub|<text|r>>*H> and
  <math|<Del>H<m><rxn>> are equal. Figure <reference|fig:11-S-H-xi>(a) on
  page <pageref|fig:11-S-H-xi> illustrates this linear dependence for a
  reaction in an ideal gas mixture.<\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/S-H-xi.eps|303pt|174pt||>>
        <label|fig:11-S-H-xi>Enthalpy and entropy as functions of advancement
        at constant <math|T> and <math|p>. The curves are for a reaction
        A<math|\<rightarrow\>>2B with positive
        <math|\<Delta\><rsub|<text|r>>*H> taking place in an ideal gas
        mixture with initial amounts <math|n<rsub|<text|A>,0>=1<mol>> and
        <math|n<rsub|<text|B>,0>=0>.
      </big-figure>
    </framed>
  </float>

  In contrast, Fig. <reference|fig:11-S-H-xi>(b) shows the nonlinearity of
  the entropy as a function of <math|\<xi\>> during the same reaction. The
  nonlinearity is a consequence of the dependence of the partial molar
  entropy <math|S<rsub|i>> on the mixture composition (Eq. <reference|S_i=>).
  In the figure, the slope of the curve at each value of <math|\<xi\>> equals
  <math|\<Delta\><rsub|<text|r>>*S> at that point; its value changes as the
  reaction advances and the composition of the reaction mixture changes.
  Consequently, the molar integral reaction entropy
  <math|<Del>S<m><rxn>=<Del>S<rxn>/<Del>\<xi\>> approaches the value of
  <math|\<Delta\><rsub|<text|r>>*S> only in the limit as <math|<Del>\<xi\>>
  approaches zero.

  <subsection|Standard molar reaction quantities><label|11-st molar rxn
  quantities><label|c11 sec-amrq-std-quantities>

  If a chemical process takes place at constant temperature while each
  reactant and product remains in its standard state of unit activity, the
  molar reaction quantity <math|\<Delta\><rsub|<text|r>>*X> is called the
  <subindex|Standard molar|reaction quantity><subsubindex|Molar|reaction
  quantity|standard><newterm|standard molar reaction quantity> and is denoted
  by <math|\<Delta\><rsub|<text|r>>*X<st>>. For instance,
  <math|\<Delta\><rsub|<text|vap>>*H<st>> is a standard molar enthalpy of
  vaporization (already discussed in Sec. <reference|8-st molar trans
  quantities>), and <math|\<Delta\><rsub|<text|r>>*G<st>> is the standard
  molar Gibbs energy of a reaction.

  From Eq. <reference|del(r)Xm=sum(nu_i)X_i>, the relation between a standard
  molar reaction quantity and the standard molar quantities of the reactants
  and products at the same temperature is

  <\equation>
    <label|del(r)Xmo=sum(nu_i)X_io>\<Delta\><rsub|<text|r>>*X<st><defn><big|sum><rsub|i>\<nu\><rsub|i>*X<rsub|i><st>
  </equation>

  Two comments are in order.

  <\enumerate>
    <item>Whereas a molar reaction quantity is usually a function of
    <math|T>, <math|p>, and <math|\<xi\>>, a <em|standard> molar reaction
    quantity is a function only of <math|T>. This is evident because
    standard-state conditions imply that each reactant and product is in a
    separate phase of constant defined composition and constant pressure
    <math|p<st>>.

    <item><label|st molar diff and int identical>Since the value of a
    standard molar reaction quantity is independent of <math|\<xi\>>, the
    standard molar integral and differential quantities are identical (page
    <pageref|delX_m(rxn)=del(r)X>):

    <\equation>
      <Del>X<m><st><rxn>=\<Delta\><rsub|<text|r>>*X<st>
    </equation>
  </enumerate>

  These general concepts will now be applied to some specific chemical
  processes.

  <section|Molar Reaction Enthalpy><label|c11 sec mre>

  Recall that <math|<Del>H<m><rxn>> is a molar integral reaction enthalpy
  equal to <math|<Del>H<rxn>/<Del>\<xi\>>, and that
  <math|\<Delta\><rsub|<text|r>>*H> is a molar differential reaction enthalpy
  defined by <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*H<rsub|i>>
  and equal to <math|<pd|H|\<xi\>|T,p>>.

  <subsection|Molar reaction enthalpy and heat><label|c11 sec mre-heat>

  During a process in a closed system at constant pressure with expansion
  work only, the enthalpy change equals the energy transferred across the
  boundary in the form of heat: <math|<dif>H=<dq>> (Eq. <reference|dH=dq
  (dp=0)>). Thus for the molar reaction enthalpy
  <math|\<Delta\><rsub|<text|r>>*H=<pd|H|\<xi\>|T,p>>, which refers to a
  process not just at constant pressure but also at constant temperature, we
  can write

  <equation-cov2|<label|del(r)H=dq/dxi>\<Delta\><rsub|<text|r>>*H=<frac|<dq>|<dif>\<xi\>>|(constant
  <math|T> and <math|p>, <math|<dbar|w<rprime|'>>=0>)>

  Note that when there is nonexpansion work (<math|w<rprime|'>>), such as
  electrical work, the enthalpy change is not equal to the heat.<label|dH not
  equal to dq>For example, if we compare a reaction taking place in a
  galvanic cell with the same reaction in a reaction vessel, the heats at
  constant <math|T> and <math|p> for a given change of <math|\<xi\>> are
  different, and may even have opposite signs. The value of
  <math|\<Delta\><rsub|<text|r>>H> is the same in both systems, but the ratio
  of heat to advancement, <math|<dq>/<dif>\<xi\>>, is different.

  An <index|Exothermic reaction><subindex|Reaction|exothermic><newterm|exothermic>
  reaction is one for which <math|\<Delta\><rsub|<text|r>>H> is negative, and
  an <index|Endothermic reaction><subindex|Reaction|endothermic><newterm|endothermic>
  reaction is one for which <math|\<Delta\><rsub|<text|r>>H> is positive.
  Thus in a reaction at constant temperature and pressure with expansion work
  only, heat is transferred out of the system during an exothermic process
  and into the system during an endothermic process. If the process takes
  place at constant pressure in a system with thermally-insulated walls, the
  temperature increases during an exothermic process and decreases during an
  endothermic process.

  These comments apply not just to chemical reactions, but to the other
  chemical processes at constant temperature and pressure discussed in this
  chapter.

  <subsection|Standard molar enthalpies of reaction and
  formation><label|11-st molar enthalpy of formation><label|c11
  sec-mre-std-enthalpies>

  A <subsubindex|Enthalpy|reaction|standard molar><newterm|standard molar
  reaction enthalpy>, <math|\<Delta\><rsub|<text|r>>**H<st>>, is the same as
  the molar integral reaction enthalpy <math|<Del>H<m><rxn>> for the reaction
  taking place under standard state conditions (each reactant and product at
  unit activity) at constant temperature (page <pageref|st molar diff and int
  identical>).

  At constant temperature, partial molar enthalpies depend only mildly on
  pressure. It is therefore usually safe to assume that unless the
  experimental pressure is much greater than <math|p<st>>, the reaction is
  exothermic if <math|\<Delta\><rsub|<text|r>>*H<st>> is negative and
  endothermic if <math|\<Delta\><rsub|<text|r>>*H<st>> is positive.

  The <index|Formation reaction><newterm|formation reaction> of a substance
  is the reaction in which the substance, at a given temperature and in a
  given physical state, is formed from the constituent elements in their
  reference states at the same temperature. The
  <index-complex|<tuple|reference state|element>|||<tuple|Reference state|of
  an element>><em|reference state of an element> is usually chosen to be the
  standard state of the element in the allotropic form and physical state
  that is stable at the given temperature and the standard pressure. For
  instance, at <math|298.15<K>> and <math|1<br>> the stable allotrope of
  carbon is crystalline graphite rather than diamond.

  Phosphorus is an exception to the rule regarding reference states of
  elements. Although red phosphorus is the stable allotrope at
  <math|298.15<K>>, it is not well characterized. Instead, the reference
  state is white phosphorus (crystalline P<rsub|<math|4>>) at <math|1<br>>.

  At <math|298.15<K>>, the reference states of the elements are the
  following:

  <\itemize>
    <item>For H<rsub|<math|2>>, N<rsub|<math|2>>, O<rsub|<math|2>>,
    F<rsub|<math|2>>, Cl<rsub|<math|2>>, and the noble gases, the reference
    state is the ideal gas at <math|1<br>>.

    <item>For Br<rsub|<math|2>> and Hg, the reference state is the liquid at
    <math|1<br>>.

    <item>For P, as mentioned above, the reference state is crystalline white
    phosphorus at <math|1<br>>.

    <item>For all other elements, the reference state is the stable
    crystalline allotrope at <math|1<br>>.
  </itemize>

  The <index-complex|<tuple|enthalpy|formation standard>|||<tuple|Enthalpy|of
  formation, standard molar>><newterm|standard molar enthalpy of formation>
  (or standard molar heat of formation), <math|\<Delta\><rsub|<text|f>>*H<st>>,
  of a substance is the enthalpy change per amount of substance produced in
  the formation reaction of the substance in its standard state. Thus, the
  standard molar enthalpy of formation of gaseous methyl bromide at
  <math|298.15<K>> is the molar reaction enthalpy of the reaction

  <\equation*>
    <text|C><around*|(|<text|s>,<text|graphite>,p<st>|)>+<tfrac|3|2>*<text|H<rsub|2>><around*|(|<text|ideal
    gas>,p<st>|)>+<tfrac|1|2>*<text|Br<rsub|2>(1,p<st>)<arrow>CH<rsub|3>Br><around*|(|<text|ideal
    gas>,p<st>|)>
  </equation*>

  The value of <math|\<Delta\><rsub|<text|f>>*H<st>> for a given substance
  depends only on <math|T>. By definition,
  <math|\<Delta\><rsub|<text|f>>*H<st>> for the reference state of an element
  is zero.

  A principle called <index|Hess's law><newterm|Hess's law> can be used to
  calculate the standard molar enthalpy of formation of a substance at a
  given temperature from standard molar reaction enthalpies at the same
  temperature, and to calculate a standard molar reaction enthalpy from
  tabulated values of standard molar enthalpies of formation. The principle
  is an application of the fact that enthalpy is a state function. Therefore,
  <math|<Del>H> for a given change of the state of the system is independent
  of the path and is equal to the sum of <math|<Del>H> values for any
  sequence of changes whose net result is the given change. (We may apply the
  same principle to a change of <em|any> state function.)

  For example, the following combustion reactions can be carried out
  experimentally in a <subindex|Calorimeter|bomb><index|Bomb calorimeter>bomb
  calorimeter (Sec. <reference|11-bomb calorimeter>), yielding the values
  shown below of standard molar reaction enthalpies (at <math|T=298.15<K>>,
  <math|p=p<st>=1<br>>):

  <\eqnarray*>
    <tformat|<table|<row|<cell|<text|C><around*|(|<text|s>,<text|graphite>|)>+<text|O<rsub|2>><around*|(|<text|g>|)><arrow><text|CO<rsub|2>><around*|(|<text|g>|)>>|<cell|<text|<space|4em>>>|<cell|\<Delta\><rsub|<text|r>>*H<st>=-393.51
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|<text|CO><around*|(|<text|g>|)>+<tfrac|1|2>*<text|O<rsub|2>><around*|(|<text|g>|)><arrow><text|CO<rsub|2>><around*|(|<text|g>|)>>|<cell|>|<cell|\<Delta\><rsub|<text|r>>*H<st>=-282.98
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>>>
  </eqnarray*>

  (Note that the first reaction, in addition to being the combustion reaction
  of graphite, is also the formation reaction of carbon dioxide.) The change
  resulting from the first reaction followed by the reverse of the second
  reaction is the formation reaction of carbon monoxide:

  <\equation*>
    <text|C><around*|(|<text|s>,<text|graphite>|)>+<tfrac|1|2>*<text|O<rsub|2>><around*|(|<text|g>|)><arrow><text|CO><around*|(|<text|g>|)>
  </equation*>

  It would not be practical to measure the molar enthalpy of this last
  reaction by allowing graphite to react with oxygen in a calorimeter,
  because it would be difficult to prevent the formation of some
  CO<rsub|<math|2>>. From Hess's law, the standard molar enthalpy of
  formation of CO is the sum of the standard molar enthalpies of the
  reactions that have the formation reaction as the net result:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\><rsub|<text|r>>*H<st><around*|(|<text|CO>,<text|g>,298.15
    <text|K>|)>>|<cell|=>|<cell|<around*|(|-393.51+282.98|)>
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|>|<cell|=>|<cell|-110.53
    <text|kJ>\<cdot\><text|mol><rsup|-1><eq-number>>>>>
  </eqnarray*>

  This value is one of the many standard molar enthalpies of formation to be
  found in compilations of thermodynamic properties of individual substances,
  such as the table in Appendix <reference|app:props>. We may use the
  tabulated values to evaluate the standard molar reaction enthalpy
  <math|\<Delta\><rsub|<text|r>>*H<st>> of a reaction using a formula based
  on <index|Hess's law>Hess's law. Imagine the reaction to take place in two
  steps: First each reactant in its standard state changes to the constituent
  elements in their reference states (the reverse of a formation reaction),
  and then these elements form the products in their standard states. The
  resulting formula is

  <equation-cov2|<label|Hess's law>\<Delta\><rsub|<text|r>>*H<st>=<big|sum><rsub|i>\<nu\><rsub|i>\<Delta\><rsub|<text|f>>*H<st><around|(|i|)>|(Hess's
  law)>

  where <math|\<Delta\><rsub|<text|f>>*H<st><around|(|i|)>> is the standard
  molar enthalpy of formation of substance <math|i>. Recall that the
  <subindex|Stoichiometric|number>stoichiometric number <math|\<nu\><rsub|i>>
  of each reactant is negative and that of each product is positive, so
  according to Hess's law <subsubindex|Enthalpy|reaction|standard molar>the
  standard molar reaction enthalpy is the sum of the standard molar
  enthalpies of formation of the products minus the sum of the standard molar
  enthalpies of formation of the reactants. Each term is multiplied by the
  appropriate stoichiometric coefficient from the reaction equation.

  A <index-complex|<tuple|enthalpy|formation
  standard|solute>|||<tuple|Enthalpy|of formation, standard molar|of a
  solute>>standard molar enthalpy of formation can be defined for a
  <em|solute in solution> to use in Eq. <reference|Hess's law>. For instance,
  the formation reaction of aqueous sucrose is

  <\equation*>
    12*<text|C><around*|(|<text|s>,<text|graphite>|)>+11*<text|H<rsub|2>><around*|(|<text|g>|)>+<tfrac|11|2>*<text|O<rsub|2>><around*|(|<text|g>|)><arrow><text|C<rsub|12>H<rsub|22>O<rsub|11>><around*|(|<text|aq>|)>
  </equation*>

  and <math|\<Delta\><rsub|<text|f>>*H<st>> for
  C<rsub|<math|12>>H<rsub|<math|22>>O<rsub|<math|11>>(aq) is the enthalpy
  change per amount of sucrose formed when the reactants and product are in
  their standard states. Note that this formation reaction does <em|not>
  include the formation of the solvent H<rsub|<math|2>>O from
  H<rsub|<math|2>> and O<rsub|<math|2>>. Instead, the solute once formed
  combines with the amount of pure liquid water needed to form the solution.
  If the aqueous solute is formed in its standard state, the amount of water
  needed is very large so as to have the solute exhibit infinite-dilution
  behavior.

  There is no ordinary reaction that would produce an individual <em|ion in
  solution> from its element or elements without producing other species as
  well. We can, however, prepare a consistent set of
  <index-complex|<tuple|enthalpy|formation standard|ion>|||<tuple|Enthalpy|of
  formation, standard molar|of an ion>>standard molar enthalpies of formation
  of ions by assigning a value to a single reference ion.<footnote|This
  procedure is similar to that described on page <pageref|conventional V(i)
  for ion> for partial molar volumes of ions.> We can use these values for
  ions in Eq. <reference|Hess's law> just like values of
  <math|\<Delta\><rsub|<text|f>>*H<st>> for substances and nonionic solutes.
  Aqueous hydrogen ion is the usual reference ion, to which is assigned the
  arbitrary value

  <\equation>
    \<Delta\><rsub|<text|f>>*H<st><around*|(|<text|H><rsup|+>,<text|aq>|)>=0<space|2em><text|at
    all temperatures>
  </equation>

  To see how we can use this reference value, consider the reaction for the
  formation of aqueous HCl (hydrochloric acid):

  <\equation*>
    <tfrac|1|2>*<text|H<rsub|2>><around*|(|<text|g>|)>+<tfrac|1|2>*<text|Cl<rsub|2>><around*|(|<text|g>|)><arrow><text|H><rsup|+><around*|(|<text|aq>|)>+<text|Cl><rsup|-><around*|(|<text|aq>|)>
  </equation*>

  The standard molar reaction enthalpy at <math|298.15<K>> for this reaction
  is known, from <subindex|Calorimetry|reaction>reaction calorimetry, to have
  the value <math|\<Delta\><rsub|<text|r>>*H<st>=-167.08
  <text|kJ>\<cdot\><text|mol><rsup|-1>>. The standard states of the gaseous
  H<rsub|<math|2>> and Cl<rsub|<math|2>> are, of course, the pure gases
  acting ideally at pressure <math|p<st>>, and the standard state of each of
  the aqueous ions is the ion at the standard molality and standard pressure,
  acting as if its activity coefficient on a molality basis were <math|1>.
  From Eq. <reference|Hess's law>, we equate the value of
  <math|\<Delta\><rsub|<text|r>>*H<st>> to the sum

  <\equation*>
    -<tfrac|1|2>*\<Delta\><rsub|<text|f>>*H<st><around*|(|<text|H<rsub|2>,g>|)>-<tfrac|1|2>*\<Delta\><rsub|<text|f>>*H<st><around*|(|<text|Cl<rsub|2>>,<text|g>|)>+\<Delta\><rsub|<text|f>>*H<st><around*|(|<text|H><rsup|+>,aq|)>+\<Delta\><rsub|<text|f>>*H<st><around*|(|<text|Cl><rsup|->,<text|aq>|)>
  </equation*>

  But the first three terms of this sum are zero. Therefore, the value of
  <math|\<Delta\><rsub|<text|f>>*H<st>>(Cl<rsup|<math|->>,<space|0.17em>aq)
  is <math|-167.08 <text|kJ>\<cdot\><text|mol><rsup|-1>>.

  Next we can combine this value of <math|\<Delta\><rsub|<text|f>>*H<st>>(Cl<rsup|<math|->>,<space|0.17em>aq)
  with the measured standard molar enthalpy of formation of aqueous sodium
  chloride

  <\equation*>
    <text|Na><around*|(|<text|s>|)>+<tfrac|1|2>*<text|Cl<rsub|2>><around*|(|<text|g>|)><arrow><text|Na><rsup|+><around*|(|<text|aq>|)>+<text|Cl><rsup|-><around*|(|<text|aq>|)>
  </equation*>

  to evaluate the standard molar enthalpy of formation of aqueous sodium ion.
  By continuing this procedure with other reactions, we can build up a
  consistent set of <math|\<Delta\><rsub|<text|f>>*H<st>> values of various
  ions in aqueous solution.

  <subsection|Molar reaction heat capacity><label|c11 sec mre-heat-capacity>

  The molar reaction enthalpy <math|\<Delta\><rsub|<text|r>>*H> is in general
  a function of <math|T>, <math|p>, and <math|\<xi\>>. Using the relations
  <math|\<Delta\><rsub|<text|r>>*H=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*H<rsub|i>>
  (from Eq. <reference|del(r)Xm=sum(nu_i)X_i>) and
  <math|C<rsub|p,i>=<pd|H<rsub|i>|T|p,<space|0.17em>\<xi\>>> (Eq.
  <reference|C_pi=dH_i/dT>), we can write

  <\equation>
    <label|dDel(r)H/dT=><Pd|\<Delta\><rsub|<text|r>>*H|T|p,<space|0.17em>\<xi\>>=<Pd|<big|sum><rsub|i>\<nu\><rsub|i>*H<rsub|i>|T|p,<space|0.17em>\<xi\>>=<big|sum><rsub|i>\<nu\><rsub|i>*C<rsub|p,i>=\<Delta\><rsub|<text|r>>*C<rsub|p>
  </equation>

  where <math|\<Delta\><rsub|<text|r>>*C<rsub|p>> is the <subindex|Heat
  capacity|molar reaction>molar reaction heat capacity at constant pressure,
  equal to the rate at which the heat capacity <math|C<rsub|p>> changes with
  <math|\<xi\>> at constant <math|T> and <math|p>.

  Under standard state conditions, Eq. <reference|dDel(r)H/dT=> becomes

  <\equation>
    <label|dDel(r)H^o/dT=><dif>\<Delta\><rsub|<text|r>>*H<st>/<dif>T=\<Delta\><rsub|<text|r>>*C<rsub|p><st>
  </equation>

  <subsection|Effect of temperature on reaction enthalpy><label|c11
  sec-mre-effect-temperature>

  <index-complex|<tuple|enthalpy|molar, effect of temperature on>||c11
  sec-mre-effect-temperature idx1|<tuple|Enthalpy|molar, effect of
  temperature on>>Consider a reaction occurring with a certain finite change
  of the advancement in a closed system at temperature <math|T<rprime|'>> and
  at constant pressure. The reaction is characterized by a change of the
  advancement from <math|\<xi\><rsub|1>> to <math|\<xi\><rsub|2>>, and the
  integral reaction enthalpy at this temperature is denoted
  <math|<Del>H<around*|(|<text|rxn>,T<rprime|'>|)>>. We wish to find an
  expression for the reaction enthalpy <math|<Del>H<around*|(|<text|rxn>,T<rprime|''>|)>>
  for the same values of <math|\<xi\><rsub|1>> and <math|\<xi\><rsub|2>> at
  the same pressure but at a different temperature, <math|T<rprime|''>>.

  The heat capacity of the system at constant pressure is related to the
  enthalpy by Eq. <vpageref|Cp=dH/dT>: <math|C<rsub|p>=<pd|H|T|p,<space|0.17em>\<xi\>>>.
  We integrate <math|<dif>H=C<rsub|p>*<dif>T> from <math|T<rprime|'>> to
  <math|T<rprime|''>> at constant <math|p> and <math|\<xi\>>, for both the
  final and initial values of the advancement:

  <\equation>
    <label|H(xi_2)=>H*<around|(|\<xi\><rsub|2>,T<rprime|''>|)>=H*<around|(|\<xi\><rsub|2>,T<rprime|'>|)>+<big|int><rsub|T<rprime|'>><rsup|T<rprime|''>><space|-0.17em><space|-0.17em>C<rsub|p><around|(|\<xi\><rsub|2>|)>*<dif>T
  </equation>

  <\equation>
    <label|H(xi_1)=>H*<around|(|\<xi\><rsub|1>,T<rprime|''>|)>=H*<around|(|\<xi\><rsub|1>,T<rprime|'>|)>+<big|int><rsub|T<rprime|'>><rsup|T<rprime|''>><space|-0.17em><space|-0.17em>C<rsub|p><around|(|\<xi\><rsub|1>|)>*<dif>T
  </equation>

  Subtracting Eq. <reference|H(xi_1)=> from Eq. <reference|H(xi_2)=>, we
  obtain

  <\equation>
    <label|Kirchhoff eq-1><Del>H<around*|(|<text|rxn>,T<rprime|''>|)>=<Del>H<around*|(|rxn,T<rprime|'>|)>+<big|int><rsub|T*'><rsup|T*'*'><space|-0.17em><space|-0.17em><space|-0.17em><Del>C<rsub|p>*<dif>T
  </equation>

  where <math|<Del>C<rsub|p>> is the difference between the heat capacities
  of the system at the final and initial values of <math|\<xi\>>, a function
  of <math|T>: <math|<Del>C<rsub|p>=C<rsub|p><around|(|\<xi\><rsub|2>|)>-C<rsub|p><around|(|\<xi\><rsub|1>|)>>.
  Equation <reference|Kirchhoff eq-1> is the <index|Kirchhoff
  equation><newterm|Kirchhoff equation>.

  When <math|<Del>C<rsub|p>> is essentially constant in the temperature range
  from <math|T<rprime|'>> to <math|T<rprime|''>>, the Kirchhoff equation
  becomes

  <\equation>
    <label|Kirchhoff eq-2><Del>H<around*|(|<text|rxn>,T<rprime|''>|)>=<Del>H<around*|(|<text|rxn>,T<rprime|'>|)>+<Del>C<rsub|p>*<around|(|T<rprime|''>-T'|)>
  </equation>

  Figure <vpageref|fig:11-Kirchhoff><\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/KIRCHHOF.eps|178pt|112pt||>>
        <label|fig:11-Kirchhoff>Dependence of reaction enthalpy on
        temperature at constant pressure.
      </big-figure>
    </framed>
  </float>

  illustrates the principle of the Kirchhoff equation as expressed by Eq.
  <reference|Kirchhoff eq-2>. <math|<Del>C<rsub|p>> equals the difference in
  the slopes of the two dashed lines in the figure, and the product of
  <math|<Del>C<rsub|p>> and the temperature difference
  <math|T<rprime|''>-T<rprime|'>> equals the change in the value of
  <math|<Del>H<rxn>>. The figure illustrates an exothermic reaction with
  negative <math|<Del>C<rsub|p>>, resulting in a more negative value of
  <math|<Del>H<rxn>> at the higher temperature.

  We can also find the effect of temperature on the molar differential
  reaction enthalpy <math|\<Delta\><rsub|<text|r>>*H>. From Eq.
  <reference|dDel(r)H/dT=>, we have <math|<pd|\<Delta\><rsub|<text|r>>*H|T|p,<space|0.17em>\<xi\>>=\<Delta\><rsub|<text|r>>*C<rsub|p>>.
  Integration from temperature <math|T<rprime|'>> to temperature
  <math|T<rprime|''>> yields the relation

  <\equation>
    <label|Del(r)H(T'')=>\<Delta\><rsub|<text|r>>*H*<around|(|T<rprime|''><space|-0.17em>,\<xi\>|)>=\<Delta\><rsub|<text|r>>*H*<around|(|T<rprime|'><space|-0.17em>,\<xi\>|)>+<big|int><rsub|T<rprime|'>><rsup|T<rprime|''>><space|-0.17em><space|-0.17em>\<Delta\><rsub|<text|r>>*C<rsub|p>*<around|(|T,\<xi\>|)>*<dif>T
  </equation>

  This relation is analogous to Eq. <reference|Kirchhoff eq-1>, using molar
  differential reaction quantities in place of integral reaction
  quantities.<index-complex|<tuple|enthalpy|molar, effect of temperature
  on>||c11 sec-mre-effect-temperature idx1|<tuple|Enthalpy|molar, effect of
  temperature on>>

  <\bio-insert>
    <include|bio-HESS.tm>
  </bio-insert>

  <page-break>

  <section|Enthalpies of Solution and Dilution><label|11-enthalpies of soln
  and diln><label|c11 sec esd>

  The processes of solution (dissolution) and dilution are related. The
  <index|IUPAC Green Book>IUPAC Green Book<footnote|Ref. <cite|greenbook-3>,
  Sec. 2.11.1.> recommends the abbreviations sol and dil for these processes.

  During a <subindex|Solution|process><subindex|Process|solution><newterm|solution
  process>, a solute is transferred from a pure solute phase (solid, liquid,
  or gas) to a solvent or solution phase. During a <index|Dilution
  process><subindex|Process|dilution><newterm|dilution process>, solvent is
  transferred from a pure solvent phase to a solution phase. We may specify
  the advancement of these two kinds of processes by
  <math|\<xi\><rsub|<text|sol>>> and <math|\<xi\><rsub|<text|dil>>>,
  respectively. Note that both processes take place in <em|closed> systems
  that (at least initially) have two phases. The total amounts of solvent and
  solute in the systems do not change, but the amounts in pure phases
  diminish as the processes advance and <math|\<xi\><rsub|<text|sol>>> or
  <math|\<xi\><rsub|<text|dil>>> increases (Fig.
  <vpageref|fig:11-soln/diln>).<\float|float|hb>
    <\framed>
      <\big-figure|<image|11-SUP/SOL-DIL.eps|268pt|88pt||>>
        <label|fig:11-soln/diln>Two related processes in closed systems. A:
        solvent; B: solute. The dashed rectangles represent the system
        boundaries.

        <\enumerate-alpha>
          <item>Solution process.

          <item>Dilution process.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float>

  The equations in this section are about enthalpies of solution and
  dilution, but you can replace <math|H> by any other extensive state
  function to obtain relations for its solution and dilution properties.

  <subsection|Molar enthalpy of solution><label|11-enthalpy of sln><label|c11
  sec esd-solution>

  First let us consider a solution process in which solute is transferred
  from a pure solute phase to a solution. The
  <index-complex|<tuple|enthalpy|solution|molar
  differential>|||<tuple|Enthalpy|of solution|molar
  differential>><newterm|molar differential enthalpy of solution>,
  <math|\<Delta\><rsub|<text|sol>>*H>, is the rate of change of <math|H> with
  the advancement <math|\<xi\><rsub|<text|sol>>> at constant <math|T> and
  <math|p>, where <math|\<xi\><rsub|<text|sol>>> is the amount of solute
  transferred:

  <\equation>
    <label|del(sol)Hm=dH/d(xi(sol))>\<Delta\><rsub|<text|sol>>*H=<Pd|H|\<xi\><rsub|<text|sol>>|T,p,n<A>>
  </equation>

  The value of <math|\<Delta\><rsub|<text|sol>>*H> at a given <math|T> and
  <math|p> depends only on the solution molality and not on the amount of
  solution.

  When we write the solution reaction as B<math|<rsup|\<ast\>><arrow>>B(sln),
  the general relation <math|\<Delta\><rsub|<text|r>>*X=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*X<rsub|i>>
  (Eq. <reference|del(r)Xm=sum(nu_i)X_i>) becomes

  <\equation>
    <label|del(sol)Hm=HB-Hmb*>\<Delta\><rsub|<text|sol>>*H=H<B>-H<B><rsup|\<ast\>>
  </equation>

  where <math|H<B>> is the partial molar enthalpy of the solute in the
  solution and <math|H<B><rsup|\<ast\>>> is the molar enthalpy of the pure
  solute at the same <math|T> and <math|p>.

  The <index-complex|<tuple|enthalpy|solution|infinite
  dilution>|||<tuple|Enthalpy|of solution|at infinite
  dilution>><newterm|molar enthalpy of solution at infinite dilution>,
  <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>>, is the rate of change
  of <math|H> with <math|\<xi\><rsub|<text|sol>>> when the solute is
  transferred to a solution with the thermal properties of an infinitely
  dilute solution. We can think of <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>>
  as the enthalpy change per amount of solute transferred to a very large
  volume of pure solvent. According to Eq. <reference|del(sol)Hm=HB-Hmb*>,
  this quantity is given by

  <\equation>
    <label|del(sol)Hm(infty)=HB(infty)-Hmb*>\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>=H<B><rsup|\<infty\>>-H<B><rsup|\<ast\>>
  </equation>

  Note that because the values of <math|H<B><rsup|\<infty\>>> and
  <math|H<B><rsup|\<ast\>>> are independent of the solution composition, the
  molar differential and integral enthalpies of solution at infinite dilution
  are the same.

  An <index|Integral enthalpy of solution><index-complex|<tuple|enthalpy|solution|integral>|||<tuple|Enthalpy|of
  solution|integral>><newterm|integral enthalpy of solution>,
  <math|<Del>H<sol>>, is the enthalpy change for a process in which a finite
  amount <math|\<xi\><rsub|<text|sol>>> of solute is transferred from a pure
  solute phase to a specified amount of pure solvent to form a homogeneous
  solution phase with the same temperature and pressure as the initial state.
  Division by the amount transferred gives the
  <index-complex|<tuple|enthalpy|solution|molar
  integral>|||<tuple|Enthalpy|of solution|molar integral>><newterm|molar
  integral enthalpy of solution> which this book will denote by
  <math|<Del>H<m><solmB>>, where <math|m<B>> is the molality of the solution
  formed:

  <\equation>
    <label|del(sol)Hm(int)=del(sol)H/xi(sol)><Del>H<m><solmB>=<frac|<Del>H<sol>|\<xi\><rsub|<text|sol>>>
  </equation>

  An integral enthalpy of solution can be evaluated by carrying out the
  solution process in a constant-pressure reaction calorimeter, as will be
  described in Sec. <reference|11-constant-pressure calorimeter>.
  Experimental values of <math|<Del>H<sol>> as a function of
  <math|\<xi\><rsub|<text|sol>>> can be collected by measuring enthalpy
  changes during a series of successive additions of the solute to a fixed
  amount of solvent, resulting in a solution whose molality increases in
  stages. The enthalpy changes are cumulative, so the value of
  <math|<Del>H<sol>> after each addition is the sum of the enthalpy changes
  for this and the previous additions.

  The relations between <math|<Del>H<sol>> and the molar integral and
  differential enthalpies of solution are illustrated in Fig.
  <vpageref|fig:11-Na acetate><\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/NAOAC.eps|209pt|163pt||>>
        <label|fig:11-Na acetate>Enthalpy change for the dissolution of
        NaCH<rsub|<math|3>>CO<rsub|<math|2>>(s) in one kilogram of water in a
        closed system at <math|298.15<K>> and <math|1<br>>, as a function of
        the amount <math|\<xi\><rsub|<text|sol>>> of dissolved
        solute.<note-ref|+17DAf9rZ1rq092F6> The open circle at
        <math|\<xi\><rsub|<text|sol>>=15 <mol>> indicates the approximate
        saturation limit; data to the right of this point come from
        supersaturated solutions. At the composition <math|m<B|=>=15
        <text|mol>\<cdot\><text|kg><rsup|-1>>, the value of
        <math|<Del>H<m><solmB>> is the slope of line a and the value of
        <math|\<Delta\><rsub|<text|sol>>*H> is the slope of line b. The value
        of <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>> is the slope
        of line c.

        \;

        <note-inline||+17DAf9rZ1rq092F6>Data from Ref. <cite|wagman-82>, page
        2-315.
      </big-figure>
    </framed>
  </float> with data for the solution of crystalline sodium acetate in water.
  The curve shows <math|<Del>H<sol>> as a function of
  <math|\<xi\><rsub|<text|sol>>>, with <math|\<xi\><rsub|<text|sol>>> defined
  as the amount of solute dissolved in one kilogram of water. Thus at any
  point along the curve, the molality is <math|m<B>=\<xi\><rsub|<text|sol>>/<around|(|1
  <text|kg>|)>> and the ratio <math|<Del>H<sol>/\<xi\><rsub|<text|sol>>> is
  the molar integral enthalpy of solution <math|<Del>H<m><solmB>> for the
  solution process that produces solution of this molality. The slope of the
  curve is the <index-complex|<tuple|enthalpy|solution|molar
  differential>|||<tuple|Enthalpy|of solution|molar differential>>molar
  differential enthalpy of solution:

  <equation-cov2|<label|del(sol)Hm=d del(sol)H/d
  xi(sol)>\<Delta\><rsub|<text|sol>>*H=<frac|<dif><Del>H<sol>|<dif>\<xi\><rsub|<text|sol>>>|(constant
  <math|T>, <math|p>, and <math|n<A>>)>

  The slope of the curve at <math|\<xi\><rsub|<text|sol>>=0> is
  <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>>, the molar enthalpy of
  solution at infinite dilution. If the measurements are made at the standard
  pressure, <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>> is the same
  as the standard molar enthalpy of solution,
  <math|\<Delta\><rsub|<text|sol>>*H<st>>, because the standard molar
  enthalpy of a solute is the molar enthalpy at <math|p=p<st>> and infinite
  dilution.

  <subsection|Enthalpy of dilution><label|c11 sec esd-dilution>

  Next let us consider a dilution process in which solvent is transferred
  from a pure solvent phase to a solution phase. The
  <index-complex|<tuple|enthalpy|dilution|molar
  differential>|||<tuple|Enthalpy|of dilution|molar
  differential>><newterm|molar differential enthalpy of dilution> is the rate
  of change of <math|H> with the advancement <math|\<xi\><rsub|<text|dil>>>
  at constant <math|T> and <math|p> of the dilution process, where
  <math|\<xi\><rsub|<text|dil>>> is the amount of solvent transferred:

  <\equation>
    <label|del(dil)Hm=dH/d(xi(dil))>\<Delta\><rsub|<text|dil>>*H=<Pd|H|\<xi\><rsub|<text|dil>>|T,p,n<B>>
  </equation>

  For the dilution reaction A<math|<rsup|\<ast\>><arrow>>A(sln), the general
  relation <math|\<Delta\><rsub|<text|r>>*X=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*X<rsub|i>>
  becomes

  <\equation>
    <label|del(dil)Hm=HA-HmA*>\<Delta\><rsub|<text|dil>>*H=H<A>-H<A><rsup|\<ast\>>
  </equation>

  where <math|H<A>> is the partial molar enthalpy of the solvent in the
  solution. In the limit of infinite dilution, <math|H<A>> must approach the
  molar enthalpy of pure solvent, <math|H<A><rsup|\<ast\>>>; then Eq.
  <reference|del(dil)Hm=HA-HmA*> shows that
  <math|\<Delta\><rsub|<text|dil>>*H> approaches zero in this limit.

  An <index|Integral enthalpy of dilution><index-complex|<tuple|enthalpy|dilution|integral>|||<tuple|Enthalpy|of
  dilution|integral>><newterm|integral enthalpy of dilution>,
  <math|<Del>H<dil>>, refers to the enthalpy change for transfer of a finite
  amount of solvent from a pure solvent phase to a solution, <math|T> and
  <math|p> being the same before and after the process. The
  <index-complex|<tuple|enthalpy|dilution|molar
  integral>|||<tuple|Enthalpy|of dilution|molar integral>><newterm|molar
  integral enthalpy of dilution> is the ratio of <math|<Del>H<dil>> and the
  amount of solute in the solution. For a dilution process at constant solute
  amount <math|n<B>> in which the molality changes from <math|m<B><rprime|'>>
  to <math|m<B><rprime|''>>, this book will use the notation
  <math|<Del>H<m><around*|(|<text|dil>,m<B><rprime|'><ra>m<B><rprime|''>|)><around|(|<text|dil>,<math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>>:

  <\equation>
    <Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>=<frac|<Del>H<dil>|n<B>>
  </equation>

  The value of <math|<Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>>
  at a given <math|T> and <math|p> depends only on the initial and final
  molalities <math|m<B><rprime|'>> and <math|m<B><rprime|''>>.

  There is a simple relation between molar integral enthalpies of solution
  and dilution, as the following derivation demonstrates. Consider the
  following two ways of preparing a solution of molality
  <math|m<B><rprime|''>> from pure solvent and solute phases. Both paths are
  at constant <math|T> and <math|p> in a closed system.

  <\itemize>
    <item>Path 1: The solution forms directly by dissolution of the solute in
    the solvent. The enthalpy change is <math|n<B><Del>H<m><around|(|<text|sol>,<space|0.17em><math|m<B><rprime|''>>|)>>,
    where the molality of the solution is indicated in parentheses.

    <item>Path 2: Starting with the unmixed solvent and solute, the solute
    dissolves in a portion of the solvent to form a solution of composition
    <math|m<rprime|'><B>> (more concentrated than <math|m<B><rprime|''>>).
    The enthalpy change is <math|n<B><Del>H<m><around|(|<text|sol>,<space|0.17em><math|m<B><rprime|'>>|)>>.
    In a second step of this path, the remaining pure solvent mixes with the
    solution to dilute it from <math|m<B><rprime|'>> to
    <math|m<B><rprime|''>>. The enthalpy change of the second step is
    <math|n<B><Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>>.
  </itemize>

  Since both paths have the same initial states and the same final states,
  both have the same overall enthalpy change:

  <\equation>
    n<B>*<Del>H<m><around|(|<text|sol>,<space|0.17em><math|m<B><rprime|''>>|)>=n<B>*<Del>H<m><around|(|<text|sol>,<space|0.17em><math|m<B><rprime|'>>|)>+n<B>*<Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>
  </equation>

  or

  <\equation>
    <label|DelH(sol,mB'')=.+.><Del>H<m><around|(|<text|sol>,<space|0.17em><math|m<B><rprime|''>>|)>=<Del>H<m><around|(|<text|sol>,<space|0.17em><math|m<B><rprime|'>>|)>+<Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>
  </equation>

  Equation <reference|DelH(sol,mB'')=.+.> is the desired relation. It shows
  how a measurement of the molar integral enthalpy change for a solution
  process that produces solution of a certain molality can be combined with
  dilution measurements in order to calculate molar integral enthalpies of
  solution for more dilute solutions. Experimentally, it is sometimes more
  convenient to carry out the dilution process than the solution process,
  especially when the pure solute is a gas or solid.

  <subsection|Molar enthalpies of solute formation><label|11-solute
  formation><label|c11 sec-esd-solute-formation>

  Molar integral enthalpies of solution and dilution are conveniently
  expressed in terms of molar enthalpies of formation. The
  <index-complex|<tuple|enthalpy|formation of a solute>|||<tuple|Enthalpy|of
  formation of a solute, molar>>molar enthalpy of formation of a solute in
  solution is the enthalpy change per amount of solute for a process at
  constant <math|T> and <math|p> in which the solute, in a solution of a
  given molality, is formed from its constituent elements in their reference
  states. The molar enthalpy of formation of solute B in solution of molality
  <math|m<B>> will be denoted by <math|\<Delta\><rsub|<text|f>>*H<around|(|<text|B>,<space|0.17em><math|m<B>>|)>>.

  As explained in Sec. <reference|11-st molar enthalpy of formation>, the
  formation reaction of a solute in solution does not include the formation
  of the solvent from its elements. For example, the formation reaction for
  NaOH in an aqueous solution that has <math|50> moles of water for each mole
  of NaOH is

  <\equation*>
    <text|Na><around*|(|<text|s>|)>+<tfrac|1|2>*<text|O<rsub|2>><around*|(|<text|g>|)>+<tfrac|1|2>*<text|H<rsub|2>><around*|(|<text|g>|)>+50*<text|H<rsub|2>O><around*|(|<text|l>|)><ra><text|NaOH>
    <text|in> 50*<text|H<rsub|2>O>
  </equation*>

  Consider a solution process at constant <math|T> and <math|p> in which an
  amount <math|n<B>> of pure solute (solid, liquid, or gas) is mixed with an
  amount <math|n<A>> of pure solvent, resulting in solution of molality
  <math|m<B>>. We may equate the enthalpy change of this process to the sum
  of the enthalpy changes for the following two hypothetical steps:

  <\enumerate>
    <item>An amount <math|n<B>> of the pure solute decomposes to the
    constituent elements in their reference states. This is the reverse of
    the formation reaction of the pure solute.

    <item>The solution is formed from these elements and an amount
    <math|n<A>> of the solvent.
  </enumerate>

  The total enthalpy change is then <math|<Del>H<sol>=-n<B>\<Delta\><rsub|<text|f>>*H<around|(|<text|B><rsup|<math|\<ast\>>>|)>+n<B>\<Delta\><rsub|<text|f>>*H<around|(|<text|B>,<space|0.17em><math|m<B>>|)>>.
  Dividing by <math|n<B>>, we obtain the molar integral enthalpy of solution:

  <\equation>
    <label|del(sol)H(int)=-(nB)del(f)Hmo(B*)+...><Del>H<m><solmB>=\<Delta\><rsub|<text|f>>*H<around|(|<text|B>,<space|0.17em><math|m<B>>|)>-\<Delta\><rsub|<text|f>>*H<around|(|<text|B><rsup|<math|\<ast\>>>|)>
  </equation>

  By combining Eqs. <reference|DelH(sol,mB'')=.+.> and
  <reference|del(sol)H(int)=-(nB)del(f)Hmo(B*)+...>, we obtain the following
  expression for a molar integral enthalpy of dilution in terms of molar
  enthalpies of formation:

  <\equation>
    <label|DelHm(dil,m'-\<gtr\>m'')=><Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>=\<Delta\><rsub|<text|f>>*H<around|(|<text|B>,<space|0.17em><math|m<B><rprime|''>>|)>-\<Delta\><rsub|<text|f>>*H<around|(|<text|B>,<space|0.17em><math|m<B><rprime|'>>|)>
  </equation>

  From tabulated values of molar enthalpies of formation, we can calculate
  <index-complex|<tuple|enthalpy|solution|molar
  integral>|||<tuple|enthalpy|of solution|molar integral>>molar integral
  enthalpies of solution with Eq. <reference|del(sol)H(int)=-(nB)del(f)Hmo(B*)+...>
  and molar integral enthalpies of dilution with Eq.
  <reference|DelHm(dil,m'-\<gtr\>m'')=>. Conversely, calorimetric
  measurements of these molar integral enthalpies can be combined with the
  value of <math|\<Delta\><rsub|<text|f>>*H<around|(|<text|B><rsup|<math|\<ast\>>>|)>>
  to establish the values of molar enthalpies of solute formation in
  solutions of various molalities.

  <subsection|Evaluation of relative partial molar enthalpies><label|11-eval
  of partial molar H><label|c11 sec-esd-relative-partial>

  Although it is not possible to determine absolute values of partial molar
  enthalpies, we can evaluate <math|H<A>> and <math|H<B>> relative to
  appropriate solvent and solute reference states.

  The <index-complex|<tuple|relative partial molar
  enthalpy|solvent>|||<tuple|Relative partial molar enthalpy|of the
  solvent>><subsubindex|Partial molar|enthalpy|relative, of the
  solvent><subsubindex|Enthalpy|partial molar|relative, of the
  solvent><newterm|relative partial molar enthalpy of the solvent> is defined
  by

  <\equation>
    <label|L(A) defn>L<A><defn>H<A>-H<A><rsup|\<ast\>>
  </equation>

  This is the partial molar enthalpy of the solvent in a solution of given
  composition relative to pure solvent at the same temperature and pressure.

  <math|L<A>> can be related to molar differential and integral enthalpies of
  solution as follows. The enthalpy change to form a solution from amounts
  <math|n<A>> and <math|n<B>> of pure solvent and solute is given, from the
  <index|Additivity rule>additivity rule, by
  <math|<Del>H<sol>=<around|(|n<A>H<A>+n<B>H<B>|)>-<around|(|n<A>H<A><rsup|\<ast\>>+n<B>H<B><rsup|\<ast\>>|)>>.
  We rearrange and make substitutions from Eqs.
  <reference|del(sol)Hm=HB-Hmb*> and <reference|L(A) defn>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<Del>H<sol>>|<cell|=>|<cell|n<A>*<around|(|H<A>-H<A><rsup|\<ast\>>|)>+n<B>*<around|(|H<B>-H<B><rsup|\<ast\>>|)>>>|<row|<cell|>|<cell|=>|<cell|<A>L<A>+n<B>\<Delta\><rsub|<text|sol>>*H<eq-number>>>>>
  </eqnarray*>

  <math|<Del>H<sol>> is also given, from Eq.
  <reference|del(sol)Hm(int)=del(sol)H/xi(sol)>, by

  <\equation>
    <Del>H<sol>=n<B><Del>H<m><solmB>
  </equation>

  Equating both expressions for <math|<Del>H<sol>>, solving for <math|L<A>>,
  and replacing <math|n<B>/n<A>> by <math|M<A>m<B>>, we obtain

  <\equation>
    <label|L_A=M_A*mB[]>L<A>=M<A>m<B><around*|[|<Del>H<m><solmB>-\<Delta\><rsub|<text|sol>>*H|]>
  </equation>

  Thus <math|L<A>> depends on the difference between the molar integral and
  differential enthalpies of solution.

  The <index-complex|<tuple|relative partial molar
  enthalpy|solute>|||<tuple|Relative partial molar enthalpy|of a
  solute>><subsubindex|Partial molar|enthalpy|relative, of a
  solute><subsubindex|Enthalpy|partial molar|relative, of a
  solute><newterm|relative partial molar enthalpy of a solute> is defined by

  <\equation>
    <label|L(B) defn>L<B><defn>H<B>-H<B><rsup|\<infty\>>
  </equation>

  The reference state for the solute is the solute at infinite dilution. To
  relate <math|L<B>> to molar enthalpies of solution, we write the identity

  <\equation>
    L<B>=H<B>-H<B><rsup|\<infty\>>=<around|(|H<B>-H<B><rsup|\<ast\>>|)>-<around|(|H<B><rsup|\<infty\>>-H<B><rsup|\<ast\>>|)>
  </equation>

  From Eqs. <reference|del(sol)Hm=HB-Hmb*> and
  <reference|del(sol)Hm(infty)=HB(infty)-Hmb*>, this becomes

  <\equation>
    <label|L_B=Del(sol)H-Del(sol)H^infty>L<B>=\<Delta\><rsub|<text|sol>>*H-\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>
  </equation>

  We see that <math|L<B>> is equal to the difference between the molar
  differential enthalpies of solution at the molality of interest and at
  infinite dilution.

  For a solution of a given molality, <math|L<A>> and <math|L<B>> can be
  evaluated from calorimetric measurements of <math|<Del>H<sol>> by various
  methods. Three general methods are as follows.<footnote|The descriptions
  refer to graphical plots with smoothed curves drawn through experimental
  points. A plot can be replaced by an algebraic function (e.g., a power
  series) fitted to the points, and slopes and intercepts can then be
  evaluated by numerical methods. >

  <\itemize>
    <item><math|L<A>> and <math|L<B>> can be evaluated by the variant of the
    method of intercepts described on page <pageref|intercepts variant>. The
    molar integral enthalpy of mixing, <math|<Del>H<m><mix>=<Del>H<sol>/<around|(|n<A>+n<B>|)>>,
    is plotted versus <math|x<B>>. The tangent to the curve at a given value
    of <math|x<B>> has intercepts <math|L<A>> at <math|x<B|=>0> and
    <math|H<B>-H<B><rsup|\<ast\>>=\<Delta\><rsub|<text|sol>>*H> at
    <math|x<B|=>1>, where the values of <math|L<A>> and
    <math|\<Delta\><rsub|<text|sol>>*H> are for the solution of composition
    <math|x<B>>. The tangent to the curve at <math|x<B|=>0> has intercept
    <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>> at <math|x<B|=>1>.
    <math|L<B>> is equal to the difference of these values of
    <math|\<Delta\><rsub|<text|sol>>*H> and
    <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>> (Eq.
    <reference|L_B=Del(sol)H-Del(sol)H^infty>).

    <item>Values of <math|<Del>H<sol>> for a constant amount of solvent can
    be plotted as a function of <math|\<xi\><rsub|<text|sol>>>, as in Fig.
    <reference|fig:11-Na acetate>. The slope of the tangent to the curve at
    any point on the curve is equal to <math|\<Delta\><rsub|<text|sol>>*H>
    for the molality <math|m<B>> at that point, and the initial slope at
    <math|\<xi\><rsub|<text|sol>>=0> is equal to
    <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>>. <math|L<B>> at
    molality <math|m<B>> is equal to the difference of these two values, and
    <math|L<A>> can be calculated from Eq. <reference|L_A=M_A*mB[]>.

    <item>A third method for the evaluation of <math|L<A>> and <math|L<B>> is
    especially useful for solutions of an electrolyte solute. This method
    takes advantage of the fact that a plot of <math|<Del>H<m><solmB>> versus
    <math|<sqrt|m<B>>> has a finite limiting slope at <math|<sqrt|m<B>>=0>
    whose value for an electrolyte can be predicted from the Debye\UH�ckel
    limiting law, providing a useful guide for the extrapolation of
    <math|<Del>H<m><solmB>> to its limiting value
    <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>>. The remainder of
    this section describes this third method.
  </itemize>

  The third method assumes we measure the integral enthalpy of solution
  <math|<Del>H<sol>> for varying amounts <math|\<xi\><rsub|<text|sol>>> of
  solute transferred at constant <math|T> and <math|p> from a pure solute
  phase to a fixed amount of solvent. From Eq. <reference|del(sol)Hm=d
  del(sol)H/d xi(sol)>, the molar differential enthalpy of solution is given
  by <math|\<Delta\><rsub|<text|sol>>*H=<dif><Del>H<sol>/<dif>\<xi\><rsub|<text|sol>>>
  when <math|n<A>> is held constant. We make the substitution
  <math|<Del>H<sol>=\<xi\><rsub|<text|sol>><Del>H<m><solmB>> and take the
  derivative of the expression with respect to
  <math|\<xi\><rsub|<text|sol>>>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\><rsub|<text|sol>>*H>|<cell|=>|<cell|<frac|<dif><around*|[|\<xi\><rsub|<text|sol>><Del>H<m><solmB>|]>|<dif>\<xi\><rsub|<text|sol>>>>>|<row|<cell|>|<cell|=>|<cell|<Del>H<m><solmB>+\<xi\><rsub|<text|sol>><frac|<dif><Del>H<m><solmB>|<dif>\<xi\><rsub|<text|sol>>><eq-number>>>>>
  </eqnarray*>

  At constant <math|n<A>>, <math|m<B>> is proportional to
  <math|\<xi\><rsub|<text|sol>>>, so that
  <math|<dif>\<xi\><rsub|<text|sol>>/\<xi\><rsub|<text|sol>>> can be replaced
  by <math|<dif>m<B>/m<B>>. When we combine the resulting expression for
  <math|\<Delta\><rsub|<text|sol>>*H> with Eq.
  <reference|L_B=Del(sol)H-Del(sol)H^infty>, we get the following expression
  for the relative partial molar enthalpy of the solute:

  <\equation>
    <label|L(B)=..>L<B>=<Del>H<m><solmB>+m<B><frac|<dif><Del>H<m><solmB>|<dif>m<B>>-\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>
  </equation>

  It is convenient to define the quantity

  <\equation>
    <label|Phi(L) defn><varPhi><rsub|L><defn><Del>H<m><solmB>-\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>
  </equation>

  known as the <index|Relative apparent molar enthalpy of a
  solute><subindex|Enthalpy|relative apparent, of a solute><em|relative
  apparent molar enthalpy of the solute>. Because
  <math|\<Delta\><rsub|<text|sol>>*H<rsup|\<infty\>>> is independent of
  <math|m<B>>, the derivative <math|<dif><varPhi><rsub|L>/<dif>m<B>> is equal
  to <math|<dif><Del>H<m><solmB>/<dif>m<B>>. We can therefore write Eq.
  <reference|L(B)=..> in the compact form

  <equation-cov2|<label|L(B)=...>L<B>=<varPhi><rsub|L>+m<B><frac|<dif><varPhi><rsub|L>|<dif>m<B>>|(constant
  <math|T> and <math|p>)>

  Equation <reference|L(B)=...> allows us to evaluate <math|L<B>> at any
  molality from the dependence of <math|<varPhi><rsub|L>> on <math|m<B>>,
  with <math|<varPhi><rsub|L>> obtained from experimental molar integral
  enthalpies of solution according to Eq. <reference|Phi(L) defn>.

  Once <math|<varPhi><rsub|L>> and <math|L<B>> have been evaluated for a
  given molality, it is a simple matter to calculate <math|L<A>> at that
  molality. By combining Eqs. <reference|L_A=M_A*mB[]> and <reference|Phi(L)
  defn>, we obtain the relation

  <\equation>
    <label|L_A=M_A mB(Phi_L-L_B)>L<A>=M<A>m<B><around|(|<varPhi><rsub|L>-L<B>|)>
  </equation>

  For an electrolyte solute, a plot of <math|<Del>H<m><solmB>> versus
  <math|m<B>> has a limiting slope of <math|+\<infty\>> at <math|m<B|=>0>,
  whereas the limiting slope of <math|<Del>H<m><solmB>> versus
  <math|<sqrt|m<B>>> is finite and can be predicted from the
  <subindex|Debye\UH�ckel|limiting law>Debye\UH�ckel limiting law.
  Accordingly, a satisfactory procedure is to plot <math|<Del>H<m><solmB>>
  versus <math|<sqrt|m<B>>>, perform a linear extrapolation of the
  experimental points to <math|<sqrt|m<B>>=0>, and then shift the origin to
  the extrapolated intercept. The result is a plot of <math|<varPhi><rsub|L>>
  versus <math|<sqrt|m<B>>>. An example for aqueous NaCl solutions is shown
  in Fig. <reference|fig:11-NaCl>(a) on page
  <pageref|fig:11-NaCl>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/NaCl.eps|373pt|163pt||>>
        <label|fig:11-NaCl>Thermal properties of aqueous NaCl at <math|25.00
        <degC>>.

        <\enumerate-alpha>
          <item>Left axis: molar integral enthalpy of solution to produce
          solution of molality <math|m<B>>.<note-ref|+1BMXlf48J76g2i> The
          dashed line has a slope equal to the theoretical limiting value of
          the slope of the curve. Right axis: relative apparent molar
          enthalpy of the solute.

          <item>Relative partial molar enthalpy of the solute as a function
          of molality.<note-ref|+1BMXlf48J76g2j>
        </enumerate-alpha>

        \;

        <note-inline||+1BMXlf48J76g2i>Calculated from molar enthalpy of
        formation values in Ref. <cite|wagman-82>, p. 2-301.

        <note-inline||+1BMXlf48J76g2j>Based on data in Ref. <cite|parker-65>,
        Table X.
      </big-figure>
    </framed>
  </float>

  We can also evaluate <math|<varPhi><rsub|L>> from experimental enthalpies
  of dilution. From Eqs. <reference|DelH(sol,mB'')=.+.> and <reference|Phi(L)
  defn>, we obtain the relation

  <\equation>
    <label|Phi(L)(mB'')-Phi(L)(mB')=><varPhi><rsub|L><around|(|m<B><rprime|''>|)>-<varPhi><rsub|L><around|(|m<B><rprime|'>|)>=<Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>
  </equation>

  We can measure the enthalpy changes for diluting a solution of initial
  molality <math|m<B><rprime|'>> to various molalities
  <math|m<B><rprime|''>>, plot the values of
  <math|<Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>>
  versus <math|<sqrt|m<B>>>, extrapolate the curve to <math|<sqrt|m<B>>=0>,
  and shift the origin to the extrapolated intercept, resulting in a plot of
  <math|<varPhi><rsub|L>> versus <math|<sqrt|m<B>>>.

  In order to be able to use Eq. <reference|L(B)=...>, we need to relate the
  derivative <math|<dif><varPhi><rsub|L>/<dif>m<B>> to the slope of the curve
  of <math|<varPhi><rsub|L>> versus <math|<sqrt|m<B>>>. We write

  <\equation>
    <dif><sqrt|m<B>>=<frac|1|2*<sqrt|m<B>>><dif>m<B><space|2em><dif>m<B>=2*<sqrt|m<B>><space|0.17em><space|0.17em><dif><sqrt|m<B>>
  </equation>

  Substituting this expression for <math|<dif>m<B>> into Eq.
  <reference|L(B)=...>, we obtain the following operational equation for
  evaluating <math|L<B>> from the plot of <math|<varPhi><rsub|L>> versus
  <math|<sqrt|m<B>>>:

  <equation-cov2|<label|L(B)=....>L<B>=<varPhi><rsub|L>+<frac|<sqrt|m<B>>|2>*<space|0.17em><space|0.17em><frac|<dif><varPhi><rsub|L>|<dif><sqrt|m<B>>>|(constant
  <math|T> and <math|p>)>

  The value of <math|<varPhi><rsub|L>> goes to zero at infinite dilution.
  When the solute is an electrolyte, the dependence of
  <math|<varPhi><rsub|L>> on <math|m<B>> in solutions dilute enough for the
  Debye\UH�ckel limiting law to apply is given by

  <equation-cov2|<label|Phi(L) dil sln><varPhi><rsub|L>=C<rsub|<varPhi><rsub|L>>*<sqrt|m<B>>|(very
  dilute solution)>

  For aqueous solutions of a 1:1 electrolyte at <math|25 <degC>>, the
  coefficient <math|C<rsub|<varPhi><rsub|L>>> has the value<footnote|The fact
  that <math|C<rsub|<varPhi><rsub|L>>> is positive means, according to Eq.
  <reference|Phi(L)(mB'')-Phi(L)(mB')=>, that dilution of a very dilute
  electrolyte solution is an exothermic process.>

  <\equation>
    <label|C=>C<rsub|<varPhi><rsub|L>>=1.988<timesten|3>
    <text|J>\<cdot\><text|kg><rsup|<frac*|1|2>>\<cdot\><text|mol><rsup|-<frac*|3|2>>
  </equation>

  <math|C<rsub|<varPhi><rsub|L>>> is equal to the limiting slope of
  <math|<varPhi><rsub|L>> versus <math|<sqrt|m<B>>>, of
  <math|<Del>H<m><solmB>> versus <math|<sqrt|m<B>>>, and of
  <math|<Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>>
  versus <math|<sqrt|m<rprime|'><B>>>. The value given by Eq. <reference|C=>
  can be used for extrapolation of measurements at <math|25 <degC>> and low
  molality to infinite dilution.

  <\quote-env>
    Equation <reference|Phi(L) dil sln> can be derived as follows. For
    simplicity, we assume the pressure is the standard pressure <math|p<st>>.
    At this pressure <math|H<B><rsup|\<infty\>>> is the same as
    <math|H<B><st>>, and Eq. <reference|L(B) defn> becomes
    <math|L<B>=H<B>-H<B><st>>. From Eqs. <reference|d(mu_i/T)/dT=-H_i/T^2>
    and <reference|d(mu_io/T)/dT=-H_io/T^2> in the next chapter, we can write
    the relations

    <\equation>
      H<B>=-T<rsup|2><bPd|<around|(|\<mu\><B>/T|)>|T|p,<allni>><space|2em>H<B><st>=-T<rsup|2>*<frac|<dif><around|(|\<mu\><mbB><st>/T|)>|<dif>T>
    </equation>

    Subtracting the second of these relations from the first, we obtain

    <\equation>
      H<B>-H<B><st>=-T<rsup|2><bPd|<around|(|\<mu\><B>-\<mu\><mbB><st>|)>/T|T|p,<allni>>
    </equation>

    The solute activity on a molality basis, <math|a<mbB>>, is defined by
    <math|\<mu\><B>-\<mu\><mbB><st>=R*T*ln a<mbB>>. The activity of an
    electrolyte solute at the standard pressure, from Eq.
    <reference|a(mB),general>, is given by
    <math|a<mbB>=<around|(|\<nu\><rsub|+><rsup|\<nu\><rsub|+>>*\<nu\><rsub|-><rsup|\<nu\><rsub|->>|)><g><rsub|\<pm\>><rsup|\<nu\>><around|(|m<B>/m<st>|)><rsup|\<nu\>>>.
    Accordingly, the relative partial molar enthalpy of the solute is related
    to the mean ionic activity coefficient by

    <\equation>
      <label|L(B)=-RT^2..>L<B>=-R*T<rsup|2>*\<nu\><Pd|ln
      <g><rsub|\<pm\>>|T|<space|-0.17em><space|-0.17em>p,<allni>>
    </equation>

    We assume the solution is sufficiently dilute for the mean ionic activity
    coefficient to be adequately described by the
    <subindex|Debye\UH�ckel|limiting law>Debye\UH�ckel limiting law, Eq.
    <reference|DH limiting law>: <math|ln
    <g><rsub|\<pm\>>=-A<rsub|<text|D>\<nocomma\><text|H>>*<around*|\||z<rsub|+>*z<rsub|->|\|>*<sqrt|I<rsub|m>>>,
    where <math|A<rsub|<text|D>\<nocomma\><text|H>>> is a
    temperature-dependent quantity defined on page <pageref|A(DH) defn>. Then
    Eq. <reference|L(B)=-RT^2..> becomes

    <equation-cov2|<label|L(B)=RT^2...>L<B>=R*T<rsup|2>*\<nu\>*<around*|\||z<rsub|+>*z<rsub|->|\|><sqrt|I<rsub|m>><Pd|A<rsub|<text|D>\<nocomma\><text|H>>|T|<space|-0.17em><space|-0.17em>p,<allni>>|(very
    dilute solution)>

    Substitution of the expression given by Eq.
    <vpageref|I(m)=(1/2)nu\|x+z-\|mB> for <math|I<rsub|m>> in a solution of a
    single completely-dissociated electrolyte converts Eq.
    <reference|L(B)=RT^2...> to

    <\eqnarray*>
      <tformat|<table|<row|<cell|L<B>>|<cell|=>|<cell|<around*|[|<frac|R*T<rsup|2>|<sqrt|2>>*<Pd|\<rho\><A><rsup|\<ast\>>|T|p,<allni>>*<around*|(|v*<around*|\||z<rsub|+>*z<rsub|->|\|><rsup|<frac*|3|2>>|)>|]>*<sqrt|m<B>>>>|<row|<cell|>|<cell|=>|<cell|C<rsub|L<B>><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<text|<around|(|very
      dilute solution|)>>>>>>><label|L(B)=C..>>>>>
    </eqnarray*>

    The coefficient <math|C<rsub|L<B>>> (the quantity in brackets) depends on
    <math|T>, the kind of solvent, and the ion charges and number of ions per
    solute formula unit, but not on the solute molality.

    Let <math|C<rsub|<varPhi><rsub|L>>> represent the limiting slope of
    <math|<varPhi><rsub|L>> versus <math|<sqrt|m<B>>>. In a very dilute
    solution we have <math|<varPhi><rsub|L>=C<rsub|<varPhi><rsub|L>>*<sqrt|m<B>>>,
    and Eq. <reference|L(B)=....> becomes

    <\equation>
      <label|L(B)=.=.>L<B>=<varPhi><rsub|L>+<frac|<sqrt|m<B>>|2>*<space|0.17em><space|0.17em><frac|<dif><varPhi><rsub|L>|<dif><sqrt|m<B>>>=C<rsub|<varPhi><rsub|L>>*<sqrt|m<B>>+<frac|<sqrt|m<B>>|2>*<space|0.17em><space|0.17em>C<rsub|<varPhi><rsub|L>>
    </equation>

    By equating this expression for <math|L<B>> with the one given by Eq.
    <reference|L(B)=C..> and solving for <math|C<rsub|<varPhi><rsub|L>>>, we
    obtain <math|C<rsub|<varPhi><rsub|L>>=<around|(|2/3|)>*C<rsub|L<B>>> and
    <math|<varPhi><rsub|L>=<around|(|2/3|)>*C<rsub|L<B>>*<sqrt|m<B>>>.
  </quote-env>

  <section|Reaction Calorimetry><label|11-rxn calorimetry><label|c11 sec rc>

  <index-complex|<tuple|calorimetry|reaction>||c11 sec rc
  idx1|<tuple|Calorimetry|reaction>>Reaction calorimetry is used to evaluate
  the molar integral reaction enthalpy <math|<Del>H<m><rxn>> of a reaction or
  other chemical process at constant temperature and pressure. The
  measurement actually made, however, is a temperature change.

  Sections <reference|11-constant-pressure calorimeter> and
  <reference|11-bomb calorimeter> will describe two common types of
  calorimeters designed for reactions taking place at either constant
  pressure or constant volume. The constant-pressure type is usually called a
  <em|reaction calorimeter>, and the constant-volume type is known as a
  <subindex|Calorimeter|bomb><subindex|Calorimeter|combustion><index|Bomb
  calorimeter><em|bomb calorimeter> or <em|combustion calorimeter>.

  In either type of calorimeter, the chemical process takes place in a
  reaction vessel surrounded by an outer jacket. The jacket may be of either
  the adiabatic type or the isothermal-jacket type described in Sec.
  <reference|7-ht cap measurement> in connection with heat capacity
  measurements. A temperature-measuring device is immersed either in the
  vessel or in a phase in thermal contact with it. The measured temperature
  change is caused by the chemical process, instead of by electrical work as
  in the determination of heat capacity. One important way in which these
  calorimeters differ from ones used for heat capacity measurements is that
  work is kept deliberately small, in order to minimize changes of internal
  energy and enthalpy during the experimental process.

  <subsection|The constant-pressure reaction
  calorimeter><label|11-constant-pressure calorimeter><label|c11
  sec-rc-constant-pressure>

  <index-complex|<tuple|calorimeter|reaction>||c11 sec-rc-constant-pressure
  idx1|<tuple|Calorimeter|reaction>>The contents of a constant-pressure
  calorimeter are usually open to the atmosphere, so this type of calorimeter
  is unsuitable for processes involving gases. It is, however, a convenient
  apparatus in which to study a liquid-phase chemical reaction, the
  dissolution of a solid or liquid solute in a liquid solvent, or the
  dilution of a solution with solvent.

  The process is initiated in the calorimeter by allowing the reactants to
  come into contact. The temperature in the reaction vessel is measured over
  a period of time starting before the process initiation and ending after
  the advancement has reached a final value with no further change.

  The heating or cooling curve (temperature as a function of time) is
  observed over a period of time that includes the period during which the
  advancement <math|\<xi\>> changes. For an exothermic reaction occurring in
  an <subindex|Adiabatic|calorimeter><subindex|Calorimeter|adiabatic>adiabatic
  calorimeter, the heating curve may resemble that shown in Fig.
  \ <vpageref|fig:7-ad heating curve>, and the heating curve in an
  <subindex|Calorimeter|isothermal-jacket><index|Isoperibol
  calorimeter><subindex|Calorimeter|isoperibol>isothermal-jacket calorimeter
  may resemble that shown in Fig. <vpageref|fig:7-heating curve>. Two points
  are designated on the heating or cooling curve: one at temperature
  <math|T<rsub|1>>, before the reaction is initiated, and the other at
  <math|T<rsub|2>>, after <math|\<xi\>> has reached its final value. These
  points are indicated by open circles in Figs. <reference|fig:7-ad heating
  curve> and <reference|fig:7-heating curve>.

  Figure <vpageref|fig:11-calorimeter paths><\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/paths.eps|153pt|92pt||>>
        <label|fig:11-calorimeter paths>Enthalpy changes for paths at
        constant pressure (schematic). R denotes reactants and P denotes
        products.
      </big-figure>
    </framed>
  </float> depicts three paths at constant pressure. The enthalpy change of
  the experimental process, in which reactants at temperature
  <math|T<rsub|1>> change to products at temperature <math|T<rsub|2>>, is
  denoted <math|<Del>H<expt>>.

  The value of <math|<Del>H<expt>> at constant pressure would be zero if the
  process were perfectly adiabatic and the only work were expansion work, but
  this is rarely the case. There may be unavoidable work from stirring and
  from electrical temperature measurement. We can evaluate
  <math|<Del>H<expt>> by one of the methods described in Sec. <reference|7-ht
  cap measurement>. For an <subindex|Adiabatic|calorimeter><subindex|Calorimeter|adiabatic>adiabatic
  calorimeter, the appropriate expression is
  <math|<Del>H<expt>=\<epsilon\>*r*<around|(|t<rsub|2>-t<rsub|1>|)>> (Eq.
  <vpageref|delH=w(el)+er(t2-t1)> with <math|w<el>> set equal to zero), where
  <math|\<epsilon\>> is the <index|Energy equivalent>energy equivalent of the
  calorimeter, <math|r> is the slope of the heating curve when no reaction is
  occurring, and <math|t<rsub|1>> and <math|t<rsub|2>> are the times at
  temperatures <math|T<rsub|1>> and <math|T<rsub|2>>. For an
  <subindex|Calorimeter|isothermal-jacket><index|Isoperibol
  calorimeter><subindex|Calorimeter|isoperibol>isothermal-jacket calorimeter,
  we evaluate <math|<Del>H<expt>> using Eq. <vpageref|delH=-k
  int((T-T(inf))dt+w(el)> with <math|w<el>> set equal to zero.

  The enthalpy change we wish to find is the reaction enthalpy
  <math|<Del>H<around|(|<text|rxn>,<space|0.17em>T<rsub|1>|)>>, which is the
  change for the same advancement of the reaction at <em|constant>
  temperature <math|T<rsub|1>>. The paths labeled <math|<Del>H<expt>> and
  <math|<Del>H<around|(|<text|rxn>,<space|0.17em>T<rsub|1>|)>> in the figure
  have the same initial state and different final states. The path connecting
  these two final states is for a change of the temperature from
  <math|T<rsub|1>> to <math|T<rsub|2>> with <math|\<xi\>> fixed at its final
  value; the enthalpy change for this path is denoted
  <math|<Del>H<around|(|<text|P>|)>>.<footnote|The symbol P refers to the
  final equilibrium state in which the reaction vessel contains products of
  the reaction and any excess reactants.> The value of
  <math|<Del>H<around|(|<text|P>|)>> can be calculated from

  <\equation>
    <Del>H<around|(|<text|P>|)>=\<epsilon\><rsub|<text|P>>*<around|(|T<rsub|2>-T<rsub|1>|)>
  </equation>

  where <math|\<epsilon\><rsub|<text|P>>> is the <index|Energy
  equivalent>energy equivalent (the average heat capacity of the calorimeter)
  when the calorimeter contains the products. To measure
  <math|\<epsilon\><rsub|<text|P>>>, we can carry out a second experiment
  involving work with an electric heater included in the calorimeter, similar
  to the methods described in Sec. <reference|7-ht cap measurement>.

  Since the difference of enthalpy between two states is independent of the
  path, we can write <math|<Del>H<expt>=<Del>H<around|(|<text|rxn>,<space|0.17em>T<rsub|1>|)>+\<epsilon\><rsub|<text|P>>*<around|(|T<rsub|2>-T<rsub|1>|)>>,
  or

  <\equation>
    <label|del(B)H=del(A)H-del(B)H><Del>H<around|(|<text|rxn>,<space|0.17em>T<rsub|1>|)>=-\<epsilon\><rsub|<text|P>>*<around|(|T<rsub|2>-T<rsub|1>|)>+<Del>H<expt>
  </equation>

  The molar integral reaction enthalpy at temperature <math|T<rsub|1>> is the
  reaction enthalpy divided by <math|<Del>\<xi\>>, the advancement during the
  experimental process:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\>*H<rsub|<text|m>><rxn>>|<cell|=>|<cell|\<Delta\>*H<around*|(|<text|rxn>,T<rsub|1>|)>/\<Delta\>*\<xi\>>>|<row|<cell|>|<cell|=>|<cell|<frac|-\<epsilon\><rsub|<text|P>>*<around*|(|T<rsub|2>-T<rsub|1>|)>+\<Delta\>*H<expt>|\<Delta\>*\<xi\>><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<text|(constant-pressure>>>|<row|<cell|<text|calorimeter)>>>>>><label|del(r)Hm(int)=>>>>>
  </eqnarray*>

  Note that <math|<Del>H<expt>> is small, so that <math|<Del>H<m><rxn>> is
  approximately equal to <math|-\<epsilon\><rsub|<text|P>>*<around|(|T<rsub|2>-T<rsub|1>|)>/<Del>\<xi\>>.
  If <math|T<rsub|2>> is greater than <math|T<rsub|1>> (the process is
  exothermic), then <math|<Del>H<m><rxn>> is <em|negative>, reflecting the
  fact that after the reaction takes place in the calorimeter, heat would
  have to leave the system in order for the temperature to return to its
  initial value. If <math|T<rsub|2>> is less than <math|T<rsub|1>> (the
  process is endothermic), <math|<Del>H<m><rxn>> is <em|positive>.

  Most reactions cause a change in the composition of one or more phases, in
  which case <math|<Del>H<m><rxn>> is not the same as the molar differential
  reaction enthalpy, <math|\<Delta\><rsub|<text|r>>*H=<pd|H|\<xi\>|T,p>>,
  unless the phase or phases can be treated as ideal mixtures (see Sec.
  <reference|11-molar rxn quantities in general>). Corrections, usually
  small, are needed to obtain the standard molar reaction enthalpy
  <math|\<Delta\><rsub|<text|r>>*H<st>> from
  <math|<Del>H<m><rxn>>.<index-complex|<tuple|calorimeter|reaction>||c11
  sec-rc-constant-pressure idx1|<tuple|Calorimeter|reaction>>

  <subsection|The bomb calorimeter><label|11-bomb calorimeter><label|c11 sec
  rc-bomb>

  <index-complex|<tuple|calorimeter|bomb>||c11 sec rc-bomb
  idx1|<tuple|Calorimeter|bomb>><index-complex|<tuple|bomb calorimeter>||c11
  sec rc-bomb idx2|<tuple|Bomb calorimeter>><subindex|Calorimetry|bomb>A bomb
  calorimeter typically is used to carry out the complete combustion of a
  solid or liquid substance in the presence of excess oxygen. The combustion
  reaction is initiated with electrical ignition. In addition to the main
  combustion reaction, there may be unavoidable side reactions, such as the
  formation of nitrogen oxides if N<rsub|<math|2>> is not purged from the gas
  phase. Sometimes auxiliary reactions are deliberately carried out to
  complete or moderate the main reaction.

  From the measured heating curve and known properties of the calorimeter,
  reactants, and products, it is possible to evaluate the standard molar
  enthalpy of combustion, <math|\<Delta\><rsub|<text|c>>*H<st>>, of the
  substance of interest at a particular temperature called the reference
  temperature, <math|T<rsub|<text|ref>>>. (<math|T<rsub|<text|ref>>> is often
  chosen to be <math|298.15<K>>, which is <math|25.00 <degC>>.) With careful
  work, using temperature measurements with a resolution of
  <math|1<timesten|-4><K>> or better and detailed corrections, the precision
  of <math|\<Delta\><rsub|<text|c>>*H<st>> can be of the order of <math|0.01>
  percent.

  Bomb calorimetry is the principal means by which
  <index-complex|<tuple|enthalpy|combustion>|||<tuple|Enthalpy|of combustion,
  standard molar>>standard molar enthalpies of combustion of individual
  elements and of compounds of these elements are evaluated. From these
  values, using <index|Hess's law>Hess's law, we can calculate the standard
  molar enthalpies of <em|formation> of the compounds as described in Sec.
  <reference|11-st molar enthalpy of formation>. From the formation values of
  only a few compounds, the standard molar reaction enthalpies of innumerable
  reactions can be calculated with Hess's law (Eq. <vpageref|Hess's law>).

  Because of their importance, the experimental procedure and the analysis of
  the data it provides will now be described in some detail. A comprehensive
  problem (Prob. <reference|prb:11-hexane combustion>) based on this material
  is included at the end of the chapter.

  There are five main steps in the procedure of evaluating a standard molar
  enthalpy of combustion:

  <\enumerate>
    <item>The combustion reaction, and any side reactions and auxiliary
    reactions, are carried out in the calorimeter, and the course of the
    resulting temperature change is observed.

    <item>The experimental data are used to determine the value of
    <math|<Del>U<around|(|<text|IBP>,T<rsub|2>|)>>, the internal energy
    change of the isothermal bomb process at the final temperature of the
    reaction. The <subindex|Isothermal|bomb process><newterm|isothermal bomb
    process> is the idealized process that would have occurred if the
    reaction or reactions had taken place in the calorimeter at constant
    temperature.

    <item>The internal energy change of the isothermal bomb process is
    corrected to yield <math|<Del>U<around|(|<text|IBP>,T<rsub|<text|ref>>|)>>,
    the value at the reference temperature of interest.

    <item><index-complex|<tuple|enthalpy|combustion>|||<tuple|Enthalpy|of
    combustion, standard molar>>The standard molar internal energy of
    combustion, <math|\<Delta\><rsub|<text|c>>*U<st><around|(|T<rsub|<text|ref>>|)>>,
    is calculated. This calculation is called <index|Reduction to standard
    states><newterm|reduction to standard states>.

    <item>The standard molar enthalpy of combustion,
    <math|\<Delta\><rsub|<text|c>>*H<st><around|(|T<rsub|<text|ref>>|)>>, is
    calculated.
  </enumerate>

  These five steps are described below.

  <subsubsection*|Experimental><label|c11 sec rc-bomb-exp>

  The common form of combustion bomb calorimeter shown in Fig.
  <vpageref|fig:11-bomb calorimeter><\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/BOMB.eps|313pt|202pt||>>
        <label|fig:11-bomb calorimeter>Section view of a bomb calorimeter.
      </big-figure>
    </framed>
  </float> consists of a thick-walled cylindrical metal vessel to contain the
  reactants of the combustion reaction. It is called a \Pbomb\Q because it is
  designed to withstand high pressure. The bomb can be sealed with a
  gas-tight screw cap. During the reaction, the sealed bomb vessel is
  immersed in water in the calorimeter, which is surrounded by a jacket.
  Conceptually, we take the <em|system> to be everything inside the jacket,
  including the calorimeter walls, water, bomb vessel, and contents of the
  bomb vessel.

  To prepare the calorimeter for a combustion experiment, a weighed sample of
  the substance to be combusted is placed in a metal sample holder. The
  calculations are simplified if we can assume all of the sample is initially
  in a single phase. Thus, a volatile liquid is usually encapsulated in a
  bulb of thin glass (which shatters during the ignition) or confined in the
  sample holder by cellulose tape of known combustion properties. If one of
  the combustion products is H<rsub|<math|2>>O, a small known mass of liquid
  water is placed in the bottom of the bomb vessel to saturate the gas space
  of the bomb vessel with H<rsub|<math|2>>O. The sample holder and ignition
  wires are lowered into the bomb vessel, the cap is screwed on, and oxygen
  gas is admitted through a valve in the cap to a total pressure of about
  <math|30<br>>.

  To complete the setup, the sealed bomb vessel is immersed in a known mass
  of water in the calorimeter. A precision thermometer and a stirrer are also
  immersed in the water. With the stirrer turned on, the temperature is
  monitored until it is found to change at a slow, practically-constant rate.
  This drift is due to heat transfer through the jacket, mechanical stirring
  work, and the electrical work needed to measure the temperature. A
  particular time is chosen as the initial time <math|t<rsub|1>>. The
  measured temperature at this time is <math|T<rsub|1>>, assumed to be
  practically uniform throughout the system.

  At or soon after time <math|t<rsub|1>>, the <index|Ignition
  circuit><subindex|Circuit|ignition>ignition circuit is closed to initiate
  the combustion reaction in the bomb vessel. If the reaction is exothermic,
  the measured temperature rapidly increases over the course of several
  minutes. For a while the temperature in the system is far from uniform, as
  energy is transferred by heat through the walls of the bomb vessel walls to
  the water outside.

  When the measured temperature is again observed to change at a slow and
  practically constant rate, the reaction is assumed to be complete and the
  temperature is assumed once more to be uniform. A second time is now
  designated as the final time <math|t<rsub|2>>, with final temperature
  <math|T<rsub|2>>. For best accuracy, conditions are arranged so that
  <math|T<rsub|2>> is close to the desired reference temperature
  <math|T<rsub|<text|ref>>>.

  Because the jacket is not gas tight, the pressure of the water outside the
  bomb vessel stays constant at the pressure of the atmosphere. Inside the
  bomb vessel, the changes in temperature and composition take place at
  essentially constant volume, so the pressure inside the vessel is <em|not>
  constant. The volume change of the entire system during the process is
  negligible.

  <subsubsection*|The isothermal bomb process><label|c11 sec rc-bomb-ibp>

  <index-complex|<tuple|isothermal|bomb process>||c11 sec rc-bomb-ibp
  idx1|<tuple|Isothermal|bomb process>>The relations derived here parallel
  those of Sec. <reference|11-constant-pressure calorimeter> for a
  constant-pressure calorimeter. The three paths depicted in Fig.
  <vpageref|fig:11-bomb paths><\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/BOMBPATH.eps|128pt|78pt||>>
        <label|fig:11-bomb paths>Internal energy changes for paths at
        constant volume in a bomb calorimeter (schematic). R denotes
        reactants and P denotes products.
      </big-figure>
    </framed>
  </float>

  are similar to those in Fig. <vpageref|fig:11-calorimeter paths>, except
  that instead of being at constant pressure they are at constant volume. We
  shall assume the combustion reaction is exothermic, with <math|T<rsub|2>>
  being greater than <math|T<rsub|1>>.

  The internal energy change of the experimental process that actually occurs
  in the calorimeter between times <math|t<rsub|1>> and <math|t<rsub|2>> is
  denoted <math|<Del>U<expt>> in the figure. Conceptually, the overall change
  of state during this process would be duplicated by a path in which the
  temperature of the system with the reactants present increases from
  <math|T<rsub|1>> to <math|T<rsub|2>>,<footnote|When one investigates a
  combustion reaction, the path in which temperature changes without reaction
  is best taken with reactants rather than products present because the
  reactants are more easily characterized.> followed by the isothermal bomb
  process at temperature <math|T<rsub|2>>. In the figure these paths are
  labeled with the internal energy changes <math|<Del>U<around|(|<text|R>|)>>
  and <math|<Del>U<around|(|<text|IBP>,T<rsub|2>|)>>, and we can write

  <\equation>
    <label|DelU(expt)=><Del>U<expt>=<Del>U<around|(|<text|R>|)>+<Del>U<around|(|<text|IBP>,T<rsub|2>|)>
  </equation>

  To evaluate <math|<Del>U<around|(|<text|R>|)>>, we can use the
  <index|Energy equivalent>energy equivalent
  <math|\<epsilon\><rsub|<text|R>>> of the calorimeter with reactants present
  in the bomb vessel. <math|\<epsilon\><rsub|<text|R>>> is the average heat
  capacity of the system between <math|T<rsub|1>> and <math|T<rsub|2>>\Vthat
  is, the ratio <math|q/<around|(|T<rsub|2>-T<rsub|1>|)>>, where <math|q> is
  the heat that would be needed to change the temperature from
  <math|T<rsub|1>> to <math|T<rsub|2>>. From the first law, with expansion
  work assumed negligible, the internal energy change equals this heat,
  giving us the relation

  <\equation>
    <label|DelU(R)=><Del>U<around|(|<text|R>|)>=\<epsilon\><rsub|<text|R>>*<around|(|T<rsub|2>-T<rsub|1>|)>
  </equation>

  The initial and final states of the path are assumed to be equilibrium
  states, and there may be some transfer of reactants or H<rsub|<math|2>>O
  from one phase to another within the bomb vessel during the heating
  process.

  The value of <math|\<epsilon\><rsub|<text|R>>> is obtained in a separate
  calibration experiment. The calibration is usually carried out with the
  combustion of a reference substance, such as benzoic acid, whose internal
  energy of combustion under controlled conditions is precisely known from
  standardization based on electrical work. If the bomb vessel is immersed in
  the same mass of water in both experiments and other conditions are
  similar, the difference in the values of <math|\<epsilon\><rsub|<text|R>>>
  in the two experiments is equal to the known difference in the heat
  capacities of the initial contents (reactants, water, etc.) of the bomb
  vessel in the two experiments.

  The internal energy change we wish to find is
  <math|<Del>U<around|(|<text|IBP>,T<rsub|2>|)>>, that of the isothermal bomb
  process in which reactants change to products at temperature
  <math|T<rsub|2>>, accompanied perhaps by some further transfer of
  substances between phases. From Eqs. <reference|DelU(expt)=> and
  <reference|DelU(R)=>, we obtain

  <\equation>
    <label|DelU(IBP)=><Del>U<around|(|<text|IBP>,T<rsub|2>|)>=-\<epsilon\>*<around|(|T<rsub|2>-T<rsub|1>|)>+<Del>U<expt>
  </equation>

  The value of <math|<Del>U<expt>> is small. To evaluate it, we must look in
  detail at the possible sources of energy transfer between the system and
  the surroundings during the experimental process. These sources are

  <\enumerate>
    <subindex|Electrical|work><subindex|Work|electrical>

    <item>electrical work <math|w<rsub|<text|ign>>> done on the system by the
    <index|Ignition circuit><subindex|Circuit|ignition>ignition circuit;

    <item>heat transfer, minimized but not eliminated by the jacket;

    <item>mechanical stirring work done on the system;

    <item>electrical work done on the system by an electrical thermometer.
  </enumerate>

  The ignition work occurs during only a short time interval at the beginning
  of the process, and its value is known. The effects of heat transfer,
  stirring work, and temperature measurement continue throughout the course
  of the experiment. With these considerations, Eq. <reference|DelU(IBP)=>
  becomes

  <\equation>
    <label|DelU(IBP)=-e(T2-T1)+w(ign)+DelU'><Del>U<around|(|<text|IBP>,T<rsub|2>|)>=-\<epsilon\>*<around|(|T<rsub|2>-T<rsub|1>|)>+w<rsub|<text|ign>>+<Del>U<rprime|'><expt>
  </equation>

  where <math|<Del>U<rprime|'><expt>> is the internal energy change due to
  heat, stirring, and temperature measurement. <math|<Del>U<rprime|'><expt>>
  can be evaluated from the <index|Energy equivalent>energy equivalent and
  the observed rates of temperature change at times <math|t<rsub|1>> and
  <math|t<rsub|2>>; the relevant relations for an isothermal jacket are Eq.
  <reference|delU=-k int((T-T(inf))dt+w(el)> (with <math|w<el>> set equal to
  zero) and Eq. <reference|k=[(r1-r2)/(T2-T1)]e>.<index-complex|<tuple|isothermal|bomb
  process>||c11 sec rc-bomb-ibp idx1|<tuple|Isothermal|bomb process>>

  <subsubsection*|Correction to the reference temperature><label|c11 sec
  rc-bomb-crt>

  The value of <math|<Del>U<around|(|<text|IBP>,T<rsub|2>|)>> evaluated from
  Eq. <reference|DelU(IBP)=-e(T2-T1)+w(ign)+DelU'> is the internal energy
  change of the isothermal bomb process at temperature <math|T<rsub|2>>. We
  need to correct this value to the desired reference temperature
  <math|T<rsub|<text|ref>>>. If <math|T<rsub|2>> and
  <math|T<rsub|<text|ref>>> are close in value, the correction is small and
  can be calculated with a modified version of the <index|Kirchhoff
  equation>Kirchhoff equation (Eq. <vpageref|Kirchhoff eq-2>):

  <\equation>
    <label|Del(IBP)U(Tref)=><Del>U<around|(|<text|IBP>,T<rsub|<text|ref>>|)>=<Del>U<around|(|<text|IBP>,T<rsub|2>|)>+<around|[|<space|0.17em>C<rsub|V><around|(|<text|P>|)>-C<rsub|V><around|(|<text|R>|)><space|0.17em>|]>*<around|(|T<rsub|<text|ref>>-T<rsub|2>|)>
  </equation>

  Here <math|C<rsub|V><around|(|<text|P>|)>> and
  <math|C<rsub|V><around|(|<text|R>|)>> are the heat capacities at constant
  volume of the contents of the bomb vessel with products and reactants,
  respectively, present.

  <subsubsection*|Reduction to standard states><label|c11 sec rc-bomb-rss>

  We want to obtain the value of <math|\<Delta\><rsub|<text|c>>*U<st><around|(|T<rsub|<text|ref>>|)>>,
  the molar internal energy change for the main combustion reaction at the
  reference temperature under standard-state conditions. Once we have this
  value, it is an easy matter to find the molar <em|enthalpy> change under
  standard-state conditions, our ultimate goal.

  Consider a hypothetical process with the following three isothermal steps
  carried out at the reference temperature <math|T<rsub|<text|ref>>>:

  <\enumerate>
    <label|3steps>

    <item>Each substance initially present in the bomb vessel changes from
    its standard state to the state it actually has at the start of the
    isothermal bomb process.

    <item>The isothermal bomb process takes place, including the main
    combustion reaction and any side reactions and auxiliary reactions.

    <item>Each substance present in the final state of the isothermal bomb
    process changes to its standard state.
  </enumerate>

  The net change is a decrease in the amount of each reactant in its standard
  state and an increase in the amount of each product in its standard state.
  The internal energy change of step 2 is
  <math|<Del>U<around|(|<text|IBP>,T<rsub|<text|ref>>|)>>, whose value is
  found from Eq. <reference|Del(IBP)U(Tref)=>. The internal energy changes of
  steps 1 and 3 are called <index|Washburn corrections><newterm|Washburn
  corrections>.<footnote|Ref. <cite|washburn-33>.>

  Thus, we calculate the standard internal energy change of the main
  combustion reaction at temperature <math|T<rsub|<text|ref>>> from

  <\equation>
    <label|DelUo=DelU(IBP)+...><Del>U<st><around|(|<text|cmb>,T<rsub|<text|ref>>|)>=<Del>U<around|(|<text|IBP>,T<rsub|<text|ref>>|)>+<around|(|<text|Washburn
    corrections>|)>-<big|sum><rsub|i><Del>\<xi\><rsub|i>\<Delta\><rsub|<text|r>>*U<st><around|(|i|)>
  </equation>

  where the sum over <math|i> is for side reactions and auxiliary reactions
  if present. Finally, we calculate the standard <em|molar> internal energy
  of combustion from

  <\equation>
    <label|DelUom=DelUo/xi>\<Delta\><rsub|<text|c>>*U<st><around|(|T<rsub|<text|ref>>|)>=<frac|<Del>U<st><around|(|<text|cmb>,T<rsub|<text|ref>>|)>|<Del>\<xi\><rsub|<text|c>>>
  </equation>

  where <math|<Del>\<xi\><rsub|<text|c>>> is the advancement of the main
  combustion reaction in the bomb vessel.

  <subsubsection*|Standard molar enthalpy change><label|c11 sec rc-bomb-smec>

  The quantity <math|\<Delta\><rsub|<text|c>>*U<st><around|(|T<rsub|<text|ref>>|)>>
  is the molar internal energy change for the main combustion reaction
  carried out at constant temperature <math|T<rsub|<text|ref>>> with each
  reactant and product in its standard state at pressure <math|p<st>>. From
  the relations <math|\<Delta\><rsub|<text|c>>*H=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*H<rsub|i>>
  (Eq. <reference|del(r)Xm=sum(nu_i)X_i>) and
  <math|H<rsub|i>=U<rsub|i>+p*V<rsub|i>> (from Eq. <reference|U_i=H_i-pV_i>),
  we get

  <\equation>
    \<Delta\><rsub|<text|c>>*H<st><around|(|T<rsub|<text|ref>>|)>=\<Delta\><rsub|<text|c>>*U<st><around|(|T<rsub|<text|ref>>|)>+p<st><big|sum><rsub|i>\<nu\><rsub|i>*V<rsub|i><st>
  </equation>

  Molar volumes of condensed phases are much smaller than those of gases, and
  to a good approximation we may write

  <\equation>
    \<Delta\><rsub|<text|c>>*H<st><around|(|T<rsub|<text|ref>>|)>=\<Delta\><rsub|<text|c>>*U<st><around|(|T<rsub|<text|ref>>|)>+p<st><big|sum><rsub|i>\<nu\><rsub|i><rsup|<text|g>>V<rsub|i><st><gas>
  </equation>

  where the sum includes only gaseous reactants and products of the main
  combustion reaction. Since a gas in its standard state is an ideal gas with
  molar volume equal to <math|R*T/p<st>>, the final relation is
  <index-complex|<tuple|enthalpy|combustion>|||<tuple|Enthalpy|of combustion,
  standard molar>>

  <\equation>
    <label|DelHom=DelUom+sum nu_i(g)RT>\<Delta\><rsub|<text|c>>*H<st><around|(|T<rsub|<text|ref>>|)>=\<Delta\><rsub|<text|c>>*U<st><around|(|T<rsub|<text|ref>>|)>+<big|sum><rsub|i>\<nu\><rsub|i><rsup|<text|g>>*R*T<rsub|<text|ref>>
  </equation>

  <subsubsection*|Washburn corrections><label|c11 sec rc-bomb-wc>

  <index-complex|<tuple|washburn corrections>||c11 sec rc-bomb-wc
  idx1|<tuple|Washburn corrections>>The Washburn corrections needed in Eq.
  <reference|DelUo=DelU(IBP)+...> are internal energy changes for certain
  hypothetical physical processes occurring at the reference temperature
  <math|T<rsub|<text|ref>>> involving the substances present in the bomb
  vessel. In these processes, substances change from their standard states to
  the initial state of the <subindex|Isothermal|bomb process>isothermal bomb
  process, or change from the final state of the isothermal bomb process to
  their standard states.

  For example, consider the complete combustion of a solid or liquid compound
  of carbon, hydrogen, and oxygen in which the combustion products are
  CO<rsub|<math|2>> and H<rsub|<math|2>>O and there are no side reactions or
  auxiliary reactions. In the initial state of the isothermal bomb process,
  the bomb vessel contains the pure reactant, liquid water with
  O<rsub|<math|2>> dissolved in it, and a gaseous mixture of O<rsub|<math|2>>
  and H<rsub|<math|2>>O, all at a high pressure <math|p<rsub|1>>. In the
  final state, the bomb vessel contains liquid water with O<rsub|<math|2>>
  and CO<rsub|<math|2>> dissolved in it and a gaseous mixture of
  O<rsub|<math|2>>, H<rsub|<math|2>>O, and CO<rsub|<math|2>>, all at pressure
  <math|p<rsub|2>>. In addition, the bomb vessel contains internal parts of
  constant mass such as the sample holder and ignition wires.

  In making Washburn corrections, we must use a single standard state for
  each substance in order for Eq. <reference|DelUo=DelU(IBP)+...> to
  correctly give the standard internal energy of combustion. In the present
  example we choose the following standard states: pure solid or liquid for
  the reactant compound, pure liquid for the H<rsub|<math|2>>O, and pure
  ideal gases for the O<rsub|<math|2>> and CO<rsub|<math|2>>, each at
  pressure <math|p<st>=1<br>>.

  We can calculate the amount of each substance in each phase, in both the
  initial state and final state of the isothermal bomb process, from the
  following information: the internal volume of the bomb vessel; the mass of
  solid or liquid reactant initially placed in the vessel; the initial amount
  of H<rsub|<math|2>>O; the initial O<rsub|<math|2>> pressure; the water
  vapor pressure; the solubilities (estimated from Henry's law constants) of
  O<rsub|<math|2>> and CO<rsub|<math|2>> in the water; and the stoichiometry
  of the combustion reaction. Problem <vpageref|prb:11-hexane combustion>
  guides you through these calculations.<index-complex|<tuple|washburn
  corrections>||c11 sec rc-bomb-wc idx1|<tuple|Washburn
  corrections>><index-complex|<tuple|calorimeter|bomb>||c11 sec rc-bomb
  idx1|<tuple|Calorimeter|bomb>><index-complex|<tuple|bomb calorimeter>||c11
  sec rc-bomb idx2|<tuple|Bomb calorimeter>>

  <subsection|Other calorimeters><label|c11 sec rc-other>

  Experimenters have used great ingenuity in designing calorimeters to
  measure reaction enthalpies and to improve their precision. In addition to
  the constant-pressure reaction calorimeter and bomb calorimeter described
  above, three additional types will be briefly mentioned.

  A <subindex|Calorimeter|phase-change><em|phase-change calorimeter> has two
  coexisting phases of a pure substance in thermal contact with the reaction
  vessel and an adiabatic outer jacket. The two coexisting phases constitute
  a univariant subsystem that at constant pressure is at the fixed
  temperature of the equilibrium phase transition. The thermal energy
  released or absorbed by the reaction, instead of changing the temperature,
  is transferred isothermally to or from the coexisting phases and can be
  measured by the volume change of the phase transition. A reaction enthalpy,
  of course, can only be measured by this method at the temperature of the
  equilibrium phase transition. The well-known <subindex|Calorimeter|Bunsen
  ice>Bunsen ice calorimeter uses the ice\Uwater transition at <math|0
  <degC>>. The solid\Uliquid transition of diphenyl ether has a relatively
  large volume change and is useful for measurements at <math|26.9 <degC>>.
  Phase-transition calorimeters are especially useful for slow reactions.

  A <subindex|Calorimeter|heat-flow><em|heat-flow calorimeter> is a variation
  of an <subindex|Calorimeter|isothermal-jacket><index|Isoperibol
  calorimeter><subindex|Calorimeter|isoperibol>isothermal-jacket calorimeter.
  It uses a <index|Thermopile>thermopile (Sec. <vpageref|2-practical
  thermometers>) to continuously measure the temperature difference between
  the reaction vessel and an outer jacket acting as a constant-temperature
  heat sink. The heat transfer takes place mostly through the thermocouple
  wires, and to a high degree of accuracy is proportional to the temperature
  difference integrated over time. This is the best method for an extremely
  slow reaction, and it can also be used for rapid reactions.

  A <subindex|Calorimeter|flame><em|flame calorimeter> is a flow system in
  which oxygen, fluorine, or another gaseous oxidant reacts with a gaseous
  fuel. The heat transfer between the flow tube and a heat sink can be
  measured with a thermopile, as in a heat-flow
  calorimeter.<index-complex|<tuple|calorimetry|reaction>||c11 sec rc
  idx1|<tuple|Calorimetry|reaction>>

  <section|Adiabatic Flame Temperature><label|c11 sec aft>

  <index-complex|<tuple|adiabatic|flame temperature>||c11 sec aft
  idx1|<tuple|Adiabatic|flame temperature>>With a few simple approximations,
  we can estimate the temperature of a flame formed in a flowing gas mixture
  of oxygen or air and a fuel. We treat a moving segment of the gas mixture
  as a closed system in which the temperature increases as combustion takes
  place. We assume that the reaction occurs at a constant pressure equal to
  the standard pressure, and that the process is adiabatic and the gas is an
  ideal-gas mixture.

  The principle of the calculation is similar to that used for a
  constant-pressure calorimeter as explained by the paths shown in Fig.
  <vpageref|fig:11-calorimeter paths>. When the combustion reaction in the
  segment of gas reaches reaction equilibrium, the advancement has changed by
  <math|<Del>\<xi\>> and the temperature has increased from <math|T<rsub|1>>
  to <math|T<rsub|2>>. Because the reaction is assumed to be adiabatic at
  constant pressure, <math|<Del>H<expt>> is zero. Therefore, the sum of
  <math|<Del>H*<around|(|<text|rxn>,T<rsub|1>|)>> and
  <math|<Del>H<around|(|<text|P>|)>> is zero, and we can write

  <\equation>
    <label|ad. flame><Del>\<xi\>*\<Delta\><rsub|<text|c>>*H<st><around|(|T<rsub|1>|)>+<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em>C<rsub|p><around|(|<text|P>|)>*<dif>T=0
  </equation>

  where <math|\<Delta\><rsub|<text|c>>*H<st><around|(|T<rsub|1>|)>> is the
  standard molar enthalpy of combustion at the initial temperature, and
  <math|C<rsub|p><around|(|<text|P>|)>> is the heat capacity at constant
  pressure of the product mixture.

  The value of <math|T<rsub|2>> that satisfies Eq. <reference|ad. flame> is
  the <em|estimated> flame temperature. Problem <reference|prb:11-flame>
  presents an application of this calculation. Several factors cause the
  actual temperature in a flame to be lower: the process is never completely
  adiabatic, and in the high temperature of the flame there may be product
  dissociation and other reactions in addition to the main combustion
  reaction.<index-complex|<tuple|adiabatic|flame temperature>||c11 sec aft
  idx1|<tuple|Adiabatic|flame temperature>>

  <section|Gibbs Energy and Reaction Equilibrium><label|c11 sec gere>

  This section begins by examining the way in which the Gibbs energy changes
  as a chemical process advances in a closed system at constant <math|T> and
  <math|p> with expansion work only. A universal criterion for reaction
  equilibrium is derived involving the molar reaction Gibbs energy.

  <subsection|The molar reaction Gibbs energy><label|11-molar rxn Gibbs
  energy><label|c11 sec-gere-gibbs-energy>

  Applying the general definition of a molar differential reaction quantity
  (Eq. <reference|del(r)Xm=sum(nu_i)X_i>) to the Gibbs energy of a closed
  system with <math|T>, <math|p>, and <math|\<xi\>> as the independent
  variables, we obtain the definition of the <subindex|Gibbs energy|molar
  reaction><newterm|molar reaction Gibbs energy> or molar Gibbs energy of
  reaction, <math|\<Delta\><rsub|<text|r>>*G>:

  <\equation>
    <label|del(r)Gm=sum(nu_i)(mu_i)>\<Delta\><rsub|<text|r>>*G<defn><big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>
  </equation>

  Equation <reference|del(r)Xm=dX/dxi> shows that this quantity is also given
  by the partial derivative

  <equation-cov2|<label|del(r)Gm=dG/dx>\<Delta\><rsub|<text|r>>*G=<Pd|G|\<xi\>|T,p>|(closed
  system)>

  The total differential of <math|G> is then

  <equation-cov2|<label|dG=-SdT+Vdp+(del(r)Gm)dxi><dif>G=-S<dif>T+V<difp>+\<Delta\><rsub|<text|r>>*G<dif>\<xi\>|(closed
  system)>

  <subsection|Spontaneity and reaction equilibrium><label|11-spont and
  eqm><label|c11 sec-gere-spontaneity>

  In Sec. <reference|5-combining>, we found that the spontaneous direction of
  a process taking place in a closed system at constant <math|T> and
  <math|p>, with expansion work only, is the direction of decreasing
  <math|G>. In the case of a chemical process occurring at constant <math|T>
  and <math|p>, <math|\<Delta\><rsub|<text|r>>*G> is the rate at which
  <math|G> changes with <math|\<xi\>>. Thus if
  <math|\<Delta\><rsub|<text|r>>*G> is positive, <math|\<xi\>> spontaneously
  decreases; if <math|\<Delta\><rsub|<text|r>>*G> is negative, <math|\<xi\>>
  spontaneously increases. During a <subindex|Process|spontaneous><index|Spontaneous
  process>spontaneous process <math|<dif>\<xi\>> and
  <math|\<Delta\><rsub|<text|r>>*G> have opposite signs.<footnote|Sometimes
  reaction spontaneity at constant <math|T> and <math|p> is ascribed to the
  \Pdriving force\Q of a quantity called the <index|Affinity of
  reaction><em|affinity of reaction>, defined as the negative of
  <math|\<Delta\><rsub|<text|r>>*G>. <math|\<xi\>> increases spontaneously if
  the affinity is positive and decreases spontaneously if the affinity is
  negative; the system is at equilibrium when the affinity is zero.>

  <\quote-env>
    \ Note how the equality of Eq. <reference|dG=-SdT+Vdp+(del(r)Gm)dxi>
    agrees with the inequality <math|<dif>G\<less\>-S*<dif>T+V*<difp>>, a
    criterion of spontaneity in a closed system with expansion work only (Eq.
    <vpageref|dG\<less\>=-SdT+Vdp+dw'>). When <math|<dif>\<xi\>> and
    <math|\<Delta\><rsub|<text|r>>*G> have opposite signs,
    <math|\<Delta\><rsub|<text|r>>*G<dif>\<xi\>> is negative and
    <math|<dif>G=<around|(|-S*<dif>T+V*<difp>+\<Delta\><rsub|<text|r>>*G*<dif>\<xi\>|)>>
    is less than <math|<around|(|-S*<dif>T+V*<difp>|)>>.
  </quote-env>

  If the system is closed and contains at least one phase that is a mixture,
  a state of reaction equilibrium can be approached spontaneously at constant
  <math|T> and <math|p> in either direction of the reaction; that is, by both
  positive and negative changes of <math|\<xi\>>. In this equilibrium state,
  therefore, <math|G> has its minimum value for the given <math|T> and
  <math|p>. Since <math|G> is a smooth function of <math|\<xi\>>, its rate of
  change with respect to <math|\<xi\>> is zero in the equilibrium state. The
  condition for <subindex|Equilibrium|reaction><em|reaction equilibrium>,
  then, is that <math|\<Delta\><rsub|<text|r>>*G> must be zero:

  <equation-cov2|<label|del(r)G(m)=0>\<Delta\><rsub|<text|r>>*G=<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>=0|(reaction
  equilibrium)>

  It is important to realize that this condition is independent of whether or
  not reaction equilibrium is approached at constant temperature and
  pressure. It is a universal criterion of reaction equilibrium. The value of
  <math|\<Delta\><rsub|<text|r>>*G> is equal to
  <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i>> and
  depends on the state of the system. If the state is such that
  <math|\<Delta\><rsub|<text|r>>*G> is positive, the direction of spontaneous
  change is one that, under the existing constraints, allows
  <math|\<Delta\><rsub|<text|r>>*G> to decrease. If
  <math|\<Delta\><rsub|<text|r>>*G> is negative, the spontaneous change
  increases the value of <math|\<Delta\><rsub|<text|r>>*G>. When the system
  reaches reaction equilibrium, whatever the path of the spontaneous process,
  the value of <math|\<Delta\><rsub|<text|r>>*G> becomes zero.

  <subsection|General derivation><label|11-gen rxn eqm><label|c11 sec
  gere-derivation>

  <index-complex|<tuple|equilibrium conditions>||c11 sec gere-derivation
  idx1|<tuple|Equilibrium conditions|for reaction>>We can obtain the
  condition of reaction equilibrium given by Eq. <reference|del(r)G(m)=0> in
  a more general and rigorous way by an extension of the derivation of Sec.
  <reference|9-eqm conditions>, which was for equilibrium conditions in a
  multiphase, multicomponent system.

  Consider a system with a reference phase, <math|<pha><rprime|'>>, and
  optionally other phases labeled by <math|<pha>\<ne\><pha><rprime|'>>. Each
  phase contains one or more species labeled by subscript <math|i>, and some
  or all of the species are the reactants and products of a reaction.

  The total differential of the internal energy is given by Eq.
  <vpageref|dU(multiphase,mixt)>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dif>U>|<cell|=>|<cell|T<aphp>*<dif>S<aphp>-p<aphp>*<dif>V<aphp>+<big|sum><rsub|i>\<mu\><rsub|i><aphp>*<dif>n<rsub|i>>>|<row|<cell|>|<cell|>|<cell|+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><around*|(|T<aph>*<dif>S<aph>-p<aph>*<dif>V<aph>+<big|sum><rsub|i>\<mu\><rsub|i><aph>*<dif>n<rsub|i><aph>|)><eq-number><label|dU=T(alpha)dS(alpha)
    +..>>>>>
  </eqnarray*>

  The conditions of isolation are

  <\eqnarray*>
    <tformat|<cwith|1|-1|2|2|cell-halign|l>|<table|<row|<cell|>|<cell|<dif>U=0>|<cell|<text|<space|2em>(constant
    internal energy)><eq-number>>>|<row|<cell|>|<cell|<dif>V<aphp>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>V<aph>=0>|<cell|<text|<space|2em>(no
    expansion work)><eq-number>>>|<row|<cell|>|<cell|<text|For each species
    <math|i>:>>|<cell|>>|<row|<cell|>|<cell|<dif>n<rsub|i><aphp>+<big|sum><rsub|<pha>\<ne\><pha>'><dif>n<rsub|i><aph>=\<nu\><rsub|i>*<dif>\<xi\>>|<cell|<text|<space|2em>(closed
    system)><eq-number><label|closed sys (eqm)>>>>>
  </eqnarray*>

  In Eq. <reference|closed sys (eqm)>, <math|<dif>n<rsup|<pha><rprime|''>><rsub|i<rprime|'>>>
  should be set equal to zero for a species <math|i<rprime|'>> that is
  excluded from phase <math|<pha><rprime|''>>, and
  <math|\<nu\><rsub|i<rprime|''>>> should be set equal to zero for a species
  <math|i<rprime|''>> that is not a reactant or product of the reaction.

  We use these conditions of isolation to substitute for <math|<dif>U>,
  <math|<dif>V<aphp>>, and <math|<dif>n<rsub|i><aphp>> in Eq.
  <reference|dU=T(alpha)dS(alpha) +..>, and make the further substitution
  <math|<dif>S<aphp>=<dif>S-<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>S<aph>>.
  Solving for <math|<dif>S>, we obtain

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dif>S>|<cell|=>|<cell|<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|<around|(|T<aphp>-T<aph>|)>|T<aphp>>*<dif>S<aph>-<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|<around|(|p<aphp>-p<aph>|)>|T<aphp>>*<dif>V<aph>>>|<row|<cell|>|<cell|>|<cell|+<big|sum><rsub|i><big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|<around|(|\<mu\><rsub|i><aphp>-\<mu\><rsub|i><aph>|)>|T<aphp>>*<dif>n<rsub|i><aph>-<frac|<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i><aphp>|T<aphp>>*<dif>\<xi\><eq-number><label|dS=
    (eqm)>>>>>
  </eqnarray*>

  The equilibrium condition is that the coefficient multiplying each
  differential on the right side of Eq. <reference|dS= (eqm)> must be zero.
  We conclude that at equilibrium the temperature of each phase is equal to
  that of phase <math|<pha><rprime|'>>; the pressure of each phase is equal
  to that of phase <math|<pha><rprime|'>>; the chemical potential of each
  species, in each phase containing that species, is equal to the chemical
  potential of the species in phase <math|<pha><rprime|'>>; and the quantity
  <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i><aphp>>
  (which is equal to <math|\<Delta\><rsub|<text|r>>*G>) is zero.

  In short, <em|in an equilibrium state each phase has the same temperature
  and the same pressure, each species has the same chemical potential in the
  phases in which it is present, and the molar reaction Gibbs energy of each
  phase is zero>.

  <subsection|Pure phases><label|11-pure phases><label|c11 sec
  gere-pure-phases>

  <index-complex|<tuple|reaction|between pure phases>||c11 sec
  gere-pure-phases idx1|<tuple|Reaction|between pure phases>>Consider a
  chemical process in which each reactant and product is in a separate pure
  phase. For example, the decomposition of calcium carbonate,
  CaCO<rsub|<math|3>>(s)<space|0.17em><ra><space|0.17em>CaO(s) +
  CO<rsub|<math|2>>(g), involves three pure phases if no other gas is allowed
  to mix with the CO<rsub|<math|2>>.

  As this kind of reaction advances at constant <math|T> and <math|p>, the
  chemical potential of each substance remains constant, and
  <math|\<Delta\><rsub|<text|r>>*G> is therefore constant. The value of
  <math|\<Delta\><rsub|<text|r>>*G> for this reaction depends only on
  <math|T> and <math|p>. If <math|\<Delta\><rsub|<text|r>>*G> is negative,
  the reaction proceeds spontaneously to the right until one of the reactants
  is exhausted; the reaction is said to \Pgo to completion.\Q If
  <math|\<Delta\><rsub|<text|r>>*G> is positive, the reaction proceeds
  spontaneously to the left until one of the products is
  exhausted.<footnote|Keep in mind that whether a species is called a
  reactant or a product depends, not on whether its amount decreases or
  increases during a reaction process, but rather on which side of the
  reaction equation it appears.> The reactants and products can remain in
  equilibrium only if <math|T> and <math|p> are such that
  <math|\<Delta\><rsub|<text|r>>*G> is zero. These three cases are
  illustrated in Fig. <vpageref|fig:11-G vs xi, pure
  phases>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/G-PURE.eps|240pt|103pt||>>
        <label|fig:11-G vs xi, pure phases>Gibbs energy versus advancement at
        constant <math|T> and <math|p> in systems of pure phases. <math|G> is
        a linear function of <math|\<xi\>> with slope equal to
        <math|\<Delta\><rsub|<text|r>>*G>.

        <\enumerate-alpha>
          <item><math|\<Delta\><rsub|<text|r>>*G> is negative; <math|\<xi\>>
          spontaneously increases.

          <item><math|\<Delta\><rsub|<text|r>>*G> is positive; <math|\<xi\>>
          spontaneously decreases.

          <item><math|\<Delta\><rsub|<text|r>>*G> is zero; the system is in
          reaction equilibrium at all values of <math|\<xi\>>.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float>

  <\quote-env>
    Note the similarity of this behavior to that of an
    <subindex|Equilibrium|phase transition><subsubindex|Phase|transition|equilibrium>equilibrium
    phase transition of a pure substance. Only one phase of a pure substance
    is present at equilibrium unless <math|\<Delta\><rsub|<text|trs>>*G> is
    zero. A phase transition is a special case of a chemical process.
  </quote-env>

  <index-complex|<tuple|reaction|between pure phases>||c11 sec
  gere-pure-phases idx1|<tuple|Reaction|between pure phases>>

  <subsection|Reactions involving mixtures><label|11-rxns involving
  mixtures><label|c11 sec gere-mixtures>

  <index-complex|<tuple|reaction|mixture>||c11 sec gere-mixtures
  idx1|<tuple|Reaction|in a mixture>>If any of the reactants or products of a
  chemical process taking place in a closed system is a constituent of a
  mixture, a plot of <math|G> versus <math|\<xi\>> (at constant <math|T> and
  <math|p>) turns out to exhibit a minimum with a slope of zero; see the
  example in Fig. <vpageref|fig:11-G vs xi>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/G-XI-1.eps|126pt|148pt||>>
        <label|fig:11-G vs xi>Gibbs energy as a function of advancement at
        constant <math|T> and <math|p> in a closed system containing a
        mixture. The open circle is at the minimum value of <math|G>. (The
        reaction is the same as in Fig. <vpageref|fig:11-S-H-xi>.)
      </big-figure>
    </framed>
  </float> At constant <math|T> and <math|p>, <math|\<xi\>> changes
  spontaneously in the direction of decreasing <math|G> until the minimum is
  reached, at which point <math|\<Delta\><rsub|<text|r>>*G> (the slope of the
  curve) is zero and the system is in a state of reaction equilibrium.

  The condition of reaction equilibrium given by
  <math|\<Delta\><rsub|<text|r>>*G=0> or <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i>=0>
  is a general one that is valid whether or not the reaction proceeds at
  constant <math|T> and <math|p>. Suppose a spontaneous reaction occurs in a
  closed system at constant temperature and <em|volume>. The system is at
  reaction equilibrium when <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i>>
  becomes equal to zero. To relate this condition to the change of a
  thermodynamic potential, we take the expression for the total differential
  of the Helmholtz energy of an open system, with expansion work only, given
  by Eq. <vpageref|dA=-SdT-pdV+sum(mu_i)dn_i>:

  <\equation>
    <dif>A=-S*<dif>T-p*<dif>V+<big|sum><rsub|i>\<mu\><rsub|i>*<dif>n<rsub|i>
  </equation>

  When we make the substitution <math|<dif>n<rsub|i>=\<nu\><rsub|i>*<dif>\<xi\>>,
  we obtain an expression for the total differential of <math|A> in a closed
  system with a chemical reaction:

  <\equation>
    <label|dA=-SdT-pdV+sum()dxi><dif>A=-S*<dif>T-p*<dif>V+<around*|(|<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>|)>*<dif>\<xi\>
  </equation>

  We identify the coefficient of the last term on the right as a partial
  derivative:

  <\equation>
    <big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>=<Pd|A|\<xi\>|T,V>
  </equation>

  This equation shows that as the reaction proceeds spontaneously at constant
  <math|T> and <math|V>, it reaches reaction equilibrium at the point where
  <math|<pd|A|\<xi\>|T,V>> is zero. This is simply another way to express the
  criterion for spontaneity stated on page <pageref|spon at const T & V>: If
  the only work is expansion work, the Helmholtz energy of a closed system
  decreases during a spontaneous process at constant <math|T> and <math|V>
  and has its minimum value when the system attains an equilibrium
  state.<index-complex|<tuple|reaction|mixture>||c11 sec gere-mixtures
  idx1|<tuple|Reaction|in a mixture>>

  <subsection|Reaction in an ideal gas mixture><label|c11 sec gere-ideal-gas>

  <index-complex|<tuple|reaction|ideal gas mixture>||c11 sec gere-ideal-gas
  idx1|<tuple|Reaction|in an ideal gas mixture>>Let us look in detail at the
  source of the minimum in <math|G> for the case of a reaction occurring in
  an ideal gas mixture in a closed system at constant <math|T> and <math|p>.
  During this process the system has only one independent variable, which it
  is convenient to choose as the advancement <math|\<xi\>>. The
  <index|Additivity rule>additivity rule (Eq. <reference|X=sum(X_i*n_i)>) for
  the Gibbs energy is

  <\equation>
    <label|G=sum(n_i)(mu_i)>G=<big|sum><rsub|i>n<rsub|i>*\<mu\><rsub|i>
  </equation>

  where both <math|n<rsub|i>> and <math|\<mu\><rsub|i>> depend on
  <math|\<xi\>>. Thus, <math|G> is a complicated function of <math|\<xi\>>.

  For the chemical potential of each substance, we write
  <math|\<mu\><rsub|i>=\<mu\><rsub|i><st><gas>+R*T*ln
  <around|(|p<rsub|i>/p<st>|)>> (Eq. <reference|mu_i=mu_io(g)+RT*ln(p_i/po)>),
  where <math|p<rsub|i>> is the partial pressure of <math|i> in the mixture.
  Substitution in Eq. <reference|G=sum(n_i)(mu_i)> gives, for the Gibbs
  energy at any value of <math|\<xi\>>,

  <\equation>
    G<around|(|\<xi\>|)>=<big|sum><rsub|i>n<rsub|i>*<around*|[|\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|p<rsub|i>|p<st>>|]>
  </equation>

  At <math|\<xi\>=0>, the amounts and partial pressures have their initial
  values <math|n<rsub|i,0>> and <math|p<rsub|i,0>>:

  <\equation>
    G<around|(|0|)>=<big|sum><rsub|i>n<rsub|i,0>*<around*|[|\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|p<rsub|i,0>|p<st>>|]>
  </equation>

  The difference between these two expressions is

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|G<around|(|\<xi\>|)>-G<around|(|0|)>>|<cell|=<big|sum><rsub|i><around|(|n<rsub|i>-n<rsub|i,0>|)>*\<mu\><rsub|i><st><gas>>>|<row|<cell|>|<cell|<space|1em>+R*T*<big|sum><rsub|i>n<rsub|i>*ln
      <frac|p<rsub|i>|p<st>>-R*T*<big|sum><rsub|i>n<rsub|i,0>*ln
      <frac|p<rsub|i,0>|p<st>>>>>>
    </eqsplit>
  </equation>

  Converting partial pressures to mole fractions with
  <math|p<rsub|i>=y<rsub|i>*p> and <math|p<rsub|i,0>=y<rsub|i,0>*p> gives

  <\eqnarray*>
    <tformat|<table|<row|<cell|G<around|(|\<xi\>|)>-G<around|(|0|)>>|<cell|=>|<cell|<big|sum><rsub|i><around|(|n<rsub|i>-n<rsub|i,0>|)>*\<mu\><rsub|i><st><gas>+R*T*<big|sum><rsub|i>n<rsub|i>*ln
    y<rsub|i>>>|<row|<cell|>|<cell|>|<cell|-R*T*<big|sum><rsub|i>n<rsub|i,0>*ln
    y<rsub|i,0>+R*T*<big|sum><rsub|i><around|(|n<rsub|i>-n<rsub|i,0>|)>*ln
    <frac|p|p<st>><eq-number><label|G(xi)-G(0)>>>>>
  </eqnarray*>

  With the substitution <math|n<rsub|i>-n<rsub|i,0>=\<nu\><rsub|i>*\<xi\>>
  (Eq. <reference|n_i=n_(i,0)+(nu_i)xi>) in the first and last terms on the
  right side of Eq. <reference|G(xi)-G(0)>, the result is

  <\eqnarray*>
    <tformat|<table|<row|<cell|G<around|(|\<xi\>|)>-G<around|(|0|)>>|<cell|=>|<cell|\<xi\>*<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i><st><gas>+R*T*<big|sum><rsub|i>n<rsub|i>*ln
    y<rsub|i>>>|<row|<cell|>|<cell|>|<cell|-R*T*<big|sum><rsub|i>n<rsub|i.*0>*ln
    y<rsub|i,0>+R*T<around*|(|<big|sum><rsub|i>\<nu\><rsub|i>|)>*\<xi\>*ln
    <frac|p|p<st>><eq-number><label|G(xi)-G(0)=>>>>>
  </eqnarray*>

  The sum <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i><st><gas>><label|Del(r)G^o=sum>in
  the first term on the right side of Eq. <reference|G(xi)-G(0)=> is
  <math|\<Delta\><rsub|<text|r>>*G<st>>, the standard molar reaction Gibbs
  energy. Making this substitution gives finally

  <\eqnarray*>
    <tformat|<table|<row|<cell|G<around*|(|\<xi\>|)>-G<around*|(|0|)>>|<cell|=>|<cell|\<xi\>*\<Delta\><rsub|<text|r>>*G<st>+R*T*<big|sum><rsub|i>n<rsub|i>*ln
    y<rsub|i>-R*T*<big|sum><rsub|i>n<rsub|i.*0>*ln
    y<rsub|i,0>>>|<row|<cell|>|<cell|>|<cell|+R*T*<around*|(|<big|sum><rsub|i>\<nu\><rsub|i>|)>*\<xi\>*ln
    <frac|p|p<st>><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<text|(ideal
    gas mixture)>>>>>><label|G(xi)-G(0)= (IG)>>>>>
  </eqnarray*>

  There are four terms on the right side of Eq. <reference|G(xi)-G(0)= (IG)>.
  The first term is the Gibbs energy change for the reaction of pure
  reactants to form pure products under standard-state conditions, the second
  is a mixing term, the third term is constant, and the last term is an
  adjustment of <math|G> from the standard pressure to the pressure of the
  gas mixture. Note that the first and last terms are proportional to the
  advancement and cannot be the cause of a minimum in the curve of the plot
  of <math|G> versus <math|\<xi\>>. It is the <em|mixing term>
  <math|R*T*<big|sum><rsub|i>n<rsub|i>*ln y<rsub|i>> that is responsible for
  the observed minimum.<footnote|This term also causes the slope of the curve
  of <math|G<around|(|\<xi\>|)>-G<around|(|0|)>> versus <math|\<xi\>> to be
  <math|-\<infty\>> and <math|+\<infty\>> at the left and right extremes of
  the curve.> This term divided by <math|n=<big|sum><rsub|i>n<rsub|i>> is
  <math|<Del>G<m><mix>>, the molar differential Gibbs energy of mixing to
  form an ideal mixture (see Eq. <vpageref|del(mix)Gm(id)=RT*sum(x_i)ln(x_i)>);
  the term is also equal to <math|-n*T*<Del>S<m><mix>> (Eq.
  <reference|del(mix)Sm(id)=-R*sum(x_i)ln(x_i)>), showing that the minimum is
  entirely an entropy effect.

  Now let us consider specifically the simple reaction

  <\equation*>
    <text|A><around*|(|<text|g>|)><arrow><text|B><around*|(|<text|g>|)>
  </equation*>

  in an ideal gas mixture, for which <math|\<nu\><A>> is <math|-1> and
  <math|\<nu\><B>> is <math|+1>. Let the initial state be one of pure A:
  <math|n<rsub|<text|B>,0>=0>. The initial mole fractions are then
  <math|y<rsub|<text|A>,0>=1> and <math|y<rsub|<text|B>,0>=0>. In this
  reaction, the total amount <math|n=n<A>+n<B>> is constant. Substituting
  these values in Eq. <reference|G(xi)-G(0)= (IG)> gives<footnote|Note that
  although <math|ln y<A>> approaches <math|-\<infty\>> as <math|y<A>>
  approaches zero, the product <math|y<A>*ln y<A>> approaches <em|zero> in
  this limit. This behavior can be proved with l'Hospital's rule (see any
  calculus textbook).>

  <\equation>
    G<around|(|\<xi\>|)>-G<around|(|0|)>=\<xi\>*\<Delta\><rsub|<text|r>>*G<st>+n*R*T*<around|(|y<A>*ln
    y<A>+y<B>*ln y<B>|)>
  </equation>

  The second term on the right side is <math|n<Del>G<m><mix>>, the Gibbs
  energy of mixing pure ideal gases A and B at constant <math|T> and <math|p>
  to form an ideal gas mixture of composition <math|y<A>> and <math|y<B>>.
  Since the curve of <math|<Del>G<m><mix>> plotted against <math|\<xi\>> has
  a minimum (as shown in Fig. <vpageref|fig:11-mixing quantities>),
  <math|G<around|(|\<xi\>|)>-G<around|(|0|)>> also has a minimum.

  Figure <vpageref|fig:11-G vs xi, multipart><\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/G-XI-2.eps|353pt|172pt||>>
        <label|fig:11-G vs xi, multipart>Gibbs energy as a function of the
        advancement of the reaction A<math|<arrow>>B in an ideal gas mixture
        at constant <math|T> and <math|p>. The initial amount of B is zero.
        The equilibrium positions are indicated by open circles.

        (a)<nbsp><math|\<Delta\><rsub|<text|r>>*G<st>\<less\>0><space|2em>
        (b)<nbsp><math|\<Delta\><rsub|<text|r>>*G<st>=0><space|2em>
        (c)<nbsp><math|\<Delta\><rsub|<text|r>>*G<st>\<gtr\>0>
      </big-figure>
    </framed>
  </float> illustrates how the position of the minimum, which is the position
  of reaction equilibrium, depends on the value of
  <math|\<Delta\><rsub|<text|r>>*G<st>>. The more negative is
  <math|\<Delta\><rsub|<text|r>>*G<st>>, the closer to the product side of
  the reaction is the equilibrium position. On the other hand, the more
  positive is <math|\<Delta\><rsub|<text|r>>*G<st>>, the smaller is the value
  of <math|\<xi\>> at equilibrium. These statements apply to any reaction in
  a homogeneous mixture.

  As the reaction A<math|<arrow>>B proceeds, there is no change in the total
  number of molecules, and therefore in an ideal gas mixture at constant
  temperature and volume there is no pressure change. The point of reaction
  equilibrium is at the minimum of <math|G> when both <math|V> and <math|p>
  are constant.

  The situation is different when the number of molecules changes during the
  reaction. Consider the reaction A<math|<arrow>>2<space|0.17em>B in an ideal
  gas mixture. As this reaction proceeds to the right at constant <math|T>,
  the volume increases if the pressure is held constant and the pressure
  increases if the volume is held constant. Figure
  <vpageref|fig:11-V-p-G><\float|float|thb>
    <\framed>
      <\big-figure|<image|11-SUP/V-P-G.eps|264pt|253pt||>>
        <label|fig:11-V-p-G>Dependence of Gibbs energy on volume and
        pressure, at constant temperature, in a closed system containing an
        ideal gas mixture of A and B. The reaction is
        A<math|<arrow>>2<space|0.17em>B with
        <math|\<Delta\><rsub|<text|r>>*G<st|=>=0>. Solid curves: contours of
        constant <math|G> plotted at an interval of
        <math|0.5*n<rsub|<text|A>,0>*R*T>. Dashed curve: states of reaction
        equilibrium (<math|\<Delta\><rsub|<text|r>>*G=0>). Dotted curves:
        limits of possible values of the advancement. Open circle: position
        of minimum <math|G> (and an equilibrium state) at the constant
        pressure <math|p=1.02*p<st>>. Filled circle: position of minimum
        <math|G> for a constant volume of <math|1.41*V<rsub|0>>, where
        <math|V<rsub|0>> is the initial volume at pressure <math|p<st>>.
      </big-figure>
    </framed>
  </float> shows how <math|G> depends on both <math|p> and <math|V> for this
  reaction. Movement along the horizontal dashed line in the figure
  corresponds to reaction at constant <math|T> and <math|p>. The minimum of
  <math|G> along this line is at the volume indicated by the open circle. At
  this volume, <math|G> has an even lower minimum at the pressure indicated
  by the filled circle, where the vertical dashed line is tangent to one of
  the contours of constant <math|G>. The condition needed for reaction
  equilibrium, however, is that <math|\<Delta\><rsub|<text|r>>*G> must be
  zero. This condition is satisfied along the vertical dashed line only at
  the position of the open circle.

  This example demonstrates that for a reaction occurring at constant
  temperature and <em|volume> in which the pressure changes, the point of
  reaction equilibrium is not the point of minimum <math|G>. Instead, the
  point of reaction equilibrium in this case is at the minimum of the
  Helmholtz energy <math|A> (Sec. <reference|11-rxns involving
  mixtures>).<index-complex|<tuple|reaction|ideal gas mixture>||c11 sec
  gere-ideal-gas idx1|<tuple|Reaction|in an ideal gas
  mixture>><index-complex|<tuple|equilibrium conditions>||c11 sec
  gere-derivation idx1|<tuple|Equilibrium conditions|for reaction>>

  <section|The Thermodynamic Equilibrium Constant><label|c11 sec tec>

  <subsection|Activities and the definition of <math|K>><label|11-eq constant
  defn><label|c11 sec-tec-activities>

  Equation <reference|mu_i(a_i,phi)=> gives the general relation between the
  chemical potential <math|\<mu\><rsub|i>> and the activity <math|a<rsub|i>>
  of species <math|i> in a phase of electric potential <math|\<phi\>>:

  <\equation>
    <label|mu_i(a_i,phi)= again>\<mu\><rsub|i>=\<mu\><rsub|i><st>+R*T*ln
    a<rsub|i>+z<rsub|i>*F*\<phi\>
  </equation>

  The electric potential affects <math|\<mu\><rsub|i>> only if the charge
  number <math|z<rsub|i>> is nonzero, i.e., only if species <math|i> is an
  ion.

  Consider a reaction in which any reactants and products that are ions are
  in a single phase of electric potential <math|\<phi\><rprime|'>>, or in
  several phases of equal electric potential <math|\<phi\><rprime|'>>. Under
  these conditions, substitution of the expression above for
  <math|\<mu\><rsub|i>> in <math|\<Delta\><rsub|<text|r>>*G=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i>>
  gives

  <equation-cov2|<label|Del(r)G=sum+RTsum>\<Delta\><rsub|<text|r>>*G=<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i><st>+R*T*<big|sum><rsub|i>\<nu\><rsub|i>*ln
  a<rsub|i>+F*\<phi\><rprime|'>*<big|sum><rsub|i>\<nu\><rsub|i>*z<rsub|i>|(all
  ions at <math|\<phi\>=\<phi\><rprime|'>>)>

  The first term on the right side of Eq. <reference|Del(r)G=sum+RTsum> is
  the <subindex|Gibbs energy|reaction, standard molar><newterm|standard molar
  reaction Gibbs energy>, or standard molar Gibbs energy of reaction:

  <\equation>
    <label|del(r)Gmo=sum(nu_i)(mu_io)>\<Delta\><rsub|<text|r>>*G<st><defn><big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i><st>
  </equation>

  Since the standard chemical potential <math|\<mu\><st><rsub|i>> of each
  species <math|i> is a function only of <math|T>, the value of
  <math|\<Delta\><rsub|<text|r>>*G<st>> for a given reaction as defined by
  the reaction equation depends only on <math|T> and on the choice of a
  standard state for each reactant and product.

  The last term on the right side of Eq. <reference|Del(r)G=sum+RTsum> is the
  sum <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*z<rsub|i>>.
  Because charge is conserved during the advancement of a reaction in a
  closed system, this sum is zero.

  With these substitutions, Eq. <reference|Del(r)G=sum+RTsum> becomes

  <equation-cov2|<label|Del(r)G=Del(r)o+RTsum>\<Delta\><rsub|<text|r>>*G=\<Delta\><rsub|<text|r>>*G<st>+R*T*<big|sum><rsub|i>\<nu\><rsub|i>*ln
  a<rsub|i>|(all ions at same <math|\<phi\>>)>

  This relation enables us to say that for a reaction at a given temperature
  in which any charged reactants or products are all in the same phase, or in
  phases of equal electric potential, the value of
  <math|\<Delta\><rsub|<text|r>>*G> and <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i>>
  depends only on the activities of the reactants and products and is
  independent of what the electric potentials of any of the phases might
  happen to be.

  Unless a reaction involving ions is carried out in a galvanic cell, the
  ions are usually present in a single phase, and this will not be shown as a
  condition of validity in the rest of this chapter. The special case of a
  reaction in a galvanic cell will be discussed in Sec. <reference|14-molar
  rxn quantities>.

  We may use properties of logarithms to write the sum on the right side of
  Eq. <reference|Del(r)G=Del(r)o+RTsum> as follows:<footnote|The symbol
  <math|<big|prod>> stands for a continued product. If, for instance, there
  are three species, <math|<big|prod><rsub|i>a<rsub|i><rsup|\<nu\><rsub|i>>>
  is the product <math|<around|(|a<rsub|1><rsup|\<nu\><rsub|1>>|)>*<around|(|a<rsub|2><rsup|\<nu\><rsub|2>>|)>*<around|(|a<rsub|3><rsup|\<nu\><rsub|3>>|)>>.>

  <\equation>
    <big|sum><rsub|i>\<nu\><rsub|i>*ln a<rsub|i>=<big|sum><rsub|i>ln
    <around*|(|a<rsub|i><rsup|\<nu\><rsub|i>>|)>=ln
    <big|prod><rsub|i>a<rsub|i><rsup|\<nu\><rsub|i>>
  </equation>

  The product <math|<big|prod><rsub|i>a<rsub|i><rsup|\<nu\><rsub|i>>> is
  called the <subindex|Reaction|quotient><index|Activity
  quotient><newterm|reaction quotient> or activity quotient,
  <math|Q<rsub|<text|rxn>>>:

  <\equation>
    <label|Q=prod(a_i)^(nu_i)>Q<rsub|<text|rxn>><defn><big|prod><rsub|i>a<rsub|i><rsup|\<nu\><rsub|i>>
  </equation>

  <math|Q<rsub|<text|rxn>>> consists of a factor for each reactant and
  product. Each factor is the activity raised to the power of the
  <subindex|Stoichiometric|number>stoichiometric number
  <math|\<nu\><rsub|i>>. Since the value of <math|\<nu\><rsub|i>> is positive
  for a product and negative for a reactant, <math|Q<rsub|<text|rxn>>> is a
  quotient in which the activities of the products appear in the numerator
  and those of the reactants appear in the denominator, with each activity
  raised to a power equal to the corresponding stoichiometric coefficient in
  the reaction equation. Such a quotient, with quantities raised to these
  powers, is called a <index|Proper quotient><newterm|proper quotient>. The
  reaction quotient is a proper quotient of activities.

  For instance, for the ammonia synthesis reaction
  N<rsub|<math|2>>(g)<space|0.17em>+<space|0.17em>3<space|0.17em>H<rsub|<math|2>>(g)<math|<arrow>>2<space|0.17em>NH<rsub|<math|3>>(g)
  the reaction quotient is given by

  <\equation>
    Q<rsub|<text|rxn>>=<frac|a<rsub|<text|NH<rsub|3>>><rsup|2>|a<rsub|<text|N<rsub|2>>>*a<rsub|<text|H<rsub|2>>><rsup|3>>
  </equation>

  <math|Q<rsub|<text|rxn>>> is a dimensionless quantity. It is a function of
  <math|T>, <math|p>, and the mixture composition, so its value changes as
  the reaction advances.

  The expression for the molar reaction Gibbs energy given by Eq.
  <reference|Del(r)G=Del(r)o+RTsum> can now be written

  <\equation>
    <label|del(r)Gm=del(r)Gmo+RT*lnQ(r)>\<Delta\><rsub|<text|r>>*G=\<Delta\><rsub|<text|r>>*G<st>+R*T*ln
    Q<rsub|<text|rxn>>
  </equation>

  The value of <math|Q<rsub|<text|rxn>>> under equilibrium conditions is the
  <subindex|Thermodynamic|equilibrium constant><subindex|Equilibrium
  constant|thermodynamic><newterm|thermodynamic equilibrium constant>,
  <math|K>. The general definition of <math|K> is

  <\equation>
    <label|K=prod(a_i)^(nu_i)>K<defn><big|prod><rsub|i><around|(|a<rsub|i>|)><eq><rsup|\<nu\><rsub|i>>
  </equation>

  where the subscript eq indicates an equilibrium state. Note that <math|K>,
  like <math|Q<rsub|<text|rxn>>>, is dimensionless.

  <\quote-env>
    The <index|IUPAC Green Book>IUPAC Green Book<footnote|Ref.
    <cite|greenbook-3>, p. 58.> gives <math|K<rsup|<space|.01in>\<circ\><space|-.065in>->>
    as an alternative symbol for the thermodynamic equilibrium constant, the
    appended superscript denoting \Pstandard\Q. An IUPAC Commission on
    Thermodynamics<footnote|Ref. <cite|ewing-94>.> has furthermore
    recommended the name \Pstandard equilibrium constant\Q, apparently
    because its value depends on the choice of standard states. Using this
    alternative symbol and name could cause confusion, since the quantity
    defined by Eq. <reference|K=prod(a_i)^(nu_i)> does not refer to reactants
    and products in their standard states but rather to reactants and
    products in an <em|equilibrium> state.
  </quote-env>

  Substituting the equilibrium conditions <math|\<Delta\><rsub|<text|r>>*G=0>
  and <math|Q<rsub|<text|rxn>>=K> in Eq. <reference|del(r)Gm=del(r)Gmo+RT*lnQ(r)>
  gives an important relation between the standard molar reaction Gibbs
  energy and the thermodynamic equilibrium constant:

  <\equation>
    <label|del(r)Gmo=-RT*ln(K)>\<Delta\><rsub|<text|r>>*G<st>=-R*T*ln K
  </equation>

  We can solve this equation for <math|K> to obtain the equivalent relation

  <\equation>
    <label|K=exp(-del(r)Gmo/RT)>K=exp <around*|(|-<frac|\<Delta\><rsub|<text|r>>*G<st>|R*T>|)>
  </equation>

  We have seen that the value of <math|\<Delta\><rsub|<text|r>>*G<st>>
  depends only on <math|T> and the choice of the standard states of the
  reactants and products. This being so, Eq. <reference|K=exp(-del(r)Gmo/RT)>
  shows that the value of <math|K> for a given reaction depends only on
  <math|T> and the choice of standard states. No other condition, neither
  pressure nor composition, can affect the value of <math|K>. We also see
  from Eq. <reference|K=exp(-del(r)Gmo/RT)> that <math|K> is less than
  <math|1> if <math|\<Delta\><rsub|<text|r>>*G<st>> is positive and greater
  than <math|1> if <math|\<Delta\><rsub|<text|r>>*G<st>> is negative. At a
  fixed temperature, reaction equilibrium is attained only if and only if the
  value of <math|Q<rsub|<text|rxn>>> becomes equal to the value of <math|K>
  at that temperature.

  The thermodynamic equilibrium constant <math|K> is the proper quotient of
  the activities of species in reaction equilibrium. At typical temperatures
  and pressures, an activity cannot be many orders of magnitude greater than
  <math|1>. For instance, a partial pressure cannot be greater than the total
  pressure, so at a pressure of <math|10<br>> the activity of a gaseous
  constituent cannot be greater than about <math|10>. The molarity of a
  solute is rarely much greater than <math|10
  <text|mol>\<cdot\><text|dm><rsup|-3>>, corresponding to an activity (on a
  concentration basis) of about <math|10>. Activities can, however, be
  extremely small.

  These considerations lead us to the conclusion that in an equilibrium state
  of a reaction with a very <em|large> value of <math|K>, the activity of at
  least one of the <em|reactants> must be very small. That is, if <math|K> is
  very large then the reaction goes practically to completion and at
  equilibrium a limiting reactant is essentially entirely exhausted. The
  opposite case, a reaction with a very <em|small> value of <math|K>, must
  have at equilibrium one or more <em|products> with very small activities.
  These two cases are the two extremes of the trends shown in Fig.
  <vpageref|fig:11-G vs xi, multipart>.

  Equation <reference|del(r)Gmo=-RT*ln(K)> correctly relates
  <math|\<Delta\><rsub|<text|r>>*G<st>> and <math|K> only if they are both
  calculated with the same standard states. For instance, if we base the
  standard state of a particular solute species on molality in calculating
  <math|\<Delta\><rsub|<text|r>>*G<st>>, the activity of that species
  appearing in the expression for <math|K> (Eq.
  <reference|K=prod(a_i)^(nu_i)>) must also be based on molality.

  <subsection|Reaction in a gas phase><label|11-rxn in gas phase><label|c11
  sec tec-reaction-gas>

  <index-complex|<tuple|reaction|gas phase>||c11 sec tec-reaction-gas
  idx1|<tuple|Reaction|in a gas phase>>If a reaction takes place in a gaseous
  mixture, the standard state of each reactant and product is the pure gas
  behaving ideally at the standard pressure <math|p<st>> (Sec.
  <reference|9-partial molar, id gas mixts>). In this case, each activity is
  given by <math|a<rsub|i><gas>=<fug><rsub|i>/p<st>=\<phi\><rsub|i>*p<rsub|i>/p<st>>
  where <math|\<phi\><rsub|i>> is a fugacity coefficient (Table
  <reference|tbl:9-activities>). When we substitute this expression into Eq.
  <reference|K=prod(a_i)^(nu_i)>, we find we can express the thermodynamic
  equilibrium constant as the product of three factors:

  <equation-cov2|<label|K=prod(phi_i^(nu_i)x...>K=<around*|[|<big|prod><rsub|i><around|(|\<phi\><rsub|i>|)><eq><rsup|\<nu\><rsub|i>>|]>*<around*|[|<big|prod><rsub|i><around|(|p<rsub|i>|)><eq><rsup|\<nu\><rsub|i>>|]>*<around*|[|<around|(|p<st>|)><rsup|-<big|sum><rsub|i>\<nu\><rsub|i>>|]>|(gas
  mixture)>

  On the right side of this equation, the first factor is the proper quotient
  of fugacity coefficients in the mixture at reaction equilibrium, the second
  factor is the proper quotient of partial pressures in this mixture, and the
  third factor is the power of <math|p<st>> needed to make <math|K>
  dimensionless.

  The proper quotient of equilibrium partial pressures is an
  <index-complex|<tuple|equilibrium constant|pressure
  basis>|||<tuple|Equilibrium constant|on a pressure
  basis>><newterm|equilibrium constant on a pressure basis>,
  <math|K<rsub|p>>:

  <equation-cov2|<label|Kp=prod(p_i)^(nu_i)>K<rsub|p>=<big|prod><rsub|i><around|(|p<rsub|i>|)><eq><rsup|\<nu\><rsub|i>>|(gas
  mixture)>

  Note that <math|K<rsub|p>> is dimensionless only if
  <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>> is equal to zero.

  The value of <math|K<rsub|p>> can vary at constant temperature, so
  <math|K<rsub|p>> is not a thermodynamic equilibrium constant. For instance,
  consider what happens when we take an ideal gas mixture at reaction
  equilibrium and compress it isothermally. As the gas pressure increases,
  the fugacity coefficient of each constituent changes from its low pressure
  value of <math|1> and the gas mixture becomes nonideal. In order for the
  mixture to remain in reaction equilibrium, and the product of factors on
  the right side of Eq. <reference|K=prod(phi_i^(nu_i)x...> to remain
  constant, there must be a change in the value of <math|K<rsub|p>>. In other
  words, the reaction equilibrium <em|shifts> as we increase <math|p> at
  constant <math|T>, an effect that will be considered in more detail in Sec.
  <reference|11-effects of T and p>.

  As an example of the difference between <math|K> and <math|K<rsub|p>>,
  consider again the ammonia synthesis <math|<text|N<rsub|2>><around|(|<text|g>|)>+3*<text|H<rsub|2>><around|(|<text|g>|)><arrow>2*<text|NH<rsub|3>><around|(|<text|g>|)>>
  in which the sum <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>>
  equals <math|-2>. For this reaction, the expression for the thermodynamic
  equilibrium constant is

  <\equation>
    K=<around*|(|<frac|\<phi\><rsub|<text|NH<rsub|3>>><rsup|2>|\<phi\><rsub|<text|N<rsub|2>>>*\<phi\><rsub|<text|H<rsub|2>>><rsup|3>>|)><eq>*K<rsub|p><around|(|p<st>|)><rsup|2>
  </equation>

  where <math|K<rsub|p>> is given by

  <\equation>
    K<rsub|p>=<around*|(|<frac|p<rsub|<text|NH<rsub|3>>><rsup|2>|p<rsub|<text|N<rsub|2>>>*p<rsub|<text|H<rsub|2>>><rsup|3>>|)><eq>
  </equation>

  <index-complex|<tuple|reaction|gas phase>||c11 sec tec-reaction-gas
  idx1|<tuple|Reaction|in a gas phase>>

  <subsection|Reaction in solution><label|11-rxn in solution><label|c11 sec
  tec-reaction-solution>

  <index-complex|<tuple|reaction|solution>||c11 sec tec-reaction-solution
  idx1|<tuple|Reaction|in solution>>If any of the reactants or products are
  solutes in a solution, the value of <math|K> depends on the choice of the
  solute standard state.

  For a given reaction at a given temperature, we can derive relations
  between values of <math|K> that are based on different solute standard
  states. In the limit of infinite dilution, each solute activity coefficient
  is unity, and at the standard pressure each pressure factor is unity. Under
  these conditions of infinite dilution and standard pressure, the activities
  of solute B on a mole fraction, concentration, and molality basis are
  therefore

  <\equation>
    <label|a(x,B)= etc>a<xbB>=x<B><space|2em>a<cbB>=c<B>/c<st><space|2em>a<mbB>=m<B>/m<st>
  </equation>

  In the limit of infinite dilution, the solute composition variables
  approach values given by the relations in Eq. <vpageref|nB/nA (dilute)>:
  <math|x<B>=V<A><rsup|\<ast\>>*c<B>=M<A>*m<B>>. Combining these with
  <math|a<xbB>=x<B>> from Eq. <reference|a(x,B)= etc>, we write

  <\equation>
    a<xbB>=V<A><rsup|\<ast\>>*c<B>=M<A>*m<B>
  </equation>

  Then, using the relations for <math|a<cbB>> and <math|a<mbB>> in Eq.
  <reference|a(x,B)= etc>, we find that the activities of solute B at
  infinite dilution and pressure <math|p<st>> are related by

  <\equation>
    <label|a(B) on x,c,m basis>a<xbB>=V<A><rsup|\<ast\>>*c<st>a<cbB>=M<A>*m<st>*a<mbB>
  </equation>

  The expression <math|K=<big|prod><rsub|i><around|(|a<rsub|i>|)><eq><rsup|\<nu\><rsub|i>>>
  has a factor <math|<around|(|a<B>|)><eq><rsup|\<nu\><B>>> for each solute B
  that is a reactant or product. From Eq. <reference|a(B) on x,c,m basis>, we
  see that for solutes at infinite dilution at pressure <math|p<st>>, the
  relations between the values of <math|K> based on different solute standard
  states are

  <\equation>
    <label|K(x basis)=.=.>K<around|(|<text|<math|x>
    basis>|)>=<big|prod><rsub|<text|B>><around|(|V<rsup|\<ast\>><A><rsup|\<nosymbol\>>*c<st>|)><rsup|\<nu\><B>>*K<around|(|<text|<math|c>
    basis>|)>=<big|prod><rsub|<text|B>><around|(|M<A>*m<st>|)><rsup|\<nu\><B>>*K<around|(|<text|<math|m>
    basis>|)>
  </equation>

  For a given reaction at a given temperature, and with a given choice of
  solute standard state, the value of <math|K> is not affected by pressure or
  dilution. The relations of Eq. <reference|K(x basis)=.=.> are therefore
  valid under all conditions.<index-complex|<tuple|reaction|solution>||c11
  sec tec-reaction-solution idx1|<tuple|Reaction|in solution>>

  <subsection|Evaluation of <math|K>><label|11-eval of K><label|c11 sec
  tec-evaluation-k>

  The relation <math|K=exp <space|0.17em><around|(|-\<Delta\><rsub|<text|r>>*G<st>/<around*|(|R*T|)>|)>>
  (Eq. <reference|K=exp(-del(r)Gmo/RT)>) gives us a way to evaluate the
  thermodynamic equilibrium constant <math|K> of a reaction at a given
  temperature from the value of the standard molar reaction Gibbs energy
  <math|\<Delta\><rsub|<text|r>>*G<st>> at that temperature. If we know the
  value of <math|\<Delta\><rsub|<text|r>>*G<st>>, we can calculate the value
  of <math|K>.

  One method is to calculate <math|\<Delta\><rsub|<text|r>>*G<st>> from
  values of the <index-complex|<tuple|gibbs energy|formation
  standard>|||<tuple|Gibbs energy|of formation, standard
  molar>><newterm|standard molar Gibbs energy of formation>
  <math|\<Delta\><rsub|<text|f>>*G<st>> of each reactant and product. These
  values are the standard molar reaction Gibbs energies for the formation
  reactions of the substances. To relate <math|\<Delta\><rsub|<text|f>>*G<st>>
  to measurable quantities, we make the substitution
  <math|\<mu\><rsub|i>=H<rsub|i>-T*S<rsub|i>> (Eq. <reference|mu_i=H_i-TS_i>)
  in <math|\<Delta\><rsub|<text|r>>*G=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i>>
  to give <math|\<Delta\><rsub|<text|r>>*G=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*H<rsub|i>-T*<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*S<rsub|i>>,
  or

  <\equation>
    <label|del(r)Gm=del(r)Hm-Tdel(r)Sm>\<Delta\><rsub|<text|r>>*G=\<Delta\><rsub|<text|r>>*H-T*\<Delta\><rsub|<text|r>>*S
  </equation>

  When we apply this equation to a reaction with each reactant and product in
  its standard state, it becomes

  <\equation>
    <label|del(r)Gmo=del(r)Hmo-Tdel(r)Smo>\<Delta\><rsub|<text|r>>*G<st>=\<Delta\><rsub|<text|r>>*H<st>-T*\<Delta\><rsub|<text|r>>*S<st>
  </equation>

  where the standard molar reaction entropy is given by

  <\equation>
    <label|del(r)Smo=sum(nu_i)Smio>\<Delta\><rsub|<text|r>>*S<st>=<big|sum><rsub|i>\<nu\><rsub|i>*S<rsub|i><st>
  </equation>

  If the reaction is the <em|formation> reaction of a substance, we have

  <\equation>
    <label|del(f)Gmo=del(f)Hmo-Tdel(f)Smo>\<Delta\><rsub|<text|f>>*G<st>=\<Delta\><rsub|<text|f>>*H<st>-T*<big|sum><rsub|i>\<nu\><rsub|i>*S<rsub|i><st>
  </equation>

  where the sum over <math|i> is for the reactants and product of the
  formation reaction. We can evaluate the standard molar Gibbs energy of
  formation of a substance, then, from its standard molar enthalpy of
  formation and the standard molar entropies of the reactants and product.

  Extensive tables are available of values of
  <math|\<Delta\><rsub|<text|f>>*G<st>> for substances and ions. An
  abbreviated version at the single temperature <math|298.15<K>> is given in
  Appendix <reference|app:props>. For a reaction of interest, the tabulated
  values enable us to evaluate <math|\<Delta\><rsub|<text|r>>*G<st>>, and
  then <math|K>, from the expression (analogous to Hess's law)

  <\equation>
    <label|del(r)Gmo=sum(nu_i)del(f)Gmio>\<Delta\><rsub|<text|r>>*G<st>=<big|sum><rsub|i>\<nu\><rsub|i>*\<Delta\><rsub|<text|f>>*G<st><around|(|i|)>
  </equation>

  The sum over <math|i> is for the reactants and products of the reaction of
  interest.

  Recall that the standard molar enthalpies of formation needed in Eq.
  <reference|del(f)Gmo=del(f)Hmo-Tdel(f)Smo> can be evaluated by calorimetric
  methods (Sec. <reference|11-st molar enthalpy of formation>). The absolute
  molar entropy values <math|S<rsub|i><st>> come from heat capacity data or
  statistical mechanical theory by methods discussed in Sec.
  <reference|6-molar entropies>. Thus, it is entirely feasible to use nothing
  but <index-complex|<tuple|calorimetry|evaluate an equilibrium
  constant>|||<tuple|Calorimetry|to evaluate an equilibrium
  constant>>calorimetry to evaluate an equilibrium constant, a goal sought by
  thermodynamicists during the first half of the 20th
  century.<footnote|Another method, for a reaction that can be carried out
  reversibly in a galvanic cell, is described in Sec. <reference|14-st molar
  rxn quantities>.>

  <index-complex|<tuple|gibbs energy|formation standard|ion>|||<tuple|Gibbs
  energy|of formation, standard molar|of an ion>>For <em|ions in aqueous
  solution>, the values of <math|S<m><st>> and
  <math|\<Delta\><rsub|<text|f>>*G<st>> found in Appendix
  <reference|app:props> are based on the reference values <math|S<m><st|=>=0>
  and <math|\<Delta\><rsub|<text|f>>*G<st|=>=0> for H<rsup|<math|+>>(aq) at
  all temperatures, similar to the convention for
  <math|\<Delta\><rsub|<text|f>>*H<st>> values discussed in Sec.
  <reference|11-st molar enthalpy of formation>.<footnote|Note that the
  values of <math|S<m><st>> in Appendix <reference|app:props> for some ions,
  unlike the values for substances, are <em|negative>; this simply means that
  the standard molar entropies of these ions are less than that of
  H<rsup|<math|+>>(aq).> For a reaction with aqueous ions as reactants or
  products, these values correctly give <math|\<Delta\><rsub|<text|r>>*S<st>>
  using Eq. <reference|del(r)Smo=sum(nu_i)Smio>, or
  <math|\<Delta\><rsub|<text|r>>*G<st>> using Eq.
  <reference|del(r)Gmo=sum(nu_i)del(f)Gmio>.

  <\quote-env>
    The relation of Eq. <reference|del(f)Gmo=del(f)Hmo-Tdel(f)Smo> does not
    apply to an ion, because we cannot write a formation reaction for a
    single ion. Instead, the relation between
    <math|\<Delta\><rsub|<text|f>>*G<st>>,
    <math|\<Delta\><rsub|<text|f>>*H<st>> and <math|S<m><st>> is more
    complicated.

    Consider first a hypothetical reaction in which hydrogen ions and one or
    more elements form H<rsub|<math|2>> and a cation M<rsup|<math|z<rsub|+>>>
    with charge number <math|z<rsub|+>>:

    <\equation*>
      z<rsub|+><text|H><rsup|<math|+>><around|(|aq|)>+<text|elements><arrow><around|(|z<rsub|+>/2|)>*<text|H><rsub|<math|2>><around|(|g|)>+<text|M><rsup|<math|z<rsub|+>>><around|(|aq|)>
    </equation*>

    For this reaction, using the convention that
    <math|\<Delta\><rsub|<text|f>>*H<st>>, <math|S<m><st>>, and
    <math|\<Delta\><rsub|<text|f>>*G<st>> are zero for the aqueous
    H<rsup|<math|+>> ion and the fact that
    <math|\<Delta\><rsub|<text|f>>*H<st>> and
    <math|\<Delta\><rsub|<text|f>>*G<st>> are zero for the elements, we can
    write the following expressions for standard molar reaction quantities:

    <\eqnarray*>
      <tformat|<table|<row|<cell|\<Delta\><rsub|<text|r>>*H<st>>|<cell|=>|<cell|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|M><rsup|<math|z<rsub|+>>>|)><eq-number>>>|<row|<cell|\<Delta\><rsub|<text|r>>*S<st>>|<cell|=>|<cell|<around|(|z<rsub|+>/2|)>*S<m><st><around|(|<text|H><rsub|<math|2>>|)>+S<m><st><around|(|<text|M><rsup|<math|z<rsub|+>>>|)>-<space|-0.17em><big|sum><rsub|<text|elements>><space|-0.17em><space|-0.17em><space|-0.17em>S<rsub|i><st><eq-number>>>|<row|<cell|\<Delta\><rsub|<text|r>>*G<st>>|<cell|=>|<cell|\<Delta\><rsub|<text|f>>*G<st><around|(|<text|M><rsup|<math|z<rsub|+>>>|)><eq-number>>>>>
    </eqnarray*>

    Then, from <math|\<Delta\><rsub|<text|r>>*G<st>=\<Delta\><rsub|<text|r>>*H<st>-T\<Delta\><rsub|<text|r>>*S<st>>,
    we find

    <\eqnarray*>
      <tformat|<table|<row|<cell|\<Delta\><rsub|<text|f>>*G<st><around|(|<text|M><rsup|<math|z<rsub|+>>>|)>>|<cell|=>|<cell|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|M><rsup|<math|z<rsub|+>>>|)>>>|<row|<cell|>|<cell|>|<cell|-T*<around*|[|S<m><st><around|(|<text|M><rsup|<math|z<rsub|+>>>|)>-<big|sum><rsub|<text|elements>><space|-0.17em><space|-0.17em><space|-0.17em>S<rsub|i><st>+<around|(|z<rsub|+>/2|)>*S<m><st><around|(|<text|H><rsub|<math|2>>|)>|]><eq-number>>>>>
    </eqnarray*>

    For example, the standard molar Gibbs energy of the aqueous mercury(I)
    ion is found from

    <\eqnarray*>
      <tformat|<table|<row|<cell|\<Delta\><rsub|<text|f>>*G<st><around|(|<chem|<text|Hg<rsub|2>>|2+>|)>>|<cell|=>|<cell|\<Delta\><rsub|<text|f>>*H<st><around|(|<chem|<text|Hg<rsub|2>>|2+>|)>-T*S<m><st><around|(|<chem|<text|Hg<rsub|2>>|2+>|)>>>|<row|<cell|>|<cell|>|<cell|+2*T*S<m><st><around|(|<text|Hg>|)>-<frac|2|2>*T*S<m><st><around|(|<text|H<rsub|2>>|)><eq-number>>>>>
    </eqnarray*>

    For an anion X<rsup|<math|z<rsub|->>> with negative charge number
    <math|z<rsub|->>, using the hypothetical reaction

    <\equation*>
      <around|\||z<rsub|->/2|\|>*<text|H<rsub|2>><around|(|g|)>+<text|elements><arrow><around|\||z<rsub|->|\|>*<text|H><rsup|+><around|(|aq|)>+<text|X><rsup|<math|z<rsub|->>><around|(|aq|)>
    </equation*>

    we find by the same method

    <\equation>
      <\eqsplit>
        <tformat|<table|<row|<cell|\<Delta\><rsub|<text|f>>*G<st><around|(|<text|X><rsup|<math|z<rsub|->>>|)>>|<cell|=\<Delta\><rsub|<text|f>>*H<st><around|(|<text|X><rsup|<math|z<rsub|->>>|)>>>|<row|<cell|>|<cell|<space|1em>-T*<around*|[|S<m><st><around|(|<text|X><rsup|<math|z<rsub|->>>|)>-<space|-0.17em><big|sum><rsub|<text|elements>><space|-0.17em><space|-0.17em><space|-0.17em>S<rsub|i><st>-<around|\||z<rsub|->/2|\|>*<space|0.17em>S<m><st><around|(|<text|H<rsub|2>>|)>|]>>>>>
      </eqsplit>
    </equation>

    For example, the calculation for the nitrate ion is

    <\equation>
      <\eqsplit>
        <tformat|<table|<row|<cell|\<Delta\><rsub|<text|f>>*G<st><around|(|<chem|<text|NO<rsub|3>>|->|)>>|<cell|=\<Delta\><rsub|<text|f>>*H<st><around|(|<text|<chem|NO<rsub|3>|->>|)>-T*S<m><st><around|(|<chem|<text|NO<rsub|3>>|->|)>>>|<row|<cell|>|<cell|<with|math-display|false|<space|1em>+<frac|1|2>*T*S<m><st><around|(|<text|N<rsub|2>>|)>+<frac|3|2>*T*S<m><st><around|(|<text|O<rsub|2>>|)>+<frac|1|2>*T*S<m><st><around|(|<text|H><rsub|2>|)>>>>>>
      </eqsplit>
    </equation>
  </quote-env>

  <section|Effects of Temperature and Pressure on Equilibrium
  Position><label|11-effects of T and p><label|c11 sec etpep>

  <index-complex|<tuple|equilibrium|position, effect of T and p on>||c11 sec
  etpep idx1|<tuple|Equilibrium|position, effect of <math|T> and <math|p>
  on>>The advancement <math|\<xi\>> of a chemical reaction in a closed system
  describes the changes in the amounts of the reactants and products from
  specified initial values of these amounts. We have seen that if the system
  is maintained at constant temperature and pressure, <math|\<xi\>> changes
  spontaneously in the direction that decreases the Gibbs energy. The change
  continues until the system reaches a state of reaction equilibrium at the
  minimum of <math|G>. The value of the advancement in this equilibrium state
  will be denoted <math|\<xi\><eq>>, as shown in Fig. <vpageref|fig:11-G vs
  xi>. The value of <math|\<xi\><eq>> depends in general on the values of
  <math|T> and <math|p>. Thus when we change the temperature or pressure of a
  closed system that is at equilibrium, <math|\<xi\><eq>> usually changes
  also and the reaction spontaneously <em|shifts> to a new equilibrium
  position.

  To investigate this effect, we write the total differential of <math|G>
  with <math|T>, <math|p>, and <math|\<xi\>> as independent variables

  <\equation>
    <label|dG=.dT+.dp+.dxi><dif>G=-S*<dif>T+V*<difp>+\<Delta\><rsub|<text|r>>*G*<dif>\<xi\>
  </equation>

  and obtain the reciprocity relations

  <\equation>
    <label|dDel(r)G/dT=,dDel(r)G/dp=><Pd|\<Delta\><rsub|<text|r>>*G|T|p,<space|0.17em>\<xi\>>=-<Pd|S|\<xi\>|T,p><space|2em><Pd|\<Delta\><rsub|<text|r>>*G|p|T,<space|0.17em>\<xi\>>=<Pd|V|\<xi\>|T,p>
  </equation>

  We recognize the partial derivative on the right side of each of these
  relations as a molar differential reaction quantity:

  <\equation>
    <Pd|\<Delta\><rsub|<text|r>>*G|T|p,<space|0.17em>\<xi\>>=-\<Delta\><rsub|<text|r>>*S<space|2em><Pd|\<Delta\><rsub|<text|r>>*G|p|T,<space|0.17em>\<xi\>>=\<Delta\><rsub|<text|r>>*V
  </equation>

  We use these expressions for two of the coefficients in an expression for
  the total differential of <math|\<Delta\><rsub|<text|r>>*G>:

  <equation-cov2|<label|ddel(r)Gm=()dT+()dp+()dxi><dif>\<Delta\><rsub|<text|r>>*G=-\<Delta\><rsub|<text|r>>*S*<dif>T+\<Delta\><rsub|<text|r>>*V*<difp>+<Pd|\<Delta\><rsub|<text|r>>*G|\<xi\>|T,p><dif>\<xi\>|(closed
  system)>

  Since <math|\<Delta\><rsub|<text|r>>*G> is the partial derivative of
  <math|G> with respect to <math|\<xi\>> at constant <math|T> and <math|p>,
  the coefficient <math|<pd|\<Delta\><rsub|<text|r>>*G|\<xi\>|T,p>> is the
  partial <em|second> derivative of <math|G> with respect to <math|\<xi\>>:

  <\equation>
    <Pd|\<Delta\><rsub|<text|r>>*G|\<xi\>|T,p>=<Pd|<rsup|2>G|\<xi\><rsup|2>|T,p>
  </equation>

  We know that at a fixed <math|T> and <math|p>, a plot of <math|G> versus
  <math|\<xi\>> has a slope at each point equal to
  <math|\<Delta\><rsub|<text|r>>*G> and a minimum at the position of reaction
  equilibrium where <math|\<xi\>> is <math|\<xi\><eq>>. At the minimum of the
  plotted curve, the slope <math|\<Delta\><rsub|<text|r>>*G> is zero and the
  second derivative is positive (see Fig. <vpageref|fig:11-G vs xi>). By
  setting <math|\<Delta\><rsub|<text|r>>*G> equal to zero in the general
  relation <math|\<Delta\><rsub|<text|r>>*G=\<Delta\><rsub|<text|r>>*H-T\<Delta\><rsub|<text|r>>*S>,
  we obtain the equation <math|\<Delta\><rsub|<text|r>>*S=\<Delta\><rsub|<text|r>>*H/T>
  which is valid only at reaction equilibrium where <math|\<xi\>> equals
  <math|\<xi\><eq>>. Making this substitution in Eq.
  <reference|ddel(r)Gm=()dT+()dp+()dxi>, and setting
  <math|<dif>\<Delta\><rsub|<text|r>>*G> equal to zero and <math|<dif>\<xi\>>
  equal to <math|<dif>\<xi\><eq>>, we obtain

  <equation-cov2|<label|0=()dT+()dp+()dxi(eq)>0=-<frac|\<Delta\><rsub|<text|r>>*H|T>*<dif>T+\<Delta\><rsub|<text|r>>*V*<difp>+<Pd|<rsup|2>G|\<xi\><rsup|2>|T,p>*<dif>\<xi\><eq>|(closed
  system)>

  which shows how infinitesimal changes in <math|T>, <math|p>, and
  <math|\<xi\><eq>> are related.

  Now we are ready to see how <math|\<xi\><eq>> is affected by changes in
  <math|T> or <math|p>. Solving Eq. <reference|0=()dT+()dp+()dxi(eq)> for
  <math|<dif>\<xi\><eq>> gives

  <equation-cov2|<label|dxi(eq)=><dif>\<xi\><eq>=<frac|<with|math-display|true|<frac|\<Delta\><rsub|<text|r>>*H|T>>*<dif>T-\<Delta\><rsub|<text|r>>*V*<difp>|<Pd|<rsup|2>G|\<xi\><rsup|2>|T,p>>|(closed
  system)>

  The right side of Eq. <reference|dxi(eq)=> is the expression for the total
  differential of <math|\<xi\>> in a closed system at reaction equilibrium,
  with <math|T> and <math|p> as the independent variables. Thus, at constant
  pressure the equilibrium shifts with temperature according to

  <equation-cov2|<label|dxi(eq)/dT=><Pd|\<xi\><eq>|T|<space|-0.17em>p>=<frac|<space|-0.17em><space|-0.17em><space|-0.17em>\<Delta\><rsub|<text|r>>*H|T<Pd|<rsup|2>G|\<xi\><rsup|2>|T,p>>|(closed
  system)>

  and at constant temperature the equilibrium shifts with pressure according
  to

  <equation-cov2|<label|dxi(eq)/dp=><Pd|\<xi\><eq>|p|T>=-<frac|<space|-0.17em><space|-0.17em><space|-0.17em><space|-0.17em><space|-0.17em>\<Delta\><rsub|<text|r>>*V|<Pd|<rsup|2>G|\<xi\><rsup|2>|T,p>>|(closed
  system)>

  Because the partial second derivative <math|<pd|<rsup|2>G|\<xi\><rsup|2>|T,p>>
  is positive, Eqs. <reference|dxi(eq)/dT=> and <reference|dxi(eq)/dp=> show
  that <math|<pd|\<xi\><eq>|T|p>> and <math|\<Delta\><rsub|<text|r>>*H> have
  the same sign, whereas <math|<pd|\<xi\><eq>|p|T>> and
  <math|\<Delta\><rsub|<text|r>>*V> have opposite signs.

  These statements express the application to temperature and pressure
  changes of what is known as <index-complex|<tuple|le
  chatelier's>|||<tuple|Le Ch�telier's principle>><em|Le<nbsp>Ch�telier's
  principle>: When a change is made to a closed system at equilibrium, the
  equilibrium shifts in the direction that tends to oppose the change. Here
  are two examples.

  <\enumerate>
    <item>Suppose <math|\<Delta\><rsub|<text|r>>*H> is negative\Vthe reaction
    is exothermic. Since <math|<pd|\<xi\><eq>|T|p>> has the same sign as
    <math|\<Delta\><rsub|<text|r>>*H>, an increase in temperature causes
    <math|\<xi\><eq>> to decrease: the equilibrium shifts to the left. This
    is the shift that would reduce the temperature if the reaction were
    adiabatic.

    <item>If <math|\<Delta\><rsub|<text|r>>*V> is positive, the volume
    increases as the reaction proceeds to the right at constant <math|T> and
    <math|p>. <math|<pd|\<xi\><eq>|p|T>> has the opposite sign, so if we
    increase the pressure isothermally by reducing the volume, the
    equilibrium shifts to the left. This is the shift that would reduce the
    pressure if the reaction occurred at constant <math|T> and <math|V>.
  </enumerate>

  It is easy to misuse or to be misled by Le<nbsp>Ch�telier's principle.
  Consider the solution process B<rsup|<math|\<ast\>>>(s)<math|<arrow>>B(sln)
  for which <math|<pd|\<xi\><eq>|T|p>>, the rate of change of solubility with
  <math|T>, has the same sign as the molar differential enthalpy of solution
  <math|\<Delta\><rsub|<text|sol>>*H> at saturation.<label|d
  xi(sol),eq)/dT>The sign of <math|\<Delta\><rsub|<text|sol>>*H> at
  saturation may be different from the sign of the
  <index-complex|<tuple|enthalpy|solution|molar
  integral>|||<tuple|Enthalpy|of solution|molar integral>>molar <em|integral>
  enthalpy of solution, <math|<Del>H<m><sol>>. This is the situation for the
  dissolution of sodium acetate shown in Fig. <vpageref|fig:11-Na acetate>.
  The equilibrium position (saturation) with one kilogram of water is at
  <math|\<xi\><rsub|<text|sol>>\<approx\>15<mol>>, indicated in the figure by
  an open circle. At this position, <math|\<Delta\><rsub|<text|sol>>*H> is
  positive and <math|<Del>H<m><sol>> is negative. So, despite the fact that
  the dissolution of 15 moles of sodium acetate in one kilogram of water to
  form a saturated solution is an exothermic process, the solubility of
  sodium acetate actually <em|increases> with increasing temperature,
  contrary to what one might predict from Le<nbsp>Ch�telier's
  principle.<footnote|Ref. <cite|brice-83>.>

  Another kind of change for which <index-complex|<tuple|le
  chatelier's>|||<tuple|Le Ch�telier's principle>>Le<nbsp>Ch�telier's
  principle gives an incorrect prediction is the addition of an inert gas to
  a gas mixture of constant volume. Adding the inert gas at constant <math|V>
  increases the pressure, but has little effect on the equilibrium position
  of a gas-phase reaction regardless of the value of
  <math|\<Delta\><rsub|<text|r>>*V>. This is because the inert gas affects
  the activities of the reactants and products only slightly, and not at all
  if the gas mixture is ideal, so there is little or no effect on the value
  of <math|Q<rsub|<text|rxn>>>. (Note that the dependence of
  <math|\<xi\><eq>> on <math|p> expressed by Eq. <reference|dxi(eq)/dp=> does
  not apply to an open system.)

  The rigorous criterion for the equilibrium position of a reaction is always
  the requirement that <math|Q<rsub|<text|rxn>>> must equal <math|K> or,
  equivalently, that <math|\<Delta\><rsub|<text|r>>*G> must be
  zero.<index-complex|<tuple|equilibrium|position, effect of T and p on>||c11
  sec etpep idx1|<tuple|Equilibrium|position, effect of <math|T> and <math|p>
  on>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|10>
    <associate|page-first|249>
    <associate|preamble|false>
    <associate|section-nr|7>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|0=()dT+()dp+()dxi(eq)|<tuple|11.9.6|44>>
    <associate|11-NH3 example|<tuple|11.2.1|9>>
    <associate|11-bomb calorimeter|<tuple|11.5.2|27>>
    <associate|11-constant-pressure calorimeter|<tuple|11.5.1|26>>
    <associate|11-effects of T and p|<tuple|11.9|43>>
    <associate|11-enthalpies of soln and diln|<tuple|11.4|19>>
    <associate|11-enthalpy of sln|<tuple|11.4.1|19>>
    <associate|11-eq constant defn|<tuple|11.8.1|38>>
    <associate|11-eval of K|<tuple|11.8.4|41>>
    <associate|11-eval of partial molar H|<tuple|11.4.4|22>>
    <associate|11-excess quantities|<tuple|11.1.3|3>>
    <associate|11-gen rxn eqm|<tuple|11.7.3|34>>
    <associate|11-ideal mixts|<tuple|11.1.2|2>>
    <associate|11-mixing|<tuple|11.1|1>>
    <associate|11-mixing in general|<tuple|11.1.1|1>>
    <associate|11-mol model of id mixt|<tuple|11.1.5|5>>
    <associate|11-molar rxn Gibbs energy|<tuple|11.7.1|33>>
    <associate|11-molar rxn quantities in general|<tuple|11.2.2|11>>
    <associate|11-phase sep|<tuple|11.1.6|7>>
    <associate|11-pure phases|<tuple|11.7.4|34>>
    <associate|11-rxn calorimetry|<tuple|11.5|26>>
    <associate|11-rxn in gas phase|<tuple|11.8.2|40>>
    <associate|11-rxn in solution|<tuple|11.8.3|41>>
    <associate|11-rxns involving mixtures|<tuple|11.7.5|35>>
    <associate|11-solute formation|<tuple|11.4.3|21>>
    <associate|11-spont and eqm|<tuple|11.7.2|33>>
    <associate|11-st molar enthalpy of formation|<tuple|11.3.2|14>>
    <associate|11-st molar rxn quantities|<tuple|11.2.3|13>>
    <associate|2-par Redlich-Kister|<tuple|11.1.36|7>>
    <associate|3steps|<tuple|11.5.8|30>>
    <associate|C=|<tuple|11.4.29|25>>
    <associate|Chap. 11|<tuple|11|1>>
    <associate|Del S (A+B)|<tuple|11.1.26|5>>
    <associate|Del X_m(id)(mix)=|<tuple|11.1.6|2>>
    <associate|Del X_m(mix)=|<tuple|11.1.5|2>>
    <associate|Del(IBP)U(Tref)=|<tuple|11.5.8|30>>
    <associate|Del(r)G=Del(r)o+RTsum|<tuple|11.8.4|39>>
    <associate|Del(r)G=sum+RTsum|<tuple|11.8.2|38>>
    <associate|Del(r)G^o=sum|<tuple|11.7.18|36>>
    <associate|Del(r)H(T'')=|<tuple|11.3.11|17>>
    <associate|DelGm(mix)=sum(x_i)(mu_i-mu_i*)|<tuple|11.1.4|2>>
    <associate|DelH(sol,mB'')=.+.|<tuple|11.4.10|21>>
    <associate|DelHm(dil,m'-\<gtr\>m'')=|<tuple|11.4.12|22>>
    <associate|DelHom=DelUom+sum nu_i(g)RT|<tuple|11.5.13|31>>
    <associate|DelU(IBP)=|<tuple|11.5.6|30>>
    <associate|DelU(IBP)=-e(T2-T1)+w(ign)+DelU'|<tuple|11.5.7|30>>
    <associate|DelU(R)=|<tuple|11.5.5|29>>
    <associate|DelU(expt)=|<tuple|11.5.4|29>>
    <associate|DelUo=DelU(IBP)+...|<tuple|11.5.9|31>>
    <associate|DelUom=DelUo/xi|<tuple|11.5.10|31>>
    <associate|G(xi)-G(0)|<tuple|11.7.17|36>>
    <associate|G(xi)-G(0)=|<tuple|11.7.18|36>>
    <associate|G(xi)-G(0)= (IG)|<tuple|11.7.19|36>>
    <associate|G=sum(n_i)(mu_i)|<tuple|11.7.13|36>>
    <associate|Gm(E)=RT sum[x(i)ln(gamma(i)]|<tuple|11.1.19|4>>
    <associate|Gm^E defn|<tuple|11.1.14|4>>
    <associate|H(xi_1)=|<tuple|11.3.8|17>>
    <associate|H(xi_2)=|<tuple|11.3.7|17>>
    <associate|Hess's law|<tuple|11.3.3|15>>
    <associate|K(x basis)=.=.|<tuple|11.8.19|41>>
    <associate|K=exp(-del(r)Gmo/RT)|<tuple|11.8.11|40>>
    <associate|K=prod(a_i)^(nu_i)|<tuple|11.8.9|39>>
    <associate|K=prod(phi_i^(nu_i)x...|<tuple|11.8.12|40>>
    <associate|Kirchhoff eq-1|<tuple|11.3.9|17>>
    <associate|Kirchhoff eq-2|<tuple|11.3.10|17>>
    <associate|Kp=prod(p_i)^(nu_i)|<tuple|11.8.13|41>>
    <associate|L(A) defn|<tuple|11.4.13|22>>
    <associate|L(B) defn|<tuple|11.4.17|22>>
    <associate|L(B)=-RT^2..|<tuple|11.4.32|25>>
    <associate|L(B)=..|<tuple|11.4.21|23>>
    <associate|L(B)=...|<tuple|11.4.23|23>>
    <associate|L(B)=....|<tuple|11.4.27|24>>
    <associate|L(B)=.=.|<tuple|11.4.35|25>>
    <associate|L(B)=C..|<tuple|11.4.34|25>>
    <associate|L(B)=RT^2...|<tuple|11.4.33|25>>
    <associate|L_A=M_A mB(Phi_L-L_B)|<tuple|11.4.24|24>>
    <associate|L_A=M_A*mB[]|<tuple|11.4.16|22>>
    <associate|L_B=Del(sol)H-Del(sol)H^infty|<tuple|11.4.19|23>>
    <associate|Phi(L) defn|<tuple|11.4.22|23>>
    <associate|Phi(L) dil sln|<tuple|11.4.28|24>>
    <associate|Phi(L)(mB'')-Phi(L)(mB')=|<tuple|11.4.25|24>>
    <associate|Q=prod(a_i)^(nu_i)|<tuple|11.8.6|39>>
    <associate|S_i*=|<tuple|11.1.25|5>>
    <associate|S_i=|<tuple|11.1.24|4>>
    <associate|U(mixt)|<tuple|11.1.29|6>>
    <associate|U_B(molecular model)|<tuple|11.1.33|6>>
    <associate|U_B=UBo(g)+k(BB)|<tuple|11.1.28|6>>
    <associate|Xm(E)=|<tuple|11.1.13|3>>
    <associate|a(B) on x,c,m basis|<tuple|11.8.18|41>>
    <associate|a(x,B)= etc|<tuple|11.8.16|41>>
    <associate|ad. flame|<tuple|11.6.1|32>>
    <associate|auto-1|<tuple|11|1>>
    <associate|auto-10|<tuple|11.1.1|1>>
    <associate|auto-100|<tuple|Calorimeter|15>>
    <associate|auto-101|<tuple|Bomb calorimeter|15>>
    <associate|auto-102|<tuple|Hess's law|15>>
    <associate|auto-103|<tuple|Stoichiometric|15>>
    <associate|auto-104|<tuple|Enthalpy|15>>
    <associate|auto-105|<tuple|<tuple|enthalpy|formation
    standard|solute>|16>>
    <associate|auto-106|<tuple|<tuple|enthalpy|formation standard|ion>|16>>
    <associate|auto-107|<tuple|Calorimetry|16>>
    <associate|auto-108|<tuple|11.3.3|16>>
    <associate|auto-109|<tuple|Heat capacity|16>>
    <associate|auto-11|<tuple|Additivity rule|1>>
    <associate|auto-110|<tuple|11.3.4|17>>
    <associate|auto-111|<tuple|<tuple|enthalpy|molar, effect of temperature
    on>|17>>
    <associate|auto-112|<tuple|Kirchhoff equation|17>>
    <associate|auto-113|<tuple|Kirchhoff equation|17>>
    <associate|auto-114|<tuple|11.3.1|17>>
    <associate|auto-115|<tuple|<tuple|enthalpy|molar, effect of temperature
    on>|17>>
    <associate|auto-116|<tuple|1|18|bio-HESS.tm>>
    <associate|auto-117|<tuple|Hess, Germain|18|bio-HESS.tm>>
    <associate|auto-118|<tuple|11.4|19>>
    <associate|auto-119|<tuple|IUPAC Green Book|19>>
    <associate|auto-12|<tuple|<tuple|gibbs energy|mixing>|1>>
    <associate|auto-120|<tuple|Solution|19>>
    <associate|auto-121|<tuple|Process|19>>
    <associate|auto-122|<tuple|solution process|19>>
    <associate|auto-123|<tuple|Dilution process|19>>
    <associate|auto-124|<tuple|Process|19>>
    <associate|auto-125|<tuple|dilution process|19>>
    <associate|auto-126|<tuple|11.4.1|19>>
    <associate|auto-127|<tuple|11.4.1|19>>
    <associate|auto-128|<tuple|<tuple|enthalpy|solution|molar
    differential>|19>>
    <associate|auto-129|<tuple|molar differential enthalpy of solution|19>>
    <associate|auto-13|<tuple|Gibbs energy of mixing|1>>
    <associate|auto-130|<tuple|<tuple|enthalpy|solution|infinite
    dilution>|19>>
    <associate|auto-131|<tuple|molar enthalpy of solution at infinite
    dilution|19>>
    <associate|auto-132|<tuple|Integral enthalpy of solution|20>>
    <associate|auto-133|<tuple|<tuple|enthalpy|solution|integral>|20>>
    <associate|auto-134|<tuple|integral enthalpy of solution|20>>
    <associate|auto-135|<tuple|<tuple|enthalpy|solution|molar integral>|20>>
    <associate|auto-136|<tuple|molar integral enthalpy of solution|20>>
    <associate|auto-137|<tuple|11.4.2|20>>
    <associate|auto-138|<tuple|<tuple|enthalpy|solution|molar
    differential>|20>>
    <associate|auto-139|<tuple|11.4.2|20>>
    <associate|auto-14|<tuple|<tuple|gibbs energy|mixing|molar>|2>>
    <associate|auto-140|<tuple|<tuple|enthalpy|dilution|molar
    differential>|20>>
    <associate|auto-141|<tuple|molar differential enthalpy of dilution|20>>
    <associate|auto-142|<tuple|Integral enthalpy of dilution|21>>
    <associate|auto-143|<tuple|<tuple|enthalpy|dilution|integral>|21>>
    <associate|auto-144|<tuple|integral enthalpy of dilution|21>>
    <associate|auto-145|<tuple|<tuple|enthalpy|dilution|molar integral>|21>>
    <associate|auto-146|<tuple|molar integral enthalpy of dilution|21>>
    <associate|auto-147|<tuple|11.4.3|21>>
    <associate|auto-148|<tuple|<tuple|enthalpy|formation of a solute>|21>>
    <associate|auto-149|<tuple|<tuple|enthalpy|solution|molar integral>|22>>
    <associate|auto-15|<tuple|molar Gibbs energy of mixing|2>>
    <associate|auto-150|<tuple|11.4.4|22>>
    <associate|auto-151|<tuple|<tuple|relative partial molar
    enthalpy|solvent>|22>>
    <associate|auto-152|<tuple|Partial molar|22>>
    <associate|auto-153|<tuple|Enthalpy|22>>
    <associate|auto-154|<tuple|relative partial molar enthalpy of the
    solvent|22>>
    <associate|auto-155|<tuple|Additivity rule|22>>
    <associate|auto-156|<tuple|<tuple|relative partial molar
    enthalpy|solute>|22>>
    <associate|auto-157|<tuple|Partial molar|22>>
    <associate|auto-158|<tuple|Enthalpy|22>>
    <associate|auto-159|<tuple|relative partial molar enthalpy of a
    solute|22>>
    <associate|auto-16|<tuple|11.1.2|2>>
    <associate|auto-160|<tuple|Relative apparent molar enthalpy of a
    solute|23>>
    <associate|auto-161|<tuple|Enthalpy|23>>
    <associate|auto-162|<tuple|Debye\UH�ckel|24>>
    <associate|auto-163|<tuple|11.4.3|24>>
    <associate|auto-164|<tuple|Debye\UH�ckel|25>>
    <associate|auto-165|<tuple|11.5|26>>
    <associate|auto-166|<tuple|<tuple|calorimetry|reaction>|26>>
    <associate|auto-167|<tuple|Calorimeter|26>>
    <associate|auto-168|<tuple|Calorimeter|26>>
    <associate|auto-169|<tuple|Bomb calorimeter|26>>
    <associate|auto-17|<tuple|Ideal mixture|2>>
    <associate|auto-170|<tuple|11.5.1|26>>
    <associate|auto-171|<tuple|<tuple|calorimeter|reaction>|26>>
    <associate|auto-172|<tuple|Adiabatic|26>>
    <associate|auto-173|<tuple|Calorimeter|26>>
    <associate|auto-174|<tuple|Calorimeter|26>>
    <associate|auto-175|<tuple|Isoperibol calorimeter|26>>
    <associate|auto-176|<tuple|Calorimeter|26>>
    <associate|auto-177|<tuple|11.5.1|26>>
    <associate|auto-178|<tuple|Adiabatic|26>>
    <associate|auto-179|<tuple|Calorimeter|26>>
    <associate|auto-18|<tuple|<tuple|gibbs energy|mixing|ideal mixture>|2>>
    <associate|auto-180|<tuple|Energy equivalent|26>>
    <associate|auto-181|<tuple|Calorimeter|26>>
    <associate|auto-182|<tuple|Isoperibol calorimeter|26>>
    <associate|auto-183|<tuple|Calorimeter|26>>
    <associate|auto-184|<tuple|Energy equivalent|27>>
    <associate|auto-185|<tuple|<tuple|calorimeter|reaction>|27>>
    <associate|auto-186|<tuple|11.5.2|27>>
    <associate|auto-187|<tuple|<tuple|calorimeter|bomb>|27>>
    <associate|auto-188|<tuple|<tuple|bomb calorimeter>|27>>
    <associate|auto-189|<tuple|Calorimetry|27>>
    <associate|auto-19|<tuple|<tuple|entropy|mixing|form>|2>>
    <associate|auto-190|<tuple|<tuple|enthalpy|combustion>|27>>
    <associate|auto-191|<tuple|Hess's law|27>>
    <associate|auto-192|<tuple|Isothermal|28>>
    <associate|auto-193|<tuple|isothermal bomb process|28>>
    <associate|auto-194|<tuple|<tuple|enthalpy|combustion>|28>>
    <associate|auto-195|<tuple|Reduction to standard states|28>>
    <associate|auto-196|<tuple|reduction to standard states|28>>
    <associate|auto-197|<tuple|5|28>>
    <associate|auto-198|<tuple|11.5.2|28>>
    <associate|auto-199|<tuple|Ignition circuit|29>>
    <associate|auto-2|<tuple|Process|1>>
    <associate|auto-20|<tuple|<tuple|entropy|mixing|negative value>|2>>
    <associate|auto-200|<tuple|Circuit|29>>
    <associate|auto-201|<tuple|Circuit|29>>
    <associate|auto-202|<tuple|<tuple|isothermal|bomb process>|29>>
    <associate|auto-203|<tuple|11.5.3|29>>
    <associate|auto-204|<tuple|Energy equivalent|29>>
    <associate|auto-205|<tuple|Electrical|30>>
    <associate|auto-206|<tuple|Work|30>>
    <associate|auto-207|<tuple|Ignition circuit|30>>
    <associate|auto-208|<tuple|Circuit|30>>
    <associate|auto-209|<tuple|Energy equivalent|30>>
    <associate|auto-21|<tuple|<tuple|enthalpy|mixing>|2>>
    <associate|auto-210|<tuple|<tuple|isothermal|bomb process>|30>>
    <associate|auto-211|<tuple|<tuple|isothermal|bomb process>|30>>
    <associate|auto-212|<tuple|Kirchhoff equation|30>>
    <associate|auto-213|<tuple|11.5.8|30>>
    <associate|auto-214|<tuple|Washburn corrections|31>>
    <associate|auto-215|<tuple|Washburn corrections|31>>
    <associate|auto-216|<tuple|11.5.10|31>>
    <associate|auto-217|<tuple|<tuple|enthalpy|combustion>|31>>
    <associate|auto-218|<tuple|11.5.13|31>>
    <associate|auto-219|<tuple|<tuple|washburn corrections>|31>>
    <associate|auto-22|<tuple|<tuple|internal energy|mixing>|3>>
    <associate|auto-220|<tuple|Isothermal|31>>
    <associate|auto-221|<tuple|<tuple|washburn corrections>|32>>
    <associate|auto-222|<tuple|<tuple|calorimeter|bomb>|32>>
    <associate|auto-223|<tuple|<tuple|bomb calorimeter>|32>>
    <associate|auto-224|<tuple|11.5.3|32>>
    <associate|auto-225|<tuple|Calorimeter|32>>
    <associate|auto-226|<tuple|Calorimeter|32>>
    <associate|auto-227|<tuple|Calorimeter|32>>
    <associate|auto-228|<tuple|Calorimeter|32>>
    <associate|auto-229|<tuple|Isoperibol calorimeter|32>>
    <associate|auto-23|<tuple|Athermal process|3>>
    <associate|auto-230|<tuple|Calorimeter|32>>
    <associate|auto-231|<tuple|Thermopile|32>>
    <associate|auto-232|<tuple|Calorimeter|32>>
    <associate|auto-233|<tuple|<tuple|calorimetry|reaction>|32>>
    <associate|auto-234|<tuple|11.6|32>>
    <associate|auto-235|<tuple|<tuple|adiabatic|flame temperature>|32>>
    <associate|auto-236|<tuple|<tuple|adiabatic|flame temperature>|32>>
    <associate|auto-237|<tuple|11.7|33>>
    <associate|auto-238|<tuple|11.7.1|33>>
    <associate|auto-239|<tuple|Gibbs energy|33>>
    <associate|auto-24|<tuple|<tuple|volume|mixing>|3>>
    <associate|auto-240|<tuple|molar reaction Gibbs energy|33>>
    <associate|auto-241|<tuple|11.7.2|33>>
    <associate|auto-242|<tuple|Process|33>>
    <associate|auto-243|<tuple|Spontaneous process|33>>
    <associate|auto-244|<tuple|Affinity of reaction|33>>
    <associate|auto-245|<tuple|Equilibrium|33>>
    <associate|auto-246|<tuple|11.7.3|34>>
    <associate|auto-247|<tuple|<tuple|equilibrium conditions>|34>>
    <associate|auto-248|<tuple|11.7.4|34>>
    <associate|auto-249|<tuple|<tuple|reaction|between pure phases>|34>>
    <associate|auto-25|<tuple|11.1.2|3>>
    <associate|auto-250|<tuple|11.7.1|35>>
    <associate|auto-251|<tuple|Equilibrium|35>>
    <associate|auto-252|<tuple|Phase|35>>
    <associate|auto-253|<tuple|<tuple|reaction|between pure phases>|35>>
    <associate|auto-254|<tuple|11.7.5|35>>
    <associate|auto-255|<tuple|<tuple|reaction|mixture>|35>>
    <associate|auto-256|<tuple|11.7.2|35>>
    <associate|auto-257|<tuple|<tuple|reaction|mixture>|36>>
    <associate|auto-258|<tuple|11.7.6|36>>
    <associate|auto-259|<tuple|<tuple|reaction|ideal gas mixture>|36>>
    <associate|auto-26|<tuple|11.1.3|3>>
    <associate|auto-260|<tuple|Additivity rule|36>>
    <associate|auto-261|<tuple|11.7.3|37>>
    <associate|auto-262|<tuple|11.7.4|38>>
    <associate|auto-263|<tuple|<tuple|reaction|ideal gas mixture>|38>>
    <associate|auto-264|<tuple|<tuple|equilibrium conditions>|38>>
    <associate|auto-265|<tuple|11.8|38>>
    <associate|auto-266|<tuple|11.8.1|38>>
    <associate|auto-267|<tuple|Gibbs energy|39>>
    <associate|auto-268|<tuple|standard molar reaction Gibbs energy|39>>
    <associate|auto-269|<tuple|Reaction|39>>
    <associate|auto-27|<tuple|Excess|3>>
    <associate|auto-270|<tuple|Activity quotient|39>>
    <associate|auto-271|<tuple|reaction quotient|39>>
    <associate|auto-272|<tuple|Stoichiometric|39>>
    <associate|auto-273|<tuple|Proper quotient|39>>
    <associate|auto-274|<tuple|proper quotient|39>>
    <associate|auto-275|<tuple|Thermodynamic|39>>
    <associate|auto-276|<tuple|Equilibrium constant|39>>
    <associate|auto-277|<tuple|thermodynamic equilibrium constant|39>>
    <associate|auto-278|<tuple|IUPAC Green Book|40>>
    <associate|auto-279|<tuple|11.8.2|40>>
    <associate|auto-28|<tuple|excess quantity|3>>
    <associate|auto-280|<tuple|<tuple|reaction|gas phase>|40>>
    <associate|auto-281|<tuple|<tuple|equilibrium constant|pressure
    basis>|41>>
    <associate|auto-282|<tuple|equilibrium constant on a pressure basis|41>>
    <associate|auto-283|<tuple|<tuple|reaction|gas phase>|41>>
    <associate|auto-284|<tuple|11.8.3|41>>
    <associate|auto-285|<tuple|<tuple|reaction|solution>|41>>
    <associate|auto-286|<tuple|<tuple|reaction|solution>|41>>
    <associate|auto-287|<tuple|11.8.4|41>>
    <associate|auto-288|<tuple|<tuple|gibbs energy|formation standard>|42>>
    <associate|auto-289|<tuple|standard molar Gibbs energy of formation|42>>
    <associate|auto-29|<tuple|Molar|3>>
    <associate|auto-290|<tuple|<tuple|calorimetry|evaluate an equilibrium
    constant>|42>>
    <associate|auto-291|<tuple|<tuple|gibbs energy|formation
    standard|ion>|42>>
    <associate|auto-292|<tuple|11.9|43>>
    <associate|auto-293|<tuple|<tuple|equilibrium|position, effect of T and p
    on>|43>>
    <associate|auto-294|<tuple|<tuple|le chatelier's>|44>>
    <associate|auto-295|<tuple|<tuple|enthalpy|solution|molar integral>|44>>
    <associate|auto-296|<tuple|<tuple|le chatelier's>|45>>
    <associate|auto-297|<tuple|<tuple|equilibrium|position, effect of T and p
    on>|45>>
    <associate|auto-3|<tuple|Chemical process|1>>
    <associate|auto-30|<tuple|excess molar quantity|3>>
    <associate|auto-31|<tuple|Gibbs--Duhem equation|4>>
    <associate|auto-32|<tuple|11.1.4|4>>
    <associate|auto-33|<tuple|<tuple|entropy|mixing|form>|4>>
    <associate|auto-34|<tuple|Additivity rule|5>>
    <associate|auto-35|<tuple|11.1.3|5>>
    <associate|auto-36|<tuple|11.1.5|5>>
    <associate|auto-37|<tuple|Quasicrystalline lattice model|5>>
    <associate|auto-38|<tuple|Mixture|5>>
    <associate|auto-39|<tuple|Simple mixture|5>>
    <associate|auto-4|<tuple|Exergonic process|1>>
    <associate|auto-40|<tuple|Statistical mechanics|6>>
    <associate|auto-41|<tuple|Regular solution|6>>
    <associate|auto-42|<tuple|Solution|6>>
    <associate|auto-43|<tuple|Redlich--Kister series|7>>
    <associate|auto-44|<tuple|Redlich\UKister series|7>>
    <associate|auto-45|<tuple|11.1.6|7>>
    <associate|auto-46|<tuple|<tuple|phase|separation of a liquid
    mixture>|7>>
    <associate|auto-47|<tuple|11.1.4|7>>
    <associate|auto-48|<tuple|11.1.5|8>>
    <associate|auto-49|<tuple|<tuple|miscibility gap|binary system>|9>>
    <associate|auto-5|<tuple|11.1|1>>
    <associate|auto-50|<tuple|<tuple|phase|separation of a liquid
    mixture>|9>>
    <associate|auto-51|<tuple|11.2|9>>
    <associate|auto-52|<tuple|Reaction|9>>
    <associate|auto-53|<tuple|Equation|9>>
    <associate|auto-54|<tuple|Chemical equation|9>>
    <associate|auto-55|<tuple|Equation|9>>
    <associate|auto-56|<tuple|Reactant|9>>
    <associate|auto-57|<tuple|Product|9>>
    <associate|auto-58|<tuple|Stoichiometric|9>>
    <associate|auto-59|<tuple|Equation|9>>
    <associate|auto-6|<tuple|Mixing process|1>>
    <associate|auto-60|<tuple|11.2.1|9>>
    <associate|auto-61|<tuple|Advancement|10>>
    <associate|auto-62|<tuple|advancement|10>>
    <associate|auto-63|<tuple|Extent of reaction|10>>
    <associate|auto-64|<tuple|Enthalpy|10>>
    <associate|auto-65|<tuple|molar reaction enthalpy|10>>
    <associate|auto-66|<tuple|11.2.2|11>>
    <associate|auto-67|<tuple|Stoichiometric|11>>
    <associate|auto-68|<tuple|Stoichiometric|11>>
    <associate|auto-69|<tuple|stoichiometric number|11>>
    <associate|auto-7|<tuple|Process|1>>
    <associate|auto-70|<tuple|Molar|12>>
    <associate|auto-71|<tuple|Reaction quantity|12>>
    <associate|auto-72|<tuple|molar reaction quantity|12>>
    <associate|auto-73|<tuple|Molar|12>>
    <associate|auto-74|<tuple|Reaction quantity|12>>
    <associate|auto-75|<tuple|Molar|12>>
    <associate|auto-76|<tuple|Reaction quantity|12>>
    <associate|auto-77|<tuple|11.2.1|13>>
    <associate|auto-78|<tuple|11.2.3|13>>
    <associate|auto-79|<tuple|Standard molar|13>>
    <associate|auto-8|<tuple|mixing process|1>>
    <associate|auto-80|<tuple|Molar|13>>
    <associate|auto-81|<tuple|standard molar reaction quantity|13>>
    <associate|auto-82|<tuple|11.3|13>>
    <associate|auto-83|<tuple|11.3.1|14>>
    <associate|auto-84|<tuple|Exothermic reaction|14>>
    <associate|auto-85|<tuple|Reaction|14>>
    <associate|auto-86|<tuple|exothermic|14>>
    <associate|auto-87|<tuple|Endothermic reaction|14>>
    <associate|auto-88|<tuple|Reaction|14>>
    <associate|auto-89|<tuple|endothermic|14>>
    <associate|auto-9|<tuple|11.1.1|2>>
    <associate|auto-90|<tuple|11.3.2|14>>
    <associate|auto-91|<tuple|Enthalpy|14>>
    <associate|auto-92|<tuple|standard molar reaction enthalpy|14>>
    <associate|auto-93|<tuple|Formation reaction|14>>
    <associate|auto-94|<tuple|formation reaction|14>>
    <associate|auto-95|<tuple|<tuple|reference state|element>|14>>
    <associate|auto-96|<tuple|<tuple|enthalpy|formation standard>|15>>
    <associate|auto-97|<tuple|standard molar enthalpy of formation|15>>
    <associate|auto-98|<tuple|Hess's law|15>>
    <associate|auto-99|<tuple|Hess's law|15>>
    <associate|bio:hess|<tuple|1|18|bio-HESS.tm>>
    <associate|c11|<tuple|11|1>>
    <associate|c11 sec aft|<tuple|11.6|32>>
    <associate|c11 sec amrq|<tuple|11.2|9>>
    <associate|c11 sec amrq-ex-ammonia|<tuple|11.2.1|9>>
    <associate|c11 sec esd|<tuple|11.4|19>>
    <associate|c11 sec esd-dilution|<tuple|11.4.2|20>>
    <associate|c11 sec esd-solution|<tuple|11.4.1|19>>
    <associate|c11 sec etpep|<tuple|11.9|43>>
    <associate|c11 sec gere|<tuple|11.7|33>>
    <associate|c11 sec gere-derivation|<tuple|11.7.3|34>>
    <associate|c11 sec gere-ideal-gas|<tuple|11.7.6|36>>
    <associate|c11 sec gere-mixtures|<tuple|11.7.5|35>>
    <associate|c11 sec gere-pure-phases|<tuple|11.7.4|34>>
    <associate|c11 sec mp|<tuple|11.1|1>>
    <associate|c11 sec mp-excess|<tuple|11.1.3|3>>
    <associate|c11 sec mp-general|<tuple|11.1.1|1>>
    <associate|c11 sec mp-ideal|<tuple|11.1.2|2>>
    <associate|c11 sec mre|<tuple|11.3|13>>
    <associate|c11 sec mre-heat|<tuple|11.3.1|14>>
    <associate|c11 sec mre-heat-capacity|<tuple|11.3.3|16>>
    <associate|c11 sec rc|<tuple|11.5|26>>
    <associate|c11 sec rc-bomb|<tuple|11.5.2|27>>
    <associate|c11 sec rc-bomb-crt|<tuple|<tuple|isothermal|bomb
    process>|30>>
    <associate|c11 sec rc-bomb-exp|<tuple|5|28>>
    <associate|c11 sec rc-bomb-ibp|<tuple|Circuit|29>>
    <associate|c11 sec rc-bomb-rss|<tuple|11.5.8|30>>
    <associate|c11 sec rc-bomb-smec|<tuple|11.5.10|31>>
    <associate|c11 sec rc-bomb-wc|<tuple|11.5.13|31>>
    <associate|c11 sec rc-other|<tuple|11.5.3|32>>
    <associate|c11 sec tec|<tuple|11.8|38>>
    <associate|c11 sec tec-evaluation-k|<tuple|11.8.4|41>>
    <associate|c11 sec tec-reaction-gas|<tuple|11.8.2|40>>
    <associate|c11 sec tec-reaction-solution|<tuple|11.8.3|41>>
    <associate|c11 sec-amrq-general|<tuple|11.2.2|11>>
    <associate|c11 sec-amrq-std-quantities|<tuple|11.2.3|13>>
    <associate|c11 sec-esd-relative-partial|<tuple|11.4.4|22>>
    <associate|c11 sec-esd-solute-formation|<tuple|11.4.3|21>>
    <associate|c11 sec-gere-gibbs-energy|<tuple|11.7.1|33>>
    <associate|c11 sec-gere-spontaneity|<tuple|11.7.2|33>>
    <associate|c11 sec-mp-entropy-ideal|<tuple|11.1.4|4>>
    <associate|c11 sec-mp-liquid-phase-sep|<tuple|11.1.6|7>>
    <associate|c11 sec-mp-molecular-model|<tuple|11.1.5|5>>
    <associate|c11 sec-mre-effect-temperature|<tuple|11.3.4|17>>
    <associate|c11 sec-mre-std-enthalpies|<tuple|11.3.2|14>>
    <associate|c11 sec-rc-constant-pressure|<tuple|11.5.1|26>>
    <associate|c11 sec-tec-activities|<tuple|11.8.1|38>>
    <associate|closed sys (eqm)|<tuple|11.7.8|34>>
    <associate|d xi(sol),eq)/dT|<tuple|2|44>>
    <associate|d(nG^E)=|<tuple|11.1.21|4>>
    <associate|dA=-SdT-pdV+sum()dxi|<tuple|11.7.11|36>>
    <associate|dDel(r)G/dT=,dDel(r)G/dp=|<tuple|11.9.2|43>>
    <associate|dDel(r)H/dT=|<tuple|11.3.5|16>>
    <associate|dDel(r)H^o/dT=|<tuple|11.3.6|17>>
    <associate|dG=-SdT+Vdp+(del(r)Gm)dxi|<tuple|11.7.3|33>>
    <associate|dG=.dT+.dp+.dxi|<tuple|11.9.1|43>>
    <associate|dH not equal to dq|<tuple|11.3.1|14>>
    <associate|dH=()dxi|<tuple|11.2.5|10>>
    <associate|dH=H(N2)dn(N2)+...|<tuple|11.2.1|10>>
    <associate|dS= (eqm)|<tuple|11.7.9|34>>
    <associate|dU=T(alpha)dS(alpha) +..|<tuple|11.7.5|34>>
    <associate|dX=()dT+()dp+del(r)Xm*dxi|<tuple|11.2.14|12>>
    <associate|dX=.dT+.dp+sum|<tuple|11.2.13|12>>
    <associate|ddel(r)Gm=()dT+()dp+()dxi|<tuple|11.9.4|43>>
    <associate|del(B)H=del(A)H-del(B)H|<tuple|11.5.2|27>>
    <associate|del(dil)Hm=HA-HmA*|<tuple|11.4.7|21>>
    <associate|del(dil)Hm=dH/d(xi(dil))|<tuple|11.4.6|20>>
    <associate|del(f)Gmo=del(f)Hmo-Tdel(f)Smo|<tuple|11.8.23|42>>
    <associate|del(mix)G=G2-G1|<tuple|11.1.3|1>>
    <associate|del(mix)Gm(id)=RT*sum(x_i)ln(x_i)|<tuple|11.1.8|2>>
    <associate|del(mix)Hm(id)=0|<tuple|11.1.10|2>>
    <associate|del(mix)Sm(id)=-R*sum(x_i)ln(x_i)|<tuple|11.1.9|2>>
    <associate|del(mix)Um(id)=0|<tuple|11.1.11|3>>
    <associate|del(mix)Vm(id)=0|<tuple|11.1.12|3>>
    <associate|del(r)G(m)=0|<tuple|11.7.4|33>>
    <associate|del(r)Gm=dG/dx|<tuple|11.7.2|33>>
    <associate|del(r)Gm=del(r)Gmo+RT*lnQ(r)|<tuple|11.8.8|39>>
    <associate|del(r)Gm=del(r)Hm-Tdel(r)Sm|<tuple|11.8.20|42>>
    <associate|del(r)Gm=sum(nu_i)(mu_i)|<tuple|11.7.1|33>>
    <associate|del(r)Gmo=-RT*ln(K)|<tuple|11.8.10|40>>
    <associate|del(r)Gmo=del(r)Hmo-Tdel(r)Smo|<tuple|11.8.21|42>>
    <associate|del(r)Gmo=sum(nu_i)(mu_io)|<tuple|11.8.3|39>>
    <associate|del(r)Gmo=sum(nu_i)del(f)Gmio|<tuple|11.8.24|42>>
    <associate|del(r)H=dq/dxi|<tuple|11.3.1|14>>
    <associate|del(r)Hm(int)=|<tuple|11.5.3|27>>
    <associate|del(r)Hm=-H(N2)...|<tuple|11.2.6|10>>
    <associate|del(r)Hm=dH/dxi|<tuple|11.2.7|10>>
    <associate|del(r)Smo=sum(nu_i)Smio|<tuple|11.8.22|42>>
    <associate|del(r)Xm=dX/dxi|<tuple|11.2.16|12>>
    <associate|del(r)Xm=sum(nu_i)X_i|<tuple|11.2.15|12>>
    <associate|del(r)Xmo=sum(nu_i)X_io|<tuple|11.2.18|13>>
    <associate|del(sol)H(int)=-(nB)del(f)Hmo(B*)+...|<tuple|11.4.11|22>>
    <associate|del(sol)Hm(infty)=HB(infty)-Hmb*|<tuple|11.4.3|19>>
    <associate|del(sol)Hm(int)=del(sol)H/xi(sol)|<tuple|11.4.4|20>>
    <associate|del(sol)Hm=HB-Hmb*|<tuple|11.4.2|19>>
    <associate|del(sol)Hm=d del(sol)H/d xi(sol)|<tuple|11.4.5|20>>
    <associate|del(sol)Hm=dH/d(xi(sol))|<tuple|11.4.1|19>>
    <associate|delU(mix) - model|<tuple|11.1.31|6>>
    <associate|delX_m(rxn)=del(r)X|<tuple|Reaction quantity|12>>
    <associate|dnGm(E)/dn(i)=RTln(gamma(i))|<tuple|11.1.20|4>>
    <associate|dn_i=(nu_i)dxi|<tuple|11.2.12|11>>
    <associate|dxi(eq)/dT=|<tuple|11.9.8|44>>
    <associate|dxi(eq)/dp=|<tuple|11.9.9|44>>
    <associate|dxi(eq)=|<tuple|11.9.7|44>>
    <associate|fig:11-G vs xi|<tuple|11.7.2|35>>
    <associate|fig:11-G vs xi, multipart|<tuple|11.7.3|37>>
    <associate|fig:11-G vs xi, pure phases|<tuple|11.7.1|35>>
    <associate|fig:11-Kirchhoff|<tuple|11.3.1|17>>
    <associate|fig:11-Na acetate|<tuple|11.4.2|20>>
    <associate|fig:11-NaCl|<tuple|11.4.3|24>>
    <associate|fig:11-Redlich-Kister|<tuple|11.1.5|8>>
    <associate|fig:11-S-H-xi|<tuple|11.2.1|13>>
    <associate|fig:11-V-p-G|<tuple|11.7.4|38>>
    <associate|fig:11-bomb calorimeter|<tuple|11.5.2|28>>
    <associate|fig:11-bomb paths|<tuple|11.5.3|29>>
    <associate|fig:11-calorimeter paths|<tuple|11.5.1|26>>
    <associate|fig:11-ideal gas mixing|<tuple|11.1.3|5>>
    <associate|fig:11-mixing process|<tuple|11.1.1|2>>
    <associate|fig:11-mixing quantities|<tuple|11.1.2|3>>
    <associate|fig:11-phase separation|<tuple|11.1.4|7>>
    <associate|fig:11-soln/diln|<tuple|11.4.1|19>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-11.1.1|<tuple|11.1.1|2>>
    <associate|footnote-11.1.2|<tuple|11.1.2|3>>
    <associate|footnote-11.1.3|<tuple|11.1.3|6>>
    <associate|footnote-11.1.4|<tuple|11.1.4|6>>
    <associate|footnote-11.1.5|<tuple|11.1.5|7>>
    <associate|footnote-11.3.1|<tuple|11.3.1|16>>
    <associate|footnote-11.3.2|<tuple|11.3.2|18|bio-HESS.tm>>
    <associate|footnote-11.3.3|<tuple|11.3.3|18|bio-HESS.tm>>
    <associate|footnote-11.3.4|<tuple|11.3.4|18|bio-HESS.tm>>
    <associate|footnote-11.4.1|<tuple|11.4.1|19>>
    <associate|footnote-11.4.2|<tuple|11.4.2|20>>
    <associate|footnote-11.4.3|<tuple|11.4.3|23>>
    <associate|footnote-11.4.4|<tuple|11.4.4|24>>
    <associate|footnote-11.4.5|<tuple|11.4.5|24>>
    <associate|footnote-11.4.6|<tuple|11.4.6|25>>
    <associate|footnote-11.5.1|<tuple|11.5.1|27>>
    <associate|footnote-11.5.2|<tuple|11.5.2|29>>
    <associate|footnote-11.5.3|<tuple|11.5.3|31>>
    <associate|footnote-11.7.1|<tuple|11.7.1|33>>
    <associate|footnote-11.7.2|<tuple|11.7.2|34>>
    <associate|footnote-11.7.3|<tuple|11.7.3|37>>
    <associate|footnote-11.7.4|<tuple|11.7.4|37>>
    <associate|footnote-11.8.1|<tuple|11.8.1|39>>
    <associate|footnote-11.8.2|<tuple|11.8.2|40>>
    <associate|footnote-11.8.3|<tuple|11.8.3|40>>
    <associate|footnote-11.8.4|<tuple|11.8.4|42>>
    <associate|footnote-11.8.5|<tuple|11.8.5|42>>
    <associate|footnote-11.9.1|<tuple|11.9.1|44>>
    <associate|footnr-1|<tuple|Exergonic process|1>>
    <associate|footnr-11.1.1|<tuple|11.1.1|2>>
    <associate|footnr-11.1.2|<tuple|11.1.2|3>>
    <associate|footnr-11.1.3|<tuple|Statistical mechanics|6>>
    <associate|footnr-11.1.4|<tuple|11.1.4|6>>
    <associate|footnr-11.1.5|<tuple|11.1.5|7>>
    <associate|footnr-11.3.1|<tuple|11.3.1|16>>
    <associate|footnr-11.3.2|<tuple|11.3.2|18|bio-HESS.tm>>
    <associate|footnr-11.3.3|<tuple|11.3.3|18|bio-HESS.tm>>
    <associate|footnr-11.3.4|<tuple|11.3.4|18|bio-HESS.tm>>
    <associate|footnr-11.4.1|<tuple|11.4.1|19>>
    <associate|footnr-11.4.2|<tuple|11.4.2|20>>
    <associate|footnr-11.4.3|<tuple|11.4.3|23>>
    <associate|footnr-11.4.4|<tuple|a|24>>
    <associate|footnr-11.4.5|<tuple|b|24>>
    <associate|footnr-11.4.6|<tuple|11.4.6|25>>
    <associate|footnr-11.5.1|<tuple|11.5.1|27>>
    <associate|footnr-11.5.2|<tuple|11.5.2|29>>
    <associate|footnr-11.5.3|<tuple|11.5.3|31>>
    <associate|footnr-11.7.1|<tuple|Affinity of reaction|33>>
    <associate|footnr-11.7.2|<tuple|11.7.2|34>>
    <associate|footnr-11.7.3|<tuple|11.7.3|37>>
    <associate|footnr-11.7.4|<tuple|11.7.4|37>>
    <associate|footnr-11.8.1|<tuple|11.8.1|39>>
    <associate|footnr-11.8.2|<tuple|11.8.2|40>>
    <associate|footnr-11.8.3|<tuple|11.8.3|40>>
    <associate|footnr-11.8.4|<tuple|11.8.4|42>>
    <associate|footnr-11.8.5|<tuple|11.8.5|42>>
    <associate|footnr-11.9.1|<tuple|11.9.1|44>>
    <associate|mu_i(a_i,phi)= again|<tuple|11.8.1|38>>
    <associate|n(N2)=...|<tuple|11.2.3|10>>
    <associate|n_i=n_(i,0)+(nu_i)xi|<tuple|11.2.11|11>>
    <associate|part:bio-HESS.tm|<tuple|<tuple|enthalpy|molar, effect of
    temperature on>|18>>
    <associate|st molar diff \ int identical|<tuple|2|13>>
    <associate|st molar diff and int identical|<tuple|2|?>>
    <associate|stoich reln|<tuple|11.2.10|11>>
    <associate|sum(x_i)dln(ac_i)+sum(dx_i)=0|<tuple|11.1.22|4>>
    <associate|y= (common tangent)|<tuple|11.1.38|8>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      mcglashan-79

      hildebrand-62

      kohler-2011

      leicester-51

      hess-1840

      hess-1840

      davis-51

      greenbook-3

      wagman-82

      wagman-82

      parker-65

      washburn-33

      greenbook-3

      ewing-94

      brice-83
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.1.1>|>
        Initial state (left) and final state (right) of mixing process for
        liquid substances A and B.
      </surround>|<pageref|auto-9>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.1.2>|>
        Molar mixing quantities for a binary ideal mixture at
        <with|mode|<quote|math>|298.15<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>>.
      </surround>|<pageref|auto-25>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.1.3>|>
        Reversible mixing process for ideal gases A and B confined in a
        cylinder. Piston 1 is permeable to A but not B; piston 2 is permeable
        to B but not A.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Gases
                A and B are in separate phases at the same temperature and
                pressure.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>The
                pistons move apart at constant temperature with negative
                reversible work, creating an ideal gas mixture of A and B in
                continuous transfer equilibrium with the pure gases.

                <assign|item-nr|3><hidden-binding|<tuple>|c><assign|last-item-nr|3><assign|last-item|c><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|c<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>The
                two gases are fully mixed at the initial temperature and
                pressure.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-35>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.1.4>|>
        Molar Gibbs energy of mixing as a function of the composition of a
        binary liquid mixture with spontaneous phase separation. The
        inflection points are indicated by filled circles.
      </surround>|<pageref|auto-47>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.1.5>|>
        Binary liquid mixtures at <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>. The curves are calculated from the
        two-parameter Redlich\UKister series using the following parameter
        values.

        <\surround||<specific|texmacs|<htab|0fn|first>>>
          <\with|par-left|<quote|<tmlen|13456|26911.9|40368>>>
            Curve 1: <with|mode|<quote|math>|a=b=0> (ideal liquid mixture).

            Curve 2: <with|mode|<quote|math>|a/R*T=1.8>,
            <with|mode|<quote|math>|b/R*T=0.36>.

            Curve 3: <with|mode|<quote|math>|a/R*T=2.4>,
            <with|mode|<quote|math>|b/R*T=0.48>.
          </with>
        </surround>

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Molar
                Gibbs energy of mixing as a function of composition.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Activity
                of component A (using a pure-liquid standard state) as a
                function of composition.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-48>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.2.1>|>
        Enthalpy and entropy as functions of advancement at constant
        <with|mode|<quote|math>|T> and <with|mode|<quote|math>|p>. The curves
        are for a reaction A<with|mode|<quote|math>|\<rightarrow\>>2B with
        positive <with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*H>
        taking place in an ideal gas mixture with initial amounts
        <with|mode|<quote|math>|n<rsub|<with|mode|<quote|text>|A>,0>=1<with|mode|<quote|text>|mol>>
        and <with|mode|<quote|math>|n<rsub|<with|mode|<quote|text>|B>,0>=0>.
      </surround>|<pageref|auto-77>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.3.1>|>
        Dependence of reaction enthalpy on temperature at constant pressure.
      </surround>|<pageref|auto-114>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.4.1>|>
        Two related processes in closed systems. A: solvent; B: solute. The
        dashed rectangles represent the system boundaries.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Solution
                process.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Dilution
                process.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-126>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.4.2>|>
        Enthalpy change for the dissolution of
        NaCH<rsub|<with|mode|<quote|math>|3>>CO<rsub|<with|mode|<quote|math>|2>>(s)
        in one kilogram of water in a closed system at
        <with|mode|<quote|math>|298.15<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>> and <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>, as a function of the amount
        <with|mode|<quote|math>|\<xi\><rsub|<with|mode|<quote|text>|sol>>> of
        dissolved solute.<space|0spc><assign|footnote-nr|2><hidden-binding|<tuple>|11.4.2><assign|fnote-+17DAf9rZ1rq092F6|<quote|11.4.2>><assign|fnlab-+17DAf9rZ1rq092F6|<quote|11.4.2>><rsup|<with|font-shape|<quote|right>|<reference|footnote-11.4.2>>>
        The open circle at <with|mode|<quote|math>|\<xi\><rsub|<with|mode|<quote|text>|sol>>=15
        <with|mode|<quote|text>|mol>> indicates the approximate saturation
        limit; data to the right of this point come from supersaturated
        solutions. At the composition <with|mode|<quote|math>|m<rsub|<with|mode|<quote|text>|B>>=15
        <with|mode|<quote|text>|mol>\<cdot\><with|mode|<quote|text>|kg><rsup|-1>>,
        the value of <with|mode|<quote|math>|\<Delta\>*H<rsub|<with|mode|<quote|text>|m>><around*|(|<with|mode|<quote|text>|sol>,m<rsub|<with|mode|<quote|text>|B>>|)>>
        is the slope of line a and the value of
        <with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|sol>>*H>
        is the slope of line b. The value of
        <with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|sol>>*H<rsup|\<infty\>>>
        is the slope of line c.

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-C0972628--C77D2DB0>|<link|hyperlink|<id|%-C0972628--C77D2DB0>|<url|#footnr-11.4.2>>|11.4.2>.
        |<hidden-binding|<tuple|footnote-11.4.2>|11.4.2>|>>>Data from Ref.
        [<write|bib|wagman-82><reference|bib-wagman-82>], page 2-315.
      </surround>|<pageref|auto-137>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.4.3>|>
        Thermal properties of aqueous NaCl at <with|mode|<quote|math>|25.00
        <rsup|\<circ\>><with|mode|<quote|text>|C>>.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Left
                axis: molar integral enthalpy of solution to produce solution
                of molality <with|mode|<quote|math>|m<rsub|<with|mode|<quote|text>|B>>>.<space|0spc><assign|footnote-nr|4><hidden-binding|<tuple>|11.4.4><assign|fnote-+1BMXlf48J76g2i|<quote|11.4.4>><assign|fnlab-+1BMXlf48J76g2i|<quote|11.4.4>><rsup|<with|font-shape|<quote|right>|<reference|footnote-11.4.4>>>
                The dashed line has a slope equal to the theoretical limiting
                value of the slope of the curve. Right axis: relative
                apparent molar enthalpy of the solute.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Relative
                partial molar enthalpy of the solute as a function of
                molality.<space|0spc><assign|footnote-nr|5><hidden-binding|<tuple>|11.4.5><assign|fnote-+1BMXlf48J76g2j|<quote|11.4.5>><assign|fnlab-+1BMXlf48J76g2j|<quote|11.4.5>><rsup|<with|font-shape|<quote|right>|<reference|footnote-11.4.5>>>
              </surround>
            </with>
          </surround>
        </with>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-C0972628--C69299E8>|<link|hyperlink|<id|%-C0972628--C69299E8>|<url|#footnr-11.4.4>>|11.4.4>.
        |<hidden-binding|<tuple|footnote-11.4.4>|11.4.4>|>>>Calculated from
        molar enthalpy of formation values in Ref.
        [<write|bib|wagman-82><reference|bib-wagman-82>], p. 2-301.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-C0972628--C69599E0>|<link|hyperlink|<id|%-C0972628--C69599E0>|<url|#footnr-11.4.5>>|11.4.5>.
        |<hidden-binding|<tuple|footnote-11.4.5>|11.4.5>|>>>Based on data in
        Ref. [<write|bib|parker-65><reference|bib-parker-65>], Table X.
      </surround>|<pageref|auto-163>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.5.1>|>
        Enthalpy changes for paths at constant pressure (schematic). R
        denotes reactants and P denotes products.
      </surround>|<pageref|auto-177>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.5.2>|>
        Section view of a bomb calorimeter.
      </surround>|<pageref|auto-198>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.5.3>|>
        Internal energy changes for paths at constant volume in a bomb
        calorimeter (schematic). R denotes reactants and P denotes products.
      </surround>|<pageref|auto-203>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.7.1>|>
        Gibbs energy versus advancement at constant
        <with|mode|<quote|math>|T> and <with|mode|<quote|math>|p> in systems
        of pure phases. <with|mode|<quote|math>|G> is a linear function of
        <with|mode|<quote|math>|\<xi\>> with slope equal to
        <with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G>.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|><with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G>
                is negative; <with|mode|<quote|math>|\<xi\>> spontaneously
                increases.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|><with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G>
                is positive; <with|mode|<quote|math>|\<xi\>> spontaneously
                decreases.

                <assign|item-nr|3><hidden-binding|<tuple>|c><assign|last-item-nr|3><assign|last-item|c><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|c<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|><with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G>
                is zero; the system is in reaction equilibrium at all values
                of <with|mode|<quote|math>|\<xi\>>.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-250>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.7.2>|>
        Gibbs energy as a function of advancement at constant
        <with|mode|<quote|math>|T> and <with|mode|<quote|math>|p> in a closed
        system containing a mixture. The open circle is at the minimum value
        of <with|mode|<quote|math>|G>. (The reaction is the same as in Fig.
        <reference|fig:11-S-H-xi> on page <pageref|fig:11-S-H-xi>.)
      </surround>|<pageref|auto-256>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.7.3>|>
        Gibbs energy as a function of the advancement of the reaction
        A<with|mode|<quote|math>|\<rightarrow\>>B in an ideal gas mixture at
        constant <with|mode|<quote|math>|T> and <with|mode|<quote|math>|p>.
        The initial amount of B is zero. The equilibrium positions are
        indicated by open circles.

        (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>><with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G<rsup|\<circ\>>\<less\>0><space|2em>
        (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>><with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G<rsup|\<circ\>>=0><space|2em>
        (c) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>><with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G<rsup|\<circ\>>\<gtr\>0>
      </surround>|<pageref|auto-261>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.7.4>|>
        Dependence of Gibbs energy on volume and pressure, at constant
        temperature, in a closed system containing an ideal gas mixture of A
        and B. The reaction is A<with|mode|<quote|math>|\<rightarrow\>>2<space|0.17em>B
        with <with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G<rsup|\<circ\>>=0>.
        Solid curves: contours of constant <with|mode|<quote|math>|G> plotted
        at an interval of <with|mode|<quote|math>|0.5*n<rsub|<with|mode|<quote|text>|A>,0>*R*T>.
        Dashed curve: states of reaction equilibrium
        (<with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G=0>).
        Dotted curves: limits of possible values of the advancement. Open
        circle: position of minimum <with|mode|<quote|math>|G> (and an
        equilibrium state) at the constant pressure
        <with|mode|<quote|math>|p=1.02*p<rsup|\<circ\>>>. Filled circle:
        position of minimum <with|mode|<quote|math>|G> for a constant volume
        of <with|mode|<quote|math>|1.41*V<rsub|0>>, where
        <with|mode|<quote|math>|V<rsub|0>> is the initial volume at pressure
        <with|mode|<quote|math>|p<rsup|\<circ\>>>.
      </surround>|<pageref|auto-262>>
    </associate>
    <\associate|gly>
      <tuple|normal|mixing process|<pageref|auto-8>>

      <tuple|normal|Gibbs energy of mixing|<pageref|auto-13>>

      <tuple|normal|molar Gibbs energy of mixing|<pageref|auto-15>>

      <tuple|normal|excess quantity|<pageref|auto-28>>

      <tuple|normal|excess molar quantity|<pageref|auto-30>>

      <tuple|normal|Redlich\UKister series|<pageref|auto-44>>

      <tuple|normal|advancement|<pageref|auto-62>>

      <tuple|normal|molar reaction enthalpy|<pageref|auto-65>>

      <tuple|normal|stoichiometric number|<pageref|auto-69>>

      <tuple|normal|molar reaction quantity|<pageref|auto-72>>

      <tuple|normal|standard molar reaction quantity|<pageref|auto-81>>

      <tuple|normal|exothermic|<pageref|auto-86>>

      <tuple|normal|endothermic|<pageref|auto-89>>

      <tuple|normal|standard molar reaction enthalpy|<pageref|auto-92>>

      <tuple|normal|formation reaction|<pageref|auto-94>>

      <tuple|normal|standard molar enthalpy of formation|<pageref|auto-97>>

      <tuple|normal|Hess's law|<pageref|auto-99>>

      <tuple|normal|Kirchhoff equation|<pageref|auto-113>>

      <tuple|normal|solution process|<pageref|auto-122>>

      <tuple|normal|dilution process|<pageref|auto-125>>

      <tuple|normal|molar differential enthalpy of
      solution|<pageref|auto-129>>

      <tuple|normal|molar enthalpy of solution at infinite
      dilution|<pageref|auto-131>>

      <tuple|normal|integral enthalpy of solution|<pageref|auto-134>>

      <tuple|normal|molar integral enthalpy of solution|<pageref|auto-136>>

      <tuple|normal|molar differential enthalpy of
      dilution|<pageref|auto-141>>

      <tuple|normal|integral enthalpy of dilution|<pageref|auto-144>>

      <tuple|normal|molar integral enthalpy of dilution|<pageref|auto-146>>

      <tuple|normal|relative partial molar enthalpy of the
      solvent|<pageref|auto-154>>

      <tuple|normal|relative partial molar enthalpy of a
      solute|<pageref|auto-159>>

      <tuple|normal|isothermal bomb process|<pageref|auto-193>>

      <tuple|normal|reduction to standard states|<pageref|auto-196>>

      <tuple|normal|Washburn corrections|<pageref|auto-215>>

      <tuple|normal|molar reaction Gibbs energy|<pageref|auto-240>>

      <tuple|normal|standard molar reaction Gibbs energy|<pageref|auto-268>>

      <tuple|normal|reaction quotient|<pageref|auto-271>>

      <tuple|normal|proper quotient|<pageref|auto-274>>

      <tuple|normal|thermodynamic equilibrium constant|<pageref|auto-277>>

      <tuple|normal|equilibrium constant on a pressure
      basis|<pageref|auto-282>>

      <tuple|normal|standard molar Gibbs energy of
      formation|<pageref|auto-289>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Process|chemical>|<pageref|auto-2>>

      <tuple|<tuple|Chemical process>|<pageref|auto-3>>

      <tuple|<tuple|Exergonic process>|<pageref|auto-4>>

      <tuple|<tuple|Mixing process>|<pageref|auto-6>>

      <tuple|<tuple|Process|mixing>|<pageref|auto-7>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-11>>

      <tuple|<tuple|gibbs energy|mixing>|||<tuple|Gibbs energy|of
      mixing>|<pageref|auto-12>>

      <tuple|<tuple|gibbs energy|mixing|molar>|||<tuple|Gibbs energy|of
      mixing|molar>|<pageref|auto-14>>

      <tuple|<tuple|Ideal mixture|mixing process>|<pageref|auto-17>>

      <tuple|<tuple|gibbs energy|mixing|ideal mixture>|||<tuple|Gibbs
      energy|of mixing|to form an ideal mixture>|<pageref|auto-18>>

      <tuple|<tuple|entropy|mixing|form>|||<tuple|Entropy|of mixing|to form
      an ideal mixture>|<pageref|auto-19>>

      <tuple|<tuple|entropy|mixing|negative value>|||<tuple|Entropy|of
      mixing|negative value>|<pageref|auto-20>>

      <tuple|<tuple|enthalpy|mixing>|||<tuple|Enthalpy|of mixing to form an
      ideal mixture>|<pageref|auto-21>>

      <tuple|<tuple|internal energy|mixing>|||<tuple|Internal energy|of
      mixing to form an ideal mixture>|<pageref|auto-22>>

      <tuple|<tuple|Athermal process>|<pageref|auto-23>>

      <tuple|<tuple|volume|mixing>|||<tuple|Volume|of mixing to form an ideal
      mixture>|<pageref|auto-24>>

      <tuple|<tuple|Excess|quantity>|<pageref|auto-27>>

      <tuple|<tuple|Molar|excess quantity>|<pageref|auto-29>>

      <tuple|<tuple|Gibbs--Duhem equation>|<pageref|auto-31>>

      <tuple|<tuple|entropy|mixing|form>|||<tuple|Entropy|of mixing|to form
      an ideal mixture>|<pageref|auto-33>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-34>>

      <tuple|<tuple|Quasicrystalline lattice model>|<pageref|auto-37>>

      <tuple|<tuple|Mixture|simple>|<pageref|auto-38>>

      <tuple|<tuple|Simple mixture>|<pageref|auto-39>>

      <tuple|<tuple|Statistical mechanics|mixture theory>|<pageref|auto-40>>

      <tuple|<tuple|Regular solution>|<pageref|auto-41>>

      <tuple|<tuple|Solution|regular>|<pageref|auto-42>>

      <tuple|<tuple|Redlich--Kister series>|<pageref|auto-43>>

      <tuple|<tuple|phase|separation of a liquid mixture>||c11
      sec-mp-liquid-phase-sep idx1|<tuple|Phase|separation of a liquid
      mixture>|<pageref|auto-46>>

      <tuple|<tuple|miscibility gap|binary system>|||<tuple|Miscibility
      gap|in a binary system>|<pageref|auto-49>>

      <tuple|<tuple|phase|separation of a liquid mixture>||c11
      sec-mp-liquid-phase-sep idx1|<tuple|Phase|separation of a liquid
      mixture>|<pageref|auto-50>>

      <tuple|<tuple|Reaction|equation>|<pageref|auto-52>>

      <tuple|<tuple|Equation|reaction>|<pageref|auto-53>>

      <tuple|<tuple|Chemical equation>|<pageref|auto-54>>

      <tuple|<tuple|Equation|chemical>|<pageref|auto-55>>

      <tuple|<tuple|Reactant>|<pageref|auto-56>>

      <tuple|<tuple|Product>|<pageref|auto-57>>

      <tuple|<tuple|Stoichiometric|equation>|<pageref|auto-58>>

      <tuple|<tuple|Equation|stoichiometric>|<pageref|auto-59>>

      <tuple|<tuple|Advancement>|<pageref|auto-61>>

      <tuple|<tuple|Extent of reaction>|<pageref|auto-63>>

      <tuple|<tuple|Enthalpy|molar reaction>|<pageref|auto-64>>

      <tuple|<tuple|Stoichiometric|coefficient>|<pageref|auto-67>>

      <tuple|<tuple|Stoichiometric|number>|<pageref|auto-68>>

      <tuple|<tuple|Molar|reaction quantity>|<pageref|auto-70>>

      <tuple|<tuple|Reaction quantity|molar>|<pageref|auto-71>>

      <tuple|<tuple|Molar|integral reaction quantity>|<pageref|auto-73>>

      <tuple|<tuple|Reaction quantity|molar integral>|<pageref|auto-74>>

      <tuple|<tuple|Molar|differential reaction quantity>|<pageref|auto-75>>

      <tuple|<tuple|Reaction quantity|molar differential>|<pageref|auto-76>>

      <tuple|<tuple|Standard molar|reaction quantity>|<pageref|auto-79>>

      <tuple|<tuple|Molar|reaction quantity|standard>|<pageref|auto-80>>

      <tuple|<tuple|Exothermic reaction>|<pageref|auto-84>>

      <tuple|<tuple|Reaction|exothermic>|<pageref|auto-85>>

      <tuple|<tuple|Endothermic reaction>|<pageref|auto-87>>

      <tuple|<tuple|Reaction|endothermic>|<pageref|auto-88>>

      <tuple|<tuple|Enthalpy|reaction|standard molar>|<pageref|auto-91>>

      <tuple|<tuple|Formation reaction>|<pageref|auto-93>>

      <tuple|<tuple|reference state|element>|||<tuple|Reference state|of an
      element>|<pageref|auto-95>>

      <tuple|<tuple|enthalpy|formation standard>|||<tuple|Enthalpy|of
      formation, standard molar>|<pageref|auto-96>>

      <tuple|<tuple|Hess's law>|<pageref|auto-98>>

      <tuple|<tuple|Calorimeter|bomb>|<pageref|auto-100>>

      <tuple|<tuple|Bomb calorimeter>|<pageref|auto-101>>

      <tuple|<tuple|Hess's law>|<pageref|auto-102>>

      <tuple|<tuple|Stoichiometric|number>|<pageref|auto-103>>

      <tuple|<tuple|Enthalpy|reaction|standard molar>|<pageref|auto-104>>

      <tuple|<tuple|enthalpy|formation standard|solute>|||<tuple|Enthalpy|of
      formation, standard molar|of a solute>|<pageref|auto-105>>

      <tuple|<tuple|enthalpy|formation standard|ion>|||<tuple|Enthalpy|of
      formation, standard molar|of an ion>|<pageref|auto-106>>

      <tuple|<tuple|Calorimetry|reaction>|<pageref|auto-107>>

      <tuple|<tuple|Heat capacity|molar reaction>|<pageref|auto-109>>

      <tuple|<tuple|enthalpy|molar, effect of temperature on>||c11
      sec-mre-effect-temperature idx1|<tuple|Enthalpy|molar, effect of
      temperature on>|<pageref|auto-111>>

      <tuple|<tuple|Kirchhoff equation>|<pageref|auto-112>>

      <tuple|<tuple|enthalpy|molar, effect of temperature on>||c11
      sec-mre-effect-temperature idx1|<tuple|Enthalpy|molar, effect of
      temperature on>|<pageref|auto-115>>

      <tuple|<tuple|Hess, Germain>|<pageref|auto-117>>

      <tuple|<tuple|IUPAC Green Book>|<pageref|auto-119>>

      <tuple|<tuple|Solution|process>|<pageref|auto-120>>

      <tuple|<tuple|Process|solution>|<pageref|auto-121>>

      <tuple|<tuple|Dilution process>|<pageref|auto-123>>

      <tuple|<tuple|Process|dilution>|<pageref|auto-124>>

      <tuple|<tuple|enthalpy|solution|molar
      differential>|||<tuple|Enthalpy|of solution|molar
      differential>|<pageref|auto-128>>

      <tuple|<tuple|enthalpy|solution|infinite dilution>|||<tuple|Enthalpy|of
      solution|at infinite dilution>|<pageref|auto-130>>

      <tuple|<tuple|Integral enthalpy of solution>|<pageref|auto-132>>

      <tuple|<tuple|enthalpy|solution|integral>|||<tuple|Enthalpy|of
      solution|integral>|<pageref|auto-133>>

      <tuple|<tuple|enthalpy|solution|molar integral>|||<tuple|Enthalpy|of
      solution|molar integral>|<pageref|auto-135>>

      <tuple|<tuple|enthalpy|solution|molar
      differential>|||<tuple|Enthalpy|of solution|molar
      differential>|<pageref|auto-138>>

      <tuple|<tuple|enthalpy|dilution|molar
      differential>|||<tuple|Enthalpy|of dilution|molar
      differential>|<pageref|auto-140>>

      <tuple|<tuple|Integral enthalpy of dilution>|<pageref|auto-142>>

      <tuple|<tuple|enthalpy|dilution|integral>|||<tuple|Enthalpy|of
      dilution|integral>|<pageref|auto-143>>

      <tuple|<tuple|enthalpy|dilution|molar integral>|||<tuple|Enthalpy|of
      dilution|molar integral>|<pageref|auto-145>>

      <tuple|<tuple|enthalpy|formation of a solute>|||<tuple|Enthalpy|of
      formation of a solute, molar>|<pageref|auto-148>>

      <tuple|<tuple|enthalpy|solution|molar integral>|||<tuple|enthalpy|of
      solution|molar integral>|<pageref|auto-149>>

      <tuple|<tuple|relative partial molar
      enthalpy|solvent>|||<tuple|Relative partial molar enthalpy|of the
      solvent>|<pageref|auto-151>>

      <tuple|<tuple|Partial molar|enthalpy|relative, of the
      solvent>|<pageref|auto-152>>

      <tuple|<tuple|Enthalpy|partial molar|relative, of the
      solvent>|<pageref|auto-153>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-155>>

      <tuple|<tuple|relative partial molar enthalpy|solute>|||<tuple|Relative
      partial molar enthalpy|of a solute>|<pageref|auto-156>>

      <tuple|<tuple|Partial molar|enthalpy|relative, of a
      solute>|<pageref|auto-157>>

      <tuple|<tuple|Enthalpy|partial molar|relative, of a
      solute>|<pageref|auto-158>>

      <tuple|<tuple|Relative apparent molar enthalpy of a
      solute>|<pageref|auto-160>>

      <tuple|<tuple|Enthalpy|relative apparent, of a
      solute>|<pageref|auto-161>>

      <tuple|<tuple|Debye\UH�ckel|limiting law>|<pageref|auto-162>>

      <tuple|<tuple|Debye\UH�ckel|limiting law>|<pageref|auto-164>>

      <tuple|<tuple|calorimetry|reaction>||c11 sec rc
      idx1|<tuple|Calorimetry|reaction>|<pageref|auto-166>>

      <tuple|<tuple|Calorimeter|bomb>|<pageref|auto-167>>

      <tuple|<tuple|Calorimeter|combustion>|<pageref|auto-168>>

      <tuple|<tuple|Bomb calorimeter>|<pageref|auto-169>>

      <tuple|<tuple|calorimeter|reaction>||c11 sec-rc-constant-pressure
      idx1|<tuple|Calorimeter|reaction>|<pageref|auto-171>>

      <tuple|<tuple|Adiabatic|calorimeter>|<pageref|auto-172>>

      <tuple|<tuple|Calorimeter|adiabatic>|<pageref|auto-173>>

      <tuple|<tuple|Calorimeter|isothermal-jacket>|<pageref|auto-174>>

      <tuple|<tuple|Isoperibol calorimeter>|<pageref|auto-175>>

      <tuple|<tuple|Calorimeter|isoperibol>|<pageref|auto-176>>

      <tuple|<tuple|Adiabatic|calorimeter>|<pageref|auto-178>>

      <tuple|<tuple|Calorimeter|adiabatic>|<pageref|auto-179>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-180>>

      <tuple|<tuple|Calorimeter|isothermal-jacket>|<pageref|auto-181>>

      <tuple|<tuple|Isoperibol calorimeter>|<pageref|auto-182>>

      <tuple|<tuple|Calorimeter|isoperibol>|<pageref|auto-183>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-184>>

      <tuple|<tuple|calorimeter|reaction>||c11 sec-rc-constant-pressure
      idx1|<tuple|Calorimeter|reaction>|<pageref|auto-185>>

      <tuple|<tuple|calorimeter|bomb>||c11 sec rc-bomb
      idx1|<tuple|Calorimeter|bomb>|<pageref|auto-187>>

      <tuple|<tuple|bomb calorimeter>||c11 sec rc-bomb idx2|<tuple|Bomb
      calorimeter>|<pageref|auto-188>>

      <tuple|<tuple|Calorimetry|bomb>|<pageref|auto-189>>

      <tuple|<tuple|enthalpy|combustion>|||<tuple|Enthalpy|of combustion,
      standard molar>|<pageref|auto-190>>

      <tuple|<tuple|Hess's law>|<pageref|auto-191>>

      <tuple|<tuple|Isothermal|bomb process>|<pageref|auto-192>>

      <tuple|<tuple|enthalpy|combustion>|||<tuple|Enthalpy|of combustion,
      standard molar>|<pageref|auto-194>>

      <tuple|<tuple|Reduction to standard states>|<pageref|auto-195>>

      <tuple|<tuple|Ignition circuit>|<pageref|auto-199>>

      <tuple|<tuple|Circuit|ignition>|<pageref|auto-200>>

      <tuple|<tuple|isothermal|bomb process>||c11 sec rc-bomb-ibp
      idx1|<tuple|Isothermal|bomb process>|<pageref|auto-202>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-204>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-205>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-206>>

      <tuple|<tuple|Ignition circuit>|<pageref|auto-207>>

      <tuple|<tuple|Circuit|ignition>|<pageref|auto-208>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-209>>

      <tuple|<tuple|isothermal|bomb process>||c11 sec rc-bomb-ibp
      idx1|<tuple|Isothermal|bomb process>|<pageref|auto-210>>

      <tuple|<tuple|Kirchhoff equation>|<pageref|auto-212>>

      <tuple|<tuple|Washburn corrections>|<pageref|auto-214>>

      <tuple|<tuple|enthalpy|combustion>|||<tuple|Enthalpy|of combustion,
      standard molar>|<pageref|auto-217>>

      <tuple|<tuple|washburn corrections>||c11 sec rc-bomb-wc
      idx1|<tuple|Washburn corrections>|<pageref|auto-219>>

      <tuple|<tuple|Isothermal|bomb process>|<pageref|auto-220>>

      <tuple|<tuple|washburn corrections>||c11 sec rc-bomb-wc
      idx1|<tuple|Washburn corrections>|<pageref|auto-221>>

      <tuple|<tuple|calorimeter|bomb>||c11 sec rc-bomb
      idx1|<tuple|Calorimeter|bomb>|<pageref|auto-222>>

      <tuple|<tuple|bomb calorimeter>||c11 sec rc-bomb idx2|<tuple|Bomb
      calorimeter>|<pageref|auto-223>>

      <tuple|<tuple|Calorimeter|phase-change>|<pageref|auto-225>>

      <tuple|<tuple|Calorimeter|Bunsen ice>|<pageref|auto-226>>

      <tuple|<tuple|Calorimeter|heat-flow>|<pageref|auto-227>>

      <tuple|<tuple|Calorimeter|isothermal-jacket>|<pageref|auto-228>>

      <tuple|<tuple|Isoperibol calorimeter>|<pageref|auto-229>>

      <tuple|<tuple|Calorimeter|isoperibol>|<pageref|auto-230>>

      <tuple|<tuple|Thermopile>|<pageref|auto-231>>

      <tuple|<tuple|Calorimeter|flame>|<pageref|auto-232>>

      <tuple|<tuple|calorimetry|reaction>||c11 sec rc
      idx1|<tuple|Calorimetry|reaction>|<pageref|auto-233>>

      <tuple|<tuple|adiabatic|flame temperature>||c11 sec aft
      idx1|<tuple|Adiabatic|flame temperature>|<pageref|auto-235>>

      <tuple|<tuple|adiabatic|flame temperature>||c11 sec aft
      idx1|<tuple|Adiabatic|flame temperature>|<pageref|auto-236>>

      <tuple|<tuple|Gibbs energy|molar reaction>|<pageref|auto-239>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-242>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-243>>

      <tuple|<tuple|Affinity of reaction>|<pageref|auto-244>>

      <tuple|<tuple|Equilibrium|reaction>|<pageref|auto-245>>

      <tuple|<tuple|equilibrium conditions>||c11 sec gere-derivation
      idx1|<tuple|Equilibrium conditions|for reaction>|<pageref|auto-247>>

      <tuple|<tuple|reaction|between pure phases>||c11 sec gere-pure-phases
      idx1|<tuple|Reaction|between pure phases>|<pageref|auto-249>>

      <tuple|<tuple|Equilibrium|phase transition>|<pageref|auto-251>>

      <tuple|<tuple|Phase|transition|equilibrium>|<pageref|auto-252>>

      <tuple|<tuple|reaction|between pure phases>||c11 sec gere-pure-phases
      idx1|<tuple|Reaction|between pure phases>|<pageref|auto-253>>

      <tuple|<tuple|reaction|mixture>||c11 sec gere-mixtures
      idx1|<tuple|Reaction|in a mixture>|<pageref|auto-255>>

      <tuple|<tuple|reaction|mixture>||c11 sec gere-mixtures
      idx1|<tuple|Reaction|in a mixture>|<pageref|auto-257>>

      <tuple|<tuple|reaction|ideal gas mixture>||c11 sec gere-ideal-gas
      idx1|<tuple|Reaction|in an ideal gas mixture>|<pageref|auto-259>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-260>>

      <tuple|<tuple|reaction|ideal gas mixture>||c11 sec gere-ideal-gas
      idx1|<tuple|Reaction|in an ideal gas mixture>|<pageref|auto-263>>

      <tuple|<tuple|equilibrium conditions>||c11 sec gere-derivation
      idx1|<tuple|Equilibrium conditions|for reaction>|<pageref|auto-264>>

      <tuple|<tuple|Gibbs energy|reaction, standard
      molar>|<pageref|auto-267>>

      <tuple|<tuple|Reaction|quotient>|<pageref|auto-269>>

      <tuple|<tuple|Activity quotient>|<pageref|auto-270>>

      <tuple|<tuple|Stoichiometric|number>|<pageref|auto-272>>

      <tuple|<tuple|Proper quotient>|<pageref|auto-273>>

      <tuple|<tuple|Thermodynamic|equilibrium constant>|<pageref|auto-275>>

      <tuple|<tuple|Equilibrium constant|thermodynamic>|<pageref|auto-276>>

      <tuple|<tuple|IUPAC Green Book>|<pageref|auto-278>>

      <tuple|<tuple|reaction|gas phase>||c11 sec tec-reaction-gas
      idx1|<tuple|Reaction|in a gas phase>|<pageref|auto-280>>

      <tuple|<tuple|equilibrium constant|pressure basis>|||<tuple|Equilibrium
      constant|on a pressure basis>|<pageref|auto-281>>

      <tuple|<tuple|reaction|gas phase>||c11 sec tec-reaction-gas
      idx1|<tuple|Reaction|in a gas phase>|<pageref|auto-283>>

      <tuple|<tuple|reaction|solution>||c11 sec tec-reaction-solution
      idx1|<tuple|Reaction|in solution>|<pageref|auto-285>>

      <tuple|<tuple|reaction|solution>||c11 sec tec-reaction-solution
      idx1|<tuple|Reaction|in solution>|<pageref|auto-286>>

      <tuple|<tuple|gibbs energy|formation standard>|||<tuple|Gibbs energy|of
      formation, standard molar>|<pageref|auto-288>>

      <tuple|<tuple|calorimetry|evaluate an equilibrium
      constant>|||<tuple|Calorimetry|to evaluate an equilibrium
      constant>|<pageref|auto-290>>

      <tuple|<tuple|gibbs energy|formation standard|ion>|||<tuple|Gibbs
      energy|of formation, standard molar|of an ion>|<pageref|auto-291>>

      <tuple|<tuple|equilibrium|position, effect of T and p on>||c11 sec
      etpep idx1|<tuple|Equilibrium|position, effect of
      <with|mode|<quote|math>|T> and <with|mode|<quote|math>|p>
      on>|<pageref|auto-293>>

      <tuple|<tuple|le chatelier's>|||<tuple|Le Ch�telier's
      principle>|<pageref|auto-294>>

      <tuple|<tuple|enthalpy|solution|molar integral>|||<tuple|Enthalpy|of
      solution|molar integral>|<pageref|auto-295>>

      <tuple|<tuple|le chatelier's>|||<tuple|Le Ch�telier's
      principle>|<pageref|auto-296>>

      <tuple|<tuple|equilibrium|position, effect of T and p on>||c11 sec
      etpep idx1|<tuple|Equilibrium|position, effect of
      <with|mode|<quote|math>|T> and <with|mode|<quote|math>|p>
      on>|<pageref|auto-297>>
    </associate>
    <\associate|parts>
      <tuple|bio-HESS.tm|chapter-nr|11|section-nr|3|subsection-nr|4>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|11<space|2spc>Reactions
      and Other Chemical Processes> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      11.1<space|2spc>Mixing Processes <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>

      <with|par-left|<quote|1tab>|11.1.1<space|2spc>Mixtures in general
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|1tab>|11.1.2<space|2spc>Ideal mixtures
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>

      <with|par-left|<quote|1tab>|11.1.3<space|2spc>Excess quantities
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-26>>

      <with|par-left|<quote|1tab>|11.1.4<space|2spc>The entropy change to
      form an ideal gas mixture <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-32>>

      <with|par-left|<quote|1tab>|11.1.5<space|2spc>Molecular model of a
      liquid mixture <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-36>>

      <with|par-left|<quote|1tab>|11.1.6<space|2spc>Phase separation of a
      liquid mixture <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-45>>

      11.2<space|2spc>The Advancement and Molar Reaction Quantities
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-51>

      <with|par-left|<quote|1tab>|11.2.1<space|2spc>An example: ammonia
      synthesis <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-60>>

      <with|par-left|<quote|1tab>|11.2.2<space|2spc>Molar reaction quantities
      in general <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-66>>

      <with|par-left|<quote|1tab>|11.2.3<space|2spc>Standard molar reaction
      quantities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-78>>

      11.3<space|2spc>Molar Reaction Enthalpy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-82>

      <with|par-left|<quote|1tab>|11.3.1<space|2spc>Molar reaction enthalpy
      and heat <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-83>>

      <with|par-left|<quote|1tab>|11.3.2<space|2spc>Standard molar enthalpies
      of reaction and formation <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-90>>

      <with|par-left|<quote|1tab>|11.3.3<space|2spc>Molar reaction heat
      capacity <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-108>>

      <with|par-left|<quote|1tab>|11.3.4<space|2spc>Effect of temperature on
      reaction enthalpy <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-110>>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Germain
      Henri Hess> (1802\U1850) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-116><vspace|0.15fn>>

      11.4<space|2spc>Enthalpies of Solution and Dilution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-118>

      <with|par-left|<quote|1tab>|11.4.1<space|2spc>Molar enthalpy of
      solution <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-127>>

      <with|par-left|<quote|1tab>|11.4.2<space|2spc>Enthalpy of dilution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-139>>

      <with|par-left|<quote|1tab>|11.4.3<space|2spc>Molar enthalpies of
      solute formation <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-147>>

      <with|par-left|<quote|1tab>|11.4.4<space|2spc>Evaluation of relative
      partial molar enthalpies <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-150>>

      11.5<space|2spc>Reaction Calorimetry
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-165>

      <with|par-left|<quote|1tab>|11.5.1<space|2spc>The constant-pressure
      reaction calorimeter <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-170>>

      <with|par-left|<quote|1tab>|11.5.2<space|2spc>The bomb calorimeter
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-186>>

      <with|par-left|<quote|2tab>|Experimental
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-197>>

      <with|par-left|<quote|2tab>|The isothermal bomb process
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-201>>

      <with|par-left|<quote|2tab>|Correction to the reference temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-211>>

      <with|par-left|<quote|2tab>|Reduction to standard states
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-213>>

      <with|par-left|<quote|2tab>|Standard molar enthalpy change
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-216>>

      <with|par-left|<quote|2tab>|Washburn corrections
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-218>>

      <with|par-left|<quote|1tab>|11.5.3<space|2spc>Other calorimeters
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-224>>

      11.6<space|2spc>Adiabatic Flame Temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-234>

      11.7<space|2spc>Gibbs Energy and Reaction Equilibrium
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-237>

      <with|par-left|<quote|1tab>|11.7.1<space|2spc>The molar reaction Gibbs
      energy <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-238>>

      <with|par-left|<quote|1tab>|11.7.2<space|2spc>Spontaneity and reaction
      equilibrium <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-241>>

      <with|par-left|<quote|1tab>|11.7.3<space|2spc>General derivation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-246>>

      <with|par-left|<quote|1tab>|11.7.4<space|2spc>Pure phases
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-248>>

      <with|par-left|<quote|1tab>|11.7.5<space|2spc>Reactions involving
      mixtures <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-254>>

      <with|par-left|<quote|1tab>|11.7.6<space|2spc>Reaction in an ideal gas
      mixture <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-258>>

      11.8<space|2spc>The Thermodynamic Equilibrium Constant
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-265>

      <with|par-left|<quote|1tab>|11.8.1<space|2spc>Activities and the
      definition of <with|mode|<quote|math>|K>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-266>>

      <with|par-left|<quote|1tab>|11.8.2<space|2spc>Reaction in a gas phase
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-279>>

      <with|par-left|<quote|1tab>|11.8.3<space|2spc>Reaction in solution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-284>>

      <with|par-left|<quote|1tab>|11.8.4<space|2spc>Evaluation of
      <with|mode|<quote|math>|K> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-287>>

      11.9<space|2spc>Effects of Temperature and Pressure on Equilibrium
      Position <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-292>
    </associate>
  </collection>
</auxiliary>