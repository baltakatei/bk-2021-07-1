<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <no-indent>BIOGRAPHICAL SKETCH

  <paragraph|<person|James Prescott Joule> (1818\U1889)>

  <htab|5mm><image|BIO/joule.png|76pt|115pt||><htab|5mm>

  <label|bio:joule><index|Joule, James Prescott><no-indent>James Joule drove
  the final nails into the coffin of the caloric theory by his experimental
  demonstrations of the mechanical equivalent of heat.

  Joule (pronounced like \Pjewel\Q) was born in Salford, near Manchester,
  England. His father was a prosperous brewery owner; after his death, James
  and one of his brothers carried on the business until it was sold in 1854.

  Joule was a sickly child with a minor spinal weakness. He was tutored at
  home, and at the age of 16 was a pupil of the atomic theory advocate John
  Dalton.

  As an adult, Joule was a political conservative and a member of the Church
  of England. He dressed plainly, was of a somewhat nervous disposition, and
  was a poor speaker. He was shy and reserved unless with friends, had a
  strong sense of humor, and loved nature.

  Joule never attended a university or had a university appointment, but as
  an \Pamateur\Q scientist and inventor he published over 100 papers (some of
  them jointly with collaborators) and received many honors. He invented arc
  welding and a mercury displacement pump. He carried out investigations on
  electrical heating and, in collaboration with William Thomson, on the
  cooling accompanying the expansion of a gas through a porous plug (the
  Joule\UThomson experiment). The <em|joule>, of course, is now the SI
  derived unit of energy.

  Joule's best-known experiment was the determination of the mechanical
  equivalent of heat using a paddle wheel to agitate water (Sec.
  <reference|3-Joule paddle wheel> and Prob. 3.<reference|prb:3-Joule_expt>).
  He reported his results in 1845, and published a more refined measurement
  in 1850.<footnote|Ref. <cite|joule-1850>.>

  In a note dated 1885 in his <em|Collected Papers>, Joule wrote:

  <quotation|It was in the year 1843 that I read a paper \POn the Calorific
  Effects of Magneto-Electricity and the Mechanical Value of Heat\Q to the
  Chemical Section of the British Association assembled at Cork. With the
  exception of some eminent men ...the subject did not excite much general
  attention; so that when I brought it forward again at the meeting in 1847,
  the chairman suggested that, as the business of the section pressed, I
  should not read my paper, but confine myself to a short verbal description
  of my experiments. This I endeavoured to do, and discussion not being
  invited, the communication would have passed without comment if a young man
  had not risen in the section, and by his intelligent observations created a
  lively interest in the new theory. The young man was William Thomson, who
  had two years previously passed the University of Cambridge with the
  highest honour, and is now probably the foremost scientific authority of
  the age.>

  The William Thomson mentioned in Joule's note later became Lord Kelvin.
  Thomson described introducing himself to Joule after the 1847 meeting,
  which was in Oxford, as a result of which the two became collaborators and
  life-long friends. Thomson wrote:<footnote|Ref. <cite|bottomley-1882>.>

  <quotation|Joule's paper at the Oxford meeting made a great sensation.
  Faraday was there and was much struck with it, but did not enter fully into
  the new views. It was many years after that before any of the scientific
  chiefs began to give their adhesion.>

  According to a biographer:<footnote|Ref. <cite|joule-1892>.>

  <quotation|His modesty was always notable. 'I believe,' he told his brother
  on 14 Sept. 1887, 'I have done two or three little things, but nothing to
  make a fuss about.' During the later years of his life he received many
  distinctions both English and foreign.>
</body>

<\initial>
  <\collection>
    <associate|font-base-size|9>
    <associate|page-medium|paper>
    <associate|par-columns|2>
    <associate|par-columns-sep|1fn>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|Joule, James Prescott|1>>
    <associate|bio:joule|<tuple|1|1>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|1>>
    <associate|footnote-3|<tuple|3|1>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|1>>
    <associate|footnr-3|<tuple|3|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      joule-1850

      bottomley-1882

      joule-1892
    </associate>
    <\associate|idx>
      <tuple|<tuple|Joule, James Prescott>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <with|par-left|<quote|4tab>|Biographical
      Sketch<next-line><with|font-shape|<quote|small-caps>|James Prescott
      Joule> (1818\U1889) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>