<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;
  </hide-preamble>

  <chapter|The Third Law and Cryogenics><label|Chap. 6><label|c6>

  The third law of thermodynamics concerns the entropy of perfectly-ordered
  crystals at zero kelvins.

  When a chemical reaction or phase transition is studied at low
  temperatures, and all substances are pure crystals presumed to be perfectly
  ordered, the entropy change is found to approach zero as the temperature
  approaches zero kelvins:

  <equation-cov2|<label|lim(T=0)del(r)S=0>lim<rsub|T<space|-0.17em><ra>0><Del>S=0|(pure,
  perfectly\Uordered crystals)>

  Equation <reference|lim(T=0)del(r)S=0> is the mathematical statement of the
  <subindex|Nernst|heat theorem><em|Nernst heat
  theorem><footnote|<index|Nernst, Walther>Nernst preferred to avoid the use
  of the entropy function and to use in its place the partial derivative
  <math|-<pd|A|T|V>> (Eq. <reference|dA/dT=-S>). The original 1906 version of
  his heat theorem was in the form <math|lim<rsub|T<space|-0.17em><ra>0><pd|<Del>A|T|V|=>0>
  (Ref. <cite|cropper-87>).> or <index|Third law of
  thermodynamics><newterm|third law of thermodynamics>. It is true in general
  only if each reactant and product is a pure crystal with identical unit
  cells arranged in perfect spatial order.

  <section|The Zero of Entropy><label|6-entropy zero><label|c6 sec zoe>

  <index-complex|<tuple|entropy|zero of>||c6 sec zoe idx1|<tuple|Entropy|zero
  of>>There is no theoretical relation between the entropies of different
  chemical elements. We can arbitrarily choose the entropy of every pure
  crystalline element to be zero at zero kelvins. Then the experimental
  observation expressed by Eq. <reference|lim(T=0)del(r)S=0> requires that
  the entropy of every pure crystalline <em|compound> also be zero at zero
  kelvins, in order that the entropy change for the formation of a compound
  from its elements will be zero at this temperature.

  A classic statement of the third law principle appears in the 1923 book
  <em|Thermodynamics and the Free Energy of Chemical Substances> by
  <index|Lewis, Gilbert Newton>G. N. Lewis and <index|Randall, Merle>M.
  Randall:<footnote|Ref. <cite|lewis-23>, p. 448.>

  <\quote-env>
    \PIf the entropy of each element in some crystalline state be taken as
    zero at the absolute zero of temperature: <em|every substance has a
    finite positive entropy, but at the absolute zero of temperature the
    entropy may become zero, and does so become in the case of perfect
    crystalline substances>.\Q
  </quote-env>

  According to this principle, every substance (element or compound) in a
  pure, perfectly-ordered crystal at <math|0<K>>, at any
  pressure,<footnote|The entropy becomes independent of pressure as <math|T>
  approaches zero kelvins. This behavior can be deduced from the relation
  <math|<pd|S|p|T>=-\<alpha\>*V> (Table <vpageref|tbl:7-const T>) combined
  with the experimental observation that the cubic expansion coefficient
  <math|\<alpha\>> approaches zero as <math|T> approaches zero kelvins.> has
  a molar entropy of zero:

  <equation-cov2|S<m><around*|(|0 <text|K>|)>=0|(pure, perfectly\Uordered
  crystal)>

  This convention establishes a scale of absolute entropies at temperatures
  above zero kelvins called <index-complex|<tuple|third law
  entropy>|||<tuple|Third-law entropy>><subindex|Entropy|third-law><newterm|third-law
  entropies>, as explained in the next section.

  <index-complex|<tuple|entropy|zero of>||c6 sec zoe idx1|<tuple|Entropy|zero
  of>>

  <section|Molar Entropies><label|6-molar entropies><label|c6 sec me>

  <index-complex|<tuple|entropy|molar>||c6 sec me
  idx1|<tuple|Entropy|molar>>With the convention that the entropy of a pure,
  perfectly-ordered crystalline solid at zero kelvins is zero, we can
  establish the third-law value of the molar entropy of a pure substance at
  any temperature and pressure. Absolute values of <math|S<m>> are what are
  usually tabulated for calculational use.

  <subsection|Third-law molar entropies><label|6-third law molar
  entropies><label|c6 sec me-3l>

  h to evaluate the entropy of an amount <math|n> of a pure substance at a
  certain temperature <math|T<rprime|'>> and a certain pressure. The same
  substance, in a perfectly-ordered crystal at zero kelvins and the same
  pressure, has an entropy of zero. The entropy at the temperature and
  pressure of interest, then, is the entropy change
  <math|<Del>S=<big|int><rsub|0><rsup|T<rprime|'>><space|-0.17em><dq>/T> of a
  reversible heating process at constant pressure that converts the
  perfectly-ordered crystal at zero kelvins to the state of interest.

  Consider a reversible isobaric heating process of a pure substance while it
  exists in a single phase. The definition of heat capacity as
  <math|<dq>/<dif>T> (Eq. <reference|heat capacity def>) allows us to
  substitute <math|C<rsub|p>*<dif>T> for <math|<dq>>, where <math|C<rsub|p>>
  is the heat capacity of the phase at constant pressure.

  If the substance in the state of interest is a liquid or gas, or a crystal
  of a different form than the perfectly-ordered crystal present at zero
  kelvins, the heating process will include one or more
  <subsubindex|Phase|transition|equilibrium>equilibrium phase transitions
  under conditions where two phases are in equilibrium at the same
  temperature and pressure (Sec. <reference|2-phase coexistence>). For
  example, a reversible heating process at a pressure above the triple point
  that transforms the crystal at <math|0 <text|K>> to a gas may involve
  transitions from one crystal form to another, and also melting and
  vaporization transitions.

  Each such reversible phase transition requires positive heat
  <math|q<rsub|<text|trs>>>. Because the pressure is constant, the heat is
  equal to the enthalpy change (Eq. <reference|delH=q (dp=0)>). The ratio
  <math|q<rsub|<text|trs>>/n> is called the molar heat or molar enthalpy of
  the transition, <math|\<Delta\><rsub|<text|trs>>H> (see Sec.
  <reference|8-molar trans quantities>). Because the phase transition is
  reversible, the entropy change during the transition is given by
  <math|\<Delta\><rsub|<text|trs>>S=q<rsub|<text|trs>>/n*T<rsub|<text|trs>>>
  where <math|T<rsub|<text|trs>>> is the transition temperature.

  With these considerations, we can write the following expression for the
  entropy change of the entire heating process:

  <\equation>
    <Del>S=<big|int><rsub|0><rsup|T<rprime|'>><space|-0.17em><frac|C<rsub|p>|T>*<dif>T+<big|sum><frac|n*\<Delta\><rsub|<text|trs>>*H|T<rsub|<text|trs>>>
  </equation>

  The resulting operational equation for the calculation of the
  <index-complex|<tuple|entropy|molar|calorimetry>|||<tuple|Entropy|molar|from
  calorimetry>><em|molar> entropy of the substance at the temperature and
  pressure of interest is

  <\equation-cov2|<label|Sm=int(Cpm/T)dT+sum(delH/T)>S<m><around|(|T<rprime|'>|)>=<frac|<Del>S|n>=<big|int><rsub|0><rsup|T<rprime|'>><frac|<Cpm>|T>*<dif>T+<big|sum><frac|\<Delta\><rsub|<text|trs>>*H|T<rsub|<text|trs>>>>
    (pure substance

    constant <math|p>)
  </equation-cov2>

  where <math|<Cpm>=C<rsub|p>/n> is the molar heat capacity at constant
  pressure. The summation is over each equilibrium phase transition occurring
  during the heating process.

  Since <math|<Cpm>> is positive at all temperatures above zero kelvins, and
  <math|<Delsub|trs>H> is positive for all transitions occurring during a
  reversible heating process, the molar entropy of a substance is
  <em|positive> at all temperatures above zero kelvins.

  The heat capacity and transition enthalpy data required to evaluate
  <math|S<m><around|(|T<rprime|'>|)>> using Eq.

  <reference|Sm=int(Cpm/T)dT+sum(delH/T)> come from calorimetry. The
  calorimeter can be cooled to about <math|10<K>> with liquid hydrogen, but
  it is difficult to make measurements below this temperature.
  <subindex|Statistical mechanics|Debye crystal theory>Statistical mechanical
  theory may be used to approximate the part of the integral in Eq.
  <reference|Sm=int(Cpm/T)dT+sum(delH/T)> between zero kelvins and the lowest
  temperature at which a value of <math|<Cpm>> can be measured. The
  appropriate formula for nonmagnetic nonmetals comes from the <index|Debye
  crystal theory>Debye theory for the lattice vibration of a monatomic
  crystal. This theory predicts that at low temperatures (from <math|0<K>> to
  about <math|30<K>>), the molar heat capacity at constant volume is
  proportional to <math|T<rsup|3>>: <math|<CVm>=a*T<rsup|3>>, where <math|a>
  is a constant. For a solid, the molar heat capacities at constant volume
  and at constant pressure are practically equal. Thus for the integral on
  the right side of Eq. <reference|Sm=int(Cpm/T)dT+sum(delH/T)> we can, to a
  good approximation, write

  <\equation>
    <label|int(Cp/T)dT=><big|int><rsub|0><rsup|T<rprime|'>><frac|<Cpm>|T>*<dif>T=a*<big|int><rsub|0><rsup|T<rprime|''>><space|-0.17em><space|-0.17em>T<rsup|2>*<dif>T+<big|int><rsub|T<rprime|''>><rsup|T<rprime|'>><frac|<Cpm>|T><dif>T
  </equation>

  where <math|T<rprime|''>> is the lowest temperature at which <math|<Cpm>>
  is measured. The first term on the right side of Eq.
  <reference|int(Cp/T)dT=> is

  <\equation>
    a*<big|int><rsub|0><rsup|T<rprime|''>><space|-0.17em><space|-0.17em>T<rsup|2><dif>T=<around*|\<nobracket\>|<around|(|a*T<rsup|3>/3|)>|\|><rsub|0><rsup|T<rprime|''>>=a<around|(|T<rprime|''>|)><rsup|3>/3
  </equation>

  But <math|a<around|(|T<rprime|''>|)><rsup|3>> is the value of <math|<Cpm>>
  at <math|T<rprime|''>>, so Eq. <reference|Sm=int(Cpm/T)dT+sum(delH/T)>
  becomes

  <\equation-cov2|<label|Sm=Cpm/3+int(Cpm/T)dT+sum(delH/T)>S<m><around|(|T<rprime|'>|)>=<frac|<Cpm><around|(|T<rprime|''>|)>|3>+<big|int><rsub|T<rprime|''>><rsup|T<rprime|'>><frac|<Cpm>|T>*<dif>T+<big|sum><frac|\<Delta\><rsub|<text|trs>>*H|T<rsub|<text|trs>>>>
    (pure substance,

    constant <math|p>)
  </equation-cov2>

  <\quote-env>
    \ In the case of a metal, <subindex|Statistical mechanics|molar heat
    capacity of a metal>statistical mechanical theory predicts an electronic
    contribution to the molar heat capacity, proportional to <math|T> at low
    temperature, that should be added to the Debye <math|T<rsup|3>> term:
    <math|<Cpm>=a*T<rsup|3>+b*T>. The error in using Eq.
    <reference|Sm=Cpm/3+int(Cpm/T)dT+sum(delH/T)>, which ignores the
    electronic term, is usually negligible if the heat capacity measurements
    are made down to about <math|10 <text|K>>.
  </quote-env>

  We may evaluate the integral on the right side of Eq.
  <reference|Sm=Cpm/3+int(Cpm/T)dT+sum(delH/T)> by numerical integration. We
  need the area under the curve of <math|<Cpm>/T> plotted as a function of
  <math|T> between some low temperature, <math|T<rprime|''>>, and the
  temperature <math|T<rprime|'>> at which the molar entropy is to be
  evaluated. Since the integral may be written in the form

  <\equation>
    <label|int(Cpm/T)dT=int(Cpm)dlnT><big|int><rsub|T<rprime|''>><rsup|T<rprime|'>><frac|<Cpm>|T><dif>T=<big|int><rsub|T=T<rprime|''>><rsup|T=T<rprime|'>><Cpm>*<dif>ln
    <around|(|T/<text|K>|)>
  </equation>

  we may also evaluate the integral from the area under a curve of
  <math|<Cpm>> plotted as a function of <math|ln <around|(|T/K|)>>.

  The procedure of evaluating the entropy from the heat capacity is
  illustrated for the case of hydrogen chloride in Fig. <vpageref|fig:6-HCl
  entropy>.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|06-SUP/HCl-S.eps|381pt|354pt||>
      <|big-figure>
        Properties of hydrogen chloride (HCl): the dependence of
        <math|<Cpm>>, <math|<Cpm>/T>, and <math|S<m>> on temperature at a
        pressure of <math|1 <text|bar>>. The discontinuities are at a
        solid<math|<ra>>solid phase transition, the melting temperature, and
        the vaporization temperature. (Condensed-phase data from Ref.
        <cite|giauque-28>; gas-phase data from Ref. <cite|NIST-98>, p.
        762.)<label|fig:6-HCl entropy>
      </big-figure>
    </framed>
  </float>

  The areas under the curves of <math|<Cpm>/T> versus <math|T>, and of
  <math|<Cpm>> versus <math|ln <around|(|T/<text|K>|)>>, in a given
  temperature range are numerically identical (Eq.
  <reference|int(Cpm/T)dT=int(Cpm)dlnT>). Either curve may be used in Eq.
  <reference|Sm=int(Cpm/T)dT+sum(delH/T)> to find the dependence of
  <math|S<m>> on <math|T>. Note how the molar entropy increases continuously
  with increasing <math|T> and has a discontinuity at each phase transition.

  <\quote-env>
    \ As explained in Sec. <reference|6-entropy zero>, by convention the zero
    of entropy of any substance refers to the pure, perfectly-ordered crystal
    at zero kelvins. In practice, experimental entropy values depart from
    this convention in two respects. First, an element is usually a mixture
    of two or more isotopes, so that the substance is not isotopically pure.
    Second, if any of the nuclei have spins, weak interactions between the
    nuclear spins in the crystal would cause the spin orientations to become
    ordered at a very low temperature. Above <math|1<K>>, however, the
    orientation of the nuclear spins become essentially random, and this
    change of orientation is not included in the Debye <math|T<rsup|3>>
    formula.

    The neglect of these two effects results in a
    <subsubindex|Entropy|scale|practical><subsubindex|Entropy|scale|conventional><em|practical
    entropy scale>, or conventional entropy scale, on which the crystal that
    is assigned an entropy of zero has randomly-mixed isotopes and
    randomly-oriented nuclear spins, but is pure and ordered in other
    respects. This is the scale that is used for published values of absolute
    ``third-law'' molar entropies. The shift of the zero away from a
    completely-pure and perfectly-ordered crystal introduces no inaccuracies
    into the calculated value of <math|<Del>S> for any process occurring
    above <math|1<K>>, because the shift is the same in the initial and final
    states. That is, isotopes remain randomly mixed and nuclear spins remain
    randomly oriented.
  </quote-env>

  <subsection|Molar entropies from spectroscopic measurements><label|6-S of
  gas from stat mech><label|c6 sec-me-spectroscopic>

  <subindex|Statistical mechanics|molar entropy of a gas>Statistical
  mechanical theory applied to spectroscopic measurements provides an
  accurate means of evaluating the molar entropy of a pure ideal gas from
  experimental molecular properties. This is often the preferred method of
  evaluating <math|S<m>> for a gas. The zero of entropy is the same as the
  practical entropy scale\Vthat is, isotope mixing and nuclear spin
  interactions are ignored. Intermolecular interactions are also ignored,
  which is why the results apply only to an ideal gas.

  <\quote-env>
    \ The statistical mechanics formula writes the molar entropy as the sum
    of a translational contribution and an internal contribution:
    <math|S<m>=S<rsub|<text|m>,<text|trans>>+S<rsub|<text|m>,<text|int>>>.
    The translational contribution is given by the Sackur--Tetrode equation:

    <\equation>
      S<rsub|<text|m>,<text|trans>>=R*ln <frac|<around|(|2*\<pi\>*M|)><rsup|3/2>*<around|(|R*T|)><rsup|5/2>|p*h<rsup|3>*N<rsub|<text|A>><rsup|4>>+<around|(|5/2|)>*R
    </equation>

    Here <math|h> is the Planck constant and <math|N<rsub|<text|A>>> is the
    Avogadro constant. The internal contribution is given by

    <\equation>
      S<rsub|<text|m>,<text|int>>=R*ln q<rsub|<text|int>>+R*T*<around|(|<dif>ln
      q<rsub|<text|int>>/<dif>T|)>
    </equation>

    where <math|q<rsub|<text|int>>> is the molecular partition function
    defined by

    <\equation>
      <label|q(int)=>q<rsub|<text|int>>=<big|sum><rsub|i>exp
      <space|0.17em><around|(|-\<epsilon\><rsub|i>/k*T|)>
    </equation>

    In Eq. <reference|q(int)=>, <math|\<epsilon\><rsub|i>> is the energy of a
    molecular quantum state relative to the lowest energy level, <math|k> is
    the Boltzmann constant, and the sum is over the quantum states of one
    molecule with appropriate averaging for natural isotopic abundance. The
    experimental data needed to evaluate <math|q<rsub|<text|int>>> consist of
    the energies of low-lying electronic energy levels, values of electronic
    degeneracies, fundamental vibrational frequencies, rotational constants,
    and other spectroscopic parameters.
  </quote-env>

  When the spectroscopic method is used to evaluate <math|S<m>> with <math|p>
  set equal to the standard pressure <math|p<st>=1<br>>, the value is the
  <subindex|Entropy|standard molar><em|standard> molar entropy,
  <math|S<m><st>>, of the substance in the gas phase. This value is useful
  for thermodynamic calculations even if the substance is not an ideal gas at
  the standard pressure, as will be discussed in Sec. <reference|7-st molar
  fncs of a gas>.

  <subsection|Residual entropy><label|c6 sec me-residual>

  <index-complex|<tuple|residual entropy>||c6 sec me-residual
  idx1|<tuple|Residual entropy>><index-complex|<tuple|entropy|residual>||c6
  sec me-residual idx2|<tuple|Entropy|residual>>Ideally, the molar entropy
  values obtained by the calorimetric (third-law) method for a gas should
  agree closely with the values calculated from spectroscopic data. Table
  <vpageref|tbl:6-residual_S> shows that for some substances this agreement
  is not present. The table lists values of <math|S<m><st>> for ideal gases
  at <math|298.15<K>> evaluated by both the calorimetric and spectroscopic
  methods. The quantity <math|S<rsub|<text|m>,0><rsub|>> in the last column
  is the difference between the two <math|S<m><st>> values, and is called the
  molar <em|residual entropy>.<float|float|thb|<\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|2|2|cell-col-span|2>|<cwith|1|1|2|2|cell-halign|c>||<cwith|1|1|2|3|cell-bborder|1ln>|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|2|2|1|-1|cell-bborder|1ln>||||<cwith|7|7|1|-1|cell-tborder|0ln>||<cwith|-1|-1|1|-1|cell-bborder|2ln>|||<cwith|3|-1|2|-1|cell-halign|C.>|<table|<row|<cell|>|<cell|<math|S<m><st>/<around*|(|<text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>|)>>>|<cell|>|<cell|>>|<row|<cell|Substance>|<cell|calorimetric>|<cell|spectroscopic<footnote|Ref.
    <cite|NIST-98>.>>|<cell|<math|S<rsub|<text|m>,0>/<around*|(|<text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol>|)>>>>|<row|<cell|HCl>|<cell|<math|186.3\<pm\>0.4><footnote|Ref.
    <cite|giauque-28>.>>|<cell|<math|186.901>>|<cell|<math|0.6\<pm\>0.4>>>|<row|<cell|CO>|<cell|<math|193.4\<pm\>0.4><footnote|Ref.
    <cite|clayton-32>.>>|<cell|<math|197.65\<pm\>0.04>>|<cell|<math|4.3\<pm\>0.4>>>|<row|<cell|NO>|<cell|<math|208.0\<pm\>0.4><footnote|Ref.
    <cite|johnston-29>.>>|<cell|<math|210.758>>|<cell|<math|2.8\<pm\>0.4>>>|<row|<cell|N<rsub|<math|2>>O
    (NNO)>|<cell|<math|215.3\<pm\>0.4><footnote|Ref.
    <cite|blue-35>.>>|<cell|<math|219.957>>|<cell|<math|4.7\<pm\>0.4>>>|<row|<cell|H<rsub|<math|2>>O>|<cell|<math|185.4\<pm\>0.2><footnote|Ref.
    <cite|giauque-36>.>>|<cell|<math|188.834\<pm\>0.042>>|<cell|<math|3.4\<pm\>0.2>>>>>>
  <|big-table>
    <label|tbl:6-residual_S>Standard molar entropies of several substances
    (ideal gases at <math|T=298.15<K>> and <math|p=1<br>>) and molar residual
    entropies
  </big-table>>

  In the case of HCl, the experimental value of the residual entropy is
  comparable to its uncertainty, indicating good agreement between the
  calorimetric and spectroscopic methods. This agreement is typical of most
  substances, particularly those like HCl whose molecules are polar and
  asymmetric with a large energetic advantage of forming perfectly-ordered
  crystals.

  The other substances listed in Table <vpageref|tbl:6-residual_S> have
  residual entropies that are greater than zero within the uncertainty of the
  data. What is the meaning of this discrepancy between the calorimetric and
  spectroscopic results? We can assume that the true values of
  <math|S<m><st>> at <math|298.15<K>> are the <em|spectroscopic> values,
  because their calculation assumes the solid has only one microstate at
  <math|0<K>>, with an entropy of zero, and takes into account all of the
  possible accessible microstates of the ideal gas. The <em|calorimetric>
  values, on the other hand, are based on Eq.
  <reference|Sm=int(Cpm/T)dT+sum(delH/T)> which assumes the solid becomes a
  perfectly-ordered crystal as the temperature approaches
  <math|0<K>>.<footnote|The calorimetric values in Table
  <reference|tbl:6-residual_S> were calculated as follows. Measurements of
  heat capacities and heats of transition were used in Eq.
  <reference|Sm=int(Cpm/T)dT+sum(delH/T)> to find the third-law value of
  <math|S<m>> for the vapor at the boiling point of the substance at
  <math|p=1 <text|atm>>. This calculated value for the gas was corrected to
  that for the ideal gas at <math|p=1<br>> and adjusted to <math|T=298.15<K>>
  with spectroscopic data.>

  The conventional explanation of a nonzero residual entropy is the presence
  of random rotational orientations of molecules in the solid at the lowest
  temperature at which the heat capacity can be measured, so that the
  crystals are not perfectly ordered. The random structure is established as
  the crystals form from the liquid, and becomes frozen into the crystals as
  the temperature is lowered below the freezing point. This tends to happen
  with almost-symmetric molecules with small dipole moments which in the
  crystal can have random rotational orientations of practically equal
  energy. In the case of solid H<rsub|<math|2>>O it is the arrangement of
  intermolecular hydrogen bonds that is random. Crystal imperfections such as
  dislocations can also contribute to the residual entropy. If such crystal
  imperfection is present at the lowest experimental temperature, the
  calorimetric value of <math|S<m><st>> for the gas at <math|298.15<K>> is
  the molar entropy increase for the change at <math|1<br>> from the
  imperfectly-ordered solid at <math|0<K>> to the ideal gas at
  <math|298.15<K>>, and the residual entropy <math|S<rsub|<text|m>,0>> is the
  molar entropy of this imperfectly-ordered
  solid.<index-complex|<tuple|residual entropy>||c6 sec me-residual
  idx1|<tuple|Residual entropy>><index-complex|<tuple|entropy|residual>||c6
  sec me-residual idx2|<tuple|Entropy|residual>><index-complex|<tuple|entropy|molar>||c6
  sec me idx1|<tuple|Entropy|molar>>

  <section|Cryogenics><label|6-cryogenics><label|c6 sec c>

  <index-complex|<tuple|cryogenics>||c6 sec c idx1|<tuple|Cryogenics>>The
  field of cryogenics involves the production of very low temperatures, and
  the study of the behavior of matter at these temperatures. These low
  temperatures are needed to evaluate third-law entropies using calorimetric
  measurements. There are some additional interesting thermodynamic
  applications.

  <subsection|Joule\UThompson expansion><label|6-J-T expansion><label|c6 sec
  c-jt>

  A gas can be cooled by expanding it adiabatically with a piston (Sec.
  <reference|3-rev adiab expan>), and a liquid can be cooled by pumping on
  its vapor to cause evaporation (vaporization). An evaporation procedure
  with a refrigerant fluid is what produces the cooling in an ordinary
  kitchen refrigerator.

  For further cooling of a fluid, a common procedure is to use a continuous
  <index|Throttling process><subindex|Process|throttling><newterm|throttling
  process> in which the fluid is forced to flow through a porous plug, valve,
  or other constriction that causes an abrupt drop in pressure. A slow
  continuous adiabatic throttling of a gas is called the
  <subindex|Joule--Thomson|experiment><newterm|Joule--Thomson experiment>, or
  <subindex|Joule--Kelvin|experiment>Joule\UKelvin experiment, after the two
  scientists who collaborated between 1852 and 1862 to design and analyze
  this procedure.<footnote|William Thomson later became Lord Kelvin.>

  The principle of the Joule\UThomson experiment is shown in Fig.
  <vpageref|fig:6-Joule-Thomson>.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|06-SUP/J-T.eps|259pt|87pt||>
      <|big-figure>
        <label|fig:6-Joule-Thomson>Joule\UThomson expansion of a gas through
        a porous plug. The shaded area represents a fixed-amount sample of
        the gas (a) at time <math|t<rsub|1>>; (b) at a later time
        <math|t<rsub|2>>.
      </big-figure>
    </framed>
  </float>

  A tube with thermally insulated walls contains a gas maintained at a
  constant pressure <math|p<rprime|'>> at the left side of a porous plug and
  at a constant lower pressure <math|p<rprime|''>> at the right side. Because
  of the pressure difference, the gas flows continuously from left to right
  through the plug. The flow is slow, and the pressure is essentially uniform
  throughout the portion of the tube at each side of the plug, but has a
  large gradient within the pores of the plug.

  After the gas has been allowed to flow for a period of time, a steady state
  develops in the tube. In this steady state, the gas is assumed to have a
  uniform temperature <math|T<rprime|'>> at the left side of the plug and a
  uniform temperature <math|T<rprime|''>> (not necessarily equal to
  <math|T<rprime|'>>) at the right side of the plug.

  Consider the segment of gas whose position at times <math|t<rsub|1>> and
  <math|t<rsub|2>> is indicated by shading in Fig.
  <reference|fig:6-Joule-Thomson>. This segment contains a fixed amount of
  gas and expands as it moves through the porous plug from higher to lower
  pressure. We can treat this gas segment as a <em|closed system>. During the
  interval between times <math|t<rsub|1>> and <math|t<rsub|2>>, the system
  passes through a sequence of different states, none of which is an
  equilibrium state since the process is irreversible. The energy transferred
  across the boundary by heat is <em|zero>, because the tube wall is
  insulated and there is no temperature gradient at either end of the gas
  segment. We calculate the energy transferred by work at each end of the gas
  segment from <math|<dw>=-p<bd>*<As>*<dx>>, where <math|p<bd>> is the
  pressure (either <math|p<rprime|'>> or <math|p<rprime|''>>) at the moving
  boundary, <math|<As>> is the cross-section area of the tube, and <math|x>
  is the distance along the tube. The result is

  <\equation>
    <label|w(Joule-Thomson)>w=-p<rprime|'>*<around|(|V<rprime|'><rsub|2>-V<rprime|'><rsub|1>|)>-p<rprime|''>*<around|(|V<rprime|''><rsub|2>-V<rprime|''><rsub|1>|)>
  </equation>

  where the meaning of the volumes <math|V<rprime|'><rsub|1>>,
  <math|V<rprime|'><rsub|2>>, and so on is indicated in the figure.

  The internal energy change <math|<Del>U> of the gas segment must be equal
  to <math|w>, since <math|q> is zero. Now let us find the enthalpy change
  <math|<Del>H>. At each instant, a portion of the gas segment is in the
  pores of the plug, but this portion contributes an unchanging contribution
  to both <math|U> and <math|H> because of the steady state. The rest of the
  gas segment is in the portions on either side of the plug, with enthalpies
  <math|U<rprime|'>+p<rprime|'>*V<rprime|'>> at the left and
  <math|U<rprime|''>+p<rprime|''>*V<rprime|''>> at the right. The overall
  enthalpy change of the gas segment must be

  <\equation>
    <Del>H=<Del>U+<around|(|p<rprime|'>*V<rprime|'><rsub|2>+p<rprime|''>*V<rprime|''><rsub|2>|)>-<around|(|p<rprime|'>*V<rprime|'><rsub|1>+p<rprime|''>*V<rprime|''><rsub|1>|)>
  </equation>

  which, when combined with the expression of Eq.
  <reference|w(Joule-Thomson)> for <math|w=<Del>U>, shows that <math|<Del>H>
  is <em|zero>. In other words, the gas segment has the same enthalpy before
  and after it passes through the plug: the throttling process is
  <subindex|Process|isenthalpic><em|isenthalpic>.

  The temperatures <math|T<rprime|'>> and <math|T<rprime|''>> can be measured
  directly. When values of <math|T<rprime|''>> versus <math|p<rprime|''>> are
  plotted for a series of Joule\UThomson experiments having the same values
  of <math|T<rprime|'>> and <math|p<rprime|'>> and different values of
  <math|p<rprime|''>>, the curve drawn through the points is a curve of
  constant enthalpy. The slope at any point on this curve is equal to the
  <subindex|Joule--Thomson|coefficient><newterm|Joule\UThomson coefficient>
  (or <subindex|Joule--Kelvin|coefficient>Joule\UKelvin coefficient) defined
  by

  <\equation>
    <label|mu(JT) def>\<mu\><rsub|<text|JT>><defn><Pd|T|p|H>
  </equation>

  For an ideal gas, <math|\<mu\><rsub|<text|JT>>> is zero because the
  enthalpy of an ideal gas depends only on <math|T> (Prob.
  <reference|prb:5-H(T)>); <math|T> cannot change if <math|H> is constant.
  For a <em|nonideal> gas, <math|\<mu\><rsub|<text|JT>>> is a function of
  <math|T> and <math|p> and the kind of gas.<footnote|See Sec.
  <reference|7-JK coeff> for the relation of the Joule\UThomson coefficient
  to other properties of a gas.> For most gases, at low to moderate pressures
  and at temperatures not much greater than room temperature,
  <math|\<mu\><rsub|<text|JK>>> is positive. Under these conditions, a
  Joule\UThomson expansion to a lower pressure has a cooling effect, because
  <math|T> will decrease as <math|p> decreases at constant <math|H>. Hydrogen
  and helium, however, have negative values of <math|\<mu\><rsub|<text|JK>>>
  at room temperature and must be cooled by other means to about
  <math|200<K>> and <math|40<K>>, respectively, in order for a Joule\UThomson
  expansion to cause further cooling.

  The cooling effect of a Joule\UThomson expansion is often used to cool a
  gas down to its condensation temperature. This procedure can be carried out
  continuously by pumping the gas through the throttle and recirculating the
  cooler gas on the low-pressure side through a heat exchanger to help cool
  the gas on the high-pressure side. Starting at room temperature, gaseous
  nitrogen can be condensed by this means to liquid nitrogen at
  <math|77.4<K>>. The liquid nitrogen can then be used as a cooling bath for
  gaseous hydrogen. At <math|77.4<K>>, hydrogen has a positive Joule\UThomson
  coefficient, so that it in turn can be converted by a throttling process to
  liquid hydrogen at <math|20.3<K>>. Finally, gaseous helium, whose
  Joule\UThomson coefficient is positive at <math|20.3<K>>, can be converted
  to liquid helium at <math|4.2<K>>. Further cooling of the liquid helium to
  about <math|1<K>> can be carried out by pumping to cause rapid evaporation.

  <subsection|Magnetization><label|c6 sec c-magnet>

  The <index-complex|<tuple|work|magnetization>|||<tuple|Work|of
  magnetization>>work of magnetization of an isotropic paramagnetic phase can
  be written <math|<dw><rprime|'>=B*<dif>m<rsub|<text|mag>>>, where <math|B>
  is the magnitude of the <subindex|Magnetic|flux density><index|Flux
  density, magnetic>magnetic flux density and <math|m<rsub|<text|mag>>> is
  the magnitude of the magnetic dipole moment of the phase. The total
  differential of the internal energy of a closed isotropic phase with
  magnetization is given by

  <\equation>
    <label|dU=TdS-pdV+Bdm(mag)><dif>U=T*<dif>S-p*<dif>V+B*<dif>m<rsub|<text|mag>>
  </equation>

  with <math|S>, <math|V>, and <math|m<rsub|<text|mag>>> as the independent
  variables.

  The technique of <subindex|Adiabatic|demagnetization><newterm|adiabatic
  demagnetization> can be used to obtain temperatures below <math|1<K>>. This
  method, suggested by <index|Debye, Peter>Peter Debye in 1926 and
  independently by <index|Giauque, William>William Giauque in 1927, requires
  a paramagnetic solid in which ions with unpaired electrons are sufficiently
  separated that at <math|1<K>> the orientations of the magnetic dipoles are
  almost completely random. Gadolinium(III) sulfate octahydrate,
  <math|<text|G>*<text|d><rsub|2>*<around*|(|<text|SO><rsub|4>|)><rsub|3>\<cdot\>8*<text|H><rsub|2><text|O>>,
  is commonly used.

  Figure <vpageref|fig:6-ad-demag> illustrates the principle of the
  technique. The solid curve shows the temperature dependence of the entropy
  of a paramagnetic solid in the absence of an applied
  <subindex|Magnetic|field><subindex|Field|magnetic>magnetic field, and the
  dashed curve is for the solid in a constant, finite magnetic field. The
  temperature range shown is from <math|0<K>> to approximately <math|1<K>>.
  At <math|0<K>>, the magnetic dipoles are perfectly ordered. The increase of
  <math|S> shown by the solid curve between <math|0<K>> and <math|1<K>> is
  due almost entirely to increasing disorder in the orientations of the
  magnetic dipoles as heat enters the system.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|06-SUP/ad-demag.eps|151pt|104pt||>
      <|big-figure>
        <label|fig:6-ad-demag>Adiabatic demagnetization to achieve a low
        temperature in a paramagnetic solid.
      </big-figure>
    </framed>
  </float>

  \ the process that occurs when the paramagnetic solid, surrounded by
  gaseous helium in thermal contact with liquid helium that has been cooled
  to about <math|1<K>>, is slowly moved into a strong
  <subindex|Magnetic|field><subindex|Field|magnetic>magnetic field. The
  process is <subindex|Isothermal|magnetization><index|Magnetization,
  isothermal><em|isothermal magnetization>, which partially orients the
  magnetic dipoles and reduces the entropy. During this process there is heat
  transfer to the liquid helium, which partially boils away. In path B, the
  thermal contact between the solid and the liquid helium has been broken by
  pumping away the gas surrounding the solid, and the sample is slowly moved
  away from the <subindex|Magnetic|field><subindex|Field|magnetic>magnetic
  field. This step is a reversible adiabatic demagnetization. Because the
  process is reversible and adiabatic, the entropy change is zero, which
  brings the state of the solid to a lower temperature as shown.

  The sign of <math|<pd|T|B|S,p>> is of interest because it tells us the sign
  of the temperature change during a reversible adiabatic demagnetization
  (path B of Fig. <vpageref|fig:6-ad-demag>). To change the independent
  variables in Eq. <reference|dU=TdS-pdV+Bdm(mag)> to <math|S>, <math|p>, and
  <math|B>, we define the <index|Legendre transform>Legendre transform

  <\equation>
    <label|H' defn>H<rprime|'><defn>U+p*V-B*m<rsub|<text|mag>>
  </equation>

  (<math|H<rprime|'>> is sometimes called the
  <subindex|Magnetic|enthalpy><em|magnetic enthalpy>.) From Eqs.
  <reference|dU=TdS-pdV+Bdm(mag)> and <reference|H' defn> we obtain the total
  differential

  <\equation>
    <dif>H<rprime|'>=T<dif>S+V<difp>-m<rsub|<text|mag>>*<dif>B
  </equation>

  From it we find the reciprocity relation

  <\equation>
    <label|dT/dB=-dm/dS><Pd|T|B|S,p>=-<Pd|m<rsub|<text|mag>>|S|p,B>
  </equation>

  <index|Curie's law of magnetization>According to Curie's law of
  magnetization, the magnetic dipole moment <math|m<rsub|<text|mag>>> of a
  paramagnetic phase at constant magnetic flux density <math|B> is
  proportional to <math|1/T>. This law applies when <math|B> is small, but
  even if <math|B> is not small <math|m<rsub|<text|mag>>> decreases with
  increasing <math|T>. To increase the temperature of a phase at constant
  <math|B>, we allow heat to enter the system, and <math|S> then increases.
  Thus, <math|<pd|m<rsub|mag>|S|p,B>> is negative and, according to Eq.
  <reference|dT/dB=-dm/dS>, <math|<pd|T|B|S,p>> must be positive.
  <subindex|Adiabatic|demagnetization>Adiabatic demagnetization is a
  constant-entropy process in which <math|B> decreases, and therefore the
  temperature also <em|decreases>.

  We can find the sign of the entropy change during the
  <subindex|Isothermal|magnetization><index|Magnetization,
  isothermal>isothermal magnetization process shown as path A in Fig.
  <vpageref|fig:6-ad-demag>. In order to use <math|T>, <math|p>, and <math|B>
  as the independent variables, we define the <index|Legendre
  transform>Legendre transform <math|G<rprime|'><defn>H<rprime|'>-T*S>. Its
  total differential is

  <\equation>
    <dif>G<rprime|'>=-S*<dif>T+V*<difp>-m<rsub|<text|mag>>*<dif>B
  </equation>

  From this total differential, we obtain the reciprocity relation

  <\equation>
    <Pd|S|B|T,p>=<Pd|m<rsub|<text|mag>>|T|p,B>
  </equation>

  Since <math|m<rsub|<text|mag>>> at constant <math|B> decreases with
  increasing <math|T>, as explained above, we see that the entropy change
  during isothermal magnetization is <em|negative>.

  By repeatedly carrying out a procedure of isothermal magnetization and
  adiabatic demagnetization, starting each stage at the temperature produced
  by the previous stage, it has been possible to attain a temperature as low
  as <math|0.0015<K>>. The temperature can be reduced still further, down to
  16 microkelvins, by using adiabatic nuclear demagnetization. However, as is
  evident from the figure, if in accordance with the third law both of the
  entropy curves come together at the absolute zero of the kelvin scale, then
  it is not possible to attain a temperature of zero kelvins in a finite
  number of stages of adiabatic demagnetization. This conclusion is called
  the <index|Absolute zero, unattainability of><em|principle of the
  unattainability of absolute zero>.<index-complex|<tuple|cryogenics>||c6 sec
  c idx1|<tuple|Cryogenics>>

  \;

  <\bio-insert>
    <include|bio-NERNST.tm>
  </bio-insert>

  <\bio-insert>
    <include|bio-GIAUQUE.tm>
  </bio-insert>

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|5>
    <associate|page-first|126>
    <associate|preamble|false>
    <associate|section-nr|9>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|6-J-T expansion|<tuple|6.3.1|?>>
    <associate|6-S of gas from stat mech|<tuple|6.2.2|?>>
    <associate|6-cryogenics|<tuple|6.3|?>>
    <associate|6-entropy zero|<tuple|6.1|?>>
    <associate|6-molar entropies|<tuple|6.2|?>>
    <associate|6-third law molar entropies|<tuple|6.2.1|?>>
    <associate|Chap. 6|<tuple|6|?>>
    <associate|H' defn|<tuple|6.3.5|?>>
    <associate|Sm=Cpm/3+int(Cpm/T)dT+sum(delH/T)|<tuple|6.2.5|?>>
    <associate|Sm=int(Cpm/T)dT+sum(delH/T)|<tuple|6.2.2|?>>
    <associate|auto-1|<tuple|6|?>>
    <associate|auto-10|<tuple|<tuple|third law entropy>|?>>
    <associate|auto-11|<tuple|Entropy|?>>
    <associate|auto-12|<tuple|third-law entropies|?>>
    <associate|auto-13|<tuple|<tuple|entropy|zero of>|?>>
    <associate|auto-14|<tuple|6.2|?>>
    <associate|auto-15|<tuple|<tuple|entropy|molar>|?>>
    <associate|auto-16|<tuple|6.2.1|?>>
    <associate|auto-17|<tuple|Phase|?>>
    <associate|auto-18|<tuple|<tuple|entropy|molar|calorimetry>|?>>
    <associate|auto-19|<tuple|Statistical mechanics|?>>
    <associate|auto-2|<tuple|Nernst|?>>
    <associate|auto-20|<tuple|Debye crystal theory|?>>
    <associate|auto-21|<tuple|Statistical mechanics|?>>
    <associate|auto-22|<tuple|6.2.1|?>>
    <associate|auto-23|<tuple|Entropy|?>>
    <associate|auto-24|<tuple|Entropy|?>>
    <associate|auto-25|<tuple|6.2.2|?>>
    <associate|auto-26|<tuple|Statistical mechanics|?>>
    <associate|auto-27|<tuple|Entropy|?>>
    <associate|auto-28|<tuple|6.2.3|?>>
    <associate|auto-29|<tuple|<tuple|residual entropy>|?>>
    <associate|auto-3|<tuple|Nernst, Walther|?>>
    <associate|auto-30|<tuple|<tuple|entropy|residual>|?>>
    <associate|auto-31|<tuple|6.2.6|?>>
    <associate|auto-32|<tuple|<tuple|residual entropy>|?>>
    <associate|auto-33|<tuple|<tuple|entropy|residual>|?>>
    <associate|auto-34|<tuple|<tuple|entropy|molar>|?>>
    <associate|auto-35|<tuple|6.3|?>>
    <associate|auto-36|<tuple|<tuple|cryogenics>|?>>
    <associate|auto-37|<tuple|6.3.1|?>>
    <associate|auto-38|<tuple|Throttling process|?>>
    <associate|auto-39|<tuple|Process|?>>
    <associate|auto-4|<tuple|Third law of thermodynamics|?>>
    <associate|auto-40|<tuple|throttling process|?>>
    <associate|auto-41|<tuple|Joule--Thomson|?>>
    <associate|auto-42|<tuple|Joule--Thomson experiment|?>>
    <associate|auto-43|<tuple|Joule--Kelvin|?>>
    <associate|auto-44|<tuple|6.3.1|?>>
    <associate|auto-45|<tuple|Process|?>>
    <associate|auto-46|<tuple|Joule--Thomson|?>>
    <associate|auto-47|<tuple|Joule\UThomson coefficient|?>>
    <associate|auto-48|<tuple|Joule--Kelvin|?>>
    <associate|auto-49|<tuple|6.3.2|?>>
    <associate|auto-5|<tuple|third law of thermodynamics|?>>
    <associate|auto-50|<tuple|<tuple|work|magnetization>|?>>
    <associate|auto-51|<tuple|Magnetic|?>>
    <associate|auto-52|<tuple|Flux density, magnetic|?>>
    <associate|auto-53|<tuple|Adiabatic|?>>
    <associate|auto-54|<tuple|adiabatic demagnetization|?>>
    <associate|auto-55|<tuple|Debye, Peter|?>>
    <associate|auto-56|<tuple|Giauque, William|?>>
    <associate|auto-57|<tuple|Magnetic|?>>
    <associate|auto-58|<tuple|Field|?>>
    <associate|auto-59|<tuple|6.3.2|?>>
    <associate|auto-6|<tuple|6.1|?>>
    <associate|auto-60|<tuple|Magnetic|?>>
    <associate|auto-61|<tuple|Field|?>>
    <associate|auto-62|<tuple|Isothermal|?>>
    <associate|auto-63|<tuple|Magnetization, isothermal|?>>
    <associate|auto-64|<tuple|Magnetic|?>>
    <associate|auto-65|<tuple|Field|?>>
    <associate|auto-66|<tuple|Legendre transform|?>>
    <associate|auto-67|<tuple|Magnetic|?>>
    <associate|auto-68|<tuple|Curie's law of magnetization|?>>
    <associate|auto-69|<tuple|Adiabatic|?>>
    <associate|auto-7|<tuple|<tuple|entropy|zero of>|?>>
    <associate|auto-70|<tuple|Isothermal|?>>
    <associate|auto-71|<tuple|Magnetization, isothermal|?>>
    <associate|auto-72|<tuple|Legendre transform|?>>
    <associate|auto-73|<tuple|Absolute zero, unattainability of|?>>
    <associate|auto-74|<tuple|<tuple|cryogenics>|?>>
    <associate|auto-75|<tuple|1|?|bio-NERNST.tm>>
    <associate|auto-76|<tuple|Nernst, Walther|?|bio-NERNST.tm>>
    <associate|auto-77|<tuple|2|?|bio-GIAUQUE.tm>>
    <associate|auto-78|<tuple|Giauque, William|?|bio-GIAUQUE.tm>>
    <associate|auto-8|<tuple|Lewis, Gilbert Newton|?>>
    <associate|auto-9|<tuple|Randall, Merle|?>>
    <associate|bio:giauque|<tuple|2|?|bio-GIAUQUE.tm>>
    <associate|bio:nernst|<tuple|1|?|bio-NERNST.tm>>
    <associate|c6|<tuple|6|?>>
    <associate|c6 sec c|<tuple|6.3|?>>
    <associate|c6 sec c-jt|<tuple|6.3.1|?>>
    <associate|c6 sec c-magnet|<tuple|6.3.2|?>>
    <associate|c6 sec me|<tuple|6.2|?>>
    <associate|c6 sec me-3l|<tuple|6.2.1|?>>
    <associate|c6 sec me-residual|<tuple|6.2.3|?>>
    <associate|c6 sec zoe|<tuple|6.1|?>>
    <associate|c6 sec-me-spectroscopic|<tuple|6.2.2|?>>
    <associate|dT/dB=-dm/dS|<tuple|6.3.7|?>>
    <associate|dU=TdS-pdV+Bdm(mag)|<tuple|6.3.4|?>>
    <associate|fig:6-HCl entropy|<tuple|6.2.1|?>>
    <associate|fig:6-Joule-Thomson|<tuple|6.3.1|?>>
    <associate|fig:6-ad-demag|<tuple|6.3.2|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-6.1.1|<tuple|6.1.1|?>>
    <associate|footnote-6.1.2|<tuple|6.1.2|?>>
    <associate|footnote-6.2.1|<tuple|6.2.1|?>>
    <associate|footnote-6.2.2|<tuple|6.2.2|?>>
    <associate|footnote-6.2.3|<tuple|6.2.3|?>>
    <associate|footnote-6.2.4|<tuple|6.2.4|?>>
    <associate|footnote-6.2.5|<tuple|6.2.5|?>>
    <associate|footnote-6.2.6|<tuple|6.2.6|?>>
    <associate|footnote-6.2.7|<tuple|6.2.7|?>>
    <associate|footnote-6.3.1|<tuple|6.3.1|?>>
    <associate|footnote-6.3.2|<tuple|6.3.2|?>>
    <associate|footnote-6.3.3|<tuple|6.3.3|?|bio-NERNST.tm>>
    <associate|footnote-6.3.4|<tuple|6.3.4|?|bio-NERNST.tm>>
    <associate|footnote-6.3.5|<tuple|6.3.5|?|bio-GIAUQUE.tm>>
    <associate|footnote-6.3.6|<tuple|6.3.6|?|bio-GIAUQUE.tm>>
    <associate|footnote-6.3.7|<tuple|6.3.7|?|bio-GIAUQUE.tm>>
    <associate|footnote-6.3.8|<tuple|6.3.8|?|bio-GIAUQUE.tm>>
    <associate|footnote-6.3.9|<tuple|6.3.9|?|bio-GIAUQUE.tm>>
    <associate|footnr-1|<tuple|Nernst, Walther|?>>
    <associate|footnr-6.1.1|<tuple|6.1.1|?>>
    <associate|footnr-6.1.2|<tuple|6.1.2|?>>
    <associate|footnr-6.2.1|<tuple|6.2.1|?>>
    <associate|footnr-6.2.2|<tuple|6.2.2|?>>
    <associate|footnr-6.2.3|<tuple|6.2.3|?>>
    <associate|footnr-6.2.4|<tuple|6.2.4|?>>
    <associate|footnr-6.2.5|<tuple|6.2.5|?>>
    <associate|footnr-6.2.6|<tuple|6.2.6|?>>
    <associate|footnr-6.2.7|<tuple|6.2.7|?>>
    <associate|footnr-6.3.1|<tuple|6.3.1|?>>
    <associate|footnr-6.3.2|<tuple|6.3.2|?>>
    <associate|footnr-6.3.3|<tuple|6.3.3|?|bio-NERNST.tm>>
    <associate|footnr-6.3.4|<tuple|6.3.4|?|bio-NERNST.tm>>
    <associate|footnr-6.3.5|<tuple|6.3.5|?|bio-GIAUQUE.tm>>
    <associate|footnr-6.3.6|<tuple|6.3.6|?|bio-GIAUQUE.tm>>
    <associate|footnr-6.3.7|<tuple|6.3.7|?|bio-GIAUQUE.tm>>
    <associate|footnr-6.3.8|<tuple|6.3.8|?|bio-GIAUQUE.tm>>
    <associate|footnr-6.3.9|<tuple|6.3.9|?|bio-GIAUQUE.tm>>
    <associate|int(Cp/T)dT=|<tuple|6.2.3|?>>
    <associate|int(Cpm/T)dT=int(Cpm)dlnT|<tuple|6.2.6|?>>
    <associate|lim(T=0)del(r)S=0|<tuple|1|?>>
    <associate|mu(JT) def|<tuple|6.3.3|?>>
    <associate|part:bio-GIAUQUE.tm|<tuple|6.3.4|?>>
    <associate|part:bio-NERNST.tm|<tuple|<tuple|cryogenics>|?>>
    <associate|q(int)=|<tuple|6.2.9|?>>
    <associate|tbl:6-residual_S|<tuple|6.2.1|?>>
    <associate|w(Joule-Thomson)|<tuple|6.3.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      cropper-87

      lewis-23

      giauque-28

      NIST-98

      NIST-98

      giauque-28

      clayton-32

      johnston-29

      blue-35

      giauque-36

      millikan-42

      coffey-08

      pitzer-96

      stranges-90

      giauque-49

      giauque-33

      pitzer-96
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|6.2.1>|>
        Properties of hydrogen chloride (HCl): the dependence of
        <with|mode|<quote|math>|C<rsub|p,<with|mode|<quote|text>|m>>>,
        <with|mode|<quote|math>|C<rsub|p,<with|mode|<quote|text>|m>>/T>, and
        <with|mode|<quote|math>|S<rsub|<with|mode|<quote|text>|m>>> on
        temperature at a pressure of <with|mode|<quote|math>|1
        <with|mode|<quote|text>|bar>>. The discontinuities are at a
        solid<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>solid
        phase transition, the melting temperature, and the vaporization
        temperature. (Condensed-phase data from Ref.
        [<write|bib|giauque-28><reference|bib-giauque-28>]; gas-phase data
        from Ref. [<write|bib|NIST-98><reference|bib-NIST-98>], p. 762.)
      </surround>|<pageref|auto-22>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6.3.1>|>
        Joule\UThomson expansion of a gas through a porous plug. The shaded
        area represents a fixed-amount sample of the gas (a) at time
        <with|mode|<quote|math>|t<rsub|1>>; (b) at a later time
        <with|mode|<quote|math>|t<rsub|2>>.
      </surround>|<pageref|auto-44>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6.3.2>|>
        Adiabatic demagnetization to achieve a low temperature in a
        paramagnetic solid.
      </surround>|<pageref|auto-59>>
    </associate>
    <\associate|gly>
      <tuple|normal|third law of thermodynamics|<pageref|auto-5>>

      <tuple|normal|third-law entropies|<pageref|auto-12>>

      <tuple|normal|throttling process|<pageref|auto-40>>

      <tuple|normal|Joule--Thomson experiment|<pageref|auto-42>>

      <tuple|normal|Joule\UThomson coefficient|<pageref|auto-47>>

      <tuple|normal|adiabatic demagnetization|<pageref|auto-54>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Nernst|heat theorem>|<pageref|auto-2>>

      <tuple|<tuple|Nernst, Walther>|<pageref|auto-3>>

      <tuple|<tuple|Third law of thermodynamics>|<pageref|auto-4>>

      <tuple|<tuple|entropy|zero of>||c6 sec zoe idx1|<tuple|Entropy|zero
      of>|<pageref|auto-7>>

      <tuple|<tuple|Lewis, Gilbert Newton>|<pageref|auto-8>>

      <tuple|<tuple|Randall, Merle>|<pageref|auto-9>>

      <tuple|<tuple|third law entropy>|||<tuple|Third-law
      entropy>|<pageref|auto-10>>

      <tuple|<tuple|Entropy|third-law>|<pageref|auto-11>>

      <tuple|<tuple|entropy|zero of>||c6 sec zoe idx1|<tuple|Entropy|zero
      of>|<pageref|auto-13>>

      <tuple|<tuple|entropy|molar>||c6 sec me
      idx1|<tuple|Entropy|molar>|<pageref|auto-15>>

      <tuple|<tuple|Phase|transition|equilibrium>|<pageref|auto-17>>

      <tuple|<tuple|entropy|molar|calorimetry>|||<tuple|Entropy|molar|from
      calorimetry>|<pageref|auto-18>>

      <tuple|<tuple|Statistical mechanics|Debye crystal
      theory>|<pageref|auto-19>>

      <tuple|<tuple|Debye crystal theory>|<pageref|auto-20>>

      <tuple|<tuple|Statistical mechanics|molar heat capacity of a
      metal>|<pageref|auto-21>>

      <tuple|<tuple|Entropy|scale|practical>|<pageref|auto-23>>

      <tuple|<tuple|Entropy|scale|conventional>|<pageref|auto-24>>

      <tuple|<tuple|Statistical mechanics|molar entropy of a
      gas>|<pageref|auto-26>>

      <tuple|<tuple|Entropy|standard molar>|<pageref|auto-27>>

      <tuple|<tuple|residual entropy>||c6 sec me-residual
      idx1|<tuple|Residual entropy>|<pageref|auto-29>>

      <tuple|<tuple|entropy|residual>||c6 sec me-residual
      idx2|<tuple|Entropy|residual>|<pageref|auto-30>>

      <tuple|<tuple|residual entropy>||c6 sec me-residual
      idx1|<tuple|Residual entropy>|<pageref|auto-32>>

      <tuple|<tuple|entropy|residual>||c6 sec me-residual
      idx2|<tuple|Entropy|residual>|<pageref|auto-33>>

      <tuple|<tuple|entropy|molar>||c6 sec me
      idx1|<tuple|Entropy|molar>|<pageref|auto-34>>

      <tuple|<tuple|cryogenics>||c6 sec c
      idx1|<tuple|Cryogenics>|<pageref|auto-36>>

      <tuple|<tuple|Throttling process>|<pageref|auto-38>>

      <tuple|<tuple|Process|throttling>|<pageref|auto-39>>

      <tuple|<tuple|Joule--Thomson|experiment>|<pageref|auto-41>>

      <tuple|<tuple|Joule--Kelvin|experiment>|<pageref|auto-43>>

      <tuple|<tuple|Process|isenthalpic>|<pageref|auto-45>>

      <tuple|<tuple|Joule--Thomson|coefficient>|<pageref|auto-46>>

      <tuple|<tuple|Joule--Kelvin|coefficient>|<pageref|auto-48>>

      <tuple|<tuple|work|magnetization>|||<tuple|Work|of
      magnetization>|<pageref|auto-50>>

      <tuple|<tuple|Magnetic|flux density>|<pageref|auto-51>>

      <tuple|<tuple|Flux density, magnetic>|<pageref|auto-52>>

      <tuple|<tuple|Adiabatic|demagnetization>|<pageref|auto-53>>

      <tuple|<tuple|Debye, Peter>|<pageref|auto-55>>

      <tuple|<tuple|Giauque, William>|<pageref|auto-56>>

      <tuple|<tuple|Magnetic|field>|<pageref|auto-57>>

      <tuple|<tuple|Field|magnetic>|<pageref|auto-58>>

      <tuple|<tuple|Magnetic|field>|<pageref|auto-60>>

      <tuple|<tuple|Field|magnetic>|<pageref|auto-61>>

      <tuple|<tuple|Isothermal|magnetization>|<pageref|auto-62>>

      <tuple|<tuple|Magnetization, isothermal>|<pageref|auto-63>>

      <tuple|<tuple|Magnetic|field>|<pageref|auto-64>>

      <tuple|<tuple|Field|magnetic>|<pageref|auto-65>>

      <tuple|<tuple|Legendre transform>|<pageref|auto-66>>

      <tuple|<tuple|Magnetic|enthalpy>|<pageref|auto-67>>

      <tuple|<tuple|Curie's law of magnetization>|<pageref|auto-68>>

      <tuple|<tuple|Adiabatic|demagnetization>|<pageref|auto-69>>

      <tuple|<tuple|Isothermal|magnetization>|<pageref|auto-70>>

      <tuple|<tuple|Magnetization, isothermal>|<pageref|auto-71>>

      <tuple|<tuple|Legendre transform>|<pageref|auto-72>>

      <tuple|<tuple|Absolute zero, unattainability of>|<pageref|auto-73>>

      <tuple|<tuple|cryogenics>||c6 sec c
      idx1|<tuple|Cryogenics>|<pageref|auto-74>>

      <tuple|<tuple|Nernst, Walther>|<pageref|auto-76>>

      <tuple|<tuple|Giauque, William>|<pageref|auto-78>>
    </associate>
    <\associate|parts>
      <tuple|bio-NERNST.tm|chapter-nr|6|section-nr|3|subsection-nr|2>

      <tuple|bio-GIAUQUE.tm|chapter-nr|6|section-nr|3|subsection-nr|2>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|6.2.1>|>
        Standard molar entropies of several substances (ideal gases at
        <with|mode|<quote|math>|T=298.15<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>> and <with|mode|<quote|math>|p=1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>) and molar residual entropies
      </surround>|<pageref|auto-31>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|6<space|2spc>The
      Third Law and Cryogenics> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      6.1<space|2spc>The Zero of Entropy <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>

      6.2<space|2spc>Molar Entropies <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>

      <with|par-left|<quote|1tab>|6.2.1<space|2spc>Third-law molar entropies
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>

      <with|par-left|<quote|1tab>|6.2.2<space|2spc>Molar entropies from
      spectroscopic measurements <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-25>>

      <with|par-left|<quote|1tab>|6.2.3<space|2spc>Residual entropy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-28>>

      6.3<space|2spc>Cryogenics <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-35>

      <with|par-left|<quote|1tab>|6.3.1<space|2spc>Joule\UThompson expansion
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-37>>

      <with|par-left|<quote|1tab>|6.3.2<space|2spc>Magnetization
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-49>>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Walther
      Hermann Nernst> (1864-1941) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-75><vspace|0.15fn>>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|William
      Francis Giauque> (1895\U1982) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-77><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>