<TeXmacs|1.99.20>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|Problems>

  <\problem>
    Derive the following relations from the definitions of <math|\<alpha\>>,
    <math|<kT>>, and <math|\<rho\>>:

    <\equation*>
      \<alpha\>=-<frac|1|\<rho\>><Pd|\<rho\>|T|<space|-0.17em>p><space|2em><kT>=<frac|1|\<rho\>><Pd|\<rho\>|p|T>
    </equation*>
  </problem>

  <\problem>
    Use equations in this chapter to derive the following expressions for an
    ideal gas: <index-complex|<tuple|cuvic expansion coefficient|ideal
    gas>|||<tuple|Cubic expansion coefficient|of an ideal
    gas>><index-complex|<tuple|isothermal|compressibility|ideal
    gas>|||<tuple|Isothermal|compressibility|of an ideal gas>>

    <\equation*>
      \<alpha\>=1/T<space|2em><kT>=1/p
    </equation*>
  </problem>

  <\problem>
    For a gas with the simple equation of state

    <\equation*>
      V<m>=<frac|R*T|p>+B
    </equation*>

    (Eq. <reference|Vm=RT/p+B>), where <math|B> is the second virial
    coefficient (a function of <math|T>), find expressions for
    <math|\<alpha\>>, <math|<kT>>, and <math|<pd|U<m>|V|T>> in terms of
    <math|<dif>B/<dif>T> and other state functions.
  </problem>

  <\problem>
    Show that when the virial equation <math|p*V<m>=R*T*<around|(|1+B<rsub|p>*p+C<rsub|p>*p<rsup|2>+\<cdots\>|)>>
    (Eq. <reference|pVm=RT(1+B_p p...)>) adequately represents the equation
    of state of a real gas, the Joule\UThomson coefficient is given by

    <\equation*>
      \<mu\><rsub|<text|JT>>=<frac|R*T<rsup|2>*<around|[|<dif>B<rsub|p>/<dif>T+<around|(|<dif>C<rsub|p>/<dif>T|)>*p+\<cdots\>|]>|<Cpm>>
    </equation*>

    Note that the limiting value at low pressure,
    <math|R*T<rsup|2>*<around|(|<dif>B<rsub|p>/<dif>T|)>/<Cpm>>, is not
    necessarily equal to zero even though the equation of state approaches
    that of an ideal gas in this limit.
  </problem>

  <\problem>
    The quantity <math|<pd|T|V|U>> is called the
    <subindex|Joule|coefficient><em|Joule coefficient>. <index|Joule, James
    Prescott>James Joule attempted to evaluate this quantity by measuring the
    temperature change accompanying the expansion of air into a vacuum\Vthe
    <subindex|Joule|experiment>\PJoule experiment.\Q Write an expression for
    the total differential of <math|U> with <math|T> and <math|V> as
    independent variables, and by a procedure similar to that used in Sec.
    <reference|7-JK coeff> show that the Joule coefficient is equal to

    <\equation*>
      <frac|p-\<alpha\>*T/<kT>|C<rsub|V>>
    </equation*>
  </problem>

  <\problem>
    <label|prb:7-aniline><math|p>\U<math|V>\U<math|T> data for several
    organic liquids were measured by Gibson and Loeffler.<footnote|Ref.
    <cite|gibson-39>.> The following formulas describe the results for
    aniline.

    <\itemize>
      <item>Molar volume as a function of temperature at <math|p=1<br>>
      (<math|298>\U<math|358<K>>):

      <\equation*>
        V<m>=a+b*T+c*T<rsup|2>+d*T<rsup|3>
      </equation*>

      where the parameters have the values

      <alignat*|2|<tformat|<table|<row|<cell|a>|<cell|=69.287
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>|<cell|<space|2em>c>|<cell|=-1.0443<timesten|-4>
      <text|cm><rsup|3>\<cdot\><text|K><rsup|-2>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|b>|<cell|=0.08852
      <text|cm><rsup|3>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>|<cell|<space|2em>d>|<cell|=1.940<timesten|-7>
      <text|cm><rsup|3>\<cdot\><text|K><rsup|-3>\<cdot\><text|mol><rsup|-1>>>>>>

      <item>Molar volume as a function of pressure at <math|T=298.15<K>>
      (<math|1>\U<math|1000<br>>):

      <\equation*>
        V<m>=e-f*ln <around|(|g+p/<text|bar>|)>
      </equation*>

      where the parameter values are

      <\equation*>
        e=156.812 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1><space|2em>f=8.5834
        <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1><space|2em>g=2006.6
      </equation*>
    </itemize>

    <\enumerate-alpha>
      <item>Use these formulas to evaluate <math|\<alpha\>>, <math|<kT>>,
      <math|<pd|p|T|V>>, and <math|<pd|U|V|T>> (the internal pressure) for
      aniline at <math|T=298.15<K>> and <math|p=1.000<br>>.

      <item>Estimate the pressure increase if the temperature of a fixed
      amount of aniline is increased by <math|0.10<K>> at constant volume.
    </enumerate-alpha>
  </problem>

  <\problem>
    \;

    <\enumerate-alpha>
      <item>From the total differential of <math|H> with <math|T> and
      <math|p> as independent variables, derive the relation
      <math|<pd|<Cpm>|p|T>=-T<pd|<rsup|2>V<m>|T<rsup|2>|p>>.

      <item>Evaluate <math|<pd|<Cpm>|p|T>> for liquid aniline at
      <math|300.0<K>> and <math|1<br>> using data in Prob.
      <reference|prb:7-aniline>.
    </enumerate-alpha>
  </problem>

  <\problem>
    \;

    <\enumerate-alpha>
      <item>From the total differential of <math|V> with <math|T> and
      <math|p> as independent variables, derive the relation
      <math|<pd|\<alpha\>|p|T>=-<pd|<kT>|T|p>>.

      <item>Use this relation to estimate the value of <math|\<alpha\>> for
      benzene at <math|25 <degC>> and <math|500<br>>, given that the value of
      <math|\<alpha\>> is <math|1.2<timesten|-3> <text|K><rsup|-1>> at
      <math|25 <degC>> and <math|1<br>>. (Use information from Fig.
      <vpageref|fig:7-kappaT vs T>.)
    </enumerate-alpha>
  </problem>

  <\problem>
    Certain equations of state supposed to be applicable to nonpolar liquids
    and gases are of the form <math|p=T*f<around|(|V<m>|)>-a/V<m><rsup|2>>,
    where <math|f<around|(|V<m>|)>> is a function of the molar volume only
    and <math|a> is a constant.

    <\enumerate-alpha>
      <item>Show that the van der Waals equation of state
      <math|<around|(|p+a/V<m><rsup|2>|)>*<around|(|V<m>-b|)>=R*T> (where
      <math|a> and <math|b> are constants) is of this form.

      <item>Show that any fluid with an equation of state of this form has an
      internal pressure equal to <math|a/V<m><rsup|2>>.
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:7-delH, delS formulas> Suppose that the molar heat capacity at
    constant pressure of a substance has a temperature dependence given by
    <math|<Cpm>=a+b*T+c*T<rsup|2>>, where <math|a>, <math|b>, and <math|c>
    are constants. Consider the heating of an amount <math|n> of the
    substance from <math|T<rsub|1>> to <math|T<rsub|2>> at constant pressure.
    Find expressions for <math|<Del>H> and <math|<Del>S> for this process in
    terms of <math|a>, <math|b>, <math|c>, <math|n>, <math|T<rsub|1>>, and
    <math|T<rsub|2>>.
  </problem>

  <\problem>
    <label|prb:7-aluminum> At <math|p=1 <text|atm>>, the molar heat capacity
    at constant pressure of aluminum is given by

    <\equation*>
      <Cpm>=a+b*T
    </equation*>

    where the constants have the values

    <\equation*>
      a=20.67 <text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1><space|2em>b=0.01238
      <text|J>\<cdot\><text|K><rsup|-2>\<cdot\><text|mol><rsup|-1>
    </equation*>

    Calculate the quantity of electrical work needed to heat <math|2.000
    <text|mol>> of aluminum from <math|300.00<K>> to <math|400.00<K>> at
    <math|1 <text|atm>> in an adiabatic enclosure.
  </problem>

  <\problem>
    <label|prb:7-CO2 DelH, DelS> The temperature dependence of the standard
    molar heat capacity of gaseous carbon dioxide in the temperature range
    <math|298<K>>\U<math|2000<K>> is given by

    <\equation*>
      <Cpm><st>=a+b*T+<frac|c|T<rsup|2>>
    </equation*>

    where the constants have the values

    <\equation*>
      a=44.2 <text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1><space|1em>b=8.8<timesten|-3>
      <text|J>\<cdot\><text|K><rsup|-2>\<cdot\><text|mol><rsup|-1><space|1em>c=-8.6<timesten|5>
      <text|J>\<cdot\><text|K>\<cdot\><text|mol><rsup|-1>
    </equation*>

    Calculate the enthalpy and entropy changes when one mole of
    CO<rsub|<math|2>> is heated at <math|1<br>> from <math|300.00<K>> to
    <math|800.00<K>>. You can assume that at this pressure <math|<Cpm>> is
    practically equal to <math|<Cpm><st>>.
  </problem>

  <\problem>
    This problem concerns gaseous carbon dioxide. At <math|400<K>>, the
    relation between <math|p> and <math|V<m>> at pressures up to at least
    <math|100<br>> is given to good accuracy by a virial equation of state
    truncated at the second virial coefficient, <math|B>. In the temperature
    range <math|300<K>>\U<math|800<K>> the dependence of <math|B> on
    temperature is given by

    <\equation*>
      B=a<rprime|'>+b<rprime|'>*T+c<rprime|'>*T<rsup|2>+d<rprime|'>*T<rsup|3>
    </equation*>

    where the constants have the values

    <\eqnarray*>
      <tformat|<table|<row|<cell|a<rprime|'>>|<cell|=>|<cell|-521
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|b<rprime|'>>|<cell|=>|<cell|2.08
      <text|cm><rsup|3>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|c<rprime|'>>|<cell|=>|<cell|-2.89<timesten|-3>
      <text|cm><rsup|3>\<cdot\><text|K><rsup|-2>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|d<rprime|'>>|<cell|=>|<cell|1.397<timesten|-6>
      <text|cm><rsup|3>\<cdot\><text|K><rsup|-3>\<cdot\><text|mol><rsup|-1>>>>>
    </eqnarray*>

    <\enumerate-alpha>
      <item>From information in Prob. 7.<reference|prb:7-CO2 DelH, DelS>,
      calculate the standard molar heat capacity at constant pressure,
      <math|<Cpm><st>>, at <math|T=400.0<K>>.

      <item>Estimate the value of <math|<Cpm>> under the conditions
      <math|T=400.0<K>> and <math|p=100.0<br>>.
    </enumerate-alpha>
  </problem>

  <\problem>
    A chemist, needing to determine the specific heat capacity of a certain
    liquid but not having an electrically heated calorimeter at her disposal,
    used the following simple procedure known as
    <subindex|Calorimetry|drop><em|drop calorimetry>. She placed <math|500.0
    <text|g>> of the liquid in a thermally insulated container equipped with
    a lid and a thermometer. After recording the initial temperature of the
    liquid, <math|24.80 <degC>>, she removed a <math|60.17>-g block of
    aluminum metal from a boiling water bath at <math|100.00 <degC>> and
    quickly immersed it in the liquid in the container. After the contents of
    the container had become thermally equilibrated, she recorded a final
    temperature of <math|27.92 <degC>>. She calculated the specific heat
    capacity <math|C<rsub|p>/m> of the liquid from these data, making use of
    the molar mass of aluminum (<math|M=26.9815
    <text|g>\<cdot\><text|mol><rsup|-1>>) and the formula for the molar heat
    capacity of aluminum given in Prob. 7.<reference|prb:7-aluminum>.

    <\enumerate-alpha>
      <item>From these data, find the specific heat capacity of the liquid
      under the assumption that its value does not vary with temperature.
      Hint: Treat the temperature equilibration process as adiabatic and
      isobaric (<math|<Del>H=0>), and equate <math|<Del>H> to the sum of the
      enthalpy changes in the two phases.

      <item>Show that the value obtained in part (a) is actually an average
      value of <math|C<rsub|p>/m> over the temperature range between the
      initial and final temperatures of the liquid given by

      <\equation*>
        <frac|<with|math-display|true|<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><around|(|C<rsub|p>/m|)><dif>T>|T<rsub|2>-T<rsub|1>>
      </equation*>
    </enumerate-alpha>
  </problem>

  <\problem>
    Suppose a gas has the virial equation of state
    <math|p*V<m>=R*T*<around|(|1+B<rsub|p>*p+C<rsub|p>*p<rsup|2>|)>>, where
    <math|B<rsub|p>> and <math|C<rsub|p>> depend only on <math|T>, and higher
    powers of <math|p> can be ignored.

    <\enumerate-alpha>
      <item>Derive an expression for the fugacity coefficient,
      <math|\<phi\>>, of this gas as a function of <math|p>.

      <item>For CO<rsub|<math|2>>(g) at <math|0.00 <degC>>, the virial
      coefficients have the values <math|B<rsub|p>=-6.67<timesten|-3>
      <text|bar><rsup|-1>> and <math|C<rsub|p>=-3.4<timesten|-5>
      <text|bar><rsup|-2>>. Evaluate the fugacity <math|<fug>> at <math|0.00
      <degC>> and <math|p=20.0<br>>.
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:7-H2O fug> Table <vpageref|tbl:7-H2O(g)> lists values of the
    molar volume of gaseous H<rsub|<math|2>>O at <math|400.00 <degC>> and 12
    pressures.<\float|float|thb>
      <\big-table|<tabular*|<tformat|<cwith|2|-1|1|-1|cell-bsep|0.25fn>|<cwith|2|-1|1|-1|cell-tsep|0.25fn>|<cwith|2|-1|2|2|cell-lsep|>|<cwith|2|-1|2|2|cell-rsep|>|<cwith|2|-1|5|5|cell-halign|C.>|<cwith|2|-1|2|2|cell-halign|C.>|<cwith|7|7|1|-1|cell-bborder|1ln>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|5|5|cell-rborder|0ln>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-lborder|0ln>|<cwith|1|1|5|5|cell-rborder|0ln>|<table|<row|<cell|<math|p/10<rsup|5>
      <text|Pa>>>|<cell|<math|V<m>/10<rsup|-3>
      <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>|<cell|<space|2em>>|<cell|<math|p/10<rsup|5>
      <text|Pa>>>|<cell|<math|V<m>/10<rsup|-3>
      <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<cell|1>|<cell|55.896>|<cell|>|<cell|100>|<cell|0.47575>>|<row|<cell|10>|<cell|5.5231>|<cell|>|<cell|120>|<cell|0.37976>>|<row|<cell|20>|<cell|2.7237>|<cell|>|<cell|140>|<cell|0.31020>>|<row|<cell|40>|<cell|1.3224>|<cell|>|<cell|160>|<cell|0.25699>>|<row|<cell|60>|<cell|0.85374>|<cell|>|<cell|180>|<cell|0.21447>>|<row|<cell|80>|<cell|0.61817>|<cell|>|<cell|200>|<cell|0.17918>>>>>>
        <label|tbl:7-H2O(g)>Molar volume of H<rsub|<math|2>>O(g) at
        <math|400.00 <degC>><rsup|<note-inline||+FbTpX6q2UH75xAp>>

        <note-ref|+FbTpX6q2UH75xAp> based on data in Ref. <cite|grigull-90>
      </big-table>
    </float>

    <\enumerate-alpha>
      <item>Evaluate the fugacity coefficient and fugacity of
      H<rsub|<math|2>>O(g) at <math|400.00 <degC>> and <math|200<br>>.

      <item>Show that the second virial coefficient <math|B> in the virial
      equation of state, <math|p*V<m>=R*T*<around|(|1+B/V<m>+C/V<m><rsup|2>+\<cdots\>|)>>,
      is given by

      <\equation*>
        B=R*T*lim<rsub|p<ra>0><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)>
      </equation*>

      where the limit is taken at constant <math|T>. Then evaluate <math|B>
      for H<rsub|<math|2>>O(g) at <math|400.00 <degC>>.
    </enumerate-alpha>
  </problem>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|7>
    <associate|page-first|122>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|9>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|10|?>>
    <associate|auto-2|<tuple|<tuple|cuvic expansion coefficient|ideal
    gas>|?>>
    <associate|auto-3|<tuple|<tuple|isothermal|compressibility|ideal gas>|?>>
    <associate|auto-4|<tuple|Joule|?>>
    <associate|auto-5|<tuple|Joule, James Prescott|?>>
    <associate|auto-6|<tuple|Joule|?>>
    <associate|auto-7|<tuple|Calorimetry|?>>
    <associate|auto-8|<tuple|10.1|?>>
    <associate|footnote-10.1|<tuple|10.1|?>>
    <associate|footnote-10.2|<tuple|10.2|?>>
    <associate|footnr-10.1|<tuple|10.1|?>>
    <associate|footnr-10.2|<tuple|10.1|?>>
    <associate|prb:7-CO2 DelH, DelS|<tuple|10.12|?>>
    <associate|prb:7-H2O fug|<tuple|10.16|?>>
    <associate|prb:7-aluminum|<tuple|10.11|?>>
    <associate|prb:7-aniline|<tuple|10.6|?>>
    <associate|prb:7-delH, delS formulas|<tuple|10.10|?>>
    <associate|tbl:7-H2O(g)|<tuple|10.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      gibson-39

      grigull-90
    </associate>
    <\associate|idx>
      <tuple|<tuple|cuvic expansion coefficient|ideal gas>|||<tuple|Cubic
      expansion coefficient|of an ideal gas>|<pageref|auto-2>>

      <tuple|<tuple|isothermal|compressibility|ideal
      gas>|||<tuple|Isothermal|compressibility|of an ideal
      gas>|<pageref|auto-3>>

      <tuple|<tuple|Joule|coefficient>|<pageref|auto-4>>

      <tuple|<tuple|Joule, James Prescott>|<pageref|auto-5>>

      <tuple|<tuple|Joule|experiment>|<pageref|auto-6>>

      <tuple|<tuple|Calorimetry|drop>|<pageref|auto-7>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|10.1>|>
        Molar volume of H<rsub|<with|mode|<quote|math>|2>>O(g) at
        <with|mode|<quote|math>|400.00 <rsup|\<circ\>><with|mode|<quote|text>|C>><rsup|<surround|<assign|footnote-nr|2><hidden-binding|<tuple>|10.2><assign|fnote-+FbTpX6q2UH75xAp|<quote|10.2>><assign|fnlab-+FbTpX6q2UH75xAp|<quote|10.2>>||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-3E44F27F8--3D467DD10>|<link|hyperlink|<id|%-3E44F27F8--3D467DD10>|<url|#footnr-10.2>>|10.2>.
        |<hidden-binding|<tuple|footnote-10.2>|10.2>|>>>>

        <space|0spc><rsup|<with|font-shape|<quote|right>|<reference|footnote-10.2>>>
        based on data in Ref. [<write|bib|grigull-90><reference|bib-grigull-90>]
      </surround>|<pageref|auto-8>>
    </associate>
    <\associate|toc>
      10<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>