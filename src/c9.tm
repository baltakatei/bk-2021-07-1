<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <chapter|Mixtures><label|Chap. 9><label|c9>

  A homogeneous mixture is a phase containing more than one substance. This
  chapter discusses composition variables and partial molar quantities of
  mixtures in which no chemical reaction is occurring. The ideal mixture is
  defined. Chemical potentials, activity coefficients, and activities of
  individual substances in both ideal and nonideal mixtures are discussed.

  Except for the use of fugacities to determine activity coefficients in
  condensed phases, a discussion of phase equilibria involving mixtures will
  be postponed to Chap. <reference|Chap. 13>.

  <section|Composition Variables><label|c9 sec cv>

  A <index|Composition variable><newterm|composition variable> is an
  intensive property that indicates the relative amount of a particular
  species or substance in a phase.

  <subsection|Species and substances><label|9-species \ substances><label|c9
  sec cv-species-substances>

  We sometimes need to make a distinction between a species and a substance.
  A <index|Species><newterm|species> is any entity of definite elemental
  composition and charge and can be described by a chemical formula, such as
  H<rsub|<math|2>>O, H<rsub|<math|3>>O<rsup|<math|+>>, NaCl, or
  Na<rsup|<math|+>>. A <index|Substance><newterm|substance> is a species that
  can be prepared in a pure state (e.g., N<rsub|<math|2>> and NaCl). Since we
  cannot prepare a macroscopic amount of a single kind of ion by itself, a
  charged species such as H<rsub|<math|3>>O<rsup|<math|+>> or
  Na<rsup|<math|+>> is not a substance. Chap. <reference|Chap. 10> will
  discuss the special features of mixtures containing charged species.

  <subsection|Mixtures in general><label|9-mixts in general><label|c9 sec
  cv-mixtures>

  The <index|Mole fraction><newterm|mole fraction> of species <math|i> is
  defined by

  <equation-cov2|<label|x_i=n_i/sum(n_j)>x<rsub|i><defn><frac|n<rsub|i>|<big|sum><rsub|j>n<rsub|j>><space|2em><text|<math|or>><space|2em>y<rsub|i><defn><frac|n<rsub|i>|<big|sum><rsub|j>n<rsub|j>>|(<math|P=1>)>

  where <math|n<rsub|i>> is the amount of species <math|i> and the sum is
  taken over all species in the mixture. The symbol <math|x<rsub|i>> is used
  for a mixture in general, and <math|y<rsub|i>> is used when the mixture is
  a gas.

  The <index|Mass fraction><newterm|mass fraction>, or weight fraction, of
  species <math|i> is defined by

  <equation-cov2|<label|w_i=n_iM_i/sum(n_jM_j)>w<rsub|i><defn><frac|m<around|(|i|)>|m>=<frac|n<rsub|i>*M<rsub|i>|<big|sum><rsub|j>n<rsub|j>*M<rsub|j>>|(<math|P=1>)>

  where <math|m<around|(|i|)>> is the mass of species <math|i> and <math|m>
  is the total mass.

  The <index|Concentration><newterm|concentration>, or molarity, of species
  <math|i> in a mixture is defined by

  <equation-cov2|<label|c_i=n_i/V>c<rsub|i><defn><frac|n<rsub|i>|V>|(<math|P=1>)>

  The symbol M is often used to stand for units of
  <math|<text|mol>\<cdot\><text|L><rsup|-1>>, or
  mol<space|0.17em>dm<rsup|<math|-3>>. Thus, a concentration of <math|0.5
  <text|M>> is <math|0.5> moles per liter, or <math|0.5> molar.

  <\quote-env>
    \ Concentration is sometimes called \Pamount concentration\Q or \Pmolar
    concentration\Q to avoid confusion with number concentration (the number
    of <em|particles> per unit volume). An alternative notation for
    <math|c<A>> is [A].
  </quote-env>

  A <index|Binary mixture><subindex|Mixture|binary><newterm|binary mixture>
  is a mixture of <em|two> substances.

  <subsection|Solutions><label|9-solutions><label|c9 sec cv-solutions>

  A <index|Solution><newterm|solution>, strictly speaking, is a mixture in
  which one substance, the <index|Solvent><newterm|solvent>, is treated in a
  special way. Each of the other species comprising the mixture is then a
  <index|Solute><newterm|solute>. The solvent is denoted by A and the solute
  species by B, C, and so on.<footnote|Some chemists denote the solvent by
  subscript <math|1> and use <math|2>, <math|3>, and so on for solutes.>
  Although in principle a solution can be a gas mixture, in this section we
  will consider only liquid and solid solutions.

  We can prepare a solution of varying composition by gradually mixing one or
  more solutes with the solvent so as to continuously increase the solute
  mole fractions. During this mixing process, the physical state (liquid or
  solid) of the solution remains the same as that of the pure solvent. When
  the sum of the solute mole fractions is small compared to <math|x<A>>
  (i.e., <math|x<A>> is close to unity), the solution is called <em|dilute>.
  As the solute mole fractions increase, we say the solution becomes more
  <em|concentrated>.

  Mole fraction, mass fraction, and concentration can be used as composition
  variables for both solvent and solute, just as they are for mixtures in
  general. A fourth composition variable, molality, is often used for a
  solute. The <index|Molality><newterm|molality> of solute species B is
  defined by

  <equation-cov2|<label|mB=nB/mass of solvent>m<B><defn><frac|n<B>|m<around*|(|<text|A>|)>>|(solution)>

  where <math|m<around|(|<text|A>|)>=n<A>*M<A>> is the mass of solvent. The
  symbol m is sometimes used to stand for units of
  <math|<text|mol>\<cdot\><text|kg><rsup|-1>>, although this should be
  discouraged because m is also the symbol for meter. For example, a solute
  molality of <math|0.6 <text|m>> is <math|0.6> moles of solute per kilogram
  of solvent, or <math|0.6> molal.

  <subsection|Binary solutions><label|2-binary solns><label|c9 sec
  cv-solutions-binary>

  <index|Binary solution><subindex|Solution|binary>We may write simplified
  equations for a binary solution of two substances, solvent A and solute B.
  Equations <reference|x_i=n_i/sum(n_j)>\U<reference|mB=nB/mass of solvent>
  become

  <equation-cov2|<label|xB=nB/(nA+nB)>x<B>=<frac|n<B>|n<A>+n<B>>|(binary
  solution)>

  <equation-cov2|<label|wB=nBMB/(nAMA+nBMB)>w<B>=<frac|n<B>M<B>|n<A>M<A>+n<B>M<B>>|(binary
  solution)>

  <equation-cov2|<label|cB=nB/V>c<B>=<frac|n<B>|V>=<frac|n<B>\<rho\>|n<A>M<A>+n<B>M<B>>|(binary
  solution)>

  <equation-cov2|<label|mB=nB/(nA*MA)>m<B>=<frac|n<B>|n<A>M<A>>|(binary
  solution)>

  The right sides of Eqs. <reference|xB=nB/(nA+nB)>\U<reference|mB=nB/(nA*MA)>
  express the solute composition variables in terms of the amounts and molar
  masses of the solvent and solute and the density <math|\<rho\>> of the
  solution.

  To be able to relate the values of these composition variables to one
  another, we solve each equation for <math|n<B>> and divide by <math|n<A>>
  to obtain an expression for the <index|Mole ratio>mole ratio
  <math|n<B>/n<A>>:<label|mole ratios>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<text|from Eq.
    <math|<reference|xB=nB/(nA+nB)>>><space|4em><dfrac|n<rsub|<text|B>>|n<rsub|<text|A>>>>|<cell|=>|<cell|<dfrac|x<rsub|<text|B>>|1-x<rsub|<text|B>>><htab|5mm><tabular*|<tformat|<table|<row|<cell|<eq-number>>>|<row|<cell|<around*|(|binary
    solution|)>>>>>><label|nB/nA=xB/(1-xB)>>>|<row|<cell|<text|from Eq.
    <math|<reference|wB=nBMB/(nAMA+nBMB)>>><space|4em><dfrac|n<rsub|<text|B>>|n<rsub|<text|A>>>>|<cell|=>|<cell|<frac|M<A>*w<B>|M<B>*<around|(|1-w<B>|)>><htab|5mm><tabular*|<tformat|<table|<row|<cell|<eq-number>>>|<row|<cell|<around*|(|binary
    solution|)>>>>>><label|nB/nA=M(A)wB/M(B)(1-wB)>>>|<row|<cell|<text|from
    Eq. <math|<reference|cB=nB/V>>><space|4em><dfrac|n<rsub|<text|B>>|n<rsub|<text|A>>>>|<cell|=>|<cell|<frac|M<A>*c<B>|\<rho\>-M<B>*c<B>><htab|5mm><tabular*|<tformat|<table|<row|<cell|<eq-number>>>|<row|<cell|<around*|(|binary
    solution|)>>>>>><label|nB/nA=M(A)cB/(rho-M(B)cB)>>>|<row|<cell|<text|from
    Eq. <math|<reference|mB=nB/(nA*MA)>>><space|4em><dfrac|n<rsub|<text|B>>|n<rsub|<text|A>>>>|<cell|=>|<cell|M<A>*m<B><htab|5mm><tabular*|<tformat|<table|<row|<cell|<eq-number>>>|<row|<cell|<around*|(|binary
    solution|)>>>>>><label|nB/nA=M(A)mB>>>>>
  </eqnarray*>

  These expressions for <math|n<B>/n<A>> allow us to find one composition
  variable as a function of another. For example, to find molality as a
  function of concentration, we equate the expressions for <math|n<B>/n<A>>
  on the right sides of Eqs. <reference|nB/nA=M(A)cB/(rho-M(B)cB)> and
  <reference|nB/nA=M(A)mB> and solve for <math|m<B>> to obtain

  <\equation>
    m<B>=<frac|c<B>|\<rho\>-M<B>c<B>>
  </equation>

  A binary solution becomes more dilute as any of the solute composition
  variables becomes smaller. In the limit of infinite dilution, the
  expressions for <math|n<B>/n<A>> become:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dfrac|n<rsub|<text|B>>|n<rsub|<text|A>>>>|<cell|=>|<cell|x<rsub|<text|B>>>>|<row|<cell|>|<cell|=>|<cell|<dfrac|M<rsub|<text|A>>|M<rsub|<text|B>>>\<cdot\>w<rsub|<text|B>>>>|<row|<cell|>|<cell|=>|<cell|<dfrac|M<rsub|<text|A>>|\<rho\><rsup|\<ast\>><rsub|<text|A>>>\<cdot\>c<rsub|<text|B>>=V<rsub|<text|m>,<text|A>><rsup|\<ast\>>\<cdot\>c<rsub|<text|B>>>>|<row|<cell|>|<cell|=>|<cell|M<rsub|<text|A>>\<cdot\>m<rsub|<text|B>><htab|5mm><tabular*|<tformat|<table|<row|<cell|<eq-number>>>|<row|<cell|<text|<tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|(binary
    solution at>>|<row|<cell|infinite dilution)>>>>>>>>>>><label|nB/nA
    (dilute)>>>>>
  </eqnarray*>

  where a superscript asterisk (<rsup|<math|\<ast\>>>) denotes a pure phase.
  <subindex|Composition variable|relations at infinite dilution>We see that,
  in the limit of infinite dilution, the composition variables <math|x<B>>,
  <math|w<B>>, <math|c<B>>, and <math|m<B>> are proportional to one another.
  These expressions are also valid for solute B in a <em|multi>solute
  solution in which <em|each> solute is very dilute; that is, in the limit
  <math|x<A>><ra><math|1>.

  <\quote-env>
    The rule of thumb that the molarity and molality values of a dilute
    aqueous solution are approximately equal is explained by the relation
    <math|M<A>*c<B>/\<rho\><A><rsup|\<ast\>>=M<A>*m<B>> (from Eq.
    <reference|nB/nA (dilute)>), or <math|c<B>/\<rho\><A><rsup|\<ast\>>=m<B>>,
    and the fact that the density <math|\<rho\><A><rsup|\<ast\>>> of water is
    approximately <math|1 <text|kg>\<cdot\><text|L><rsup|-1>>. Hence, if the
    solvent is water and the solution is dilute, the numerical value of
    <math|c<B>> expressed in <math|<text|mol>\<cdot\><text|L><rsup|-1>> is
    approximately equal to the numerical value of <math|m<B>> expressed in
    <math|<text|mol>\<cdot\><text|kg><rsup|-1>>.
  </quote-env>

  <subsection|The composition of a mixture><label|9-composition of a
  mixture><label|c9 sec-cv-mixture-composition>

  We can describe the composition of a phase with the amounts of each
  species, or with any of the <index|Composition variable>composition
  variables defined earlier: mole fraction, mass fraction, concentration, or
  molality. If we use mole fractions or mass fractions to describe the
  composition, we need the values for all but one of the species, since the
  sum of all fractions is unity.

  Other composition variables are sometimes used, such as volume fraction,
  mole ratio, and mole percent. To describe the composition of a gas mixture,
  partial pressures can be used (Sec. <reference|9-partial p>).

  When the composition of a mixture is said to be <em|fixed> or <em|constant>
  during changes of temperature, pressure, or volume, this means there is no
  change in the relative <em|amounts> or <em|masses> of the various species.
  A <index-complex|<tuple|mixture|fixed composition>|||<tuple|Mixture|of
  fixed composition>>mixture of fixed composition has fixed values of mole
  fractions, mass fractions, and molalities, but not necessarily of
  concentrations and partial pressures. Concentrations will change if the
  volume changes, and partial pressures in a gas mixture will change if the
  pressure changes.

  <section|Partial Molar Quantities><label|c9 sec pmq>

  The symbol <math|X<rsub|i>>, where <math|X> is an extensive property of a
  homogeneous mixture and the subscript <math|i> identifies a constituent
  species of the mixture, denotes the <subindex|Partial
  molar|quantity><newterm|partial molar quantity> of species <math|i> defined
  by

  <equation-cov2|<label|X_i=dX/dn_i>X<rsub|i><defn><Pd|X|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>|(mixture)>

  This is the rate at which property <math|X> changes with the amount of
  species <math|i> added to the mixture as the temperature, the pressure, and
  the amounts of all other species are kept constant. A partial molar
  quantity is an <em|intensive> state function. Its value depends on the
  temperature, pressure, and composition of the mixture.

  Keep in mind that as a practical matter, a macroscopic amount of a charged
  species (i.e., an ion) cannot be added by itself to a phase because of the
  huge electric charge that would result. Thus if species <math|i> is
  charged, <math|X<rsub|i>> as defined by Eq. <reference|X_i=dX/dn_i> is a
  theoretical concept whose value cannot be determined
  experimentally.<label|can't measure X(i) of ion>

  <\quote-env>
    \ An older notation for a partial molar quantity uses an overbar:
    <math|<wide|X|\<bar\>><rsub|i>>. The notation <math|X<rsub|i><rprime|'>>
    was suggested in the first edition of the IUPAC Green Book,<footnote|Ref.
    <cite|greenbook-1>, p. 44.> but is not mentioned in later editions.
  </quote-env>

  <subsection|Partial molar volume><label|9-partial molar volume><label|c9
  sec pmq-volume>

  <index-complex|<tuple|partial molar|volume>||c9 sec pmq-volume
  idx1|<tuple|Partial molar|volume>>In order to gain insight into the
  significance of a partial molar quantity as defined by Eq.
  <reference|X_i=dX/dn_i>, let us first apply the concept to the <em|volume>
  of an open single-phase system. Volume has the advantage for our example of
  being an extensive property that is easily visualized. Let the system be a
  binary mixture of water (substance A) and methanol (substance B), two
  liquids that mix in all proportions. The partial molar volume of the
  methanol, then, is the rate at which the system volume changes with the
  amount of methanol added to the mixture at constant temperature and
  pressure: <math|V<B>=<pd|V|n<B>|T,p,n<A>>>.

  At <math|25 <degC>> and <math|1<br>>, the molar volume of pure water is
  <math|V<mA><rsup|\<ast\>>=18.07 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>
  and that of pure methanol is <math|V<mB><rsup|\<ast\>>=40.75
  <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>. If we mix <math|100.0
  <text|cm><rsup|3>> of water at <math|25 <degC>> with <math|100.0
  <text|cm><rsup|3>> of methanol at <math|25 <degC>>, we find the volume of
  the resulting mixture at <math|25 <degC>> is not the sum of the separate
  volumes, <math|200.0 <text|cm><rsup|3>>, but rather the slightly smaller
  value <math|193.1 <text|cm><rsup|3>>.<label|water-methanol mixt>The
  difference is due to new intermolecular interactions in the mixture
  compared to the pure liquids.

  Let us calculate the mole fraction composition of this mixture:

  <\align>
    <tformat|<table|<row|<cell|n<A>>|<cell|=<frac|V<A><rsup|\<ast\>>|V<mA><rsup|\<ast\>>>=<frac|100.0
    <text|cm><rsup|3>|18.07 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>=5.53<mol><eq-number>>>|<row|<cell|n<B>>|<cell|=<frac|V<B><rsup|\<nosymbol\>>|V<mB><rsup|\<nosymbol\>>>=<frac|100.0
    <text|cm><rsup|3>|40.75 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>=2.45<mol><eq-number>>>|<row|<cell|x<B>>|<cell|=<frac|n<B>|n<A>+n<B>>=<frac|2.45
    <mol>|5.53 <mol>+2.45 <mol>>=0.307<eq-number>>>>>
  </align>

  Now suppose we prepare a large volume of a mixture of this composition
  (<math|x<B>=0.307>) and add an additional <math|40.75 <text|cm><rsup|3>>
  (one mole) of pure methanol, as shown in Fig.
  <reference|fig:9-tubes>(a).<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/TUBES.eps|129pt|86pt||>>
        <label|fig:9-tubes>Addition of pure methanol (substance B) to a
        water--methanol mixture at constant <math|T> and <math|p>.

        <\enumerate-alpha>
          <item><math|40.75 <text|cm><rsup|3>> (one mole) of methanol is
          placed in a narrow tube above a much greater volume of a mixture
          (shaded) of composition <math|x<B>=0.307>. The dashed line
          indicates the level of the upper meniscus.

          <item>After the two liquid phases have mixed by diffusion, the
          volume of the mixture has increased by only <math|38.8
          <text|cm><rsup|3>>.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float> If the initial volume of the mixture at <math|25 <degC>> was
  <math|10\<comma\>000.0 <text|cm><rsup|3>>, we find the volume of the new
  mixture at the same temperature is <math|10,038.8 <text|cm><rsup|3>>, an
  increase of <math|38.8 <text|cm><rsup|3>>\Vsee Fig.
  <reference|fig:9-tubes>(b). The amount of methanol added is not
  infinitesimal, but it is small enough compared to the amount of initial
  mixture to cause very little change in the mixture composition: <math|x<B>>
  increases by only <math|0.5>%. Treating the mixture as an
  <subindex|System|open>open system, we see that the addition of one mole of
  methanol to the system at constant <math|T>, <math|p>, and <math|n<A>>
  causes the system volume to increase by <math|38.8 <text|cm><rsup|3>>. To a
  good approximation, then, the partial molar volume of methanol in the
  mixture, <math|V<B>=<pd|V|n<B>|T,p,n<A>>>, is given by
  <math|<Del>V/<Del>n<B>=38.8 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>.

  The volume of the mixture to which we add the methanol does not matter as
  long as it is large. We would have observed practically the same volume
  increase, <math|38.8 <text|cm><rsup|3>>, if we had mixed one mole of pure
  methanol with <math|100\<comma\>000.0 <text|cm><rsup|3>> of the mixture
  instead of only <math|10\<comma\>000.0 <text|cm><rsup|3>>.

  <subsubindex|Partial molar|volume|interpretation>Thus, we may interpret the
  partial molar volume of B as the volume change per amount of B added at
  constant <math|T> and <math|p> when B is mixed with such a large volume of
  mixture that the composition is not appreciably affected. We may also
  interpret the partial molar volume as the volume change per amount when an
  infinitesimal amount is mixed with a finite volume of mixture.

  The partial molar volume of B is an intensive property that is a function
  of the composition of the mixture, as well as of <math|T> and <math|p>. The
  limiting value of <math|V<B>> as <math|x<B>> approaches <math|1> (pure B)
  is <math|V<mB><rsup|\<ast\>>>, the molar volume of pure B. We can see this
  by writing <math|V=n<B>V<mB><rsup|\<ast\>>> for pure B, giving us
  <math|V<B><around|(|x<B|=>1|)>=<pd|n<B>*V<mB><rsup|\<ast\>>|n<B>|T,p,n<A>>=V<mB><rsup|\<ast\>>>.

  If the mixture is a binary mixture of A and B, and <math|x<B>> is small, we
  may treat the mixture as a dilute solution of solvent A and solute B. As
  <math|x<B>> approaches <math|0> in this solution, <math|V<B>> approaches a
  certain limiting value that is the volume increase per amount of B mixed
  with a large amount of pure A. In the resulting mixture, each solute
  molecule is surrounded only by solvent molecules. We denote this limiting
  value of <math|V<B>> by <math|V<B><rsup|\<infty\>>>, the partial molar
  volume of solute B at infinite dilution.

  <\quote-env>
    \ It is possible for a partial molar volume to be <subsubindex|Partial
    molar|volume|negative value of><em|negative>. Magnesium sulfate, in
    aqueous solutions of molality less than <math|0.07
    <text|mol>\<cdot\><text|kg><rsup|-1>>, has a negative partial molar
    volume. Physically, this means that when a small amount of crystalline
    MgSO<rsub|<math|4>> dissolves at constant temperature in water, the
    liquid phase contracts. This unusual behavior is due to strong attractive
    water--ion interactions.
  </quote-env>

  <index-complex|<tuple|partial molar|volume>||c9 sec pmq-volume
  idx1|<tuple|Partial molar|volume>>

  <subsection|The total differential of the volume in an open
  system><label|c9 sec pmq-totdiff>

  Consider an <subindex|System|open>open single-phase system consisting of a
  mixture of nonreacting substances. How many independent variables does this
  system have?

  We can prepare the mixture with various amounts of each substance, and we
  are able to adjust the temperature and pressure to whatever values we wish
  (within certain limits that prevent the formation of a second phase). Each
  choice of temperature, pressure, and amounts results in a definite value of
  every other property, such as volume, density, and mole fraction
  composition. Thus, an open single-phase system of <math|C> substances has
  <math|2+C> independent variables.<footnote|<math|C> in this kind of system
  is actually the <index|Components, number of>number of <em|components>. The
  number of components is usually the same as the number of substances, but
  is less if certain constraints exist, such as reaction equilibrium or a
  fixed mixture composition. The general meaning of <math|C> will be
  discussed in Sec. <reference|13-phase rule>.>

  For a binary mixture (<math|C=2>), the number of independent variables is
  four. We may choose these variables to be <math|T>, <math|p>, <math|n<A>>,
  and <math|n<B>>, and write the <subindex|Volume|total differential in an
  open system><index-complex|<tuple|total differential|volume>|||<tuple|Total
  differential|of the volume>>total differential of <math|V> in the general
  form

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dvar|V>>|<cell|=>|<cell|<Pd|V|T|p,n<rsub|<text|A>>,n<rsub|<text|B>>>*<dvar|T>+<Pd|V|p|T,n<rsub|<text|A>>,n<rsub|<text|B>>>*<dvar|p>>>|<row|<cell|>|<cell|>|<cell|+<Pd|V|n<rsub|<text|A>>|T,p,n<rsub|<text|B>>>*<dvar|n<rsub|<text|A>>>+<Pd|V|n<rsub|<text|B>>|T,p,n<rsub|<text|A>>>*<dvar|n<rsub|<text|B>>><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<around*|(|<text|binary
    mixture>|)>>>>>><label|dV=()dT+()dp+()dnA+()dnB>>>>>
  </eqnarray*>

  We know the first two partial derivatives on the right side are given
  by<footnote|See Eqs. <reference|alpha def> and <reference|kappaT def>,
  which are for closed systems.>

  <\equation>
    <Pd|V|T|p,n<A>,n<B>>=\<alpha\>*V<space|2em><Pd|V|p|T,n<A>,n<B>>=-<kT>V
  </equation>

  We identify the last two partial derivatives on the right side of Eq.
  <reference|dV=()dT+()dp+()dnA+()dnB> as the partial molar volumes
  <math|V<A>> and <math|V<B>>. Thus, we may write the total differential of
  <math|V> for this open system in the compact form

  <equation-cov2|<label|dV=alpha*VdT-kappaT*Vdp+()dnA+()dnB><dif>V=\<alpha\>*V*<dif>T-<kT>*V*<difp>+V<A>*<dif>n<A>+V<B>*<dif>n<B>|(binary
  mixture)>

  If we compare this equation with the total differential of <math|V> for a
  one-component <em|closed> system, <math|<dif>V=\<alpha\>*V*<dif>T-<kT>*V*<difp>>
  (Eq. <reference|dV=alpha*VdT-kappaT*Vdp>), we see that an additional term
  is required for each constituent of the mixture to allow the system to be
  open and the composition to vary.

  When <math|T> and <math|p> are held constant, Eq.
  <reference|dV=alpha*VdT-kappaT*Vdp+()dnA+()dnB> becomes

  <\equation-cov2|<label|dV=VA*dnA+VB*dnB><dif>V=V<A>*<dif>n<A>+V<B>*<dif>n<B>>
    (binary mixture,

    constant <math|T> and <math|p>)
  </equation-cov2>

  We obtain an important relation between the mixture volume and the partial
  molar volumes by imagining the following process. Suppose we continuously
  pour pure water and pure methanol at constant but not necessarily equal
  volume rates into a stirred, thermostatted container to form a mixture of
  increasing volume and constant composition, as shown schematically in Fig.
  <vpageref|fig:9-additivity>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/additiv.eps|177pt|54pt||>>
        <label|fig:9-additivity>Mixing of water (A) and methanol (B) in a 2:1
        ratio of volumes to form a mixture of increasing volume and constant
        composition. The <em|system> is the mixture.
      </big-figure>
    </framed>
  </float> If this mixture remains at constant <math|T> and <math|p> as it is
  formed, none of its intensive properties change during the process, and the
  partial molar volumes <math|V<A>> and <math|V<B>> remain constant. Under
  these conditions, we can integrate Eq. <reference|dV=VA*dnA+VB*dnB> to
  obtain the <index|Additivity rule><newterm|additivity rule> for
  volume:<footnote|The equation is an example of the result of applying
  Euler's theorem on homogeneous functions to <math|V> treated as a function
  of <math|n<A>> and <math|n<B>>.>

  <equation-cov2|<label|V=VA*nA+VB*nB>V=V<A>*n<A>+V<B>*n<B>|(binary mixture)>

  This equation allows us to calculate the mixture volume from the amounts of
  the constituents and the appropriate partial molar volumes for the
  particular temperature, pressure, and composition.

  For example, given that the partial molar volumes in a water\Umethanol
  mixture of composition <math|x<B>=0.307> are <math|V<A>=17.74
  <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>> and <math|V<B>=38.76
  <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>, we calculate the volume of
  the water\Umethanol mixture described at the beginning of Sec.
  <reference|9-partial molar volume> as follows:

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|V>|<cell|=<around|(|17.74
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>|)><around|(|5.53<mol>|)>+<around|(|38.76
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>|)><around|(|2.45<mol>|)>>>|<row|<cell|>|<cell|=193.1
      <text|cm><rsup|3>>>>>
    </eqsplit>
  </equation>

  We can differentiate Eq. <reference|V=VA*nA+VB*nB> to obtain a general
  expression for <math|<dif>V> under conditions of constant <math|T> and
  <math|p>:

  <\equation>
    <dif>V=V<A>*<dif>n<A>+V<B>*<dif>n<B>+n<A>*<dif>V<A>+n<B>*<dif>V<B>
  </equation>

  But this expression for <math|<dif>V> is consistent with Eq.
  <reference|dV=VA*dnA+VB*dnB> only if the sum of the last two terms on the
  right is zero:

  <\equation-cov2|<label|nA*dVA+nB*dVB=0>n<A>*<dif>V<A>+n<B>*<dif>V<B>=0>
    (binary mixture,

    constant <math|T> and <math|p>)
  </equation-cov2>

  Equation <reference|nA*dVA+nB*dVB=0> is the <index|Gibbs--Duhem
  equation><newterm|Gibbs\UDuhem equation> for a binary mixture, applied to
  partial molar volumes. (Section <reference|9-general relations> will give a
  general version of this equation.) Dividing both sides of the equation by
  <math|n<A>+n<B>> gives the equivalent form

  <\equation-cov2|<label|xA*dVA+xB*dVB=0>x<A>*<dif>V<A>+x<B>*<dif>V<B>=0>
    (binary mixture,

    constant <math|T> and <math|p>)
  </equation-cov2>

  Equation <reference|nA*dVA+nB*dVB=0> shows that changes in the values of
  <math|V<A>> and <math|V<B>> are related when the composition changes at
  constant <math|T> and <math|p>. If we rearrange the equation to the form

  <\equation-cov2|<label|dVA=-(nB/nA)dVB><dif>V<A>=-<frac|n<B>|n<A>>*<dif>V<B>>
    (binary mixture,

    constant <math|T> and <math|p>)
  </equation-cov2>

  we see that a composition change that <em|increases ><math|V<B>> (so that
  <math|<dif>V<B>> is positive) must make <math|V<A>> <em|decrease>.

  <subsection|Evaluation of partial molar volumes in binary
  mixtures><label|c9 sec pmq-mixtures-binary>

  <label|9-intercepts>The partial molar volumes <math|V<A>> and <math|V<B>>
  in a binary mixture can be evaluated by the <index|Method of
  intercepts><newterm|method of intercepts>. To use this method, we plot
  experimental values of the quantity <math|V/n> (where <math|n> is
  <math|n<A>+n<B>>) versus the mole fraction <math|x<B>>. <math|V/n> is
  called the <index|Mean molar volume><subindex|Volume|mean molar><em|mean
  molar volume>.

  See Fig. <reference|fig:9-water+MeOH vols>(a) on page
  <pageref|fig:9-water+MeOH vols><\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/H2O-MEOH.eps|379pt|214pt||>>
        <label|fig:9-water+MeOH vols>Mixtures of water (A) and methanol (B)
        at <math|25 <degC>> and <math|1<br>>.<space|.15em><note-ref|+pnwft35dEGUGeZ>

        <\enumerate-alpha>
          <item>Mean molar volume as a function of <math|x<B>>. The dashed
          line is the tangent to the curve at <math|x<B>=0.307>.

          <item>Molar volume of mixing as a function of <math|x<B>>. The
          dashed line is the tangent to the curve at <math|x<B>=0.307>.

          <item>Partial molar volumes as functions of <math|x<B>>. The points
          at <math|x<B>=0.307> (open circles) are obtained from the
          intercepts of the dashed line in either (a) or (b).
        </enumerate-alpha>

        \;

        <note-inline|Based on data in Ref. <cite|benson-80>.|+pnwft35dEGUGeZ>
      </big-figure>
    </framed>
  </float> for an example. In this figure, the tangent to the curve drawn at
  the point on the curve at the composition of interest (the composition used
  as an illustration in Sec. <reference|9-partial molar volume>) intercepts
  the vertical line where <math|x<B>> equals <math|0> at <math|V/n=V<A>=17.7
  <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>, and intercepts the vertical
  line where <math|x<B>> equals <math|1> at <math|V/n=V<B>=38.8
  <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>.

  <\quote-env>
    \ To derive this property of a tangent line for the plot of <math|V/n>
    versus <math|x<B>>, we use Eq. <reference|V=VA*nA+VB*nB> to write

    <\eqnarray*>
      <tformat|<table|<row|<cell|<around|(|V/n|)>>|<cell|=>|<cell|<frac|V<A>*n<A>+V<B>*n<B>|n>=V<A>*x<A>+V<B>*x<B>>>|<row|<cell|>|<cell|=>|<cell|V<A>*<around|(|1-x<B>|)>+V<B>*x<B>=<around|(|V<B>-V<A>|)>*x<B>+V<A><eq-number><label|V/n=>>>>>
    </eqnarray*>

    When we differentiate this expression for <math|V/n> with respect to
    <math|x<B>>, keeping in mind that <math|V<A>> and <math|V<B>> are
    functions of <math|x<B>>, we obtain

    <\eqnarray*>
      <tformat|<table|<row|<cell|<frac|<dif><around|(|V/n|)>|<dx><B>>>|<cell|=>|<cell|<dfrac|<dvar|<around*|[|<around|(|V<B>-V<A>|)>*x<B>+V<A>|]>>|<dx><B>>>>|<row|<cell|>|<cell|=>|<cell|V<B>-V<A>+<around*|(|<frac|<dif>V<B>|<dx><B>>-<frac|<dif>V<A>|<dx><B>>|)>*x<B>+<frac|<dif>V<A>|<dx><B>>>>|<row|<cell|>|<cell|=>|<cell|V<B>-V<A>+<around*|(|<frac|<dif>V<A>|<dx><B>>|)>*<around|(|1-x<B>|)>+<around*|(|<frac|<dif>V<B>|<dx><B>>|)>*x<B>>>|<row|<cell|>|<cell|=>|<cell|V<B>-V<A>+<around*|(|<frac|<dif>V<A>|<dx><B>>|)>*x<A>+<around*|(|<frac|<dif>V<B>|<dx><B>>|)>*x<B><eq-number><label|d(V/n)/dx(B)=>>>>>
    </eqnarray*>

    The differentials <math|<dif>V<A>> and <math|<dif>V<B>> are related to
    one another by the Gibbs\UDuhem equation (Eq.
    <reference|xA*dVA+xB*dVB=0>): <math|x<A>*<dif>V<A>+x<B>*<dif>V<B>=0>. We
    divide both sides of this equation by <math|<dx><B>> to obtain

    <\equation>
      <label|[dV(A)/dxB]xA+[dV(B)/dxB]xB=0><around*|(|<frac|<dif>V<A>|<dx><B>>|)>*x<A>+<around*|(|<frac|<dif>V<B>|<dx><B>>|)>*x<B>=0
    </equation>

    and substitute in Eq. <reference|d(V/n)/dx(B)=> to obtain

    <\equation>
      <label|d(V/n)/dxB=V(B)-V(A)><frac|<dif><around|(|V/n|)>|<dx><B>>=V<B>-V<A>
    </equation>

    Let the partial molar volumes of the constituents of a binary mixture of
    arbitrary composition <math|x<rprime|'><B>> be <math|V<rprime|'><A>> and
    <math|V<rprime|'><B>>. Equation <reference|V/n=> shows that the value of
    <math|V/n> at the point on the curve of <math|V/n> versus <math|x<B>>
    where the composition is <math|x<rprime|'><B>> is
    <math|<around|(|V<rprime|'><B>-V<rprime|'><A>|)>*x<rprime|'><B>+V<rprime|'><A>>.
    Equation <reference|d(V/n)/dxB=V(B)-V(A)> shows that the tangent to the
    curve at this point has a slope of <math|V<rprime|'><B>-V<rprime|'><A>>.
    The equation of the line that passes through this point and has this
    slope, and thus is the tangent to the curve at this point, is
    <math|y=<around|(|V<rprime|'><B>-V<rprime|'><A>|)>*x<B>+V<rprime|'><A>>,
    where <math|y> is the vertical ordinate on the plot of
    <math|<around|(|V/n|)>> versus <math|x<B>>. The line has intercepts
    <math|y=V<rprime|'><A>> at <math|x<B|=>0> and <math|y=V<rprime|'><B>> at
    <math|x<B|=>1>.
  </quote-env>

  <index|Method of intercepts>A variant of the method of
  intercepts<label|intercepts variant>is to plot the molar integral volume of
  mixing given by

  <\equation>
    <label|binary mixt Del(mix)V><Del>V<m><mix>=<frac|<Del>V<mix>|n>=<frac|V-n<A>*V<mA><rsup|\<ast\>>-n<B>*V<mB><rsup|\<ast\>>|n>
  </equation>

  versus <math|x<B>>, as illustrated in Fig. <reference|fig:9-water+MeOH
  vols>(b). <math|<Del>V<mix>> is the integral volume of mixing\Vthe volume
  change at constant <math|T> and <math|p> when solvent and solute are mixed
  to form a mixture of volume <math|V> and total amount <math|n> (see Sec.
  <reference|11-mixing in general>). The tangent to the curve at the
  composition of interest has intercepts <math|V<A>-V<mA><rsup|\<ast\>>> at
  <math|x<B|=>0> and <math|V<B>-V<mB><rsup|\<ast\>>> at <math|x<B|=>1>.

  <\quote-env>
    \ To see this, we write

    <\eqnarray*>
      <tformat|<table|<row|<cell|<Del>V<m><mix>>|<cell|=>|<cell|<around|(|V/n|)>-x<A>*V<mA><rsup|\<ast\>>-x<B>*V<mB><rsup|\<ast\>>>>|<row|<cell|>|<cell|=>|<cell|<around|(|V/n|)>-<around|(|1-x<B>|)>*V<mA><rsup|\<ast\>>-x<B>*V<mB><rsup|\<ast\>><eq-number>>>>>
    </eqnarray*>

    We make the substitution <math|<around|(|V/n|)>=<around|(|V<B>-V<A>|)>*x<B>+V<A>>
    from Eq. <reference|V/n=> and rearrange:

    <\equation>
      <label|Vm^E=><Del>V<m><mix>=<around*|[|<around*|(|V<B>-V<mB><rsup|\<ast\>>|)>-<around*|(|V<A>-V<mA><rsup|\<ast\>>|)>|]>*x<B>+<around*|(|V<A>-V<mA><rsup|\<ast\>>|)>
    </equation>

    Differentiation with respect to <math|x<B>> yields

    <\eqnarray*>
      <tformat|<table|<row|<cell|<frac|<dif><Del>V<m><mix>|<dx><B>>>|<cell|=>|<cell|<around*|(|V<B>-V<mB><rsup|\<ast\>>|)>-<around*|(|V<A>-V<mA><rsup|\<ast\>>|)>+<around*|(|<frac|<dif>V<B>|<dx><B>>-<frac|<dif>V<A>|<dx><B>>|)>*x<B>+<frac|<dif>V<A>|<dx><B>>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|V<B>-V<mB><rsup|\<ast\>>|)>-<around*|(|V<A>-V<mA><rsup|\<ast\>>|)>+<around*|(|<frac|<dif>V<A>|<dx><B>>|)>*<around|(|1-x<B>|)>+<around*|(|<frac|<dif>V<B>|<dx><B>>|)>*x<B>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|V<B>-V<mB><rsup|\<ast\>>|)>-<around*|(|V<A>-V<mA><rsup|\<ast\>>|)>+<around*|(|<frac|<dif>V<A>|<dx><B>>|)>*x<A>+<around*|(|<frac|<dif>V<B>|<dx><B>>|)>*x<B><eq-number>>>>>
    </eqnarray*>

    With a substitution from Eq. <reference|[dV(A)/dxB]xA+[dV(B)/dxB]xB=0>,
    this becomes

    <\equation>
      <label|dVm^E/dxB=><frac|<dif><Del>V<m><mix>|<dx><B>>=<around*|(|V<B>-V<mB><rsup|\<ast\>>|)>-<around*|(|V<A>-V<mA><rsup|\<ast\>>|)>
    </equation>

    Equations <reference|Vm^E=> and <reference|dVm^E/dxB=> are analogous to
    Eqs. <reference|V/n=> and <reference|d(V/n)/dxB=V(B)-V(A)>, with
    <math|V/n> replaced by <math|<Del>V<m><mix>>, <math|V<A>> by
    <math|<around|(|V<A>-V<mA><rsup|\<ast\>>|)>>, and <math|V<B>> by
    <math|<around|(|V<B>-V<mB><rsup|\<ast\>>|)>>. Using the same reasoning as
    for a plot of <math|V/n> versus <math|x<B>>, we find the intercepts of
    the tangent to a point on the curve of <math|<Del>V<m><mix>> versus
    <math|x<B>> are at <math|V<A>-V<mA><rsup|\<ast\>>> and
    <math|V<B>-V<mB><rsup|\<ast\>>>.
  </quote-env>

  Figure <reference|fig:9-water+MeOH vols> shows smoothed experimental data
  for water\Umethanol mixtures plotted in both kinds of graphs, and the
  resulting partial molar volumes as functions of composition. Note in Fig.
  <reference|fig:9-water+MeOH vols>(c) how the <math|V<A>> curve mirrors the
  <math|V<B>> curve as <math|x<B>> varies, as predicted by the
  <index|Gibbs--Duhem equation>Gibbs\UDuhem equation. The minimum in
  <math|V<B>> at <math|x<B|\<approx\>>\<approx\>0.09> is mirrored by a
  maximum in <math|V<A>> in agreement with Eq. <reference|dVA=-(nB/nA)dVB>;
  the maximum is much attenuated because <math|n<B>/n<A>> is much less than
  unity.

  <\quote-env>
    Macroscopic measurements are unable to provide unambiguous information
    about molecular structure. Nevertheless, it is interesting to speculate
    on the implications of the minimum observed for the partial molar volume
    of methanol. One interpretation is that in a mostly aqueous environment,
    there is association of methanol molecules, perhaps involving the
    formation of dimers.
  </quote-env>

  <subsection|General relations><label|9-general relations><label|c9 sec
  pmq-general-relations>

  The discussion above of partial molar volumes used the notation
  <math|V<mA><rsup|\<ast\>>> and <math|V<mB><rsup|\<ast\>>> for the molar
  volumes of pure A and B. The partial molar volume of a pure substance is
  the same as the molar volume, so we can simplify the notation by using
  <math|V<A><rsup|\<ast\>>> and <math|V<B><rsup|\<ast\>>> instead. Hereafter,
  this book will denote molar quantities of pure substances by such symbols
  as <math|V<A><rsup|\<ast\>>>, <math|H<B><rsup|\<ast\>>>, and
  <math|S<rsub|i><rsup|\<ast\>>>.

  The relations derived above for the volume of a binary mixture may be
  generalized for any extensive property <math|X> of a mixture of any number
  of constituents. The <index-complex|<tuple|partial
  molar|quantity|general>|||<tuple|Partial molar|quantity|in general>>partial
  molar quantity of species <math|i>, defined by

  <\equation>
    X<rsub|i><defn><Pd|X|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>
  </equation>

  is an intensive property that depends on <math|T>, <math|p>, and the
  composition of the mixture. The <index|Additivity rule>additivity rule for
  property <math|X> is

  <equation-cov2|<label|X=sum(X_i*n_i)>X=<big|sum><rsub|i>n<rsub|i>*X<rsub|i>|(mixture)>

  and the <index|Gibbs--Duhem equation>Gibbs\UDuhem equation applied to
  <math|X> can be written in the equivalent forms

  <equation-cov2|<label|sum(n_i)dX_i=0><big|sum><rsub|i>n<rsub|i><dif>X<rsub|i>=0|(constant
  <math|T> and <math|p>)>

  and

  <equation-cov2|<label|sum(x_i)dX_i=0><big|sum><rsub|i>x<rsub|i><dif>X<rsub|i>=0|(constant
  <math|T> and <math|p>)>

  These relations can be applied to a mixture in which each species <math|i>
  is a nonelectrolyte substance, an electrolyte substance that is dissociated
  into ions, or an individual ionic species. In Eq.
  <reference|sum(x_i)dX_i=0>, the mole fraction <math|x<rsub|i>> must be
  based on the different species considered to be present in the mixture. For
  example, an aqueous solution of NaCl could be treated as a mixture of
  components A=H<rsub|<math|2>>O and B=NaCl, with <math|x<B>> equal to
  <math|n<B>/<around|(|n<A>+n<B>|)>>; or the constituents could be taken as
  H<rsub|<math|2>>O, Na<rsup|<math|+>>, and Cl<rsup|<math|->>, in which case
  the mole fraction of Na<rsup|<math|+>> would be
  <math|x<rsub|+>=n<rsub|+>/<around|(|n<A>+n<rsub|+>+n<rsub|->|)>>.

  A general method to evaluate the partial molar quantities <math|X<A>> and
  <math|X<B>> in a binary mixture is based on the variant of the method of
  intercepts described in Sec. <reference|9-intercepts>. The molar mixing
  quantity <math|<Del>X<mix>/n> is plotted versus <math|x<B>>, where
  <math|<Del>X<mix>> is <math|<around|(|X-n<A>*X<A><rsup|\<ast\>>-n<B>*X<B><rsup|\<ast\>>|)>>.<label|Del
  Xm(mix) plot>On this plot, the tangent to the curve at the composition of
  interest has intercepts equal to <math|X<A|->*X<A><rsup|\<ast\>>> at
  <math|x<B|=>0> and <math|X<B|->*X<B><rsup|\<ast\>>> at <math|x<B|=>1>.

  We can obtain experimental values of such partial molar quantities of an
  uncharged species as <math|V<rsub|i>>, <math|C<rsub|p,i>>, and
  <math|S<rsub|i>>. It is not possible, however, to evaluate the partial
  molar quantities <math|U<rsub|i>>, <math|H<rsub|i>>, <math|A<rsub|i>>, and
  <math|G<rsub|i>> because these quantities involve the internal energy
  brought into the system by the species, and we cannot evaluate the absolute
  value of internal energy (Sec. <reference|2-internal energy>). For example,
  while we can evaluate the difference <math|H<rsub|i>-H<rsub|i><rsup|\<ast\>>>
  from calorimetric measurements of enthalpies of mixing, we cannot evaluate
  the partial molar enthalpy <math|H<rsub|i>> itself. We can, however,
  include such quantities as <math|H<rsub|i>> in useful theoretical
  relations.

  <\quote-env>
    \ As mentioned on page <pageref|can't measure X(i) of ion>, a partial
    molar quantity of a <em|charged> species is something else we cannot
    evaluate. It is possible, however, to obtain values relative to a
    reference ion. Consider an aqueous solution of a fully-dissociated
    electrolyte solute with the formula <math|<text|M<rsub|\<nu\><rsub|+>>>*<text|X<rsub|\<nu\><rsub|->>>>
    , where <math|\<nu\><rsub|+>> and <math|\<nu\><rsub|->> are the numbers
    of cations and anions per solute formula unit. The partial molar volume
    <math|V<B>> of the solute, which can be determined experimentally, is
    related to the (unmeasurable) partial molar volumes <math|V<rsub|+>> and
    <math|V<rsub|->> of the constituent ions by

    <\equation>
      V<B>=\<nu\><rsub|+>*V<rsub|+>+\<nu\><rsub|->*V<rsub|->
    </equation>

    For aqueous solutions, the usual reference ion is H<rsup|<math|+>>, and
    the partial molar volume of this ion at infinite dilution is arbitrarily
    set equal to zero: <math|V<rsub|<chem|<text|H>|+>><rsup|\<infty\>>=0>.

    For example, given the value (at <math|298.15<K>> and <math|1<br>>) of
    the partial molar volume at infinite dilution of aqueous hydrogen
    chloride

    <\equation>
      V<rsub|<chem|<text|HCl>|>><rsup|\<infty\>>=17.82
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>
    </equation>

    we can find the so-called \Pconventional\Q partial molar
    volume<label|conventional V(i) for ion> of Cl<rsup|<math|->> ion:

    <\equation>
      V<rsub|<chem|<text|Cl>|->><rsup|\<infty\>>=V<rsub|<chem|<text|HCl>|>><rsup|\<infty\>>-V<rsub|<chem|<text|H>|+>><rsup|\<infty\>>=17.82
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>
    </equation>

    Going one step further, the measured value
    <math|V<rsub|<chem|<text|NaCl>|>><rsup|\<infty\>>=16.61
    <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>> gives, for
    Na<rsup|<math|+>> ion, the conventional value

    <\equation>
      V<rsub|<text|<chem|<text|Na>|+>>><rsup|\<infty\>>=V<rsub|<text|NaCl>><rsup|\<infty\>>-V<rsub|<chem|<text|Cl>|->><rsup|\<infty\>>=<around|(|16.61-17.82|)>
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>=-1.21
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>
    </equation>
  </quote-env>

  <subsection|Partial specific quantities><label|c9
  sec-pmq-specific-quantities>

  A <subindex|Partial|specific quantity><newterm|partial specific quantity>
  of a substance is the partial molar quantity divided by the molar mass, and
  has dimensions of volume divided by mass. For example, the partial specific
  volume <subindex|Partial|specific volume><math|v<B>> of solute B in a
  binary solution is given by

  <\equation>
    v<B>=<frac|V<B>|M<B>>=<bPd|V|m<around*|(|<text|B>|)>|T,p,m<around*|(|<text|A>|)>>
  </equation>

  where <math|m<around*|(|<text|A>|)>> and <math|m<around|(|<text|B>|)>> are
  the masses of solvent and solute.<label|9-partial specific volume>

  Although this book makes little use of specific quantities and partial
  specific quantities, in some applications they have an advantage over molar
  quantities and partial molar quantities because they can be evaluated
  without knowledge of the molar mass. For instance, the value of a solute's
  partial specific volume is used to determine its molar mass by the method
  of sedimentation equilibrium (Sec. <reference|9-centrifuge>).

  The general relations in Sec. <reference|9-general relations> involving
  partial molar quantities may be turned into relations involving partial
  specific quantities by replacing amounts by masses, mole fractions by mass
  fractions, and partial molar quantities by partial specific quantities.
  Using volume as an example, we can write an additivity relation
  <math|V=<big|sum><rsub|i>m<around|(|i|)>*v<rsub|i>>, and Gibbs\UDuhem
  relations <math|<big|sum><rsub|i>m<around|(|i|)>*<dif>v<rsub|i>=0> and
  <math|<big|sum><rsub|i>w<rsub|i>*<dif>v<rsub|i>=0>. For a binary mixture of
  A and B, we can plot the specific volume <math|v> versus the mass fraction
  <math|w<B>>; then the tangent to the curve at a given composition has
  intercepts equal to <math|v<A>> at <math|w<B|=>0> and <math|v<B>> at
  <math|w<B|=>1>. A variant of this plot is
  <math|<around*|(|v-w<A>*v<A><rsup|\<ast\>>-w<B>*v<B><rsup|\<ast\>>|)>>
  versus <math|w<B>>; the intercepts are then equal to
  <math|v<A>-v<A><rsup|\<ast\>>> and <math|v<B>-v<B><rsup|\<ast\>>>.

  <subsection|The chemical potential of a species in a mixture><label|9-chem
  pot of species in a mixt><label|c9 sec-pmq-mixture-chempot>

  Just as the molar Gibbs energy of a pure substance is called the
  <em|chemical potential> and given the special symbol <math|\<mu\>>, the
  partial molar Gibbs energy <math|G<rsub|i>> of species <math|i> in a
  mixture is called the <index-complex|<tuple|chemical potential|species in a
  mixture>|||<tuple|Chemical potential|of a species in a
  mixture>><newterm|chemical potential> of species <math|i>, defined by

  <equation-cov2|<label|mu_i=dG/dn_i>\<mu\><rsub|i><defn><Pd|G|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>|(mixture)>

  If there are work coordinates for nonexpansion work, the partial derivative
  is taken at constant values of these coordinates.

  The chemical potential of a species in a phase plays a crucial role in
  equilibrium problems, because it is a measure of the escaping tendency of
  the species from the phase. Although we cannot determine the absolute value
  of <math|\<mu\><rsub|i>> for a given state of the system, we are usually
  able to evaluate the difference between the value in this state and the
  value in a defined reference state.

  In an <subindex|System|open>open single-phase system containing a mixture
  of <math|s> different nonreacting species, we may in principle
  independently vary <math|T>, <math|p>, and the amount of each species. This
  is a total of <math|2+s> independent variables. The
  <index-complex|<tuple|total differential|gibbs energy of a
  mixture>|||<tuple|Total differential|of the Gibbs energy of a
  mixture>><subindex|Gibbs energy|total differential of, for a mixture>total
  differential of the Gibbs energy of this system is given by Eq.
  <vpageref|dG=-SdT+Vdp+sum(mu_i)dn_i>, often called the <index|Fundamental
  equation, Gibbs><subindex|Gibbs|fundamental equation>Gibbs fundamental
  equation:

  <equation-cov2|<label|dG=-SdT+Vdp+sum(mu_i*dn_i)><dif>G=-S*<dif>T+V*<difp>+<big|sum><rsub|i=1><rsup|s>\<mu\><rsub|i>*<dif>n<rsub|i>|(mixture)>

  Consider the special case of a mixture containing <em|charged> species, for
  example an aqueous solution of the electrolyte KCl. We could consider the
  constituents to be either the substances H<rsub|<math|2>>O and KCl, or else
  H<rsub|<math|2>>O and the species K<rsup|<math|+>> and Cl<rsup|<math|->>.
  Any mixture we can prepare in the laboratory must remain
  <subindex|Electrical|neutrality><index|Neutrality, electrical>electrically
  neutral, or virtually so. Thus, while we are able to independently vary the
  amounts of H<rsub|<math|2>>O and KCl, we cannot in practice independently
  vary the amounts of K<rsup|<math|+>> and Cl<rsup|<math|->> in the mixture.
  The chemical potential of the K<rsup|<math|+>> ion is defined as the rate
  at which the Gibbs energy changes with the amount of K<rsup|<math|+>> added
  at constant <math|T> and <math|p> while the amount of Cl<rsup|<math|->> is
  kept constant. This is a hypothetical process in which the net charge of
  the mixture increases. The chemical potential of a ion is therefore a valid
  but purely theoretical concept. Let A stand for H<rsub|<math|2>>O, B for
  KCl, <math|+> for K<rsup|<math|+>>, and <math|-> for Cl<rsup|<math|->>.
  Then it is theoretically valid to write the total differential of <math|G>
  for the KCl solution either as

  <\equation>
    <label|dG=....><dif>G=-S*<dif>T+V*<difp>+\<mu\><A>*<dif>n<A>+\<mu\><B>*<dif>n<B>
  </equation>

  or as

  <\equation>
    <label|dG=.....><dif>G=-S*<dif>T+V*<difp>+\<mu\><A>*<dif>n<A>+\<mu\><rsub|+>*<dif>n<rsub|+>+\<mu\><rsub|->*<dif>n<rsub|->
  </equation>

  <subsection|Equilibrium conditions in a multiphase, multicomponent
  system><label|9-eqm conditions><label|c9 sec pmq-equilcond-multicomp>

  <index-complex|<tuple|equilibrium conditions|multiphase multicomponent
  systems>||c9 sec pmq-equilcond-multicomp idx1|<tuple|Equilibrium
  conditions|in a multiphase multicomponent system>>This section extends the
  derivation described in Sec. <reference|8-multiphase>, which was for
  equilibrium conditions in a multiphase system containing a single
  substance, to a more general kind of system: one with two or more
  homogeneous phases containing mixtures of nonreacting species. The
  derivation assumes there are no internal partitions that could prevent
  transfer of species and energy between the phases, and that effects of
  gravity and other external force fields are negligible.

  The system consists of a reference phase, <math|<pha><rprime|'>>, and other
  phases labeled by <math|<pha|\<ne\>><pha><rprime|'>>. Species are labeled
  by subscript <math|i>. Following the procedure of Sec. <reference|8-eqm
  conditions>, we write for the total differential of the internal energy

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dvar|U>>|<cell|=>|<cell|<dif>U<aphp>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>U<aph>>>|<row|<cell|>|<cell|=>|<cell|T<aphp>*<dif>S<aphp>-p<aphp>*<dif>V<aphp>+<big|sum><rsub|i>\<mu\><rsub|i><aphp>*<dif>n<rsub|i><aphp>>>|<row|<cell|>|<cell|>|<cell|+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><around*|(|T<aph>*<dif>S<aph>-p<aph>*<dif>V<aph>+<big|sum><rsub|i>\<mu\><rsub|i><aph>*<dif>n<rsub|i><aph>|)><eq-number><label|dU(multiphase,mixt)>>>>>
  </eqnarray*>

  The conditions of isolation are

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dvar|U=0>>|<cell|<text|<space|2em>>>|<cell|<around*|(|<text|constant
    internal energy>|)><eq-number>>>|<row|<cell|<dif>V<aphp>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>V<aph>=0>|<cell|>|<cell|<around*|(|<text|no
    expansion work>|)><eq-number>>>|<row|<cell|<text|For each species
    <math|i>:>>|<cell|>|<cell|>>|<row|<cell|<dif>n<rsub|i><aphp>+<big|sum><rsub|<pha>\<ne\><pha>'><dif>n<rsub|i><aph>=0>|<cell|>|<cell|<around*|(|<text|closed
    system>|)><eq-number>>>>>
  </eqnarray*>

  We use these relations to substitute for <math|<dif>U>,
  <math|<dif>V<aphp>>, and <math|<dif>n<rsub|i><aphp>> in Eq.
  <reference|dU(multiphase,mixt)>. After making the further substitution
  <math|<dif>S<aphp>=<dif>S-<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>S<aph>>
  and solving for <math|<dif>S>, we obtain

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dif>S>|<cell|=>|<cell|<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|T<aphp>-T<aph>|T<aphp>>*<dif>S<aph>-<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|p<aphp>-p<aph>|T<aphp>>*<dif>V<aph>>>|<row|<cell|>|<cell|>|<cell|+<big|sum><rsub|i><big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|\<mu\><rsub|i><aphp>-\<mu\><rsub|i><aph>|T<aphp>>*<dif>n<rsub|i><aph><eq-number><label|dS=sum(alpha'
    ne alpha)...>>>>>
  </eqnarray*>

  This equation is like Eq. <vpageref|dS=()dS(alpha)-()dV(alpha)+()dn(alpha)>
  with provision for more than one species.

  In the equilibrium state of the isolated system, <math|S> has the maximum
  possible value, <math|<dif>S> is equal to zero for an infinitesimal change
  of any of the independent variables, and the coefficient of each term on
  the right side of Eq. <reference|dS=sum(alpha' ne alpha)...> is zero. We
  find that in this state each phase has the same temperature and the same
  pressure, and for each species the chemical potential is the same in each
  phase.

  Suppose the system contains a species <math|i<rprime|'>> that is
  effectively excluded from a particular phase, <math|<pha><rprime|''>>. For
  instance, sucrose molecules dissolved in an aqueous phase are not
  accommodated in the crystal structure of an ice phase, and a nonpolar
  substance may be essentially insoluble in an aqueous phase. We can treat
  this kind of situation by setting <math|<dif>n<rsup|<pha><rprime|''>><rsub|i<rprime|'>>>
  equal to zero. Consequently there is no equilibrium condition involving the
  chemical potential of this species in phase <math|<pha><rprime|''>>.

  To summarize these conclusions: In an equilibrium state of a multiphase,
  multicomponent system without internal partitions, the temperature and
  pressure are uniform throughout the system, and each species has a uniform
  chemical potential except in phases where it is excluded.

  <\quote-env>
    \ This statement regarding the uniform chemical potential of a species
    applies to both a substance and an ion, as the following argument
    explains. The derivation in this section begins with Eq.
    <reference|dU(multiphase,mixt)>, an expression for the total differential
    of <math|U>. Because it is a total differential, the expression requires
    the amount <math|n<rsub|i>> of each species <math|i> in each phase to be
    an independent variable. Suppose one of the phases is the aqueous
    solution of KCl used as an example at the end of the preceding section.
    In principle (but not in practice), the amounts of the species
    H<rsub|<math|2>>O, K<rsup|<math|+>>, and Cl<rsup|<math|->> can be varied
    independently, so that it is valid to include these three species in the
    sums over <math|i> in Eq. <reference|dU(multiphase,mixt)>. The derivation
    then leads to the conclusion that K<rsup|<math|+>> has the same chemical
    potential in phases that are in transfer equilibrium with respect to
    K<rsup|<math|+>>, and likewise for Cl<rsup|<math|->>. This kind of
    situation arises when we consider a Donnan membrane equilibrium (Sec.
    <reference|12-Donnan eqm>) in which transfer equilibrium of ions exists
    between solutions of electrolytes separated by a semipermeable membrane.
  </quote-env>

  <index-complex|<tuple|equilibrium conditions|multiphase multicomponent
  systems>||c9 sec pmq-equilcond-multicomp idx1|<tuple|Equilibrium
  conditions|in a multiphase multicomponent system>>

  <subsection|Relations involving partial molar quantities><label|c9
  sec-pmq-relations>

  <index-complex|<tuple|partial molar|quantity|general relations>||c9
  sec-pmq-relations idx1|<tuple|Partial molar|quantity|general
  relations>>Here we derive several useful relations involving partial molar
  quantities in a single-phase system that is a mixture. The independent
  variables are <math|T>, <math|p>, and the amount <math|n<rsub|i>> of each
  constituent species <math|i>.

  From Eqs. <reference|sum(n_i)dX_i=0> and <reference|sum(x_i)dX_i=0>, the
  Gibbs\UDuhem equation applied to the chemical potentials can be written in
  the equivalent forms

  <equation-cov2|<label|sum(n_i)dmu_i=0><big|sum><rsub|i>n<rsub|i><dif>\<mu\><rsub|i>=0|(constant
  <math|T> and <math|p>)>

  and

  <equation-cov2|<label|sum(x_i)dmu_i=0><big|sum><rsub|i>x<rsub|i><dif>\<mu\><rsub|i>=0|(constant
  <math|T> and <math|p>)>

  These equations show that the chemical potentials of different species
  cannot be varied independently at constant <math|T> and <math|p>.

  A more general version of the Gibbs\UDuhem equation, without the
  restriction of constant <math|T> and <math|p>, is

  <\equation>
    S<dif>T-V<difp>+<big|sum><rsub|i>n<rsub|i><dif>\<mu\><rsub|i>=0
  </equation>

  This version is derived by comparing the expression for <math|<dif>G> given
  by Eq. <reference|dG=-SdT+Vdp+sum(mu_i*dn_i)> with the differential
  <math|<dif>G=<big|sum><rsub|i>\<mu\><rsub|i><dif>n<rsub|i>+<big|sum><rsub|i>n<rsub|i><dif>\<mu\><rsub|i>>
  obtained from the <index|Additivity rule>additivity rule
  <math|G=<big|sum><rsub|i>\<mu\><rsub|i>*n<rsub|i>>.

  The Gibbs energy is defined by <math|G=H-T*S>. Taking the partial
  derivatives of both sides of this equation with respect to <math|n<rsub|i>>
  at constant <math|T>, <math|p>, and <math|n<rsub|j\<ne\>i>> gives us

  <\equation>
    <Pd|G|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>=<Pd|H|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>-T<Pd|S|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>
  </equation>

  We recognize each partial derivative as a partial molar quantity and
  rewrite the equation as

  <\equation>
    <label|mu_i=H_i-TS_i>\<mu\><rsub|i>=H<rsub|i>-T*S<rsub|i>
  </equation>

  This is analogous to the relation <math|\<mu\>=G/n=H<m>-T*S<m>> for a pure
  substance.

  From the total differential of the Gibbs energy,
  <math|<dif>G=-S*<dif>T+V*<difp>+<big|sum><rsub|i>\<mu\><rsub|i>*<dif>n<rsub|i>>
  (Eq. <reference|dG=-SdT+Vdp+sum(mu_i*dn_i)>), we obtain the following
  reciprocity relations:

  <\equation>
    <Pd|\<mu\><rsub|i>|T|p,<allni>>=-<Pd|S|n<rsub|i>|T,p,n<rsub|j\<ne\>i>><space|2em><Pd|\<mu\><rsub|i>|p|T,<allni>>=<Pd|V|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>
  </equation>

  The symbol <math|<allni>> stands for the set of amounts of all species, and
  subscript <math|<allni>> on a partial derivative means the amount of
  <em|each> species is constant\Vthat is, the derivative is taken at constant
  composition of a closed system. Again we recognize partial derivatives as
  partial molar quantities and rewrite these relations as follows:

  <\equation>
    <label|d(mu_i)/dT=-S_i><Pd|\<mu\><rsub|i>|T|p,<allni>>=-S<rsub|i>
  </equation>

  <\equation>
    <label|d(mu_i)/dp=V_i><Pd|\<mu\><rsub|i>|p|T,<allni>>=V<rsub|i>
  </equation>

  These equations are the equivalent for a mixture of the relations
  <math|<pd|\<mu\>|T|p>=-S<m>> and <math|<pd|\<mu\>|p|T>=V<m>> for a pure
  phase (Eqs. <reference|dmu/dT=-Sm> and <reference|dmu/dp=Vm>).

  Taking the partial derivatives of both sides of <math|U=H-p*V> with respect
  to <math|n<rsub|i>> at constant <math|T>, <math|p>, and
  <math|n<rsub|j\<ne\>i>> gives

  <\equation>
    <label|U_i=H_i-pV_i>U<rsub|i>=H<rsub|i>-p*V<rsub|i>
  </equation>

  Finally, we can obtain a formula for <math|C<rsub|p,i>>, the partial molar
  heat capacity at constant pressure of species <math|i>, by writing the
  total differential of <math|H> in the form

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dif>H>|<cell|=>|<cell|<Pd|H|T|p,<allni>>*<dif>T+<Pd|H|p|T,<allni>>*<difp>+<big|sum><rsub|i><Pd|H|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>*<dif>n<rsub|i>>>|<row|<cell|>|<cell|=>|<cell|C<rsub|p>*<dif>T+<Pd|H|p|T,<allni>>*<difp>+<big|sum><rsub|i>H<rsub|i>*<dif>n<rsub|i><eq-number><label|dH=(Cp)dT+()dp+sum()dn_i>>>>>
  </eqnarray*>

  from which we have the reciprocity relation
  <math|<pd|C<rsub|p>|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>=<pd|H<rsub|i>|T|p,<allni>>>,
  or

  <\equation>
    <label|C_pi=dH_i/dT>C<rsub|p,i>=<Pd|H<rsub|i>|T|p,<allni>>
  </equation>

  <index-complex|<tuple|partial molar|quantity|general relations>||c9
  sec-pmq-relations idx1|<tuple|Partial molar|quantity|general relations>>

  <section|Gas Mixtures><label|c9 sec gm>

  The gas mixtures described in this chapter are assumed to be mixtures of
  nonreacting gaseous substances.

  <subsection|Partial pressure><label|9-partial p><label|c9 sec gm-partial-p>

  The <index|Partial pressure><subindex|Pressure|partial><newterm|partial
  pressure> <math|p<rsub|i>> of substance <math|i> in a gas mixture is
  defined as the product of its mole fraction in the gas phase and the
  pressure of the phase:

  <equation-cov2|<label|p_i=y_i*p>p<rsub|i><defn>y<rsub|i>*p|(gas mixture)>

  The sum of the partial pressures of all substances in a gas mixture is
  <math|<big|sum><rsub|i>p<rsub|i>=<big|sum><rsub|i>y<rsub|i>*p=p*<big|sum><rsub|i>y<rsub|i>>.
  Since the sum of the mole fractions of all substances in a mixture is
  <math|1>, this sum becomes

  <equation-cov2|<label|sum(p_i)=p><big|sum><rsub|i>p<rsub|i>=p|(gas
  mixture)>

  Thus, the sum of the partial pressures equals the pressure of the gas
  phase. This statement is known as <index|Dalton's law><newterm|Dalton's
  Law>. It is valid for any gas mixture, regardless of whether or not the gas
  obeys the ideal gas equation.

  <subsection|The ideal gas mixture><label|c9 sec gm-ideal>

  As discussed in Sec. <reference|3-U of ideal gas>, an <subindex|Ideal
  gas|mixture><subsubindex|Gas|ideal|mixture>ideal gas (whether pure or a
  mixture) is a gas with negligible intermolecular interactions. It obeys the
  ideal gas equation <math|p=n*R*T/V> (where <math|n> in a mixture is the sum
  <math|<big|sum><rsub|i>n<rsub|i>>) and its internal energy in a closed
  system is a function only of temperature. The partial pressure of substance
  <math|i> in an ideal gas mixture is <math|p<rsub|i>=y<rsub|i>*p=y<rsub|i>*n*R*T/V>;
  but <math|y<rsub|i>*n> equals <math|n<rsub|i>>,
  giving<index-complex|<tuple|partial pressure|ideal gas
  mixture>|||<tuple|Partial pressure|in an ideal gas mixture>>

  <equation-cov2|<label|p_i=n_i*RT/V>p<rsub|i>=<frac|n<rsub|i>*R*T|V>|(ideal
  gas mixture)>

  Equation <reference|p_i=n_i*RT/V> is the ideal gas equation with the
  partial pressure of a constituent substance replacing the total pressure,
  and the amount of the substance replacing the total amount. The equation
  shows that the partial pressure of a substance in an ideal gas mixture is
  the pressure the substance by itself, with all others removed from the
  system, would have at the same <math|T> and <math|V> as the mixture. Note
  that this statement is only true for an <em|ideal> gas mixture. The partial
  pressure of a substance in a real gas mixture is in general different from
  the pressure of the pure substance at the same <math|T> and <math|V>,
  because the intermolecular interactions are different.

  <subsection|Partial molar quantities in an ideal gas
  mixture><label|9-partial molar, id gas mixts><label|c9
  sec-gm-ideal-partial-molar>

  We need to relate the chemical potential of a constituent of a gas mixture
  to its partial pressure. We cannot measure the absolute value of a chemical
  potential, but we can evaluate its value relative to the chemical potential
  in a particular reference state called the standard state.

  The <index-complex|<tuple|standard state|gas mixture
  constituent>|||<tuple|Standard state|of a gas mixture
  constituent>><em|standard state of substance> <math|i> <em|in a gas
  mixture> is the same as the standard state of the pure gas described in
  Sec. <reference|7-st states of pure substances>: It is the hypothetical
  state in which pure gaseous <math|i> has the same temperature as the
  mixture, is at the standard pressure <math|p<st>>, and behaves as an ideal
  gas. The <index-complex|<tuple|chemical potential|standard|gas
  constituent>|||<tuple|Chemical potential|standard|of a gas
  constituent>>standard chemical potential <math|\<mu\><rsub|i><st><gas>> of
  gaseous <math|i> is the chemical potential of <math|i> in this gas standard
  state, and is a function of temperature.

  To derive an expression for <math|\<mu\><rsub|i>> in an ideal gas mixture
  relative to <math|\<mu\><rsub|i><st><gas>>, we make an assumption based on
  the following argument. Suppose we place pure A, an ideal gas, in a rigid
  box at pressure <math|p<rprime|'>>. We then slide a rigid membrane into the
  box so as to divide the box into two compartments. The membrane is
  permeable to A; that is, molecules of A pass freely through its pores.
  There is no reason to expect the membrane to affect the pressures on either
  side,<footnote|We assume the gas is not adsorbed to a significant extent on
  the surface of the membrane or in its pores.> which remain equal to
  <math|p<rprime|'>>. Finally, without changing the volume of either
  compartment, we add a second gaseous substance, B, to one side of the
  membrane to form an ideal gas mixture, as shown in Fig.
  <vpageref|fig:9-pure gas \ mixture>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/GAS-MIXT.eps|115pt|58pt||>>
        <label|fig:9-pure gas \ mixture>System with two gas phases, pure A
        and a mixture of A and B, separated by a semipermeable membrane
        through which only A can pass. Both phases are ideal gases at the
        same temperature.
      </big-figure>
    </framed>
  </float> The membrane is impermeable to B, so the molecules of B stay in
  one compartment and cause a pressure increase there. Since the mixture is
  an ideal gas, the molecules of A and B do not interact, and the addition of
  gas B causes no change in the amounts of A on either side of the membrane.
  Thus, the pressure of A in the pure phase and the partial pressure of A in
  the mixture are both equal to <math|p<rprime|'>>.

  Our assumption, then, is that the partial pressure <math|p<A>> of gas A in
  an ideal gas mixture in equilibrium with pure ideal gas A is equal to the
  pressure of the pure gas.

  Because the system shown in Fig. <reference|fig:9-pure gas \ mixture> is in
  an equilibrium state, gas A must have the same chemical potential in both
  phases. This is true even though the phases have different pressures (see
  Sec. <reference|9-eqm conditions>). Since the chemical potential of the
  pure ideal gas is given by <math|\<mu\>=\<mu\><st><gas>+R*T*ln
  <around|(|p/p<st>|)>>, and we assume that <math|p<A>> in the mixture is
  equal to <math|p> in the pure gas, the chemical potential of A in the
  mixture is given by

  <\equation>
    \<mu\><A>=\<mu\><A><st><gas>+R*T*ln <frac|p<A>|p<st>>
  </equation>

  In general, for each substance <math|i> in an ideal gas mixture, we have
  the relation

  <equation-cov2|<label|mu_i=mu_io(g)+RT*ln(p_i/po)>\<mu\><rsub|i>=\<mu\><rsub|i><st><gas>+R*T*ln
  <frac|p<rsub|i>|p<st>>|(ideal gas mixture)>

  where <math|\<mu\><rsub|i><st><gas>> is the chemical potential of <math|i>
  in the gas standard state at the same temperature as the mixture.

  <\quote-env>
    \ Equation <reference|mu_i=mu_io(g)+RT*ln(p_i/po)> shows that if the
    partial pressure of a constituent of an ideal gas mixture is equal to
    <math|p<st>>, so that <math|ln <around|(|p<rsub|i>/p<st>|)>> is zero, the
    chemical potential is equal to the standard chemical potential.
    Conceptually, a standard state should be a well-defined state of the
    system, which in the case of a gas is the <em|pure> ideal gas at
    <math|p=p<st>>. Thus, although a constituent of an ideal gas mixture with
    a partial pressure of <math|1<br>> is not in its standard state, it has
    the same chemical potential as in its standard state.
  </quote-env>

  Equation <reference|mu_i=mu_io(g)+RT*ln(p_i/po)> will be taken as the
  thermodynamic <subindex|Ideal gas|mixture><subsubindex|Gas|ideal|mixture><em|definition>
  of an ideal gas mixture. Any gas mixture in which each constituent <math|i>
  obeys this relation between <math|\<mu\><rsub|i>> and <math|p<rsub|i>> at
  all compositions is by definition an ideal gas mixture. The nonrigorous
  nature of the assumption used to obtain Eq.
  <reference|mu_i=mu_io(g)+RT*ln(p_i/po)> presents no difficulty if we
  consider the equation to be the basic definition.

  By substituting the expression for <math|\<mu\><rsub|i>> into
  <math|<pd|\<mu\><rsub|i>|T|p,<allni>>=-S<rsub|i>> (Eq.
  <reference|d(mu_i)/dT=-S_i>), we obtain an expression for the
  <index-complex|<tuple|partial molar|entropy|ideal gas
  mixture>|||<tuple|Partial molar|entropy|in an ideal gas mixture>>partial
  molar entropy of substance <math|i> in an ideal gas mixture:

  <\eqnarray*>
    <tformat|<table|<row|<cell|S<rsub|i>>|<cell|=>|<cell|-<bPd|\<mu\><rsub|i><st><gas>|T|p,<around*|{|n<rsub|i>|}>>-R*<text|ln>
    <dfrac|p<rsub|i>|p<st>>>>|<row|<cell|>|<cell|=>|<cell|S<rsub|i><st>-R*<text|ln>
    <dfrac|p<rsub|i>|p<st>><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<around*|(|<text|ideal
    gas mixture>|)>>>>>><label|S_i=S_io-R*ln(p_i/po)>>>>>
  </eqnarray*>

  The quantity <math|S<rsub|i><st>=-<bpd|\<mu\><rsub|i><st><gas>|T|p,<allni>>>
  is the <index-complex|<tuple|entropy|standard
  molar|gas>|||<tuple|Entropy|standard molar|of a gas>><newterm|standard
  molar entropy> of constituent <math|i>. It is the molar entropy of <math|i>
  in its standard state of pure ideal gas at pressure <math|p<st>>.

  Substitution of the expression for <math|\<mu\><rsub|i>> from Eq.
  <reference|mu_i=mu_io(g)+RT*ln(p_i/po)> and the expression for
  <math|S<rsub|i>> from Eq. <reference|S_i=S_io-R*ln(p_i/po)> into
  <math|H<rsub|i>=\<mu\><rsub|i>+T*S<rsub|i>> (from Eq.
  <reference|mu_i=H_i-TS_i>) yields <math|H<rsub|i>=\<mu\><rsub|i><st><gas>+T*S<rsub|i><st>>,
  which is equivalent to

  <equation-cov2|<label|H_i=H_io>H<rsub|i>=H<rsub|i><st>|(ideal gas mixture)>

  This tells us that the <index-complex|<tuple|partial molar|enthalpy|ideal
  gas mixture>|||<tuple|Partial molar|enthalpy|in an ideal gas
  mixture>><index-complex|<tuple|enthalpy|partial molar|ideal gas
  mixture>|||<tuple|enthalpy|partial molar|in an ideal gas mixture>>partial
  molar enthalpy of a constituent of an ideal gas mixture at a given
  temperature is <em|independent> of the partial pressure or mixture
  composition; it is a function only of <math|T>.

  From <math|<pd|\<mu\><rsub|i>|p|T,<allni>>=V<rsub|i>> (Eq.
  <reference|d(mu_i)/dp=V_i>), the <index-complex|<tuple|partial
  molar|volume|ideal gas mixture>|||<tuple|Partial molar|volume|in an ideal
  gas mixture>><index-complex|<tuple|volume|partial molar|ideal gas
  mixture>|||<tuple|Volume|partial molar|in an ideal gas mixture>>partial
  molar volume of <math|i> in an ideal gas mixture is given by

  <\equation>
    V<rsub|i>=<bPd|\<mu\><rsub|i><st><gas>|p|T,<allni>>+R*T<bPd|ln
    <around|(|p<rsub|i>/p<st>|)>|p|T,<allni>>
  </equation>

  The first partial derivative on the right is zero because
  <math|\<mu\><rsub|i><st><gas>> is a function only of <math|T>. For the
  second partial derivative, we write <math|p<rsub|i>/p<st>=y<rsub|i>*p/p<st>>.
  The mole fraction <math|y<rsub|i>> is constant when the amount of each
  substance is constant, so we have <math|<bpd|ln
  <around|(|y<rsub|i>*p/p<st>|)>|p|T,<allni>>=1/p>. The partial molar volume
  is therefore given by

  <equation-cov2|<label|V_i=RT/p>V<rsub|i>=<frac|R*T|p>|(ideal gas mixture)>

  which is what we would expect simply from the ideal gas equation. The
  partial molar volume is not necessarily equal to the standard molar volume,
  which is <math|V<rsub|i><st>=R*T/p<st>> for an ideal gas.

  From Eqs. <reference|U_i=H_i-pV_i>, <reference|C_pi=dH_i/dT>,
  <reference|H_i=H_io>, and <reference|V_i=RT/p> we obtain the relations

  <equation-cov2|<label|U_i=U_io>U<rsub|i>=U<rsub|i><st>|(ideal gas mixture)>

  and

  <equation-cov2|<label|C_pi=C_pio>C<rsub|p,i>=C<rsub|p,i><st>|(ideal gas
  mixture)>

  Thus, in an ideal gas mixture the partial molar internal energy and the
  partial molar heat capacity at constant pressure, like the partial molar
  enthalpy, are functions only of <math|T>.<label|Ui,Cpi,Hi in id gas mixt>

  <\quote-env>
    \ The definition of an ideal gas mixture given by Eq.
    <reference|mu_i=mu_io(g)+RT*ln(p_i/po)> is consistent with the criteria
    for an ideal gas listed at the beginning of Sec. <reference|3-U of ideal
    gas>, as the following derivation shows. From Eq. <reference|V_i=RT/p>
    and the <index|Additivity rule>additivity rule, we find the volume is
    given by <math|V=<big|sum><rsub|i>n<rsub|i>*V<rsub|i>=<big|sum><rsub|i>n<rsub|i>*R*T/p=n*R*T/p>,
    which is the ideal gas equation. From Eq. <reference|U_i=U_io> we have
    <math|U=<big|sum><rsub|i>n<rsub|i>*U<rsub|i>=<big|sum><rsub|i>n<rsub|i>*U<rsub|i><st>>,
    showing that <math|U> is a function only of <math|T> in a closed system.
    These properties apply to any gas mixture obeying Eq.
    <reference|mu_i=mu_io(g)+RT*ln(p_i/po)>, and they are the properties that
    define an ideal gas according to Sec. <reference|3-U of ideal gas>.
  </quote-env>

  <subsection|Real gas mixtures><label|9-real gas mixtures><label|c9 sec
  gm-real>

  <subsubsection|Fugacity>

  The fugacity <math|<fug>> of a pure gas is defined by
  <math|\<mu\>=\<mu\><st><gas>+R*T*ln <around|(|<fug>/p<st>|)>> (Eq.
  <vpageref|mu=muo(g)+RT*ln(f/po)>). By analogy with this equation, the
  <index-complex|<tuple|fugacity|gas mixture
  constituent>|||<tuple|Fugacity|of a gas mixture constituent>>fugacity
  <math|<fug><rsub|i>> of substance <math|i> in a real gas <em|mixture> is
  defined by the relation

  <equation-cov2|<label|mu_i=mu_io(g)+RT*(f_i/po)>\<mu\><rsub|i>=\<mu\><rsub|i><st><gas>+R*T*ln
  <frac|<fug><rsub|i>|p<st>><space|1em><text|or><space|1em><fug><rsub|i><defn>p<st>*exp
  <around*|[|<frac|\<mu\><rsub|i>-\<mu\><rsub|i><st><gas>|R*T>|]>|(gas
  mixture)>

  Just as the fugacity of a pure gas is a kind of effective pressure, the
  fugacity of a constituent of a gas mixture is a kind of effective
  <em|partial> pressure. That is, <math|<fug><rsub|i>> is the partial
  pressure substance <math|i> would have in an ideal gas mixture that is at
  the same temperature as the real gas mixture and in which the chemical
  potential of <math|i> is the same as in the real gas mixture.

  To derive a relation allowing us to evaluate <math|<fug><rsub|i>> from the
  pressure\Uvolume properties of the gaseous mixture, we follow the steps
  described for a pure gas in Sec. <reference|7-chem pot and fugacity -
  gases>. The temperature and composition are constant. From Eq.
  <reference|mu_i=mu_io(g)+RT*(f_i/po)>, the difference between the chemical
  potentials of substance <math|i> in the mixture at pressures
  <math|p<rprime|'>> and <math|p<rprime|''>> is

  <\equation>
    \<mu\><rprime|'><rsub|i>-\<mu\><rprime|''><rsub|i>=R*T*ln
    <frac|<fug><rprime|'><rsub|i>|<fug><rprime|''><rsub|i>>
  </equation>

  Integration of <math|<dif>\<mu\><rsub|i>=V<rsub|i><difp>> (from Eq.
  <reference|d(mu_i)/dp=V_i>) between these pressures yields

  <\equation>
    \<mu\><rprime|'><rsub|i>-\<mu\><rprime|''><rsub|i>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>>V<rsub|i>*<difp>
  </equation>

  When we equate these two expressions for
  <math|\<mu\><rprime|'><rsub|i>-\<mu\><rprime|''><rsub|i>>, divide both
  sides by <math|R*T>, subtract the identity

  <\equation>
    ln <frac|p<rprime|'>|p<rprime|''>>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>><frac|<difp>|p>
  </equation>

  and take the ideal-gas behavior limits <math|p<rprime|''>><ra><math|0> and
  <math|<fug><rprime|''><rsub|i>><ra><math|y<rsub|i>*p<rprime|''>=<around|(|p<rprime|'><rsub|i>/p<rprime|'>|)>*p<rprime|''>>,
  we obtain

  <equation-cov2|<label|ln(f_i/p_i)=int(V_i/RT-1/p)dp>ln
  <frac|<fug><rprime|'><rsub|i>|p<rprime|'><rsub|i>>=<big|int><rsub|0><rsup|p<rprime|'>><around*|(|<frac|V<rsub|i>|R*T>-<frac|1|p>|)>*<difp>|(gas
  mixture, constant <math|T>)>

  The <index-complex|<tuple|fugacity coefficient|gas mixture
  constituent>|||<tuple|Fugacity coefficient|of a gas mixture
  constituent>>fugacity coefficient <math|\<phi\><rsub|i>> of constituent
  <math|i> is defined by

  <equation-cov2|<label|f_i=phi_i*p_i><fug><rsub|i><defn>\<phi\><rsub|i>*p<rsub|i>|(gas
  mixture)>

  Accordingly, the fugacity coefficient at pressure <math|p<rprime|'>> is
  given by

  <equation-cov2|<label|ln(phi_i)=int(V_i/RT-1/p)dp>ln
  \<phi\><rsub|i><around|(|p<rprime|'>|)>=<big|int><rsub|0><rsup|p<rprime|'>><around*|(|<frac|V<rsub|i>|R*T>-<frac|1|p>|)>*<difp>|(gas
  mixture, constant <math|T>)>

  As <math|p<rprime|'>> approaches zero, the integral in Eqs.
  <reference|ln(f_i/p_i)=int(V_i/RT-1/p)dp> and
  <reference|ln(phi_i)=int(V_i/RT-1/p)dp> approaches zero,
  <math|<fug><rprime|'><rsub|i>> approaches <math|p<rprime|'><rsub|i>>, and
  <math|\<phi\><rsub|i><around|(|p<rprime|'>|)>> approaches unity.

  <subsubsection|Partial molar quantities>

  By combining Eqs. <reference|mu_i=mu_io(g)+RT*(f_i/po)> and
  <reference|ln(f_i/p_i)=int(V_i/RT-1/p)dp>, we obtain

  <\equation-cov2|<label|mu_i=mu_io+RTln(p_i/po)+int...>\<mu\><rsub|i><around|(|p<rprime|'>|)>=\<mu\><rsub|i><st><gas>+R*T*ln
  <frac|p<rprime|'><rsub|i>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><space|-0.17em><space|-0.17em><around*|(|V<rsub|i>-<frac|R*T|p>|)><difp>>
    (gas mixture,

    constant <math|T>)
  </equation-cov2>

  which is the analogue for a gas mixture of Eq.
  <reference|mu=muo+RT*ln(p/po)+int...> for a pure gas. Section
  <reference|7-st molar fncs of a gas> describes the procedure needed to
  obtain formulas for various molar quantities of a pure gas from Eq.
  <reference|mu=muo+RT*ln(p/po)+int...>. By following a similar procedure
  with Eq. <reference|mu_i=mu_io+RTln(p_i/po)+int...>, we obtain the formulas
  for differences between partial molar and standard molar quantities of a
  constituent of a gas mixture shown in the second column of Table
  <vpageref|tbl:9-gas mixt>.<float|float|thb|<\big-table>
    <\equation*>
      <bktable3|<tformat|||||||||<cwith|2|-1|1|-1|cell-rsep|2fn>|<table|<row|<cell|<text|Difference>>|<cell|<text|General
      expression at pressure <math|p<rprime|'>>>>|<cell|<tabular*|<tformat|<table|<row|<cell|<text|Equation
      of state<note-ref|+pnwft35dEGUGea>>>>|<row|<cell|V=n*R*T/p+n*B>>>>>>>|<row|<cell|\<mu\><rsub|i>-\<mu\><rsub|i><st><gas>>|<cell|R*T*ln
      <frac|p<rprime|'><rsub|i>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><around*|(|V<rsub|i>-<frac|R*T|p>|)>*<difp>>|<cell|R*T*ln
      <frac|p<rsub|i>|p<st>>+B<rprime|'><rsub|i>*p>>|<row|<cell|S<rsub|i>-S<rsub|i><st><gas>>|<cell|-R*ln
      <frac|p<rprime|'><rsub|i>|p<st>>-<big|int><rsub|0><rsup|p<rprime|'>><around*|[|<Pd|V<rsub|i>|T|<space|-0.17em>p>-<frac|R|p>|]>*<difp>>|<cell|-R*ln
      <frac|p<rsub|i>|p<st>>-p*<frac|<dif>B<rprime|'><rsub|i>|<dif>T>>>|<row|<cell|H<rsub|i>-H<rsub|i><st><gas>>|<cell|<big|int><rsub|0><rsup|p<rprime|'>><around*|[|V<rsub|i>-T*<Pd|V<rsub|i>|T|<space|-0.17em>p>|]>*<difp>>|<cell|p*<around*|(|B<rprime|'><rsub|i>-T*<frac|<dif>B<rprime|'><rsub|i>|<dif>T>|)>>>|<row|<cell|U<rsub|i>-U<rsub|i><st><gas>>|<cell|<big|int><rsub|0><rsup|p<rprime|'>><around*|[|V<rsub|i>-T*<Pd|V<rsub|i>|T|<space|-0.17em>p>|]>*<difp>+R*T-p<rprime|'>*V<rsub|i>>|<cell|-p*T*<frac|<dif>B<rprime|'><rsub|i>|<dif>T>>>|<row|<cell|C<rsub|p,i>-C<rsub|p,i><st><gas>>|<cell|-<big|int><rsub|0><rsup|p<rprime|'>>T*<Pd|<rsup|2>V<rsub|i>|T<rsup|2>|<space|-0.17em>p>*<difp>>|<cell|-p*T*<frac|<dif><rsup|2>B<rprime|'><rsub|i>|<dif>T<rsup|2>>>>>>>
    </equation*>

    \;
  <|big-table>
    <label|tbl:9-gas mixt><index-complex|<tuple|partial molar|quantity|gas
    mixture constituent>|||<tuple|Partial molar|of a gas mixture
    constituent>>Gas mixture: expressions for differences between partial
    molar and standard molar quantities of constituent <math|i>

    \;

    <note-inline|<math|B> and <math|B<rprime|'><rsub|i>> are defined by Eqs.
    <reference|B=sum(i)sum(j)y(i)y(j)B(ij)> and <reference|Bi'=2 sum yj
    Bij-B>|+pnwft35dEGUGea>
  </big-table>>

  These formulas are obtained with the help of Eqs.
  <reference|mu_i=H_i-TS_i>, <reference|d(mu_i)/dT=-S_i>,
  <reference|U_i=H_i-pV_i>, and <reference|C_pi=dH_i/dT>.

  <subsubsection|Equation of state>

  The equation of state of a real gas mixture can be written as the
  <index-complex|<tuple|virial|equation|gas
  mixture>|||<tuple|Virial|equation|for a gas mixture>>virial equation

  <\equation>
    <label|pV/n=RT[1+B/(V/n)+...)>p*V/n=R*T*<around*|[|1+<frac|B|<around|(|V/n|)>>+<frac|C|<around|(|V/n|)><rsup|2>>+\<cdots\>|]>
  </equation>

  This equation is the same as Eq. <reference|pVm=RT(1+B/Vm...)> for a pure
  gas, except that the molar volume <math|V<m>> is replaced by the mean molar
  volume <math|V/n>, and the virial coefficients <math|B,C,\<ldots\>> depend
  on composition as well as temperature.

  At low to moderate pressures, the simple equation of state
  <index-complex|<tuple|equation of state|gas at low
  pressure>|||<tuple|Equation of state|of gas at low pressure>>

  <\equation>
    <label|V=nRT/p+nB>V/n=<frac|R*T|p>+B
  </equation>

  describes a gas mixture to a sufficiently high degree of accuracy (see Eq.
  <vpageref|Vm=RT/p+B>). This is equivalent to a compression factor given by

  <\equation>
    <label|Z=(1+Bp/RT)>Z<defn><frac|p*V|n*R*T>=1+<frac|B*p|R*T>
  </equation>

  From <subindex|Statistical mechanics|second virial coefficient>statistical
  mechanical theory, the dependence of the second virial coefficient <math|B>
  of a binary gas mixture on the mole fraction composition is given by

  <equation-cov2|<label|B=yA^2 B(AA)+...>B=y<A><rsup|2>*B<rsub|<text|A>\<nocomma\><text|A>>+2*y<A>*y<B>*B<rsub|<text|A>\<nocomma\><text|B>>+y<B><rsup|2>*B<rsub|<text|B>\<nocomma\><text|B>>|(binary
  gas mixture)>

  where <math|B<rsub|<text|A>\<nocomma\><text|A>>> and
  <math|B<rsub|<text|B>\<nocomma\><text|B>>> are the second virial
  coefficients of pure A and B, and <math|B<rsub|<text|A>\<nocomma\><text|B>>>
  is a mixed second virial coefficient. <math|B<rsub|<text|A>\<nocomma\><text|A>>>,
  <math|B<rsub|<text|B>\<nocomma\><text|B>>>, and
  <math|B<rsub|<text|A>\<nocomma\><text|B>>> are functions of <math|T> only.
  For a gas mixture with any number of constituents, the composition
  dependence of <math|B> is given by

  <equation-cov2|<label|B=sum(i)sum(j)y(i)y(j)B(ij)>B=<big|sum><rsub|i><big|sum><rsub|j>y<rsub|i>*y<rsub|j>*B<rsub|i\<nocomma\>j>|(gas
  mixture, <math|B<rsub|i\<nocomma\>j>=B<rsub|j\<nocomma\>i>>>

  Here <math|B<rsub|i*j>> is the second virial of <math|i> if <math|i> and
  <math|j> are the same, or a mixed second virial coefficient if <math|i> and
  <math|j> are different.

  If a gas mixture obeys the <index-complex|<tuple|equation of state|gas at
  low pressure>|||<tuple|Equation of state|of a gas at low pressure>>equation
  of state of Eq. <reference|V=nRT/p+nB>, the partial molar volume of
  constituent <math|i> is given by

  <\equation>
    <label|Vi=RT/p+Bi'>V<rsub|i>=<frac|R*T|p>+B<rprime|'><rsub|i>
  </equation>

  where the quantity <math|B<rprime|'><rsub|i>>, in order to be consistent
  with <math|V<rsub|i>=<pd|V|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>>, is found to be
  given by

  <\equation>
    <label|Bi'=2 sum yj Bij-B>B<rprime|'><rsub|i>=2*<big|sum><rsub|j>y<rsub|j>*B<rsub|i\<nocomma\>j>-B
  </equation>

  For the constituents of a binary mixture of A and B, Eq. <reference|Bi'=2
  sum yj Bij-B> becomes

  <equation-cov2|<label|B(A)'=>B<A><rprime|'>=B<rsub|<text|A>\<nocomma\><text|A>>+<around|(|-B<rsub|<text|A>\<nocomma\><text|A>>+2*B<rsub|<text|A>\<nocomma\><text|B>>-B<rsub|<text|B>\<nocomma\><text|B>>|)>*y<B><rsup|2>|(binary
  gas mixture)>

  <equation-cov2|<label|B(B)'=>B<B><rprime|'>=B<rsub|<text|B>\<nocomma\><text|B>>+<around|(|-B<rsub|<text|A>\<nocomma\><text|A>>+2*B<rsub|<text|A>\<nocomma\><text|B>>-B<rsub|<text|B>\<nocomma\><text|B>>|)>*y<A><rsup|2>|(binary
  gas mixture)>

  When we substitute the expression of Eq. <reference|Vi=RT/p+Bi'> for
  <math|V<rsub|i>> in Eq. <reference|ln(phi_i)=int(V_i/RT-1/p)dp>, we obtain
  a relation between the <index-complex|<tuple|fugacity coefficient|gas
  mixture constituent>|||<tuple|Fugacity coefficient|of a gas mixture
  constituent>>fugacity coefficient of constituent <math|i> and the function
  <math|B<rprime|'><rsub|i>>:

  <\equation>
    <label|ln(phi_i)=Bi'p/RT>ln \<phi\><rsub|i>=<frac|B<rprime|'><rsub|i>*p|R*T>
  </equation>

  The third column of Table <reference|tbl:9-gas mixt> gives formulas for
  various partial molar quantities of constituent <math|i> in terms of
  <math|B<rprime|'><rsub|i>> and its temperature derivative. The formulas are
  the same as the approximate formulas in the third column of Table
  <reference|tbl:7-gas standard molar> for molar quantities of a <em|pure>
  gas, with <math|B<rprime|'><rsub|i>> replacing the second virial
  coefficient <math|B>.

  <section|Liquid and Solid Mixtures of Nonelectrolytes><label|c9 sec lsmn>

  Homogeneous liquid and solid mixtures are condensed phases of variable
  composition. Most of the discussion of condensed-phase mixtures in this
  section focuses on liquids. The same principles, however, apply to
  homogeneous solid mixtures, often called <subindex|Solution|solid>solid
  solutions. These solid mixtures include most metal alloys, many gemstones,
  and doped semiconductors.

  The relations derived in this section apply to mixtures of
  nonelectrolytes\Vsubstances that do not dissociate into charged species.
  Solutions of electrolytes behave quite differently in many ways, and will
  be discussed in the next chapter.

  <subsection|Raoult's law><label|9-Raoult's law><label|c9 sec lsmn-raoult>

  In 1888, the French physical chemist <index|Raoult, Fran�ois>Fran�ois
  Raoult published his finding that when a dilute liquid solution of a
  volatile solvent and a nonelectrolyte solute is equilibrated with a gas
  phase, the partial pressure <math|p<A>> of the solvent in the gas phase is
  proportional to the mole fraction <math|x<A>> of the solvent in the
  solution:

  <\equation>
    <label|pA=xA pA*>p<A>=x<A>*p<A><rsup|\<ast\>>
  </equation>

  Here <math|p<A><rsup|\<ast\>>> is the saturation vapor pressure of the pure
  solvent (the pressure at which the pure liquid and pure gas phases are in
  equilibrium).

  In order to place Raoult's law in a rigorous thermodynamic framework,
  consider the two systems depicted in Fig.
  <vpageref|fig:9-liquid+gas>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/LIQ-GAS.eps|200pt|122pt||>>
        <label|fig:9-liquid+gas>Two systems with equilibrated liquid and gas
        phases.
      </big-figure>
    </framed>
  </float> The liquid phase of system 1 is a binary solution of solvent A and
  solute B, whereas the liquid in system 2 is the pure solvent. In system 1,
  the partial pressure <math|p<A>> in the equilibrated gas phase depends on
  the temperature and the solution composition. In system 2,
  <math|p<A><rsup|\<ast\>>> depends on the temperature. Both <math|p<A>> and
  <math|p<A><rsup|\<ast\>>> have a mild dependence on the total pressure
  <math|p>, which can be varied with an inert gas constituent C of negligible
  solubility in the liquid.

  Suppose that we vary the composition of the solution in system 1 at
  constant temperature, while adjusting the partial pressure of C so as to
  keep <math|p> constant. If we find that the partial pressure of the solvent
  over a range of composition is given by
  <math|p<A>=x<A>*p<A><rsup|\<ast\>>>, where <math|p<A><rsup|\<ast\>>> is the
  partial pressure of A in system 2 at the same <math|T> and <math|p>, we
  will say that the solvent obeys <index-complex|<tuple|raoult's
  law|partial>|||<tuple|Raoult's law|for partial pressure>><em|Raoult's law
  for partial pressure> in this range. This is the same as the original
  Raoult's law, except that <math|p<A><rsup|\<ast\>>> is now the vapor
  pressure of pure liquid A at the pressure <math|p> of the liquid mixture.
  Section <reference|12-effect of p on fug> will show that unless <math|p> is
  much greater than <math|p<A><rsup|\<ast\>>>, <math|p<A><rsup|\<ast\>>> is
  practically the same as the saturation vapor pressure of pure liquid A, in
  which case Raoult's law for partial pressure becomes identical to the
  original law.

  A form of Raoult's law with fugacities in place of partial pressures is
  often more useful: <math|<fug><A>=x<A>*<fug><A><rsup|\<ast\>>>, where
  <math|<fug><A><rsup|\<ast\>>> is the fugacity of A in the gas phase of
  system 2 at the same <math|T> and <math|p> as the solution. If this
  relation is found to hold over a given composition range, we will say the
  solvent in this range obeys <index-complex|<tuple|raoult's
  law|fugacity>|||<tuple|Raoult's law|for fugacity>><em|Raoult's law for
  fugacity>.

  We can generalize the two forms of Raoult's law for any constituent
  <math|i> of a liquid mixture:

  <index-complex|<tuple|raoult's law|partial>|||<tuple|Raoult's law|for
  partial pressure>>

  <equation-cov2|<label|p_i=(x_i)(p_i*)>p<rsub|i>=x<rsub|i>*p<rsub|i><rsup|\<ast\>>|(Raoult's
  law for partial pressure)>

  <index-complex|<tuple|raoult's law|fugacity>|||<tuple|Raoult's law|for
  fugacity>>

  <equation-cov2|<label|f_i=(x_i)(f_i*)><fug><rsub|i>=x<rsub|i><fug><rsub|i><rsup|\<ast\>>|(Raoult's
  law for fugacity)>

  Here <math|x<rsub|i>> is the mole fraction of <math|i> in the liquid
  mixture, and <math|p<rsub|i><rsup|\<ast\>>> and
  <math|<fug><rsub|i><rsup|\<ast\>>> are the partial pressure and fugacity in
  a gas phase equilibrated with pure liquid <math|i> at the same <math|T> and
  <math|p> as the liquid mixture. Both <math|p<A><rsup|\<ast\>>> and
  <math|<fug><rsub|i><rsup|\<ast\>>> are functions of <math|T> and <math|p>.

  These two forms of Raoult's law are equivalent when the gas phases are
  ideal gas mixtures. When it is necessary to make a distinction between the
  two forms, this book will refer specifically to Raoult's law for partial
  pressure or Raoult's law for fugacity.

  Raoult's law for fugacity can be recast in terms of chemical potential.
  Section <reference|9-eqm conditions> showed that if substance <math|i> has
  transfer equilibrium between a liquid and a gas phase, its chemical
  potential <math|\<mu\><rsub|i>> is the same in both equilibrated phases.
  The chemical potential in the gas phase is given by
  <math|\<mu\><rsub|i>=\<mu\><rsub|i><st><gas>+R*T*ln <fug><rsub|i>/p<st>>
  (Eq. <reference|mu_i=mu_io(g)+RT*(f_i/po)>). Replacing <math|<fug><rsub|i>>
  by <math|x<rsub|i>*<fug><rsub|i><rsup|\<ast\>>> according to Raoult's law,
  and rearranging, we obtain

  <\equation>
    \<mu\><rsub|i>=<around*|[|\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|<fug><rsub|i><rsup|\<ast\>>|p<st>>|]>+R*T*ln x<rsub|i>
  </equation>

  The expression in brackets is independent of the mixture composition. We
  replace this expression by a quantity <math|\<mu\><rsub|i><rsup|\<ast\>>>,
  a function of <math|T> and <math|p>, and write

  <\equation>
    <label|mu_i=(mu_i*)+RT*ln(x_i)>\<mu\><rsub|i>=\<mu\><rsub|i><rsup|\<ast\>>+R*T*ln
    x<rsub|i>
  </equation>

  Equation <reference|mu_i=(mu_i*)+RT*ln(x_i)> is an expression for the
  chemical potential in the liquid phase when Raoult's law for fugacity is
  obeyed. By setting <math|x<rsub|i>> equal to <math|1>, we see that
  <math|\<mu\><rsub|i><rsup|\<ast\>>> represents the chemical potential of
  pure liquid <math|i> at the temperature and pressure of the mixture.
  Because Eq. <reference|mu_i=(mu_i*)+RT*ln(x_i)> is valid for any
  constituent whose fugacity obeys Eq. <reference|f_i=(x_i)(f_i*)>, it is
  equivalent to <index-complex|<tuple|raoult's
  law|fugacity>|||<tuple|Raoult's law|for fugacity>>Raoult's law for fugacity
  for that constituent.

  <subsection|Ideal mixtures><label|9-ideal mixtures><label|c9 sec
  lsmn-ideal>

  Depending on the temperature, pressure, and identity of the constituents of
  a liquid mixture, Raoult's law for fugacity may hold for constituent
  <math|i> at all liquid compositions, or over only a limited composition
  range when <math|x<rsub|i>> is close to unity.

  An <index-complex|<tuple|ideal mixture|raoult's law>|||<tuple|Ideal
  mixture|and Raoult's law>><index-complex|<tuple|mixture|ideal|raoult's
  law>|||<tuple|Mixture|ideal|and Raoult's law>><em|ideal liquid mixture> is
  defined as a liquid mixture in which, at a given temperature and pressure,
  <em|each> constituent obeys Raoult's law for fugacity (Eq.
  <reference|f_i=(x_i)(f_i*)> or <reference|mu_i=(mu_i*)+RT*ln(x_i)>) over
  the entire range of composition. Equation <reference|f_i=(x_i)(f_i*)>
  applies only to a volatile constituent, whereas Eq.
  <reference|mu_i=(mu_i*)+RT*ln(x_i)> applies regardless of whether the
  constituent is volatile.

  Few liquid mixtures are found to approximate the behavior of an ideal
  liquid mixture. In order to do so, the constituents must have similar
  molecular size and structure, and the pure liquids must be miscible in all
  proportions. Benzene and toluene, for instance, satisfy these requirements,
  and liquid mixtures of benzene and toluene are found to obey Raoult's law
  quite closely. In contrast, water and methanol, although miscible in all
  proportions, form liquid mixtures that deviate considerably from Raoult's
  law. The most commonly encountered situation for mixtures of organic
  liquids is that each constituent deviates from Raoult's law behavior by
  having a <em|higher> fugacity than predicted by Eq.
  <reference|f_i=(x_i)(f_i*)>\Va <em|positive> deviation from Raoult's law.

  Similar statements apply to ideal <em|solid> mixtures. In addition, a
  relation with the same form as Eq. <reference|mu_i=(mu_i*)+RT*ln(x_i)>
  describes the chemical potential of each constituent of an
  <index-complex|<tuple|ideal gas|mixture|raoult's law>|||<tuple|Ideal
  gas|mixture|and Raoult's law>>ideal <em|gas> mixture, as the following
  derivation shows. In an ideal gas mixture at a given <math|T> and <math|p>,
  the chemical potential of substance <math|i> is given by Eq.
  <reference|mu_i=mu_io(g)+RT*ln(p_i/po)>:

  <\equation>
    \<mu\><rsub|i>=\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|p<rsub|i>|p<st>>=\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|y<rsub|i>*p|p<st>>
  </equation>

  Here <math|y<rsub|i>> is the mole fraction of <math|i> in the gas. The
  chemical potential of the pure ideal gas (<math|y<rsub|i>=1>) is

  <\equation>
    \<mu\><rsub|i><rsup|\<ast\>>=\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|p|p<st>>
  </equation>

  By eliminating <math|\<mu\><rsub|i><st><gas>> between these equations and
  rearranging, we obtain Eq. <reference|mu_i=(mu_i*)+RT*ln(x_i)> with
  <math|x<rsub|i>> replaced by <math|y<rsub|i>>.

  Thus, an <index|Ideal mixture><index-complex|<tuple|mixture|ideal|chemical
  potential>|||<tuple|Mixture|ideal|and chemical potential>><newterm|ideal
  mixture>, whether solid, liquid, or gas, is a mixture in which the chemical
  potential of each constituent at a given <math|T> and <math|p> is a linear
  function of the logarithm of the mole fraction:

  <equation-cov2|<label|ideal mixture>\<mu\><rsub|i>=\<mu\><rsub|i><rsup|\<ast\>>+R*T*ln
  x<rsub|i>|(ideal mixture)>

  <subsection|Partial molar quantities in ideal mixtures><label|9-partial
  molar, id mixts><label|c9 sec-lsmn-partial-molar>

  <index-complex|<tuple|partial molar|quantity|ideal mixture>||c9
  sec-lsmn-partial-molar idx1|<tuple|Partial molar|quantity|in an ideal
  mixture>>With the help of Eq. <reference|ideal mixture> for the chemical
  potential of a constituent of an ideal mixture, we will now be able to find
  expressions for partial molar quantities. These expressions find their
  greatest use for ideal liquid and solid mixtures.

  For the <subindex|Partial molar|entropy><subindex|Entropy|partial
  molar>partial molar entropy of substance <math|i>, we have
  <math|S<rsub|i>=-<pd|\<mu\><rsub|i>|T|p,<allni>>> (from Eq.
  <reference|d(mu_i)/dT=-S_i>) or, for the ideal mixture,

  <equation-cov2|<label|S_i=S_i^*-R*ln x_i>S<rsub|i>=-<Pd|\<mu\><rsub|i><rsup|\<ast\>>|T|<space|-0.17em>p>-R*ln
  x<rsub|i>=S<rsub|i><rsup|\<ast\>>-R*ln x<rsub|i>|(ideal mixture)>

  Since <math|ln x<rsub|i>> is negative in a mixture, the partial molar
  entropy of a constituent of an ideal mixture is greater than the molar
  entropy of the pure substance at the same <math|T> and <math|p>.

  For the <subindex|Partial molar|enthalpy><subindex|Enthalpy|partial
  molar>partial molar enthalpy, we have <math|H<rsub|i>=\<mu\><rsub|i>+T*S<rsub|i>>
  (from Eq. <reference|mu_i=H_i-TS_i>). Using the expressions for
  <math|\<mu\><rsub|i>> and <math|S<rsub|i>> gives us

  <equation-cov2|<label|H_i=H_i^*>H<rsub|i>=\<mu\><rsub|i><rsup|\<ast\>>+T*S<rsub|i><rsup|\<ast\>>=H<rsub|i><rsup|\<ast\>>|(ideal
  mixture)>

  Thus, <math|H<rsub|i>> in an ideal mixture is independent of the mixture
  composition and is equal to the molar enthalpy of pure <math|i> at the same
  <math|T> and <math|p> as the mixture. In the case of an ideal <em|gas>
  mixture, <math|H<rsub|i>> is also independent of <math|p>, because the
  molar enthalpy of an ideal gas depends only on <math|T>.

  The <subindex|Partial molar|volume><subindex|Volume|partial molar>partial
  molar volume is given by <math|V<rsub|i>=<pd|\<mu\><rsub|i>|p|T,<allni>>>
  (Eq. <reference|d(mu_i)/dp=V_i>), so we have

  <equation-cov2|<label|V_i=V_i^*>V<rsub|i>=<Pd|\<mu\><rsub|i><rsup|\<ast\>>|p|T>=V<rsub|i><rsup|\<ast\>>|(ideal
  mixture)>

  Finally, from Eqs. <reference|U_i=H_i-pV_i> and <reference|C_pi=dH_i/dT>
  and the expressions above for <math|H<rsub|i>> and <math|V<rsub|i>>, we
  obtain

  <subindex|Partial molar|internal energy><subindex|Internal energy|partial
  molar>

  <equation-cov2|<label|U_i=U_i^*>U<rsub|i>=H<rsub|i><rsup|\<ast\>>-p*V<rsub|i><rsup|\<ast\>>=U<rsub|i><rsup|\<ast\>>|(ideal
  mixture)>

  and

  <subindex|Partial molar|heat capacity at constant
  pressure><index-complex|<tuple|heat capacity|constant pressure|partial
  molar>|||<tuple|Heat capacity|at constant pressure|partial molar>>

  <equation-cov2|<label|Cpi=Cpi^*>C<rsub|p,i>=<pd|H<rsub|i><rsup|\<ast\>>|T|p,<allni>>=C<rsub|p,i><rsup|\<ast\>>|(ideal
  mixture)>

  Note that in an ideal mixture held at constant <math|T> and <math|p>, the
  partial molar quantities <math|H<rsub|i>>, <math|V<rsub|i>>,
  <math|U<rsub|i>>, and <math|C<rsub|p,i>> do not vary with the
  composition.<index-complex|<tuple|partial molar|quantity|ideal mixture>||c9
  sec-lsmn-partial-molar idx1|<tuple|Partial molar|quantity|in an ideal
  mixture>>

  \;

  <subsection|Henry's law><label|9-Henry's law><label|c9 sec lsmn-henry>

  Consider the system shown in Fig. <vpageref|fig:9-i in liquid
  \ gas>,<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/LIQGAS-P.eps|87pt|58pt||>>
        <label|fig:9-i in liquid \ gas>Equilibrated liquid and gas mixtures.
        Substance <math|i> is present in both phases.
      </big-figure>
    </framed>
  </float> in which a liquid mixture is equilibrated with a gas phase.
  Transfer equilibrium exists for substance <math|i>, a constituent of both
  phases. Substance <math|i> is assumed to have the same molecular form in
  both phases, and is not, for instance, an electrolyte. We can vary the mole
  fraction <math|x<rsub|i>> in the liquid and evaluate the fugacity
  <math|<fug><rsub|i>> in the gas phase.

  Suppose we allow <math|x<rsub|i>> to approach zero at constant <math|T> and
  <math|p> while the relative amounts of the other liquid constituents remain
  constant. It is found experimentally that the fugacity <math|<fug><rsub|i>>
  becomes proportional to <math|x<rsub|i>>:

  <equation-cov2|<fug><rsub|i><ra><kHi><space|0.17em>x<rsub|i><space|1em><text|as><space|1em>x<rsub|i><ra>0|(constant
  <math|T> and <math|p>)>

  This behavior is called <index|Henry's law><newterm|Henry's law>. The
  proportionality constant <math|<kHi>> is the <index|Henry's law
  constant><newterm|Henry's law constant> of substance <math|i>. The value of
  <math|<kHi>> depends on the temperature and the total pressure, and also on
  the relative amounts of the constituents other than <math|i> in the liquid
  mixture.

  If the liquid phase happens to be an ideal liquid mixture, then by
  definition constituent <math|i> obeys Raoult's law for fugacity at all
  values of <math|x<rsub|i>>. In that case, <math|<kHi>> is equal to
  <math|<fug><rsub|i><rsup|\<ast\>>>, the fugacity when the gas phase is
  equilibrated with pure liquid <math|i> at the same temperature and pressure
  as the liquid mixture.

  If we treat the liquid mixture as a binary solution in which solute B is a
  volatile nonelectrolyte, Henry's law behavior occurs in the limit of
  infinite dilution:

  <equation-cov2|<label|fB-\<gtr\>k xB><fug><B><ra><kHB><space|0.17em>x<B><space|1em><text|as><space|1em>x<B><ra>0|(constant
  <math|T> and <math|p>)>

  An example of this behavior is shown in Fig. <reference|fig:9-fugacity vs
  xB>(a) on page <pageref|fig:9-fugacity vs xB>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/FUG-XB.eps|262pt|170pt||>>
        <label|fig:9-fugacity vs xB>Liquid solutions of 2,3-dimethylbutane
        (B) in cyclooctane at <math|298.15<K>> and
        <math|1<br>>.<note-ref|+pnwft35dEGUGeb>

        <\enumerate-alpha>
          <item>Fugacity of B in an equilibrated gas phase as a function of
          solution composition. The dashed line, tangent to the curve at
          <math|x<B>=0>, is Henry's law behavior, and its slope is
          <math|<kHB>>.

          <item>Fugacity divided by mole fraction as a function of
          composition; the limiting value at <math|x<B>=0> is the Henry's law
          constant <math|<kHB>>.
        </enumerate-alpha>

        \;

        <note-inline||+pnwft35dEGUGeb>Based on data in Ref. <cite|marsh-74>.
      </big-figure>
    </framed>
  </float> The limiting slope of the plot of <math|f<B>> versus <math|x<B>>
  is finite, not zero or infinite. (The fugacity of a volatile
  <em|electrolyte>, such as HCl dissolved in water, displays a much different
  behavior, as will be shown in Chap. <reference|10-electrolyte solutions>.)

  <\quote-env>
    \ Equation <reference|fB-\<gtr\>k xB> can be applied to a solution of
    more than one solute if the combination of constituents other than B is
    treated as the solvent, and the relative amounts of these constituents
    remain constant as <math|x<B>> is varied.
  </quote-env>

  Since the mole fraction, concentration, and molality of a solute become
  proportional to one another in the limit of infinite dilution (Eq.
  <reference|nB/nA (dilute)>), in a very dilute solution the fugacity is
  proportional to all three of these composition variables. This leads to
  three versions of Henry's law:

  <\eqnarray*>
    <tformat|<cwith|1|-1|2|2|cell-halign|c>|<table|<row|<cell|<htab|5mm><text|<em|mole
    fraction basis>><htab|5mm><fug><B>>|<cell|=>|<cell|<kHB>*x<B><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<dilate|0.75|0.75|<tformat|<cwith|1|2|1|1|cell-halign|r>|<table|<row|<cell|<text|(nonelectrolyte
    solute>>>|<row|<cell|<text|at infinite
    dilution)>>>>>>>>>>><label|fB=KxB*xB>>>|<row|<cell|<htab|5mm><text|<em|concentration
    basis>><htab|5mm><fug><B>>|<cell|=>|<cell|k<cbB>*c<B><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<dilate|0.75|0.75|<tformat|<cwith|1|2|1|1|cell-halign|r>|<table|<row|<cell|<text|(nonelectrolyte
    solute>>>|<row|<cell|<text|at infinite
    dilution)>>>>>>>>>>><label|fB=KcB*cB>>>|<row|<cell|<htab|5mm><text|<em|molality
    basis>><htab|5mm><fug><B>>|<cell|=>|<cell|k<mbB>*m<B><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<dilate|0.75|0.75|<tformat|<cwith|1|2|1|1|cell-halign|r>|<table|<row|<cell|<text|(nonelectrolyte
    solute>>>|<row|<cell|<text|at infinite
    dilution)>>>>>>>>>>><label|fB=KmB*mB>>>>>
  </eqnarray*>

  In these equations <math|<kHB>>, <math|k<cbB>>, and <math|k<mbB>> are
  Henry's law constants defined by

  <\eqnarray*>
    <tformat|<table|<row|<cell|<htab|5mm><text|<em|mole fraction
    basis>><htab|5mm><kHB>>|<cell|<defn>>|<cell|lim<rsub|x<B><ra>0>
    <around*|(|<frac|<fug><B>|x<B>>|)><eq-number><label|KxB=lim(fB/xB)>>>|<row|<cell|<htab|5mm><text|<em|concentration
    basis>><htab|5mm>k<cbB>>|<cell|<defn>>|<cell|lim<rsub|c<B><ra>0>
    <around*|(|<frac|<fug><B>|c<B>>|)><eq-number><label|KcB=lim(fB/cB>>>|<row|<cell|<htab|5mm><text|<em|molality
    basis>><htab|5mm>k<mbB>>|<cell|<defn>>|<cell|lim<rsub|m<B><ra>0>
    <around*|(|<frac|<fug><B>|m<B>>|)><eq-number><label|KmB=lim(fB/mB>>>>>
  </eqnarray*>

  Note that the Henry's law constants are not dimensionless, and are
  functions of <math|T> and <math|p>. To <subindex|Henry's law
  constant|evaluation of>evaluate one of these constants, we can plot
  <math|<fug><B>> divided by the appropriate composition variable as a
  function of the composition variable and extrapolate to infinite dilution.
  The evaluation of <math|<kHB>> by this procedure is illustrated in Fig.
  <reference|fig:9-fugacity vs xB>(b).

  Relations <index|Henry's law constants, relations between different>between
  these Henry's law constants can be found with the use of Eqs.
  <reference|nB/nA (dilute)> and <reference|fB=KxB*xB>\U<reference|fB=KmB*mB>:

  <\equation>
    <label|kcB=,kmB=>k<cbB>=V<A><rsup|\<ast\>>*<kHB><space|2em>k<mbB>=M<A>*<kHB>
  </equation>

  <subsection|The ideal-dilute solution><label|9-ideal-dilute soln><label|c9
  sec lsmn-ideal-dilute>

  An <index|Ideal-dilute solution><subindex|Solution|ideal-dilute><newterm|ideal-dilute
  solution> is a real solution that is dilute enough for each solute to obey
  Henry's law. On the microscopic level, the requirement is that solute
  molecules be sufficiently separated to make solute\Usolute interactions
  negligible.

  Note that an ideal-dilute solution is not necessarily an ideal mixture. Few
  liquid mixtures behave as ideal mixtures, but a solution of any
  nonelectrolyte solute becomes an ideal-dilute solution when sufficiently
  dilute.

  Within the composition range that a solution effectively behaves as an
  ideal-dilute solution, then, the fugacity of solute B in a gas phase
  equilibrated with the solution is proportional to its mole fraction
  <math|x<B>> in the solution. The chemical potential of B in the gas phase,
  which is equal to that of B in the liquid, is related to the fugacity by
  <math|\<mu\><B>=\<mu\><B><st><gas>+R*T*ln <around|(|<fug><B>/p<st>|)>> (Eq.
  <reference|mu_i=mu_io(g)+RT*(f_i/po)>). Substituting
  <math|<fug><B>=<kHB><space|0.17em>x<B>> (Henry's law) into this equation,
  we obtain

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<mu\><B>>|<cell|=>|<cell|\<mu\><B><st><gas>+R*T*ln
    <frac|<kHB>*x<B>|p<st>>>>|<row|<cell|>|<cell|=>|<cell|<around*|[|\<mu\><B><st><gas>+R*T*ln
    <frac|<kHB>|p<st>>|]>+R*T*ln x<B><eq-number><label|muB=[]+RTln(xB)>>>>>
  </eqnarray*>

  where the composition variable <math|x<B>> is segregated in the last term
  on the right side.

  The expression in brackets in Eq. <reference|muB=[]+RTln(xB)> is a function
  of <math|T> and <math|p>, but not of <math|x<B>>, and represents the
  chemical potential of B in a hypothetical <index-complex|<tuple|reference
  state|solute>||c9 sec lsmn-ideal-dilute idx1|<tuple|Reference state|of a
  solute>><index-complex|<tuple|solute|reference state>||c9 sec
  lsmn-ideal-dilute idx2|<tuple|Solute|reference state>>solute reference
  state. This chemical potential will be denoted by <math|\<mu\><xbB><rf>>,
  where the <math|x> in the subscript reminds us that the reference state is
  based on mole fraction. The equation then becomes

  <\equation-cov2|<label|muB=mu(xB)(ref)+RT*ln(xB)>\<mu\><B><around|(|T,p|)>=\<mu\><xbB><rf><around|(|T,p|)>+R*T*ln
  x<B>>
    (ideal\Udilute solution,

    of an electrolyte)
  </equation-cov2>

  Here the notation emphasizes the fact that <math|\<mu\><B>> and
  <math|\<mu\><xbB><rf>> are functions of <math|T> and <math|p>.

  <\quote-env>
    \ Equation <reference|muB=mu(xB)(ref)+RT*ln(xB)>, derived using fugacity,
    is valid even if the solute has such low volatility that its fugacity in
    an equilibrated gas phase is too low to measure. In principle, no solute
    is completely nonvolatile, and there is always a finite solute fugacity
    in the gas phase even if immeasurably small.

    It is worthwhile to describe in detail the reference state to which
    <math|\<mu\><xbB><rf>> refers. The general concept is also applicable to
    other solute reference states and solute standard states to be
    encountered presently. Imagine a hypothetical solution with the same
    constituents as the real solution. This hypothetical solution has the
    magical property that it continues to exhibit the ideal-dilute behavior
    described by Eq. <reference|muB=mu(xB)(ref)+RT*ln(xB)>, even when
    <math|x<B>> increases beyond the ideal-dilute range of the real solution.
    The reference state is the state of this hypothetical solution at
    <math|x<B|=>1>. It is a fictitious state in which the mole fraction of B
    is unity and B behaves as in an ideal-dilute solution, and is sometimes
    called the <em|ideal-dilute solution of unit solute mole fraction>.

    By setting <math|x<B>> equal to unity in Eq.
    <reference|muB=mu(xB)(ref)+RT*ln(xB)>, so that <math|ln x<B>> is zero, we
    see that <math|\<mu\><xbB><rf>> is the chemical potential of B in the
    reference state. In a gas phase equilibrated with the hypothetical
    solution, the solute fugacity <math|<fug><B>> increases as a linear
    function of <math|x<B>> all the way to <math|x<B|=>1>, unlike the
    behavior of the real solution (unless it happens to be an ideal mixture).
    In the reference state, <math|<fug><B>> is equal to the Henry's law
    constant <math|<kHB>>; an example is indicated by the filled circle in
    Fig. <reference|fig:9-fugacity vs xB>(a).
  </quote-env>

  By similar steps, combining Henry's law based on concentration or molality
  (Eqs. <reference|fB=KcB*cB> and <reference|fB=KmB*mB>) with the relation
  <math|\<mu\><B>=\<mu\><B><st><gas>+R*T*ln <around|(|<fug><B>/p<st>|)>>, we
  obtain for the solute chemical potential in the ideal-dilute range the
  equations

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<mu\><B>>|<cell|=>|<cell|\<mu\><B><st><gas>+R*T*ln
    <around*|(|<frac|k<cbB>*c<B>|p<st>>\<cdot\><frac|c<st>|c<st>>|)>>>|<row|<cell|>|<cell|=>|<cell|<around*|[|\<mu\><B><st><gas>+R*T*ln
    <frac|k<cbB>*c<st>|p<st>>|]>+R*T*ln <frac|c<B>|c<st>><eq-number>>>|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|\<mu\><B>>|<cell|=>|<cell|\<mu\><B><st><gas>+R*T*ln
    <around*|(|<frac|k<mbB>*m<B>|p<st>>\<cdot\><frac|m<st>|m<st>>|)>>>|<row|<cell|>|<cell|=>|<cell|<around*|[|\<mu\><B><st><gas>+R*T*ln
    <frac|k<mbB>*m<st>|p<st>>|]>+R*T*ln <frac|m<B>|m<st>><eq-number>>>>>
  </eqnarray*>

  Note how in each equation the argument of a logarithm is multiplied and
  divided by a constant, <math|c<st>> or <math|m<st>>, in order to make the
  arguments of the resulting logarithms dimensionless. These constants are
  called <subindex|Standard|composition><em|standard compositions> with the
  following values:

  <\description-compact>
    <item*|standard concentration><subindex|Standard|concentration><subindex|Concentration|standard><math|c<st>=1
    <text|mol>\<cdot\><text|dm><rsup|-3>> (equal to one mole per liter, or
    one molar) <subindex|Standard|molality><subindex|Molality|standard>

    <item*|standard molality><math|m<st>=1
    <text|mol>\<cdot\><text|kg><rsup|-1>> (equal to one molal)
  </description-compact>

  Again in each of these equations, we replace the expression in brackets,
  which depends on <math|T> and <math|p> but not on composition, with the
  chemical potential of a solute reference state:

  <\equation-cov2|<label|muB=mu(cB)(ref)+RT*ln(cB/co)>\<mu\><B><around|(|T,p|)>=\<mu\><cbB><rf><around|(|T,p|)>+R*T*ln
  <frac|c<B>|c<st>>>
    (ideal\Udolute solution

    of a nonelectrolyte)
  </equation-cov2>

  <\equation-cov2|<label|muB=mu(mB)(ref)+RT*ln(mB/mo)>\<mu\><B><around|(|T,p|)>=\<mu\><mbB><rf><around|(|T,p|)>+R*T*ln
  <frac|m<B>|m<st>>>
    (ideal\Udilute solution

    of a nonelectrolyte)
  </equation-cov2>

  The quantities <math|\<mu\><cbB><rf>> and <math|\<mu\><mbB><rf>> are the
  chemical potentials of the solute in hypothetical reference states that are
  solutions of standard concentration and standard molality, respectively, in
  which B behaves as in an ideal-dilute solution. Section <reference|9-mixt
  st states> will show that when the pressure is the standard pressure, these
  reference states are solute <em|standard>
  states.<index-complex|<tuple|reference state|solute>||c9 sec
  lsmn-ideal-dilute idx1|<tuple|Reference state|of a
  solute>><index-complex|<tuple|solute|reference state>||c9 sec
  lsmn-ideal-dilute idx2|<tuple|Solute|reference state>>

  For consistency with Eqs. <reference|muB=mu(cB)(ref)+RT*ln(cB/co)> and
  <reference|muB=mu(mB)(ref)+RT*ln(mB/mo)>, we can rewrite Eq.
  <reference|muB=mu(xB)(ref)+RT*ln(xB)> in the form

  <\equation>
    \<mu\><B><around|(|T,p|)>=\<mu\><xbB><rf><around|(|T,p|)>+R*T*ln
    <frac|x<B>|x<st>>
  </equation>

  <subindex|Standard|mole fraction><subindex|Mole fraction|standard>with
  <math|x<st>>, the <newterm|standard mole fraction>, given by
  <math|x<st>=1>.

  <subsection|Solvent behavior in the ideal-dilute solution><label|9-Solvent
  in ideal-dilute soln><label|c9 sec-lsmn-ideal-dilute-solvent>

  <index-complex|<tuple|solvent|behavior in an ideal-dilute solution>||c9
  sec-lsmn-ideal-dilute-solvent idx1|<tuple|Solvent|behavior in an
  ideal\Udilute solution>><index-complex|<tuple|ideal-dilute solution|solvent
  behavior in>||c9 sec-lsmn-ideal-dilute-solvent idx2|<tuple|Ideal\Udilute
  solution|solvent behavior in>>We now use the Gibbs\UDuhem equation to
  investigate the behavior of the solvent in an ideal-dilute solution of one
  or more nonelectrolyte solutes. <index|Gibbs--Duhem equation>The
  Gibbs\UDuhem equation applied to chemical potentials at constant <math|T>
  and <math|p> can be written <math|<big|sum><rsub|i>x<rsub|i>*<dif>\<mu\><rsub|i>=0>
  (Eq. <reference|sum(x_i)dmu_i=0>). We use subscript A for the solvent,
  rewrite the equation as <math|x<A><dif>\<mu\><A>+<big|sum><rsub|i\<ne\><text|A>>x<rsub|i>*<dif>\<mu\><rsub|i>=0>,
  and rearrange to

  <equation-cov2|<label|dmu_A=-(1/x_A)sum(x_i)dmu_i><dif>\<mu\><A>=-<frac|1|x<A>>*<big|sum><rsub|i\<ne\><text|A>>x<rsub|i>*<dif>\<mu\><rsub|i>|(constant
  <math|T> and <math|p>)>

  This equation shows how changes in the solute chemical potentials, due to a
  composition change at constant <math|T> and <math|p>, affect the chemical
  potential of the solvent.

  In an ideal-dilute solution, the chemical potential of each solute is given
  by <math|\<mu\><rsub|i>=\<mu\><rsub|x,i><rf>+R*T*ln x<rsub|i>> and the
  differential of <math|\<mu\><rsub|i>> at constant <math|T> and <math|p> is

  <\equation>
    <dif>\<mu\><rsub|i>=R*T*<dif>ln x<rsub|i>=R*T*<dx><rsub|i>/x<rsub|i>
  </equation>

  (Here the fact has been used that <math|\<mu\><rsub|x,i><rf>> is a constant
  at a given <math|T> and <math|p>.) When we substitute this expression for
  <math|<dif>\<mu\><rsub|i>> in Eq. <reference|dmu_A=-(1/x_A)sum(x_i)dmu_i>,
  we obtain

  <\equation>
    <label|d muA =><dif>\<mu\><A>=-<frac|R*T|x<A>>*<big|sum><rsub|i\<ne\><text|A>><dx><rsub|i>
  </equation>

  Now since the sum of all mole fractions is <math|1>, we have the relation
  <math|<big|sum><rsub|i\<neq\><text|A>>x<rsub|i>=1-x<A>> whose differential
  is <math|<big|sum><rsub|i\<neq\><text|A>><dx><rsub|i>=-<dx><A>>. Making
  this substitution in Eq. <reference|d muA => gives us

  <\equation-cov2|<label|d muA=RT dln(xA)><dif>\<mu\><A>=<frac|R*T|x<A>>*<dx><A>=R*T*<dif>ln
  x<A>>
    (ideal\Udilute solution

    of nonelectrolyte)
  </equation-cov2>

  Consider a process in an open system in which we start with a fixed amount
  of pure solvent and continuously add the solute or solutes at constant
  <math|T> and <math|p>. The solvent mole fraction decreases from unity to a
  value <math|x<rprime|'><A>>, and the solvent chemical potential changes
  from <math|\<mu\><A><rsup|\<ast\>>> to <math|\<mu\><rprime|'><A>>. We
  assume the solution formed in this process is in the ideal-dilute solution
  range, and integrate Eq. <reference|d muA=RT dln(xA)> over the path of the
  process:

  <\equation>
    <big|int><rsub|\<mu\><A><rsup|\<ast\>>><rsup|\<mu\><rprime|'><A>><dif>\<mu\><A>=R*T*<big|int><rsub|x<A>=1><rsup|x<A>=x<rprime|'><A>><dif>ln
    x<A>
  </equation>

  The result is <math|\<mu\><rprime|'><A>-\<mu\><A><rsup|\<ast\>>=R*T*ln
  x<rprime|'><A>>, or in general

  <\equation>
    <label|mu_A(id-dil sln)>\<mu\><A>=\<mu\><A><rsup|\<ast\>>+R*T*ln x<A>
  </equation>

  Comparison with Eq. <vpageref|mu_i=(mu_i*)+RT*ln(x_i)> shows that Eq.
  <reference|mu_A(id-dil sln)> is equivalent to Raoult's law for fugacity.

  <index-complex|<tuple|raoult's law|fugacity|ideal dilute>|||<tuple|Raoult's
  law|for fugacity|in an ideal\Udilute solution>>Thus, in an ideal-dilute
  solution of nonelectrolytes <em|each solute obeys Henry's law and the
  solvent obeys Raoult's law>.

  An equivalent statement is that a nonelectrolyte constituent of a liquid
  mixture approaches Henry's law behavior as its mole fraction approaches
  zero, and approaches Raoult's law behavior as its mole fraction approaches
  unity. This is illustrated in Fig. <vpageref|fig:9-ethanol
  fug>,<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/ETOH-FUG.eps|197pt|161pt||>>
        <label|fig:9-ethanol fug>Fugacity of ethanol in a gas phase
        equilibrated with a binary liquid mixture of ethanol (A) and
        H<rsub|<math|2>>O at <math|25<degC>> and <math|1<br>>. Open circles:
        experimental measurements.<space|.15em><footnote|Ref.
        <cite|dobson-25>.> The dashed lines show Henry's law behavior and
        Raoult's law behavior.
      </big-figure>
    </framed>
  </float> which shows the behavior of ethanol in ethanol-water mixtures. The
  ethanol exhibits positive deviations from Raoult's law and negative
  deviations from Henry's law.<index-complex|<tuple|solvent|behavior in an
  ideal-dilute solution>||c9 sec-lsmn-ideal-dilute-solvent
  idx1|<tuple|Solvent|behavior in an ideal\Udilute
  solution>><index-complex|<tuple|ideal-dilute solution|solvent behavior
  in>||c9 sec-lsmn-ideal-dilute-solvent idx2|<tuple|Ideal\Udilute
  solution|solvent behavior in>>

  <subsection|Partial molar quantities in the ideal-dilute
  solution><label|9-partial molar, id dil sln><label|c9
  sec-lsmn-ideal-dilute-partial-molar>

  <index-complex|<tuple|partial molar|quantity|ideal-dilute solution>||c9
  sec-lsmn-ideal-dilute-partial-molar idx1|<tuple|Partial molar|quantity|in
  an ideal\Udilute solution>><index-complex|<tuple|ideal-dilute
  solution|partial molar quantities in>||c9
  sec-lsmn-ideal-dilute-partial-molar idx2|<tuple|Ideal\Udilute
  solution|partial molar quantities in>>Consider the <em|solvent>, A, of a
  solution that is dilute enough to be in the ideal-dilute range. In this
  range, the solvent fugacity obeys Raoult's law, and the partial molar
  quantities of the solvent are the same as those in an ideal mixture.
  Formulas for these quantities were given in Eqs. <reference|ideal
  mixture>\U<reference|Cpi=Cpi^*> and are collected in the first column of
  Table <vpageref|tbl:9-id dil sln>.<float|float|thb|<\big-table>
    <\equation*>
      <bktable3|<tformat|<cwith|2|11|1|2|cell-halign|C=>|||||||||||<table|<row|<cell|<text|Solvent>>|<cell|<text|Solute>>>|<row|<cell|\<mu\><A>=\<mu\><A><rsup|\<ast\>>+R*T*ln
      x<A>>|<cell|\<mu\><B>=\<mu\><xbB><rf>+R*T*ln
      x<B>>>|<row|<cell|>|<cell|=\<mu\><cbB><rf>+R*T*ln
      <around|(|c<B>/c<st>|)>>>|<row|<cell|>|<cell|=\<mu\><mbB><rf>+R*T*ln
      <around|(|m<B>/m<st>|)>>>|<row|<cell|S<A>=S<A><rsup|\<ast\>>-R*ln
      x<A>>|<cell|S<B>=S<xbB><rf>-R*ln x<B>>>|<row|<cell|>|<cell|=S<cbB><rf>-R*ln
      <around|(|c<B>/c<st>|)>>>|<row|<cell|>|<cell|=S<mbB><rf>-R*ln
      <around|(|m<B>/m<st>|)>>>|<row|<cell|H<A>=H<A><rsup|\<ast\>>>|<cell|H<B>=H<B><rsup|\<infty\>>>>|<row|<cell|V<A>=V<A><rsup|\<ast\>>>|<cell|V<B>=V<B><rsup|\<infty\>>>>|<row|<cell|U<A>=U<A><rsup|\<ast\>>>|<cell|U<B>=U<B><rsup|\<infty\>>>>|<row|<cell|C<rsub|p,<text|A>>=C<rsub|p,<text|A>><rsup|\<ast\>>>|<cell|C<rsub|p,<text|B>>=C<rsub|p,<text|B>><rsup|\<infty\>>>>>>>
    </equation*>

    \;
  <|big-table>
    <label|tbl:9-id dil sln>Partial molar quantities of solvent and
    nonelectrolyte solute in an ideal-dilute solution
  </big-table>>

  The formulas show that the chemical potential and partial molar entropy of
  the solvent, at constant <math|T> and <math|p>, vary with the solution
  composition and, in the limit of infinite dilution
  (<math|x<A>><ra><math|1>), approach the values for the pure solvent. The
  partial molar enthalpy, volume, internal energy, and heat capacity, on the
  other hand, are independent of composition in the ideal-dilute region and
  are equal to the corresponding molar quantities for the pure solvent.

  Next consider a <em|solute>, B, of a binary ideal-dilute solution. The
  solute obeys Henry's law, and its chemical potential is given by
  <math|\<mu\><B>=\<mu\><xbB><rf>+R*T*ln x<B>> (Eq.
  <reference|muB=mu(xB)(ref)+RT*ln(xB)>) where <math|\<mu\><xbB><rf>> is a
  function of <math|T> and <math|p>, but not of composition. <math|\<mu\><B>>
  varies with the composition and goes to <math|-\<infty\>> as the solution
  becomes infinitely dilute (<math|x<A>><ra><math|1> and
  <math|x<B>><ra><math|0>).

  For the <index-complex|<tuple|partial molar|entropy|solute in an
  ideal-dilute solution>|||<tuple|Partial molar|entropy|of a solute in an
  ideal\Udilute solution>><index-complex|<tuple|entropy|partial molar|solute
  in an ideal-dilute solution>|||<tuple|Entropy|partial molar|of a solute in
  an ideal\Udilute solution>>partial molar entropy of the solute, we use
  <math|S<B>=-<pd|\<mu\><B>|T|p,<allni>>> (Eq. <reference|d(mu_i)/dT=-S_i>)
  and obtain

  <\equation>
    <label|S(B)=>S<B>=-<Pd|\<mu\><xbB><rf>|T|<space|-0.17em>p>-R*ln x<B>
  </equation>

  The term <math|-<pd|\<mu\><xbB><rf>|T|p>> represents the partial molar
  entropy <math|S<xbB><rf>> of B in the fictitious reference state of unit
  solute mole fraction. Thus, we can write Eq. <reference|S(B)=> in the form

  <\equation-cov2|<label|S(B)=S(B,ref)-R*ln(xB)>S<B>=S<xbB><rf>-R*ln x<B>>
    (ideal\Udilute solution

    of a nonelectrolyte)
  </equation-cov2>

  This equation shows that the partial molar entropy varies with composition
  and goes to <math|+\<infty\>> in the limit of infinite dilution. From the
  expressions of Eqs. <reference|muB=mu(cB)(ref)+RT*ln(cB/co)> and
  <reference|muB=mu(mB)(ref)+RT*ln(mB/mo)>, we can derive similar expressions
  for <math|S<B>> in terms of the solute reference states on a concentration
  or molality basis.

  The relation <math|H<B>=\<mu\><B>+T*S<B>> (from Eq.
  <reference|mu_i=H_i-TS_i>), combined with Eqs.
  <reference|muB=mu(xB)(ref)+RT*ln(xB)> and
  <reference|S(B)=S(B,ref)-R*ln(xB)>, yields

  <\equation>
    H<B>=\<mu\><xbB><rf>+T*S<xbB><rf>=H<xbB><rf>
  </equation>

  showing that at constant <math|T> and <math|p>, the
  <index-complex|<tuple|partial molar|enthalpy|solute in an ideal-dilute
  solution>|||<tuple|Partial molar|enthalpy|of a solute in an ideal\Udilute
  solution>><index-complex|<tuple|enthalpy|partial molar|solute in an
  ideal-dilute solution>|||<tuple|Enthalpy|partial molar|of a solute in an
  ideal\Udilute solution>>partial molar enthalpy of the solute is constant
  throughout the ideal-dilute solution range. Therefore, we can write

  <\equation-cov2|H<B>=H<B><rsup|\<infty\>>>
    (ideal\Udilute solution

    of a nonelectrolyte)
  </equation-cov2>

  where <math|H<B><rsup|\<infty\>>> is the partial molar enthalpy at infinite
  dilution. By similar reasoning, using Eqs.
  <reference|d(mu_i)/dp=V_i>\U<reference|C_pi=dH_i/dT>, we find that the
  partial molar volume, internal energy, and heat capacity of the solute are
  constant in the ideal-dilute range and equal to the values at infinite
  dilution. The expressions are listed in the second column of Table
  <reference|tbl:9-id dil sln>.

  When the pressure is equal to the standard pressure <math|p<st>>, the
  quantities <math|H<B><rsup|\<infty\>>>, <math|V<B><rsup|\<infty\>>>,
  <math|U<B><rsup|\<infty\>>>, and <math|C<rsub|p,<text|B>><rsup|\<infty\>>>
  are the same as the standard values <math|H<B><st>>, <math|V<B><st>>,
  <math|U<B><st>>, and <math|C<rsub|p,<text|B>><st>>.<index-complex|<tuple|partial
  molar|quantity|ideal-dilute solution>||c9
  sec-lsmn-ideal-dilute-partial-molar idx1|<tuple|Partial molar|quantity|in
  an ideal\Udilute solution>><index-complex|<tuple|ideal-dilute
  solution|partial molar quantities in>||c9
  sec-lsmn-ideal-dilute-partial-molar idx2|<tuple|Ideal\Udilute
  solution|partial molar quantities in>>

  <\bio-insert>
    <include|bio-HENRY.tm>
  </bio-insert>

  <page-break>

  <section|Activity Coefficients in Mixtures of Nonelectrolytes><label|9-act
  coeffs><label|c9 sec-acmn>

  An <index|Activity coefficient><em|activity coefficient> of a species is a
  kind of adjustment factor that relates the actual behavior to ideal
  behavior at the same temperature and pressure. The ideal behavior is based
  on a <index|Reference state><em|reference state> for the species.

  We begin by describing reference states for nonelectrolytes. The
  thermodynamic behavior of an electrolyte solution is more complicated than
  that of a mixture of nonelectrolytes, and will be discussed in the next
  chapter.

  <subsection|Reference states and standard states><label|c9
  sec-acmn-refstd-states>

  A <em|reference state> of a constituent of a mixture has the same
  temperature and pressure as the mixture. When species <math|i> is in its
  reference state, its chemical potential <math|\<mu\><rsub|i><rf>> depends
  only on the temperature and pressure of the mixture.

  If the pressure is the standard pressure <math|p<st>>, the reference state
  of species <math|i> becomes its <em|standard state>. In the standard state,
  the chemical potential is the <subindex|Chemical
  potential|standard><em|standard chemical potential>
  <math|\<mu\><rsub|i><st>>, which is a function only of temperature.

  Reference states are useful for derivations involving processes taking
  place at constant <math|T> and <math|p> when the pressure is not
  necessarily the standard pressure.

  Table <vpageref|tbl:9-st states> describes the reference states of
  nonelectrolytes used in this book, and lists symbols for chemical
  potentials of substances in these states. The symbols for solutes include
  <math|x>, <math|c>, or <math|m> in the subscript to indicate the basis of
  the reference state.<float|float|hb|<\big-table>
    <bktable3|<tformat|<cwith|2|7|1|1|cell-valign|c>|<cwith|3|7|2|2|cell-valign|c>|<cwith|1|1|1|3|cell-hyphen|b>|<cwith|2|7|2|2|cell-hyphen|t>|<cwith|2|7|1|1|cell-hyphen|t>|||||||<twith|table-width|1par>|<twith|table-hmode|exact>|<table|<row|<\cell>
      Constituent
    </cell>|<\cell>
      Reference state
    </cell>|<\cell>
      <no-indent*>Chemical

      potential
    </cell>>|<row|<\cell>
      Substance <math|i> in a gas mixture
    </cell>|<\cell>
      Pure <math|i> behaving as an ideal gas<rsup|a>
    </cell>|<cell|<math|\<mu\><rsub|i><rf><gas>>>>|<row|<\cell>
      Substance <math|i> in a liquid or solid mixture
    </cell>|<\cell>
      Pure <math|i> in the same physical state as the mixture
    </cell>|<cell|<math|\<mu\><rsub|i><rsup|\<ast\>>>>>|<row|<\cell>
      Solvent A of a solution
    </cell>|<\cell>
      Pure A in the same physical state as the solution
    </cell>|<cell|<math|\<mu\><A><rsup|\<ast\>>>>>|<row|<\cell>
      Solute B, mole fraction basis
    </cell>|<\cell>
      B at mole fraction <math|1>, behavior extrapolated from infinite
      dilution on a mole fraction basis<rsup|<math|a>>
    </cell>|<cell|<math|\<mu\><rf><xbB>>>>|<row|<\cell>
      Solute B, concentration basis
    </cell>|<\cell>
      B at concentration <math|c<st>>, behavior extrapolated from infinite
      dilution on a concentration basis<rsup|<math|a>>
    </cell>|<cell|<math|\<mu\><rf><cbB>>>>|<row|<\cell>
      Solute B, molality basis
    </cell>|<\cell>
      B at molality <math|m<st>>, behavior extrapolated from infinite
      dilution on a molality basis<rsup|<math|a>>
    </cell>|<cell|<math|\<mu\><rf><mbB>>>>>>>

    \;
  <|big-table>
    <label|tbl:9-st states>Reference states for nonelectrolyte constituents
    of mixtures. In each reference state, the temperature and pressure are
    the same as those of the mixture.

    \;

    <rsup|a>A hypothetical state.
  </big-table>>

  <subsection|Ideal mixtures><label|c9 sec acmn-mixtures-ideal>

  <index-complex|<tuple|ideal mixture>||c9 sec acmn-mixtures-ideal
  idx1|<tuple|Ideal mixture>><index-complex|<tuple|mixture|ideal>||c9 sec
  acmn-mixtures-ideal idx2|<tuple|Mixture|ideal>>Since the activity
  coefficient of a species relates its actual behavior to its ideal behavior
  at the same <math|T> and <math|p>, let us begin by examining behavior in
  ideal mixtures.

  Consider first an ideal gas mixture at pressure <math|p>. The chemical
  potential of substance <math|i> in this ideal gas mixture is given by Eq.
  <reference|mu_i=mu_io(g)+RT*ln(p_i/po)> (the superscript \Pid\Q stands for
  ideal):

  <\equation>
    <label|mu_i(id)=mu_i^o(g)+>\<mu\><rsub|i><gas>=\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|p<rsub|i>|p<st>>
  </equation>

  The reference state of gaseous substance <math|i> is pure <math|i> acting
  as an ideal gas at pressure <math|p>. Its chemical potential is given by

  <\equation>
    <label|mu_i(ref)=>\<mu\><rsub|i><rf><gas>=\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|p|p<st>>
  </equation>

  Subtracting Eq. <reference|mu_i(ref)=> from Eq.
  <reference|mu_i(id)=mu_i^o(g)+>, we obtain

  <\equation>
    <label|mu_i(id)-mu_i(ref)=>\<mu\><rsub|i><gas>-\<mu\><rsub|i><rf><gas>=R*T*ln
    <frac|p<rsub|i>|p>
  </equation>

  Consider the following expressions for chemical potentials in ideal
  mixtures and ideal-dilute solutions of nonelectrolytes. The first equation
  is a rearrangement of Eq. <reference|mu_i(id)-mu_i(ref)=>, and the others
  are from earlier sections of this chapter.<footnote|In order of occurrence,
  Eqs. <reference|ideal mixture>, <reference|mu_A(id-dil sln)>,
  <reference|muB=mu(xB)(ref)+RT*ln(xB)>, <reference|muB=mu(cB)(ref)+RT*ln(cB/co)>,
  and <reference|muB=mu(mB)(ref)+RT*ln(mB/mo)>.>

  <\eqnarray*>
    <tformat|<cwith|1|-1|2|2|cell-halign|c>|<table|<row|<cell|<hspace|4fn><text|Constituent
    of an ideal gas mixture><htab|5mm>\<mu\><rsub|i><gas>>|<cell|=>|<cell|\<mu\><rsub|i><rf><gas>+R*T*ln
    <frac|p<rsub|i>|p><eq-number><label|mu_i(id) gas
    mixt>>>|<row|<cell|<hspace|4fn><text|Constituent of an ideal liquid or
    solid mixture><htab|5mm>\<mu\><rsub|i>>|<cell|=>|<cell|\<mu\><rsub|i><rsup|\<ast\>>+R*T*ln
    x<rsub|i><eq-number>>>|<row|<cell|<hspace|4fn><text|Solvent of an
    ideal\Udilute solution><htab|5mm>\<mu\><A>>|<cell|=>|<cell|\<mu\><A><rsup|\<ast\>>+R*T*ln
    x<A><eq-number>>>|<row|<cell|<hspace|4fn><text|Solute, ideal\Udilute
    solution, mole fraction basis><htab|5mm>\<mu\><B>>|<cell|=>|<cell|\<mu\><xbB><rf>+R*T*ln
    x<B><eq-number>>>|<row|<cell|<hspace|4fn><text|Solute, ideal\Udilute
    solution, concentration basis><htab|5mm>\<mu\><B>>|<cell|=>|<cell|\<mu\><cbB><rf>+R*T*ln
    <frac|c<B>|c<st>><eq-number><label|mu(c,B)id>>>|<row|<cell|<hspace|4fn><text|Solute,
    ideal\Udilute solution, molality basis><htab|5mm>\<mu\><B>>|<cell|=>|<cell|\<mu\><mbB><rf>+R*T*ln
    <frac|m<B>|m<st>><eq-number><label|mu(m,B)id>>>>>
  </eqnarray*>

  Note that the equations for the condensed phases have the general form

  <\equation>
    <label|mu_i(id) general form>\<mu\><rsub|i>=\<mu\><rsub|i><rf>+R*T*ln
    <around*|(|<frac|<text|composition variable>|<text|standard
    composition>>|)>
  </equation>

  where <math|\<mu\><rsub|i><rf>> is the chemical potential of component
  <math|i> in an appropriate reference state. (The standard composition on a
  mole fraction basis is <math|x<st|=>=1>.)<index-complex|<tuple|ideal
  mixture>||c9 sec acmn-mixtures-ideal idx1|<tuple|Ideal
  mixture>><index-complex|<tuple|mixture|ideal>||c9 sec acmn-mixtures-ideal
  idx2|<tuple|Mixture|ideal>>

  <subsection|Real mixtures><label|9-real mixts><label|c9 sec
  acmn-mixtures-real>

  If a mixture is <em|not> ideal, we can write an expression for the chemical
  potential of each component that includes an <index|Activity
  coefficient><newterm|activity coefficient>. The expression is like one of
  those for the ideal case (Eqs. <reference|mu_i(id) gas
  mixt>\U<reference|mu(m,B)id>) with the activity coefficient multiplying the
  quantity within the logarithm.

  Consider constituent <math|i> of a gas mixture. If we eliminate
  <math|\<mu\><rsub|i><st><gas>> from Eqs.
  <reference|mu_i=mu_io(g)+RT*(f_i/po)> and <reference|mu_i(ref)=>, we obtain

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<mu\><rsub|i>>|<cell|=>|<cell|\<mu\><rsub|i><rf><gas>+R*T*ln
    <frac|<fug><rsub|i>|p>>>|<row|<cell|>|<cell|=>|<cell|\<mu\><rsub|i><rf><gas>+R*T*ln
    <frac|\<phi\><rsub|i>*p<rsub|i>|p><eq-number><label|mu_i-mu_i(ref)(g)=>>>>>
  </eqnarray*>

  where <math|<fug><rsub|i>> is the fugacity of constituent <math|i> and
  <math|\<phi\><rsub|i>> is its fugacity coefficient. Here the activity
  coefficient is the fugacity coefficient <math|\<phi\><rsub|i>>.

  For components of a condensed-phase mixture, we write expressions for the
  chemical potential having a form similar to that in Eq. <reference|mu_i(id)
  general form>, with the composition variable now multiplied by an activity
  coefficient:

  <\equation>
    <label|act coeff defn>\<mu\><rsub|i>=\<mu\><rsub|i><rf>+R*T*ln
    <around*|[|<around|(|<text|activity coefficient of
    <math|i>>|)>\<times\><around*|(|<frac|<text|composition
    variable>|<text|standard composition>>|)>|]>
  </equation>

  The activity coefficient of a species is a dimensionless quantity whose
  value depends on the temperature, the pressure, the mixture composition,
  and the choice of the reference state for the species. Under conditions in
  which the mixture behaves ideally, the activity coefficient is unity and
  the chemical potential is given by one of the expressions of Eqs.
  <reference|mu_i(id) gas mixt>\U<reference|mu(m,B)id>; otherwise, the
  activity coefficient has the value that gives the actual chemical
  potential.

  This book will use various symbols for activity coefficients, as indicated
  in the following list of expressions for the chemical potentials of
  nonelectrolytes:

  <\eqnarray*>
    <tformat|<cwith|1|-1|2|2|cell-halign|c>|<table|<row|<cell|<hspace|4fn><text|Constituent
    of a gas mixture><htab|5mm>\<mu\><rsub|i>>|<cell|=>|<cell|\<mu\><rsub|i><rf><gas>+R*T*ln
    <around*|(|\<phi\><rsub|i>*<frac|p<rsub|i>|p>|)><eq-number><label|act
    coeff, gas>>>|<row|<cell|<hspace|4fn><text|Constituent of a liquid or
    solid mixture><htab|5mm>\<mu\><rsub|i>>|<cell|=>|<cell|\<mu\><rsub|i><rsup|\<ast\>>+R*T*ln
    <around*|(|<g><rsub|i>*x<rsub|i>|)><eq-number><label|act coeff,
    mixt>>>|<row|<cell|<hspace|4fn><text|Solvent of a
    solution><htab|5mm>\<mu\><A>>|<cell|=>|<cell|\<mu\><A><rsup|\<ast\>>+R*T*ln
    <around*|(|<g><A>*x<A>|)><eq-number><label|act coeff,
    solvent>>>|<row|<cell|<hspace|4fn><text|Solute of a solution, mole
    fraction basis><htab|5mm>\<mu\><B>>|<cell|=>|<cell|\<mu\><xbB><rf>+R*T*ln
    <around*|(|<g><xbB>*x<B>|)><eq-number><label|act coeff
    x,B>>>|<row|<cell|<hspace|4fn><text|Solute of a solution, concentration
    basis><htab|5mm>\<mu\><B>>|<cell|=>|<cell|\<mu\><cbB><rf>+R*T*ln
    <around*|(|<g><cbB><frac|c<B>|c<st>>|)><eq-number><label|act coeff
    c,B>>>|<row|<cell|<hspace|4fn><text|Solute of a solution, molality
    basis><htab|5mm>\<mu\><B>>|<cell|=>|<cell|\<mu\><mbB><rf>+R*T*ln
    <around*|(|<g><mbB>*<frac|m<B>|m<st>>|)><eq-number><label|act coeff
    m,B>>>>>
  </eqnarray*>

  Equation <reference|act coeff, mixt> refers to a component of a liquid or
  solid mixture of substances that mix in all proportions. Equation
  <reference|act coeff, solvent> refers to the solvent of a solution. The
  <index-complex|<tuple|reference state|mixture
  constituent>|||<tuple|Reference state|of a mixture
  constituent>><index-complex|<tuple|reference
  state|solvent>|||<tuple|Reference state|of a solvent>>reference states of
  these components are the pure liquid or solid at the temperature and
  pressure of the mixture. For the activity coefficients of these components,
  this book uses the symbols <math|<g><rsub|i>> and <math|<g><A>>.

  <\quote-env>
    \ The <index|IUPAC Green Book>IUPAC Green Book (Ref. <cite|greenbook-3>,
    p. 59) recommends the symbol <math|f<rsub|i>> for the activity
    coefficient of component <math|i> when the reference state is the pure
    liquid or solid. This book instead uses symbols such as
    <math|<g><rsub|i>> and <math|<g><A>>, in order to avoid confusion with
    the symbol usually used for fugacity, <math|<fug><rsub|i>>.
  </quote-env>

  In Eqs. <reference|act coeff x,B>\U<reference|act coeff m,B>, the symbols
  <math|<g><xbB>>, <math|<g><cbB>>, and <math|<g><mbB>> for activity
  coefficients of a nonelectrolyte solute include <math|x>, <math|c>, or
  <math|m> in the subscript to indicate the choice of the
  <index-complex|<tuple|reference state|solute>|||<tuple|Reference state|of a
  solute>><subindex|Solute|reference state>solute reference state. Although
  three different expressions for <math|\<mu\><B>> are shown, for a given
  solution composition they must all represent the same <em|value> of
  <math|\<mu\><B>>, equal to the rate at which the Gibbs energy increases
  with the amount of substance B added to the solution at constant <math|T>
  and <math|p>. The value of a solute activity coefficient, on the other
  hand, depends on the choice of the solute reference state.

  You may find it helpful to interpret products appearing on the right sides
  of Eqs. <reference|act coeff, gas>\U<reference|act coeff m,B> as follows.

  <\itemize>
    <item><math|\<phi\><rsub|i>*p<rsub|i>> is an effective partial pressure.

    <item><math|<g><rsub|i>*x<rsub|i>>, <math|<g><A>*x<A>>, and
    <math|<g><xbB>*x<B>> are effective mole fractions.

    <item><math|<g><cbB>*c<B>> is an effective concentration.

    <item><math|<g><mbB>*m<B>> is an effective molality.
  </itemize>

  In other words, the value of one of these products is the value of a
  partial pressure or composition variable that would give the same chemical
  potential in an ideal mixture as the actual chemical potential in the real
  mixture. These effective composition variables are an alternative way to
  express the escaping tendency of a substance from a phase; they are related
  exponentially to the chemical potential, which is also a measure of
  escaping tendency.

  A change in pressure or composition that causes a mixture to approach the
  behavior of an ideal mixture or ideal-dilute solution must cause the
  activity coefficient of each mixture constituent to approach unity:
  <subindex|Activity coefficient|approach to unity>

  <\eqnarray*>
    <tformat|<cwith|1|-1|2|2|cell-halign|c>|<table|<row|<cell|<hspace|4fn><text|Constituent
    of a gas mixture><htab|5mm>\<phi\><rsub|i>>|<cell|\<rightarrow\>>|<cell|1<space|2em><text|as><space|2em>p\<rightarrow\>0<eq-number><label|phi_i
    -\<gtr\> 1>>>|<row|<cell|<hspace|4fn><text|Constituent of a liquid or
    solid mixture><htab|5mm><g><rsub|i>>|<cell|\<rightarrow\>>|<cell|1<space|2em><text|as><space|2em>x<rsub|i>\<rightarrow\>1<eq-number><label|ac_i
    -\<gtr\> 1>>>|<row|<cell|<hspace|4fn><text|Solvent of a
    solution><htab|5mm><g><A>>|<cell|\<rightarrow\>>|<cell|1<space|2em><text|as><space|2em>x<A>\<rightarrow\>1<eq-number><label|gamma(A)-\<gtr\>1>>>|<row|<cell|<hspace|4fn><text|Solute
    of a solution, mole fraction basis><htab|5mm><g><xbB>>|<cell|\<rightarrow\>>|<cell|1<space|2em><text|as><space|2em>x<B>\<rightarrow\>0<eq-number><label|gamma(xB)-\<gtr\>1>>>|<row|<cell|<hspace|4fn><text|Solute
    of a solution, concentration basis><htab|5mm><g><cbB>>|<cell|\<rightarrow\>>|<cell|1<space|2em><text|as><space|2em>c<B>\<rightarrow\>0<eq-number><label|gamma(cB)-\<gtr\>1>>>|<row|<cell|<hspace|4fn><text|Solute
    of a solution, molality basis><htab|5mm><g><mbB>>|<cell|\<rightarrow\>>|<cell|1<space|2em><text|as><space|2em>m<B>\<rightarrow\>0<eq-number><label|gamma(mB)-\<gtr\>1>>>>>
  </eqnarray*>

  <subsection|Nonideal dilute solutions><label|9-nonideal dil solns><label|c9
  sec-acmn-solutions-nonideal>

  <index-complex|<tuple|activity coefficient|solute|dilute
  solution>|||<tuple|Activity coefficient|of a solute|in dilute solution>>How
  would we expect the activity coefficient of a nonelectrolyte solute to
  behave in a dilute solution as the solute mole fraction increases beyond
  the range of ideal-dilute solution behavior?

  <\quote-env>
    The following argument is based on molecular properties at constant
    <math|T> and <math|p>.

    We focus our attention on a single solute molecule. This molecule has
    interactions with nearby solute molecules. Each interaction depends on
    the intermolecular distance and causes a change in the internal energy
    compared to the interaction of the solute molecule with solvent at the
    same distance.<footnote|In Sec. <reference|11-mol model of id mixt>, it
    will be shown that roughly speaking the internal energy change is
    negative if the average of the attractive forces between two solute
    molecules and two solvent molecules is greater than the attractive force
    between a solute molecule and a solvent molecule at the same distance,
    and is positive for the opposite situation.> The number of solute
    molecules in a volume element at a given distance from the solute
    molecule we are focusing on is proportional to the local solute
    concentration. If the solution is dilute and the interactions weak, we
    expect the local solute concentration to be proportional to the
    macroscopic solute mole fraction. Thus, the partial molar quantities
    <math|U<B>> and <math|V<B>> of the solute should be approximately linear
    functions of <math|x<B>> in a dilute solution at constant <math|T> and
    <math|p>.

    From Eqs. <reference|mu_i=H_i-TS_i> and <reference|U_i=H_i-pV_i>, the
    solute chemical potential is given by
    <math|\<mu\><B>=U<B>+p*V<B>-T*S<B>>. In the dilute solution, we assume
    <math|U<B>> and <math|V<B>> are linear functions of <math|x<B>> as
    explained above. We also assume the dependence of <math|S<B>> on
    <math|x<B>> is approximately the same as in an ideal mixture; this is a
    prediction from <subindex|Statistical mechanics|ideal mixture>statistical
    mechanics for a mixture in which all molecules have similar sizes and
    shapes. Thus we expect the deviation of the chemical potential from
    ideal-dilute behavior, <math|\<mu\><B>=\<mu\><xbB><rf>+R*T*ln x<B>>, can
    be described by adding a term proportional to <math|x<B>>:
    <math|\<mu\><B>=\<mu\><xbB><rf>+R*T*ln x<B>+k<rsub|x>*x<B>>, where
    <math|k<rsub|x>> is a positive or negative constant related to
    solute-solute interactions.

    If we equate this expression for <math|\<mu\><B>> with the one that
    defines the activity coefficient, <math|\<mu\><B>=\<mu\><xbB><rf>+R*T*ln
    <around|(|<g><xbB><space|0.17em>x<B>|)>> (Eq. <reference|act coeff x,B>),
    and solve for the activity coefficient, we obtain the
    relation<footnote|This is essentially the result of the
    <index-complex|<tuple|mcmillan mayer>|||<tuple|McMillan\UMayer
    theory>>McMillan\UMayer solution theory from <subindex|Statistical
    mechanics|McMillan--Mayer theory>statistical mechanics.>
    <math|<g><xbB>=exp <space|0.17em><around|(|k<rsub|x>*x<B>/R*T|)>>. An
    expansion of the exponential in powers of <math|x<B>> converts this to

    <\equation>
      <g><xbB>=1+<around|(|k<rsub|x>/R*T|)>*x<B>+\<cdots\>
    </equation>

    Thus we predict that at constant <math|T> and <math|p>, <math|<g><xbB>>
    is a linear function of <math|x<B>> at low <math|x<B>>. An ideal-dilute
    solution, then, is one in which <math|x<B>> is much smaller than
    <math|R*T/k<rsub|x>> so that <math|<g><xbB>> is approximately 1. An ideal
    mixture requires the interaction constant <math|k<rsub|x>> to be zero.

    By similar reasoning, we reach analogous conclusions for solute activity
    coefficients on a concentration or molality basis. For instance, at low
    <math|m<B>> the chemical potential of B should be approximately
    <math|\<mu\><mbB><rf>+R*T*ln <around|(|m<B>/m<st>|)>+k<rsub|m>*m<B>>,
    where <math|k<rsub|m>> is a constant at a given <math|T> and <math|p>;
    then the activity coefficient at low <math|m<B>> is given by

    <\equation>
      <label|g(mB)=exp(k(m)mB/RT)><g><mbB>=exp
      <space|0.17em><around|(|k<rsub|m>*m<B>/R*T|)>=1+<around|(|k<rsub|m>/R*T|)>*m<B>+\<cdots\>
    </equation>
  </quote-env>

  The prediction from the theoretical argument above, that a solute activity
  coefficient in a dilute solution is a linear function of the composition
  variable, is borne out experimentally as illustrated in Fig.
  <vpageref|fig:9-H2O-BuOH>. This prediction applies only to a nonelectrolyte
  solute; for an electrolyte, the slope of activity coefficient versus
  molality approaches <math|-\<infty\>> at low molality (page
  <pageref|electrolyte act coeff vs molality>).

  <section|Evaluation of Activity Coefficients><label|c9 sec eac>

  This section describes several methods by which activity coefficients of
  nonelectrolyte substances may be evaluated. Section <reference|9-act coeffs
  from osmotic coeffs> describes an osmotic coefficient method that is also
  suitable for electrolyte solutes, as will be explained in Sec.
  <reference|10-ionic act coeffs from osmotic coeffs>.

  <subsection|Activity coefficients from gas fugacities><label|9-act coeff
  from fug><label|c9 sec-eac-gas-fugacity>

  <index-complex|<tuple|activity coefficient|solvent|gas fugacity>||c9
  sec-eac-gas-fugacity idx1|<tuple|Activity coefficient|of a solvent|from gas
  fugacity>><index-complex|<tuple|activity coefficient|solute|gas
  fugacity>||c9 sec-eac-gas-fugacity idx2|<tuple|Activity coefficient|of a
  solute|from gas fugacity>>Suppose we equilibrate a liquid mixture with a
  gas phase. If component <math|i> of the liquid mixture is a volatile
  nonelectrolyte, and we are able to evaluate its fugacity
  <math|<fug><rsub|i>> in the gas phase, we have a convenient way to evaluate
  the activity coefficient <math|<g><rsub|i>> in the liquid. The relation
  between <math|<g><rsub|i>> and <math|<fug><rsub|i>> will now be derived.

  When component <math|i> is in transfer equilibrium between two phases, its
  chemical potential is the same in both phases. Equating expressions for
  <math|\<mu\><rsub|i>> in the liquid mixture and the equilibrated gas phase
  (from Eqs. <reference|act coeff, mixt> and <reference|mu_i-mu_i(ref)(g)=>,
  respectively), and then solving for <math|<g><rsub|i>>, we have

  <\equation>
    <label|mu_i(mixt)=mu_i(g)>\<mu\><rsub|i><rsup|\<ast\>>+R*T*ln
    <around*|(|<g><rsub|i>x<rsub|i>|)>=\<mu\><rsub|i><rf><gas>+R*T*ln
    <space|0.17em><around|(|<fug><rsub|i>/p|)>
  </equation>

  <\equation>
    <label|gamma_i=><g><rsub|i>=exp <around*|[|<frac|\<mu\><rsub|i><rf><gas>-\<mu\><rsub|i><rsup|\<ast\>>|R*T>|]>\<times\><frac|<fug><rsub|i>|x<rsub|i>*p>
  </equation>

  On the right side of Eq. <reference|gamma_i=>, only <math|<fug><rsub|i>>
  and <math|x<rsub|i>> depend on the liquid composition. We can therefore
  write

  <\equation>
    <label|gamma_i=C f_i/x_i><g><rsub|i>=C<rsub|i>*<frac|<fug><rsub|i>|x<rsub|i>>
  </equation>

  where <math|C<rsub|i>> is a factor whose value depends on <math|T> and
  <math|p>, but not on the liquid composition. Solving Eq.
  <reference|gamma_i=C f_i/x_i> for <math|C<rsub|i>> gives
  <math|C<rsub|i>=<g><rsub|i>*x<rsub|i>/<fug><rsub|i>>.

  Now consider Eq. <vpageref|ac_i -\<gtr\> 1>. It says that as
  <math|x<rsub|i>> approaches 1 at constant <math|T> and <math|p>,
  <math|<g><rsub|i>> also approaches 1. We can use this limit to evaluate
  <math|C<rsub|i>>:

  <\equation>
    <label|C_i=1/f_i^*>C<rsub|i>=lim<rsub|x<rsub|i><ra>1>
    <frac|<g><rsub|i>*x<rsub|i>|<fug><rsub|i>>=<frac|1|<fug><rsub|i><rsup|\<ast\>>>
  </equation>

  Here <math|<fug><rsub|i><rsup|\<ast\>>> is the fugacity of <math|i> in a
  gas phase equilibrated with pure liquid <math|i> at the temperature and
  pressure of the mixture. Then substitution of this value of
  <math|C<rsub|i>> (which is independent of <math|x<rsub|i>>) in Eq.
  <reference|gamma_i=C f_i/x_i> gives us an expression for <math|<g><rsub|i>>
  at any liquid composition:

  <\equation>
    <g><rsub|i>=<frac|<fug><rsub|i>|x<rsub|i>*<fug><rsub|i><rsup|\<ast\>>>
  </equation>

  We can follow the same procedure for a solvent or solute of a liquid
  solution. We replace the left side of Eq. <reference|mu_i(mixt)=mu_i(g)>
  with an expression from among Eqs. <reference|act coeff,
  solvent>\U<reference|act coeff m,B>, then derive an expression analogous to
  Eq. <reference|gamma_i=C f_i/x_i> for the activity coefficient with a
  composition-independent factor, and finally apply the limiting conditions
  that cause the activity coefficient to approach unity (Eqs.
  <reference|gamma(A)-\<gtr\>1>\U<reference|gamma(mB)-\<gtr\>1>) and allow us
  to evaluate the factor. When we take the limits that cause the solute
  activity coefficients to approach unity, the ratios <math|<fug><B>/x<B>>,
  <math|<fug><B>/c<B>>, and <math|<fug><B>/m<B>> become Henry's law constants
  (Eqs. <reference|KxB=lim(fB/xB)>\U<reference|KmB=lim(fB/mB>). The resulting
  expressions for activity coefficients as functions of fugacity are listed
  in Table <vpageref|tbl:9-act coeff-fugacity>.

  <subsubsection|Examples>

  Ethanol and water at <math|25 <degC>> mix in all proportions, so we can
  treat the liquid phase as a liquid mixture rather than a solution. A plot
  of ethanol fugacity versus mole fraction at fixed <math|T> and <math|p>,
  shown earlier in Fig. <reference|fig:9-ethanol fug>, is repeated in Fig.
  <reference|fig:9-ethanol act>(a) on page <pageref|fig:9-ethanol
  act>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/ETOH-AC.eps|351pt|157pt||>>
        <label|fig:9-ethanol act>Liquid mixtures of ethanol (A) and
        H<rsub|<math|2>>O at <math|25<degC>> and <math|1<br>>.

        <\enumerate-alpha>
          <item>Ethanol fugacity as a function of mixture composition. The
          dashed line is Raoult's law behavior, and the filled circle is the
          pure-liquid reference state.

          <item>Ethanol activity coefficient as a function of mixture
          composition.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float><float|float|thb|<\big-table>
    <\equation*>
      <bktable3|<tformat|||<cwith|2|7|2|2|cell-halign|L=>|||||||<table|<row|<cell|<text|Substance>>|<cell|<text|Activity
      coefficient>>>|<row|<cell|<text|Substance <math|i> in gas
      mixture>>|<cell|\<phi\><rsub|i>=<dfrac|<fug><rsub|i>|p<rsub|i>>>>|<row|<cell|<text|Substance
      <math|i> in a liquid or solid mixture>>|<cell|<g><rsub|i>=<dfrac|<fug><rsub|i>|x<rsub|i>*<fug><rsub|i><rsup|\<ast\>>>>>|<row|<cell|<text|Solvent
      A of a solution>>|<cell|<g><A>=<dfrac|<fug><A>|x<A>*<fug><A><rsup|\<ast\>>>>>|<row|<cell|<text|Solute
      B, mole fraction basis>>|<cell|<g><xbB>=<dfrac|<fug><B>|<kHB>*x<B>>>>|<row|<cell|<text|Solute
      B, concentration basis>>|<cell|<g><cbB>=<dfrac|<fug><B>|k<cbB>*c<B>>>>|<row|<cell|<text|Solute
      B, molality basis>>|<cell|<g><mbB>=<dfrac|<fug><B>|k<mbB>*m<B>>>>>>>
    </equation*>

    \;
  <|big-table>
    <label|tbl:9-act coeff-fugacity>Activity coefficients as functions of
    fugacity. For a constituent of a condensed-phase mixture,
    <math|<fug><rsub|i>>, <math|<fug><A>>, and <math|<fug><B>> refer to the
    fugacity in a gas phase equilibrated with the condensed phase.
  </big-table>>

  \ Ethanol is component A. In the figure, the filled circle is the
  pure-liquid reference state at <math|x<A|=>1> where <math|f<A>> is equal to
  <math|f<A><rsup|\<ast\>>>. The open circles at <math|x<A>=0.4> indicate
  <math|f<A>>, the actual fugacity in a gas phase equilibrated with a liquid
  mixture of this composition, and <math|x<A>*f<A><rsup|\<ast\>>>, the
  fugacity the ethanol would have if the mixture were ideal and component A
  obeyed Raoult's law. The ratio of these two quantities is the activity
  coefficient <math|<g><A>>.

  Figure <reference|fig:9-ethanol act>(b) shows how <math|<g><A>> varies with
  composition. The open circle is at <math|x<A>=0.4> and
  <math|<g><A>=<fug><A>/<around|(|x<A>*<fug><A><rsup|\<ast\>>|)>>. Note how
  <math|<g><A>> approaches <math|1> as <math|x<A>> approaches <math|1>, as it
  must according to Eq. <reference|ac_i -\<gtr\> 1>.

  Water and 1-butanol are two liquids that do not mix in all proportions;
  that is, 1-butanol has limited solubility in water. Figures
  <reference|fig:9-H2O-BuOH>(a) and <reference|fig:9-H2O-BuOH>(b) on page
  <pageref|fig:9-H2O-BuOH>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/H2O-BuOH.eps|279pt|271pt||>>
        <label|fig:9-H2O-BuOH>Dilute aqueous solutions of 1-butanol (B) at
        <math|50.08 <degC>> and <math|1<br>>.<note-ref|+pnwft35dEGUGed>

        <\enumerate-alpha>
          <item><math|<fug><B>> in an equilibrated gas phase as a function of
          <math|x<B>>, measured up to the solubility limit at
          <math|x<B>=0.015>. The dilute region is shown in a magnified view.
          Dashed line: Henry's law behavior on a mole fraction basis. Filled
          circle: solute reference state based on mole fraction.

          <item><math|<fug><B>> as a function of <math|m<B>>, measured up to
          the solubility limit at <math|m<B>=0.85
          <text|mol>\<cdot\><text|kg><rsup|-1>>. Dashed line: Henry's law
          behavior on a molality basis. Filled circle: solute reference state
          on this basis.

          <item>Activity coefficient on a mole fraction basis as a function
          of <math|x<B>>.

          <item>Activity coefficient on a molality basis as a function of
          <math|m<B>>.
        </enumerate-alpha>

        \;

        <note-inline||+pnwft35dEGUGed>Based on data in Ref. <cite|fischer-94>
      </big-figure>
    </framed>
  </float> show the fugacity of 1-butanol plotted as functions of both mole
  fraction and molality. The figures demonstrate how, treating 1-butanol as a
  solute, we locate the solute reference state by a linear extrapolation of
  the fugacity to the standard composition. The fugacity <math|<fug><B>> is
  quite different in the two reference states. At the reference state
  indicated by a filled circle in Fig. <reference|fig:9-H2O-BuOH>(a),
  <math|f<B>> equals the Henry's law constant <math|<kHB>>; at the reference
  state in Fig. <reference|fig:9-H2O-BuOH>(b), <math|f<B>> equals
  <math|k<mbB>m<st>>. Note how the activity coefficients plotted in Figs.
  <reference|fig:9-H2O-BuOH>(c) and <reference|fig:9-H2O-BuOH>(d) approach
  <math|1> at infinite dilution, in agreement with Eqs.
  <reference|gamma(xB)-\<gtr\>1> and <reference|gamma(mB)-\<gtr\>1>, and how
  they vary as a linear function of <math|x<B>> or <math|m<B>> in the dilute
  solution as predicted by the theoretical argument of Sec.
  <reference|9-nonideal dil solns>.<index-complex|<tuple|activity
  coefficient|solvent|gas fugacity>||c9 sec-eac-gas-fugacity
  idx1|<tuple|Activity coefficient|of a solvent|from gas
  fugacity>><index-complex|<tuple|activity coefficient|solute|gas
  fugacity>||c9 sec-eac-gas-fugacity idx2|<tuple|Activity coefficient|of a
  solute|from gas fugacity>>

  <subsection|Activity coefficients from the Gibbs\UDuhem equation><label|c9
  sec-eac-gibbs-duhem>

  <index-complex|<tuple|activity coefficient|gibb-duhem>||c9
  sec-eac-gibbs-duhem idx1|<tuple|Activity coefficient|from the Gibbs\UDuhem
  equation>><index-complex|<tuple|gibbs-duhem equation>||c9
  sec-eac-gibbs-duhem idx2|<tuple|Gibbs\UDuhem equation>>If component B of a
  binary liquid mixture has low volatility, it is not practical to use its
  fugacity in a gas phase to evaluate its activity coefficient. If, however,
  component A is volatile enough for fugacity measurements over a range of
  liquid composition, we can instead use the Gibbs\UDuhem equation for this
  purpose.

  Consider a binary mixture of two liquids that mix in all proportions. We
  assume that only component A is appreciably volatile. By measuring the
  fugacity of A in a gas phase equilibrated with the binary mixture, we can
  evaluate its activity coefficient based on a pure-liquid reference state:
  <math|<g><A>=<fug><A>/<around|(|x<A>*<fug><A><rsup|\<ast\>>|)>> (Table
  <reference|tbl:9-act coeff-fugacity>). We wish to use the same fugacity
  measurements to determine the activity coefficient of the nonvolatile
  component, B.

  The Gibbs\UDuhem equation for a binary liquid mixture in the form given by
  Eq. <reference|sum(x_i)dmu_i=0> is

  <\equation>
    <label|GD-binary>x<A>*<dif>\<mu\><A>+x<B>*<dif>\<mu\><B>=0
  </equation>

  where <math|<dif>\<mu\><A>> and <math|<dif>\<mu\><B>> are the chemical
  potential changes accompanying a change of composition at constant <math|T>
  and <math|p>. Taking the differential at constant <math|T> and <math|p> of
  <math|\<mu\><A>=\<mu\><A><rsup|\<ast\>>+R*T*ln <around|(|<g><A>*x<A>|)>>
  (Eq. <reference|act coeff, mixt>), we obtain

  <\equation>
    <dif>\<mu\><A>=R*T*<dif>ln <g><A>+R*T*<dif>ln x<A>=R*T*<dif>ln
    <g><A>+<frac|R*T|x<A>>*<dx><A>
  </equation>

  For component B, we obtain in the same way

  <\equation>
    <dif>\<mu\><B>=R*T*<dif>ln <g><B>+<frac|R*T|x<B>>*<dx><B>=R*T*<dif>ln
    <g><B>-<frac|R*T|x<B>>*<dx><A>
  </equation>

  Substituting these expressions for <math|<dif>\<mu\><A>> and
  <math|<dif>\<mu\><B>> in Eq. <reference|GD-binary> and solving for
  <math|<dif>ln <g><B>>, we obtain the following relation:

  <\equation>
    <dif>ln <g><B>=-<frac|x<A>|x<B>>*<dif>ln <g><A>
  </equation>

  Integration from <math|x<B>=1>, where <math|<g><B>> equals <math|1> and
  <math|ln <g><B>> equals <math|0>, to composition <math|x<B><rprime|'>>
  gives

  <\equation-cov2|<label|ln(ac_B)=int...>ln
  <g><B><around|(|x<B><rprime|'>|)>=-<big|int><rsub|x<B>=1><rsup|x<B>=x<B><rprime|'>><frac|x<A>|x<B>>*<dif>ln
  <g><A>>
    (binary mixture,

    constant <math|T> and <math|p>)
  </equation-cov2>

  Equation <reference|ln(ac_B)=int...> allows us to evaluate the activity
  coefficient of the nonvolatile component, B, at any given liquid
  composition from knowledge of the activity coefficient of the volatile
  component A as a function of composition.

  Next consider a binary liquid mixture in which component B is neither
  volatile nor able to mix in all proportions with A. In this case, it is
  appropriate to treat B as a solute and to base its activity coefficient on
  a solute reference state. We could obtain an expression for <math|ln
  <g><xbB>> similar to Eq. <reference|ln(ac_B)=int...>, but the integration
  would have to start at <math|x<B|=>0> where the integrand <math|x<A>/x<B>>
  would be infinite. Instead, it is convenient in this case to use the method
  described in the next section.<index-complex|<tuple|activity
  coefficient|gibb-duhem>||c9 sec-eac-gibbs-duhem idx1|<tuple|Activity
  coefficient|from the Gibbs\UDuhem equation>><index-complex|<tuple|gibbs-duhem
  equation>||c9 sec-eac-gibbs-duhem idx2|<tuple|Gibbs\UDuhem equation>>

  <subsection|Activity coefficients from osmotic coefficients><label|9-act
  coeffs from osmotic coeffs><label|c9 sec-eac-osmotic>

  <index-complex|<tuple|activity coefficient|osmotic coefficient>||c9
  sec-eac-osmotic idx1|<tuple|Activity coefficient|from the osmotic
  coefficient>>It is customary to evaluate the activity coefficient of a
  nonvolatile solute with a function <math|\<phi\><rsub|m>> called the
  <index|Osmotic coefficient><newterm|osmotic coefficient>, or osmotic
  coefficient on a molality basis. The osmotic coefficient of a solution of
  nonelectrolyte solutes is defined by

  <equation-cov2|<label|phi(m) def>\<phi\><rsub|m><defn><frac|\<mu\><A><rsup|\<ast\>>-\<mu\><A>|R*T*M<A>*<big|sum><rsub|i\<ne\><text|A>>m<rsub|i>>|(nonelectrolyte
  solution)>

  <\quote-env>
    \ The definition of <math|\<phi\><rsub|m>> in Eq. <reference|phi(m) def>
    has the following significance. The sum
    <math|<big|sum><rsub|i\<ne\><text|A>>m<rsub|i>> is the total molality of
    all solute species. In an ideal-dilute solution, the solvent chemical
    potential is <math|\<mu\><A>=\<mu\><A><rsup|\<ast\>>+R*T*ln x<A>>. The
    expansion of the function <math|ln x<A>> in powers of
    <math|<around|(|1-x<A>|)>> gives the power series <math|ln
    x<A>=-<around|(|1-x<A>|)>-<around|(|1-x<A>|)><rsup|2>/2-<around|(|1-x<A>|)><rsup|3>/3-\<cdots\>>.
    Thus, in a very dilute solution we have <math|ln
    x<A>\<approx\>-<around|(|1-x<A>|)>=-<big|sum><rsub|i\<ne\><text|A>>x<rsub|i>>.
    In the limit of infinite dilution, the mole fraction of solute <math|i>
    becomes <math|x<rsub|i>=M<A>*m<rsub|i>> (see Eq. <reference|nB/nA
    (dilute)>). In the limit of infinite dilution, therefore, we have

    <equation-cov2|<label|ln(xA)=-MAsum(mi)>ln
    x<A>=-M<A>*<big|sum><rsub|i\<ne\><text|A>>m<rsub|i>|(infinite dilution)>

    and the solvent chemical potential is related to solute molalities by

    <equation-cov2|\<mu\><A>=\<mu\><A><rsup|\<ast\>>-R*T*M<A>*<big|sum><rsub|i\<ne\><text|A>>m<rsub|i>|(infinite
    dilution)>

    The deviation of <math|\<phi\><rsub|m>> from unity is a measure of the
    deviation of <math|\<mu\><A>> from infinite-dilution behavior, as we can
    see by comparing the preceding equation with a rearrangement of Eq.
    <reference|phi(m) def>:

    <\equation>
      \<mu\><A>=\<mu\><A><rsup|\<ast\>>-\<phi\><rsub|m>*R*T*M<A>*<big|sum><rsub|i\<ne\><text|A>>m<rsub|i>
    </equation>

    The reason <math|\<phi\><rsub|m>> is called the osmotic coefficient has
    to do with its relation to the osmotic pressure <math|<varPi>> of the
    solution: The ratio <math|<varPi>/m<B>> is equal to the product of
    <math|\<phi\><rsub|m>> and the limiting value of <math|<varPi>/m<B>> at
    infinite dilution (see Sec. <reference|12-osmotic pressure>).
  </quote-env>

  <subsubsection|Evaluation of <math|\<phi\><rsub|m>>>

  Any method that measures <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>>, the
  lowering of the solvent chemical potential caused by the presence of a
  solute or solutes, allows us to evaluate <math|\<phi\><rsub|m>> through Eq.
  <reference|phi(m) def>.

  The chemical potential of the solvent in a solution is related to the
  fugacity in an equilibrated gas phase by
  <math|\<mu\><A>=\<mu\><A><rf><gas>+R*T*ln <around|(|<fug><A>/p|)>> (from
  Eq. <reference|mu_i-mu_i(ref)(g)=>). For the pure solvent, this relation is
  <math|\<mu\><A><rsup|\<ast\>>=\<mu\><A><rf><gas>+R*T*ln
  <around|(|<fug><A><rsup|\<ast\>>/p|)>>. Taking the difference between these
  two equations, we obtain

  <\equation>
    \<mu\><A><rsup|\<ast\>>-\<mu\><A>=R*T*ln
    <frac|<fug><A><rsup|\<ast\>>|<fug><A>>
  </equation>

  which allows us to evaluate <math|\<phi\><rsub|m>> from fugacity
  measurements.

  Osmotic coefficients can also be evaluated from freezing point and osmotic
  pressure measurements that will be described in Sec. <reference|12-solvent
  mu from phase eq>.

  <subsubsection|Use of <math|\<phi\><rsub|m>>>

  Suppose we have a solution of a nonelectrolyte solute B whose activity
  coefficient <math|<g><mbB>> we wish to evaluate as a function of
  <math|m<B>>. For a binary solution, Eq. <reference|phi(m) def> becomes

  <equation-cov2|<label|phi(m)=(muA*-muA)/RTM(A)mB>\<phi\><rsub|m>=<frac|\<mu\><A><rsup|\<ast\>>-\<mu\><A>|R*T*M<A>*m<B>>|(binary
  nonelectrolyte solution)>

  Solving for <math|\<mu\><A>> and taking its differential at constant
  <math|T> and <math|p>, we obtain

  <\equation>
    <dif>\<mu\><A>=-R*T*M<A>*<dif><around|(|\<phi\><rsub|m>*m<B>|)>=-R*T*M<A>*<around|(|\<phi\><rsub|m>*<dif>m<B>+m<B>*<dif>\<phi\><rsub|m>|)>
  </equation>

  From <math|\<mu\><B>=\<mu\><mbB><rf>+R*T*ln
  <around|(|<g><mbB>*m<B>/m<st>|)>> (Eq. <reference|act coeff m,B>), we
  obtain

  <\equation>
    <dif>\<mu\><B>=R*T*<dif>ln <frac|<g><mbB>*m<B>|m<st>>=R*T*<around*|(|<dif>ln
    <g><mbB>+<frac|<dif>m<B>|m<B>>|)>
  </equation>

  We substitute these expressions for <math|<dif>\<mu\><A>> and
  <math|<dif>\<mu\><B>> in the <index|Gibbs--Duhem equation>Gibbs\UDuhem
  equation in the form given by Eq. <reference|sum(n_i)dX_i=0>,
  <math|n<A>*<dif>\<mu\><A>+n<B>*<dif>\<mu\><B>=0>, make the substitution
  <math|n<A>*M<A>=n<B>/m<B>>, and rearrange to

  <\equation>
    <dif>ln <g><mbB>=<dif>\<phi\><rsub|m>+<frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>
  </equation>

  We integrate both sides of this equation for a composition change at
  constant <math|T> and <math|p> from <math|m<B>=0> (where <math|ln x<B>> is
  <math|0> and <math|\<phi\><rsub|m>> is <math|1>) to any desired molality
  <math|m<rprime|'><B>>, with the result

  <\equation-cov2|<label|ln gamma(mB)=>ln
  <g><mbB><around|(|m<rprime|'><B>|)>=\<phi\><rsub|m><around|(|m<rprime|'><B>|)>-1+<big|int><rsub|0><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>>
    (binary

    nonelectrolyte solution)
  </equation-cov2>

  When the solute is a nonelectrolyte, the integrand
  <math|<around|(|\<phi\><rsub|m>-1|)>/m<B>> is found to be a slowly varying
  function of <math|m<B>> and to approach a finite value as <math|m<B>>
  approaches zero.

  Once <math|\<phi\><rsub|m>> has been measured as a function of molality
  from zero up to the molality of interest, Eq. <reference|ln gamma(mB)=> can
  be used to evaluate the solute activity coefficient <math|<g><mbB>> at that
  molality.

  Figure <reference|fig:9-aq sucrose>(a) on page <pageref|fig:9-aq
  sucrose><\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/SUCROSE.eps|362pt|198pt||>>
        <label|fig:9-aq sucrose>Aqueous sucrose solutions at <math|25
        <degC>>.

        <\enumerate-alpha>
          <item>Integrand of the integral in Eq. <reference|ln gamma(mB)=> as
          a function of solution composition.

          <item>Solute activity coefficient on a molality basis.

          <item>Product of activity coefficient and molality as a function of
          composition. The dashed line is the extrapolation of ideal-dilute
          behavior.
        </enumerate-alpha>

        \;

        <note-inline|Based on data in Ref. <cite|robinson-59>, Appendix
        8.6.|+pnwft35dEGUGee>
      </big-figure>
    </framed>
  </float> shows the function <math|<around|(|\<phi\><rsub|m>-1|)>/m<B>> for
  aqueous sucrose solutions over a wide range of molality. The dependence of
  the solute activity coefficient on molality, generated from Eq.
  <reference|ln gamma(mB)=>, is shown in Fig. <reference|fig:9-aq
  sucrose>(b). Figure <reference|fig:9-aq sucrose>(c) is a plot of the
  effective sucrose molality <math|<g><mbB>m<B>> as a function of
  composition. Note how the activity coefficient becomes greater than unity
  beyond the ideal-dilute region, and how in consequence the effective
  molality <math|<g><mbB>m<B>> becomes considerably greater than the actual
  molality <math|m<B>>.<index-complex|<tuple|activity coefficient|osmotic
  coefficient>||c9 sec-eac-osmotic idx1|<tuple|Activity coefficient|from the
  osmotic coefficient>>

  \;

  <subsection|Fugacity measurements><label|9-fugacity measurements><label|c9
  sec eac-fugacity-measurements>

  Section <reference|9-act coeff from fug> described the evaluation of the
  activity coefficient of a constituent of a liquid mixture from its fugacity
  in a gas phase equilibrated with the mixture. Section <reference|9-act
  coeffs from osmotic coeffs> mentioned the use of solvent fugacities in gas
  phases equilibrated with pure solvent and with a solution, in order to
  evaluate the osmotic coefficient of the solution.

  Various experimental methods are available for measuring a partial pressure
  in a gas phase equilibrated with a liquid mixture. A correction for gas
  nonideality, such as that given by Eq. <reference|ln(f_i/p_i)=int(V_i/RT-1/p)dp>,
  can be used to convert the partial pressure to fugacity.

  If the solute of a solution is nonvolatile, we may pump out the air above
  the solution and use a manometer to measure the pressure, which is the
  partial pressure of the solvent. Dynamic methods involve passing a stream
  of inert gas through a liquid mixture and analyzing the gas mixture to
  evaluate the partial pressures of volatile components. For instance, we
  could pass dry air successively through an aqueous solution and a desiccant
  and measure the weight gained by the desiccant.

  The <subindex|Isopiestic|vapor pressure technique><newterm|isopiestic vapor
  pressure technique> is one of the most useful methods for determining the
  fugacity of H<rsub|<math|2>>O in a gas phase equilibrated with an aqueous
  solution. This is a comparative method using a binary solution of the
  solute of interest, B, and a nonvolatile reference solute of known
  properties. Some commonly used reference solutes for which data are
  available are sucrose, NaCl, and CaCl<rsub|<math|2>>.

  In this method, solute B can be either a nonelectrolyte or electrolyte.
  Dishes, each containing water and an accurately weighed sample of one of
  the solutes, are placed in wells drilled in a block made of metal for good
  thermal equilibration. The assembly is placed in a gas-tight chamber, the
  air is evacuated, and the apparatus is gently rocked in a thermostat for a
  period of up to several days, or even weeks. During this period,
  H<rsub|<math|2>>O is transferred among the dishes through the vapor space
  until the chemical potential of the water becomes the same in each
  solution. The solutions are then said to be
  <subindex|Isopiestic|solution><em|isopiestic>. Finally, the dishes are
  removed from the apparatus and weighed to establish the molality of each
  solution. The H<rsub|<math|2>>O fugacity is known as a function of the
  molality of the reference solute, and is the same as the H<rsub|<math|2>>O
  fugacity in equilibrium with the solution of solute B at its measured
  molality.

  The isopiestic vapor pressure method can also be used for nonaqueous
  solutions. <subindex|Osmotic coefficient|evaluation>

  <\bio-insert>
    <include|bio-LEWIS.tm>
  </bio-insert>

  <page-break>

  <section|Activity of an Uncharged Species><label|9-activities><label|c9 sec
  aus>

  The <index|Activity><newterm|activity> <math|a<rsub|i>> of uncharged
  species <math|i> (i.e., a substance) is defined by the relation

  <equation-cov2|<label|a_i=exp(mu_i-mu_io)/RT>a<rsub|i><defn>exp
  <around*|(|<frac|\<mu\><rsub|i>-\<mu\><rsub|i><st>|R*T>|)>|(uncharged
  species)>

  or

  <equation-cov2|<label|mu_i=mu_io+RTln(a_i)>\<mu\><rsub|i>=\<mu\><rsub|i><st>+R*T*ln
  a<rsub|i>|(uncharged species)>

  where <math|\<mu\><rsub|i><st>> is the <subindex|Chemical
  potential|standard>standard chemical potential of the
  species.<footnote|Some chemists define the activity by
  <math|\<mu\><rsub|i>=\<mu\><rsub|i><rf>+R*T*ln a<rsub|i>>. The activity
  defined this way is not the same as the activity used in this book unless
  the phase is at the standard pressure.> The activity of a species in a
  given phase is a dimensionless quantity whose value depends on the choice
  of the standard state and on the intensive properties of the phase:
  temperature, pressure, and composition.

  The quantity <math|a<rsub|i>> is sometimes called the <index|Relative
  activity><subindex|Activity|relative><em|relative activity> of <math|i>,
  because it depends on the chemical potential relative to a standard
  chemical potential. An important application of the activity concept is the
  definition of equilibrium constants (Sec. <reference|11-eq constant defn>).

  For convenience in later applications, we specify that the value of
  <math|a<rsub|i>> is the same<label|a_i indep of h and phi>in phases that
  have the same temperature, pressure, and composition but are at different
  elevations in a gravitational field, or are at different electric
  potentials. Section <reference|9-mixtures in grav \ centrif fields>
  <reference|10-single ion quantities> will describe a modification of the
  defining equation <math|\<mu\><rsub|i>=\<mu\><rsub|i><st>+R*T*ln a<rsub|i>>
  for a system with phases of different elevations, and Sec.
  <reference|10-single ion quantities> will describe the modification needed
  for a charged species.

  <subsection|Standard states><label|9-mixt st states><label|c9 sec
  aus-std-states>

  <index-complex|<tuple|standard state|mixture component>|||<tuple|Standard
  state|of a mixture component>>The standard states of different kinds of
  mixture components have the same definitions as those for reference states
  (Table <reference|tbl:9-st states>), with the additional stipulation in
  each case that the pressure is equal to the standard pressure <math|p<st>>.

  When component <math|i> is in its standard state, its chemical potential is
  the standard chemical potential <math|\<mu\><st><rsub|i>>. It is important
  to note from Eq. <reference|mu_i=mu_io+RTln(a_i)> that when
  <math|\<mu\><rsub|i>> equals <math|\<mu\><rsub|i><st>>, the logarithm of
  <math|a<rsub|i>> is zero and the activity in the standard state is
  therefore unity.

  The following equations in the form of Eq. <reference|mu_i=mu_io+RTln(a_i)>
  show the notation used in this book for the standard chemical potentials
  and activities of various kinds of uncharged mixture components:

  <\eqnarray*>
    <tformat|<cwith|1|-1|2|2|cell-halign|c>|<table|<row|<cell|<hspace|4fn><text|Substance
    <math|i> in a gas mixture><htab|5mm>\<mu\><rsub|i>>|<cell|=>|<cell|\<mu\><rsub|i><st><gas>+R*T*ln
    a<rsub|i><gas><eq-number><label|act gas>>>|<row|<cell|<hspace|4fn><text|Substance
    <math|i> in a liquid or solid mixture><htab|5mm>\<mu\><rsub|i>>|<cell|=>|<cell|\<mu\><rsub|i><st>+R*T*ln
    a<rsub|i><eq-number><label|act mixt>>>|<row|<cell|<hspace|4fn><text|Solvent
    A of a solution><htab|5mm>\<mu\><A>>|<cell|=>|<cell|\<mu\><A><st>+R*T*ln
    a<A><eq-number><label|act solvent>>>|<row|<cell|<hspace|4fn><text|Solute
    B, mole fraction basis><htab|5mm>\<mu\><B>>|<cell|=>|<cell|\<mu\><xbB><st>+R*T*ln
    a<xbB><eq-number><label|act x,B>>>|<row|<cell|<hspace|4fn><text|Solute B,
    concentration basis><htab|5mm>\<mu\><B>>|<cell|=>|<cell|\<mu\><cbB><st>+R*T*ln
    a<cbB><eq-number><label|act c,B>>>|<row|<cell|<hspace|4fn><text|Solute B,
    molality basis><htab|5mm>\<mu\><B>>|<cell|=>|<cell|\<mu\><mbB><st>+R*T*ln
    a<mbB><eq-number><label|act m,B>>>>>
  </eqnarray*>

  <subsection|Activities and composition><label|c9
  sec-aus-activities-composition>

  We need to be able to relate the activity of component <math|i> to the
  mixture composition. We can do this by finding the relation between the
  chemical potential of component <math|i> in its reference state and in its
  standard state, both at the same temperature. These two chemical
  potentials, <math|\<mu\><rsub|i><rf>> and <math|\<mu\><rsub|i><st>>, are
  equal only if the mixture is at the standard pressure <math|p<st>>.

  It will be useful to define the following dimensionless
  quantity:<index|Pressure factor>

  <\equation>
    <label|Gamma defn><G><rsub|i><defn>exp
    <around*|(|<frac|\<mu\><rsub|i><rf>-\<mu\><rsub|i><st>|R*T>|)>
  </equation>

  The symbol <math|<G><rsub|i>> for this quantity was introduced by
  <index|Pitzer, Kenneth>Pitzer and <index|Brewer, Leo>Brewer.<footnote|Ref.
  <cite|lewis-61>, p. 249.> They called it <em|the activity in a reference
  state>. To see why, compare the definition of activity given by
  <math|\<mu\><rsub|i>=\<mu\><rsub|i><st>+R*T*ln a<rsub|i>> with a
  rearrangement of Eq. <reference|Gamma defn>:
  <math|\<mu\><rsub|i><rf>=\<mu\><rsub|i><st>+R*T*ln <G><rsub|i>>.

  At a given temperature, the difference <math|\<mu\><rsub|i><rf>-\<mu\><rsub|i><st>>
  depends only on the pressure <math|p> of the mixture, and is zero when
  <math|p> is equal to <math|p<st>>. Thus <math|<G><rsub|i>> is a function of
  <math|p> with a value of 1 when <math|p> is equal to <math|p<st>>. This
  book will call <math|<G><rsub|i>> the <index|Pressure
  factor><newterm|pressure factor> of species <math|i>.

  To understand how activity is related to composition, let us take as an
  example the activity <math|a<mbB>> of solute B based on molality. From Eqs.
  <reference|act coeff m,B> and <reference|act m,B>, we have

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<mu\><B>>|<cell|=>|<cell|\<mu\><mbB><rf>+R*T*ln
    <around*|(|<g><mbB><frac|m<B>|m<st>>|)>>>|<row|<cell|>|<cell|=>|<cell|\<mu\><mbB><st>+R*T*ln
    a<mbB><eq-number>>>>>
  </eqnarray*>

  The activity is then given by

  <\eqnarray*>
    <tformat|<table|<row|<cell|ln a<mbB>>|<cell|=>|<cell|<frac|\<mu\><mbB><rf>-\<mu\><mbB><st>|R*T>+ln
    <around*|(|<g><mbB><frac|m<B>|m<st>>|)>>>|<row|<cell|>|<cell|=>|<cell|ln
    <G><mbB>+ln <around*|(|<g><mbB><frac|m<B>|m<st>>|)><eq-number>>>>>
  </eqnarray*>

  <\equation>
    a<mbB>=<G><mbB><space|0.17em><g><mbB><frac|m<B>|m<st>><space|2.28cm>
  </equation>

  The activity of a constituent of a condensed-phase mixture is in general
  equal to the product of the pressure factor, the activity coefficient, and
  the composition variable divided by the standard composition.

  Table <vpageref|tbl:9-activities> gives explicit expressions for the
  activities of various kinds of nonelectrolyte
  substances.<index-complex|<tuple|activity|gas>|||<tuple|Activity|of a
  gas>><index-complex|<tuple|activity|pure liquid or
  solid>|||<tuple|Activity|of a pure liquid or
  solid>><index-complex|<tuple|activity|mixture
  constituent>|||<tuple|Activity|of a mixture
  constituent>><index-complex|<tuple|activity|solvent>|||<tuple|Activity|of a
  solvent>><index-complex|<tuple|activity|solute>|||<tuple|Activity|of a
  solute>><float|float|thb|<\big-table>
    <\equation*>
      <bktable3|<tformat|<cwith|2|-1|2|2|cell-halign|L=>|||||||||<table|<row|<cell|<text|Substance>>|<cell|<text|Activity>>>|<row|<cell|<text|Pure
      gas>>|<cell|a<gas>=<G><gas>*\<phi\>=<frac|<fug>|p<st>>>>|<row|<cell|<text|Pure
      liquid or solid>>|<cell|a=<G>>>|<row|<cell|<text|Substance <math|i> in
      a gas mixture>>|<cell|a<rsub|i><gas>=<G><rsub|i><gas>*\<phi\><rsub|i>*<frac|p<rsub|i>|p>=<frac|<fug><rsub|i>|p<st>>>>|<row|<cell|<text|Substance
      <math|i> in a liquid or solid mixture>>|<cell|a<rsub|i>=<G><rsub|i>*<g><rsub|i>*x<rsub|i>=<G><rsub|i>*<frac|<fug><rsub|i>|<fug><rsub|i><rsup|\<ast\>>>>>|<row|<cell|<text|Solvent
      A of a solution>>|<cell|a<A>=<G><A>*<g><A>*x<A>=<G><A>*<frac|<fug><A>|<fug><A><rsup|\<ast\>>>>>|<row|<cell|<text|Solute
      B, mole fraction basis>>|<cell|a<xbB>=<G><xbB>*<g><xbB>*x<B>=<G><xbB>*<frac|<fug><B>|<kHB>>>>|<row|<cell|<text|Solute
      B, concentration basis>>|<cell|a<cbB>=<G><cbB>*<g><cbB>*<frac|c<B>|c<st>>=<G><cbB>*<frac|<fug><B>|k<cbB>*c<st>>>>|<row|<cell|<text|Solute
      B, molality basis>>|<cell|a<mbB>=<G><mbB>*<g><mbB>*<frac|m<B>|m<st>>=<G><mbB>*<frac|<fug><B>|k<mbB>*m<st>>>>>>>
    </equation*>

    \;
  <|big-table>
    <label|tbl:9-activities>Expressions for activities of nonelectrolytes.
    For a constituent of a condensed-phase mixture, <math|<fug><rsub|i>>,
    <math|<fug><A>>, and <math|<fug><B>> refer to the fugacity in a gas phase
    equilibrated with the condensed phase.
  </big-table>>

  <subsection|Pressure factors and pressure><label|c9 sec aus-pressure>

  <index-complex|<tuple|pressure factor>||c9 sec aus-pressure
  idx1|<tuple|Pressure factor>>At a given temperature, the pressure factor
  <math|<G><rsub|i>> of component <math|i> of a mixture is a function only of
  pressure. To derive the pressure dependence of <math|<G><rsub|i>> for
  various kinds of mixture components, we need expressions for
  <math|<around|(|\<mu\><rsub|i><rf>-\<mu\><rsub|i><st>|)>> as functions of
  pressure to substitute in the defining equation <math|<G><rsub|i>=exp
  <space|0.17em><around|[|<space|0.17em><around|(|\<mu\><rsub|i><rf>-\<mu\><rsub|i><st>|)>/R*T|]>>.

  For component <math|i> of a <em|gas mixture>, the reference state is pure
  gas <math|i> at the pressure of the mixture, behaving as an ideal gas. The
  chemical potential of a pure ideal gas depends on its pressure according to
  Eq. <reference|mu=muo(g)+RT*ln(p/po)>: <math|\<mu\>=\<mu\><st><gas>+R*T*ln
  <space|0.17em><around|(|p/p<st>|)>>. Thus the chemical potential of the
  reference state of gas component <math|i> is
  <math|\<mu\><rsub|i><rf><gas>=\<mu\><rsub|i><st><gas>+R*T*ln
  <space|0.17em><around|(|p/p<st>|)>>, and
  <math|\<mu\><rsub|i><rf><gas|->\<mu\><rsub|i><st><gas>> is equal to
  <math|R*T*ln <space|0.17em><around|(|p/p<st>|)>>. This gives us the
  following expression for the pressure dependence of the pressure factor:

  <\equation>
    <G><rsub|i><gas>=<frac|p|p<st>>
  </equation>

  For a mixture in a <em|condensed phase>, we will make use of
  <math|<pd|\<mu\><rsub|i>|p|T,<allni>>=V<rsub|i>> (Eq.
  <reference|d(mu_i)/dp=V_i>). The relation between changes of
  <math|\<mu\><rsub|i>> and <math|p> at constant temperature and composition
  is therefore <math|<dif>\<mu\><rsub|i>=V<rsub|i><difp>>. Recall (Sec.
  <reference|9-composition of a mixture>) that \Pconstant composition\Q means
  that the mole fraction or molality of each component, but not necessarily
  the concentration, is constant.

  Consider a process in which the system initially consists of a phase with
  component <math|i> in its standard state. We change the pressure
  isothermally from <math|p<st>> to the pressure <math|p<rprime|'>> of the
  mixture of interest. For a pure-liquid, pure-solid, or solvent reference
  state, or a solute reference state based on mole fraction or molality, this
  process brings the system to the reference state of component <math|i> at
  pressure <math|p<rprime|'>>. The change of <math|\<mu\><rsub|i>> in this
  case is given by integration of <math|<dif>\<mu\><rsub|i>=V<rsub|i><difp>>:

  <\equation>
    <label|mu_i(ref)-mu_io=>\<mu\><rsub|i><rf><around|(|p<rprime|'>|)>-\<mu\><rsub|i><st>=<big|int><rsub|p<st>><rsup|p<rprime|'>><space|-0.17em><space|-0.17em>V<rsub|i><difp>
  </equation>

  The appropriate partial molar volume <math|V<rsub|i>> is the molar volume
  <math|V<rsub|i><rsup|\<ast\>>> or <math|V<A><rsup|\<ast\>>> of the pure
  substance, or the partial molar volume <math|V<B><rsup|\<infty\>>> of
  solute B at infinite dilution.

  Suppose we want to use a reference state for solute B based on
  concentration. Because the isothermal pressure change involves a small
  change of volume, <math|c<B>> changes slightly during the process, so that
  the right side of Eq. <reference|mu_i(ref)-mu_io=> is not quite the correct
  expression for <math|\<mu\><cbB><rf><around|(|p<rprime|'>|)>-\<mu\><cbB><st>>.

  <\quote-env>
    We can derive a rigorous expression for
    <math|\<mu\><cbB><rf><around|(|p<rprime|'>|)>-\<mu\><cbB><st>> as
    follows. Consider an ideal-dilute solution of solute B at an arbitrary
    pressure <math|p>, with solute chemical potential given by
    <math|\<mu\><B>=\<mu\><cbB><rf>+R*T*ln <around|(|c<B>/c<st>|)>> (Table
    <reference|tbl:9-id dil sln>). From this equation we obtain

    <\equation>
      <label|dmuB/dp - id dil><Pd|\<mu\><B>|p|T,<allni>>=<Pd|\<mu\><cbB><rf>|p|T>+R*T<bPd|ln
      <around|(|c<B>/c<st>|)>|p|T,<allni>>
    </equation>

    The partial derivative <math|<pd|\<mu\><B>|p|T,<allni>>> is equal to the
    partial molar volume <math|V<B>> (Eq. <reference|d(mu_i)/dp=V_i>), which
    in the ideal-dilute solution has its infinite-dilution value
    <math|V<B><rsup|\<infty\>>>. We rewrite the second partial derivative on
    the right side of Eq. <reference|dmuB/dp - id dil> as follows:

    <\eqnarray*>
      <tformat|<table|<row|<cell|<bPd|ln <around|(|c<B>/c<st>|)>|p|T,<allni>>>|<cell|=>|<cell|<frac|1|c<B>>*<Pd|c<B>|p|T,<allni>>=<frac|1|n<B>/V>*<bPd|<around|(|n<B>/V|)>|p|T,<allni>>>>|<row|<cell|>|<cell|=>|<cell|V*<bPd|<around|(|1/V|)>|p|T,<allni>>=-<frac|1|V>*<Pd|V|p|T,<allni>>>>|<row|<cell|>|<cell|=>|<cell|<kT><eq-number><label|dln(cB/co)/dp=kappa_T>>>>>
    </eqnarray*>

    Here <math|<kT>> is the isothermal compressibility of the solution, which
    at infinite dilution is <math|<kT><rsup|\<infty\>>>, the isothermal
    compressibility of the pure solvent. Equation <reference|dmuB/dp - id
    dil> becomes

    <\equation>
      V<B><rsup|\<infty\>>=<Pd|\<mu\><cbB><rf>|p|T>+R*T*<kT><rsup|\<infty\>>
    </equation>

    Solving for <math|<dif>\<mu\><cbB><rf>> at constant <math|T>, and
    integrating from <math|p<st>> to <math|p<rprime|'>>, we obtain finally

    <\equation>
      \<mu\><cbB><rf><around|(|p<rprime|'>|)>-\<mu\><cbB><st>=<big|int><rsub|p<st>><rsup|p<rprime|'>><space|-0.17em><space|-0.17em><around*|(|V<B><rsup|\<infty\>>-R*T*<kT><rsup|\<infty\>>|)>*<difp>
    </equation>
  </quote-env>

  We are now able to write explicit formulas for <math|<G><rsub|i>> for each
  kind of mixture component. They are collected in Table
  <vpageref|tbl:9-Gamma_i>.<float|float|thb|<\big-table>
    <\equation*>
      <bktable3|<tformat|<cwith|1|-1|1|-1|table-width|1par>|<cwith|1|-1|1|-1|table-hmode|exact>|||||||<cwith|2|6|2|2|cell-halign|L=>|<cwith|2|-1|1|1|cell-hyphen|c>|<cwith|2|-1|1|1|cell-width|0.4par>|<cwith|2|-1|1|1|cell-hmode|exact>|<table|<row|<cell|<text|Substance>>|<cell|<text|Pressure
      factor at pressure <math|p<rprime|'>>>>>|<row|<\cell>
        <text|Substance <math|i> in a gas mixture, or the pure gas>
      </cell>|<cell|<G><rsub|i><gas>=<frac|p<rprime|'>|p<st>>>>|<row|<\cell>
        <text|Substance <math|i> in a liquid or solid mixture, or the pure
        liquid or solid>
      </cell>|<cell|<G><rsub|i>=exp <around*|(|<big|int><rsub|p<st>><rsup|p<rprime|'>><space|-0.17em><frac|V<rsub|i><rsup|\<ast\>>|R*T>*<difp>|)>\<approx\>exp
      <around*|[|<frac|V<rsub|i><rsup|\<ast\>>*<around|(|p<rprime|'>-p<st>|)>|R*T>|]>>>|<row|<\cell>
        <text|Solvent A of a solution>
      </cell>|<cell|<G><A>=exp <around*|(|<big|int><rsub|p<st>><rsup|p<rprime|'>><space|-0.17em><frac|V<A><rsup|\<ast\>>|R*T>*<difp>|)>\<approx\>exp
      <around*|[|<frac|V<A><rsup|\<ast\>><around|(|p<rprime|'>-p<st>|)>|R*T>|]>>>|<row|<\cell>
        <text|Solute B, mole fraction or molality basis>
      </cell>|<cell|<G><xbB>=<G><mbB>=exp
      <around*|(|<big|int><rsub|p<st>><rsup|p<rprime|'>><space|-0.17em><frac|V<B><rsup|\<infty\>>|R*T>*<difp>|)>\<approx\>exp
      <around*|[|<frac|V<B><rsup|\<infty\>>*<around|(|p<rprime|'>-p<st>|)>|R*T>|]>>>|<row|<\cell>
        <text|Solute B, concentration basis>
      </cell>|<cell|<G><cbB>=exp <around*|[|<big|int><rsub|p<st>><rsup|p<rprime|'>><space|-0.17em><space|-0.17em><around*|(|<frac|V<B><rsup|\<infty\>>|R*T>-<kT><rsup|\<infty\>>|)>*<difp>|]>\<approx\>exp
      <around*|[|<frac|V<B><rsup|\<infty\>>*<around|(|p<rprime|'>-p<st>|)>|R*T>|]>>>>>>
    </equation*>

    <math|<with|math-display|true|>>
  <|big-table>
    <label|tbl:9-Gamma_i>Expressions for the dependence of pressure factors
    of nonelectrolytes on pressure. The approximate expressions assume the
    phase is incompressible, or the solute partial molar volume is
    independent of pressure.
  </big-table>>

  \ Considering a constituent of a condensed-phase mixture, by how much is
  the pressure factor likely to differ from unity? If we use the values
  <math|p<st>=1<br>> and <math|T=300<K>>, and assume the molar volume of pure
  <math|i> is <math|V<rsub|i><rsup|\<ast\>>=100
  <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>> at all pressures, we find
  that <math|<G><rsub|i>> is <math|0.996> in the limit of zero pressure,
  unity at <math|1<br>>, <math|1.004> at <math|2<br>>, <math|1.04> at
  <math|10<br>>, and <math|1.49> at <math|100<br>>. For a solution with
  <math|V<B><rsup|\<infty\>>=100 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>,
  we obtain the same values as these for <math|<G><xbB>>, <math|<G><mbB>>,
  and <math|<G><cbB>>. These values demonstrate that it is only at high
  pressures that the pressure factor differs appreciably from unity. For this
  reason, it is common to see expressions for activity in which this factor
  is omitted:<label|pressure factor omitted><math|a<rsub|i>=<g><rsub|i>*x<rsub|i>>,
  <math|a<mbB>=<g><mbB>*m<B>/m<st>>, and so on.

  <\quote-env>
    \ In principle, we can specify any convenient value for the
    <subindex|Standard|pressure><subindex|Pressure|standard>standard pressure
    <math|p<st>>. For a chemist making measurements at high pressures, it
    would be convenient to specify a value of <math|p<st>> within the range
    of the experimental pressures, for example <math|p<st>=1 <text|kbar>>, in
    order that the value of each pressure factor be close to unity.
  </quote-env>

  <index-complex|<tuple|pressure factor>||c9 sec aus-pressure
  idx1|<tuple|Pressure factor>>

  <section|Mixtures in Gravitational and Centrifugal Fields><label|9-mixtures
  in grav \ centrif fields><label|c9 sec-mgcf>

  A tall column of a gas mixture in a <subindex|Gravitational|field><subindex|Field|gravitational>gravitational
  field, and a liquid solution in the cell of a spinning
  <index|Centrifuge>centrifuge rotor, are systems with equilibrium states
  that are nonuniform in pressure and composition. This section derives the
  ways in which pressure and composition vary spatially within these kinds of
  systems at equilibrium.

  <subsection|Gas mixture in a gravitational field><label|9-gas mixt in grav
  field><label|c9 sec-mgcf-gas-gravity>

  <index-complex|<tuple|mixture|gas, in a gravitational field>||c9
  sec-mgcf-gas-gravity idx1|<tuple|Mixture|gas, in a gravitational
  field>><index-complex|<tuple|ideal gas|mixture|gravitational field>||c9
  sec-mgcf-gas-gravity idx2|<tuple|Ideal gas|mixture|in a gravitational
  field>><index-complex|<tuple|equilibrium conditions|gas mixture>||c9
  sec-mgcf-gas-gravity idx3|<tuple|Equilibrium conditions|for a gas mixture
  in a gravitational field>>Consider a tall column of a gas mixture in an
  earth-fixed <index|Lab frame><subindex|Frame|lab>lab frame. Our treatment
  will parallel that for a tall column of a pure gas in Sec. <reference|8-gas
  in gravity>. We imagine the gas to be divided into many thin slab-shaped
  phases at different elevations in a rigid container, as in Fig.
  <vpageref|fig:8-slabs>. We want to find the equilibrium conditions reached
  spontaneously when the system is isolated from its surroundings.

  The derivation is the same as that in Sec. <reference|9-eqm conditions>,
  with the additional constraint that for each phase <math|<pha>>,
  <math|<dif>V<aph>> is zero in order that each phase stays at a constant
  elevation. The result is the relation

  <\equation>
    <label|dS =(gas mixt col)><dif>S=<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|T<aphp>-T<aph>|T<aphp>>*<dif>S<aph>+<big|sum><rsub|i><big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|\<mu\><rsub|i><aphp>-\<mu\><rsub|i><aph>|T<aphp>>*<dif>n<rsub|i><aph>
  </equation>

  In an equilibrium state, <math|S> is at a maximum and <math|<dif>S> is zero
  for an infinitesimal change of any of the independent variables. This
  requires the coefficient of each term in the sums on the right side of Eq.
  <reference|dS =(gas mixt col)> to be zero. The equation therefore tells
  that at equilibrium <em|the temperature and the chemical potential of each
  constituent are uniform throughout the gas mixture>. The equation says
  nothing about the pressure.

  Just as the chemical potential of a pure substance at a given elevation is
  defined in this book as the molar Gibbs energy at that elevation (page
  <pageref|8-total chem. pot.>), the chemical potential of substance <math|i>
  in a mixture at elevation <math|h> is the partial molar Gibbs energy at
  that elevation.

  We define the standard potential <math|\<mu\><rsub|i><st><gas>> of
  component <math|i> of the gas mixture as the chemical potential of <math|i>
  under standard state conditions at the reference elevation <math|h=0>. At
  this elevation, the chemical potential and fugacity are related by

  <\equation>
    <label|mu_i(0)=mu_i^o+RTln(f/p^o)>\<mu\><rsub|i><around|(|0|)>=\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|<fug><rsub|i><around|(|0|)>|p<st>>
  </equation>

  If we reversibly raise a small sample of mass <math|m> of the gas mixture
  by an infinitesimal distance <math|<dif>h>, without heat and at constant
  <math|T> and <math|V>, the fugacity <math|<fug><rsub|i>> remains constant.
  The gravitational work during the elevation process is
  <math|<dw><rprime|'>=m*g<dif>h>. This work contributes to the internal
  energy change: <math|<dif>U=T<dif>S-p<dif>V+<big|sum><rsub|i>\<mu\><rsub|i><dif>n<rsub|i>+m*g<dif>h>.
  The total differential of the Gibbs energy of the sample is

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dif>G>|<cell|=>|<cell|<dif><around|(|U-T*S+p*V|)>>>|<row|<cell|>|<cell|=>|<cell|-S*<dif>T+V*<difp>+<big|sum><rsub|i>\<mu\><rsub|i>*<dif>n<rsub|i>+m*g*<dif>h<eq-number>>>>>
  </eqnarray*>

  From this total differential, we write the reciprocity relation

  <\equation>
    <Pd|\<mu\><rsub|i>|h|T,p,<allni>>=<Pd|m*g|n<rsub|i>|T,p,n<rsub|j\<ne\>i>,h>
  </equation>

  With the substitution <math|m=<big|sum><rsub|i>n<rsub|i>*M<rsub|i>> in the
  partial derivative on the right side, the partial derivative becomes
  <math|M<rsub|i>*g>. At constant <math|T>, <math|p>, and composition,
  therefore, we have <math|<dif>\<mu\><rsub|i>=M<rsub|i>*g<dif>h>.
  Integrating over a finite elevation change from <math|h=0> to
  <math|h=h<rprime|'>>, we obtain

  <equation-cov2|<label|mu_i(h')=mu_i(0) +>\<mu\><rsub|i><around|(|h<rprime|'>|)>-\<mu\><rsub|i><around|(|0|)>=<big|int><rsub|0><rsup|h<rprime|'>><space|-0.17em><space|-0.17em><space|-0.17em>M<rsub|i>*g*<dif>h=M<rsub|i>*g*h<rprime|'>|(
  <math|<math|<fug><rsub|i><around|(|h<rprime|'>|)>=<fug><rsub|i><around|(|0|)>>>
  )>

  The general relation between <math|\<mu\><rsub|i>>, <math|<fug><rsub|i>>,
  and <math|h> that agrees with Eqs. <reference|mu_i(0)=mu_i^o+RTln(f/p^o)>
  and <reference|mu_i(h')=mu_i(0) +> is

  <\equation>
    <label|mui(h)=muio(0)+RTln(f/po)+M_igh>\<mu\><rsub|i><around|(|h|)>=\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|<fug><rsub|i><around|(|h|)>|p<st>>+M<rsub|i>*g*h
  </equation>

  In the equilibrium state of the tall column of gas,
  <math|\<mu\><rsub|i><around|(|h|)>> is equal to
  <math|\<mu\><rsub|i><around|(|0|)>>. Equation
  <reference|mui(h)=muio(0)+RTln(f/po)+M_igh> shows that this is only
  possible if <math|<fug><rsub|i>> decreases as <math|h> increases. Equating
  the expressions given by this equation for
  <math|\<mu\><rsub|i><around|(|h|)>> and
  <math|\<mu\><rsub|i><around|(|0|)>>, we have

  <\equation>
    \<mu\><rsub|i><st><gas>+R*T*ln <frac|<fug><rsub|i><around|(|h|)>|p<st>>+M<rsub|i>*g*h=\<mu\><rsub|i><st><gas>+R*T*ln
    <frac|<fug><rsub|i><around|(|0|)>|p<st>>
  </equation>

  Solving for <math|<fug><rsub|i><around|(|h|)>> gives

  <equation-cov2|<fug><rsub|i><around|(|h|)>=<fug><rsub|i><around|(|0|)>*e<rsup|-M<rsub|i>*g*h/R*T>|(gas
  mixture at equilibrium)>

  If the gas is an ideal gas mixture, <math|<fug><rsub|i>> is the same as the
  partial pressure <math|p<rsub|i>>:

  <equation-cov2|<label|p(i) in gravity>p<rsub|i><around|(|h|)>=p<rsub|i><around|(|0|)>*e<rsup|-M<rsub|i>*g*h/R*T>|(ideal
  gas mixture at equilibrium)>

  Equation <reference|p(i) in gravity> shows that each constituent of an
  ideal gas mixture individually obeys the <index|Barometric
  formula>barometric formula given by Eq. <vpageref|p=po*exp(-Mgh/RT)>.

  The pressure at elevation <math|h> is found from
  <math|p<around|(|h|)>=<big|sum><rsub|i>p<rsub|i><around|(|h|)>>. If the
  constituents have different molar masses, the mole fraction composition
  changes with elevation. For example, in a binary ideal gas mixture the mole
  fraction of the constituent with the greater molar mass decreases with
  increasing elevation, and the mole fraction of the other constituent
  increases.<index-complex|<tuple|mixture|gas, in a gravitational field>||c9
  sec-mgcf-gas-gravity idx1|<tuple|Mixture|gas, in a gravitational
  field>><index-complex|<tuple|ideal gas|mixture|gravitational field>||c9
  sec-mgcf-gas-gravity idx2|<tuple|Ideal gas|mixture|in a gravitational
  field>><index-complex|<tuple|equilibrium conditions|gas mixture>||c9
  sec-mgcf-gas-gravity idx3|<tuple|Equilibrium conditions|for a gas mixture
  in a gravitational field>>

  <subsection|Liquid solution in a centrifuge
  cell><label|9-centrifuge><label|c9 sec-mgcf-liq-centrifuge>

  This section derives equilibrium conditions of a dilute binary solution
  confined to a cell embedded in a spinning centrifuge
  rotor.<index-complex|<tuple|solution|centrifuge cell>||c9
  sec-mgcf-liq-centrifuge idx1|<tuple|Solution|in a centrifuge
  cell>><index-complex|<tuple|equilibrium conditions|solution in a
  centrifuge>||c9 sec-mgcf-liq-centrifuge idx2|<tuple|Equilibrium
  conditions|for a solution in a centrifuge>><index-complex|<tuple|centrifuge|cell>||c9
  sec-mgcf-liq-centrifuge idx3|<tuple|Centrifuge|cell>><index-complex|<tuple|ultracentrifuge>||c9
  sec-mgcf-liq-centrifuge idx4|<tuple|Ultracentrifuge>>

  The <em|system> is the solution. The rotor's angle of rotation with respect
  to a <index|Lab frame><subindex|Frame|lab>lab frame is not relevant to the
  state of the system, so we use a <index|Local
  frame><subindex|Frame|local>local reference frame fixed in the
  rotor<\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/centrifuge_cell_2-parts.eps|281pt|138pt||>>
        <label|fig:9-cell>

        <\enumerate-alpha>
          <item>Sample cell of a centrifuge rotor (schematic), with Cartesian
          axes <math|x>, <math|y>, <math|z> of a stationary lab frame and
          axes <math|x<rprime|'>>, <math|y<rprime|'>>, <math|z> of a local
          frame fixed in the spinning rotor. (The rotor is not shown.) The
          axis of rotation is along the <math|z> axis. The angular velocity
          of the rotor is <math|\<omega\>=<dif>\<vartheta\>/<dt>>. The sample
          cell (heavy lines) is stationary in the local frame.

          <item>Thin slab-shaped volume elements in the sample cell.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float> as shown in Fig. <reference|fig:9-cell>(a) on page
  <pageref|fig:9-cell> . The values of heat, work, and energy changes
  measured in this <index|Rotating local frame><subsubindex|Frame|local|rotating>rotating
  frame are different from those in a lab frame (Sec. <reference|A-rotating
  frame> in Appendix <reference|app:forces>). Nevertheless, the laws of
  thermodynamics and the relations derived from them are obeyed in the local
  frame when we measure the heat, work, and state functions in this frame
  (page <pageref|thermo-noninertial frame>).

  Note that an equilibrium state can only exist relative to the
  <index|Rotating local frame><subsubindex|Frame|local|rotating>rotating
  local frame; an observer fixed in this frame would see no change in the
  state of the isolated solution over time. While the rotor rotates, however,
  there is no equilibrium state relative to the lab frame, because the
  system's position in the frame constantly changes.

  We assume the centrifuge rotor rotates about the vertical <math|z> axis at
  a constant angular velocity <math|\<omega\>>. As shown in Fig.
  <reference|fig:9-cell>(a), the elevation of a point within the local frame
  is given by <math|z> and the radial distance from the axis of rotation is
  given by <math|r>.

  In the rotating local frame, a body of mass <math|m> has exerted on it a
  centrifugal force <math|F<rsup|<text|centr>>=m*\<omega\><rsup|2>*r>
  directed horizontally in the outward <math|+r> radial direction (Sec.
  <reference|A-rotating frame>).<footnote|There is also a <index|Coriolis
  force><subindex|Force|Coriolis>Coriolis force that vanishes as the body's
  velocity in the rotating local frame approaches zero. The centrifugal and
  Coriolis forces are <subindex|Force|apparent><em|apparent> or
  <subindex|Force|fictitious><em|fictitious> forces, in the sense that they
  are caused by the acceleration of the rotating frame rather than by
  interactions between particles. When we treat these forces as if they are
  real forces, we can use Newton's second law of motion to relate the net
  force on a body and the body's acceleration in the rotating frame (see Sec.
  <reference|A-local frame>).> The gravitational force in this frame,
  directed in the downward <math|-z> direction, is the same as the
  gravitational force in a lab frame. Because the height of a typical
  centrifuge cell is usually no greater than about one centimeter, in an
  equilibrium state the variation of pressure and composition between the top
  and bottom of the cell at any given distance from the axis of rotation is
  completely negligible\Vall the measurable variation is along the radial
  direction.

  To find conditions for equilibrium, we imagine the solution to be divided
  into many thin curved volume elements at different distances from the axis
  of rotation as depicted in Fig. <reference|fig:9-cell>(b). We treat each
  volume element as a uniform phase held at constant volume so that it is at
  a constant distance from the axis of rotation. The derivation is the same
  as the one used in the preceding section to obtain Eq. <reference|dS =(gas
  mixt col)>, and leads to the same conclusion: in an equilibrium state
  <em|the temperature and the chemical potential of each substance (solvent
  and solute) are uniform throughout the solution>.

  We find the dependence of pressure on <math|r> as follows. Consider one of
  the thin slab-shaped volume elements of Fig. <reference|fig:9-cell>(b). The
  volume element is located at radial position <math|r> and its faces are
  perpendicular to the direction of increasing <math|r>. The thickness of the
  volume element is <math|\<up-delta\>*r>, the surface area of each face is
  <math|<As>>, and the mass of the solution in the volume element is
  <math|m=\<rho\>*<As>*\<up-delta\>*r>. Expressed as components in the
  direction of increasing <math|r> of the forces exerted on the volume
  element, the force at the inner face is <math|p*<As>>, the force at the
  outer face is <math|-<around|(|p+\<up-delta\>*<space|-0.17em>p|)><As>>, and
  the centrifugal force is <math|m*\<omega\><rsup|2>*r=\<rho\>*<As>*\<omega\><rsup|2>*r*\<up-delta\>*r>.
  From Newton's second law, the sum of these components is zero at
  equilibrium:

  <\equation>
    p*<As>-<around|(|p+\<up-delta\>*<space|-0.17em>p|)>*<As>+\<rho\>*<As>*\<omega\><rsup|2>*r*\<up-delta\>*r=0
  </equation>

  or <math|\<up-delta\>*<space|-0.17em>p=\<rho\>*\<omega\><rsup|2>*r*\<up-delta\>*r>.
  In the limit as <math|\<up-delta\>*r> and
  <math|\<up-delta\>*<space|-0.17em>p> are made infinitesimal, this becomes

  <\equation>
    <label|dp=rho omega^2 rdr><difp>=\<rho\>*\<omega\><rsup|2>*r<dif>r
  </equation>

  We will assume the density <math|\<rho\>> is uniform throughout the
  solution.<footnote|In the centrifugal field, this assumption is strictly
  true only if the solution is incompressible and its density is independent
  of composition.> Then integration of Eq. <reference|dp=rho omega^2 rdr>
  yields

  <\equation>
    <label|p''-p'=>p<rprime|''>-p<rprime|'>=<big|int><rsub|p<rprime|'>><rsup|p<rprime|''>><space|-0.17em><difp>=\<rho\>*\<omega\><rsup|2>*<space|-0.17em><big|int><rsub|r<rprime|'>><rsup|r<rprime|''>><space|-0.17em><space|-0.17em><space|-0.17em>r*<dif>r=<frac|\<rho\>*\<omega\><rsup|2>|2>*<around*|[|<around*|(|r<rprime|''>|)><rsup|2>-<around*|(|r<rprime|'>|)><rsup|2>|]>
  </equation>

  where the superscripts <math|<rprime|'>> and <math|<rprime|''>> denote
  positions at two different values of <math|r> in the cell. The pressure is
  seen to increase with increasing distance from the axis of rotation.

  Next we investigate the dependence of the solute concentration <math|c<B>>
  on <math|r> in the equilibrium state of the binary solution. Consider a
  small sample of the solution of mass <math|m>. Assume the extent of this
  sample in the radial direction is small enough for the variation of the
  centrifugal force field to be negligible. The reversible work in the
  <index|Local frame><subindex|Frame|local>local frame needed to move this
  small sample an infinitesimal distance <math|<dif>r> at constant <math|z>,
  <math|T>, and <math|p>, using an external force
  <math|-F<rsup|<text|centr>>> that opposes the centrifugal force, is

  <\equation>
    <dw><rprime|'>=F<sur>*<dif>r=<around|(|-F<rsup|<text|centr>>|)>*<dif>r=-m*\<omega\><rsup|2>*r*<dif>r
  </equation>

  This work is a contribution to the change <math|<dif>U> of the internal
  energy. The Gibbs energy of the small sample in the local frame is a
  function of the independent variables <math|T>, <math|p>, <math|n<A>>,
  <math|n<B>>, and <math|r>, and its total differential is

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dif>G>|<cell|=>|<cell|<dif><around|(|U-T*S+p*V|)>>>|<row|<cell|>|<cell|=>|<cell|-S*<dif>T+V*<difp>+\<mu\><A>*<dif>n<A>+\<mu\><B>*<dif>n<B>-m*\<omega\><rsup|2>*r*<dif>r<eq-number><label|dG
    centr>>>>>
  </eqnarray*>

  We use Eq. <reference|dG centr> to write the reciprocity relation

  <\equation>
    <Pd|\<mu\><B>|r|T,p,n<A>,n<B>>=-\<omega\><rsup|2>*r*<Pd|m|n<B>|T,p,n<A>,r>
  </equation>

  Then, using <math|m=n<A>*M<A>+n<B>*M<B>>, we obtain

  <\equation>
    <Pd|\<mu\><B>|r|T,p,n<A>,n<B>>=-M<B>*\<omega\><rsup|2>*r
  </equation>

  Thus at constant <math|T>, <math|p>, and composition, which are the
  conditions that allow the activity <math|a<cbB>> to remain constant,
  <math|\<mu\><B>> for the sample varies with <math|r> according to
  <math|<dif>\<mu\><B>=-M<B>*\<omega\><rsup|2>*r*<dif>r>. We integrate from
  radial position <math|r<rprime|'>> to position <math|r<rprime|''>> to
  obtain

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<mu\><B><around*|(|r<rprime|''>|)>-\<mu\><B><around*|(|r<rprime|'>|)>>|<cell|=>|<cell|-M<B>*\<omega\><rsup|2>*<big|int><rsub|r<rprime|'>><rsup|r<rprime|''>>r*<dvar|r>>>|<row|<cell|>|<cell|=>|<cell|-<tfrac|1|2>*M<B>*\<omega\><rsup|2>*<around*|[|<around*|(|r<rprime|''>|)><rsup|2>-<around*|(|r<rprime|'>|)><rsup|2>|]><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<around*|(|a<cbB><around*|(|r<rprime|''>|)>=a<cbB><around*|(|r<rprime|'>|)>
    |)>>>>>><label|mu(r'')-mu(r')=>>>>>
  </eqnarray*>

  Let us take <math|r<rprime|'>> as a reference position, such as the end of
  the centrifuge cell farthest from the axis of rotation. We define the
  standard chemical potential <math|\<mu\><st><cbB>> as the solute chemical
  potential under standard state conditions on a concentration basis at this
  position. The solute chemical potential and activity at this position are
  related by

  <\equation>
    <label|mu(r')=mu^o+RTln a(r')>\<mu\><B><around|(|r<rprime|'>|)>=\<mu\><cbB><st>+R*T*ln
    a<cbB><around|(|r<rprime|'>|)>
  </equation>

  From Eqs. <reference|mu(r'')-mu(r')=> and <reference|mu(r')=mu^o+RTln
  a(r')>, we obtain the following general relation between <math|\<mu\><B>>
  and <math|a<cbB>> at an arbitrary radial position <math|r<rprime|''>>:

  <\equation>
    <label|muB(r)=>\<mu\><B><around|(|r<rprime|''>|)>=\<mu\><cbB><st>+R*T*ln
    a<cbB><around|(|r<rprime|''>|)>-<tfrac|1|2>*M<B>*\<omega\><rsup|2>*<around*|[|<around*|(|r<rprime|''>|)><rsup|2>-<around*|(|r<rprime|'>|)><rsup|2>|]>
  </equation>

  We found earlier that when the solution is in an equilibrium state,
  <math|\<mu\><B>> is independent of <math|r>\Vthat is,
  <math|\<mu\><B><around|(|r<rprime|''>|)>> is equal to
  <math|\<mu\><B><around|(|r<rprime|'>|)>> for any value of
  <math|r<rprime|''>>. When we equate expressions given by Eq.
  <reference|muB(r)=> for <math|\<mu\><B><around|(|r<rprime|''>|)>> and
  <math|\<mu\><B><around|(|r<rprime|'>|)>> and rearrange, we obtain the
  following relation between the activities at the two radial positions:

  <\equation-cov2|ln <frac|a<cbB><around|(|r<rprime|''>|)>|a<cbB><around|(|r<rprime|'>|)>>=<frac|M<B>*\<omega\><rsup|2>|2*R*T>*<around*|[|<around*|(|r<rprime|''>|)><rsup|2>-<around*|(|r<rprime|'>|)><rsup|2>|]>>
    (solution in centrifuge

    cell at equilibrium)
  </equation-cov2>

  The solute activity is related to the concentration <math|c<B>> by
  <math|a<cbB>=<G><cbB>*<g><cbB>*c<B>/c<st>>. We assume the solution is
  sufficiently dilute for the activity coefficient <math|<g><cbB>> to be
  approximated by <math|1>. The pressure factor is given by
  <math|<G><cbB>\<approx\>exp <around*|[|V<B><rsup|\<infty\>>*<around|(|p-p<st>|)>/R*T|]>>
  (Table <reference|tbl:9-Gamma_i>). These relations give us another
  expression for the logarithm of the ratio of activities:

  <\equation>
    ln <frac|a<cbB><around|(|r<rprime|''>|)>|a<cbB><around|(|r<rprime|'>|)>>=<frac|V<B><rsup|\<infty\>><around|(|p<rprime|''>-p<rprime|'>|)>|R*T>+ln
    <frac|c<B><around|(|r<rprime|''>|)>|c<B><around|(|r<rprime|'>|)>>
  </equation>

  We substitute for <math|p<rprime|''>-p<rprime|'>> from Eq.
  <reference|p''-p'=>. It is also useful to make the substitution
  <math|V<B><rsup|\<infty\>>=M<B>*v<B><rsup|\<infty\>>>, where
  <math|v<B><rsup|\<infty\>>> is the partial specific volume of the solute at
  infinite dilution (page <pageref|9-partial specific volume>).

  When we equate the two expressions for <math|ln
  <around|[|a<cbB><around|(|r<rprime|''>|)>/a<cbB><around|(|r<rprime|'>|)>|]>>,
  we obtain finally

  <\equation-cov2|<label|ln(c''/c') (centr)>ln
  <frac|c<B><around|(|r<rprime|''>|)>|c<B><around|(|r<rprime|'>|)>>=<frac|M<B><around*|(|1-v<B><rsup|\<infty\>>\<rho\>|)>*\<omega\><rsup|2>|2*R*T>*<around*|[|<around*|(|r<rprime|''>|)><rsup|2>-<around*|(|r<rprime|'>|)><rsup|2>|]>>
    (solution in centrifuge

    cell at equilibrium)
  </equation-cov2>

  This equation shows that if the solution density <math|\<rho\>> is less
  than the effective solute density <math|1/v<B><rsup|\<infty\>>>, so that
  <math|v<B><rsup|\<infty\>>*\<rho\>> is less than 1, the solute
  concentration increases with increasing distance from the axis of rotation
  in the equilibrium state. If, however, <math|\<rho\>> is greater than
  <math|1/v<B><rsup|\<infty\>>>, the concentration decreases with increasing
  <math|r>. The factor <math|<around*|(|1-v<B><rsup|\<infty\>>*\<rho\>|)>> is
  like a buoyancy factor for the effect of the centrifugal field on the
  solute.

  Equation <reference|ln(c''/c') (centr)> is needed for <index|Sedimentation
  equilibrium><em|sedimentation equilibrium>, a method of determining the
  molar mass of a macromolecule. A dilute solution of the macromolecule is
  placed in the cell of an analytical ultracentrifuge, and the angular
  velocity is selected to produce a measurable solute concentration gradient
  at equilibrium. The solute concentration is measured optically as a
  function of <math|r>. The equation predicts that a plot of <math|ln
  <around*|(|c<B>/c<st>|)>> versus <math|r<rsup|2>> will be linear, with a
  slope equal to <math|M<B>*<around*|(|1-v<B><rsup|\<infty\>>\<rho\>|)>*\<omega\><rsup|2>/2*R*T>.
  The partial specific volume <math|v<B><rsup|\<infty\>>> is found from
  measurements of solution density as a function of solute mass fraction
  (page <pageref|9-partial specific volume>). By this means, the
  <index-complex|<tuple|molar|mass|sedimentation
  equilibrium>|||<tuple|Molar|mass|from sedimentation equilibrium>>molar mass
  <math|M<B>> of the macromolecule is evaluated.<index-complex|<tuple|solution|centrifuge
  cell>||c9 sec-mgcf-liq-centrifuge idx1|<tuple|Solution|in a centrifuge
  cell>><index-complex|<tuple|equilibrium conditions|solution in a
  centrifuge>||c9 sec-mgcf-liq-centrifuge idx2|<tuple|Equilibrium
  conditions|for a solution in a centrifuge>><index-complex|<tuple|centrifuge|cell>||c9
  sec-mgcf-liq-centrifuge idx3|<tuple|Centrifuge|cell>><index-complex|<tuple|ultracentrifuge>||c9
  sec-mgcf-liq-centrifuge idx4|<tuple|Ultracentrifuge>>

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|8>
    <associate|page-first|185>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|5>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|2-binary solns|<tuple|9.1.4|180>>
    <associate|9-Henry's law|<tuple|9.4.4|199>>
    <associate|9-Raoult's law|<tuple|9.4.1|196>>
    <associate|9-Solvent in ideal-dilute soln|<tuple|9.4.6|202>>
    <associate|9-act coeff from fug|<tuple|9.6.1|210>>
    <associate|9-act coeffs|<tuple|9.5|206>>
    <associate|9-act coeffs from osmotic coeffs|<tuple|9.6.3|213>>
    <associate|9-activities|<tuple|9.7|217>>
    <associate|9-centrifuge|<tuple|9.8.2|222>>
    <associate|9-chem pot of species in a mixt|<tuple|9.2.6|188>>
    <associate|9-composition of a mixture|<tuple|9.1.5|181>>
    <associate|9-eqm conditions|<tuple|9.2.7|189>>
    <associate|9-fugacity measurements|<tuple|9.6.4|215>>
    <associate|9-gas mixt in grav field|<tuple|9.8.1|221>>
    <associate|9-general relations|<tuple|9.2.4|187>>
    <associate|9-ideal mixtures|<tuple|9.4.2|198>>
    <associate|9-ideal-dilute soln|<tuple|9.4.5|201>>
    <associate|9-intercepts|<tuple|9.2.3|?>>
    <associate|9-mixt st states|<tuple|9.7.1|218>>
    <associate|9-mixts in general|<tuple|9.1.2|179>>
    <associate|9-mixtures in grav \ centrif fields|<tuple|9.8|221>>
    <associate|9-nonideal dil solns|<tuple|9.5.4|209>>
    <associate|9-partial molar volume|<tuple|9.2.1|182>>
    <associate|9-partial molar, id dil sln|<tuple|9.4.7|203>>
    <associate|9-partial molar, id gas mixts|<tuple|9.3.3|192>>
    <associate|9-partial molar, id mixts|<tuple|9.4.3|198>>
    <associate|9-partial p|<tuple|9.3.1|191>>
    <associate|9-partial specific volume|<tuple|9.2.32|188>>
    <associate|9-real gas mixtures|<tuple|9.3.4|194>>
    <associate|9-real mixts|<tuple|9.5.3|207>>
    <associate|9-solutions|<tuple|9.1.3|180>>
    <associate|9-species \ substances|<tuple|9.1.1|179>>
    <associate|B(A)'=|<tuple|9.3.27|196>>
    <associate|B(B)'=|<tuple|9.3.28|196>>
    <associate|B=sum(i)sum(j)y(i)y(j)B(ij)|<tuple|9.3.24|196>>
    <associate|B=yA^2 B(AA)+...|<tuple|9.3.23|196>>
    <associate|Bi'=2 sum yj Bij-B|<tuple|9.3.26|196>>
    <associate|C_i=1/f_i^*|<tuple|9.6.4|210>>
    <associate|C_pi=C_pio|<tuple|9.3.11|194>>
    <associate|C_pi=dH_i/dT|<tuple|9.2.52|191>>
    <associate|Chap. 9|<tuple|9|179>>
    <associate|Cpi=Cpi^*|<tuple|9.4.13|199>>
    <associate|Del Xm(mix) plot|<tuple|9.2.27|187>>
    <associate|GD-binary|<tuple|9.6.6|213>>
    <associate|Gamma defn|<tuple|9.7.9|218>>
    <associate|H_i=H_i^*|<tuple|9.4.10|199>>
    <associate|H_i=H_io|<tuple|9.3.7|193>>
    <associate|KcB=lim(fB/cB|<tuple|9.4.20|200>>
    <associate|KmB=lim(fB/mB|<tuple|9.4.21|200>>
    <associate|KxB=lim(fB/xB)|<tuple|9.4.19|200>>
    <associate|S(B)=|<tuple|9.4.36|204>>
    <associate|S(B)=S(B,ref)-R*ln(xB)|<tuple|9.4.37|204>>
    <associate|S_i=S_i^*-R*ln x_i|<tuple|9.4.9|198>>
    <associate|S_i=S_io-R*ln(p_i/po)|<tuple|9.3.6|193>>
    <associate|U_i=H_i-pV_i|<tuple|9.2.50|191>>
    <associate|U_i=U_i^*|<tuple|9.4.12|199>>
    <associate|U_i=U_io|<tuple|9.3.10|193>>
    <associate|Ui,Cpi,Hi in id gas mixt|<tuple|9.3.11|194>>
    <associate|V/n=|<tuple|9.2.15|185>>
    <associate|V=VA*nA+VB*nB|<tuple|9.2.9|184>>
    <associate|V=nRT/p+nB|<tuple|9.3.21|195>>
    <associate|V_i=RT/p|<tuple|9.3.9|193>>
    <associate|V_i=V_i^*|<tuple|9.4.11|199>>
    <associate|Vi=RT/p+Bi'|<tuple|9.3.25|196>>
    <associate|Vm^E=|<tuple|9.2.21|186>>
    <associate|X=sum(X_i*n_i)|<tuple|9.2.25|187>>
    <associate|X_i=dX/dn_i|<tuple|9.2.1|181>>
    <associate|Z=(1+Bp/RT)|<tuple|9.3.22|195>>
    <associate|[dV(A)/dxB]xA+[dV(B)/dxB]xB=0|<tuple|9.2.17|186>>
    <associate|a_i indep of h and phi|<tuple|Activity|218>>
    <associate|a_i=exp(mu_i-mu_io)/RT|<tuple|9.7.1|217>>
    <associate|ac_i -\<gtr\> 1|<tuple|9.5.20|208>>
    <associate|act c,B|<tuple|9.7.7|218>>
    <associate|act coeff c,B|<tuple|9.5.17|208>>
    <associate|act coeff defn|<tuple|9.5.12|207>>
    <associate|act coeff m,B|<tuple|9.5.18|208>>
    <associate|act coeff x,B|<tuple|9.5.16|208>>
    <associate|act coeff, gas|<tuple|9.5.13|208>>
    <associate|act coeff, mixt|<tuple|9.5.14|208>>
    <associate|act coeff, solvent|<tuple|9.5.15|208>>
    <associate|act gas|<tuple|9.7.3|218>>
    <associate|act m,B|<tuple|9.7.8|218>>
    <associate|act mixt|<tuple|9.7.4|218>>
    <associate|act solvent|<tuple|9.7.5|218>>
    <associate|act x,B|<tuple|9.7.6|218>>
    <associate|auto-1|<tuple|9|179>>
    <associate|auto-10|<tuple|9.1.2|179>>
    <associate|auto-100|<tuple|<tuple|partial pressure|ideal gas
    mixture>|192>>
    <associate|auto-101|<tuple|9.3.3|192>>
    <associate|auto-102|<tuple|<tuple|standard state|gas mixture
    constituent>|192>>
    <associate|auto-103|<tuple|<tuple|chemical potential|standard|gas
    constituent>|192>>
    <associate|auto-104|<tuple|9.3.1|193>>
    <associate|auto-105|<tuple|Ideal gas|193>>
    <associate|auto-106|<tuple|Gas|193>>
    <associate|auto-107|<tuple|<tuple|partial molar|entropy|ideal gas
    mixture>|193>>
    <associate|auto-108|<tuple|<tuple|entropy|standard molar|gas>|193>>
    <associate|auto-109|<tuple|standard molar entropy|193>>
    <associate|auto-11|<tuple|Mole fraction|179>>
    <associate|auto-110|<tuple|<tuple|partial molar|enthalpy|ideal gas
    mixture>|193>>
    <associate|auto-111|<tuple|<tuple|enthalpy|partial molar|ideal gas
    mixture>|193>>
    <associate|auto-112|<tuple|<tuple|partial molar|volume|ideal gas
    mixture>|193>>
    <associate|auto-113|<tuple|<tuple|volume|partial molar|ideal gas
    mixture>|193>>
    <associate|auto-114|<tuple|Additivity rule|194>>
    <associate|auto-115|<tuple|9.3.4|194>>
    <associate|auto-116|<tuple|9.3.4.1|194>>
    <associate|auto-117|<tuple|<tuple|fugacity|gas mixture constituent>|194>>
    <associate|auto-118|<tuple|<tuple|fugacity coefficient|gas mixture
    constituent>|194>>
    <associate|auto-119|<tuple|9.3.4.2|195>>
    <associate|auto-12|<tuple|mole fraction|179>>
    <associate|auto-120|<tuple|9.3.2|195>>
    <associate|auto-122|<tuple|<tuple|partial molar|quantity|gas mixture
    constituent>|195>>
    <associate|auto-123|<tuple|9.3.4.3|195>>
    <associate|auto-124|<tuple|<tuple|virial|equation|gas mixture>|195>>
    <associate|auto-125|<tuple|<tuple|equation of state|gas at low
    pressure>|195>>
    <associate|auto-126|<tuple|Statistical mechanics|196>>
    <associate|auto-127|<tuple|<tuple|equation of state|gas at low
    pressure>|196>>
    <associate|auto-128|<tuple|<tuple|fugacity coefficient|gas mixture
    constituent>|196>>
    <associate|auto-129|<tuple|9.4|196>>
    <associate|auto-13|<tuple|Mass fraction|179>>
    <associate|auto-130|<tuple|Solution|196>>
    <associate|auto-131|<tuple|9.4.1|196>>
    <associate|auto-132|<tuple|Raoult, Fran�ois|196>>
    <associate|auto-133|<tuple|9.4.1|197>>
    <associate|auto-134|<tuple|<tuple|raoult's law|partial>|197>>
    <associate|auto-135|<tuple|<tuple|raoult's law|fugacity>|197>>
    <associate|auto-136|<tuple|<tuple|raoult's law|partial>|197>>
    <associate|auto-137|<tuple|<tuple|raoult's law|fugacity>|197>>
    <associate|auto-138|<tuple|<tuple|raoult's law|fugacity>|198>>
    <associate|auto-139|<tuple|9.4.2|198>>
    <associate|auto-14|<tuple|mass fraction|179>>
    <associate|auto-140|<tuple|<tuple|ideal mixture|raoult's law>|198>>
    <associate|auto-141|<tuple|<tuple|mixture|ideal|raoult's law>|198>>
    <associate|auto-142|<tuple|<tuple|ideal gas|mixture|raoult's law>|198>>
    <associate|auto-143|<tuple|Ideal mixture|198>>
    <associate|auto-144|<tuple|<tuple|mixture|ideal|chemical potential>|198>>
    <associate|auto-145|<tuple|ideal mixture|198>>
    <associate|auto-146|<tuple|9.4.3|198>>
    <associate|auto-147|<tuple|<tuple|partial molar|quantity|ideal
    mixture>|198>>
    <associate|auto-148|<tuple|Partial molar|198>>
    <associate|auto-149|<tuple|Entropy|198>>
    <associate|auto-15|<tuple|Concentration|179>>
    <associate|auto-150|<tuple|Partial molar|199>>
    <associate|auto-151|<tuple|Enthalpy|199>>
    <associate|auto-152|<tuple|Partial molar|199>>
    <associate|auto-153|<tuple|Volume|199>>
    <associate|auto-154|<tuple|Partial molar|199>>
    <associate|auto-155|<tuple|Internal energy|199>>
    <associate|auto-156|<tuple|Partial molar|199>>
    <associate|auto-157|<tuple|<tuple|heat capacity|constant pressure|partial
    molar>|199>>
    <associate|auto-158|<tuple|<tuple|partial molar|quantity|ideal
    mixture>|199>>
    <associate|auto-159|<tuple|9.4.4|199>>
    <associate|auto-16|<tuple|concentration|179>>
    <associate|auto-160|<tuple|9.4.2|199>>
    <associate|auto-161|<tuple|Henry's law|199>>
    <associate|auto-162|<tuple|Henry's law|199>>
    <associate|auto-163|<tuple|Henry's law constant|199>>
    <associate|auto-164|<tuple|Henry's law constant|199>>
    <associate|auto-165|<tuple|9.4.3|200>>
    <associate|auto-166|<tuple|Henry's law constant|201>>
    <associate|auto-167|<tuple|Henry's law constants, relations between
    different|201>>
    <associate|auto-168|<tuple|9.4.5|201>>
    <associate|auto-169|<tuple|Ideal-dilute solution|201>>
    <associate|auto-17|<tuple|Binary mixture|180>>
    <associate|auto-170|<tuple|Solution|201>>
    <associate|auto-171|<tuple|ideal-dilute solution|201>>
    <associate|auto-172|<tuple|<tuple|reference state|solute>|201>>
    <associate|auto-173|<tuple|<tuple|solute|reference state>|201>>
    <associate|auto-174|<tuple|Standard|202>>
    <associate|auto-175|<tuple|Standard|202>>
    <associate|auto-176|<tuple|Concentration|202>>
    <associate|auto-177|<tuple|Standard|202>>
    <associate|auto-178|<tuple|Molality|202>>
    <associate|auto-179|<tuple|<tuple|reference state|solute>|202>>
    <associate|auto-18|<tuple|Mixture|180>>
    <associate|auto-180|<tuple|<tuple|solute|reference state>|202>>
    <associate|auto-181|<tuple|Standard|202>>
    <associate|auto-182|<tuple|Mole fraction|202>>
    <associate|auto-183|<tuple|standard mole fraction|202>>
    <associate|auto-184|<tuple|9.4.6|202>>
    <associate|auto-185|<tuple|<tuple|solvent|behavior in an ideal-dilute
    solution>|202>>
    <associate|auto-186|<tuple|<tuple|ideal-dilute solution|solvent behavior
    in>|202>>
    <associate|auto-187|<tuple|Gibbs--Duhem equation|202>>
    <associate|auto-188|<tuple|<tuple|raoult's law|fugacity|ideal
    dilute>|203>>
    <associate|auto-189|<tuple|9.4.4|203>>
    <associate|auto-19|<tuple|binary mixture|180>>
    <associate|auto-190|<tuple|<tuple|solvent|behavior in an ideal-dilute
    solution>|203>>
    <associate|auto-191|<tuple|<tuple|ideal-dilute solution|solvent behavior
    in>|203>>
    <associate|auto-192|<tuple|9.4.7|203>>
    <associate|auto-193|<tuple|<tuple|partial molar|quantity|ideal-dilute
    solution>|203>>
    <associate|auto-194|<tuple|<tuple|ideal-dilute solution|partial molar
    quantities in>|203>>
    <associate|auto-195|<tuple|9.4.1|204>>
    <associate|auto-196|<tuple|<tuple|partial molar|entropy|solute in an
    ideal-dilute solution>|204>>
    <associate|auto-197|<tuple|<tuple|entropy|partial molar|solute in an
    ideal-dilute solution>|204>>
    <associate|auto-198|<tuple|<tuple|partial molar|enthalpy|solute in an
    ideal-dilute solution>|204>>
    <associate|auto-199|<tuple|<tuple|enthalpy|partial molar|solute in an
    ideal-dilute solution>|204>>
    <associate|auto-2|<tuple|9.1|179>>
    <associate|auto-20|<tuple|9.1.3|180>>
    <associate|auto-200|<tuple|<tuple|partial molar|quantity|ideal-dilute
    solution>|204>>
    <associate|auto-201|<tuple|<tuple|ideal-dilute solution|partial molar
    quantities in>|204>>
    <associate|auto-202|<tuple|9.4.7.0.1|205|bio-HENRY.tm>>
    <associate|auto-203|<tuple|Henry, William|205|bio-HENRY.tm>>
    <associate|auto-204|<tuple|9.5|206>>
    <associate|auto-205|<tuple|Activity coefficient|206>>
    <associate|auto-206|<tuple|Reference state|206>>
    <associate|auto-207|<tuple|9.5.1|206>>
    <associate|auto-208|<tuple|Chemical potential|206>>
    <associate|auto-209|<tuple|9.5.1|206>>
    <associate|auto-21|<tuple|Solution|180>>
    <associate|auto-210|<tuple|9.5.2|206>>
    <associate|auto-211|<tuple|<tuple|ideal mixture>|206>>
    <associate|auto-212|<tuple|<tuple|mixture|ideal>|206>>
    <associate|auto-213|<tuple|<tuple|ideal mixture>|207>>
    <associate|auto-214|<tuple|<tuple|mixture|ideal>|207>>
    <associate|auto-215|<tuple|9.5.3|207>>
    <associate|auto-216|<tuple|Activity coefficient|207>>
    <associate|auto-217|<tuple|activity coefficient|207>>
    <associate|auto-218|<tuple|<tuple|reference state|mixture
    constituent>|208>>
    <associate|auto-219|<tuple|<tuple|reference state|solvent>|208>>
    <associate|auto-22|<tuple|solution|180>>
    <associate|auto-220|<tuple|IUPAC Green Book|208>>
    <associate|auto-221|<tuple|<tuple|reference state|solute>|208>>
    <associate|auto-222|<tuple|Solute|208>>
    <associate|auto-223|<tuple|Activity coefficient|208>>
    <associate|auto-224|<tuple|9.5.4|209>>
    <associate|auto-225|<tuple|<tuple|activity coefficient|solute|dilute
    solution>|209>>
    <associate|auto-226|<tuple|Statistical mechanics|209>>
    <associate|auto-227|<tuple|<tuple|mcmillan mayer>|209>>
    <associate|auto-228|<tuple|Statistical mechanics|209>>
    <associate|auto-229|<tuple|9.6|210>>
    <associate|auto-23|<tuple|Solvent|180>>
    <associate|auto-230|<tuple|9.6.1|210>>
    <associate|auto-231|<tuple|<tuple|activity coefficient|solvent|gas
    fugacity>|210>>
    <associate|auto-232|<tuple|<tuple|activity coefficient|solute|gas
    fugacity>|210>>
    <associate|auto-233|<tuple|9.6.1.1|211>>
    <associate|auto-234|<tuple|9.6.1|211>>
    <associate|auto-235|<tuple|9.6.1|211>>
    <associate|auto-236|<tuple|9.6.2|212>>
    <associate|auto-237|<tuple|<tuple|activity coefficient|solvent|gas
    fugacity>|212>>
    <associate|auto-238|<tuple|<tuple|activity coefficient|solute|gas
    fugacity>|212>>
    <associate|auto-239|<tuple|9.6.2|212>>
    <associate|auto-24|<tuple|solvent|180>>
    <associate|auto-240|<tuple|<tuple|activity coefficient|gibb-duhem>|212>>
    <associate|auto-241|<tuple|<tuple|gibbs-duhem equation>|212>>
    <associate|auto-242|<tuple|<tuple|activity coefficient|gibb-duhem>|213>>
    <associate|auto-243|<tuple|<tuple|gibbs-duhem equation>|213>>
    <associate|auto-244|<tuple|9.6.3|213>>
    <associate|auto-245|<tuple|<tuple|activity coefficient|osmotic
    coefficient>|213>>
    <associate|auto-246|<tuple|Osmotic coefficient|213>>
    <associate|auto-247|<tuple|osmotic coefficient|213>>
    <associate|auto-248|<tuple|9.6.3.1|214>>
    <associate|auto-249|<tuple|9.6.3.2|214>>
    <associate|auto-25|<tuple|Solute|180>>
    <associate|auto-250|<tuple|Gibbs--Duhem equation|214>>
    <associate|auto-251|<tuple|9.6.3|215>>
    <associate|auto-252|<tuple|<tuple|activity coefficient|osmotic
    coefficient>|215>>
    <associate|auto-253|<tuple|9.6.4|215>>
    <associate|auto-254|<tuple|Isopiestic|216>>
    <associate|auto-255|<tuple|isopiestic vapor pressure technique|216>>
    <associate|auto-256|<tuple|Isopiestic|216>>
    <associate|auto-257|<tuple|Osmotic coefficient|216>>
    <associate|auto-258|<tuple|9.6.4.0.1|217|bio-LEWIS.tm>>
    <associate|auto-259|<tuple|Lewis, Gilbert Newton|217|bio-LEWIS.tm>>
    <associate|auto-26|<tuple|solute|180>>
    <associate|auto-260|<tuple|9.7|217>>
    <associate|auto-261|<tuple|Activity|217>>
    <associate|auto-262|<tuple|activity|217>>
    <associate|auto-263|<tuple|Chemical potential|218>>
    <associate|auto-264|<tuple|Relative activity|218>>
    <associate|auto-265|<tuple|Activity|218>>
    <associate|auto-266|<tuple|9.7.1|218>>
    <associate|auto-267|<tuple|<tuple|standard state|mixture component>|218>>
    <associate|auto-268|<tuple|9.7.2|218>>
    <associate|auto-269|<tuple|Pressure factor|218>>
    <associate|auto-27|<tuple|Molality|180>>
    <associate|auto-270|<tuple|Pitzer, Kenneth|218>>
    <associate|auto-271|<tuple|Brewer, Leo|218>>
    <associate|auto-272|<tuple|Pressure factor|219>>
    <associate|auto-273|<tuple|pressure factor|219>>
    <associate|auto-274|<tuple|<tuple|activity|gas>|219>>
    <associate|auto-275|<tuple|<tuple|activity|pure liquid or solid>|219>>
    <associate|auto-276|<tuple|<tuple|activity|mixture constituent>|219>>
    <associate|auto-277|<tuple|<tuple|activity|solvent>|219>>
    <associate|auto-278|<tuple|<tuple|activity|solute>|219>>
    <associate|auto-279|<tuple|9.7.1|219>>
    <associate|auto-28|<tuple|molality|180>>
    <associate|auto-280|<tuple|9.7.3|219>>
    <associate|auto-281|<tuple|<tuple|pressure factor>|219>>
    <associate|auto-282|<tuple|9.7.2|221>>
    <associate|auto-283|<tuple|Standard|221>>
    <associate|auto-284|<tuple|Pressure|221>>
    <associate|auto-285|<tuple|<tuple|pressure factor>|221>>
    <associate|auto-286|<tuple|9.8|221>>
    <associate|auto-287|<tuple|Gravitational|221>>
    <associate|auto-288|<tuple|Field|221>>
    <associate|auto-289|<tuple|Centrifuge|221>>
    <associate|auto-29|<tuple|9.1.4|180>>
    <associate|auto-290|<tuple|9.8.1|221>>
    <associate|auto-291|<tuple|<tuple|mixture|gas, in a gravitational
    field>|221>>
    <associate|auto-292|<tuple|<tuple|ideal gas|mixture|gravitational
    field>|221>>
    <associate|auto-293|<tuple|<tuple|equilibrium conditions|gas
    mixture>|221>>
    <associate|auto-294|<tuple|Lab frame|221>>
    <associate|auto-295|<tuple|Frame|221>>
    <associate|auto-296|<tuple|Barometric formula|222>>
    <associate|auto-297|<tuple|<tuple|mixture|gas, in a gravitational
    field>|222>>
    <associate|auto-298|<tuple|<tuple|ideal gas|mixture|gravitational
    field>|222>>
    <associate|auto-299|<tuple|<tuple|equilibrium conditions|gas
    mixture>|222>>
    <associate|auto-3|<tuple|Composition variable|179>>
    <associate|auto-30|<tuple|Binary solution|180>>
    <associate|auto-300|<tuple|9.8.2|222>>
    <associate|auto-301|<tuple|<tuple|solution|centrifuge cell>|222>>
    <associate|auto-302|<tuple|<tuple|equilibrium conditions|solution in a
    centrifuge>|222>>
    <associate|auto-303|<tuple|<tuple|centrifuge|cell>|222>>
    <associate|auto-304|<tuple|<tuple|ultracentrifuge>|222>>
    <associate|auto-305|<tuple|Lab frame|223>>
    <associate|auto-306|<tuple|Frame|223>>
    <associate|auto-307|<tuple|Local frame|223>>
    <associate|auto-308|<tuple|Frame|223>>
    <associate|auto-309|<tuple|9.8.1|223>>
    <associate|auto-31|<tuple|Solution|180>>
    <associate|auto-310|<tuple|Rotating local frame|223>>
    <associate|auto-311|<tuple|Frame|223>>
    <associate|auto-312|<tuple|Rotating local frame|223>>
    <associate|auto-313|<tuple|Frame|223>>
    <associate|auto-314|<tuple|Coriolis force|223>>
    <associate|auto-315|<tuple|Force|223>>
    <associate|auto-316|<tuple|Force|223>>
    <associate|auto-317|<tuple|Force|223>>
    <associate|auto-318|<tuple|Local frame|224>>
    <associate|auto-319|<tuple|Frame|224>>
    <associate|auto-32|<tuple|Mole ratio|180>>
    <associate|auto-320|<tuple|Sedimentation equilibrium|225>>
    <associate|auto-321|<tuple|<tuple|molar|mass|sedimentation
    equilibrium>|225>>
    <associate|auto-322|<tuple|<tuple|solution|centrifuge cell>|225>>
    <associate|auto-323|<tuple|<tuple|equilibrium conditions|solution in a
    centrifuge>|225>>
    <associate|auto-324|<tuple|<tuple|centrifuge|cell>|225>>
    <associate|auto-325|<tuple|<tuple|ultracentrifuge>|225>>
    <associate|auto-33|<tuple|Composition variable|181>>
    <associate|auto-34|<tuple|9.1.5|181>>
    <associate|auto-35|<tuple|Composition variable|181>>
    <associate|auto-36|<tuple|<tuple|mixture|fixed composition>|181>>
    <associate|auto-37|<tuple|9.2|181>>
    <associate|auto-38|<tuple|Partial molar|181>>
    <associate|auto-39|<tuple|partial molar quantity|181>>
    <associate|auto-4|<tuple|composition variable|179>>
    <associate|auto-40|<tuple|9.2.1|182>>
    <associate|auto-41|<tuple|<tuple|partial molar|volume>|182>>
    <associate|auto-42|<tuple|9.2.1|182>>
    <associate|auto-43|<tuple|System|182>>
    <associate|auto-44|<tuple|Partial molar|183>>
    <associate|auto-45|<tuple|Partial molar|183>>
    <associate|auto-46|<tuple|<tuple|partial molar|volume>|183>>
    <associate|auto-47|<tuple|9.2.2|183>>
    <associate|auto-48|<tuple|System|183>>
    <associate|auto-49|<tuple|Components, number of|183>>
    <associate|auto-5|<tuple|9.1.1|179>>
    <associate|auto-50|<tuple|Volume|183>>
    <associate|auto-51|<tuple|<tuple|total differential|volume>|183>>
    <associate|auto-52|<tuple|9.2.2|184>>
    <associate|auto-53|<tuple|Additivity rule|184>>
    <associate|auto-54|<tuple|additivity rule|184>>
    <associate|auto-55|<tuple|Gibbs--Duhem equation|184>>
    <associate|auto-56|<tuple|Gibbs\UDuhem equation|184>>
    <associate|auto-57|<tuple|9.2.3|185>>
    <associate|auto-58|<tuple|Method of intercepts|185>>
    <associate|auto-59|<tuple|method of intercepts|185>>
    <associate|auto-6|<tuple|Species|179>>
    <associate|auto-60|<tuple|Mean molar volume|185>>
    <associate|auto-61|<tuple|Volume|185>>
    <associate|auto-62|<tuple|9.2.3|185>>
    <associate|auto-63|<tuple|Method of intercepts|186>>
    <associate|auto-64|<tuple|Gibbs--Duhem equation|186>>
    <associate|auto-65|<tuple|9.2.4|187>>
    <associate|auto-66|<tuple|<tuple|partial molar|quantity|general>|187>>
    <associate|auto-67|<tuple|Additivity rule|187>>
    <associate|auto-68|<tuple|Gibbs--Duhem equation|187>>
    <associate|auto-69|<tuple|9.2.5|188>>
    <associate|auto-7|<tuple|species|179>>
    <associate|auto-70|<tuple|Partial|188>>
    <associate|auto-71|<tuple|partial specific quantity|188>>
    <associate|auto-72|<tuple|Partial|188>>
    <associate|auto-73|<tuple|9.2.6|188>>
    <associate|auto-74|<tuple|<tuple|chemical potential|species in a
    mixture>|188>>
    <associate|auto-75|<tuple|chemical potential|188>>
    <associate|auto-76|<tuple|System|188>>
    <associate|auto-77|<tuple|<tuple|total differential|gibbs energy of a
    mixture>|188>>
    <associate|auto-78|<tuple|Gibbs energy|188>>
    <associate|auto-79|<tuple|Fundamental equation, Gibbs|188>>
    <associate|auto-8|<tuple|Substance|179>>
    <associate|auto-80|<tuple|Gibbs|188>>
    <associate|auto-81|<tuple|Electrical|189>>
    <associate|auto-82|<tuple|Neutrality, electrical|189>>
    <associate|auto-83|<tuple|9.2.7|189>>
    <associate|auto-84|<tuple|<tuple|equilibrium conditions|multiphase
    multicomponent systems>|189>>
    <associate|auto-85|<tuple|<tuple|equilibrium conditions|multiphase
    multicomponent systems>|190>>
    <associate|auto-86|<tuple|9.2.8|190>>
    <associate|auto-87|<tuple|<tuple|partial molar|quantity|general
    relations>|190>>
    <associate|auto-88|<tuple|Additivity rule|190>>
    <associate|auto-89|<tuple|<tuple|partial molar|quantity|general
    relations>|191>>
    <associate|auto-9|<tuple|substance|179>>
    <associate|auto-90|<tuple|9.3|191>>
    <associate|auto-91|<tuple|9.3.1|191>>
    <associate|auto-92|<tuple|Partial pressure|191>>
    <associate|auto-93|<tuple|Pressure|191>>
    <associate|auto-94|<tuple|partial pressure|191>>
    <associate|auto-95|<tuple|Dalton's law|191>>
    <associate|auto-96|<tuple|Dalton's Law|191>>
    <associate|auto-97|<tuple|9.3.2|192>>
    <associate|auto-98|<tuple|Ideal gas|192>>
    <associate|auto-99|<tuple|Gas|192>>
    <associate|binary mixt Del(mix)V|<tuple|9.2.19|186>>
    <associate|bio:henry|<tuple|9.4.7.0.1|205|bio-HENRY.tm>>
    <associate|bio:lewis|<tuple|9.6.4.0.1|217|bio-LEWIS.tm>>
    <associate|c9|<tuple|9|179>>
    <associate|c9 sec acmn-mixtures-ideal|<tuple|9.5.2|206>>
    <associate|c9 sec acmn-mixtures-real|<tuple|9.5.3|207>>
    <associate|c9 sec aus|<tuple|9.7|217>>
    <associate|c9 sec aus-pressure|<tuple|9.7.3|219>>
    <associate|c9 sec aus-std-states|<tuple|9.7.1|218>>
    <associate|c9 sec cv|<tuple|9.1|179>>
    <associate|c9 sec cv-mixtures|<tuple|9.1.2|179>>
    <associate|c9 sec cv-solutions|<tuple|9.1.3|180>>
    <associate|c9 sec cv-solutions-binary|<tuple|9.1.4|180>>
    <associate|c9 sec cv-species-substances|<tuple|9.1.1|179>>
    <associate|c9 sec eac|<tuple|9.6|210>>
    <associate|c9 sec eac-fugacity-measurements|<tuple|9.6.4|215>>
    <associate|c9 sec gm|<tuple|9.3|191>>
    <associate|c9 sec gm-ideal|<tuple|9.3.2|192>>
    <associate|c9 sec gm-partial-p|<tuple|9.3.1|191>>
    <associate|c9 sec gm-real|<tuple|9.3.4|194>>
    <associate|c9 sec lsmn|<tuple|9.4|196>>
    <associate|c9 sec lsmn-henry|<tuple|9.4.4|199>>
    <associate|c9 sec lsmn-ideal|<tuple|9.4.2|198>>
    <associate|c9 sec lsmn-ideal-dilute|<tuple|9.4.5|201>>
    <associate|c9 sec lsmn-raoult|<tuple|9.4.1|196>>
    <associate|c9 sec pmq|<tuple|9.2|181>>
    <associate|c9 sec pmq-equilcond-multicomp|<tuple|9.2.7|189>>
    <associate|c9 sec pmq-general-relations|<tuple|9.2.4|187>>
    <associate|c9 sec pmq-mixtures-binary|<tuple|9.2.3|185>>
    <associate|c9 sec pmq-totdiff|<tuple|9.2.2|183>>
    <associate|c9 sec pmq-volume|<tuple|9.2.1|182>>
    <associate|c9 sec-acmn|<tuple|9.5|206>>
    <associate|c9 sec-acmn-refstd-states|<tuple|9.5.1|206>>
    <associate|c9 sec-acmn-solutions-nonideal|<tuple|9.5.4|209>>
    <associate|c9 sec-aus-activities-composition|<tuple|9.7.2|218>>
    <associate|c9 sec-cv-mixture-composition|<tuple|9.1.5|181>>
    <associate|c9 sec-eac-gas-fugacity|<tuple|9.6.1|210>>
    <associate|c9 sec-eac-gibbs-duhem|<tuple|9.6.2|212>>
    <associate|c9 sec-eac-osmotic|<tuple|9.6.3|213>>
    <associate|c9 sec-gm-ideal-partial-molar|<tuple|9.3.3|192>>
    <associate|c9 sec-lsmn-ideal-dilute-partial-molar|<tuple|9.4.7|203>>
    <associate|c9 sec-lsmn-ideal-dilute-solvent|<tuple|9.4.6|202>>
    <associate|c9 sec-lsmn-partial-molar|<tuple|9.4.3|198>>
    <associate|c9 sec-mgcf|<tuple|9.8|221>>
    <associate|c9 sec-mgcf-gas-gravity|<tuple|9.8.1|221>>
    <associate|c9 sec-mgcf-liq-centrifuge|<tuple|9.8.2|222>>
    <associate|c9 sec-pmq-mixture-chempot|<tuple|9.2.6|188>>
    <associate|c9 sec-pmq-relations|<tuple|9.2.8|190>>
    <associate|c9 sec-pmq-specific-quantities|<tuple|9.2.5|188>>
    <associate|cB=nB/V|<tuple|9.1.7|180>>
    <associate|c_i=n_i/V|<tuple|9.1.3|179>>
    <associate|can't measure X(i) of ion|<tuple|9.2.1|182>>
    <associate|conventional V(i) for ion|<tuple|9.2.29|188>>
    <associate|d muA =|<tuple|9.4.32|203>>
    <associate|d muA=RT dln(xA)|<tuple|9.4.33|203>>
    <associate|d(V/n)/dx(B)=|<tuple|9.2.16|185>>
    <associate|d(V/n)/dxB=V(B)-V(A)|<tuple|9.2.18|186>>
    <associate|d(mu_i)/dT=-S_i|<tuple|9.2.48|191>>
    <associate|d(mu_i)/dp=V_i|<tuple|9.2.49|191>>
    <associate|dG centr|<tuple|9.8.14|224>>
    <associate|dG=-SdT+Vdp+sum(mu_i*dn_i)|<tuple|9.2.34|188>>
    <associate|dG=....|<tuple|9.2.35|189>>
    <associate|dG=.....|<tuple|9.2.36|189>>
    <associate|dH=(Cp)dT+()dp+sum()dn_i|<tuple|9.2.51|191>>
    <associate|dS =(gas mixt col)|<tuple|9.8.1|221>>
    <associate|dS=sum(alpha' ne alpha)...|<tuple|9.2.41|189>>
    <associate|dU(multiphase,mixt)|<tuple|9.2.37|189>>
    <associate|dV=()dT+()dp+()dnA+()dnB|<tuple|9.2.5|183>>
    <associate|dV=VA*dnA+VB*dnB|<tuple|9.2.8|184>>
    <associate|dV=alpha*VdT-kappaT*Vdp+()dnA+()dnB|<tuple|9.2.7|183>>
    <associate|dVA=-(nB/nA)dVB|<tuple|9.2.14|184>>
    <associate|dVm^E/dxB=|<tuple|9.2.23|186>>
    <associate|dln(cB/co)/dp=kappa_T|<tuple|9.7.16|220>>
    <associate|dmuB/dp - id dil|<tuple|9.7.15|220>>
    <associate|dmu_A=-(1/x_A)sum(x_i)dmu_i|<tuple|9.4.30|202>>
    <associate|dp=rho omega^2 rdr|<tuple|9.8.11|224>>
    <associate|fB-\<gtr\>k xB|<tuple|9.4.15|200>>
    <associate|fB=KcB*cB|<tuple|9.4.17|200>>
    <associate|fB=KmB*mB|<tuple|9.4.18|200>>
    <associate|fB=KxB*xB|<tuple|9.4.16|200>>
    <associate|f_i=(x_i)(f_i*)|<tuple|9.4.3|197>>
    <associate|f_i=phi_i*p_i|<tuple|9.3.17|194>>
    <associate|fig:9-H2O-BuOH|<tuple|9.6.2|212>>
    <associate|fig:9-additivity|<tuple|9.2.2|184>>
    <associate|fig:9-aq sucrose|<tuple|9.6.3|215>>
    <associate|fig:9-cell|<tuple|9.8.1|223>>
    <associate|fig:9-ethanol act|<tuple|9.6.1|211>>
    <associate|fig:9-ethanol fug|<tuple|9.4.4|203>>
    <associate|fig:9-fugacity vs xB|<tuple|9.4.3|200>>
    <associate|fig:9-i in liquid \ gas|<tuple|9.4.2|199>>
    <associate|fig:9-liquid+gas|<tuple|9.4.1|197>>
    <associate|fig:9-pure gas \ mixture|<tuple|9.3.1|193>>
    <associate|fig:9-tubes|<tuple|9.2.1|182>>
    <associate|fig:9-water+MeOH vols|<tuple|9.2.3|185>>
    <associate|footnote-9.1.1|<tuple|9.1.1|180>>
    <associate|footnote-9.2.1|<tuple|9.2.1|182>>
    <associate|footnote-9.2.2|<tuple|9.2.2|183>>
    <associate|footnote-9.2.3|<tuple|9.2.3|183>>
    <associate|footnote-9.2.4|<tuple|9.2.4|184>>
    <associate|footnote-9.2.5|<tuple|9.2.5|185>>
    <associate|footnote-9.3.1|<tuple|9.3.1|192>>
    <associate|footnote-9.3.2|<tuple|9.3.2|195>>
    <associate|footnote-9.4.1|<tuple|9.4.1|200>>
    <associate|footnote-9.4.2|<tuple|9.4.2|?>>
    <associate|footnote-9.4.3|<tuple|9.4.3|?>>
    <associate|footnote-9.4.4|<tuple|9.4.4|205|bio-HENRY.tm>>
    <associate|footnote-9.4.5|<tuple|9.4.5|205|bio-HENRY.tm>>
    <associate|footnote-9.4.6|<tuple|9.4.6|205|bio-HENRY.tm>>
    <associate|footnote-9.4.7|<tuple|9.4.7|205|bio-HENRY.tm>>
    <associate|footnote-9.5.1|<tuple|9.5.1|207>>
    <associate|footnote-9.5.2|<tuple|9.5.2|209>>
    <associate|footnote-9.5.3|<tuple|9.5.3|209>>
    <associate|footnote-9.6.1|<tuple|9.6.1|212>>
    <associate|footnote-9.6.2|<tuple|9.6.2|215>>
    <associate|footnote-9.6.3|<tuple|9.6.3|217|bio-LEWIS.tm>>
    <associate|footnote-9.6.4|<tuple|9.6.4|217|bio-LEWIS.tm>>
    <associate|footnote-9.6.5|<tuple|9.6.5|217|bio-LEWIS.tm>>
    <associate|footnote-9.6.6|<tuple|9.6.6|217|bio-LEWIS.tm>>
    <associate|footnote-9.6.7|<tuple|9.6.7|217|bio-LEWIS.tm>>
    <associate|footnote-9.7.1|<tuple|9.7.1|218>>
    <associate|footnote-9.7.2|<tuple|9.7.2|218>>
    <associate|footnote-9.8.1|<tuple|9.8.1|223>>
    <associate|footnote-9.8.2|<tuple|9.8.2|224>>
    <associate|footnr-9.1.1|<tuple|9.1.1|180>>
    <associate|footnr-9.2.1|<tuple|9.2.1|182>>
    <associate|footnr-9.2.2|<tuple|Components, number of|183>>
    <associate|footnr-9.2.3|<tuple|9.2.3|183>>
    <associate|footnr-9.2.4|<tuple|9.2.4|184>>
    <associate|footnr-9.2.5|<tuple|9.2.3|185>>
    <associate|footnr-9.3.1|<tuple|9.3.1|192>>
    <associate|footnr-9.3.2|<tuple|9.3.2|195>>
    <associate|footnr-9.4.1|<tuple|9.4.3|200>>
    <associate|footnr-9.4.3|<tuple|9.4.3|203>>
    <associate|footnr-9.4.4|<tuple|9.4.4|205|bio-HENRY.tm>>
    <associate|footnr-9.4.5|<tuple|9.4.5|205|bio-HENRY.tm>>
    <associate|footnr-9.4.6|<tuple|9.4.6|205|bio-HENRY.tm>>
    <associate|footnr-9.4.7|<tuple|9.4.7|205|bio-HENRY.tm>>
    <associate|footnr-9.5.1|<tuple|9.5.1|207>>
    <associate|footnr-9.5.2|<tuple|9.5.2|209>>
    <associate|footnr-9.5.3|<tuple|Statistical mechanics|209>>
    <associate|footnr-9.6.1|<tuple|9.6.2|212>>
    <associate|footnr-9.6.3|<tuple|9.6.3|217|bio-LEWIS.tm>>
    <associate|footnr-9.6.4|<tuple|9.6.4|217|bio-LEWIS.tm>>
    <associate|footnr-9.6.5|<tuple|9.6.5|217|bio-LEWIS.tm>>
    <associate|footnr-9.6.6|<tuple|9.6.6|217|bio-LEWIS.tm>>
    <associate|footnr-9.6.7|<tuple|9.6.7|217|bio-LEWIS.tm>>
    <associate|footnr-9.7.1|<tuple|9.7.1|218>>
    <associate|footnr-9.7.2|<tuple|9.7.2|218>>
    <associate|footnr-9.8.1|<tuple|Force|223>>
    <associate|footnr-9.8.2|<tuple|9.8.2|224>>
    <associate|g(mB)=exp(k(m)mB/RT)|<tuple|9.5.26|209>>
    <associate|gamma(A)-\<gtr\>1|<tuple|9.5.21|208>>
    <associate|gamma(cB)-\<gtr\>1|<tuple|9.5.23|209>>
    <associate|gamma(mB)-\<gtr\>1|<tuple|9.5.24|209>>
    <associate|gamma(xB)-\<gtr\>1|<tuple|9.5.22|209>>
    <associate|gamma_i=|<tuple|9.6.2|210>>
    <associate|gamma_i=C f_i/x_i|<tuple|9.6.3|210>>
    <associate|ideal mixture|<tuple|9.4.8|198>>
    <associate|intercepts variant|<tuple|Method of intercepts|186>>
    <associate|kcB=,kmB=|<tuple|9.4.22|201>>
    <associate|ln gamma(mB)=|<tuple|9.6.20|214>>
    <associate|ln(ac_B)=int...|<tuple|9.6.10|213>>
    <associate|ln(c''/c') (centr)|<tuple|9.8.22|225>>
    <associate|ln(f_i/p_i)=int(V_i/RT-1/p)dp|<tuple|9.3.16|194>>
    <associate|ln(phi_i)=Bi'p/RT|<tuple|9.3.29|196>>
    <associate|ln(phi_i)=int(V_i/RT-1/p)dp|<tuple|9.3.18|194>>
    <associate|ln(xA)=-MAsum(mi)|<tuple|9.6.12|213>>
    <associate|mB=nB/(nA*MA)|<tuple|9.1.8|180>>
    <associate|mB=nB/mass of solvent|<tuple|9.1.4|180>>
    <associate|mole ratios|<tuple|Mole ratio|180>>
    <associate|mu(c,B)id|<tuple|9.5.8|207>>
    <associate|mu(m,B)id|<tuple|9.5.9|207>>
    <associate|mu(r'')-mu(r')=|<tuple|9.8.17|224>>
    <associate|mu(r')=mu^o+RTln a(r')|<tuple|9.8.18|224>>
    <associate|muB(r)=|<tuple|9.8.19|224>>
    <associate|muB=[]+RTln(xB)|<tuple|9.4.23|201>>
    <associate|muB=mu(cB)(ref)+RT*ln(cB/co)|<tuple|9.4.27|202>>
    <associate|muB=mu(mB)(ref)+RT*ln(mB/mo)|<tuple|9.4.28|202>>
    <associate|muB=mu(xB)(ref)+RT*ln(xB)|<tuple|9.4.24|201>>
    <associate|mu_A(id-dil sln)|<tuple|9.4.35|203>>
    <associate|mu_i(0)=mu_i^o+RTln(f/p^o)|<tuple|9.8.2|222>>
    <associate|mu_i(h')=mu_i(0) +|<tuple|9.8.5|222>>
    <associate|mu_i(id) gas mixt|<tuple|9.5.4|207>>
    <associate|mu_i(id) general form|<tuple|9.5.10|207>>
    <associate|mu_i(id)-mu_i(ref)=|<tuple|9.5.3|207>>
    <associate|mu_i(id)=mu_i^o(g)+|<tuple|9.5.1|206>>
    <associate|mu_i(mixt)=mu_i(g)|<tuple|9.6.1|210>>
    <associate|mu_i(ref)-mu_io=|<tuple|9.7.14|220>>
    <associate|mu_i(ref)=|<tuple|9.5.2|207>>
    <associate|mu_i-mu_i(ref)(g)=|<tuple|9.5.11|207>>
    <associate|mu_i=(mu_i*)+RT*ln(x_i)|<tuple|9.4.5|198>>
    <associate|mu_i=H_i-TS_i|<tuple|9.2.46|190>>
    <associate|mu_i=dG/dn_i|<tuple|9.2.33|188>>
    <associate|mu_i=mu_io(g)+RT*(f_i/po)|<tuple|9.3.12|194>>
    <associate|mu_i=mu_io(g)+RT*ln(p_i/po)|<tuple|9.3.5|192>>
    <associate|mu_i=mu_io+RTln(a_i)|<tuple|9.7.2|218>>
    <associate|mu_i=mu_io+RTln(p_i/po)+int...|<tuple|9.3.19|195>>
    <associate|mui(h)=muio(0)+RTln(f/po)+M_igh|<tuple|9.8.6|222>>
    <associate|nA*dVA+nB*dVB=0|<tuple|9.2.12|184>>
    <associate|nB/nA (dilute)|<tuple|9.1.14|181>>
    <associate|nB/nA=M(A)cB/(rho-M(B)cB)|<tuple|9.1.11|180>>
    <associate|nB/nA=M(A)mB|<tuple|9.1.12|180>>
    <associate|nB/nA=M(A)wB/M(B)(1-wB)|<tuple|9.1.10|180>>
    <associate|nB/nA=xB/(1-xB)|<tuple|9.1.9|180>>
    <associate|p''-p'=|<tuple|9.8.12|224>>
    <associate|p(i) in gravity|<tuple|9.8.9|222>>
    <associate|pA=xA pA*|<tuple|9.4.1|196>>
    <associate|pV/n=RT[1+B/(V/n)+...)|<tuple|9.3.20|195>>
    <associate|p_i=(x_i)(p_i*)|<tuple|9.4.2|197>>
    <associate|p_i=n_i*RT/V|<tuple|9.3.3|192>>
    <associate|p_i=y_i*p|<tuple|9.3.1|191>>
    <associate|part:bio-HENRY.tm|<tuple|<tuple|ideal-dilute solution|partial
    molar quantities in>|205>>
    <associate|part:bio-LEWIS.tm|<tuple|Osmotic coefficient|217>>
    <associate|phi(m) def|<tuple|9.6.11|213>>
    <associate|phi(m)=(muA*-muA)/RTM(A)mB|<tuple|9.6.16|214>>
    <associate|phi_i -\<gtr\> 1|<tuple|9.5.19|208>>
    <associate|pressure factor omitted|<tuple|9.7.2|221>>
    <associate|sum(n_i)dX_i=0|<tuple|9.2.26|187>>
    <associate|sum(n_i)dmu_i=0|<tuple|9.2.42|190>>
    <associate|sum(p_i)=p|<tuple|9.3.2|191>>
    <associate|sum(x_i)dX_i=0|<tuple|9.2.27|187>>
    <associate|sum(x_i)dmu_i=0|<tuple|9.2.43|190>>
    <associate|tbl:9-Gamma_i|<tuple|9.7.2|221>>
    <associate|tbl:9-act coeff-fugacity|<tuple|9.6.1|211>>
    <associate|tbl:9-activities|<tuple|9.7.1|219>>
    <associate|tbl:9-gas mixt|<tuple|9.3.1|195>>
    <associate|tbl:9-id dil sln|<tuple|9.4.1|204>>
    <associate|tbl:9-st states|<tuple|9.5.1|206>>
    <associate|wB=nBMB/(nAMA+nBMB)|<tuple|9.1.6|180>>
    <associate|w_i=n_iM_i/sum(n_jM_j)|<tuple|9.1.2|179>>
    <associate|water-methanol mixt|<tuple|<tuple|partial molar|volume>|182>>
    <associate|xA*dVA+xB*dVB=0|<tuple|9.2.13|184>>
    <associate|xB=nB/(nA+nB)|<tuple|9.1.5|180>>
    <associate|x_i=n_i/sum(n_j)|<tuple|9.1.1|179>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      greenbook-1

      benson-80

      marsh-74

      dobson-25

      henry-1803

      henry-1803a

      thornber

      greenbook-3

      fischer-94

      robinson-59

      lewis-00

      lewis-23

      coffey-08

      lewis-23

      kasha-84

      lewis-61
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.2.1>|>
        Addition of pure methanol (substance B) to a water--methanol mixture
        at constant <with|mode|<quote|math>|T> and
        <with|mode|<quote|math>|p>.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|><with|mode|<quote|math>|40.75
                <with|mode|<quote|text>|cm><rsup|3>> (one mole) of methanol
                is placed in a narrow tube above a much greater volume of a
                mixture (shaded) of composition
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>=0.307>.
                The dashed line indicates the level of the upper meniscus.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>After
                the two liquid phases have mixed by diffusion, the volume of
                the mixture has increased by only
                <with|mode|<quote|math>|38.8
                <with|mode|<quote|text>|cm><rsup|3>>.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-42>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.2.2>|>
        Mixing of water (A) and methanol (B) in a 2:1 ratio of volumes to
        form a mixture of increasing volume and constant composition. The
        <with|font-shape|<quote|italic>|system> is the mixture.
      </surround>|<pageref|auto-52>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.2.3>|>
        Mixtures of water (A) and methanol (B) at <with|mode|<quote|math>|25
        <rsup|\<circ\>><with|mode|<quote|text>|C>> and
        <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.<space|.15em><space|0spc><assign|footnote-nr|5><hidden-binding|<tuple>|9.2.5><assign|fnote-+pnwft35dEGUGeZ|<quote|9.2.5>><assign|fnlab-+pnwft35dEGUGeZ|<quote|9.2.5>><rsup|<with|font-shape|<quote|right>|<reference|footnote-9.2.5>>>

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Mean
                molar volume as a function of
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>>.
                The dashed line is the tangent to the curve at
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>=0.307>.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Molar
                volume of mixing as a function of
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>>.
                The dashed line is the tangent to the curve at
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>=0.307>.

                <assign|item-nr|3><hidden-binding|<tuple>|c><assign|last-item-nr|3><assign|last-item|c><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|c<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Partial
                molar volumes as functions of
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>>.
                The points at <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>=0.307>
                (open circles) are obtained from the intercepts of the dashed
                line in either (a) or (b).
              </surround>
            </with>
          </surround>
        </with>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7ED5FBCB8-7E0423070>|<link|hyperlink|<id|%-7ED5FBCB8-7E0423070>|<url|#footnr-9.2.5>>|9.2.5>.
        |<hidden-binding|<tuple|footnote-9.2.5>|9.2.5>|Based on data in Ref.
        [<write|bib|benson-80><reference|bib-benson-80>].>>>
      </surround>|<pageref|auto-62>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.3.1>|>
        System with two gas phases, pure A and a mixture of A and B,
        separated by a semipermeable membrane through which only A can pass.
        Both phases are ideal gases at the same temperature.
      </surround>|<pageref|auto-104>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.4.1>|>
        Two systems with equilibrated liquid and gas phases.
      </surround>|<pageref|auto-133>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.4.2>|>
        Equilibrated liquid and gas mixtures. Substance
        <with|mode|<quote|math>|i> is present in both phases.
      </surround>|<pageref|auto-160>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.4.3>|>
        Liquid solutions of 2,3-dimethylbutane (B) in cyclooctane at
        <with|mode|<quote|math>|298.15<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>> and <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.<space|0spc><assign|footnote-nr|1><hidden-binding|<tuple>|9.4.1><assign|fnote-+pnwft35dEGUGeb|<quote|9.4.1>><assign|fnlab-+pnwft35dEGUGeb|<quote|9.4.1>><rsup|<with|font-shape|<quote|right>|<reference|footnote-9.4.1>>>

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Fugacity
                of B in an equilibrated gas phase as a function of solution
                composition. The dashed line, tangent to the curve at
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>=0>,
                is Henry's law behavior, and its slope is
                <with|mode|<quote|math>|<with|mode|<quote|math>|k<rsub|<with|mode|<quote|text>|H>,<with|mode|<quote|text>|B>>>>.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Fugacity
                divided by mole fraction as a function of composition; the
                limiting value at <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>=0>
                is the Henry's law constant
                <with|mode|<quote|math>|<with|mode|<quote|math>|k<rsub|<with|mode|<quote|text>|H>,<with|mode|<quote|text>|B>>>>.
              </surround>
            </with>
          </surround>
        </with>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7ED5FBCB8-7E08A1C80>|<link|hyperlink|<id|%-7ED5FBCB8-7E08A1C80>|<url|#footnr-9.4.1>>|9.4.1>.
        |<hidden-binding|<tuple|footnote-9.4.1>|9.4.1>|>>>Based on data in
        Ref. [<write|bib|marsh-74><reference|bib-marsh-74>].
      </surround>|<pageref|auto-165>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.4.4>|>
        Fugacity of ethanol in a gas phase equilibrated with a binary liquid
        mixture of ethanol (A) and H<rsub|<with|mode|<quote|math>|2>>O at
        <with|mode|<quote|math>|25<rsup|\<circ\>><with|mode|<quote|text>|C>>
        and <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>. Open circles: experimental
        measurements.<space|.15em><assign|footnote-nr|2><hidden-binding|<tuple>|9.4.2><\float|footnote|>
          <with|font-size|<quote|0.771>|<with|par-mode|<quote|justify>|par-left|<quote|0cm>|par-right|<quote|0cm>|font-shape|<quote|right>|dummy|<quote|1.0fn>|dummy|<quote|7.5fn>|<surround|<locus|<id|%-7ED5FBCB8--7FD225F78>|<link|hyperlink|<id|%-7ED5FBCB8--7FD225F78>|<url|#footnr-9.4.2>>|9.4.2>.
          |<hidden-binding|<tuple|footnote-9.4.2>|9.4.2><specific|texmacs|<htab|0fn|first>>|Ref.
          [<write|bib|dobson-25><reference|bib-dobson-25>].>>>
        </float><space|0spc><rsup|<with|font-shape|<quote|right>|<reference|footnote-9.4.2>>>
        The dashed lines show Henry's law behavior and Raoult's law behavior.
      </surround>|<pageref|auto-189>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.6.1>|>
        Liquid mixtures of ethanol (A) and
        H<rsub|<with|mode|<quote|math>|2>>O at
        <with|mode|<quote|math>|25<rsup|\<circ\>><with|mode|<quote|text>|C>>
        and <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Ethanol
                fugacity as a function of mixture composition. The dashed
                line is Raoult's law behavior, and the filled circle is the
                pure-liquid reference state.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Ethanol
                activity coefficient as a function of mixture composition.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-234>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.6.2>|>
        Dilute aqueous solutions of 1-butanol (B) at
        <with|mode|<quote|math>|50.08 <rsup|\<circ\>><with|mode|<quote|text>|C>>
        and <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.<space|0spc><assign|footnote-nr|1><hidden-binding|<tuple>|9.6.1><assign|fnote-+pnwft35dEGUGed|<quote|9.6.1>><assign|fnlab-+pnwft35dEGUGed|<quote|9.6.1>><rsup|<with|font-shape|<quote|right>|<reference|footnote-9.6.1>>>

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|><with|mode|<quote|math>|f<rsub|<with|mode|<quote|text>|B>>>
                in an equilibrated gas phase as a function of
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>>,
                measured up to the solubility limit at
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>=0.015>.
                The dilute region is shown in a magnified view. Dashed line:
                Henry's law behavior on a mole fraction basis. Filled circle:
                solute reference state based on mole fraction.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|><with|mode|<quote|math>|f<rsub|<with|mode|<quote|text>|B>>>
                as a function of <with|mode|<quote|math>|m<rsub|<with|mode|<quote|text>|B>>>,
                measured up to the solubility limit at
                <with|mode|<quote|math>|m<rsub|<with|mode|<quote|text>|B>>=0.85
                <with|mode|<quote|text>|mol>\<cdot\><with|mode|<quote|text>|kg><rsup|-1>>.
                Dashed line: Henry's law behavior on a molality basis. Filled
                circle: solute reference state on this basis.

                <assign|item-nr|3><hidden-binding|<tuple>|c><assign|last-item-nr|3><assign|last-item|c><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|c<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Activity
                coefficient on a mole fraction basis as a function of
                <with|mode|<quote|math>|x<rsub|<with|mode|<quote|text>|B>>>.

                <assign|item-nr|4><hidden-binding|<tuple>|d><assign|last-item-nr|4><assign|last-item|d><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|d<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Activity
                coefficient on a molality basis as a function of
                <with|mode|<quote|math>|m<rsub|<with|mode|<quote|text>|B>>>.
              </surround>
            </with>
          </surround>
        </with>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7ED5FBCB8--7FE38D748>|<link|hyperlink|<id|%-7ED5FBCB8--7FE38D748>|<url|#footnr-9.6.1>>|9.6.1>.
        |<hidden-binding|<tuple|footnote-9.6.1>|9.6.1>|>>>Based on data in
        Ref. [<write|bib|fischer-94><reference|bib-fischer-94>]
      </surround>|<pageref|auto-236>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.6.3>|>
        Aqueous sucrose solutions at <with|mode|<quote|math>|25
        <rsup|\<circ\>><with|mode|<quote|text>|C>>.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Integrand
                of the integral in Eq. <reference|ln gamma(mB)=> as a
                function of solution composition.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Solute
                activity coefficient on a molality basis.

                <assign|item-nr|3><hidden-binding|<tuple>|c><assign|last-item-nr|3><assign|last-item|c><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|c<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Product
                of activity coefficient and molality as a function of
                composition. The dashed line is the extrapolation of
                ideal-dilute behavior.
              </surround>
            </with>
          </surround>
        </with>

        \;

        <surround|<assign|footnote-nr|2><hidden-binding|<tuple>|9.6.2><assign|fnote-+pnwft35dEGUGee|<quote|9.6.2>><assign|fnlab-+pnwft35dEGUGee|<quote|9.6.2>>||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7ED5FBCB8--7FDF916F8>|<link|hyperlink|<id|%-7ED5FBCB8--7FDF916F8>|<url|#footnr-9.6.2>>|9.6.2>.
        |<hidden-binding|<tuple|footnote-9.6.2>|9.6.2>|Based on data in Ref.
        [<write|bib|robinson-59><reference|bib-robinson-59>], Appendix
        8.6.>>>
      </surround>|<pageref|auto-251>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.8.1>|>
        \;

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Sample
                cell of a centrifuge rotor (schematic), with Cartesian axes
                <with|mode|<quote|math>|x>, <with|mode|<quote|math>|y>,
                <with|mode|<quote|math>|z> of a stationary lab frame and axes
                <with|mode|<quote|math>|x<rprime|'>>,
                <with|mode|<quote|math>|y<rprime|'>>,
                <with|mode|<quote|math>|z> of a local frame fixed in the
                spinning rotor. (The rotor is not shown.) The axis of
                rotation is along the <with|mode|<quote|math>|z> axis. The
                angular velocity of the rotor is
                <with|mode|<quote|math>|\<omega\>=<with|mode|<quote|text>|d>*\<vartheta\>/<with|mode|<quote|math>|<with|mode|<quote|text>|d>*t>>.
                The sample cell (heavy lines) is stationary in the local
                frame.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Thin
                slab-shaped volume elements in the sample cell.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-309>>
    </associate>
    <\associate|gly>
      <tuple|normal|composition variable|<pageref|auto-4>>

      <tuple|normal|species|<pageref|auto-7>>

      <tuple|normal|substance|<pageref|auto-9>>

      <tuple|normal|mole fraction|<pageref|auto-12>>

      <tuple|normal|mass fraction|<pageref|auto-14>>

      <tuple|normal|concentration|<pageref|auto-16>>

      <tuple|normal|binary mixture|<pageref|auto-19>>

      <tuple|normal|solution|<pageref|auto-22>>

      <tuple|normal|solvent|<pageref|auto-24>>

      <tuple|normal|solute|<pageref|auto-26>>

      <tuple|normal|molality|<pageref|auto-28>>

      <tuple|normal|partial molar quantity|<pageref|auto-39>>

      <tuple|normal|additivity rule|<pageref|auto-54>>

      <tuple|normal|Gibbs\UDuhem equation|<pageref|auto-56>>

      <tuple|normal|method of intercepts|<pageref|auto-59>>

      <tuple|normal|partial specific quantity|<pageref|auto-71>>

      <tuple|normal|chemical potential|<pageref|auto-75>>

      <tuple|normal|partial pressure|<pageref|auto-94>>

      <tuple|normal|Dalton's Law|<pageref|auto-96>>

      <tuple|normal|standard molar entropy|<pageref|auto-109>>

      <tuple|normal|ideal mixture|<pageref|auto-145>>

      <tuple|normal|Henry's law|<pageref|auto-162>>

      <tuple|normal|Henry's law constant|<pageref|auto-164>>

      <tuple|normal|ideal-dilute solution|<pageref|auto-171>>

      <tuple|normal|standard mole fraction|<pageref|auto-183>>

      <tuple|normal|activity coefficient|<pageref|auto-217>>

      <tuple|normal|osmotic coefficient|<pageref|auto-247>>

      <tuple|normal|isopiestic vapor pressure technique|<pageref|auto-255>>

      <tuple|normal|activity|<pageref|auto-262>>

      <tuple|normal|pressure factor|<pageref|auto-273>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Composition variable>|<pageref|auto-3>>

      <tuple|<tuple|Species>|<pageref|auto-6>>

      <tuple|<tuple|Substance>|<pageref|auto-8>>

      <tuple|<tuple|Mole fraction>|<pageref|auto-11>>

      <tuple|<tuple|Mass fraction>|<pageref|auto-13>>

      <tuple|<tuple|Concentration>|<pageref|auto-15>>

      <tuple|<tuple|Binary mixture>|<pageref|auto-17>>

      <tuple|<tuple|Mixture|binary>|<pageref|auto-18>>

      <tuple|<tuple|Solution>|<pageref|auto-21>>

      <tuple|<tuple|Solvent>|<pageref|auto-23>>

      <tuple|<tuple|Solute>|<pageref|auto-25>>

      <tuple|<tuple|Molality>|<pageref|auto-27>>

      <tuple|<tuple|Binary solution>|<pageref|auto-30>>

      <tuple|<tuple|Solution|binary>|<pageref|auto-31>>

      <tuple|<tuple|Mole ratio>|<pageref|auto-32>>

      <tuple|<tuple|Composition variable|relations at infinite
      dilution>|<pageref|auto-33>>

      <tuple|<tuple|Composition variable>|<pageref|auto-35>>

      <tuple|<tuple|mixture|fixed composition>|||<tuple|Mixture|of fixed
      composition>|<pageref|auto-36>>

      <tuple|<tuple|Partial molar|quantity>|<pageref|auto-38>>

      <tuple|<tuple|partial molar|volume>||c9 sec pmq-volume
      idx1|<tuple|Partial molar|volume>|<pageref|auto-41>>

      <tuple|<tuple|System|open>|<pageref|auto-43>>

      <tuple|<tuple|Partial molar|volume|interpretation>|<pageref|auto-44>>

      <tuple|<tuple|Partial molar|volume|negative value
      of>|<pageref|auto-45>>

      <tuple|<tuple|partial molar|volume>||c9 sec pmq-volume
      idx1|<tuple|Partial molar|volume>|<pageref|auto-46>>

      <tuple|<tuple|System|open>|<pageref|auto-48>>

      <tuple|<tuple|Components, number of>|<pageref|auto-49>>

      <tuple|<tuple|Volume|total differential in an open
      system>|<pageref|auto-50>>

      <tuple|<tuple|total differential|volume>|||<tuple|Total differential|of
      the volume>|<pageref|auto-51>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-53>>

      <tuple|<tuple|Gibbs--Duhem equation>|<pageref|auto-55>>

      <tuple|<tuple|Method of intercepts>|<pageref|auto-58>>

      <tuple|<tuple|Mean molar volume>|<pageref|auto-60>>

      <tuple|<tuple|Volume|mean molar>|<pageref|auto-61>>

      <tuple|<tuple|Method of intercepts>|<pageref|auto-63>>

      <tuple|<tuple|Gibbs--Duhem equation>|<pageref|auto-64>>

      <tuple|<tuple|partial molar|quantity|general>|||<tuple|Partial
      molar|quantity|in general>|<pageref|auto-66>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-67>>

      <tuple|<tuple|Gibbs--Duhem equation>|<pageref|auto-68>>

      <tuple|<tuple|Partial|specific quantity>|<pageref|auto-70>>

      <tuple|<tuple|Partial|specific volume>|<pageref|auto-72>>

      <tuple|<tuple|chemical potential|species in a
      mixture>|||<tuple|Chemical potential|of a species in a
      mixture>|<pageref|auto-74>>

      <tuple|<tuple|System|open>|<pageref|auto-76>>

      <tuple|<tuple|total differential|gibbs energy of a
      mixture>|||<tuple|Total differential|of the Gibbs energy of a
      mixture>|<pageref|auto-77>>

      <tuple|<tuple|Gibbs energy|total differential of, for a
      mixture>|<pageref|auto-78>>

      <tuple|<tuple|Fundamental equation, Gibbs>|<pageref|auto-79>>

      <tuple|<tuple|Gibbs|fundamental equation>|<pageref|auto-80>>

      <tuple|<tuple|Electrical|neutrality>|<pageref|auto-81>>

      <tuple|<tuple|Neutrality, electrical>|<pageref|auto-82>>

      <tuple|<tuple|equilibrium conditions|multiphase multicomponent
      systems>||c9 sec pmq-equilcond-multicomp idx1|<tuple|Equilibrium
      conditions|in a multiphase multicomponent system>|<pageref|auto-84>>

      <tuple|<tuple|equilibrium conditions|multiphase multicomponent
      systems>||c9 sec pmq-equilcond-multicomp idx1|<tuple|Equilibrium
      conditions|in a multiphase multicomponent system>|<pageref|auto-85>>

      <tuple|<tuple|partial molar|quantity|general relations>||c9
      sec-pmq-relations idx1|<tuple|Partial molar|quantity|general
      relations>|<pageref|auto-87>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-88>>

      <tuple|<tuple|partial molar|quantity|general relations>||c9
      sec-pmq-relations idx1|<tuple|Partial molar|quantity|general
      relations>|<pageref|auto-89>>

      <tuple|<tuple|Partial pressure>|<pageref|auto-92>>

      <tuple|<tuple|Pressure|partial>|<pageref|auto-93>>

      <tuple|<tuple|Dalton's law>|<pageref|auto-95>>

      <tuple|<tuple|Ideal gas|mixture>|<pageref|auto-98>>

      <tuple|<tuple|Gas|ideal|mixture>|<pageref|auto-99>>

      <tuple|<tuple|partial pressure|ideal gas mixture>|||<tuple|Partial
      pressure|in an ideal gas mixture>|<pageref|auto-100>>

      <tuple|<tuple|standard state|gas mixture constituent>|||<tuple|Standard
      state|of a gas mixture constituent>|<pageref|auto-102>>

      <tuple|<tuple|chemical potential|standard|gas
      constituent>|||<tuple|Chemical potential|standard|of a gas
      constituent>|<pageref|auto-103>>

      <tuple|<tuple|Ideal gas|mixture>|<pageref|auto-105>>

      <tuple|<tuple|Gas|ideal|mixture>|<pageref|auto-106>>

      <tuple|<tuple|partial molar|entropy|ideal gas mixture>|||<tuple|Partial
      molar|entropy|in an ideal gas mixture>|<pageref|auto-107>>

      <tuple|<tuple|entropy|standard molar|gas>|||<tuple|Entropy|standard
      molar|of a gas>|<pageref|auto-108>>

      <tuple|<tuple|partial molar|enthalpy|ideal gas
      mixture>|||<tuple|Partial molar|enthalpy|in an ideal gas
      mixture>|<pageref|auto-110>>

      <tuple|<tuple|enthalpy|partial molar|ideal gas
      mixture>|||<tuple|enthalpy|partial molar|in an ideal gas
      mixture>|<pageref|auto-111>>

      <tuple|<tuple|partial molar|volume|ideal gas mixture>|||<tuple|Partial
      molar|volume|in an ideal gas mixture>|<pageref|auto-112>>

      <tuple|<tuple|volume|partial molar|ideal gas
      mixture>|||<tuple|Volume|partial molar|in an ideal gas
      mixture>|<pageref|auto-113>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-114>>

      <tuple|<tuple|fugacity|gas mixture constituent>|||<tuple|Fugacity|of a
      gas mixture constituent>|<pageref|auto-117>>

      <tuple|<tuple|fugacity coefficient|gas mixture
      constituent>|||<tuple|Fugacity coefficient|of a gas mixture
      constituent>|<pageref|auto-118>>

      <tuple|<tuple|partial molar|quantity|gas mixture
      constituent>|||<tuple|Partial molar|of a gas mixture
      constituent>|<pageref|auto-122>>

      <tuple|<tuple|virial|equation|gas mixture>|||<tuple|Virial|equation|for
      a gas mixture>|<pageref|auto-124>>

      <tuple|<tuple|equation of state|gas at low pressure>|||<tuple|Equation
      of state|of gas at low pressure>|<pageref|auto-125>>

      <tuple|<tuple|Statistical mechanics|second virial
      coefficient>|<pageref|auto-126>>

      <tuple|<tuple|equation of state|gas at low pressure>|||<tuple|Equation
      of state|of a gas at low pressure>|<pageref|auto-127>>

      <tuple|<tuple|fugacity coefficient|gas mixture
      constituent>|||<tuple|Fugacity coefficient|of a gas mixture
      constituent>|<pageref|auto-128>>

      <tuple|<tuple|Solution|solid>|<pageref|auto-130>>

      <tuple|<tuple|Raoult, Fran�ois>|<pageref|auto-132>>

      <tuple|<tuple|raoult's law|partial>|||<tuple|Raoult's law|for partial
      pressure>|<pageref|auto-134>>

      <tuple|<tuple|raoult's law|fugacity>|||<tuple|Raoult's law|for
      fugacity>|<pageref|auto-135>>

      <tuple|<tuple|raoult's law|partial>|||<tuple|Raoult's law|for partial
      pressure>|<pageref|auto-136>>

      <tuple|<tuple|raoult's law|fugacity>|||<tuple|Raoult's law|for
      fugacity>|<pageref|auto-137>>

      <tuple|<tuple|raoult's law|fugacity>|||<tuple|Raoult's law|for
      fugacity>|<pageref|auto-138>>

      <tuple|<tuple|ideal mixture|raoult's law>|||<tuple|Ideal mixture|and
      Raoult's law>|<pageref|auto-140>>

      <tuple|<tuple|mixture|ideal|raoult's law>|||<tuple|Mixture|ideal|and
      Raoult's law>|<pageref|auto-141>>

      <tuple|<tuple|ideal gas|mixture|raoult's law>|||<tuple|Ideal
      gas|mixture|and Raoult's law>|<pageref|auto-142>>

      <tuple|<tuple|Ideal mixture>|<pageref|auto-143>>

      <tuple|<tuple|mixture|ideal|chemical
      potential>|||<tuple|Mixture|ideal|and chemical
      potential>|<pageref|auto-144>>

      <tuple|<tuple|partial molar|quantity|ideal mixture>||c9
      sec-lsmn-partial-molar idx1|<tuple|Partial molar|quantity|in an ideal
      mixture>|<pageref|auto-147>>

      <tuple|<tuple|Partial molar|entropy>|<pageref|auto-148>>

      <tuple|<tuple|Entropy|partial molar>|<pageref|auto-149>>

      <tuple|<tuple|Partial molar|enthalpy>|<pageref|auto-150>>

      <tuple|<tuple|Enthalpy|partial molar>|<pageref|auto-151>>

      <tuple|<tuple|Partial molar|volume>|<pageref|auto-152>>

      <tuple|<tuple|Volume|partial molar>|<pageref|auto-153>>

      <tuple|<tuple|Partial molar|internal energy>|<pageref|auto-154>>

      <tuple|<tuple|Internal energy|partial molar>|<pageref|auto-155>>

      <tuple|<tuple|Partial molar|heat capacity at constant
      pressure>|<pageref|auto-156>>

      <tuple|<tuple|heat capacity|constant pressure|partial
      molar>|||<tuple|Heat capacity|at constant pressure|partial
      molar>|<pageref|auto-157>>

      <tuple|<tuple|partial molar|quantity|ideal mixture>||c9
      sec-lsmn-partial-molar idx1|<tuple|Partial molar|quantity|in an ideal
      mixture>|<pageref|auto-158>>

      <tuple|<tuple|Henry's law>|<pageref|auto-161>>

      <tuple|<tuple|Henry's law constant>|<pageref|auto-163>>

      <tuple|<tuple|Henry's law constant|evaluation of>|<pageref|auto-166>>

      <tuple|<tuple|Henry's law constants, relations between
      different>|<pageref|auto-167>>

      <tuple|<tuple|Ideal-dilute solution>|<pageref|auto-169>>

      <tuple|<tuple|Solution|ideal-dilute>|<pageref|auto-170>>

      <tuple|<tuple|reference state|solute>||c9 sec lsmn-ideal-dilute
      idx1|<tuple|Reference state|of a solute>|<pageref|auto-172>>

      <tuple|<tuple|solute|reference state>||c9 sec lsmn-ideal-dilute
      idx2|<tuple|Solute|reference state>|<pageref|auto-173>>

      <tuple|<tuple|Standard|composition>|<pageref|auto-174>>

      <tuple|<tuple|Standard|concentration>|<pageref|auto-175>>

      <tuple|<tuple|Concentration|standard>|<pageref|auto-176>>

      <tuple|<tuple|Standard|molality>|<pageref|auto-177>>

      <tuple|<tuple|Molality|standard>|<pageref|auto-178>>

      <tuple|<tuple|reference state|solute>||c9 sec lsmn-ideal-dilute
      idx1|<tuple|Reference state|of a solute>|<pageref|auto-179>>

      <tuple|<tuple|solute|reference state>||c9 sec lsmn-ideal-dilute
      idx2|<tuple|Solute|reference state>|<pageref|auto-180>>

      <tuple|<tuple|Standard|mole fraction>|<pageref|auto-181>>

      <tuple|<tuple|Mole fraction|standard>|<pageref|auto-182>>

      <tuple|<tuple|solvent|behavior in an ideal-dilute solution>||c9
      sec-lsmn-ideal-dilute-solvent idx1|<tuple|Solvent|behavior in an
      ideal\Udilute solution>|<pageref|auto-185>>

      <tuple|<tuple|ideal-dilute solution|solvent behavior in>||c9
      sec-lsmn-ideal-dilute-solvent idx2|<tuple|Ideal\Udilute
      solution|solvent behavior in>|<pageref|auto-186>>

      <tuple|<tuple|Gibbs--Duhem equation>|<pageref|auto-187>>

      <tuple|<tuple|raoult's law|fugacity|ideal dilute>|||<tuple|Raoult's
      law|for fugacity|in an ideal\Udilute solution>|<pageref|auto-188>>

      <tuple|<tuple|solvent|behavior in an ideal-dilute solution>||c9
      sec-lsmn-ideal-dilute-solvent idx1|<tuple|Solvent|behavior in an
      ideal\Udilute solution>|<pageref|auto-190>>

      <tuple|<tuple|ideal-dilute solution|solvent behavior in>||c9
      sec-lsmn-ideal-dilute-solvent idx2|<tuple|Ideal\Udilute
      solution|solvent behavior in>|<pageref|auto-191>>

      <tuple|<tuple|partial molar|quantity|ideal-dilute solution>||c9
      sec-lsmn-ideal-dilute-partial-molar idx1|<tuple|Partial
      molar|quantity|in an ideal\Udilute solution>|<pageref|auto-193>>

      <tuple|<tuple|ideal-dilute solution|partial molar quantities in>||c9
      sec-lsmn-ideal-dilute-partial-molar idx2|<tuple|Ideal\Udilute
      solution|partial molar quantities in>|<pageref|auto-194>>

      <tuple|<tuple|partial molar|entropy|solute in an ideal-dilute
      solution>|||<tuple|Partial molar|entropy|of a solute in an
      ideal\Udilute solution>|<pageref|auto-196>>

      <tuple|<tuple|entropy|partial molar|solute in an ideal-dilute
      solution>|||<tuple|Entropy|partial molar|of a solute in an
      ideal\Udilute solution>|<pageref|auto-197>>

      <tuple|<tuple|partial molar|enthalpy|solute in an ideal-dilute
      solution>|||<tuple|Partial molar|enthalpy|of a solute in an
      ideal\Udilute solution>|<pageref|auto-198>>

      <tuple|<tuple|enthalpy|partial molar|solute in an ideal-dilute
      solution>|||<tuple|Enthalpy|partial molar|of a solute in an
      ideal\Udilute solution>|<pageref|auto-199>>

      <tuple|<tuple|partial molar|quantity|ideal-dilute solution>||c9
      sec-lsmn-ideal-dilute-partial-molar idx1|<tuple|Partial
      molar|quantity|in an ideal\Udilute solution>|<pageref|auto-200>>

      <tuple|<tuple|ideal-dilute solution|partial molar quantities in>||c9
      sec-lsmn-ideal-dilute-partial-molar idx2|<tuple|Ideal\Udilute
      solution|partial molar quantities in>|<pageref|auto-201>>

      <tuple|<tuple|Henry, William>|<pageref|auto-203>>

      <tuple|<tuple|Activity coefficient>|<pageref|auto-205>>

      <tuple|<tuple|Reference state>|<pageref|auto-206>>

      <tuple|<tuple|Chemical potential|standard>|<pageref|auto-208>>

      <tuple|<tuple|ideal mixture>||c9 sec acmn-mixtures-ideal
      idx1|<tuple|Ideal mixture>|<pageref|auto-211>>

      <tuple|<tuple|mixture|ideal>||c9 sec acmn-mixtures-ideal
      idx2|<tuple|Mixture|ideal>|<pageref|auto-212>>

      <tuple|<tuple|ideal mixture>||c9 sec acmn-mixtures-ideal
      idx1|<tuple|Ideal mixture>|<pageref|auto-213>>

      <tuple|<tuple|mixture|ideal>||c9 sec acmn-mixtures-ideal
      idx2|<tuple|Mixture|ideal>|<pageref|auto-214>>

      <tuple|<tuple|Activity coefficient>|<pageref|auto-216>>

      <tuple|<tuple|reference state|mixture constituent>|||<tuple|Reference
      state|of a mixture constituent>|<pageref|auto-218>>

      <tuple|<tuple|reference state|solvent>|||<tuple|Reference state|of a
      solvent>|<pageref|auto-219>>

      <tuple|<tuple|IUPAC Green Book>|<pageref|auto-220>>

      <tuple|<tuple|reference state|solute>|||<tuple|Reference state|of a
      solute>|<pageref|auto-221>>

      <tuple|<tuple|Solute|reference state>|<pageref|auto-222>>

      <tuple|<tuple|Activity coefficient|approach to
      unity>|<pageref|auto-223>>

      <tuple|<tuple|activity coefficient|solute|dilute
      solution>|||<tuple|Activity coefficient|of a solute|in dilute
      solution>|<pageref|auto-225>>

      <tuple|<tuple|Statistical mechanics|ideal mixture>|<pageref|auto-226>>

      <tuple|<tuple|mcmillan mayer>|||<tuple|McMillan\UMayer
      theory>|<pageref|auto-227>>

      <tuple|<tuple|Statistical mechanics|McMillan--Mayer
      theory>|<pageref|auto-228>>

      <tuple|<tuple|activity coefficient|solvent|gas fugacity>||c9
      sec-eac-gas-fugacity idx1|<tuple|Activity coefficient|of a solvent|from
      gas fugacity>|<pageref|auto-231>>

      <tuple|<tuple|activity coefficient|solute|gas fugacity>||c9
      sec-eac-gas-fugacity idx2|<tuple|Activity coefficient|of a solute|from
      gas fugacity>|<pageref|auto-232>>

      <tuple|<tuple|activity coefficient|solvent|gas fugacity>||c9
      sec-eac-gas-fugacity idx1|<tuple|Activity coefficient|of a solvent|from
      gas fugacity>|<pageref|auto-237>>

      <tuple|<tuple|activity coefficient|solute|gas fugacity>||c9
      sec-eac-gas-fugacity idx2|<tuple|Activity coefficient|of a solute|from
      gas fugacity>|<pageref|auto-238>>

      <tuple|<tuple|activity coefficient|gibb-duhem>||c9 sec-eac-gibbs-duhem
      idx1|<tuple|Activity coefficient|from the Gibbs\UDuhem
      equation>|<pageref|auto-240>>

      <tuple|<tuple|gibbs-duhem equation>||c9 sec-eac-gibbs-duhem
      idx2|<tuple|Gibbs\UDuhem equation>|<pageref|auto-241>>

      <tuple|<tuple|activity coefficient|gibb-duhem>||c9 sec-eac-gibbs-duhem
      idx1|<tuple|Activity coefficient|from the Gibbs\UDuhem
      equation>|<pageref|auto-242>>

      <tuple|<tuple|gibbs-duhem equation>||c9 sec-eac-gibbs-duhem
      idx2|<tuple|Gibbs\UDuhem equation>|<pageref|auto-243>>

      <tuple|<tuple|activity coefficient|osmotic coefficient>||c9
      sec-eac-osmotic idx1|<tuple|Activity coefficient|from the osmotic
      coefficient>|<pageref|auto-245>>

      <tuple|<tuple|Osmotic coefficient>|<pageref|auto-246>>

      <tuple|<tuple|Gibbs--Duhem equation>|<pageref|auto-250>>

      <tuple|<tuple|activity coefficient|osmotic coefficient>||c9
      sec-eac-osmotic idx1|<tuple|Activity coefficient|from the osmotic
      coefficient>|<pageref|auto-252>>

      <tuple|<tuple|Isopiestic|vapor pressure technique>|<pageref|auto-254>>

      <tuple|<tuple|Isopiestic|solution>|<pageref|auto-256>>

      <tuple|<tuple|Osmotic coefficient|evaluation>|<pageref|auto-257>>

      <tuple|<tuple|Lewis, Gilbert Newton>|<pageref|auto-259>>

      <tuple|<tuple|Activity>|<pageref|auto-261>>

      <tuple|<tuple|Chemical potential|standard>|<pageref|auto-263>>

      <tuple|<tuple|Relative activity>|<pageref|auto-264>>

      <tuple|<tuple|Activity|relative>|<pageref|auto-265>>

      <tuple|<tuple|standard state|mixture component>|||<tuple|Standard
      state|of a mixture component>|<pageref|auto-267>>

      <tuple|<tuple|Pressure factor>|<pageref|auto-269>>

      <tuple|<tuple|Pitzer, Kenneth>|<pageref|auto-270>>

      <tuple|<tuple|Brewer, Leo>|<pageref|auto-271>>

      <tuple|<tuple|Pressure factor>|<pageref|auto-272>>

      <tuple|<tuple|activity|gas>|||<tuple|Activity|of a
      gas>|<pageref|auto-274>>

      <tuple|<tuple|activity|pure liquid or solid>|||<tuple|Activity|of a
      pure liquid or solid>|<pageref|auto-275>>

      <tuple|<tuple|activity|mixture constituent>|||<tuple|Activity|of a
      mixture constituent>|<pageref|auto-276>>

      <tuple|<tuple|activity|solvent>|||<tuple|Activity|of a
      solvent>|<pageref|auto-277>>

      <tuple|<tuple|activity|solute>|||<tuple|Activity|of a
      solute>|<pageref|auto-278>>

      <tuple|<tuple|pressure factor>||c9 sec aus-pressure
      idx1|<tuple|Pressure factor>|<pageref|auto-281>>

      <tuple|<tuple|Standard|pressure>|<pageref|auto-283>>

      <tuple|<tuple|Pressure|standard>|<pageref|auto-284>>

      <tuple|<tuple|pressure factor>||c9 sec aus-pressure
      idx1|<tuple|Pressure factor>|<pageref|auto-285>>

      <tuple|<tuple|Gravitational|field>|<pageref|auto-287>>

      <tuple|<tuple|Field|gravitational>|<pageref|auto-288>>

      <tuple|<tuple|Centrifuge>|<pageref|auto-289>>

      <tuple|<tuple|mixture|gas, in a gravitational field>||c9
      sec-mgcf-gas-gravity idx1|<tuple|Mixture|gas, in a gravitational
      field>|<pageref|auto-291>>

      <tuple|<tuple|ideal gas|mixture|gravitational field>||c9
      sec-mgcf-gas-gravity idx2|<tuple|Ideal gas|mixture|in a gravitational
      field>|<pageref|auto-292>>

      <tuple|<tuple|equilibrium conditions|gas mixture>||c9
      sec-mgcf-gas-gravity idx3|<tuple|Equilibrium conditions|for a gas
      mixture in a gravitational field>|<pageref|auto-293>>

      <tuple|<tuple|Lab frame>|<pageref|auto-294>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-295>>

      <tuple|<tuple|Barometric formula>|<pageref|auto-296>>

      <tuple|<tuple|mixture|gas, in a gravitational field>||c9
      sec-mgcf-gas-gravity idx1|<tuple|Mixture|gas, in a gravitational
      field>|<pageref|auto-297>>

      <tuple|<tuple|ideal gas|mixture|gravitational field>||c9
      sec-mgcf-gas-gravity idx2|<tuple|Ideal gas|mixture|in a gravitational
      field>|<pageref|auto-298>>

      <tuple|<tuple|equilibrium conditions|gas mixture>||c9
      sec-mgcf-gas-gravity idx3|<tuple|Equilibrium conditions|for a gas
      mixture in a gravitational field>|<pageref|auto-299>>

      <tuple|<tuple|solution|centrifuge cell>||c9 sec-mgcf-liq-centrifuge
      idx1|<tuple|Solution|in a centrifuge cell>|<pageref|auto-301>>

      <tuple|<tuple|equilibrium conditions|solution in a centrifuge>||c9
      sec-mgcf-liq-centrifuge idx2|<tuple|Equilibrium conditions|for a
      solution in a centrifuge>|<pageref|auto-302>>

      <tuple|<tuple|centrifuge|cell>||c9 sec-mgcf-liq-centrifuge
      idx3|<tuple|Centrifuge|cell>|<pageref|auto-303>>

      <tuple|<tuple|ultracentrifuge>||c9 sec-mgcf-liq-centrifuge
      idx4|<tuple|Ultracentrifuge>|<pageref|auto-304>>

      <tuple|<tuple|Lab frame>|<pageref|auto-305>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-306>>

      <tuple|<tuple|Local frame>|<pageref|auto-307>>

      <tuple|<tuple|Frame|local>|<pageref|auto-308>>

      <tuple|<tuple|Rotating local frame>|<pageref|auto-310>>

      <tuple|<tuple|Frame|local|rotating>|<pageref|auto-311>>

      <tuple|<tuple|Rotating local frame>|<pageref|auto-312>>

      <tuple|<tuple|Frame|local|rotating>|<pageref|auto-313>>

      <tuple|<tuple|Coriolis force>|<pageref|auto-314>>

      <tuple|<tuple|Force|Coriolis>|<pageref|auto-315>>

      <tuple|<tuple|Force|apparent>|<pageref|auto-316>>

      <tuple|<tuple|Force|fictitious>|<pageref|auto-317>>

      <tuple|<tuple|Local frame>|<pageref|auto-318>>

      <tuple|<tuple|Frame|local>|<pageref|auto-319>>

      <tuple|<tuple|Sedimentation equilibrium>|<pageref|auto-320>>

      <tuple|<tuple|molar|mass|sedimentation
      equilibrium>|||<tuple|Molar|mass|from sedimentation
      equilibrium>|<pageref|auto-321>>

      <tuple|<tuple|solution|centrifuge cell>||c9 sec-mgcf-liq-centrifuge
      idx1|<tuple|Solution|in a centrifuge cell>|<pageref|auto-322>>

      <tuple|<tuple|equilibrium conditions|solution in a centrifuge>||c9
      sec-mgcf-liq-centrifuge idx2|<tuple|Equilibrium conditions|for a
      solution in a centrifuge>|<pageref|auto-323>>

      <tuple|<tuple|centrifuge|cell>||c9 sec-mgcf-liq-centrifuge
      idx3|<tuple|Centrifuge|cell>|<pageref|auto-324>>

      <tuple|<tuple|ultracentrifuge>||c9 sec-mgcf-liq-centrifuge
      idx4|<tuple|Ultracentrifuge>|<pageref|auto-325>>
    </associate>
    <\associate|parts>
      <tuple|bio-HENRY.tm|chapter-nr|9|section-nr|4|subsection-nr|7>

      <tuple|bio-LEWIS.tm|chapter-nr|9|section-nr|6|subsection-nr|4>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.3.1>|>
        <assign|the-label|<quote|<tuple|partial molar|quantity|gas mixture
        constituent>>><flag|index|dark green|key><assign|auto-nr|121><write|idx|<tuple|<tuple|partial
        molar|quantity|gas mixture constituent>|||<tuple|Partial molar|of a
        gas mixture constituent>|<pageref|auto-121>>>Gas mixture: expressions
        for differences between partial molar and standard molar quantities
        of constituent <with|mode|<quote|math>|i>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7ED5FBCB8--7FDEFB588>|<link|hyperlink|<id|%-7ED5FBCB8--7FDEFB588>|<url|#footnr-9.3.2>>|9.3.2>.
        |<hidden-binding|<tuple|footnote-9.3.2>|9.3.2>|<with|mode|<quote|math>|B>
        and <with|mode|<quote|math>|B<rprime|'><rsub|i>> are defined by Eqs.
        <reference|B=sum(i)sum(j)y(i)y(j)B(ij)> and <reference|Bi'=2 sum yj
        Bij-B>>>>
      </surround>|<pageref|auto-121>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.4.1>|>
        Partial molar quantities of solvent and nonelectrolyte solute in an
        ideal-dilute solution
      </surround>|<pageref|auto-195>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.5.1>|>
        Reference states for nonelectrolyte constituents of mixtures. In each
        reference state, the temperature and pressure are the same as those
        of the mixture.

        \;

        <rsup|a>A hypothetical state.
      </surround>|<pageref|auto-209>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.6.1>|>
        Activity coefficients as functions of fugacity. For a constituent of
        a condensed-phase mixture, <with|mode|<quote|math>|f<rsub|i>>,
        <with|mode|<quote|math>|f<rsub|<with|mode|<quote|text>|A>>>, and
        <with|mode|<quote|math>|f<rsub|<with|mode|<quote|text>|B>>> refer to
        the fugacity in a gas phase equilibrated with the condensed phase.
      </surround>|<pageref|auto-235>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.7.1>|>
        Expressions for activities of nonelectrolytes. For a constituent of a
        condensed-phase mixture, <with|mode|<quote|math>|f<rsub|i>>,
        <with|mode|<quote|math>|f<rsub|<with|mode|<quote|text>|A>>>, and
        <with|mode|<quote|math>|f<rsub|<with|mode|<quote|text>|B>>> refer to
        the fugacity in a gas phase equilibrated with the condensed phase.
      </surround>|<pageref|auto-279>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.7.2>|>
        Expressions for the dependence of pressure factors of nonelectrolytes
        on pressure. The approximate expressions assume the phase is
        incompressible, or the solute partial molar volume is independent of
        pressure.
      </surround>|<pageref|auto-282>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|9<space|2spc>Mixtures>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      9.1<space|2spc>Composition Variables
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      <with|par-left|<quote|1tab>|9.1.1<space|2spc>Species and substances
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|9.1.2<space|2spc>Mixtures in general
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|1tab>|9.1.3<space|2spc>Solutions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-20>>

      <with|par-left|<quote|1tab>|9.1.4<space|2spc>Binary solutions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-29>>

      <with|par-left|<quote|1tab>|9.1.5<space|2spc>The composition of a
      mixture <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-34>>

      9.2<space|2spc>Partial Molar Quantities
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-37>

      <with|par-left|<quote|1tab>|9.2.1<space|2spc>Partial molar volume
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-40>>

      <with|par-left|<quote|1tab>|9.2.2<space|2spc>The total differential of
      the volume in an open system <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-47>>

      <with|par-left|<quote|1tab>|9.2.3<space|2spc>Evaluation of partial
      molar volumes in binary mixtures <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-57>>

      <with|par-left|<quote|1tab>|9.2.4<space|2spc>General relations
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-65>>

      <with|par-left|<quote|1tab>|9.2.5<space|2spc>Partial specific
      quantities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-69>>

      <with|par-left|<quote|1tab>|9.2.6<space|2spc>The chemical potential of
      a species in a mixture <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-73>>

      <with|par-left|<quote|1tab>|9.2.7<space|2spc>Equilibrium conditions in
      a multiphase, multicomponent system
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-83>>

      <with|par-left|<quote|1tab>|9.2.8<space|2spc>Relations involving
      partial molar quantities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-86>>

      9.3<space|2spc>Gas Mixtures <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-90>

      <with|par-left|<quote|1tab>|9.3.1<space|2spc>Partial pressure
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-91>>

      <with|par-left|<quote|1tab>|9.3.2<space|2spc>The ideal gas mixture
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-97>>

      <with|par-left|<quote|1tab>|9.3.3<space|2spc>Partial molar quantities
      in an ideal gas mixture <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-101>>

      <with|par-left|<quote|1tab>|9.3.4<space|2spc>Real gas mixtures
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-115>>

      <with|par-left|<quote|2tab>|9.3.4.1<space|2spc>Fugacity
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-116>>

      <with|par-left|<quote|2tab>|9.3.4.2<space|2spc>Partial molar quantities
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-119>>

      <with|par-left|<quote|2tab>|9.3.4.3<space|2spc>Equation of state
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-123>>

      9.4<space|2spc>Liquid and Solid Mixtures of Nonelectrolytes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-129>

      <with|par-left|<quote|1tab>|9.4.1<space|2spc>Raoult's law
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-131>>

      <with|par-left|<quote|1tab>|9.4.2<space|2spc>Ideal mixtures
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-139>>

      <with|par-left|<quote|1tab>|9.4.3<space|2spc>Partial molar quantities
      in ideal mixtures <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-146>>

      <with|par-left|<quote|1tab>|9.4.4<space|2spc>Henry's law
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-159>>

      <with|par-left|<quote|1tab>|9.4.5<space|2spc>The ideal-dilute solution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-168>>

      <with|par-left|<quote|1tab>|9.4.6<space|2spc>Solvent behavior in the
      ideal-dilute solution <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-184>>

      <with|par-left|<quote|1tab>|9.4.7<space|2spc>Partial molar quantities
      in the ideal-dilute solution <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-192>>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|William
      Henry> (1774\U1836) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-202><vspace|0.15fn>>

      9.5<space|2spc>Activity Coefficients in Mixtures of Nonelectrolytes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-204>

      <with|par-left|<quote|1tab>|9.5.1<space|2spc>Reference states and
      standard states <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-207>>

      <with|par-left|<quote|1tab>|9.5.2<space|2spc>Ideal mixtures
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-210>>

      <with|par-left|<quote|1tab>|9.5.3<space|2spc>Real mixtures
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-215>>

      <with|par-left|<quote|1tab>|9.5.4<space|2spc>Nonideal dilute solutions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-224>>

      9.6<space|2spc>Evaluation of Activity Coefficients
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-229>

      <with|par-left|<quote|1tab>|9.6.1<space|2spc>Activity coefficients from
      gas fugacities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-230>>

      <with|par-left|<quote|2tab>|9.6.1.1<space|2spc>Examples
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-233>>

      <with|par-left|<quote|1tab>|9.6.2<space|2spc>Activity coefficients from
      the Gibbs\UDuhem equation <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-239>>

      <with|par-left|<quote|1tab>|9.6.3<space|2spc>Activity coefficients from
      osmotic coefficients <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-244>>

      <with|par-left|<quote|2tab>|9.6.3.1<space|2spc>Evaluation of
      <with|mode|<quote|math>|\<phi\><rsub|m>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-248>>

      <with|par-left|<quote|2tab>|9.6.3.2<space|2spc>Use of
      <with|mode|<quote|math>|\<phi\><rsub|m>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-249>>

      <with|par-left|<quote|1tab>|9.6.4<space|2spc>Fugacity measurements
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-253>>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Gilbert
      Newton Lewis> (1875\U1946) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-258><vspace|0.15fn>>

      9.7<space|2spc>Activity of an Uncharged Species
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-260>

      <with|par-left|<quote|1tab>|9.7.1<space|2spc>Standard states
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-266>>

      <with|par-left|<quote|1tab>|9.7.2<space|2spc>Activities and composition
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-268>>

      <with|par-left|<quote|1tab>|9.7.3<space|2spc>Pressure factors and
      pressure <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-280>>

      9.8<space|2spc>Mixtures in Gravitational and Centrifugal Fields
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-286>

      <with|par-left|<quote|1tab>|9.8.1<space|2spc>Gas mixture in a
      gravitational field <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-290>>

      <with|par-left|<quote|1tab>|9.8.2<space|2spc>Liquid solution in a
      centrifuge cell <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-300>>
    </associate>
  </collection>
</auxiliary>