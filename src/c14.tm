<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <chapter|Galvanic Cells><label|Chap. 14><label|c14>

  An <subindex|Electrochemical|cell><subindex|Cell|electrochemical><em|electrochemical
  cell> is a system in which passage of an electric current through an
  electrical circuit is linked to an internal cell reaction. A
  <index|Galvanic cell><subindex|Cell|galvanic><newterm|galvanic cell>, or
  voltaic cell, is an electrochemical cell that, when isolated, has an
  electric potential difference between its terminals; the cell is said to be
  a <em|seat of electromotive force>.

  The cell reaction in a galvanic cell differs in a fundamental way from the
  same reaction (i.e., one with the same reaction equation) taking place in a
  reaction vessel that is not part of an electrical circuit. In the reaction
  vessel, the reactants and products are in the same phase or in phases in
  contact with one another, and the reaction advances in the spontaneous
  direction until reaction equilibrium is reached. This reaction is the
  <em|direct reaction>.

  The galvanic cell, in contrast, is arranged with the reactants physically
  separated from one another so that the cell reaction can advance only when
  an <subindex|Electric|current><index|Current, electric>electric current
  passes through the cell. If there is no current, the cell reaction is
  constrained from taking place. When the electrical circuit is open and the
  cell is isolated from its surroundings, a state of thermal, mechanical, and
  transfer equilibrium is rapidly reached. In this state of <em|cell
  equilibrium> or <em|electrochemical equilibrium>, however, reaction
  equilibrium is not necessarily present\Vthat is, if the reactants and
  products were moved to a reaction vessel at the same activities, there
  might be spontaneous advancement of the reaction.

  As will be shown, measurements of the cell potential of a galvanic cell are
  capable of yielding precise values of molar reaction quantities of the cell
  reaction and thermodynamic equilibrium constants, and of mean ionic
  activity coefficients in electrolyte solutions.

  <section|Cell Diagrams and Cell Reactions><label|14-cell rxns><label|c14
  sec cdcr>

  <subsection|Elements of a galvanic cell><label|c14 sec cdcr-elements>

  We will treat a galvanic cell as a <em|system>. The cell has two metal
  wires called <em|terminals> that pass through the system boundary. Within
  the cell are phases that can conduct an electric current and are
  collectively called <subindex|Electrical|conductor of a galvanic
  cell><em|electrical conductors>. Each terminal is attached to an
  <subindex|Electron|conductor of a galvanic cell><em|electron conductor>
  that is usually a metal, but might also be graphite or a semiconductor.
  Each electron conductor is in contact with an <index|Ionic conductor of a
  galvanic cell><em|ionic conductor>, usually an electrolyte solution,
  through which ions but not electrons can move. Both of the electron
  conductors can be in contact with the same ionic conductor; or they can be
  in contact with separate ionic conductors, in which case the ionic
  conductors contact one another at a <index|Liquid junction><em|liquid
  junction>. The general arrangement of the physical elements of a galvanic
  cell is therefore

  <htab|5mm>terminal \U electron conductor \U ionic conductor(s) \U electron
  conductor \U terminal<htab|5mm>

  Both terminals must be the same metal (usually copper) in order for it to
  be possible to measure the electric potential difference between them.

  The combination of an electron conductor and the ionic conductor in contact
  with it is called an <index|Electrode><newterm|electrode>,<footnote|The
  term \Pelectrode\Q is sometimes used to refer to just the electron
  conductor.> or half-cell. To describe a galvanic cell, it is conventional
  to distinguish the <em|left> and <em|right> electrodes. In this way, we
  establish a left\Uright association with the reactants and products of the
  reactions at the electrodes.

  <subsection|Cell diagrams><label|c14 sec cdcr-diagrams>

  Consider the galvanic cell depicted in Fig. <vpageref|fig:14-galvanic
  cell>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|14-SUP/H2-AGCL.eps|147pt|152pt||>>
        <label|fig:14-galvanic cell>A galvanic cell without liquid junction.
      </big-figure>
    </framed>
  </float> This cell has a hydrogen electrode at the left and a
  silver\Usilver chloride electrode at the right. The <index|Hydrogen
  electrode><subindex|Electrode|hydrogen>hydrogen electrode is a strip of
  platinum in contact with hydrogen gas and with aqueous hydrochloric acid,
  which is the ionic conductor. In the type of hydrogen electrode shown in
  the figure, hydrogen gas is introduced through a side tube into a
  closed-end glass jacket that surrounds the platinum strip and is immersed
  in the hydrochloric acid; the gas bubbles out through holes near the bottom
  of the tube. The silver\Usilver chloride electrode is a silver strip or
  wire that dips into the hydrochloric acid and is coated with solid silver
  chloride.

  The cell in Fig. <reference|fig:14-galvanic cell> is compactly described by
  the following <subindex|Cell|diagram><em|cell diagram>:

  <\equation*>
    <text|Cu><jn><text|Pt><jn><text|H<rsub|2>>
    <around|(|<text|g>|)><jn><text|H><rsupout|+>
    <around|(|<text|aq>|)>,<text|Cl><rsupout|>
    <around|(|<text|aq>|)><jn><text|AgCl>
    <around|(|<text|s>|)><jn><text|Ag><jn><text|Cu>
  </equation*>

  A cell diagram indicates which electrode is at the left and which is at the
  right, and shows the reactants and products of the two electrode reactions.
  A single vertical bar represents a phase boundary<\footnote>
    Transcriber note: The glyph for Unicode point \ <verbatim|U+007C,
    VERTICAL LINE> is used, although the glyph recommended in the IUPAC Green
    Book differs. See <strong|Quantities, Units, and Symbols in Physical
    Chemistry,> IUPAC Green Book, 3rd edition,
    <hlink|<verbatim|https://iupac.org/what-we-do/books/greenbook/>|https://iupac.org/what-we-do/books/greenbook/>
    , Section \P<math|>2.13.1 Sign and notation conventions in
    electrochemistry\Q.
  </footnote>. Commas are used to separate different species in the same
  phase.

  The same cell can be described by a slightly different cell diagram that
  omits the copper terminals seen in the figure and shows the electrolyte
  solute instead of the ion species:

  <\equation*>
    <text|Pt><jn><text|H<rsub|2>> <around|(|<text|g>|)><jn><text|HCl>
    <around|(|<text|aq>|)><jn><text|AgCl> <around|(|<text|s>|)><jn><text|Ag>
  </equation*>

  The reason it is not necessary to include the terminals is that the
  property whose value we seek, the zero-current cell potential, is the same
  regardless of the metal used for the terminals.

  <subsection|Electrode reactions and the cell reaction><label|c14
  sec-cdcr-reactions>

  A cell diagram, with its designation of the left and right electrodes,
  allows us to write reaction equations for the cell. These equations are
  written according to the convention that electrons enter at the right
  terminal and leave at the left terminal.

  At each electrode there is an <subindex|Electrode|reaction><newterm|electrode
  reaction>, or half-reaction, one for reduction at the right electrode and
  the other for oxidation at the left electrode. The reaction equations for
  the electrode reactions include electrons as either a reactant (at the
  right terminal) or a product (at the left terminal). The
  <subindex|Cell|reaction><subindex|Reaction|cell><newterm|cell reaction>
  describes the overall chemical change; its reaction equation is the sum of
  the equations for the two electrode reactants with cancellation of the
  electrons.

  For instance, we can write the electrode reactions of the cell of Fig.
  <reference|fig:14-galvanic cell> as follows.

  <\eqnarray*>
    <tformat|<table|<row|<cell|<text|oxidation at
    left:>>|<cell|<text|<space|2em>>>|<cell|<text|H<rsub|2>>
    <around|(|<text|g>|)> <arrow>2*<text|H><rsupout|+>
    <around|(|<text|aq>|)>+2*<text|e><rsupout|->>>|<row|<cell|<text|reduction
    at right:>>|<cell|>|<cell|2*<text|AgCl><around|(|s|)>+2*<text|e><rsupout|->
    <arrow>2*<text|Ag><around|(|s|)>+2*<text|Cl><rsupout|-><around|(|aq|)>>>>>
  </eqnarray*>

  As written here, the stoichiometric numbers of the electrons have the same
  absolute value (2) in both reaction equations. This allows the electrons to
  cancel when we add the electrode reactions to form the cell reaction:

  <\equation*>
    <text|H<rsub|2>> <around|(|<text|g>|)>+2*<text|AgCl><around|(|s|)><arrow>2*<text|H><rsupout|+>
    <around|(|<text|aq>|)>+2*<text|Cl><rsupout|->
    <around|(|<text|aq>|)>+2*<text|Ag> <around|(|<text|s>|)>
  </equation*>

  The cell of Fig. <reference|fig:14-galvanic cell> has a single electrolyte
  phase with essentially the same composition at both electrodes, and is an
  example of a <subindex|Cell|without liquid junction><em|cell without liquid
  junction> or <subindex|Cell|without transference><em|cell without
  transference>. As an example of a <subindex|Cell|with transference><em|cell
  with transference>, consider the cell diagram

  <\equation*>
    <text|Zn><jn><text|Zn><rsupout|2+> <around|(|<text|aq>|)><ljn>C*u<rsup|2+>*<around|(|a*q|)><jn>C*u
  </equation*>

  This is the zinc\Ucopper cell depicted in Fig. <vpageref|fig:14-Zn-Cu
  cell>, sometimes called a Daniell cell.<\float|float|thb>
    <\framed>
      <\big-figure|<image|14-SUP/Zn-Cu_cell.eps|285pt|130pt||>>
        <label|fig:14-Zn-Cu cell>Zinc\Ucopper galvanic cell with porous
        barrier (heavy dashed line) separating two electrolyte solutions. The
        dashed rectangle indicates the system boundary.

        <\enumerate-alpha>
          <item>Open circuit with isolated system in equilibrium state.

          <item>Closed circuit.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float> The two electrolyte phases are separated by a <index|Liquid
  junction>liquid junction represented in the cell diagram by the dashed
  vertical bar<\footnote>
    Transcriber note: The glyph for Unicode point <verbatim|U+00A6> is used
    to represent a single dashed vertical bar:
  </footnote>. If the <subindex|Liquid junction|potential>liquid junction
  potential can be assumed to be negligible, the liquid junction is instead
  represented by a pair of dashed vertical bars<\footnote>
    Transcriber note: The glyph for Unicode point <verbatim|U+00A6> is used
    to construct the pair of dashed vertical bars.
  </footnote>:

  <\equation*>
    <text|Zn><jn><text|Zn><rsupout|2+> <around|(|<text|aq>|)><lljn><text|Cu><rsupout|2+>
    <around|(|<text|aq>|)><jn><text|Cu>
  </equation*>

  <subsection|Advancement and charge><label|c14 sec cdcr-charge>

  The <subindex|Electron|number><newterm|electron number> or charge number,
  <math|z>, of the cell reaction is defined as the amount of electrons
  entering at the right terminal per unit advancement of the cell reaction.
  <math|z> is a positive dimensionless quantity equal to
  <math|<around|\||\<nu\><rsub|<text|e>>|\|>>, where
  <math|\<nu\><rsub|<text|e>>> is the stoichiometric number of the electrons
  in either of the electrode reactions whose sum is the cell reaction.

  Because both electrode reactions are written with the same value of
  <math|<around|\||\<nu\><rsub|<text|e>>|\|>>, the advancements of these
  reactions and of the cell reaction are all described by the same
  advancement variable <math|\<xi\>>. For an infinitesimal change
  <math|<dif>\<xi\>>, an amount of electrons equal to <math|z<dif>\<xi\>>
  enters the system at the right terminal, an equal amount of electrons
  leaves at the left terminal, and there is no buildup of charge in any of
  the internal phases.

  The<label|Faraday const><index|Faraday constant><newterm|Faraday constant>
  <math|F> is a physical constant defined as the charge per amount of
  protons, and is equal to the product of the elementary charge (the charge
  of a proton) and the Avogadro constant: <math|F=e*N<rsub|<text|A>>>. Its
  value to five significant figures is <math|F=96\<comma\>485
  <text|Cmol><per>>. The charge per amount of electrons is <math|-F>. Thus,
  the charge entering the right terminal during advancement
  <math|<dif>\<xi\>> is

  <\equation>
    <label|dQ=-zFdxi><dQ><sys>=-z*F<dif>\<xi\>
  </equation>

  <section|Electric Potentials in the Cell><label|c14 sec epc>

  As explained at the beginning of Sec. <reference|3-electrical work>, the
  <subindex|Electric|potential><subindex|Potential|electric><newterm|electric
  potential> <math|\<phi\>> at a point in space is defined as the change in
  the electrical potential energy of an infinitesimal test charge when it is
  brought to this point from a position infinitely far from other charges,
  divided by the <subindex|Electric|charge><subindex|Charge|electric>charge.

  We are concerned with the electric potential within a phase\Vthe
  <subsubindex|Electric|potential|inner><index|Inner electric potential>inner
  electric potential, or <index|Galvani potential><em|Galvani potential>. We
  can measure the difference between the values of this electric potential in
  the two terminals of a galvanic cell, provided the terminals have the same
  chemical composition. If the terminals were of different metals, at least
  one of them would have an unknown metal\Umetal contact potential in its
  connection to the external measuring circuit.

  Since we will be applying the concept of electric potential to macroscopic
  phases, the value of the Galvani potential at a point in a phase should be
  interpreted as the <em|average> value in a small volume element at this
  point that is large enough to contain many molecules.

  <subsection|Cell potential><label|c14 sec epc-potential>

  The <index|Cell potential><newterm|cell potential> of a galvanic cell is
  the <subindex|Electric|potential difference>electric potential difference
  between terminals of the same metal, and is defined by Eq. <reference|cell
  pot defn>:

  <\equation>
    E<defn>\<phi\><rsub|<text|R>>-\<phi\><rsub|<text|L>>
  </equation>

  The subscripts R and L refer to the right and left terminals. The
  <index|Equilibrium cell potential><newterm|equilibrium cell potential>,
  <math|<Eeq>>, is the cell potential measured under conditions of zero
  current when the cell is assumed to be in an equilibrium
  state.<footnote|The equilibrium cell potential used to be called the
  <index|Electromotive force>electromotive force, or <index|Emf>emf. These
  names are deprecated by the <index|IUPAC Green Book>IUPAC Green Book (Ref.
  <cite|greenbook-3>, p. 71) because a potential difference is not a force.>

  <\quote-env>
    Over a relatively long period of time, the state of an isolated galvanic
    <abbr|>cell is found to change. Nevertheless, the assumption of an
    equilibrium state is valid if the changes are very slow compared to the
    period during which we measure <math|E>.

    The long-term changes can be of two types. If there is a liquid junction
    between electrolyte solutions of different composition, slow diffusion of
    ions through the junction is inevitable.

    In a cell without a liquid junction, the reactants of the cell reaction
    can react directly without the passage of an electric
    current.<label|direct rxn>For instance, in the cell of Fig.
    <reference|fig:14-galvanic cell> the electrolyte solution is saturated
    with respect to gaseous H<rsub|<math|2>> and solid AgCl, and therefore
    contains very small concentrations of dissolved H<rsub|<math|2>>
    molecules and Ag<rsup|<math|+>> ions. The direct reaction
    <math|<text|H><rsub|2>+2*<text|Ag><rsup|+><arrow>2*<text|H><rsup|+>+2*<text|Ag>>
    occurs irreversibly and continuously in the solution, but is slow on
    account of the low concentrations.
  </quote-env>

  It is entirely arbitrary whether we show a particular electrode at the left
  or the right of the cell diagram, although often there is a preference to
  place the electrode attached to the positive terminal at the right. If we
  exchange the positions of the two electrodes in the diagram, then we must
  reverse the reaction equations for the electrode reactions and the cell
  reaction.

  For example, it is found that the zinc\Ucopper cell of Fig.
  <reference|fig:14-Zn-Cu cell>, with typical electrolyte molalities, has its
  positive terminal at the copper electrode. When we write the cell diagram
  as

  <\equation*>
    <text|Zn><jn><text|Zn><rsup|2+>*<around|(|<text|aq>|)><lljn><text|Cu><rsup|2+>*<around|(|<text|aq>|)><jn><text|Cu>
  </equation*>

  then <math|E> and <math|<Eeq>> are positive. If we connect the two
  terminals by an external resistor as depicted in Fig.
  <reference|fig:14-Zn-Cu cell>(b), electrons will flow from the left
  terminal through the external resistor and wires to the right terminal, and
  the cell reaction

  <\equation*>
    <text|Zn>+<text|Cu><rsup|2+>*<around|(|<text|aq>|)><arrow><text|Zn><rsup|2+>*<around|(|<text|aq>|)>+<text|Cu>
  </equation*>

  will occur spontaneously in the forward direction.

  If, however, we draw the cell diagram the other way around:

  <\equation*>
    <text|Cu><jn><text|Cu><rsup|2+>*<around|(|<text|aq>|)><lljn><text|Zn><rsup|2+>*<around|(|<text|aq>|)><jn><text|Zn>
  </equation*>

  then the positive terminal is at the left, <math|E> and <math|<Eeq>> are
  negative, and electrons will flow through an external resistor from the
  right terminal to the left terminal. Since the cell reaction should show
  reduction at the right electrode and oxidation at the left, we must now
  write it as

  <\equation*>
    <text|Cu>+<text|Zn><rsup|2+>*<around|(|<text|aq>|)><arrow><text|Cu><rsup|2+>*<around|(|<text|aq>|)>+<text|Zn>
  </equation*>

  even though the arrow is not in the direction of the reaction that actually
  occurs spontaneously. In other words, the cell reaction is written
  according to the cell diagram, not according to the direction of the
  spontaneous change.

  <subsection|Measuring the equilibrium cell potential><label|c14
  sec-epc-measure>

  Figure <vpageref|fig:14-potentiometer><\float|float|thb>
    <\framed>
      <\big-figure|<image|14-SUP/POTENTIO.eps|231pt|133pt||>>
        <label|fig:14-potentiometer>Potentiometer to measure the zero-current
        cell potential of a galvanic cell.

        <\enumerate-alpha>
          <item>Galvanic cell with zero current.

          <item>Galvanic cell included in potentiometer circuit; G is a
          galvanometer.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float> shows how we can use a <index|Potentiometer to measure equilibrium
  cell potential>potentiometer to determine the equilibrium cell potential.
  Consider Fig. <reference|fig:14-potentiometer>(a). Outside the galvanic
  cell is an external circuit with a battery that allows an electric current
  to pass through a slidewire resistor. The cell's negative terminal is
  connected to the negative terminal of the battery. Since the cell is not
  part of this circuit, no current passes through the cell, and
  <math|\<phi\><rsub|<text|R>>-\<phi\><rsub|<text|L>>> is the zero-current
  cell potential <math|<Eeq>>. The left end of the slidewire is at the same
  electric potential as the left terminal of the cell.

  In the setup shown in Fig. <reference|fig:14-potentiometer>(a), the
  electric potential within the slidewire is a linear function of the
  distance from the left end. At some position along the slidewire, the
  electric potential is equal to <math|\<phi\><rsub|<text|R>>>. We can
  determine this position by connecting the right terminal of the cell to a
  slidewire contact as shown in Fig. <reference|fig:14-potentiometer>(b).
  When we place the contact at this particular position along the slidewire,
  there is no electric potential gradient in the connecting wire, and the
  galvanometer indicates a condition of zero current in the wire. It is a
  straightforward procedure to evaluate <math|\<phi\><rsub|<text|R>>-\<phi\><rsub|<text|L>>>
  from the zero-current position of the contact; this value is still equal to
  <math|<Eeq>>. When we keep the slidewire contact in this position, no
  current passes through the cell; but if we displace the contact from this
  position in either direction along the slidewire, current will pass in one
  direction or the other through the cell.

  In practice, it is more convenient to measure the zero-current cell
  potential with a high-impedance digital voltmeter (a voltmeter that draws
  negligible current) instead of with a potentiometer circuit.

  <subsection|Interfacial potential differences><label|c14
  sec-epc-potential-diff>

  What is the source of an open-circuit, zero-current cell potential? When no
  electric current passes through the cell, the electric potential must be
  uniform within each bulk phase that is an electrical conductor, because
  otherwise there would be a spontaneous movement of charged particles
  (electrons or ions) through the phase. Electric potential differences in a
  cell without current therefore exist only at phase boundaries. The
  equilibrium cell potential is the cumulative result of these potential
  differences at interfaces between different conducting phases within the
  cell.

  An interfacial potential difference appears as a vertical step in a profile
  of the Galvani potential, as shown schematically in Fig.
  <reference|fig:14-el pot profile>(a) on page <pageref|fig:14-el pot
  profile>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|14-SUP/PROFILE.eps|331pt|122pt||>>
        <label|fig:14-el pot profile>Galvani potential profile across a
        galvanic cell (schematic). LT and RT are the left and right
        terminals, LE and RE are the left and right electron conductors, and
        I is an ionic conductor such as an electrolyte solution.

        <\enumerate-alpha>
          <item>Cell with zero current.

          <item>The same cell with finite current.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float> The zero-current cell potential, <math|<Eeq>>, is the algebraic
  sum of the interfacial potential differences within the cell.

  When an external resistor is connected to the terminals to form a circuit,
  current passes through the cell and the cell performs
  <subindex|Electrical|work><subindex|Work|electrical>electrical work on the
  surroundings. Figure <reference|fig:14-el pot profile>(b) shows what
  happens to the potential profile in this case: the interfacial potential
  differences are still present within the cell, and the
  <subindex|Internal|resistance><subindex|Resistance|internal>internal
  resistance of the electrical conductors causes <math|E> to be reduced in
  magnitude compared to <math|<Eeq>>.

  We shall next look briefly at the origin and consequences of potential
  differences at interfaces between (1) two different metals, (2) a metal and
  an electrolyte solution, and (3) two different electrolyte solutions. Keep
  in mind that these potential differences are theoretical concepts whose
  values cannot be measured experimentally.

  <subsubsection|Metal\Umetal contacts><label|14-metal-metal contacts>

  An electric potential difference at an interface between two metals is
  called a <subindex|Contact|potential><em|contact potential>. When two
  different metals are placed in contact, the local densities of the free
  (mobile) electrons change so as to form an electrical double layer with an
  excess positive charge on one side of the interface and an excess negative
  charge of equal magnitude on the other side. The electrical double layer
  creates the contact potential.

  To understand why a stable equilibrium state of two metals in contact
  includes a contact potential, we can consider the chemical potential of the
  free electrons. The concept of chemical potential (i.e., partial molar
  Gibbs energy) applies to the free electrons in a metal just as it does to
  other species. The dependence of the <index-complex|<tuple|chemical
  potential|electrons>|||<tuple|Chemical potential|of
  electrons>><subindex|Electron|chemical potential>chemical potential
  <math|<mue><aph>> of free electrons in metal phase <math|<pha>> on the
  electric potential <math|\<phi\><aph>> of the phase is given by the
  relation of Eq. <vpageref|mu_i(phi)=mu_i(0)+z+Fphi>, with the charge number
  <math|z<rsub|i>> set equal to <math|-1>:

  <\equation>
    <label|mu_e(phi)=mu_e(0)-Fphi><mue><aph><around|(|\<phi\>|)>=<mue><aph><around|(|0|)>-F*\<phi\><aph>
  </equation>

  Here <math|<mue><aph><around|(|0|)>> is the electron chemical potential in
  a phase with the same intensive properties as phase <math|<pha>> but at
  zero electric potential. <math|<mue><aph><around|(|0|)>> depends only on
  the temperature and the composition of phase <math|<pha>>. (The dependence
  on pressure is so small for a solid that we will ignore it.)

  Consider two or more electron conductors that are so arranged that
  electrons can freely transfer among them. There is the usual condition for
  transfer equilibrium in these phases: the chemical potential (in this case
  <math|<mue>>) is the same in each phase. Thus, electron transfer
  equilibrium between phases <math|<pha>> and <math|<phb>> requires
  <math|<mue><aph>> and <math|<mue><bph>> to be equal. We equate
  <math|<mue><aph>> and <math|<mue><bph>>, substitute from Eq.
  <reference|mu_e(phi)=mu_e(0)-Fphi> to obtain
  <math|<mue><aph><around|(|0|)>-F*\<phi\><aph>=<mue><bph><around|(|0|)>-F*\<phi\><bph>>,
  and rearrange to

  <\equation-cov2|<label|Del phi, metal jnc>\<phi\><bph>-\<phi\><aph>=<frac|<mue><bph><around|(|0|)>-<mue><aph><around|(|0|)>|F>>
    (phases in electron

    transfer equilibrium)
  </equation-cov2>

  The quantities on the right side of Eq. <reference|Del phi, metal jnc> are
  functions only of the temperature and the compositions of phases
  <math|<pha>> and <math|<phb>>. If the phases have the same temperature and
  composition and are in electron transfer equilibrium, <math|\<phi\><aph>>
  and <math|\<phi\><bph>> are equal.

  For an equilibrium state of metals <math|<pha>> and <math|<phb>> in
  contact, Eq. <reference|Del phi, metal jnc> shows that the
  <subindex|Contact|potential>contact potential
  <math|\<phi\><bph>-\<phi\><aph>> depends only on the temperature and the
  compositions of the two metals.<footnote|The temperature dependence of a
  contact potential between two different metals is the basis of the
  operation of a <index|Thermocouple>thermocouple or
  <index|Thermopile>thermopile to measure temperature (Sec.
  <reference|2-practical thermometers>).>

  <\quote-env>
    \ Equation <reference|Del phi, metal jnc> explains why a galvanic cell
    must have at least one electrical conductor that is not an electron
    conductor. If electrons were free to pass from one terminal through the
    system to the other terminal of the same temperature and composition,
    then in a zero-current equilibrium state <math|<mue>> would be the same
    in both terminals. In that case there would be no potential difference
    between the terminals, and the system would not be a galvanic cell.
  </quote-env>

  <subsubsection|Metal\Uelectrolyte interfaces>

  An electrode reaction of a galvanic cell takes place at the interface
  between a metal electron conductor and an electrolyte solution. In an
  equilibrium state of the cell, the electrode reaction is at equilibrium.
  The condition for this equilibrium is <math|<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>=0>,
  where the sum is over the reactants and products of the electrode reaction,
  including the electrons. The chemical potentials of the ions and electrons
  in the electrode reaction are functions of the electric potentials of their
  phases. Consequently, in order for the sum to be zero, the metal and
  solution must in general have different electric potentials.

  For example, consider the zinc\Ucopper cell of Fig. <reference|fig:14-Zn-Cu
  cell>. The electrode reaction of the copper electrode at the right is

  <\equation*>
    <text|Cu><rsup|2+>*<around|(|<text|aq>|)>+2*<text|e><rsup|->*<around|(|<text|Cu>|)><arrow><text|Cu>
  </equation*>

  where the metal phase of the electrons is indicated in parentheses. In
  order for this electrode reaction to be at equilibrium, the interfacial
  potential difference between the copper conductor and the solution
  containing Cu<rsup|<math|2+>> ions must be such that the following
  condition is satisfied:

  <\equation>
    <label|Cu electrode eqm>\<mu\><around|(|<text|Cu>|)>-\<mu\><around|(|<text|Cu><rsup|2+>|)>-2*<mue><around|(|<text|Cu>|)>=0
  </equation>

  The interfacial potential difference can arise from a combination of charge
  separation across the interface, orientation of polar molecules on the
  solution side of the interface, and specific adsorption of ions. The
  thickness of the zones in which properties differ from those in the bulk
  phases is probably no greater than <math|10<rsup|-11> <text|m>> on the
  metal side and <math|10<rsup|-7> <text|m>> on the solution side.

  <subsubsection|Liquid junctions>

  Some galvanic cells contain two electrolyte solutions with different
  compositions. These solutions must be separated by a porous barrier or some
  other kind of junction in order to prevent rapid mixing. At this
  <index|Liquid junction>liquid junction in the zero-current cell, there is
  in general a <subindex|Liquid junction|potential><em|liquid junction
  potential> caused by diffusion of ions between the two bulk electrolyte
  phases.

  To understand this phenomenon, imagine the situation that would exist at
  the junction if both solution phases had the same electric potential. An
  ion species with different chemical potentials in the two solutions would
  spontaneously diffuse across the junction in the direction of lower
  chemical potential. Different ions would diffuse at different rates,
  resulting in a net charge transfer across the junction and an electric
  potential difference. It is this electric potential difference in the
  equilibrium state of the cell that prevents further net charge transfer
  under zero-current conditions.

  The liquid junction may consist of a bridging solution in a <index|Salt
  bridge><em|salt bridge>. A commonly used kind of salt bridge is a glass
  tube filled with gel made from agar and concentrated aqueous KCl or
  KNO<rsub|<math|3>>; this type of liquid junction is believed to reduce the
  liquid junction potential to several millivolts or less.

  <section|Molar Reaction Quantities of the Cell Reaction><label|14-molar rxn
  quantities><label|c14 sec-mrqcr>

  <index-complex|<tuple|gibbs energy|molar reaction|cell reaction>||c14
  sec-mrqcr idx1|<tuple|Gibbs energy|molar reaction|of a cell reaction>>This
  book will denote the molar reaction Gibbs energy of a cell reaction by
  <math|\<Delta\><rsub|<text|r>>*G>. This notation distinguishes it from the
  molar reaction Gibbs energy <math|\<Delta\><rsub|<text|r>>*G> of the direct
  reaction, which may have a different value because in the cell the chemical
  potential of an ionic species is affected by the electric potential of its
  phase. <math|\<Delta\><rsub|<text|r>>*G> is defined by

  <\equation>
    <label|del(r)G(cell) defn>\<Delta\><rsub|<text|r>>*G<defn><big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>
  </equation>

  where the sum is over the reactants and products of the cell reaction.
  <math|\<Delta\><rsub|<text|r>>*G> is also equal to the partial derivative
  <math|<pd|G|\<xi\>|T,p>>, where <math|\<xi\>> is the advancement of the
  cell reaction.

  <subsection|Relation between <math|\<Delta\><rsub|<text|r>>*G<rsub|<text|cell>>>
  and <math|E<rsub|<text|cell>,<text|eq>>>><label|14-el rxn eqm
  derivation><label|c14 sec mrqcr-drgc-e>

  When a galvanic cell is in a zero-current equilibrium state, both electrode
  reactions are at reaction equilibrium. In the electrode reaction at the
  left electrode, electrons are a product with stoichiometric number equal to
  <math|z>. At the right electrode, electrons are a reactant with
  stoichiometric number equal to <math|-z>. We can write the conditions for
  electrode reaction equilibria as follows:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<text|At the left
    electrode:>>|<cell|<text|<space|2em>>>|<cell|<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>+z<mue><around|(|<text|LE>|)>=0<eq-number><label|sum=0
    (left el)>>>|<row|<cell|<text|At the right
    electrode:>>|<cell|>|<cell|<big|sum><rsub|j>\<nu\><rsub|j>*\<mu\><rsub|j>-z<mue><around|(|<text|RE>|)>=0<eq-number><label|sum=0
    (right el)>>>>>
  </eqnarray*>

  In these equations, the sum over <math|i> is for the chemical species
  (excluding electrons) of the electrode reaction at the left electrode, and
  the sum over <math|j> is for the chemical species of the electrode reaction
  at the right electrode. <math|<mue><around|(|<text|LE>|)>> is the chemical
  potential of electrons in the electron conductor of the left electrode, and
  <math|<mue><around|(|<text|RE>|)>> is the chemical potential of electrons
  in the electron conductor of the right electrode.

  Adding Eqs. <reference|sum=0 (left el)> and <reference|sum=0 (right el)>,
  we obtain

  <\equation>
    <label|sum(nu_i)(nu_i)+..=0><big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>+<big|sum><rsub|j>\<nu\><rsub|j>*\<mu\><rsub|j>+z*<around|[|<mue><around|(|<text|LE>|)>-<mue><around|(|<text|RE>|)>|]>=0
  </equation>

  The first two terms on the left side of Eq.
  <reference|sum(nu_i)(nu_i)+..=0> are sums over all the reactants and
  products of the cell reaction. From Eq. <reference|del(r)G(cell) defn>, we
  recognize the sum of these terms as the molar reaction Gibbs energy of the
  cell reaction:

  <\equation>
    <label|....=Del(r)G(cell)><big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>+<big|sum><rsub|j>\<nu\><rsub|j>*\<mu\><rsub|j>=\<Delta\><rsub|<text|r>>*G
  </equation>

  Substituting from Eq. <reference|....=Del(r)G(cell)> into Eq.
  <reference|sum(nu_i)(nu_i)+..=0> and solving for
  <math|\<Delta\><rsub|<text|r>>*G>, we obtain

  <\equation>
    <label|Del(r)G(cell)=-z[.-.]>\<Delta\><rsub|<text|r>>*G=-z*<around|[|<mue><around|(|<text|LE>|)>-<mue><around|(|<text|RE>|)>|]>
  </equation>

  In a zero-current equilibrium state, there is electron transfer equilibrium
  between the left electron conductor and the left terminal, and between the
  right electron conductor and the right terminal:
  <math|<mue><around|(|<text|LE>|)>=<mue><around|(|<text|LT>|)>> and
  <math|<mue><around|(|<text|RE>|)>=<mue><around|(|<text|RT>|)>>, where
  <math|<mue><around|(|<text|LT>|)>> and <math|<mue><around|(|<text|RT>|)>>
  are the chemical potentials of electrons in the left terminal and right
  terminal, respectively. Thus we can rewrite Eq.
  <reference|Del(r)G(cell)=-z[.-.]> as

  <\equation>
    <label|Del(r)G(cell)=-z[LT-RT]>\<Delta\><rsub|<text|r>>*G=-z*<around|[|<mue><around|(|<text|LT>|)>-<mue><around|(|<text|RT>|)>|]>
  </equation>

  Making substitutions from Eq. <reference|mu_e(phi)=mu_e(0)-Fphi> for
  <math|<mue><around|(|<text|LT>|)>> and <math|<mue><around|(|<text|RT>|)>>,
  and recognizing that <math|<mue><around|(|0|)>> is the same in both
  terminals because they have the same composition, we obtain

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\><rsub|<text|r>>*G>|<cell|=>|<cell|-z*F*<around|(|\<phi\><rsub|<text|R>>-\<phi\><rsub|<text|L>>|)>>>|<row|<cell|>|<cell|=>|<cell|-z*F<Eeq><eq-number><label|del(r)G(cell)=-zFE>>>>>
  </eqnarray*>

  We can see from Eq. <reference|del(r)G(cell) defn> that the value of
  <math|\<Delta\><rsub|<text|r>>*G> has nothing to do with the composition of
  the terminals. The relations of Eq. <reference|del(r)G(cell)=-zFE> were
  derived for a cell with both terminals made of the same metal. We can make
  the following deductions for such a cell:

  <\enumerate>
    <item>Neither the potential difference
    <math|\<phi\><rsub|<text|R>>-\<phi\><rsub|<text|L>>> nor the equilibrium
    cell potential <math|<Eeq>> depend on the kind of metal used for the
    terminals.

    <item>If we interpose a metal conductor of any composition between the
    electron conductor and the terminal of one of the electrodes,
    <math|<mue>> will have the same value in all three conductors and there
    will be no effect on the value of <math|<Eeq>>.
  </enumerate>

  <\quote-env>
    \ Equation <reference|del(r)G(cell)=-zFE> can be derived by a different
    route. According to Eq. <vpageref|dG\<less\>=-SdT+Vdp+dw'>, reversible
    electrical work at constant <math|T> and <math|p> is equal to the Gibbs
    energy change: <math|<dw><rsub|<text|el>,<text|rev>>=<dif>G>. Making the
    substitution <math|<dw><rsub|<text|el>,<text|rev>>=<Eeq><dQ><sys>> (from
    Eq. <reference|dw(el)=E(cell)dQ(cell)>), with <math|<dQ><sys>> set equal
    to <math|-z*F<dif>\<xi\>> (Eq. <reference|dQ=-zFdxi>), followed by
    division by <math|<dif>\<xi\>>, gives<next-line><math|-z*F<Eeq>=<pd|G|\<xi\>|T,p>>,
    or <math|\<Delta\><rsub|<text|r>>*G=-z*F<Eeq>>.

    Strictly speaking, this derivation applies only to a cell without a
    liquid junction. In a cell with a liquid junction, the electric current
    is carried across the junction by different ions depending on the
    direction of the current, and the cell is therefore not reversible.
  </quote-env>

  <subsection|Relation between <math|\<Delta\><rsub|<text|r>>*G<rsub|<text|cell>>>
  and <math|\<Delta\><rsub|<text|r>>*G>><label|c14 sec mrqcr-drgc-drg>

  Suppose we have a galvanic cell in a particular zero-current equilibrium
  state. Each phase of the cell has the same temperature and pressure and a
  well-defined chemical composition. The activity of each reactant and
  product of the cell reaction therefore has a definite value in this state.

  Now imagine a reaction vessel that has the same temperature and pressure as
  the galvanic cell, and contains the same reactants and products at the same
  activities as in the cell. This reaction vessel, unlike the cell, is not
  part of an electrical circuit. In it, the reactants and products are in
  direct contact with one another, so there is no constraint preventing a
  spontaneous direct reaction. For example, the reaction vessel corresponding
  to the zinc\Ucopper cell of Fig. <reference|fig:14-Zn-Cu cell> would have
  zinc and copper strips in contact with a solution of both
  ZnSO<rsub|<math|4>> and CuSO<rsub|<math|4>>. Another example is the slow
  direct reaction in a cell without liquid junction described on page
  <pageref|direct rxn>.

  Let the reaction equation of the direct reaction be written with the same
  stoichiometric numbers <math|\<nu\><rsub|i>> as in the reaction equation
  for the cell reaction. The direct reaction in the reaction vessel is
  described by this equation or its reverse, depending on which direction is
  spontaneous for the given activities.

  The question now arises whether the molar reaction Gibbs energy
  <math|\<Delta\><rsub|<text|r>>*G> of the cell reaction is equal to the
  molar reaction Gibbs energy <math|\<Delta\><rsub|<text|r>>*G> of the direct
  reaction. Both <math|\<Delta\><rsub|<text|r>>*G> and
  <math|\<Delta\><rsub|<text|r>>*G> are defined by the sum
  <math|<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>>. Both reactions have
  the same values of <math|\<nu\><rsub|i>>, but the values of
  <math|\<mu\><rsub|i>> for charged species are in general different in the
  two systems because the electric potentials are different.

  Consider first a cell without a liquid junction. This kind of cell has a
  single electrolyte solution, and all of the reactant and product ions of
  the cell reaction are in this solution phase. The same solution phase is
  present in the reaction vessel during the direct reaction. When all ions
  are in the same phase, the value of <math|<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>>
  is independent of the electric potentials of any of the phases (see the
  comment following Eq. <vpageref|Del(r)G=Del(r)o+RTsum>), so that the molar
  reaction Gibbs energies are the same for the cell reaction and the direct
  reaction:

  <equation-cov2|<label|del(r)G(cell) no liq
  jnc>\<Delta\><rsub|<text|r>>*G=\<Delta\><rsub|<text|r>>*G|(no liquid
  junction)>

  Next, consider a cell with two electrolyte solutions separated by a liquid
  junction. For the molar reaction Gibbs energy of the cell reaction, we
  write

  <\equation>
    <label|del(r)G(cell)=>\<Delta\><rsub|<text|r>>*G=<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i><around|(|\<phi\><rsub|i>|)>+<big|sum><rsub|j>\<nu\><rsub|j>*\<mu\><rsub|j><around|(|\<phi\><rsub|j>|)>
  </equation>

  The sums here include all of the reactants and products appearing in the
  cell reaction, those with index <math|i> being at the left electrode and
  those with index <math|j> at the right electrode. Let the solution at the
  left electrode be phase <math|<pha>> and the solution at the right
  electrode be phase <math|<phb>>. Then making the substitution
  <math|\<mu\><rsub|i><around|(|\<phi\>|)>=\<mu\><rsub|i><around|(|0|)>+z<rsub|i>*F*\<phi\>>
  (Eq. <reference|mu_i(phi)=mu_i(0)+z+Fphi>) gives us

  <\equation>
    <label|del(r)G(cell)=....>\<Delta\><rsub|<text|r>>*G=<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i><around|(|0|)>+<big|sum><rsub|j>\<nu\><rsub|j>*\<mu\><rsub|j><around|(|0|)>+<big|sum><rsub|i>\<nu\><rsub|i>*z<rsub|i>*F*\<phi\><aph>+<big|sum><rsub|j>\<nu\><rsub|j>*z<rsub|j>*F*\<phi\><bph>
  </equation>

  The sum of the first two terms on the right side of Eq.
  <reference|del(r)G(cell)=....> is the molar reaction Gibbs energy of a
  reaction in which the reactants and products are in phases of zero electric
  potential. According to the comment following Eq.
  <reference|Del(r)G=Del(r)o+RTsum>, the molar reaction Gibbs energy would be
  the same if the ions were in a single phase of any electric potential.
  Consequently the sum <math|<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i><around|(|0|)>+<big|sum><rsub|j>\<nu\><rsub|j>*\<mu\><rsub|j><around|(|0|)>>
  is equal to <math|\<Delta\><rsub|<text|r>>*G> for the direct reaction.

  The conservation of charge during advancement of the electrode reactions at
  the left electrode and the right electrode is expressed by
  <math|<big|sum><rsub|i>\<nu\><rsub|i>*z<rsub|i>-z=0> and
  <math|<big|sum><rsub|j>\<nu\><rsub|j>*z<rsub|j>+z=0>, respectively.
  Equation <reference|del(r)G(cell)=....> becomes

  <equation-cov2|<label|del(r)G(cell) with liq
  jnc>\<Delta\><rsub|<text|r>>*G=\<Delta\><rsub|<text|r>>*G-z*F*<Ej>|(cell
  with liquid junction)>

  where <math|<Ej>=\<phi\><bph>-\<phi\><aph>> is the liquid junction
  potential.

  Finally, in Eqs. <reference|del(r)G(cell) no liq jnc> and
  <reference|del(r)G(cell) with liq jnc> we replace
  <math|\<Delta\><rsub|<text|r>>*G> by <math|-z*F<Eeq>> (Eq.
  <reference|del(r)G(cell)=-zFE>) and solve for <math|<Eeq>>:

  <equation-cov2|<label|E=-del(r)G/zf><Eeq>=-<frac|\<Delta\><rsub|<text|r>>*G|z*F>|(cell
  without liquid junction)>

  <equation-cov2|<label|E=-del(r)G/zf+Ej><Eeq>=-<frac|\<Delta\><rsub|<text|r>>*G|z*F>+<Ej>|(cell
  with liquid junction)>

  <math|<Eeq>> can be measured with great precision. If a reaction can be
  carried out in a galvanic cell without liquid junction, Eq.
  <reference|E=-del(r)G/zf> provides a way to evaluate
  <math|\<Delta\><rsub|<text|r>>*G> under given conditions. If the reaction
  can only be carried out in a cell with a liquid junction, Eq.
  <reference|E=-del(r)G/zf+Ej> can be used for this purpose provided that the
  liquid junction potential <math|<Ej>> can be assumed to be negligible or
  can be estimated from theory.

  Note that the cell has reaction equilibrium only if
  <math|\<Delta\><rsub|<text|r>>*G> is zero. The cell has thermal,
  mechanical, and transfer equilibrium when the electric current is zero and
  the cell potential is the zero-current cell potential <math|<Eeq>>.
  Equations <reference|E=-del(r)G/zf> and <reference|E=-del(r)G/zf+Ej> show
  that in order for the cell to also have reaction equilibrium, <math|<Eeq>>
  must equal the liquid junction potential if there is a liquid junction, or
  be zero otherwise. These are the conditions of an exhausted, \Pdead\Q cell
  that can no longer do electrical work.<index-complex|<tuple|gibbs
  energy|molar reaction|cell reaction>||c14 sec-mrqcr idx1|<tuple|Gibbs
  energy|molar reaction|of a cell reaction>>

  <subsection|Standard molar reaction quantities><label|14-st molar rxn
  quantities><label|c14 sec-mrqcr-std-quantities>

  Consider a hypothetical galvanic cell in which each reactant and product of
  the cell reaction is in its standard state at unit activity, and in which a
  liquid junction if present has a negligible liquid junction potential. The
  equilibrium cell potential of this cell is called the
  <subindex|Standard|cell potential><subindex|Potential|standard
  cell><newterm|standard cell potential> of the cell reaction,
  <math|<Eeq><st>>. An experimental procedure for evaluating <math|<Eeq><st>>
  will be described in Sec. <reference|14-eval Eo>.

  In this hypothetical cell, <math|\<Delta\><rsub|<text|r>>*G> is equal to
  the standard molar reaction Gibbs energy
  <math|\<Delta\><rsub|<text|r>>*G<st>>. From Eq. <reference|E=-del(r)G/zf>,
  or Eq. <reference|E=-del(r)G/zf+Ej> with <math|<Ej>> assumed equal to zero,
  we have

  <\equation>
    <label|del(r)Go=-zFEo>\<Delta\><rsub|<text|r>>*G<st>=-z*F<Eeq><st>
  </equation>

  <math|\<Delta\><rsub|<text|r>>*G<st>> is the molar reaction Gibbs energy
  when each reactant and product is at unit activity and, if it is an ion, is
  in a phase of zero electric potential. Since
  <math|\<Delta\><rsub|<text|r>>*G<st>> is equal to <math|-R*T*ln K> (Eq.
  <reference|del(r)Gmo=-RT*ln(K)>), we can write

  <\equation>
    <label|ln(K)=nuFEo/RT>ln K=<frac|z*F|R*T><Eeq><st>
  </equation>

  Equation <reference|ln(K)=nuFEo/RT> allows us to evaluate the
  <index-complex|<tuple|equilibrium constant|thermodynamic|cell
  reaction>|||<tuple|Equilibrium constant|thermodynamic|of a cell
  reaction>>thermodynamic equilibrium constant <math|K> of the cell reaction
  by a noncalorimetric method. Consider for example the cell

  <\equation*>
    <text|Ag><jn><text|Ag><rsup|+> <around|(|<text|aq>|)><lljn><text|Cl><rsup|->
    <around|(|<text|aq>|)><jn><text|AgCl> <around|(|<text|s>|)><jn><text|Ag>
  </equation*>

  in which the pair of dashed vertical bars indicates a liquid junction of
  negligible liquid junction potential. The electrode reactions are

  <\eqnarray*>
    <tformat|<table|<row|<cell|<text|Ag> <around*|(|<text|s>|)>>|<cell|<arrow>>|<cell|<text|Ag><rsup|+>
    <around*|(|<text|aq>|)>+<text|e><rsup|->>>|<row|<cell|<text|AgCl>
    <around*|(|<text|s>|)>+<text|e><rsup|->>|<cell|<arrow>>|<cell|<text|Ag>
    <around*|(|<text|s>|)>+Cl<rsup|-> <around*|(|<text|aq>|)>>>>>
  </eqnarray*>

  and the cell reaction is

  <\equation*>
    <text|AgCl> <around|(|<text|s>|)><arrow><text|Ag><rsup|+>
    <around|(|<text|aq>|)>+<text|Cl><rsup|-> <around|(|<text|aq>|)>
  </equation*>

  The equilibrium constant of this reaction is the <index|Solubility
  product>solubility product <math|K<rsub|<text|s>>> of silver chloride (Sec.
  <reference|12-electrolyte solubility>). At <math|298.15<K>>, the standard
  cell potential is found to be <math|<Eeq><st>=-0.5770 <V>>. We can use this
  value in Eq. <reference|ln(K)=nuFEo/RT> to evaluate <math|K<rsub|<text|s>>>
  at <math|298.15<K>> (see Prob. <reference|prb:14-Ks(AgCl)>).

  Equation <reference|ln(K)=nuFEo/RT> also allows us to evaluate the
  <subsubindex|Enthalpy|reaction|standard molar of a cell reaction>standard
  molar reaction enthalpy by substitution in Eq.
  <vpageref|del(r)Hmo=(RT^2)dln(K)/dT>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Delta\><rsub|<text|r>>*<text|H><st>>|<cell|=>|<cell|R*T<rsup|2>*<frac|<dvar|<text|ln>>
    K|<dvar|T>>>>|<row|<cell|>|<cell|=>|<cell|z*F*<around*|(|T*<frac|<dvar|<Eeq><st>>|<dvar|T>>-<Eeq><st>|)><htab|5mm><tabular*|<tformat|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<text|<tabular|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|(no
    solute standard states>>|<row|<cell|based on
    concentration)>>>>>>>>>>><label|del(r)Hmo=nuF..>>>>>
  </eqnarray*>

  Finally, by combining Eqs. <reference|del(r)Go=-zFEo> and
  <reference|del(r)Hmo=nuF..> with <math|\<Delta\><rsub|<text|r>>*G<st>=\<Delta\><rsub|<text|r>>*H<st>-T\<Delta\><rsub|<text|r>>*S<st>>,
  we obtain an expression for the <subsubindex|Entropy|reaction|standard
  molar of a cell reaction>standard molar reaction entropy:

  <\equation-cov2|<label|del(r)Smo=(nuF)dEo/dT>\<Delta\><rsub|<text|r>>*S<st>=z*F<frac|<dif><Eeq><st>|<dif>T>>
    (no solute standard states

    based on concentration)
  </equation-cov2>

  Because <math|G>, <math|H>, and <math|S> are state functions, the
  thermodynamic equilibrium constant and the molar reaction quantities
  evaluated from <math|<Eeq><st>> and <math|<dif><Eeq><st>/<dif>T> are the
  same quantities as those for the reaction when it takes place in a reaction
  vessel instead of in a galvanic cell. However, the heats at constant
  <math|T> and <math|p> are not the same (page <pageref|dH not equal to dq>).
  During a reversible cell reaction, <math|<dif>S> must equal <math|<dq>/T>,
  and <math|<dq>/<dif>\<xi\>> is therefore equal to
  <math|T*\<Delta\><rsub|<text|r>>*S<st>> during a cell reaction taking place
  reversibly under standard state conditions at constant <math|T> and
  <math|p>.

  <section|The Nernst Equation><label|c14 sec nernst>

  The <subindex|Standard|cell potential><subindex|Potential|standard
  cell>standard cell potential <math|<Eeq><st>> of a cell reaction is the
  equilibrium cell potential of the hypothetical galvanic cell in which each
  reactant and product of the cell reaction is in its standard state and
  there is no liquid junction potential. The value of <math|<Eeq><st>> for a
  given cell reaction with given choices of standard states is a function
  only of temperature. The measured equilibrium cell potential <math|<Eeq>>
  of an actual cell, however, depends on the activities of the reactants and
  products as well as on temperature and the liquid junction potential, if
  present.

  To derive a relation between <math|<Eeq>> and activities for a cell without
  liquid junction, or with a liquid junction of negligible liquid junction
  potential, we substitute expressions for <math|\<Delta\><rsub|<text|r>>*G>
  and for <math|\<Delta\><rsub|<text|r>>*G<st>> from Eqs.
  <reference|E=-del(r)G/zf> and Eq. <reference|del(r)Go=-zFEo> into
  <math|\<Delta\><rsub|<text|r>>*G=\<Delta\><rsub|<text|r>>*G<st>+R*T*ln
  Q<rsub|<text|rxn>>> (Eq. <vpageref|del(r)Gm=del(r)Gmo+RT*lnQ(r)>) and solve
  for <math|<Eeq>>:

  <equation-cov2|<label|Nernst eq><Eeq>=<Eeq><st>-<frac|R*T|z*F>*ln
  Q<rsub|<text|rxn>>|(no liquid junction, or <math|<Ej>=0>)>

  Equation <reference|Nernst eq> is the <subindex|Nernst|equation><newterm|Nernst
  equation> for the cell reaction. Here <math|Q<rsub|<text|rxn>>> is the
  <subindex|Reaction|quotient>reaction quotient for the cell reaction defined
  by Eq. <reference|Q=prod(a_i)^(nu_i)>: <math|Q<rsub|<text|rxn>>=<big|prod><rsub|i>a<rsub|i><rsup|\<nu\><rsub|i>>>.

  The rest of this section will assume that the cell reaction takes place in
  a cell without liquid junction, or in one in which <math|<Ej>> is
  negligible.

  If each reactant and product of the cell reaction is in its standard state,
  then each activity is unity and <math|ln Q<rsub|<text|rxn>>> is zero. We
  can see from the Nernst equation that the equilibrium cell potential
  <math|<Eeq>> in this case has its standard value <math|<Eeq><st>>, as
  expected. A decrease in product activities or an increase in reactant
  activities decreases the value of <math|ln Q<rsub|<text|rxn>>> and
  increases <math|<Eeq>>, as we would expect since <math|<Eeq>> should be
  greater when the forward cell reaction has a greater tendency for
  spontaneity.

  If the cell reaction comes to reaction equilibrium, as it will if we
  short-circuit the cell terminals with an external wire, the value of
  <math|Q<rsub|<text|rxn>>> becomes equal to the thermodynamic equilibrium
  constant <math|K>, and the Nernst equation becomes
  <math|<Eeq>=<Eeq><st>-<around|(|R*T/z*F|)>*ln K>. The term
  <math|<around|(|R*T/z*F|)>*ln K> is equal to <math|<Eeq><st>> (Eq.
  <reference|ln(K)=nuFEo/RT>), so <math|<Eeq>> becomes zero\Vthe cell is
  \Pdead\Q and is incapable of performing electrical work on the
  surroundings.

  At <math|T=298.15<K>> (<math|25.00 <degC>>), the value of <math|R*T/F> is
  <math|0.02569 <V>>, and we can write the Nernst equation in the compact
  form

  <equation-cov2|<label|Nernst eq, 298K><Eeq>=<Eeq><st>-<frac|0.02569
  <V>|z>*ln Q<rsub|<text|rxn>>|(<math|T=298.15 <text|K>>)>

  As an illustration of an application of the Nernst equation, consider the
  reaction equation

  <\equation*>
    <text|H<rsub|2>><around|(|<text|g>|)>+2*<text|AgCl>
    <around|(|<text|s>|)><ra>2*<text|H><rsup|+>*<around|(|<text|aq>|)>+2*<text|Cl><rsup|->*<around|(|<text|aq>|)>+2*<text|Ag>
    <around|(|<text|s>|)>
  </equation*>

  This reaction takes place in a cell without liquid junction (Fig.
  <reference|fig:14-galvanic cell>), and the electrolyte solution can be
  aqueous HCl. The expression for the reaction quotient is

  <\equation>
    Q<rsub|<text|rxn>>=<frac|a<rsub|+><rsup|2>*a<rsub|-><rsup|2>*a<rsub|<text|Ag>><rsup|2>|a<rsub|<text|H<rsub|2>>>*a<rsub|<text|AgCl>><rsup|2>>
  </equation>

  We may usually with negligible error approximate the pressure factors of
  the solids and solutes by unity. The activities of the solids are then 1,
  the solute activities are <math|a<rsub|+>=<g><rsub|+>m<rsub|+>/m<st>> and
  <math|a<rsub|->=<g><rsub|->m<rsub|->/m<st>>, and the hydrogen activity is
  <math|a<rsub|<text|H<rsub|2>>>=<fug><rsub|<text|H<rsub|2>>>/p<st>>. The ion
  molalities <math|m<rsub|+>> and <math|m<rsub|->> are equal to the HCl
  molality <math|m<B>>. The expression for <math|Q<rsub|<text|rxn>>> becomes

  <\equation>
    Q<rsub|<text|rxn>>=<frac|<g><rsub|+><rsup|2><g><rsub|-><rsup|2><around*|(|m<B>/m<st>|)><rsup|4>|<fug><rsub|<text|H<rsub|2>>>/p<st>>=<frac|<g><rsub|\<pm\>><rsup|4><around*|(|m<B>/m<st>|)><rsup|4>|<fug><rsub|<text|H<rsub|2>>>/p<st>>
  </equation>

  and the Nernst equation for this cell is

  <\eqnarray*>
    <tformat|<table|<row|<cell|<Eeq>>|<cell|=>|<cell|<Eeq><st>-<frac|R*T|2*F>*ln
    <frac|<g><rsub|\<pm\>><rsup|4><around|(|m<B>/m<st>|)><rsup|4>|<fug><rsub|<text|H<rsub|2>>>/p<st>>>>|<row|<cell|>|<cell|=>|<cell|<Eeq><st>-<frac|2*R*T|F>*ln
    <g><rsub|\<pm\>>-<frac|2*R*T|F>*ln <frac|m<B>|m<st>>+<frac|R*T|2*F>*ln
    <frac|<fug><rsub|<text|H<rsub|2>>>|p<st>><eq-number><label|AgCl-Ag Nernst
    eq>>>>>
  </eqnarray*>

  By measuring <math|<Eeq>> for a cell with known values of <math|m<B>> and
  <math|<fug><rsub|<text|H<rsub|2>>>>, and with a derived value of
  <math|<Eeq><st>>, we can use this equation to find the
  <index-complex|<tuple|activity coefficient|mean
  ionic|nernst>|||<tuple|Activity coefficient|mean ionic|from the Nernst
  equation>>mean ionic activity coefficient <math|<g><rsub|\<pm\>>> of the
  HCl solute. This is how the experimental curve for aqueous HCl in Fig.
  <vpageref|fig:10-aq HCl gamma+/-> was obtained.

  <\quote-env>
    \ We can always multiply each of the stoichiometric coefficients of a
    reaction equation by the same positive constant without changing the
    meaning of the reaction. How does this affect the Nernst equation for the
    reaction equation above? Suppose we decide to multiply the stoichiometric
    coefficients by one-half:

    <\equation*>
      <frac|1|2>*<text|H<rsub|2>> <around|(|<text|g>|)>+<text|AgCl><around|(|<text|s>|)><arrow><text|H><rsup|+>*<around|(|<text|aq>|)>+<text|Cl><rsup|->*<around|(|<text|aq>|)>+<text|Ag><around|(|<text|s>|)>
    </equation*>

    With this changed reaction equation, the value of <math|z> is changed
    from 2 to 1 and the Nernst equation becomes

    <\equation>
      <Eeq>=<Eeq><st>-<frac|R*T|F>*ln <frac|<g><rsub|\<pm\>><rsup|2><around|(|m<B>/m<st>|)><rsup|2>|<around|(|<fug><rsub|<text|H<rsub|2>>>/p<st>|)><rsup|1/2>>
    </equation>

    which yields the same value of <math|<Eeq>> for given cell conditions as
    Eq. <reference|AgCl-Ag Nernst eq>. This value must of course be
    unchanged, because physically the cell is the same no matter how we write
    its cell reaction, and measurable physical quantities such as
    <math|<Eeq>> are unaffected. However, molar reaction quantities such as
    <math|\<Delta\><rsub|<text|r>>*G> and
    <math|\<Delta\><rsub|<text|r>>*G<st>> <em|do> depend on how we write the
    cell reaction, because they are changes per extent of reaction.
  </quote-env>

  <section|Evaluation of the Standard Cell Potential><label|14-eval
  Eo><label|c14 sec escp>

  <index-complex|<tuple|standard|cell potential|evaluation>||c14 sec escp
  idx1|<tuple|Standard|cell potential|evaluation>><index-complex|<tuple|potential|standard
  cell|evaluation>||c14 sec escp idx2|<tuple|Potential|standard
  cell|evaluation of>>As we have seen, the value of the standard cell
  potential <math|<Eeq><st>> of a cell reaction has useful thermodynamic
  applications. The value of <math|<Eeq><st>> for a given cell reaction
  depends only on temperature. To evaluate it, we can extrapolate an
  appropriate function to infinite dilution where ionic activity coefficients
  are unity.

  To see how this procedure works, consider again the cell reaction
  <math|<text|H<rsub|2>><around|(|<text|g>|)>+2*<text|AgCl>
  <around|(|s|)><arrow>2*<text|H><rsup|+>
  <around|(|<text|aq>|)>+2*<text|Cl><rsup|->
  <around|(|<text|aq>|)>+2*<text|Ag> <around|(|<text|s>|)>>. The cell
  potential depends on the molality <math|m<B>> of the HCl solute according
  to Eq. <reference|AgCl-Ag Nernst eq>. We can rearrange the equation to

  <\equation>
    <label|E(cell)^o=E(cell,eq)+><Eeq><st>=<Eeq>+<frac|2*R*T|F>*ln
    <g><rsub|\<pm\>>+<frac|2*R*T|F>*ln <frac|m<B>|m<st>>-<frac|R*T|2*F>*ln
    <frac|<fug><rsub|<text|H<rsub|2>>>|p<st>>
  </equation>

  For given conditions of the cell, we can measure all quantities on the
  right side of Eq. <reference|E(cell)^o=E(cell,eq)+> except the mean ionic
  activity coefficient <math|<g><rsub|\<pm\>>> of the electrolyte. We cannot
  know the exact value of <math|ln <g><rsub|\<pm\>>> for any given molality
  until we have evaluated <math|<Eeq><st>>. We do know that as <math|m<B>>
  approaches zero, <math|<g><rsub|\<pm\>>> approaches unity and <math|ln
  <g><rsub|\<pm\>>> must approach zero. The
  <index-complex|<tuple|debye-huckel|equation|mean
  ionic>|||<tuple|Debye\UH�ckel|equation|for a mean ionic activity
  coefficient>>Debye\UH�ckel formula of Eq. <vpageref|ln(g+-) (DH)> is a
  theoretical expression for <math|ln <g><rsub|\<pm\>>> that more closely
  approximates the actual value the lower is the ionic strength. Accordingly,
  we define the quantity

  <\equation>
    <label|E'=E-...>E<rprime|'>=<Eeq>+<frac|2*R*T|F>*<around*|(|-<frac|A*<sqrt|m<B>>|1+B*a*<sqrt|m<B>>>|)>+<frac|2*R*T|F>*ln
    <frac|m<B>|m<st>>-<frac|R*T|2*F>*ln <frac|<fug><rsub|<text|H<rsub|2>>>|p<st>>
  </equation>

  The expression in parentheses is the Debye\UH�ckel formula for <math|ln
  <g><rsub|\<pm\>>> with <math|I<rsub|m>> replaced by <math|m<B>>. The
  constants <math|A> and <math|B> have known values at any temperature (Sec.
  <reference|10-Debye Huckel>), and <math|a> is an ion-size parameter for
  which we can choose a reasonable value. At a given temperature, we can
  evaluate <math|E<rprime|'>> experimentally as a function of <math|m<B>>.

  The expression on the right side of Eq. <reference|E(cell)^o=E(cell,eq)+>
  differs from that of Eq. <reference|E'=E-...> by contributions to
  <math|<around|(|2*R*T/F|)>*ln <g><rsub|\<pm\>>> not accounted for by the
  Debye\UH�ckel formula. Since these contributions approach zero in the limit
  of infinite dilution, the extrapolation of measured values of
  <math|E<rprime|'>> to <math|m<B|=>=0> yields the value of <math|<Eeq><st>>.

  Figure <vpageref|fig:14-AgCl-Ag st pot> shows this extrapolation using data
  from the literature.<\float|float|thb>
    <\framed>
      <\big-figure|<image|14-SUP/AGCL-AG.eps|219pt|171pt||>>
        <label|fig:14-AgCl-Ag st pot><math|E<rprime|'>> (defined by Eq.
        <reference|E'=E-...>) as a function of HCl molality for the cell of
        Fig. <reference|fig:14-galvanic cell> at
        <math|298.15<K>>.<note-ref|+1PCvW8IQuYAAePB> The dashed line is a
        least-squares fit to a linear relation.

        <note-inline||+1PCvW8IQuYAAePB>Data from Ref. <cite|harned-32> with
        <math|<fug><rsub|<text|H<rsub|2>>>> set equal to
        <math|p<rsub|<text|H<rsub|2>>>> and the parameter <math|a> set equal
        to <math|4.3<timesten|-10> <text|m>>.
      </big-figure>
    </framed>
  </float> The extrapolated value indicated by the filled circle is
  <math|<Eeq><st>=0.2222 <V>>, and the uncertainty is on the order of only
  <math|0.1 <text|m>\<cdot\><text|V>>.<index-complex|<tuple|standard|cell
  potential|evaluation>||c14 sec escp idx1|<tuple|Standard|cell
  potential|evaluation>><index-complex|<tuple|potential|standard
  cell|evaluation>||c14 sec escp idx2|<tuple|Potential|standard
  cell|evaluation of>>

  <section|Standard Electrode Potentials><label|c14 sec sep>

  Section <reference|14-eval Eo> explained how, by measuring the equilibrium
  cell potential of a galvanic cell at different electrolyte molalities, we
  can evaluate the standard cell potential <math|<Eeq><st>> of the cell
  reaction. It is not necessary to carry out this involved experimental
  procedure for each individual cell reaction of interest. Instead, we can
  calculate <math|<Eeq><st>> from standard electrode potentials.

  By convention, standard electrode potentials use a standard hydrogen
  electrode as a reference electrode. A <subindex|Standard|hydrogen
  electrode><subindex|Hydrogen electrode|standard><subsubindex|Electrode|hydrogen|standard><newterm|standard
  hydrogen electrode> is a hydrogen electrode, such as the electrode shown at
  the left in Fig. <reference|fig:14-galvanic cell>, in which the species
  H<rsub|<math|2>>(g) and H<rsup|<math|+>>(aq) are in their standard states.
  Since these are <em|hypothetical> gas and solute standard states, the
  standard hydrogen electrode is a hypothetical electrode\Vnot one we can
  actually construct in the laboratory.

  A <subindex|Standard|electrode potential><subindex|Electrode|potential,
  standard><newterm|standard electrode potential> <math|E<st>> is defined as
  the standard cell potential of a cell with a hydrogen electrode at the left
  and the electrode of interest at the right. For example, the cell in Fig.
  <reference|fig:14-galvanic cell> with cell diagram

  <\equation*>
    <text|Pt><jn><text|H<rsub|2>> <around|(|g|)><jn><text|HCl>
    <around|(|<text|aq>|)><jn><text|AgCl> <around|(|<text|s>|)><jn><text|Ag>
  </equation*>

  has a hydrogen electrode at the left and a silver\Usilver chloride
  electrode at the right. The standard electrode potential of the
  silver\Usilver chloride electrode, therefore, is equal to the standard cell
  potential of this cell.

  Since a cell with hydrogen electrodes at both the left and right has a
  standard cell potential of zero, the standard electrode potential of the
  hydrogen electrode is <em|zero> at all temperatures. The standard electrode
  potential of any other electrode is nonzero and is a function only of
  temperature.

  Consider the following three cells constructed from various combinations of
  three different electrodes: a hydrogen electrode, and two electrodes
  denoted L and R.

  <\itemize>
    <item>Cell 1 has electrode L at the left and electrode R at the right.

    <item>Cell 2 has the hydrogen electrode at the left and electrode L at
    the right; its standard cell potential is the standard electrode
    potential <math|E<rsub|<text|L>><st>> of electrode L.

    <item>Cell 3 has the hydrogen electrode at the left and electrode R at
    the right; its standard cell potential is the standard electrode
    potential <math|E<rsub|<text|R>><st>> of electrode R.
  </itemize>

  We wish to calculate the standard cell potential <math|<Eeq><st>> of cell 1
  from the standard electrode potentials <math|E<rsub|<text|L>><st>> and
  <math|E<rsub|<text|R>><st>>.

  If we write the cell reactions of cells 1 and 2 using the same value of the
  electron number <math|z> for both, we find that their sum is the cell
  reaction for cell 3 with the same value of <math|z>. Call these reactions
  1, 2, and 3, respectively:

  <\equation>
    <label|rxn1+rxn2=rxn3><around|(|<text|reaction
    1>|)>+<around|(|<text|reaction 2>|)>=<around|(|<text|reaction 3>|)>
  </equation>

  The relation of Eq. <reference|rxn1+rxn2=rxn3> shows that an infinitesimal
  advancement <math|<dif>\<xi\>> of reaction 1 combined with an equal
  advancement of reaction 2 causes the same changes in amounts as the
  advancement <math|<dif>\<xi\>> of reaction 3. Because
  <math|\<Delta\><rsub|<text|r>>*G<st>> for each reaction is the rate at
  which <math|G> changes with <math|\<xi\>> at constant <math|T> when the
  reactants and products are in their standard states, the following relation
  applies when the reactions take place at the same temperature:

  <\equation>
    \<Delta\><rsub|<text|r>>*G<st> <around|(|<text|reaction
    1>|)>+\<Delta\><rsub|<text|r>>*G<st> <around|(|<text|reaction
    2>|)>=\<Delta\><rsub|<text|r>>*G<st> <around|(|<text|reaction 3>|)>
  </equation>

  Making the substitution <math|\<Delta\><rsub|<text|r>>*G<st>=-z*F*<Eeq><st>>
  (Eq. <reference|del(r)Go=-zFEo>), with the same value of <math|z> for each
  reaction, gives us <math|<Eeq><st>+E<rsub|<text|L>><st>=E<rsub|<text|R>><st>>,
  or

  <\equation>
    <label|Eo=Eo(R)-Eo(L)><Eeq><st>=E<rsub|<text|R>><st>-E<rsub|<text|L>><st>
  </equation>

  where <math|<Eeq><st>>, <math|E<rsub|<text|R>><st>>, and
  <math|E<rsub|<text|L>><st>> all refer to cell 1.

  Equation <reference|Eo=Eo(R)-Eo(L)> is a general relation applicable to any
  galvanic cell. It should be apparent that we can use the relation to
  calculate the standard electrode potential of an electrode from the
  standard electrode potential of a different electrode and the standard cell
  potential of a cell that contains both electrodes. Neither electrode has to
  be a hydrogen electrode, which is difficult to work with experimentally.

  Using Eq. <reference|Eo=Eo(R)-Eo(L)> to calculate standard cell potentials
  from standard electrode potentials saves a lot of experimental work. For
  example, measurement of <math|<Eeq><st>> for ten different cells, only one
  of which needs to include a hydrogen electrode, provides values of
  <math|E<st>> for ten electrodes other than <math|E<st|=>=0> for the
  hydrogen electrode. From these ten values of <math|E<st>>, values of
  <math|<Eeq><st>> can be calculated for 35 other cells without hydrogen
  electrodes.
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|13>
    <associate|page-first|353>
    <associate|preamble|false>
    <associate|section-nr|4>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|....=Del(r)G(cell)|<tuple|14.3.5|?>>
    <associate|14-cell rxns|<tuple|14.1|?>>
    <associate|14-el rxn eqm derivation|<tuple|14.3.1|?>>
    <associate|14-eval Eo|<tuple|14.5|?>>
    <associate|14-metal-metal contacts|<tuple|14.2.3.1|?>>
    <associate|14-molar rxn quantities|<tuple|14.3|?>>
    <associate|14-st molar rxn quantities|<tuple|14.3.3|?>>
    <associate|AgCl-Ag Nernst eq|<tuple|14.4.5|?>>
    <associate|Chap. 14|<tuple|14|?>>
    <associate|Cu electrode eqm|<tuple|14.2.4|?>>
    <associate|Del phi, metal jnc|<tuple|14.2.3|?>>
    <associate|Del(r)G(cell)=-z[.-.]|<tuple|14.3.6|?>>
    <associate|Del(r)G(cell)=-z[LT-RT]|<tuple|14.3.7|?>>
    <associate|E'=E-...|<tuple|14.5.2|?>>
    <associate|E(cell)^o=E(cell,eq)+|<tuple|14.5.1|?>>
    <associate|E=-del(r)G/zf|<tuple|14.3.13|?>>
    <associate|E=-del(r)G/zf+Ej|<tuple|14.3.14|?>>
    <associate|Eo=Eo(R)-Eo(L)|<tuple|14.6.3|?>>
    <associate|Faraday const|<tuple|electron number|?>>
    <associate|Nernst eq|<tuple|14.4.1|?>>
    <associate|Nernst eq, 298K|<tuple|14.4.2|?>>
    <associate|auto-1|<tuple|14|?>>
    <associate|auto-10|<tuple|14.1.1|?>>
    <associate|auto-100|<tuple|<tuple|potential|standard cell|evaluation>|?>>
    <associate|auto-101|<tuple|<tuple|debye-huckel|equation|mean ionic>|?>>
    <associate|auto-102|<tuple|14.5.1|?>>
    <associate|auto-103|<tuple|<tuple|standard|cell potential|evaluation>|?>>
    <associate|auto-104|<tuple|<tuple|potential|standard cell|evaluation>|?>>
    <associate|auto-105|<tuple|14.6|?>>
    <associate|auto-106|<tuple|Standard|?>>
    <associate|auto-107|<tuple|Hydrogen electrode|?>>
    <associate|auto-108|<tuple|Electrode|?>>
    <associate|auto-109|<tuple|standard hydrogen electrode|?>>
    <associate|auto-11|<tuple|Electrical|?>>
    <associate|auto-110|<tuple|Standard|?>>
    <associate|auto-111|<tuple|Electrode|?>>
    <associate|auto-112|<tuple|standard electrode potential|?>>
    <associate|auto-12|<tuple|Electron|?>>
    <associate|auto-13|<tuple|Ionic conductor of a galvanic cell|?>>
    <associate|auto-14|<tuple|Liquid junction|?>>
    <associate|auto-15|<tuple|Electrode|?>>
    <associate|auto-16|<tuple|electrode|?>>
    <associate|auto-17|<tuple|14.1.2|?>>
    <associate|auto-18|<tuple|14.1.1|?>>
    <associate|auto-19|<tuple|Hydrogen electrode|?>>
    <associate|auto-2|<tuple|Electrochemical|?>>
    <associate|auto-20|<tuple|Electrode|?>>
    <associate|auto-21|<tuple|Cell|?>>
    <associate|auto-22|<tuple|14.1.3|?>>
    <associate|auto-23|<tuple|Electrode|?>>
    <associate|auto-24|<tuple|electrode reaction|?>>
    <associate|auto-25|<tuple|Cell|?>>
    <associate|auto-26|<tuple|Reaction|?>>
    <associate|auto-27|<tuple|cell reaction|?>>
    <associate|auto-28|<tuple|Cell|?>>
    <associate|auto-29|<tuple|Cell|?>>
    <associate|auto-3|<tuple|Cell|?>>
    <associate|auto-30|<tuple|Cell|?>>
    <associate|auto-31|<tuple|14.1.2|?>>
    <associate|auto-32|<tuple|Liquid junction|?>>
    <associate|auto-33|<tuple|Liquid junction|?>>
    <associate|auto-34|<tuple|14.1.4|?>>
    <associate|auto-35|<tuple|Electron|?>>
    <associate|auto-36|<tuple|electron number|?>>
    <associate|auto-37|<tuple|Faraday constant|?>>
    <associate|auto-38|<tuple|Faraday constant|?>>
    <associate|auto-39|<tuple|14.2|?>>
    <associate|auto-4|<tuple|Galvanic cell|?>>
    <associate|auto-40|<tuple|Electric|?>>
    <associate|auto-41|<tuple|Potential|?>>
    <associate|auto-42|<tuple|electric potential|?>>
    <associate|auto-43|<tuple|Electric|?>>
    <associate|auto-44|<tuple|Charge|?>>
    <associate|auto-45|<tuple|Electric|?>>
    <associate|auto-46|<tuple|Inner electric potential|?>>
    <associate|auto-47|<tuple|Galvani potential|?>>
    <associate|auto-48|<tuple|14.2.1|?>>
    <associate|auto-49|<tuple|Cell potential|?>>
    <associate|auto-5|<tuple|Cell|?>>
    <associate|auto-50|<tuple|cell potential|?>>
    <associate|auto-51|<tuple|Electric|?>>
    <associate|auto-52|<tuple|Equilibrium cell potential|?>>
    <associate|auto-53|<tuple|equilibrium cell potential|?>>
    <associate|auto-54|<tuple|Electromotive force|?>>
    <associate|auto-55|<tuple|Emf|?>>
    <associate|auto-56|<tuple|IUPAC Green Book|?>>
    <associate|auto-57|<tuple|14.2.2|?>>
    <associate|auto-58|<tuple|14.2.1|?>>
    <associate|auto-59|<tuple|Potentiometer to measure equilibrium cell
    potential|?>>
    <associate|auto-6|<tuple|galvanic cell|?>>
    <associate|auto-60|<tuple|14.2.3|?>>
    <associate|auto-61|<tuple|14.2.2|?>>
    <associate|auto-62|<tuple|Electrical|?>>
    <associate|auto-63|<tuple|Work|?>>
    <associate|auto-64|<tuple|Internal|?>>
    <associate|auto-65|<tuple|Resistance|?>>
    <associate|auto-66|<tuple|14.2.3.1|?>>
    <associate|auto-67|<tuple|Contact|?>>
    <associate|auto-68|<tuple|<tuple|chemical potential|electrons>|?>>
    <associate|auto-69|<tuple|Electron|?>>
    <associate|auto-7|<tuple|Electric|?>>
    <associate|auto-70|<tuple|Contact|?>>
    <associate|auto-71|<tuple|Thermocouple|?>>
    <associate|auto-72|<tuple|Thermopile|?>>
    <associate|auto-73|<tuple|14.2.3.2|?>>
    <associate|auto-74|<tuple|14.2.3.3|?>>
    <associate|auto-75|<tuple|Liquid junction|?>>
    <associate|auto-76|<tuple|Liquid junction|?>>
    <associate|auto-77|<tuple|Salt bridge|?>>
    <associate|auto-78|<tuple|14.3|?>>
    <associate|auto-79|<tuple|<tuple|gibbs energy|molar reaction|cell
    reaction>|?>>
    <associate|auto-8|<tuple|Current, electric|?>>
    <associate|auto-80|<tuple|14.3.1|?>>
    <associate|auto-81|<tuple|14.3.2|?>>
    <associate|auto-82|<tuple|<tuple|gibbs energy|molar reaction|cell
    reaction>|?>>
    <associate|auto-83|<tuple|14.3.3|?>>
    <associate|auto-84|<tuple|Standard|?>>
    <associate|auto-85|<tuple|Potential|?>>
    <associate|auto-86|<tuple|standard cell potential|?>>
    <associate|auto-87|<tuple|<tuple|equilibrium constant|thermodynamic|cell
    reaction>|?>>
    <associate|auto-88|<tuple|Solubility product|?>>
    <associate|auto-89|<tuple|Enthalpy|?>>
    <associate|auto-9|<tuple|14.1|?>>
    <associate|auto-90|<tuple|Entropy|?>>
    <associate|auto-91|<tuple|14.4|?>>
    <associate|auto-92|<tuple|Standard|?>>
    <associate|auto-93|<tuple|Potential|?>>
    <associate|auto-94|<tuple|Nernst|?>>
    <associate|auto-95|<tuple|Nernst equation|?>>
    <associate|auto-96|<tuple|Reaction|?>>
    <associate|auto-97|<tuple|<tuple|activity coefficient|mean
    ionic|nernst>|?>>
    <associate|auto-98|<tuple|14.5|?>>
    <associate|auto-99|<tuple|<tuple|standard|cell potential|evaluation>|?>>
    <associate|c14|<tuple|14|?>>
    <associate|c14 sec cdcr|<tuple|14.1|?>>
    <associate|c14 sec cdcr-charge|<tuple|14.1.4|?>>
    <associate|c14 sec cdcr-diagrams|<tuple|14.1.2|?>>
    <associate|c14 sec cdcr-elements|<tuple|14.1.1|?>>
    <associate|c14 sec epc|<tuple|14.2|?>>
    <associate|c14 sec epc-potential|<tuple|14.2.1|?>>
    <associate|c14 sec escp|<tuple|14.5|?>>
    <associate|c14 sec mrqcr-drgc-drg|<tuple|14.3.2|?>>
    <associate|c14 sec mrqcr-drgc-e|<tuple|14.3.1|?>>
    <associate|c14 sec nernst|<tuple|14.4|?>>
    <associate|c14 sec sep|<tuple|14.6|?>>
    <associate|c14 sec-cdcr-reactions|<tuple|14.1.3|?>>
    <associate|c14 sec-epc-measure|<tuple|14.2.2|?>>
    <associate|c14 sec-epc-potential-diff|<tuple|14.2.3|?>>
    <associate|c14 sec-mrqcr|<tuple|14.3|?>>
    <associate|c14 sec-mrqcr-std-quantities|<tuple|14.3.3|?>>
    <associate|dQ=-zFdxi|<tuple|14.1.1|?>>
    <associate|del(r)G(cell) defn|<tuple|14.3.1|?>>
    <associate|del(r)G(cell) no liq jnc|<tuple|14.3.9|?>>
    <associate|del(r)G(cell) with liq jnc|<tuple|14.3.12|?>>
    <associate|del(r)G(cell)=|<tuple|14.3.10|?>>
    <associate|del(r)G(cell)=-zFE|<tuple|14.3.8|?>>
    <associate|del(r)G(cell)=....|<tuple|14.3.11|?>>
    <associate|del(r)Go=-zFEo|<tuple|14.3.15|?>>
    <associate|del(r)Hmo=nuF..|<tuple|14.3.17|?>>
    <associate|del(r)Smo=(nuF)dEo/dT|<tuple|14.3.18|?>>
    <associate|direct rxn|<tuple|IUPAC Green Book|?>>
    <associate|fig:14-AgCl-Ag st pot|<tuple|14.5.1|?>>
    <associate|fig:14-Zn-Cu cell|<tuple|14.1.2|?>>
    <associate|fig:14-el pot profile|<tuple|14.2.2|?>>
    <associate|fig:14-galvanic cell|<tuple|14.1.1|?>>
    <associate|fig:14-potentiometer|<tuple|14.2.1|?>>
    <associate|footnote-14.1.1|<tuple|14.1.1|?>>
    <associate|footnote-14.1.2|<tuple|14.1.2|?>>
    <associate|footnote-14.1.3|<tuple|14.1.3|?>>
    <associate|footnote-14.1.4|<tuple|14.1.4|?>>
    <associate|footnote-14.2.1|<tuple|14.2.1|?>>
    <associate|footnote-14.2.2|<tuple|14.2.2|?>>
    <associate|footnote-14.5.1|<tuple|14.5.1|?>>
    <associate|footnr-14.1.1|<tuple|14.1.1|?>>
    <associate|footnr-14.1.2|<tuple|14.1.2|?>>
    <associate|footnr-14.1.3|<tuple|14.1.3|?>>
    <associate|footnr-14.1.4|<tuple|14.1.4|?>>
    <associate|footnr-14.2.1|<tuple|IUPAC Green Book|?>>
    <associate|footnr-14.2.2|<tuple|Thermopile|?>>
    <associate|footnr-14.5.1|<tuple|14.5.1|?>>
    <associate|ln(K)=nuFEo/RT|<tuple|14.3.16|?>>
    <associate|mu_e(phi)=mu_e(0)-Fphi|<tuple|14.2.2|?>>
    <associate|rxn1+rxn2=rxn3|<tuple|14.6.1|?>>
    <associate|sum(nu_i)(nu_i)+..=0|<tuple|14.3.4|?>>
    <associate|sum=0 (left el)|<tuple|14.3.2|?>>
    <associate|sum=0 (right el)|<tuple|14.3.3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      greenbook-3

      harned-32
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|14.1.1>|>
        A galvanic cell without liquid junction.
      </surround>|<pageref|auto-18>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|14.1.2>|>
        Zinc\Ucopper galvanic cell with porous barrier (heavy dashed line)
        separating two electrolyte solutions. The dashed rectangle indicates
        the system boundary.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Open
                circuit with isolated system in equilibrium state.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Closed
                circuit.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-31>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|14.2.1>|>
        Potentiometer to measure the zero-current cell potential of a
        galvanic cell.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Galvanic
                cell with zero current.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Galvanic
                cell included in potentiometer circuit; G is a galvanometer.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-58>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|14.2.2>|>
        Galvani potential profile across a galvanic cell (schematic). LT and
        RT are the left and right terminals, LE and RE are the left and right
        electron conductors, and I is an ionic conductor such as an
        electrolyte solution.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Cell
                with zero current.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>The
                same cell with finite current.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-61>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|14.5.1>|>
        <with|mode|<quote|math>|E<rprime|'>> (defined by Eq.
        <reference|E'=E-...>) as a function of HCl molality for the cell of
        Fig. <reference|fig:14-galvanic cell> at
        <with|mode|<quote|math>|298.15<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>>.<space|0spc><assign|footnote-nr|1><hidden-binding|<tuple>|14.5.1><assign|fnote-+1PCvW8IQuYAAePB|<quote|14.5.1>><assign|fnlab-+1PCvW8IQuYAAePB|<quote|14.5.1>><rsup|<with|font-shape|<quote|right>|<reference|footnote-14.5.1>>>
        The dashed line is a least-squares fit to a linear relation.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%9FF3E9B8-98077780>|<link|hyperlink|<id|%9FF3E9B8-98077780>|<url|#footnr-14.5.1>>|14.5.1>.
        |<hidden-binding|<tuple|footnote-14.5.1>|14.5.1>|>>>Data from Ref.
        [<write|bib|harned-32><reference|bib-harned-32>] with
        <with|mode|<quote|math>|f<rsub|<with|mode|<quote|text>|H<rsub|2>>>>
        set equal to <with|mode|<quote|math>|p<rsub|<with|mode|<quote|text>|H<rsub|2>>>>
        and the parameter <with|mode|<quote|math>|a> set equal to
        <with|mode|<quote|math>|4.3\<times\>10<rsup|-10>
        <with|mode|<quote|text>|m>>.
      </surround>|<pageref|auto-102>>
    </associate>
    <\associate|gly>
      <tuple|normal|galvanic cell|<pageref|auto-6>>

      <tuple|normal|electrode|<pageref|auto-16>>

      <tuple|normal|electrode reaction|<pageref|auto-24>>

      <tuple|normal|cell reaction|<pageref|auto-27>>

      <tuple|normal|electron number|<pageref|auto-36>>

      <tuple|normal|Faraday constant|<pageref|auto-38>>

      <tuple|normal|electric potential|<pageref|auto-42>>

      <tuple|normal|cell potential|<pageref|auto-50>>

      <tuple|normal|equilibrium cell potential|<pageref|auto-53>>

      <tuple|normal|standard cell potential|<pageref|auto-86>>

      <tuple|normal|Nernst equation|<pageref|auto-95>>

      <tuple|normal|standard hydrogen electrode|<pageref|auto-109>>

      <tuple|normal|standard electrode potential|<pageref|auto-112>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Electrochemical|cell>|<pageref|auto-2>>

      <tuple|<tuple|Cell|electrochemical>|<pageref|auto-3>>

      <tuple|<tuple|Galvanic cell>|<pageref|auto-4>>

      <tuple|<tuple|Cell|galvanic>|<pageref|auto-5>>

      <tuple|<tuple|Electric|current>|<pageref|auto-7>>

      <tuple|<tuple|Current, electric>|<pageref|auto-8>>

      <tuple|<tuple|Electrical|conductor of a galvanic
      cell>|<pageref|auto-11>>

      <tuple|<tuple|Electron|conductor of a galvanic cell>|<pageref|auto-12>>

      <tuple|<tuple|Ionic conductor of a galvanic cell>|<pageref|auto-13>>

      <tuple|<tuple|Liquid junction>|<pageref|auto-14>>

      <tuple|<tuple|Electrode>|<pageref|auto-15>>

      <tuple|<tuple|Hydrogen electrode>|<pageref|auto-19>>

      <tuple|<tuple|Electrode|hydrogen>|<pageref|auto-20>>

      <tuple|<tuple|Cell|diagram>|<pageref|auto-21>>

      <tuple|<tuple|Electrode|reaction>|<pageref|auto-23>>

      <tuple|<tuple|Cell|reaction>|<pageref|auto-25>>

      <tuple|<tuple|Reaction|cell>|<pageref|auto-26>>

      <tuple|<tuple|Cell|without liquid junction>|<pageref|auto-28>>

      <tuple|<tuple|Cell|without transference>|<pageref|auto-29>>

      <tuple|<tuple|Cell|with transference>|<pageref|auto-30>>

      <tuple|<tuple|Liquid junction>|<pageref|auto-32>>

      <tuple|<tuple|Liquid junction|potential>|<pageref|auto-33>>

      <tuple|<tuple|Electron|number>|<pageref|auto-35>>

      <tuple|<tuple|Faraday constant>|<pageref|auto-37>>

      <tuple|<tuple|Electric|potential>|<pageref|auto-40>>

      <tuple|<tuple|Potential|electric>|<pageref|auto-41>>

      <tuple|<tuple|Electric|charge>|<pageref|auto-43>>

      <tuple|<tuple|Charge|electric>|<pageref|auto-44>>

      <tuple|<tuple|Electric|potential|inner>|<pageref|auto-45>>

      <tuple|<tuple|Inner electric potential>|<pageref|auto-46>>

      <tuple|<tuple|Galvani potential>|<pageref|auto-47>>

      <tuple|<tuple|Cell potential>|<pageref|auto-49>>

      <tuple|<tuple|Electric|potential difference>|<pageref|auto-51>>

      <tuple|<tuple|Equilibrium cell potential>|<pageref|auto-52>>

      <tuple|<tuple|Electromotive force>|<pageref|auto-54>>

      <tuple|<tuple|Emf>|<pageref|auto-55>>

      <tuple|<tuple|IUPAC Green Book>|<pageref|auto-56>>

      <tuple|<tuple|Potentiometer to measure equilibrium cell
      potential>|<pageref|auto-59>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-62>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-63>>

      <tuple|<tuple|Internal|resistance>|<pageref|auto-64>>

      <tuple|<tuple|Resistance|internal>|<pageref|auto-65>>

      <tuple|<tuple|Contact|potential>|<pageref|auto-67>>

      <tuple|<tuple|chemical potential|electrons>|||<tuple|Chemical
      potential|of electrons>|<pageref|auto-68>>

      <tuple|<tuple|Electron|chemical potential>|<pageref|auto-69>>

      <tuple|<tuple|Contact|potential>|<pageref|auto-70>>

      <tuple|<tuple|Thermocouple>|<pageref|auto-71>>

      <tuple|<tuple|Thermopile>|<pageref|auto-72>>

      <tuple|<tuple|Liquid junction>|<pageref|auto-75>>

      <tuple|<tuple|Liquid junction|potential>|<pageref|auto-76>>

      <tuple|<tuple|Salt bridge>|<pageref|auto-77>>

      <tuple|<tuple|gibbs energy|molar reaction|cell reaction>||c14 sec-mrqcr
      idx1|<tuple|Gibbs energy|molar reaction|of a cell
      reaction>|<pageref|auto-79>>

      <tuple|<tuple|gibbs energy|molar reaction|cell reaction>||c14 sec-mrqcr
      idx1|<tuple|Gibbs energy|molar reaction|of a cell
      reaction>|<pageref|auto-82>>

      <tuple|<tuple|Standard|cell potential>|<pageref|auto-84>>

      <tuple|<tuple|Potential|standard cell>|<pageref|auto-85>>

      <tuple|<tuple|equilibrium constant|thermodynamic|cell
      reaction>|||<tuple|Equilibrium constant|thermodynamic|of a cell
      reaction>|<pageref|auto-87>>

      <tuple|<tuple|Solubility product>|<pageref|auto-88>>

      <tuple|<tuple|Enthalpy|reaction|standard molar of a cell
      reaction>|<pageref|auto-89>>

      <tuple|<tuple|Entropy|reaction|standard molar of a cell
      reaction>|<pageref|auto-90>>

      <tuple|<tuple|Standard|cell potential>|<pageref|auto-92>>

      <tuple|<tuple|Potential|standard cell>|<pageref|auto-93>>

      <tuple|<tuple|Nernst|equation>|<pageref|auto-94>>

      <tuple|<tuple|Reaction|quotient>|<pageref|auto-96>>

      <tuple|<tuple|activity coefficient|mean ionic|nernst>|||<tuple|Activity
      coefficient|mean ionic|from the Nernst equation>|<pageref|auto-97>>

      <tuple|<tuple|standard|cell potential|evaluation>||c14 sec escp
      idx1|<tuple|Standard|cell potential|evaluation>|<pageref|auto-99>>

      <tuple|<tuple|potential|standard cell|evaluation>||c14 sec escp
      idx2|<tuple|Potential|standard cell|evaluation of>|<pageref|auto-100>>

      <tuple|<tuple|debye-huckel|equation|mean
      ionic>|||<tuple|Debye\UH�ckel|equation|for a mean ionic activity
      coefficient>|<pageref|auto-101>>

      <tuple|<tuple|standard|cell potential|evaluation>||c14 sec escp
      idx1|<tuple|Standard|cell potential|evaluation>|<pageref|auto-103>>

      <tuple|<tuple|potential|standard cell|evaluation>||c14 sec escp
      idx2|<tuple|Potential|standard cell|evaluation of>|<pageref|auto-104>>

      <tuple|<tuple|Standard|hydrogen electrode>|<pageref|auto-106>>

      <tuple|<tuple|Hydrogen electrode|standard>|<pageref|auto-107>>

      <tuple|<tuple|Electrode|hydrogen|standard>|<pageref|auto-108>>

      <tuple|<tuple|Standard|electrode potential>|<pageref|auto-110>>

      <tuple|<tuple|Electrode|potential, standard>|<pageref|auto-111>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|14<space|2spc>Galvanic
      Cells> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      14.1<space|2spc>Cell Diagrams and Cell Reactions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>

      <with|par-left|<quote|1tab>|14.1.1<space|2spc>Elements of a galvanic
      cell <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|1tab>|14.1.2<space|2spc>Cell diagrams
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>>

      <with|par-left|<quote|1tab>|14.1.3<space|2spc>Electrode reactions and
      the cell reaction <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-22>>

      <with|par-left|<quote|1tab>|14.1.4<space|2spc>Advancement and charge
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-34>>

      14.2<space|2spc>Electric Potentials in the Cell
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-39>

      <with|par-left|<quote|1tab>|14.2.1<space|2spc>Cell potential
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-48>>

      <with|par-left|<quote|1tab>|14.2.2<space|2spc>Measuring the equilibrium
      cell potential <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-57>>

      <with|par-left|<quote|1tab>|14.2.3<space|2spc>Interfacial potential
      differences <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-60>>

      <with|par-left|<quote|2tab>|14.2.3.1<space|2spc>Metal\Umetal contacts
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-66>>

      <with|par-left|<quote|2tab>|14.2.3.2<space|2spc>Metal\Uelectrolyte
      interfaces <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-73>>

      <with|par-left|<quote|2tab>|14.2.3.3<space|2spc>Liquid junctions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-74>>

      14.3<space|2spc>Molar Reaction Quantities of the Cell Reaction
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-78>

      <with|par-left|<quote|1tab>|14.3.1<space|2spc>Relation between
      <with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G<rsub|<with|mode|<quote|text>|cell>>>
      and <with|mode|<quote|math>|E<rsub|<with|mode|<quote|text>|cell>,<with|mode|<quote|text>|eq>>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-80>>

      <with|par-left|<quote|1tab>|14.3.2<space|2spc>Relation between
      <with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G<rsub|<with|mode|<quote|text>|cell>>>
      and <with|mode|<quote|math>|\<Delta\><rsub|<with|mode|<quote|text>|r>>*G>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-81>>

      <with|par-left|<quote|1tab>|14.3.3<space|2spc>Standard molar reaction
      quantities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-83>>

      14.4<space|2spc>The Nernst Equation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-91>

      14.5<space|2spc>Evaluation of the Standard Cell Potential
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-98>

      14.6<space|2spc>Standard Electrode Potentials
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-105>
    </associate>
  </collection>
</auxiliary>