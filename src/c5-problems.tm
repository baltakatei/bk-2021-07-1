<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;
  </hide-preamble>

  <new-page*><section|Problems>

  <\problem>
    <label|prb:5-H(T)>Show that the enthalpy of a fixed amount of an ideal
    gas depends only on the temperature.
  </problem>

  <\problem>
    <label|prb:5-ht cap of id gas>From concepts in this chapter, show that
    the heat capacities <math|C<rsub|V>> and <math|C<rsub|p>> of a fixed
    amount of an ideal gas are functions only of <math|T>.
  </problem>

  <\problem>
    During the reversible expansion of a fixed amount of an ideal gas, each
    increment of heat is given by the expression
    <math|<dq>=C<rsub|V>*<dif>T+<around|(|n*R*T/V|)><dif>V> (Eq.
    <reference|dq=CVdT+(nRT/V)dV>).

    <\enumerate-alpha>
      <item>A necessary and sufficient condition for this expression to be an
      exact differential is that the <index|Reciprocity relation>reciprocity
      relation must be satisfied for the independent variables <math|T> and
      <math|V> (see Appendix <reference|app:state>). Apply this test to show
      that the expression is <em|not> an exact differential, and that heat
      therefore is not a state function.

      <item>By the same method, show that the entropy increment during the
      reversible expansion, given by the expression <math|<dif>S=<dq>/T>, is
      an exact differential, so that entropy is a state function.
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:5-fncs from A>This problem illustrates how an expression for
    one of the thermodynamic potentials as a function of its natural
    variables contains the information needed to obtain expressions for the
    other thermodynamic potentials and many other state functions.

    From statistical mechanical theory, a simple model for a hypothetical
    \Phard-sphere\Q liquid (spherical molecules of finite size without
    attractive intermolecular forces) gives the following expression for the
    Helmholtz energy with its natural variables <math|T>, <math|V>, and
    <math|n> as the independent variables:

    <\equation*>
      A=-n*R*T*ln <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>-n*R*T+n*a
    </equation*>

    Here <math|a>, <math|b>, and <math|c> are constants. Derive expressions
    for the following state functions of this hypothetical liquid as
    functions of <math|T>, <math|V>, and <math|n>.

    <\enumerate-alpha>
      <item>The entropy, <math|S>

      <item>The pressure, <math|p>\ 

      <item>The chemical potential, <math|\<mu\>>

      <item>The internal energy, <math|U>

      <item>The enthalpy, <math|H>

      <item>The Gibbs energy, <math|G>

      <item>The heat capacity at constant volume, <math|C<rsub|V>>

      <item>The heat capacity at constant pressure, <math|C<rsub|p>> (hint:
      use the expression for <math|p> to solve for <math|V> as a function of
      <math|T>, <math|p>, and <math|n>; then use <math|H=U+p*V>)
    </enumerate-alpha>
  </problem>

  <\problem>
    Figure <vpageref|fig:5-irrev vaporization> depicts a hypothetical liquid
    in equilibrium with its vapor. The liquid and gas are confined in a
    cylinder by a piston. An electrical resistor is immersed in the liquid.
    The <em|system> is the contents of the cylinder to the left of the piston
    (the liquid, gas, and resistor). The initial state of the system is
    described by

    <\equation*>
      V<rsub|1>=0.2200 <text|m><rsup|3><space|2em>T<rsub|1>=300.0
      <text|K><space|2em>p<rsub|1>=2.50\<times\>10<rsup|5> <text|Pa>
    </equation*>

    A constant current <math|I=0.5000 <text|A>> is passed for <math|1600
    <text|s>> through the resistor, which has electric resistance
    <math|R<rsub|<text|el>>=50.00 <upOmega>>. The piston moves slowly to the
    right against a constant external pressure equal to the vapor pressure of
    the liquid, <math|2.50\<times\>10<rsup|5> <text|Pa>>, and some of the
    liquid vaporizes. Assume that the process is adiabatic and that <math|T>
    and <math|p> remain uniform and constant. The final state is described by

    <\equation*>
      V<rsub|2>=0.2400 <text|m><rsup|3><space|2em>T<rsub|2>=300.0
      <text|K><space|2em>p<rsub|2>=2.50<timesten|5> <text|Pa>
    </equation*>

    <\enumerate-alpha>
      <item>Calculate <math|q>, <math|w>, <math|\<Delta\>*U>, and
      <math|\<Delta\>*H>.

      <item>Is the process reversible? Explain.

      <item>Devise a reversible process that accomplishes the same change of
      state, and use it to calculate <math|<Del>S>.

      <item>Compare <math|q> for the reversible process with <math|<Del>H>.
      Does your result agree with Eq. <reference|delH=q (dp=0)>?
    </enumerate-alpha>
  </problem>

  <\float|float|hb>
    <\framed>
      <\big-figure>
        <image|05-SUP/IRREVVAP.eps|149pt|84pt||>
      <|big-figure>
        <label|fig:5-irrev vaporization>
      </big-figure>
    </framed>
  </float>

  <\problem>
    Use the data in Table <vpageref|tbl:5-H2O surf ten> to evaluate
    <math|<pd|S|A<rsub|<text|s>>|T,p>> at <math|25 <degC>>, which is the rate
    at which the entropy changes with the area of the air\Uwater interface at
    this temperature.
  </problem>

  <float|float|thb|<\big-table|<tabular|<tformat|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-rborder|0ln>|<cwith|1|-1|1|-1|cell-halign|c>|<cwith|2|-1|2|2|cell-halign|c>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|6|6|1|-1|cell-bborder|1ln>|<cwith|2|-1|1|1|cell-lborder|0ln>|<cwith|2|-1|2|2|cell-rborder|0ln>|<table|<row|<cell|<math|t/<degC>>>|<cell|<math|\<gamma\>/10<rsup|-6>
  <text|J>*<text|cm><rsup|-2>>>>|<row|<cell|15>|<cell|7.350>>|<row|<cell|20>|<cell|7.275>>|<row|<cell|25>|<cell|7.199>>|<row|<cell|30>|<cell|7.120>>|<row|<cell|35>|<cell|7.041>>>>>>
    <label|tbl:5-H2O surf ten>Surface tension of water at <math|1
    <text|bar>><rsup|<note-inline||+WjtYO0N1ocE0TBr>>

    <rsup|<note-ref|+WjtYO0N1ocE0TBr>>Ref. <cite|vargaftik-83>
  </big-table>>

  <\problem>
    When an ordinary <index|Rubber, thermodynamics of>rubber band is hung
    from a clamp and stretched with constant downward force <math|F> by a
    weight attached to the bottom end, gentle heating is observed to cause
    the rubber band to contract in length. To keep the length <math|l> of the
    rubber band constant during heating, <math|F> must be increased. The
    stretching work is given by <math|<dw><rprime|'>=F*<dif>l>. From this
    information, find the sign of the partial derivative <math|<pd|T|l|S,p>>;
    then predict whether stretching of the rubber band will cause a heating
    or a cooling effect.

    (Hint: make a Legendre transform of <math|U> whose total differential has
    the independent variables needed for the partial derivative, and write a
    reciprocity relation.)

    You can check your prediction experimentally by touching a rubber band to
    the side of your face before and after you rapidly stretch it.
  </problem>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|5>
    <associate|page-first|119>
    <associate|page-height|auto>
    <associate|page-medium|papyrus>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|8>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|9|?>>
    <associate|auto-2|<tuple|Reciprocity relation|?>>
    <associate|auto-3|<tuple|9.1|?>>
    <associate|auto-4|<tuple|9.1|?>>
    <associate|auto-5|<tuple|Rubber, thermodynamics of|?>>
    <associate|fig:5-irrev vaporization|<tuple|9.1|?>>
    <associate|footnote-9.1|<tuple|9.1|?>>
    <associate|footnr-9.1|<tuple|9.1|?>>
    <associate|prb:5-H(T)|<tuple|9.1|?>>
    <associate|prb:5-fncs from A|<tuple|9.4|?>>
    <associate|prb:5-ht cap of id gas|<tuple|9.2|?>>
    <associate|tbl:5-H2O surf ten|<tuple|9.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      vargaftik-83
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.1>|>
        \;
      </surround>|<pageref|auto-3>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Reciprocity relation>|<pageref|auto-2>>

      <tuple|<tuple|Rubber, thermodynamics of>|<pageref|auto-5>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.1>|>
        Surface tension of water at <with|mode|<quote|math>|1
        <with|mode|<quote|text>|bar>><rsup|<surround|<assign|footnote-nr|1><hidden-binding|<tuple>|9.1><assign|fnote-+WjtYO0N1ocE0TBr|<quote|9.1>><assign|fnlab-+WjtYO0N1ocE0TBr|<quote|9.1>>||<with|font-size|<quote|0.771>|<surround|<locus|<id|%136890B8-927ED18>|<link|hyperlink|<id|%136890B8-927ED18>|<url|#footnr-9.1>>|9.1>.
        |<hidden-binding|<tuple|footnote-9.1>|9.1>|>>>>

        <rsup|<space|0spc><rsup|<with|font-shape|<quote|right>|<reference|footnote-9.1>>>>Ref.
        [<write|bib|vargaftik-83><reference|bib-vargaftik-83>]
      </surround>|<pageref|auto-4>>
    </associate>
    <\associate|toc>
      9<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>