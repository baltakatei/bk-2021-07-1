<TeXmacs|1.99.20>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|Problems>

  <\problem>
    Explain why an electric refrigerator, which transfers energy by means of
    heat from the cold food storage compartment to the warmer air in the
    room, is not an impossible \PClausius device.\Q
  </problem>

  <\problem>
    A system consisting of a fixed amount of an ideal gas is maintained in
    thermal equilibrium with a heat reservoir at temperature <math|T>. The
    system is subjected to the following isothermal cycle:

    <\enumerate>
      <item>The gas, initially in an equilibrium state with volume
      <math|V<rsub|0>>, is allowed to expand into a vacuum and reach a new
      equilibrium state of volume <math|V<rprime|'>>.

      <item>The gas is reversibly compressed from <math|V<rprime|'>> to
      <math|V<rsub|0>>.
    </enumerate>

    For this cycle, find expressions or values for <math|w>,
    <math|<big|oint><dbar|q>/T>, and <math|<big|oint><dvar|S>>.
  </problem>

  <\problem>
    In an irreversible isothermal process of a closed system:

    <\enumerate-alpha>
      <item>Is it possible for <math|\<Delta\>*S> to be negative?\ 

      <item>Is it possible for <math|\<Delta\>*S> to be less than <math|q/T>?
    </enumerate-alpha>
  </problem>

  <\problem>
    Suppose you have two blocks of copper, each of heat capacity
    <math|C<rsub|V>=200.0 <frac|<text|J>|<text|K>>>. Initially one block has
    a uniform temperature of <math|300.00 <text|K>> and the other
    <math|310.00 <text|K>>. Calculate the entropy change that occurs when you
    place the two blocks in thermal contact with one another and surround
    them with perfect thermal insulation. Is the sign of <math|\<Delta\>*S>
    consistent with the second law? (Assume the process occurs at constant
    volume.)
  </problem>

  <\problem>
    Refer to the apparatus shown in Figs. <vpageref|fig:3-porous plug> and
    <vpageref|fig:3-evacuated vessel> and described in Probs.
    <reference|prb:3-porous plug> and <reference|prb:3-evacuated vessel>. For
    both systems, evaluate <math|\<Delta\>*S> for the process that results
    from opening the stopcock. Also evaluate
    <math|<big|int><dbar|q>/T<rsub|<text|ext>>> for both processes (for the
    apparatus in Fig. <reference|fig:3-evacuated vessel>, assume the vessels
    have adiabatic walls). Are your results consistent with the mathematical
    statement of the second law?
  </problem>

  <\problem>
    Figure <vpageref|fig:4-paddle_problem> shows the walls of a rigid
    thermally-insulated box (cross hatching). The <em|system> is the contents
    of this box. In the box is a paddle wheel immersed in a container of
    water, connected by a cord and pulley to a weight of mass <math|m>. The
    weight rests on a stop located a distance <math|h> above the bottom of
    the box. Assume the heat capacity of the system, <math|C<rsub|V>>, is
    independent of temperature. Initially the system is in an equilibrium
    state at temperature <math|T<rsub|1>>. When the stop is removed, the
    weight irreversibly sinks to the bottom of the box, causing the paddle
    wheel to rotate in the water. Eventually the system reaches a final
    equilibrium state with thermal equilibrium. Describe a <em|reversible>
    process with the same entropy change as this irreversible process, and
    derive a formula for <math|\<Delta\>*S> in terms of <math|m>, <math|h>,
    <math|C<rsub|V>>, and <math|T<rsub|1>>.
  </problem>

  <\float|float|thb>
    <\framed>
      <\big-figure>
        <image|04-SUP/paddle_problem.eps|135pt|124pt||>
      <|big-figure>
        <label|fig:4-paddle_problem>
      </big-figure>
    </framed>
  </float>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|4>
    <associate|page-first|105>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|8>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|9|?>>
    <associate|auto-2|<tuple|9.1|?>>
    <associate|fig:4-paddle_problem|<tuple|9.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.1>|>
        \;
      </surround>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      9<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>