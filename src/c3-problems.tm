<TeXmacs|1.99.20>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|Problems>

  <\problem>
    Assume you have a metal spring that obeys Hooke's law:
    <math|F=c*<around|(|l-l<rsub|0>|)>>, where <math|F> is the force exerted
    on the spring of length <math|l>, <math|l<rsub|0>> is the length of the
    unstressed spring, and <math|c> is the spring constant. Find an
    expression for the work done on the spring when you reversibly compress
    it from length <math|l<rsub|0>> to a shorter length <math|l<rprime|'>>.
  </problem>

  <\problem>
    The apparatus shown in Fig. <vpageref|fig:3-marble> consists of fixed
    amounts of water and air and an incompressible solid glass sphere (a
    marble), all enclosed in a rigid vessel resting on a lab bench. Assume
    the marble has an adiabatic outer layer so that its temperature cannot
    change, and that the walls of the vessel are also adiabatic.

    Initially the marble is suspended above the water. When released, it
    falls through the air into the water and comes to rest at the bottom of
    the vessel, causing the water and air (but not the marble) to become
    slightly warmer. The process is complete when the system returns to an
    equilibrium state. The system energy change during this process depends
    on the frame of reference and on how the system is defined.
    <math|\<Delta\>*E<rsub|<text|sys>>> is the energy change in a lab frame,
    and <math|\<Delta\>*U> is the energy change in a specified local frame.

    For each of the following definitions of the system, give the <em|sign>
    (positive, negative, or zero) of both <math|\<Delta\>*E<rsub|<text|sys>>>
    and <math|\<Delta\>*U>, and state your reasoning. Take the local frame
    for each system to be a center-of-mass frame.

    <\enumerate-alpha>
      <item>The system is the marble.

      <item>The system is the combination of water and air.

      <item>The system is the combination of water, air, and marble.
    </enumerate-alpha>
  </problem>

  <\float|float|hb>
    <\framed>
      <\big-figure>
        <image|03-SUP/MARBLE-2.eps|58pt|115pt||>
      <|big-figure>
        <label|fig:3-marble>
      </big-figure>
    </framed>
  </float>

  <\problem>
    <label|prb:3-porous plug>Figure <vpageref|fig:3-porous plug> shows the
    initial state of an apparatus consisting of an ideal gas in a bulb, a
    stopcock, a porous plug, and a cylinder containing a frictionless piston.
    The walls are diathermal, and the surroundings are at a constant
    temperature of <math|300.0 <text|K>> and a constant pressure of
    <math|1.00 <text|bar>>.

    When the stopcock is opened, the gas diffuses slowly through the porous
    plug, and the piston moves slowly to the right. The process ends when the
    pressures are equalized and the piston stops moving. The <em|system> is
    the gas. Assume that during the process the temperature throughout the
    system differs only infinitesimally from <math|300.0 <text|K>> and the
    pressure on both sides of the piston differs only infinitesimally from
    <math|1.00 <text|bar>>.

    <\enumerate-alpha>
      <item>Which of these terms correctly describes the process: isothermal,
      isobaric, isochoric, reversible, irreversible?

      <item>Calculate <math|q> and <math|w>.
    </enumerate-alpha>
  </problem>

  <\float|float|thb>
    <\framed>
      <\big-figure>
        <image|03-SUP/por-plug.eps|208pt|95pt||>
      <|big-figure>
        <label|fig:3-porous plug>
      </big-figure>
    </framed>
  </float>

  <\problem>
    Consider a horizontal cylinder-and-piston device similar to the one shown
    in Fig. <vpageref|fig:3-cylinder>. The piston has mass <math|m>. The
    cylinder wall is diathermal and is in thermal contact with a heat
    reservoir of temperature . The <em|system> is an amount <math|n> of an
    ideal gas confined in the cylinder by the piston.

    The initial state of the system is an equilibrium state described by
    <math|p<rsub|1>> and <math|T=T<rsub|<text|ext>>>. There is a constant
    external pressure <math|p<rsub|<text|ext>>>, equal to twice
    <math|p<rsub|1>>, that supplies a constant external force on the piston.
    When the piston is released, it begins to move to the left to compress
    the gas. Make the idealized assumptions that (1) the piston moves with
    negligible friction; and (2) the gas remains practically uniform (because
    the piston is massive and its motion is slow) and has a practically
    constant temperature <math|T=T<rsub|<text|ext>>> (because temperature
    equilibration is rapid).

    <\enumerate-alpha>
      <item>Describe the resulting process.

      <item>Describe how you could calculate <math|w> and <math|q> during the
      period needed for the piston velocity to become zero again.

      <item>Calculate <math|w> and <math|q> during this period for
      <math|0.500 <text|mol>> gas at <math|300 <text|K>>.
    </enumerate-alpha>
  </problem>

  <\problem>
    This problem is designed to test the assertion on page <pageref|w approx
    equal to w(lab)> that for typical thermodynamic processes in which the
    elevation of the center of mass changes, it is usually a good
    approximation to set <math|w> equal to <math|w<rsub|<text|lab>>>. The
    cylinder shown in Fig. <vpageref|fig:3-vertical cylinder> has a vertical
    orientation, so the elevation of the center of mass of the gas confined
    by the piston changes as the piston slides up or down. The <em|system> is
    the gas. Assume the gas is nitrogen (<math|M=28.0 <text|gmol><rsup|-1>>)
    at <math|300 <text|K>>, and initially the vertical length <math|l> of the
    gas column is one meter. Treat the nitrogen as an ideal gas, use a
    center-of-mass local frame, and take the center of mass to be at the
    midpoint of the gas column. Find the difference between the values of
    <math|w> and <math|w<rsub|<text|lab>>>, expressed as a percentage of
    <math|w>, when the gas is expanded reversibly and isothermally to twice
    its initial volume.
  </problem>

  <\float|float|thb>
    <\framed>
      <\big-figure>
        <image|03-SUP/vertical-cyl.eps|52pt|164pt||>
      <|big-figure>
        <label|fig:3-vertical cylinder>
      </big-figure>
    </framed>
  </float>

  <\problem>
    Figure <vpageref|fig:3-spont compression> shows an ideal gas confined by
    a frictionless piston in a vertical cylinder. The <em|system> is the gas,
    and the boundary is adiabatic. The downward force on the piston can be
    varied by changing the weight on top of it.

    <\enumerate-alpha>
      <item>Show that when the system is in an equilibrium state, the gas
      pressure is given by <math|p=m*g*h/V> where <math|m> is the combined
      mass of the piston and weight, <math|g> is the acceleration of free
      fall, and <math|h> is the elevation of the piston shown in the figure.

      <item>Initially the combined mass of the piston and weight is
      <math|m<rsub|1>>, the piston is at height <math|h<rsub|1>>, and the
      system is in an equilibrium state with conditions <math|p<rsub|1>> and
      <math|V<rsub|1>>. The initial temperature is
      <math|T<rsub|1>=p<rsub|1>*V<rsub|1>/n*R>. Suppose that an additional
      weight is suddenly placed on the piston, so that <math|m> increases
      from <math|m<rsub|1>> to <math|m<rsub|2>>, causing the piston to sink
      and the gas to be compressed adiabatically and spontaneously. Pressure
      gradients in the gas, a form of friction, eventually cause the piston
      to come to rest at a final position <math|h<rsub|2>>. Find the final
      volume, <math|V<rsub|2>>, as a function of <math|p<rsub|1>>,
      <math|p<rsub|2>>, <math|V<rsub|1>>, and <math|C<rsub|V>>. (Assume that
      the heat capacity of the gas, <math|C<rsub|V>>, is independent of
      temperature.) Hint: The potential energy of the surroundings changes by
      <math|m<rsub|2>*g*\<Delta\>*h>; since the kinetic energy of the piston
      and weights is zero at the beginning and end of the process, and the
      boundary is adiabatic, the internal energy of the gas must change by
      <math|-m<rsub|2>*g*\<Delta\>*h=-m<rsub|2>*g*\<Delta\>*V/A<rsub|<text|s>>=-p<rsub|2>*\<Delta\>*V>.

      <item>It might seem that by making the weight placed on the piston
      sufficiently large, <math|V<rsub|2>> could be made as close to zero as
      desired. Actually, however, this is not the case. Find expressions for
      <math|V<rsub|2>> and <math|T<rsub|2>> in the limit as <math|m<rsub|2>>
      approaches infinity, and evaluate <math|V<rsub|2>/V<rsub|1>> in this
      limit if the heat capacity is <math|C<rsub|V>=<around|(|3/2|)>*n*R>
      (the value for an ideal monatomic gas at room temperature).
    </enumerate-alpha>
  </problem>

  <\float|float|thb>
    <\framed>
      <\big-figure>
        <image|03-SUP/sponcomp.eps|93pt|115pt||>
      <|big-figure>
        <label|fig:3-spont compression>
      </big-figure>
    </framed>
  </float>

  \;

  <\problem>
    The solid curve in Fig. <vpageref|fig:3-adiabat and isotherms> shows the
    path of a reversible adiabatic expansion or compression of a fixed amount
    of an ideal gas. Information about the gas is given in the figure
    caption. For compression along this path, starting at <math|V=0.3000
    <text|dm><rsup|3>> and <math|T=300.0 <text|K>> and ending at
    <math|V=0.1000 <text|dm><rsup|3>>, find the final temperature to
    <math|0.1 <text|K>> and the work.
  </problem>

  <\problem>
    <label|prb:3-evacuated vessel>Figure <vpageref|fig:3-evacuated vessel>
    shows the initial state of an apparatus containing an ideal gas. When the
    stopcock is opened, gas passes into the evacuated vessel. The <em|system>
    is the gas. Find <math|q>, <math|w>, and <math|\<Delta\>*U> under the
    following conditions.

    <\enumerate-alpha>
      <item>The vessels have adiabatic walls.

      <item>The vessels have diathermal walls in thermal contact with a water
      bath maintained at <math|300. <text|K>>, and the final temperature in
      both vessels is <math|T=300. <text|K>>.
    </enumerate-alpha>
  </problem>

  <\float|float|thb>
    <\framed>
      <\big-figure>
        <image|03-SUP/evac-ves.eps|212pt|88pt||>
      <|big-figure>
        <label|fig:3-evacuated vessel>
      </big-figure>
    </framed>
  </float>

  <\problem>
    Consider a reversible process in which the shaft of system A in Fig.
    <reference|fig:3-shaft work> makes one revolution in the direction of
    increasing <math|\<vartheta\>>. Show that the gravitational work of the
    weight is the same as the shaft work given by
    <math|w=m*g*r*\<Delta\>*\<vartheta\>>.
  </problem>

  <\problem>
    <label|prb:3-Joule_expt>This problem guides you through a calculation of
    the mechanical equivalent of heat using data from one of <index|Joule,
    James Prescott> James Joule's experiments with a paddle wheel apparatus
    (see Sec. <reference|3-Joule paddle wheel>). The experimental data are
    collected in Table <vpageref|tbl:3-Joule data>.

    In each of his experiments, Joule allowed the weights of the apparatus to
    sink to the floor twenty times from a height of about <math|1.6
    <text|m>>, using a crank to raise the weights before each descent (see
    Fig. <vpageref|fig:3-Joule apparatus>). The paddle wheel was engaged to
    the weights through the roller and strings only while the weights
    descended. Each descent took about <math|26> seconds, and the entire
    experiment lasted <math|35> minutes. Joule measured the water temperature
    with a sensitive mercury-in-glass thermometer at both the start and
    finish of the experiment.

    For the purposes of the calculations, define the <em|system> to be the
    combination of the vessel, its contents (including the paddle wheel and
    water), and its lid. All energies are measured in a lab frame. Ignore the
    small quantity of expansion work occurring in the experiment. It helps
    conceptually to think of the cellar room in which Joule set up his
    apparatus as being effectively isolated from the rest of the universe;
    then the only surroundings you need to consider for the calculations are
    the part of the room outside the system.

    <\enumerate-alpha>
      <item>Calculate the change of the gravitational potential energy
      <math|E<rsub|<text|p>>> of the lead weights during each of the
      descents. For the acceleration of free fall at Manchester, England
      (where Joule carried out the experiment) use the value <math|g=9.813
      <text|m>\<cdot\><text|s><rsup|-2>>. This energy change represents a
      decrease in the energy of the surroundings, and would be equal in
      magnitude and opposite in sign to the stirring work done on the system
      if there were no other changes in the surroundings.

      <item>Calculate the kinetic energy <math|E<rsub|<text|k>>> of the
      descending weights just before they reached the floor. This represents
      an increase in the energy of the surroundings. (This energy was
      dissipated into thermal energy in the surroundings when the weights
      came to rest on the floor.)

      <item>Joule found that during each descent of the weights, friction in
      the strings and pulleys decreased the quantity of work performed on the
      system by <math|2.87 <text|J>>. This quantity represents an increase in
      the thermal energy of the surroundings. Joule also considered the
      slight stretching of the strings while the weights were suspended from
      them: when the weights came to rest on the floor, the tension was
      relieved and the potential energy of the strings changed by <math|-1.15
      <text|J>>. Find the total change in the energy of the surroundings
      during the entire experiment from all the effects described to this
      point. Keep in mind that the weights descended <math|20> times during
      the experiment.

      <item>Data in Table <reference|tbl:3-Joule data> show that change of
      the temperature of the system during the experiment was

      <\equation*>
        \<Delta\>*T=<around|(|289.148-288.829|)> <text|K>=+0.319 <text|K>
      </equation*>

      The paddle wheel vessel had no thermal insulation, and the air
      temperature was slighter warmer, so during the experiment there was a
      transfer of some heat into the system. From a correction procedure
      described by Joule, the temperature change that would have occurred if
      the vessel had been insulated is estimated to be <math|+0.317
      <text|K>>.

      Use this information together with your results from part (c) to
      evaluate the work needed to increase the temperature of one gram of
      water by one kelvin. This is the \Pmechanical equivalent of heat\Q at
      the average temperature of the system during the experiment. (As
      mentioned on p. <pageref|Joule's mech equiv heat>, Joule obtained the
      value <math|4.165 <text|J>> based on all <math|40> of his experiments.)
    </enumerate-alpha>
  </problem>

  <float|float|thb|<\big-table|<tabular|<tformat|<cwith|1|-1|1|1|cell-hyphen|t>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|11|11|1|-1|cell-bborder|1ln>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-rborder|0ln>|<cwith|2|-1|2|2|cell-halign|L.>|<cwith|1|-1|2|2|cell-valign|b>|<table|<row|<\cell>
    Properties of the paddle wheel apparatus:
  </cell>|<cell|>>|<row|<\cell>
    <space|1em>combined mass of the two lead weights<fill-dots>
  </cell>|<cell|<math|26.3182 <text|kg>>>>|<row|<\cell>
    <space|1em>mass of water in vessel<fill-dots>
  </cell>|<cell|<math|6.04118 <text|kg>>>>|<row|<\cell>
    <space|1em>mass of water with same heat capacity

    as paddle wheel, vessel, and lid<rsup|<note-inline||+1FqrU5Ax20Ghi8rs>><fill-dots>
  </cell>|<cell|<math|0.27478 <text|kg>>>>|<row|<\cell>
    Measurements during the experiment:
  </cell>|<cell|>>|<row|<\cell>
    <space|1em>number of times weights were wound up and released<fill-dots>
  </cell>|<cell|<math|20>>>|<row|<\cell>
    <space|1em>change of elevation of weights during each descent<fill-dots>
  </cell>|<cell|<math|-1.5898 <text|m>>>>|<row|<\cell>
    <space|1em>final downward velocity of weights during descent<fill-dots>
  </cell>|<cell|<math|0.0615 <text|m>*\<cdot\><text|s><rsup|-1>>>>|<row|<\cell>
    <space|1em>initial temperature in vessel<fill-dots>
  </cell>|<cell|<math|288.829 <text|K>>>>|<row|<\cell>
    <space|1em>final temperature in vessel<fill-dots>
  </cell>|<cell|289.148 K>>|<row|<\cell>
    <space|1em>mean air temperature<fill-dots><repeat||.>
  </cell>|<cell|<math|289.228 <text|K>>>>>>>>
    <label|tbl:3-Joule data>Data for Problem <reference|prb:3-Joule_expt>.
    The values are from Joule's 1850 paper<rsup|<note-inline||+1FqrU5Ax20Ghi8rv>>
    and have been converted to SI units.

    <note-ref|+1FqrU5Ax20Ghi8rv> Ref. <cite|joule-1850>, p. 67, experiment 5

    <note-ref|+1FqrU5Ax20Ghi8rs> Calculated from the masses and specific heat
    capacities of the materials.
  </big-table>>

  <\problem>
    Refer to the apparatus depicted in Fig. <vpageref|fig:3-paddle \ heater>.
    Suppose the mass of the external weight is <math|m=1.50 <text|kg>>, the
    resistance of the electrical resistor is <math|R<rsub|<text|el>>=5.50
    <text|k\<#3A9\>>>, and the acceleration of free fall is <math|g=9.81
    <text|m>*\<cdot\><text|s><rsup|2>>. For how long a period of time will
    the external cell need to operate, providing an electric potential
    difference <math|<around|\||\<Delta\>*\<phi\>|\|>=1.30 <text|V>>, to
    cause the same change in the state of the system as the change when the
    weight sinks <math|20.0 <text|cm>> without electrical work? Assume both
    processes occur adiabatically.
  </problem>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|3>
    <associate|page-first|81>
    <associate|preamble|false>
    <associate|section-nr|10>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|11|?>>
    <associate|auto-2|<tuple|11.1|?>>
    <associate|auto-3|<tuple|11.2|?>>
    <associate|auto-4|<tuple|11.3|?>>
    <associate|auto-5|<tuple|11.4|?>>
    <associate|auto-6|<tuple|11.5|?>>
    <associate|auto-7|<tuple|Joule, James Prescott|?>>
    <associate|auto-8|<tuple|11.1|?>>
    <associate|fig:3-evacuated vessel|<tuple|11.5|?>>
    <associate|fig:3-marble|<tuple|11.1|?>>
    <associate|fig:3-porous plug|<tuple|11.2|?>>
    <associate|fig:3-spont compression|<tuple|11.4|?>>
    <associate|fig:3-vertical cylinder|<tuple|11.3|?>>
    <associate|footnote-11.1|<tuple|11.1|?>>
    <associate|footnote-11.2|<tuple|11.2|?>>
    <associate|footnr-11.1|<tuple|11.1|?>>
    <associate|footnr-11.2|<tuple|11.1|?>>
    <associate|prb:3-Joule_expt|<tuple|11.10|?>>
    <associate|prb:3-evacuated vessel|<tuple|11.8|?>>
    <associate|prb:3-porous plug|<tuple|11.3|?>>
    <associate|tbl:3-Joule data|<tuple|11.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      joule-1850
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.1>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.2>|>
        \;
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.3>|>
        \;
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.4>|>
        \;
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.5>|>
        \;
      </surround>|<pageref|auto-6>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Joule, James Prescott>|<pageref|auto-7>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.1>|>
        Data for Problem <reference|prb:3-Joule_expt>. The values are from
        Joule's 1850 paper<rsup|<surround|<assign|footnote-nr|2><hidden-binding|<tuple>|11.2><assign|fnote-+1FqrU5Ax20Ghi8rv|<quote|11.2>><assign|fnlab-+1FqrU5Ax20Ghi8rv|<quote|11.2>>||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-40A320298--401164958>|<link|hyperlink|<id|%-40A320298--401164958>|<url|#footnr-11.2>>|11.2>.
        |<hidden-binding|<tuple|footnote-11.2>|11.2>|>>>> and have been
        converted to SI units.

        <space|0spc><rsup|<with|font-shape|<quote|right>|<reference|footnote-11.2>>>
        Ref. [<write|bib|joule-1850><reference|bib-joule-1850>], p. 67,
        experiment 5

        <space|0spc><rsup|<with|font-shape|<quote|right>|<reference|footnote-11.1>>>
        Calculated from the masses and specific heat capacities of the
        materials.
      </surround>|<pageref|auto-8>>
    </associate>
    <\associate|toc>
      11<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>