<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <no-indent>BIOGRAPHICAL SKETCH

  <paragraph|<person|Gilbert Newton Lewis> (1875\U1946)>

  <\padded-center>
    <tabular|<tformat|<cwith|1|1|2|2|cell-hyphen|t>|<table|<row|<cell|<image|BIO/lewis.png|82pt|115pt||>>|<\cell>
      <\rotate|90>
        <\really-tiny>
          <\with|color|#a0a0a0>
            <\text>
              EDGAR FAHS SMITH COLLECTION

              UNIVERSITY OF PENNSYLVANIA LIBRARY
            </text>
          </with>

          \;
        </really-tiny>

        \;
      </rotate>
    </cell>>>>>
  </padded-center>

  <label|bio:lewis><index|Lewis, Gilbert Newton><no-indent>Gilbert Lewis made
  major contributions to several fields of physical chemistry. He was born in
  Weymouth, Massachusetts. His father was a lawyer and banker.

  Lewis was reserved, even shy in front of a large audience. He was also
  ambitious, had great personal charm, excelled at both experimental and
  theoretical thermodynamics, and was a chain smoker of vile Manila cigars.

  Lewis considered himself to be a disciple of Willard Gibbs. After
  completing his Ph.D dissertation at Harvard University in 1899, he
  published several papers of thermodynamic theory that introduced for the
  first time the terms fugacity (1901) and activity (1907). The first of
  these papers was entitled \PA New Conception of Thermal Pressure and a
  Theory of Solutions\Q and began:<footnote|Ref. <cite|lewis-00>.>

  <quotation|For an understanding of all kinds of physico-chemical
  equilibrium a further insight is necessary into the nature of the
  conditions which exist in the interior of any homogeneous phase. It will be
  the aim of the present paper to study this problem in the light of a new
  theory, which, although opposed to some ideas which are now accepted as
  correct, yet recommends itself by its simplicity and by its ability to
  explain several important phenomena which have hitherto received no
  satisfactory explanation.>

  His first faculty position (1905-1912) was at Boston Tech, now the
  Massachusetts Institute of Technology, where he continued work in one of
  his dissertation subjects: the measurement of standard electrode potentials
  in order to determine standard molar Gibbs energies of formation of
  substances and ions.

  In 1912 he became the chair of the chemistry department at the University
  of California at Berkeley, which he turned into a renowned center of
  chemical research and teaching. In 1916 he published his theory of the
  shared electron-pair chemical bond (Lewis structures), a concept he had
  been thinking about since at least 1902. In the course of measuring the
  thermodynamic properties of electrolyte solutions, he introduced the
  concept of ionic strength (1921).

  In 1923, at age 48, he consolidated his knowledge of thermodynamics in the
  great classic <em|Thermodynamics and the Free Energy of Chemical
  Substances><footnote|Ref. <cite|lewis-23>.> with Merle Randall as coauthor.
  After that his interests changed to other subjects. He was the first to
  prepare pure deuterium and D<rsub|<math|2>>O (1933), he formulated his
  generalized definitions of acids and bases (Lewis acids and bases, 1938),
  and at the time of his death he was doing research on photochemical
  processes.

  Lewis was nominated 35 times for the Nobel prize, but was never awarded it.
  According to a history of modern chemistry published in 2008,<footnote|Ref.
  <cite|coffey-08>, Chap. 7.> Wilhelm Palmaer, a Swedish electrochemist, used
  his position on the Nobel Committee for Chemistry to block the award to
  Lewis. Palmaer was a close friend of Walther Nernst, whom Lewis had
  criticized on the basis of occasional \Parithmetic and thermodynamic
  inaccuracy\Q.<footnote|Ref. <cite|lewis-23>, page 6.>

  His career was summarized by his last graduate student, Michael Kasha, as
  follows:<footnote|Ref. <cite|kasha-84>.>

  <quotation|Gilbert Lewis once defined physical chemistry as encompassing
  \Peverything that is interesting\Q. His own career touched virtually every
  aspect of science, and in each he left his mark. He is justly regarded as
  one of the key scientists in American history. It would be a great omission
  not to record the warmth and intellectual curiosity radiated by Lewis'
  personality. He epitomized the scientist of unlimited imagination, and the
  joy of working with him was to experience the life of the mind unhindered
  by pedestrian concerns.>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|9>
    <associate|font-base-size|9>
    <associate|page-first|217>
    <associate|page-medium|paper>
    <associate|par-columns|2>
    <associate|par-columns-sep|1fn>
    <associate|preamble|false>
    <associate|section-nr|6>
    <associate|subsection-nr|4>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|217>>
    <associate|auto-2|<tuple|Lewis, Gilbert Newton|217>>
    <associate|bio:lewis|<tuple|1|217>>
    <associate|footnote-1|<tuple|1|217>>
    <associate|footnote-2|<tuple|2|217>>
    <associate|footnote-3|<tuple|3|217>>
    <associate|footnote-4|<tuple|4|217>>
    <associate|footnote-5|<tuple|5|217>>
    <associate|footnr-1|<tuple|1|217>>
    <associate|footnr-2|<tuple|2|217>>
    <associate|footnr-3|<tuple|3|217>>
    <associate|footnr-4|<tuple|4|217>>
    <associate|footnr-5|<tuple|5|217>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      lewis-00

      lewis-23

      coffey-08

      lewis-23

      kasha-84
    </associate>
    <\associate|idx>
      <tuple|<tuple|Lewis, Gilbert Newton>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <with|par-left|<quote|4tab>|Biographical
      Sketch<next-line><with|font-shape|<quote|small-caps>|Gilbert Newton
      Lewis> (1875\U1946) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>