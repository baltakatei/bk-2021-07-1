<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|Problems>

  <\problem>
    Use values of <math|\<Delta\><rsub|<text|f>>*H<st>> and
    <math|\<Delta\><rsub|<text|f>>*G<st>> in Appendix <reference|app:props>
    to evaluate the standard molar reaction enthalpy and the thermodynamic
    equilibrium constant at <math|298.15<K>> for the oxidation of nitrogen to
    form aqueous nitric acid:

    <\equation*>
      <frac|1|2>*<text|N<rsub|2>><around|(|g|)>+<frac|5|4>*<text|O<rsub|2>><around|(|g|)>+<frac|1|2>*<text|H<rsub|2>O><around|(|l|)><arrow><chem|<text|H>|+>*<around|(|a*q|)>+<chem|<text|NO<rsub|3>>|-><around|(|a*q|)>
    </equation*>
  </problem>

  <\problem>
    In 1982, the International Union of Pure and Applied Chemistry
    recommended that the value of the <subindex|Standard|pressure><subindex|Pressure|standard>standard
    pressure <math|p<st>> be changed from <math|1 <text|atm>> to
    <math|1<br>>. This change affects the values of some standard molar
    quantities of a substance calculated from experimental data.

    <\enumerate-alpha>
      <item>Find the changes in <math|H<m><st>>, <math|S<m><st>>, and
      <math|G<m><st>> for a gaseous substance when the standard pressure is
      changed isothermally from <math|1.01325<br>> (<math|1 <text|atm>>) to
      exactly <math|1<br>>. (Such a small pressure change has an entirely
      negligible effect on these quantities for a substance in a condensed
      phase.)

      <item>What are the values of the corrections that need to be made to
      the standard molar enthalpy of formation, the standard molar entropy of
      formation, and the standard molar Gibbs energy of formation of
      N<rsub|<math|2>>O<rsub|<math|4>>(g) at <math|298.15<K>> when the
      standard pressure is changed from <math|1.01325<br>> to <math|1<br>>?
    </enumerate-alpha>
  </problem>

  <\problem>
    From data for mercury listed in Appendix <reference|app:props>, calculate
    the saturation vapor pressure of liquid mercury at both <math|298.15<K>>
    and <math|273.15<K>>. You may need to make some reasonable
    approximations.
  </problem>

  <\problem>
    Given the following experimental values at <math|T=298.15<K>>,
    <math|p=1<br>>:

    <\eqnarray*>
      <tformat|<table|<row|<cell|<chem|<text|H>|+><around*|(|<text|aq>|)>+<chem|<text|OH>|-><around*|(|<text|aq>|)><arrow><text|H<rsub|2>O><around*|(|<text|l>|)>>|<cell|<text|<space|2em>>>|<cell|\<Delta\><rsub|<text|r>>*H<st>=-55.82
      <text|kJ>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|<text|Na><rsup|+><around*|(|<text|s>|)>+<text|H<rsub|2>O><around*|(|<text|l>|)><arrow><chem|<text|Na>|+><around*|(|<text|aq>|)>+<chem|<text|OH>|-><around*|(|<text|aq>|)>+<tfrac|1|2>*<text|H<rsub|2>><around*|(|<text|g>|)>>|<cell|>|<cell|\<Delta\><rsub|<text|r>>*H<st>=-184.52
      <text|kJ>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|<text|NaOH><around*|(|<text|s>|)><arrow><text|NaOH><around*|(|<text|aq>|)>>|<cell|>|<cell|\<Delta\><rsub|<text|sol>>H<rsup|\<infty\>>=-44.75
      <text|kJ>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|<text|NaOH> <text|in>
      5*<text|H<rsub|2>O><arrow><text|NaOH> in \<infty\>
      <text|H<rsub|2>O>>|<cell|>|<cell|\<Delta\>*H<rsub|<text|m>><around*|(|<text|dil>|)>=-4.93
      <text|kJ>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|<text|NaOH><around*|(|<text|s>|)>>|<cell|>|<cell|\<Delta\><rsub|<text|f>>*H<st>=-425.61
      <text|kJ>\<cdot\><text|mol><rsup|-1>>>>>
    </eqnarray*>

    Using only these values, calculate:

    <\enumerate-alpha>
      <item><math|\<Delta\><rsub|<text|f>>*H<st>> for Na<rsup|<math|+>>(aq),
      NaOH(aq), and OH<rsup|<math|->>(aq);

      <item><math|\<Delta\><rsub|<text|f>>*H> for NaOH in 5
      H<rsub|<math|2>>O;

      <item><math|<Del>H<m><sol>> for the dissolution of <math|1<mol>>
      NaOH(s) in <math|5 <mol>> H<rsub|<math|2>>O.
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:11-Na2SO3 solns>Table <vpageref|tbl:11-Na2SO3 solns> lists
    data for water, crystalline sodium thiosulfate pentahydrate, and several
    sodium thiosulfate solutions. Find <math|<Del>H> to the nearest
    <math|0.01 <text|kJ>> for the dissolution of <math|5.00 <text|g>> of
    crystalline <math|<text|Na<rsub|2>S<rsub|2>O<rsub|3>>\<cdot\>5*<text|H<rsub|2>O><around*|(|<text|s>|)>>
    in <math|50.0 <text|g>> of water at <math|298.15<K>> and <math|1<br>>.
  </problem>

  <float|float|thb|<\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|7|7|1|-1|cell-bborder|1ln>|<cwith|2|-1|2|-1|cell-halign|C.>|<table|<row|<cell|Substance>|<cell|<math|\<Delta\><rsub|<text|f>>*H/<around*|(|<text|kJ>\<cdot\><text|mol><rsup|-1>|)>>>|<cell|<math|M/<around*|(|<text|g>\<cdot\><text|mol><rsup|-1>|)>><space|-0.17em><space|-0.17em><space|-0.17em><space|-0.17em><space|-0.17em>>>|<row|<cell|<math|<text|H<rsub|2>O><around*|(|<text|l>|)>>>|<cell|-285.830>|<cell|18.0153>>|<row|<cell|<math|<text|Na<rsub|2>S<rsub|2>O<rsub|3>>\<cdot\>5*<text|H<rsub|2>O><around*|(|<text|s>|)>>>|<cell|-2607.93>|<cell|248.1828>>|<row|<cell|<math|<text|Na<rsub|2>S<rsub|2>O<rsub|3>>
  <text|in> 50*<text|H<rsub|2>O>>>|<cell|-1135.914>|<cell|>>|<row|<cell|<math|<text|Na<rsub|2>S<rsub|2>O<rsub|3>>
  <text|in> 100*<text|H<rsub|2>O>>>|<cell|-1133.822>|<cell|>>|<row|<cell|<math|<text|Na<rsub|2>S<rsub|2>O<rsub|3>>
  <text|in> 200*<text|H<rsub|2>O>>>|<cell|-1132.236>|<cell|>>|<row|<cell|<math|<text|Na<rsub|2>S<rsub|2>O<rsub|3>>
  <text|in> 300*<text|H<rsub|2>O>>>|<cell|-1131.780>|<cell|>>>>>>
    <label|tbl:11-Na2SO3 solns>Data for Problem <reference|prb:11-Na2SO3
    solns>.<note-ref|+2TniXBwbTdAadwK>

    \;

    <note-inline||+2TniXBwbTdAadwK>Ref. <cite|wagman-82>, pages 2-307 and
    2-308.
  </big-table>>

  <\problem>
    <label|prb:11-HCl diln>Use the experimental data in Table
    <vpageref|tbl:11-HCl prob> to evaluate <math|L<A>> and <math|L<B>> at
    <math|25 <degC>> for an aqueous HCl solution of molality
    <math|m<B>=0.0900 <text|mol>\<cdot\><text|kg><rsup|-1>>.
  </problem>

  <float|float|thb|<\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|12|12|1|-1|cell-bborder|1ln>|<cwith|2|-1|1|-1|cell-halign|L.>|<table|<row|<cell|<math|m<rprime|''><B>/<around*|(|<text|mol>\<cdot\><text|kg><rsup|-1>|)>>>|<cell|<math|<Del>H<m><around|(|<text|dil>,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>|)>/<around*|(|<text|kJ>\<cdot\><text|mol><rsup|-1>|)>>>>|<row|<cell|<math|0.295>>|<cell|<math|-2.883>>>|<row|<cell|<math|0.225>>|<cell|<math|-2.999>>>|<row|<cell|<math|0.199>>|<cell|<math|-3.041>>>|<row|<cell|<math|0.147>>|<cell|<math|-3.143>>>|<row|<cell|<math|0.113>>|<cell|<math|-3.217>>>|<row|<cell|<math|0.0716>>|<cell|<math|-3.325>>>|<row|<cell|<math|0.0544>>|<cell|<math|-3.381>>>|<row|<cell|<math|0.0497>>|<cell|<math|-3.412>>>|<row|<cell|<math|0.0368>>|<cell|<math|-3.466>>>|<row|<cell|<math|0.0179>>|<cell|<math|-3.574>>>|<row|<cell|<math|0.0128>>|<cell|<math|-3.621>>>>>>>
    <label|tbl:11-HCl prob>Data for Problem <reference|prb:11-HCl diln>.
    Molar integral enthalpies of dilution of aqueous HCl
    (<math|m<rprime|'><B>=3.337 <text|mol>\<cdot\><text|kg><rsup|-1>>) at
    <math|25 <degC>>.<note-ref|+2TniXBwbTdAadwL>

    \;

    <note-inline|Ref. <cite|sturtevant-40a>|+2TniXBwbTdAadwL>
  </big-table>>

  <\problem>
    <label|prb:11-hexane combustion>This 16-part problem illustrates the use
    of experimental data from <index|Bomb
    calorimetry><subindex|Calorimetry|bomb>bomb calorimetry and other
    sources, combined with thermodynamic relations derived in this and
    earlier chapters, to evaluate the standard molar combustion enthalpy of a
    liquid hydrocarbon. The substance under investigation is <em|n>-hexane,
    and the combustion reaction in the bomb vessel is

    <\equation*>
      <text|C<rsub|6>H<rsub|14>><around|(|<text|l>|)>+<tfrac|19|2>*<text|O<rsub|2>><around|(|g|)><arrow>6*<text|CO<rsub|2>><around|(|<text|g>|)>+7*<text|H<rsub|2>O><around|(|<text|l>|)>
    </equation*>

    Assume that the sample is placed in a glass ampoule that shatters at
    ignition. Data needed for this problem are collected in Table
    <vpageref|tbl:11-hexane combustion>.

    States 1 and 2 referred to in this problem are the initial and final
    states of the isothermal bomb process. The temperature is the reference
    temperature of <math|298.15<K>>.

    <\enumerate-alpha>
      <item><label|amounts> Parts <reference|amounts>\U<reference|volumes>
      consist of simple calculations of some quantities needed in later parts
      of the problem. Begin by using the masses of
      C<rsub|<math|6>>H<rsub|<math|14>> and H<rsub|<math|2>>O placed in the
      bomb vessel, and their molar masses, to calculate the amounts (moles)
      of C<rsub|<math|6>>H<rsub|<math|14>> and H<rsub|<math|2>>O present
      initially in the bomb vessel. Then use the stoichiometry of the
      combustion reaction to find the amount of O<rsub|<math|2>> consumed and
      the amounts of H<rsub|<math|2>>O and CO<rsub|<math|2>> present in state
      2. (There is not enough information at this stage to allow you to find
      the amount of O<rsub|<math|2>> present, just the change.) Also find the
      final mass of H<rsub|<math|2>>O. Assume that oxygen is present in
      excess and the combustion reaction goes to completion.

      <item><label|molar volumes>From the molar masses and the densities of
      liquid C<rsub|<math|6>>H<rsub|<math|14>> and H<rsub|<math|2>>O,
      calculate their molar volumes.

      <item><label|volumes>From the amounts present initially in the bomb
      vessel and the internal volume, find the volumes of liquid
      C<rsub|<math|6>>H<rsub|<math|14>>, liquid H<rsub|<math|2>>O, and gas in
      state 1 and the volumes of liquid H<rsub|<math|2>>O and gas in state 2.
      For this calculation, you can neglect the small change in the volume of
      liquid H<rsub|<math|2>>O due to its vaporization.

      <item><label|O2 amounts>When the bomb vessel is charged with oxygen and
      before the inlet valve is closed, the pressure at <math|298.15<K>>
      measured on an external gauge is found to be
      <math|p<rsub|1>=30.00<br>>. To a good approximation, the gas phase of
      state 1 has the equation of state of pure O<rsub|<math|2>> (since the
      vapor pressure of water is only <math|0.1 <text|%>> of
      <math|30.00<br>>). Assume that this equation of state is given by
      <math|V<m>=R*T/p+B<rsub|<text|B>\<nocomma\><text|B>>> (Eq.
      <reference|Vm=RT/p+B>), where <math|B<rsub|<text|B>\<nocomma\><text|B>>>
      is the second virial coefficient of O<rsub|<math|2>> listed in Table
      <reference|tbl:11-hexane combustion>. Solve for the amount of
      O<rsub|<math|2>> in the gas phase of state 1. The gas phase of state 2
      is a mixture of O<rsub|<math|2>> and CO<rsub|<math|2>>, again with a
      negligible partial pressure of H<rsub|<math|2>>O. Assume that only
      small fractions of the total amounts of O<rsub|<math|2>> and
      CO<rsub|<math|2>> dissolve in the liquid water, and find the amount of
      O<rsub|<math|2>> in the gas phase of state 2 and the mole fractions of
      O<rsub|<math|2>> and CO<rsub|<math|2>> in this phase.

      <item><label|final p>You now have the information needed to find the
      pressure in state 2, which cannot be measured directly. For the mixture
      of O<rsub|<math|2>> and CO<rsub|<math|2>> in the gas phase of state 2,
      use Eq. <vpageref|B=yA^2 B(AA)+...> to calculate the second virial
      coefficient. Then solve the <index-complex|<tuple|equation of state|gas
      at low pressure>|||<tuple|Equation of state|of a gas at low
      pressure>>equation of state of Eq. <vpageref|V=nRT/p+nB> for the
      pressure. Also calculate the partial pressures of the O<rsub|<math|2>>
      and CO<rsub|<math|2>> in the gas mixture.

      <item><label|H2O fug>Although the amounts of H<rsub|<math|2>>O in the
      gas phases of states 1 and 2 are small, you need to know their values
      in order to take the energy of vaporization into account. In this part,
      you calculate the fugacities of the H<rsub|<math|2>>O in the initial
      and final gas phases, in part <reference|fug coeffs> you use gas
      equations of state to evaluate the fugacity coefficients of the
      H<rsub|<math|2>>O (as well as of the O<rsub|<math|2>> and
      CO<rsub|<math|2>>), and then in part <reference|H2O amts> you find the
      amounts of H<rsub|<math|2>>O in the initial and final gas phases.

      The pressure at which the pure liquid and gas phases of
      H<rsub|<math|2>>O are in equilibrium at <math|298.15<K>> (the
      saturation vapor pressure of water) is <math|0.03169<br>>. Use Eq.
      <vpageref|ln(phi)=Bp/RT> to estimate the fugacity of
      H<rsub|<math|2>>O(g) in equilibrium with pure liquid water at this
      temperature and pressure. The effect of pressure on fugacity in a
      one-component liquid--gas system is discussed in Sec.
      <reference|12-effect of p on fug>; use Eq. <vpageref|f_i(p2) approx> to
      find the fugacity of H<rsub|<math|2>>O in gas phases equilibrated with
      liquid water at the pressures of states 1 and 2 of the isothermal bomb
      process. (The mole fraction of O<rsub|<math|2>> dissolved in the liquid
      water is so small that you can ignore its effect on the chemical
      potential of the water.)

      <item><label|fug coeffs>Calculate the fugacity coefficients of
      H<rsub|<math|2>>O and O<rsub|<math|2>> in the gas phase of state 1 and
      of H<rsub|<math|2>>O, O<rsub|<math|2>>, and CO<rsub|<math|2>> in the
      gas phase of state 2.

      For state 1, in which the gas phase is practically-pure
      O<rsub|<math|2>>, you can use Eq. <vpageref|ln(phi)=Bp/RT> to calculate
      <math|\<phi\><rsub|<text|O<rsub|2>>>>. The other calculations require
      Eq. <vpageref|ln(phi_i)=Bi'p/RT>, with the value of
      <math|B<rsub|i><rprime|'>> found from the formulas of Eq.
      <reference|Bi'=2 sum yj Bij-B> or Eqs. <reference|B(A)'=> and
      <reference|B(B)'=> (<math|y<A>> is so small that you can set it equal
      to zero in these formulas).

      Use the fugacity coefficient and partial pressure of O<rsub|<math|2>>
      to evaluate its fugacity in states 1 and 2; likewise, find the fugacity
      of CO<rsub|<math|2>> in state 2. [You calculated the fugacity of the
      H<rsub|<math|2>>O in part <reference|H2O fug>.]

      <item><label|H2O amts>From the values of the fugacity and fugacity
      coefficient of a constituent of a gas mixture, you can calculate the
      partial pressure with Eq. <vpageref|f_i=phi_i*p_i>, then the mole
      fraction with <math|y<rsub|i>=p<rsub|i>/p>, and finally the amount with
      <math|n<rsub|i>=y<rsub|i>*n>. Use this method to find the amounts of
      H<rsub|<math|2>>O in the gas phases of states 1 and 2, and also
      calculate the amounts of H<rsub|<math|2>>O in the liquid phases of both
      states.

      <item><label|g sol>Next, consider the O<rsub|<math|2>> dissolved in the
      water of state 1 and the O<rsub|<math|2>> and CO<rsub|<math|2>>
      dissolved in the water of state 2. Treat the solutions of these gases
      as ideal dilute with the molality of solute <math|i> given by
      <math|m<rsub|i>=<fug><rsub|i>/k<rsub|m,i>> (Eq.
      <reference|KmB=lim(fB/mB>). The values of the Henry's law constants of
      these gases listed in Table <reference|tbl:11-hexane combustion> are
      for the standard pressure of <math|1<br>>. Use Eq. <vpageref|KmB(p2)
      approx> to find the appropriate values of <math|k<rsub|m,i>> at the
      pressures of states 1 and 2, and use these values to calculate the
      amounts of the dissolved gases in both states.

      <item><label|H2O vap>At this point in the calculations, you know the
      values of all properties needed to describe the initial and final
      states of the isothermal bomb process. You are now able to evaluate the
      various <index|Washburn corrections>Washburn corrections. These
      corrections are the internal energy changes, at the reference
      temperature of <math|298.15<K>>, of processes that connect the standard
      states of substances with either state 1 or state 2 of the isothermal
      bomb process.

      First, consider the gaseous H<rsub|<math|2>>O. The Washburn corrections
      should be based on a pure-liquid standard state for the
      H<rsub|<math|2>>O. Section <reference|7-st molar fncs of a gas> shows
      that the molar internal energy of a pure gas under ideal-gas conditions
      (low pressure) is the same as the molar internal energy of the gas in
      its standard state at the same temperature. Thus, the molar internal
      energy change when a substance in its pure-liquid standard state
      changes isothermally to an ideal gas is equal to the standard molar
      internal energy of vaporization, <math|\<Delta\><rsub|<text|vap>>*U<st>>.
      Using the value of <math|\<Delta\><rsub|<text|vap>>*U<st>> for
      H<rsub|<math|2>>O given in Table <reference|tbl:11-hexane combustion>,
      calculate <math|<Del>U> for the vaporization of liquid
      H<rsub|<math|2>>O at pressure <math|p<st>> to ideal gas in the amount
      present in the gas phase of state 1. Also calculate <math|<Del>U> for
      the condensation of ideal gaseous H<rsub|<math|2>>O in the amount
      present in the gas phase of state 2 to liquid at pressure <math|p<st>>.

      <item><label|gas sol delU>Next, consider the dissolved O<rsub|<math|2>>
      and CO<rsub|<math|2>>, for which gas standard states are used. Assume
      that the solutions are sufficiently dilute to have infinite-dilution
      behavior; then the partial molar internal energy of either solute in
      the solution at the standard pressure <math|p<st>=1<br>> is equal to
      the standard partial molar internal energy based on a solute standard
      state (Sec. <reference|9-mixt st states>). Values of
      <math|\<Delta\><rsub|<text|sol>>*U<st>> are listed in Table
      <reference|tbl:11-hexane combustion>. Find <math|<Del>U> for the
      dissolution of O<rsub|<math|2>> from its gas standard state to
      ideal-dilute solution at pressure <math|p<st>> in the amount present in
      the aqueous phase of state 1. Find <math|<Del>U> for the desolution
      (transfer from solution to gas phase) of O<rsub|<math|2>> and of
      CO<rsub|<math|2>> from ideal-dilute solution at pressure <math|p<st>>,
      in the amounts present in the aqueous phase of state 2, to their gas
      standard states.

      <item><label|l delU>Calculate the internal energy changes when the
      liquid phases of state 1 (<em|n>-hexane and aqueous solution) are
      compressed from <math|p<st>> to <math|p<rsub|1>> and the aqueous
      solution of state 2 is decompressed from <math|p<rsub|2>> to
      <math|p<st>>. Use an approximate expression from Table
      <reference|tbl:7-isothermal p change>, and treat the cubic expansion
      coefficient of the aqueous solutions as being the same as that of pure
      water.

      <item><label|gas delU>The final Washburn corrections are internal
      energy changes of the gas phases of states 1 and 2. H<rsub|<math|2>>O
      has such low mole fractions in these phases that you can ignore
      H<rsub|<math|2>>O in these calculations; that is, treat the gas phase
      of state 1 as pure O<rsub|<math|2>> and the gas phase of state 2 as a
      binary mixture of O<rsub|<math|2>> and CO<rsub|<math|2>>.

      One of the internal energy changes is for the compression of gaseous
      O<rsub|<math|2>>, starting at a pressure low enough for ideal-gas
      behavior (<math|U<m>=U<m><st>>) and ending at pressure <math|p<rsub|1>>
      to form the gas phase present in state 1. Use the approximate
      expression for <math|U<m>-U<m><st><gas>> in Table <reference|tbl:7-gas
      standard molar> to calculate <math|<Del>U=U<around|(|p<rsub|1>|)>-n*U<m><st><gas>>;
      a value of <math|<dif>B/<dif>T> for pure O<rsub|<math|2>> is listed in
      Table <reference|tbl:11-hexane combustion>.

      The other internal energy change is for a process in which the gas
      phase of state 2 at pressure <math|p<rsub|2>> is expanded until the
      pressure is low enough for the gas to behave ideally, and the mixture
      is then separated into ideal-gas phases of pure O<rsub|<math|2>> and
      CO<rsub|<math|2>>. The molar internal energies of the separated
      low-pressure O<rsub|<math|2>> and CO<rsub|<math|2>> gases are the same
      as the standard molar internal energies of these gases. The internal
      energy of unmixing ideal gases is zero (Eq.
      <reference|del(mix)Um(id)=0>). The dependence of the internal energy of
      the gas mixture is given, to a good approximation, by
      <math|U=<big|sum><rsub|i>U<rsub|i><st><gas>-n*p*T<dif>B/<dif>T>, where
      <math|B> is the second virial coefficient of the gas mixture; this
      expression is the analogy for a gas mixture of the approximate
      expression for <math|U<m>-U<m><st><gas>> in Table <reference|tbl:7-gas
      standard molar>. Calculate the value of <math|<dif>B/<dif>T> for the
      mixture of O<rsub|<math|2>> and CO<rsub|<math|2>> in state 2 (you need
      Eq. <vpageref|B=yA^2 B(AA)+...> and the values of
      <math|<dif>B<rsub|i*j>/<dif>T> in Table <reference|tbl:11-hexane
      combustion>) and evaluate <math|<Del>U=<big|sum><rsub|i>n<rsub|i>*U<rsub|i><st><gas>-U<around|(|p<rsub|2>|)>>
      for the gas expansion.

      <item><label|del U>Add the internal energy changes you calculated in
      parts <reference|H2O vap>--<reference|gas delU> to find the total
      internal energy change of the Washburn corrections. Note that most of
      the corrections occur in pairs of opposite sign and almost completely
      cancel one another. Which contributions are the greatest in magnitude?

      <item>The internal energy change of the isothermal bomb process in the
      bomb vessel, corrected to the reference temperature of
      <math|298.15<K>>, is found to be <math|<Del>U<around|(|<text|IBP>,T<rsub|<text|ref>>|)>=-32.504
      <text|kJ>>. Assume there are no side reactions or auxiliary reactions.
      From Eqs. <reference|DelUo=DelU(IBP)+...> and
      <reference|DelUom=DelUo/xi>, calculate the standard molar internal
      energy of combustion of <em|n>-hexane at <math|298.15<K>>.

      <item><label|del(c)Hmo(hexane)>From Eq. <reference|DelHom=DelUom+sum
      nu_i(g)RT>, calculate the standard molar enthalpy of combustion of
      <em|n>-hexane at <math|298.15<K>>.
    </enumerate-alpha>
  </problem>

  <float|float|thb|<\big-table|<tabular|<tformat|<cwith|1|-1|1|1|cell-hyphen|t>|<cwith|1|-1|2|2|cell-valign|b>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|32|32|1|-1|cell-bborder|1ln>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-rborder|0ln>|<cwith|2|-1|2|2|cell-halign|l>|<cwith|28|29|1|1|cell-hyphen|t>|<cwith|28|29|1|1|cell-lborder|0ln>|<cwith|31|32|1|1|cell-hyphen|t>|<cwith|31|32|1|1|cell-lborder|0ln>|<table|<row|<\cell>
    Properties of the bomb vessel:
  </cell>|<cell|>>|<row|<\cell>
    <space|1em>internal volume<fill-dots>
  </cell>|<cell|<math|350.0 <text|cm><rsup|3>>>>|<row|<\cell>
    <space|1em>mass of <em|n>-hexane placed in bomb<fill-dots>
  </cell>|<cell|<math|0.6741 <text|g>>>>|<row|<\cell>
    <space|1em>mass of water placed in bomb<fill-dots>
  </cell>|<cell|<math|1.0016 <text|g>>>>|<row|<\cell>
    Properties of liquid <em|n>-hexane:
  </cell>|<cell|>>|<row|<\cell>
    <space|1em>molar mass<fill-dots>
  </cell>|<cell|<math|M=86.177 <text|g>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em>density<fill-dots>
  </cell>|<cell|<math|\<rho\>=0.6548 <text|g>\<cdot\><text|cm><rsup|-3>>>>|<row|<\cell>
    <space|1em>cubic expansion coefficient<fill-dots>
  </cell>|<cell|<math|\<alpha\>=1.378<timesten|-3>
  <text|K><rsup|-1>>>>|<row|<\cell>
    Properties of liquid H<rsub|2>O:
  </cell>|<cell|>>|<row|<\cell>
    <space|1em>molar mass<fill-dots>
  </cell>|<cell|<math|M=18.0153 <text|g>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em>density<fill-dots>
  </cell>|<cell|<math|\<rho\>=0.9970 <text|g>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em>cubic expansion coefficient<fill-dots>
  </cell>|<cell|<math|\<alpha\>=2.59<timesten|-4>
  <text|K><rsup|-1>>>>|<row|<\cell>
    <space|1em>standard molar energy of vaporization<fill-dots>
  </cell>|<cell|<math|\<Delta\><rsub|<text|vap>>*U<st>=41.53
  <text|kJ>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    Second virial coefficients, <math|298.15 <text|K>>
  </cell>|<cell|>>|<row|<\cell>
    <space|1em><math|B<rsub|<text|A>\<nocomma\><text|A>>><fill-dots>
  </cell>|<cell|<math|-1158 cm<rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em><math|B<rsub|<text|B>\<nocomma\><text|B>>><fill-dots>
  </cell>|<cell|<math|-16 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em><dvar|B<rsub|<text|B>\<nocomma\><text|B>>/<dvar|T>><fill-dots>
  </cell>|<cell|<math|0.21 <text|cm><rsup|3>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em><math|B<rsub|<text|C>\<nocomma\>\<nocomma\><text|C>>><fill-dots>
  </cell>|<cell|<math|-127 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em><dvar|B<rsub|<text|C>\<nocomma\><text|C>>/<dvar|T>><fill-dots>
  </cell>|<cell|<math|0.97 <text|cm><rsup|3>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em><math|B<rsub|<text|A>\<nocomma\><text|B>>><fill-dots>
  </cell>|<cell|<math|-40 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em><math|B<rsub|<text|A>\<nocomma\><text|C>>><fill-dots>
  </cell>|<cell|<math|-214 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em><math|B<rsub|<text|B>\<nocomma\><text|C>>><fill-dots>
  </cell>|<cell|<math|-43.7 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em><dvar|B<rsub|<text|B>\<nocomma\><text|C>>/<dvar|T>><fill-dots>
  </cell>|<cell|<math|0.4 <text|cm><rsup|3>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    Henry's law constants at <math|1 <text|bar>
    <around*|(|<text|solvent>=<text|H<rsub|2>O>|)>>:
  </cell>|<cell|>>|<row|<\cell>
    <space|1em>O<rsub|2><fill-dots>
  </cell>|<cell|<math|k<rsub|m,<text|B>>=796
  <text|bar>\<cdot\><text|kg>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em>CO<rsub|2><fill-dots>
  </cell>|<cell|<math|k<rsub|m,<text|C>>=29.7
  <text|bar>\<cdot\><text|kg>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    Partial molar volumes of solutes in water
  </cell>|<cell|>>|<row|<\cell>
    <space|1em>O<rsub|2><fill-dots>
  </cell>|<cell|<math|V<B><rsup|\<infty\>>=31
  <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em>CO<rsub|2><fill-dots>
  </cell>|<cell|<math|V<rsub|<text|C>><rsup|\<infty\>>=33
  <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    Standard molar energies of solution <math|<around*|(|<text|solvent>=<text|H<rsub|2>O>|)>>:
  </cell>|<cell|>>|<row|<\cell>
    <space|1em>O<rsub|2><fill-dots>
  </cell>|<cell|<math|\<Delta\><rsub|<text|sol>>*U<st>=-9.7
  <text|kJ>\<cdot\><text|mol><rsup|-1>>>>|<row|<\cell>
    <space|1em>CO<rsub|2><fill-dots>
  </cell>|<cell|<math|\<Delta\><rsub|<text|sol>>*U<st>=-17.3
  <text|kJ>\<cdot\><text|mol><rsup|-1>>>>>>>>
    <label|tbl:11-hexane combustion>Data for Problem <reference|prb:11-hexane
    combustion>. The values of intensive properties are for a temperature of
    <math|298.15<K>> and a pressure of <math|30<br>> unless otherwise stated.
    Subscripts: A = H<rsub|<math|2>>O, B = O<rsub|<math|2>>, C =
    CO<rsub|<math|2>>.
  </big-table>>

  <\problem>
    By combining the results of Prob. <reference|prb:11-hexane
    combustion><around|(|<reference|del(c)Hmo(hexane)>|)> with the values of
    standard molar enthalpies of formation from Appendix
    <reference|app:props>, calculate the standard molar enthalpy of formation
    of liquid <em|n>-hexane at <math|298.15<K>>.
  </problem>

  <\problem>
    <label|prb:11-flame>Consider the combustion of methane:

    <\equation*>
      <text|CH<rsub|4>><around*|(|<text|g>|)>+2*<text|O<rsub|2>><around*|(|<text|g>|)><arrow><text|CO<rsub|2>><around*|(|<text|g>|)>+2*<text|H<rsub|2>O><around*|(|<text|g>|)>
    </equation*>

    Suppose the reaction occurs in a flowing gas mixture of methane and air.
    Assume that the pressure is constant at <math|1<br>>, the reactant
    mixture is at a temperature of <math|298.15<K>> and has stoichiometric
    proportions of methane and oxygen, and the reaction goes to completion
    with no dissociation. For the quantity of gaseous product mixture
    containing <math|1<mol>> CO<rsub|<math|2>>, <math|2<mol>>
    H<rsub|<math|2>>O, and the nitrogen and other substances remaining from
    the air, you may use the approximate formula
    <math|C<rsub|p><around|(|<text|P>|)>=a+b*T>, where the coefficients have
    the values <math|a=297.0 <text|J>\<cdot\><text|K><rsup|-1>> and
    <math|b=8.520<timesten|-2> <text|J>\<cdot\><text|K><rsup|-2>>. Solve Eq.
    <reference|ad. flame> for <math|T<rsub|2>> to estimate the flame
    temperature to the nearest kelvin.
  </problem>

  <\problem>
    The standard molar Gibbs energy of formation of crystalline mercury(II)
    oxide at <math|600.00<K>> has the value
    <math|\<Delta\><rsub|<text|f>>*G<st>=-26.386
    <text|kJ>\<cdot\><text|mol><rsup|-1>>. Estimate the partial pressure of
    O<rsub|<math|2>> in equilibrium with HgO at this temperature:
    <math|2*<text|HgO><around*|(|<text|s>|)><arrows>2*<text|Hg><around|(|l|)>+<text|O<rsub|2>><around|(|g|)>>.
  </problem>

  <\problem>
    The combustion of hydrogen is a reaction that is known to \Pgo to
    completion.\Q

    <\enumerate-alpha>
      <item>Use data in Appendix <reference|app:props> to evaluate the
      thermodynamic equilibrium constant at <math|298.15<K>> for the reaction

      <\equation*>
        <text|H<rsub|2>><around|(|<text|g>|)>+<tfrac|1|2><text|O<rsub|2>><around|(|<text|g>|)><arrow><text|H<rsub|2>O><around|(|<text|l>|)>
      </equation*>

      <item>Assume that the reaction is at equilibrium at <math|298.15<K>> in
      a system in which the partial pressure of O<rsub|<math|2>> is
      <math|1.0<br>>. Assume ideal-gas behavior and find the equilibrium
      partial pressure of H<rsub|<math|2>> and the number of H<rsub|<math|2>>
      <em|molecules> in <math|1.0 <text|m><rsup|3>> of the gas phase.

      <item>In the preceding part, you calculated a very small value (a
      fraction) for the number of H<rsub|<math|2>> molecules in <math|1.0
      <text|m><rsup|3>>. Statistically, this fraction can be interpreted as
      the fraction of a given length of time during which one molecule is
      present in the system. Take the age of the universe as
      <math|1.0<timesten|10>> years and find the total length of time in
      seconds, during the age of the universe, that a H<rsub|<math|2>>
      molecule is present in the equilibrium system. (This hypothetical value
      is a dramatic demonstration of the statement that the limiting reactant
      is essentially entirely exhausted during a reaction with a large value
      of <math|K>.)
    </enumerate-alpha>
  </problem>

  <\problem>
    Let G represent carbon in the form of <em|graphite> and D represent the
    <em|diamond> crystal form. At <math|298.15<K>>, the thermodynamic
    equilibrium constant for G<math|\<rightleftharpoons\>>D, based on a
    standard pressure <math|p<st>=1<br>>, has the value <math|K=0.31>. The
    molar volumes of the two crystal forms at this temperature are
    <math|V<m><around|(|<text|G>|)>=5.3<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>> and
    <math|V<m><around|(|<text|D>|)>=3.4<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>.

    <\enumerate-alpha>
      <item>Write an expression for the reaction quotient
      <math|Q<rsub|<text|rxn>>> as a function of pressure. Use the
      approximate expression of the pressure factor given in Table
      <reference|tbl:9-Gamma_i>.

      <item>Use the value of <math|K> to estimate the pressure at which the D
      and G crystal forms are in equilibrium with one another at
      <math|298.15<K>>. (This is the lowest pressure at which graphite could
      in principle be converted to diamond at this temperature.)
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:11-G-xi>Consider the dissociation reaction
    <math|<text|N<rsub|2>O<rsub|4>><around*|(|<text|g>|)><arrow>2*<text|NO<rsub|2>><around*|(|<text|g>|)>>
    taking place at a constant temperature of <math|298.15<K>> and a constant
    pressure of <math|0.0500<br>>. Initially (at <math|\<xi\>=0>) the system
    contains <math|1.000 <mol>> of N<rsub|<math|2>>O<rsub|<math|4>> and no
    NO<rsub|<math|2>>. Other needed data are found in Appendix
    <reference|app:props>. Assume ideal-gas behavior.

    <\enumerate-alpha>
      <item><label|prb:11-G-xi table>For values of the advancement
      <math|\<xi\>> ranging from 0 to <math|1<mol>>, at an interval of
      <math|0.1<mol>> or less, calculate <math|<around|[|<space|0.17em>G<around|(|\<xi\>|)>-G<around|(|0|)><space|0.17em>|]>>
      to the nearest <math|0.01 <text|kJ>>. A computer spreadsheet would be a
      convenient way to make the calculations.

      <item><label|prb:11-G-xi graph>Plot your values of
      <math|G<around|(|\<xi\>|)>-G<around|(|0|)>> as a function of
      <math|\<xi\>>, and draw a smooth curve through the points.

      <item>On your curve, indicate the estimated position of
      <math|\<xi\><eq>>. Calculate the activities of
      N<rsub|<math|2>>O<rsub|<math|4>> and NO<rsub|<math|2>> for this value
      of <math|\<xi\>>, use them to estimate the thermodynamic equilibrium
      constant <math|K>, and compare your result with the value of <math|K>
      calculated from Eq. <reference|K=exp(-del(r)Gmo/RT)>.
    </enumerate-alpha>
  </problem>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|11>
    <associate|page-first|?>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|9>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|H2O amts|<tuple|h|?>>
    <associate|H2O fug|<tuple|f|?>>
    <associate|H2O vap|<tuple|j|?>>
    <associate|O2 amounts|<tuple|d|?>>
    <associate|amounts|<tuple|a|?>>
    <associate|auto-1|<tuple|10|?>>
    <associate|auto-10|<tuple|10.3|?>>
    <associate|auto-2|<tuple|Standard|?>>
    <associate|auto-3|<tuple|Pressure|?>>
    <associate|auto-4|<tuple|10.1|?>>
    <associate|auto-5|<tuple|10.2|?>>
    <associate|auto-6|<tuple|Bomb calorimetry|?>>
    <associate|auto-7|<tuple|Calorimetry|?>>
    <associate|auto-8|<tuple|<tuple|equation of state|gas at low
    pressure>|?>>
    <associate|auto-9|<tuple|Washburn corrections|?>>
    <associate|del U|<tuple|n|?>>
    <associate|del(c)Hmo(hexane)|<tuple|p|?>>
    <associate|final p|<tuple|e|?>>
    <associate|footnote-10.1|<tuple|10.1|?>>
    <associate|footnote-10.2|<tuple|10.2|?>>
    <associate|footnr-10.1|<tuple|10.1|?>>
    <associate|footnr-10.2|<tuple|10.2|?>>
    <associate|fug coeffs|<tuple|g|?>>
    <associate|g sol|<tuple|i|?>>
    <associate|gas delU|<tuple|m|?>>
    <associate|gas sol delU|<tuple|k|?>>
    <associate|l delU|<tuple|l|?>>
    <associate|molar volumes|<tuple|b|?>>
    <associate|prb:11-G-xi|<tuple|10.13|?>>
    <associate|prb:11-G-xi graph|<tuple|b|?>>
    <associate|prb:11-G-xi table|<tuple|a|?>>
    <associate|prb:11-HCl diln|<tuple|10.6|?>>
    <associate|prb:11-Na2SO3 solns|<tuple|10.5|?>>
    <associate|prb:11-flame|<tuple|10.9|?>>
    <associate|prb:11-hexane combustion|<tuple|10.7|?>>
    <associate|tbl:11-HCl prob|<tuple|10.2|?>>
    <associate|tbl:11-Na2SO3 solns|<tuple|10.1|?>>
    <associate|tbl:11-hexane combustion|<tuple|10.3|?>>
    <associate|volumes|<tuple|c|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      wagman-82

      sturtevant-40a
    </associate>
    <\associate|idx>
      <tuple|<tuple|Standard|pressure>|<pageref|auto-2>>

      <tuple|<tuple|Pressure|standard>|<pageref|auto-3>>

      <tuple|<tuple|Bomb calorimetry>|<pageref|auto-6>>

      <tuple|<tuple|Calorimetry|bomb>|<pageref|auto-7>>

      <tuple|<tuple|equation of state|gas at low pressure>|||<tuple|Equation
      of state|of a gas at low pressure>|<pageref|auto-8>>

      <tuple|<tuple|Washburn corrections>|<pageref|auto-9>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|10.1>|>
        Data for Problem <reference|prb:11-Na2SO3
        solns>.<space|0spc><assign|footnote-nr|1><hidden-binding|<tuple>|10.1><assign|fnote-+2TniXBwbTdAadwK|<quote|10.1>><assign|fnlab-+2TniXBwbTdAadwK|<quote|10.1>><rsup|<with|font-shape|<quote|right>|<reference|footnote-10.1>>>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%AAA8498-1BF16FF0>|<link|hyperlink|<id|%AAA8498-1BF16FF0>|<url|#footnr-10.1>>|10.1>.
        |<hidden-binding|<tuple|footnote-10.1>|10.1>|>>>Ref.
        [<write|bib|wagman-82><reference|bib-wagman-82>], pages 2-307 and
        2-308.
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|10.2>|>
        Data for Problem <reference|prb:11-HCl diln>. Molar integral
        enthalpies of dilution of aqueous HCl
        (<with|mode|<quote|math>|m<rprime|'><rsub|<with|mode|<quote|text>|B>>=3.337
        <with|mode|<quote|text>|mol>\<cdot\><with|mode|<quote|text>|kg><rsup|-1>>)
        at <with|mode|<quote|math>|25 <rsup|\<circ\>><with|mode|<quote|text>|C>>.<space|0spc><assign|footnote-nr|2><hidden-binding|<tuple>|10.2><assign|fnote-+2TniXBwbTdAadwL|<quote|10.2>><assign|fnlab-+2TniXBwbTdAadwL|<quote|10.2>><rsup|<with|font-shape|<quote|right>|<reference|footnote-10.2>>>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%AAA8498-B46F868>|<link|hyperlink|<id|%AAA8498-B46F868>|<url|#footnr-10.2>>|10.2>.
        |<hidden-binding|<tuple|footnote-10.2>|10.2>|Ref.
        [<write|bib|sturtevant-40a><reference|bib-sturtevant-40a>]>>>
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|10.3>|>
        Data for Problem <reference|prb:11-hexane combustion>. The values of
        intensive properties are for a temperature of
        <with|mode|<quote|math>|298.15<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>> and a pressure of
        <with|mode|<quote|math>|30<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>> unless otherwise stated. Subscripts: A
        = H<rsub|<with|mode|<quote|math>|2>>O, B =
        O<rsub|<with|mode|<quote|math>|2>>, C =
        CO<rsub|<with|mode|<quote|math>|2>>.
      </surround>|<pageref|auto-10>>
    </associate>
    <\associate|toc>
      10<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>