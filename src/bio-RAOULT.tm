<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <no-indent>BIOGRAPHICAL SKETCH

  <paragraph|<person|Fran�ois-Marie Raoult> (1830\U1901)>

  <\padded-center>
    <image|BIO/raoult.png|94pt|115pt||>
  </padded-center>

  <label|bio:raoult><index|Raoult, Fran�ois-Marie><no-indent>Raoult was a
  French physical chemist best known for his painstaking measurements of the
  freezing points and vapor pressures of dilute solutions.

  Raoult was born in Fournes-en-Weppes in northern France into a family of
  modest means<emdash>his father was an official in the local customs
  service. He supported himself with various teaching posts until he was able
  to attain his doctor's degree from the University of Paris in 1863.

  Raoult began teaching at the University of Grenoble in 1867, and three
  years later was appointed Professor and chair of chemistry. He remained in
  Grenoble for the rest of his life. He was married and had three children,
  two of whom died before him.

  His strength was in experimental measurements rather than theory. He
  constructed most of his own apparatus; some of it was displayed in Paris at
  the Centennial Museum of the Universal Exposition of 1900.<footnote|Ref.
  <cite|boirac-1901>.>

  In all Raoult published more than 100 papers based on his measurements. The
  research for his doctoral thesis, and later at Grenoble, was on the
  thermochemistry of galvanic cells.

  His first measurements of freezing-point depressions appeared in 1878. He
  pointed out the advantages of determining the molar mass of a substance
  from the freezing point of its dilute solution, and gave specific examples
  of this procedure. He was the first to show experimentally that the
  freezing-point depression of a dilute aqueous solution of an electrolyte is
  proportional to the number of ions per solute formula unit (Eq.
  <reference|del(Tf) = -nu Kf mB>).

  Starting in 1886 Raoult began publishing his measurements of the vapor
  pressures of dilute solutions of nonvolatile solutes. He used two methods:
  (1) For a highly-volatile solvent such as diethyl ether, the solution
  sample was introduced above a mercury column at the upper closed end of a
  vertical barometer tube, and the pressure determined from the height of the
  column.<footnote|Ref. <cite|raoult-1888>.> (2) The solution was placed in a
  heated flask connected to a reflux condenser, and the pressure was reduced
  at the desired temperature until boiling was observed.<footnote|Ref.
  <cite|raoult-1890>.>

  His results for diethyl ether as the solvent led him to propose the
  relation <math|<frac|f-f<rprime|'>|f*N>=0.01>, where <math|f> and
  <math|f<rprime|'>> are the vapor pressures <math|p<A><rsup|\<ast\>>> and
  <math|p<A>> of the pure solvent and the solution, respectively, both at the
  same temperature, and <math|N> is one-hundred times the solute mole
  fraction <math|x<B>>. This relation is equivalent to the Raoult's law
  equation <math|p<A>=x<A>*p<A><rsup|\<ast\>>> (Eq. <reference|pA=xA pA*>).
  He wrote:<footnote|Ref. <cite|raoult-1888>.>

  <quotation|With a view to ascertain whether this remarkable law is general,
  I dissolved in ether compounds taken from the different chemical groups,
  and chosen from those whose boiling points are the highest; the compounds
  having molecular weights which are very widely different from one another;
  and I measured the vapor pressures of the solutions obtained. In every case
  I found ...that the ratio <math|<frac|f-f<rprime|'>|f*N>> is very nearly
  0.01.>

  His measurements with dilute solutions of nonelectrolyte solutes in various
  other solvents, including benzene, ethanol, and water, gave the same
  results.<footnote|Ref. <cite|raoult-1890>.> He was pleased that his
  measurements confirmed the theory of solutions being developed by J. H.
  van't Hoff.

  Raoult's work brought him many honors. He was most proud of being named
  Commander of the French Legion of Honor in 1900.

  Sir William Ramsey described Raoult's personality as follows:<footnote|Ref.
  <cite|ramsey-1901>.>

  <quotation|Though modest and retiring, Raoult's devotion to his work,
  dignity of character and sweetness of temper gained him many friends. He
  was not an ambitious man, but was content to work on, happy if his
  discoveries contributed to the advancement of science.>
</body>

<\initial>
  <\collection>
    <associate|font-base-size|9>
    <associate|page-medium|paper>
    <associate|par-columns|2>
    <associate|par-columns-sep|1fn>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|Raoult, Fran�ois-Marie|1>>
    <associate|bio:cat|<tuple|1|1>>
    <associate|bio:raoult|<tuple|1|?>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|1>>
    <associate|footnote-3|<tuple|3|1>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnote-5|<tuple|5|?>>
    <associate|footnote-6|<tuple|6|?>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|1>>
    <associate|footnr-3|<tuple|3|1>>
    <associate|footnr-4|<tuple|4|?>>
    <associate|footnr-5|<tuple|5|?>>
    <associate|footnr-6|<tuple|6|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      kohler-2011

      leicester-51

      hess-1840

      hess-1840

      davis-51
    </associate>
    <\associate|idx>
      <tuple|<tuple|Willard, F. D. C.>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|F. D.
      C. Willard> (1968\U1982) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>