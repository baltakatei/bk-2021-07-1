<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|Problems>

  <\problem>
    <label|prb:9-mole fractions>For a binary solution, find expressions for
    the mole fractions <math|x<B>> and <math|x<A>> as functions of the solute
    molality <math|m<B>>.
  </problem>

  <\problem>
    Consider a binary mixture of two liquids, A and B. The molar volume of
    mixing, <math|<Del>V<mix>/n>, is given by Eq. <reference|binary mixt
    Del(mix)V>.

    <\enumerate-alpha>
      <item>Find a formula for calculating the value of <math|<Del>V<mix>/n>
      of a binary mixture from values of <math|x<A>>, <math|x<B>>,
      <math|M<A>>, <math|M<B>>, <math|\<rho\>>,
      <math|\<rho\><A><rsup|\<ast\>>>, and <math|\<rho\><B><rsup|\<ast\>>>.

      \;

      <item>The molar volumes of mixing for liquid binary mixtures of
      1-hexanol (A) and 1-octene (B) at <math|25 <degC>> have been calculated
      from their measured densities. The data are in Table
      <reference|tbl:9-octene-hexOH V^E>. The molar volumes of the pure
      constituents are <math|V<A><rsup|\<ast\>>=125.31
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>> and
      <math|V<B><rsup|\<ast\>>=157.85 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>.
      Use the method of intercepts to estimate the partial molar volumes of
      both constituents in an equimolar mixture (<math|x<A>=x<B>=0.5>), and
      the partial molar volume <math|V<B><rsup|\<infty\>>> of B at infinite
      dilution.
    </enumerate-alpha>
  </problem>

  <float|float|thb|<\big-table|<math|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|5|5|cell-col-span|1>|<cwith|1|1|5|5|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|12|12|1|-1|cell-bborder|1ln>|<cwith|2|-1|1|-1|cell-halign|C.>|<table|<row|<cell|<math|x<B>>>|<cell|<math|<around|[|<Del>V<mix>/n|]>/<around*|(|<text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>|)>>>|<cell|<space|2em>>|<cell|<math|x<B>>>|<cell|<math|<around|[|<Del>V<mix>/n|]>/<around*|(|<text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>|)>>>>|<row|<cell|0>|<cell|0>|<cell|>|<cell|0.555>|<cell|0.005>>|<row|<cell|0.049>|<cell|-0.027>|<cell|>|<cell|0.597>|<cell|0.011>>|<row|<cell|0.097>|<cell|-0.050>|<cell|>|<cell|0.702>|<cell|0.029>>|<row|<cell|0.146>|<cell|-0.063>|<cell|>|<cell|0.716>|<cell|0.035>>|<row|<cell|0.199>|<cell|-0.077>|<cell|>|<cell|0.751>|<cell|0.048>>|<row|<cell|0.235>|<cell|-0.073>|<cell|>|<cell|0.803>|<cell|0.056>>|<row|<cell|0.284>|<cell|-0.074>|<cell|>|<cell|0.846>|<cell|0.058>>|<row|<cell|0.343>|<cell|-0.065>|<cell|>|<cell|0.897>|<cell|0.057>>|<row|<cell|0.388>|<cell|-0.053>|<cell|>|<cell|0.944>|<cell|0.049>>|<row|<cell|0.448>|<cell|-0.032>|<cell|>|<cell|1>|<cell|0>>|<row|<cell|0.491>|<cell|-0.016>|<cell|>|<cell|>|<cell|>>>>>>>
    <label|tbl:9-octene-hexOH V^E>Molar volumes of mixing of binary mixtures
    of 1-hexanol (A) and 1-octene (B) at <math|25
    <degC>>.<note-ref|+1PualPXI2C0ZUoKH>

    \;

    <note-inline|Ref. <cite|tresz-02>.|+1PualPXI2C0ZUoKH>
  </big-table>>

  <\problem>
    <label|prb:9-droplet>Extend the derivation of Prob.
    8.<reference|prb:8-droplet>, concerning a liquid droplet of radius
    <math|r> suspended in a gas, to the case in which the liquid and gas are
    both mixtures. Show that the equilibrium conditions are
    <math|T<rsup|<text|g>>=T<rsup|<text|l>>>,
    <math|\<mu\><rsub|i><rsup|<text|g>>=\<mu\><rsub|i><rsup|<text|l>>> (for
    each species <math|i> that can equilibrate between the two phases), and
    <math|p<rsup|<text|l>>=p<rsup|<text|g>>+2<g>/r>, where <math|<g>> is the
    surface tension. (As in Prob. 8.<reference|prb:8-droplet>, the last
    relation is the <index|Laplace equation>Laplace equation.)
  </problem>

  <\problem>
    Consider a gaseous mixture of <math|4.0000<timesten|-2> <mol>> of
    N<rsub|<math|2>> (A) and <math|4.0000<timesten|-2> <mol>> of
    CO<rsub|<math|2>> (B) in a volume of <math|1.0000<timesten|-3>
    <text|m><rsup|3>> at a temperature of <math|298.15<K>>. The second virial
    coefficients at this temperature have the values.<footnote|Refs.
    <cite|angus-76>, <cite|dymond-80>, and <cite|edwards-42>.>

    <\eqnarray*>
      <tformat|<table|<row|<cell|B<rsub|<text|A>\<nocomma\><text|A>>>|<cell|=>|<cell|-4.8<timesten|-6>
      <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|B<rsub|<text|B>\<nocomma\><text|B>>>|<cell|=>|<cell|-124.5<timesten|-6>
      <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>|<row|<cell|B<rsub|<text|A>\<nocomma\><text|B>>>|<cell|=>|<cell|-47.5<timesten|-6>
      <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>>>
    </eqnarray*>

    Compare the pressure of the real gas mixture with that predicted by the
    ideal gas equation. See Eqs. <reference|pV/n=RT[1+B/(V/n)+...)> and
    <reference|B=yA^2 B(AA)+...>.
  </problem>

  <\problem>
    <label|prb:9-air>At <math|25 <degC>> and <math|1<br>>, the Henry's law
    constants of nitrogen and oxygen dissolved in water are
    <math|k<rsub|<text|H>,<text|N><rsub|2>>=8.64<timesten|4> <br>> and
    <math|k<rsub|<text|H>,<text|O><rsub|2>>=4.41<timesten|4>
    <br>>.<footnote|Ref. <cite|wilhelm-77>.> The vapor pressure of water at
    this temperature and pressure is <math|p<rsub|<text|H<rsub|2>O>>=0.032
    <br>>. Assume that dry air contains only N<rsub|<math|2>> and
    O<rsub|<math|2>> at mole fractions <math|y<rsub|<text|N<rsub|2>>>=0.788>
    and <math|y<rsub|<text|O<rsub|2>>>=0.212>. Consider liquid\Ugas systems
    formed by equilibrating liquid water and air at <math|25 <degC>> and
    <math|1.000 <br>>, and assume that the gas phase behaves as an ideal gas
    mixture.

    Hint: The sum of the partial pressures of N<rsub|<math|2>> and
    O<rsub|<math|2>> must be <math|<around|(|1.000-0.032|)><br>=0.968<br>>.
    If the volume of one of the phases is much larger than that of the other,
    then almost all of the N<rsub|<math|2>> and O<rsub|<math|2>> will be in
    the predominant phase and the ratio of their amounts in this phase must
    be practically the same as in dry air.

    Determine the mole fractions of N<rsub|<math|2>> and O<rsub|<math|2>> in
    both phases in the following limiting cases:

    <\enumerate-alpha>
      <item>A large volume of air is equilibrated with just enough water to
      leave a small drop of liquid.

      <item>A large volume of water is equilibrated with just enough air to
      leave a small bubble of gas.
    </enumerate-alpha>
  </problem>

  <\problem>
    Derive the expression for <math|<g><mbB>> given in Table
    <reference|tbl:9-act coeff-fugacity>, starting with Eq. <reference|act
    coeff m,B>.
  </problem>

  <\problem>
    <label|prb:9-nonideal gas mixt>Consider a nonideal binary gas mixture
    with the simple <index-complex|<tuple|equation of state|gas at low
    pressure>|||<tuple|Equation of state|of a gas at low pressure>>equation
    of state <math|V=n*R*T/p+n*B> (Eq. <reference|V=nRT/p+nB>).

    <\enumerate-alpha>
      <item>The <index|Lewis and Randall rule><em|rule of Lewis and Randall>
      states that the value of the mixed second virial coefficient
      <math|B<rsub|<text|A>\<nocomma\><text|B>>> is the average of
      <math|B<rsub|<text|A>\<nocomma\><text|A>>> and
      <math|B<rsub|<text|B>\<nocomma\><text|B>>>. Show that when this rule
      holds, the fugacity coefficient of A in a binary gas mixture of any
      composition is given by <math|ln \<phi\><A>=B<rsub|<text|A>\<nocomma\><text|A>>\<cdot\>p/<around*|(|R*T|)>>.
      By comparing this expression with Eq. <reference|ln(phi)=Bp/RT> for a
      pure gas, express the fugacity of A in the mixture as a function of the
      fugacity of pure A at the same temperature and pressure as the mixture.

      <item>The rule of Lewis and Randall is not accurately obeyed when
      constituents A and B are chemically dissimilar. For example, at
      <math|298.15<K>>, the second virial coefficients of H<rsub|<math|2>>O
      (A) and N<rsub|<math|2>> (B) are <math|B<rsub|<text|A>\<nocomma\><text|A>>=-1158
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>> and
      <math|B<rsub|<text|B>\<nocomma\><text|B>>=-5
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>, respectively, whereas
      the mixed second virial coefficient is
      <math|B<rsub|<text|A>\<nocomma\><text|B>>=-40
      <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>

      When liquid water is equilibrated with nitrogen at <math|298.15<K>> and
      <math|1<br>>, the partial pressure of H<rsub|<math|2>>O in the gas
      phase is <math|p<A>=0.03185<br>>. Use the given values of
      <math|B<rsub|<text|A>\<nocomma\><text|A>>>,
      <math|B<rsub|<text|B>\<nocomma\><text|B>>>, and
      <math|B<rsub|<text|A>\<nocomma\><text|B>>> to calculate the fugacity of
      the gaseous H<rsub|<math|2>>O in this binary mixture. Compare this
      fugacity with the fugacity calculated with the value of
      <math|B<rsub|<text|A>\<nocomma\><text|B>>> predicted by the rule of
      Lewis and Randall.
    </enumerate-alpha>
  </problem>

  <float|float|thb|<\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|6|6|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|x<A>>>|<cell|<math|<g><A>>>|<cell|<space|2em>>|<cell|<math|x<A>>>|<cell|<math|<g><A>>>>|<row|<cell|0>|<cell|2.0<note-ref|+1PualPXI2C0ZUoKI>>|<cell|>|<cell|0.7631>|<cell|1.183>>|<row|<cell|0.1334>|<cell|1.915>|<cell|>|<cell|0.8474>|<cell|1.101>>|<row|<cell|0.2381>|<cell|1.809>|<cell|>|<cell|0.9174>|<cell|1.046>>|<row|<cell|0.4131>|<cell|1.594>|<cell|>|<cell|0.9782>|<cell|1.005>>|<row|<cell|0.5805>|<cell|1.370>|<cell|>|<cell|>|<cell|>>>>>>
    <label|tbl:9-benz-oct>Activity coefficient of benzene (A) in mixtures of
    benzene and 1-octanol at <math|20 <degC>>. The reference state is the
    pure liquid.

    \;

    <note-inline|Extrapolated|+1PualPXI2C0ZUoKI>
  </big-table>>

  <\problem>
    <label|prb:9-benz-oct> Benzene and 1-octanol are two liquids that mix in
    all proportions. Benzene has a measurable vapor pressure, whereas
    1-octanol is practically nonvolatile. The data in Table
    <vpageref|tbl:9-benz-oct> were obtained by Platford<footnote|Ref.
    <cite|platford-76>.> using the isopiestic vapor pressure method.

    <\enumerate-alpha>
      <item>Use numerical integration to evaluate the integral on the right
      side of Eq. <reference|ln(ac_B)=int...> at each of the values of
      <math|x<A>> listed in the table, and thus find <math|<g><B>> at these
      compositions.

      <item>Draw two curves on the same graph showing the effective mole
      fractions <math|<g><A>*x<A>> and <math|<g><B>*x<B>> as functions of
      <math|x<A>>. Are the deviations from ideal-mixture behavior positive or
      negative?
    </enumerate-alpha>
  </problem>

  <float|float|thb|<\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|7|7|cell-col-span|1>|<cwith|1|1|7|7|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|9|9|1|-1|cell-bborder|1ln>|<cwith|2|-1|1|-1|cell-halign|C.>|<cwith|2|-1|1|-1|cell-bsep|0.25fn>|<cwith|2|-1|1|-1|cell-tsep|0.25fn>|<table|<row|<cell|<math|x<A>>>|<cell|<math|y<A>>>|<cell|<math|p/<text|kPa>>>|<cell|>|<cell|<math|x<A>>>|<cell|<math|y<A>>>|<cell|<math|p/<text|kPa>>>>|<row|<cell|0>|<cell|0>|<cell|29.894>|<cell|<space|2em>>|<cell|0.4201>|<cell|0.5590>|<cell|60.015>>|<row|<cell|0.0207>|<cell|0.2794>|<cell|40.962>|<cell|>|<cell|0.5420>|<cell|0.5783>|<cell|60.416>>|<row|<cell|0.0314>|<cell|0.3391>|<cell|44.231>|<cell|>|<cell|0.6164>|<cell|0.5908>|<cell|60.416>>|<row|<cell|0.0431>|<cell|0.3794>|<cell|46.832>|<cell|>|<cell|0.7259>|<cell|0.6216>|<cell|59.868>>|<row|<cell|0.0613>|<cell|0.4306>|<cell|50.488>|<cell|>|<cell|0.8171>|<cell|0.6681>|<cell|58.321>>|<row|<cell|0.0854>|<cell|0.4642>|<cell|53.224>|<cell|>|<cell|0.9033>|<cell|0.7525>|<cell|54.692>>|<row|<cell|0.1811>|<cell|0.5171>|<cell|57.454>|<cell|>|<cell|0.9497>|<cell|0.8368>|<cell|51.009>>|<row|<cell|0.3217>|<cell|0.5450>|<cell|59.402>|<cell|>|<cell|1>|<cell|1>|<cell|44.608>>>>>>
    <label|tbl:9-benz-MeOH>Liquid and gas compositions in the two-phase
    system of methanol (A) and benzene (B) at <math|45
    <degC>>.<note-ref|+1PualPXI2C0ZUoKJ>

    \;

    <note-inline||+1PualPXI2C0ZUoKJ>Ref. <cite|toghiani-94>
  </big-table>>

  <\problem>
    <label|prb:9-MeOH-benz>Table <vpageref|tbl:9-benz-MeOH> lists measured
    values of gas-phase composition and total pressure for the binary
    two-phase methanol\Ubenzene system at constant temperature and varied
    liquid-phase composition. <math|x<A>> is the mole fraction of methanol in
    the liquid mixture, and <math|y<A>> is the mole fraction of methanol in
    the equilibrated gas phase.

    <\enumerate-alpha>
      <item>For each of the 16 different liquid-phase compositions, tabulate
      the partial pressures of A and B in the equilibrated gas phase.

      <item>Plot <math|p<A>> and <math|p<B>> versus <math|x<A>> on the same
      graph. Notice that the behavior of the mixture is far from that of an
      ideal mixture. Are the deviations from Raoult's law positive or
      negative?

      <item>Tabulate and plot the activity coefficient <math|<g><B>> of the
      benzene as a function of <math|x<A>> using a pure-liquid reference
      state. Assume that the fugacity <math|<fug><B>> is equal to
      <math|p<B>>, and ignore the effects of variable pressure.

      <item>Estimate the Henry's law constant
      <math|k<rsub|<text|H>,<text|A>>> of methanol in the benzene environment
      at <math|45 <degC>> by the graphical method suggested in Fig.
      <reference|fig:9-fugacity vs xB>(b). Again assume that <math|<fug><A>>
      and <math|p<A>> are equal, and ignore the effects of variable pressure.
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:9-dil osmotic coeff> Consider a dilute binary nonelectrolyte
    solution in which the dependence of the chemical potential of solute B on
    composition is given by

    <\equation*>
      \<mu\><B>=\<mu\><mbB><rf>+R*T*ln <frac|m<B>|m<st>>+k<rsub|m>*m<B>
    </equation*>

    where <math|\<mu\><mbB><rf>> and <math|k<rsub|m>> are constants at a
    given <math|T> and <math|p>. (The derivation of this equation is sketched
    in Sec. <reference|9-nonideal dil solns>.) Use the Gibbs\UDuhem equation
    in the form <math|<dif>\<mu\><A>=-<around|(|n<B>/n<A>|)><dif>\<mu\><B>>
    to obtain an expression for <math|\<mu\><A>-\<mu\><A><rsup|\<ast\>>> as a
    function of <math|m<B>> in this solution.
  </problem>

  <\problem>
    By means of the isopiestic vapor pressure technique, the osmotic
    coefficients of aqueous solutions of urea at <math|25 <degC>> have been
    measured at molalities up to the saturation limit of about <math|20
    <text|mol>\<cdot\><text|kg><rsup|-1>>.<footnote|Ref.
    <cite|scatchard-38a>.> The experimental values are closely approximated
    by the function

    <\equation*>
      \<phi\><rsub|m>=1.00-<frac|0.050*<space|0.17em>m<B>/m<st>|1.00+0.179*<space|0.17em>m<B>/m<st>>
    </equation*>

    where <math|m<st>> is <math|1 <text|mol>\<cdot\><text|kg><rsup|-1>>.
    Calculate values of the solvent and solute activity coefficients
    <math|<g><A>> and <math|<g><mbB>> at various molalities in the range
    0\U<math|20 <text|mol>\<cdot\><text|kg><rsup|-1>>, and plot them versus
    <math|m<B>/m<st>>. Use enough points to be able to see the shapes of the
    curves. What are the limiting slopes of these curves as <math|m<B>>
    approaches zero?
  </problem>

  <\problem>
    Use Eq. <reference|d(mu_i)/dp=V_i> to derive an expression for the rate
    at which the logarithm of the activity coefficient of component <math|i>
    of a liquid mixture changes with pressure at constant temperature and
    composition: <math|<pd|ln <g><rsub|i>|p|T,<allni>>=>?
  </problem>

  <\problem>
    Assume that at sea level the atmosphere has a pressure of <math|1.00<br>>
    and a composition given by <math|y<rsub|<text|N<rsub|2>>>=0.788> and
    <math|y<rsub|<text|O<rsub|2>>>=0.212>. Find the partial pressures and
    mole fractions of N<rsub|<math|2>> and O<rsub|<math|2>>, and the total
    pressure, at an altitude of <math|10.0 <text|km>>, making the (drastic)
    approximation that the atmosphere is an ideal gas mixture in an
    equilibrium state at <math|0 <degC>>. For <math|g> use the value of the
    standard acceleration of free fall listed in Appendix
    <reference|app:const>.
  </problem>

  <\problem>
    Consider a tall column of a dilute binary liquid solution at equilibrium
    in a gravitational field.

    <\enumerate-alpha>
      <item>Derive an expression for <math|ln
      <space|0.17em><around|[|<space|0.17em>c<B><around|(|h|)>/c<B><around|(|0|)><space|0.17em>|]>>,
      where <math|c<B><around|(|h|)>> and <math|c<B><around|(|0|)>> are the
      solute concentrations at elevations <math|h> and <math|0>. Your
      expression should be a function of <math|h>, <math|M<B>>, <math|T>,
      <math|\<rho\>>, and the partial specific volume of the solute at
      infinite dilution, <math|v<B><rsup|\<infty\>>>. For the dependence of
      pressure on elevation, you may use the hydrostatic formula
      <math|<difp><space|-0.17em>=<space|-0.17em>-\<rho\>*g*<dif>h> (Eq.
      <vpageref|dp=-rho g dh>) and assume the solution density <math|\<rho\>>
      is the same at all elevations. Hint: use the derivation leading to Eq.
      <reference|ln(c''/c') (centr)> as a guide.

      <item>Suppose you have a tall vessel containing a dilute solution of a
      macromolecule solute of molar mass <math|M<B>=10.0
      <text|kg>\<cdot\><text|mol><rsup|-1>> and partial specific volume
      <math|v<B><rsup|\<infty\>>=0.78 <text|cm><rsup|3>\<cdot\><text|g><rsup|-1>>.
      The solution density is <math|\<rho\>=1.00
      <text|g>\<cdot\><text|cm><rsup|-3>> and the temperature is
      <math|T=300<K>>. Find the height <math|h> from the bottom of the vessel
      at which, in the equilibrium state, the concentration <math|c<B>> has
      decreased to <math|99> percent of the concentration at the bottom.
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:9-centrifuge> FhuA is a protein found in the outer membrane of
    the <em|Escherichia coli> bacterium. From the known amino acid sequence,
    its molar mass is calculated to be <math|78.804
    <text|kg>\<cdot\><text|mol><rsup|-1>>. In aqueous solution, molecules of
    the detergent dodecyl maltoside bind to a FhuA molecule to form an
    aggregate that behaves as a single solute species. Figure
    <vpageref|fig:9-sed. Eq.> shows data collected in a sedimentation
    equilibrium experiment with a dilute solution of the
    aggregate.<footnote|Ref. <cite|boulanger-96>.> In the graph, <math|A> is
    the absorbance measured at a wavelength of <math|280 <text|nm>> (a
    property that is a linear function of the aggregate concentration) and
    <math|r> is the radial distance from the axis of rotation of the
    centrifuge rotor. The experimental points fall very close to the straight
    line shown in the graph. The sedimentation conditions were
    <math|\<omega\>=838 <text|s><rsup|-1>> and <math|T=293<K>>. The authors
    used the values <math|v<B><rsup|\<infty\>>=0.776
    <text|cm><rsup|3>\<cdot\><text|g><rsup|-1>> and <math|\<rho\>=1.004
    <text|g>\<cdot\><text|cm><rsup|-3>>.

    <\enumerate-alpha>
      <item>The values of <math|r> at which the absorbance was measured range
      from <math|6.95 <text|cm>> to <math|7.20 cm>. Find the difference of
      pressure in the solution between these two positions.

      <item>Find the molar mass of the aggregate solute species, and use it
      to estimate the mass binding ratio (the mass of bound detergent divided
      by the mass of protein).
    </enumerate-alpha>
  </problem>

  <\float|float|thb>
    <\framed>
      <\big-figure|<image|09-SUP/EQMSED.eps|190pt|171pt||>>
        <label|fig:9-sed. Eq.>Sedimentation equilibrium of a dilute solution
        of the FhuA-dodecyl maltoside aggregate.
      </big-figure>
    </framed>
  </float>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|9>
    <associate|page-first|233>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|8>
    <associate|subsection-nr|2>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|9|?>>
    <associate|auto-2|<tuple|9.1|?>>
    <associate|auto-3|<tuple|Laplace equation|?>>
    <associate|auto-4|<tuple|<tuple|equation of state|gas at low
    pressure>|?>>
    <associate|auto-5|<tuple|Lewis and Randall rule|?>>
    <associate|auto-6|<tuple|9.4|?>>
    <associate|auto-7|<tuple|9.3|?>>
    <associate|auto-8|<tuple|9.1|?>>
    <associate|fig:9-sed. Eq.|<tuple|9.1|?>>
    <associate|footnote-9.1|<tuple|9.1|?>>
    <associate|footnote-9.2|<tuple|9.2|?>>
    <associate|footnote-9.3|<tuple|9.3|?>>
    <associate|footnote-9.4|<tuple|9.4|?>>
    <associate|footnote-9.5|<tuple|9.5|?>>
    <associate|footnote-9.6|<tuple|9.6|?>>
    <associate|footnote-9.7|<tuple|9.7|?>>
    <associate|footnote-9.8|<tuple|9.8|?>>
    <associate|footnr-9.1|<tuple|9.1|?>>
    <associate|footnr-9.2|<tuple|9.2|?>>
    <associate|footnr-9.3|<tuple|9.3|?>>
    <associate|footnr-9.4|<tuple|9.4|?>>
    <associate|footnr-9.5|<tuple|9.5|?>>
    <associate|footnr-9.6|<tuple|9.3|?>>
    <associate|footnr-9.7|<tuple|9.7|?>>
    <associate|footnr-9.8|<tuple|9.8|?>>
    <associate|prb:9-MeOH-benz|<tuple|9.9|?>>
    <associate|prb:9-air|<tuple|9.5|?>>
    <associate|prb:9-benz-oct|<tuple|9.8|?>>
    <associate|prb:9-centrifuge|<tuple|9.15|?>>
    <associate|prb:9-dil osmotic coeff|<tuple|9.10|?>>
    <associate|prb:9-droplet|<tuple|9.3|?>>
    <associate|prb:9-mole fractions|<tuple|9.1|?>>
    <associate|prb:9-nonideal gas mixt|<tuple|9.7|?>>
    <associate|tbl:9-benz-MeOH|<tuple|9.3|?>>
    <associate|tbl:9-benz-oct|<tuple|9.2|?>>
    <associate|tbl:9-octene-hexOH V^E|<tuple|9.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      tresz-02

      angus-76

      dymond-80

      edwards-42

      wilhelm-77

      platford-76

      toghiani-94

      scatchard-38a

      boulanger-96
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.1>|>
        Sedimentation equilibrium of a dilute solution of the FhuA-dodecyl
        maltoside aggregate.
      </surround>|<pageref|auto-8>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Laplace equation>|<pageref|auto-3>>

      <tuple|<tuple|equation of state|gas at low pressure>|||<tuple|Equation
      of state|of a gas at low pressure>|<pageref|auto-4>>

      <tuple|<tuple|Lewis and Randall rule>|<pageref|auto-5>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.1>|>
        Molar volumes of mixing of binary mixtures of 1-hexanol (A) and
        1-octene (B) at <with|mode|<quote|math>|25
        <rsup|\<circ\>><with|mode|<quote|text>|C>>.<space|0spc><assign|footnote-nr|1><hidden-binding|<tuple>|9.1><assign|fnote-+1PualPXI2C0ZUoKH|<quote|9.1>><assign|fnlab-+1PualPXI2C0ZUoKH|<quote|9.1>><rsup|<with|font-shape|<quote|right>|<reference|footnote-9.1>>>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7F53B06F8-7D6C2C180>|<link|hyperlink|<id|%-7F53B06F8-7D6C2C180>|<url|#footnr-9.1>>|9.1>.
        |<hidden-binding|<tuple|footnote-9.1>|9.1>|Ref.
        [<write|bib|tresz-02><reference|bib-tresz-02>].>>>
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.2>|>
        Activity coefficient of benzene (A) in mixtures of benzene and
        1-octanol at <with|mode|<quote|math>|20
        <rsup|\<circ\>><with|mode|<quote|text>|C>>. The reference state is
        the pure liquid.

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7F53B06F8-7E7100268>|<link|hyperlink|<id|%-7F53B06F8-7E7100268>|<url|#footnr-9.4>>|9.4>.
        |<hidden-binding|<tuple|footnote-9.4>|9.4>|Extrapolated>>>
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|9.3>|>
        Liquid and gas compositions in the two-phase system of methanol (A)
        and benzene (B) at <with|mode|<quote|math>|45
        <rsup|\<circ\>><with|mode|<quote|text>|C>>.<space|0spc><assign|footnote-nr|6><hidden-binding|<tuple>|9.6><assign|fnote-+1PualPXI2C0ZUoKJ|<quote|9.6>><assign|fnlab-+1PualPXI2C0ZUoKJ|<quote|9.6>><rsup|<with|font-shape|<quote|right>|<reference|footnote-9.6>>>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7F53B06F8--7FFCEAAD0>|<link|hyperlink|<id|%-7F53B06F8--7FFCEAAD0>|<url|#footnr-9.6>>|9.6>.
        |<hidden-binding|<tuple|footnote-9.6>|9.6>|>>>Ref.
        [<write|bib|toghiani-94><reference|bib-toghiani-94>]
      </surround>|<pageref|auto-7>>
    </associate>
    <\associate|toc>
      9<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>