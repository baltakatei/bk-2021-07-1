<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk|comment>>

<\body>
  <appendix|Physical Contants><label|appendix
  physical-constants><label|app:const>

  <index|Physical constants, values of><index|Constants, values of>The
  following table lists values of fundamental physical constants used to
  define SI base units or needed in thermodynamic calculations. The 2019 SI
  revision treats the first six constants (<math|<Del>\<nu\><rsub|<text|Cs>>>
  through <math|N<rsub|<text|A>>>) as <em|defining constants> or
  <em|fundamental constants> whose values are exact by
  definition.<folded-comment|+25MwX5dg1qHrfR1z|+25MwX5dg1qHrfR20|comment|devoe|1625709978||2010
  CODATA values are the most recent as of Jan2015>

  \;

  <\big-table>
    <bktable3|<tformat|<cwith|1|11|1|3|cell-valign|c>||||||<cwith|2|-1|3|3|cell-halign|L.>|<table|<row|<cell|Constant>|<cell|Symbol>|<cell|Value
    in SI units>>|<row|<cell|cesium-133 hyperfine transition
    frequency>|<cell|<math|<Del>\<nu\><rsub|<text|Cs>>>>|<cell|<math|9.192<separating-space|0.2em>631<separating-space|0.2em>770<timesten|9>
    <text|s><per>>>>|<row|<cell|speed of light in
    vacuum>|<cell|<math|c>>|<cell|<math|2.997<separating-space|0.2em>924<separating-space|0.2em>58<timesten|8>
    <text|m>\<cdot\><text|s><per>>>>|<row|<cell|Planck
    constant>|<cell|<math|h>>|<cell|<math|6.626<separating-space|0.2em>070<separating-space|0.2em>15<timesten|-34>
    <text|J>\<cdot\><text|s>>>>|<row|<cell|elementary
    charge>|<cell|<math|e>>|<cell|<math|1.602<separating-space|0.2em>176<separating-space|0.2em>634<timesten|-19>
    <text|C>>>>|<row|<cell|Boltzmann constant>|<cell|<math|k>>|<cell|<math|1.380<separating-space|0.2em>649<timesten|-23>
    <text|J>\<cdot\><text|K><rsup|-1>>>>|<row|<cell|<index|Avogadro
    constant>Avogadro constant>|<cell|<math|N<rsub|<text|A>>>>|<cell|<math|6.022<separating-space|0.2em>140<separating-space|0.2em>76<timesten|23>
    <text|mol><per>>>>|<row|<cell|<index|Gas constant>gas
    constant<space|.15em><note-ref|+25MwX5dg1qHrfR1v>>|<cell|<math|R>>|<cell|<math|8.314<separating-space|0.2em>462\<ldots\>
    <text|J>\<cdot\><text|K><per>\<cdot\><text|mol><per>>>>|<row|<cell|<index|Faraday
    constant>Faraday constant<space|.15em><note-ref|+25MwX5dg1qHrfR1w>>|<cell|<math|F>>|<cell|<math|9.648<separating-space|0.2em>533\<ldots\><timesten|4>
    <text|C>\<cdot\><text|mol><per>>>>|<row|<cell|electric
    constant<space|.15em><note-ref|+25MwX5dg1qHrfR1x>>|<cell|<math|\<epsilon\><rsub|0>>>|<cell|<math|8.854<separating-space|0.2em>187\<ldots\><timesten|-12>
    <text|C><rsup|2>\<cdot\><text|J><per>\<cdot\><text|m><per>>>>|<row|<cell|standard
    acceleration of free fall<space|.15em><note-ref|+25MwX5dg1qHrfR1y>>|<cell|<math|g<rsub|<text|n>>>>|<cell|<math|9.806<separating-space|0.2em>65
    <text|m>\<cdot\><text|s><rsup|-2>>>>>>>

    \;
  <|big-table>
    \;

    <note-inline||+25MwX5dg1qHrfR1v>or molar gas constant; <math|R> is equal
    to <math|N<rsub|<text|A>>*k>

    <note-inline||+25MwX5dg1qHrfR1w><math|F> is equal to
    <math|N<rsub|<text|A>>*e>

    <note-inline|or permittivity of vacuum; <math|\<epsilon\><rsub|0>> is
    equal to <math|10<rsup|-7>/<around*|(|4*\<pi\>*c<rsup|2>|)>>|+25MwX5dg1qHrfR1x>

    <note-inline|or standard acceleration of gravity|+25MwX5dg1qHrfR1y>

    \;

    \;
  </big-table>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|39>
    <associate|preamble|false>
    <associate|section-nr|0>
    <associate|subsection-nr|2>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|app:const|<tuple|A|?>>
    <associate|appendix physical-constants|<tuple|A|?>>
    <associate|auto-1|<tuple|A|?>>
    <associate|auto-2|<tuple|Physical constants, values of|?>>
    <associate|auto-3|<tuple|Constants, values of|?>>
    <associate|auto-4|<tuple|Avogadro constant|?>>
    <associate|auto-5|<tuple|Gas constant|?>>
    <associate|auto-6|<tuple|Faraday constant|?>>
    <associate|auto-7|<tuple|4|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Physical constants, values of>|<pageref|auto-2>>

      <tuple|<tuple|Constants, values of>|<pageref|auto-3>>

      <tuple|<tuple|Avogadro constant>|<pageref|auto-4>>

      <tuple|<tuple|Gas constant>|<pageref|auto-5>>

      <tuple|<tuple|Faraday constant>|<pageref|auto-6>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%1BA8F708-6B47040>|<link|hyperlink|<id|%1BA8F708-6B47040>|<url|#footnr-1>>|1>.
        |<hidden-binding|<tuple|footnote-1>|1>|>>>or molar gas constant;
        <with|mode|<quote|math>|R> is equal to
        <with|mode|<quote|math>|N<rsub|<with|mode|<quote|text>|A>>*k>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%1BA8F708-65EBEF0>|<link|hyperlink|<id|%1BA8F708-65EBEF0>|<url|#footnr-2>>|2>.
        |<hidden-binding|<tuple|footnote-2>|2>|>>><with|mode|<quote|math>|F>
        is equal to <with|mode|<quote|math>|N<rsub|<with|mode|<quote|text>|A>>*e>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%1BA8F708-AD480F0>|<link|hyperlink|<id|%1BA8F708-AD480F0>|<url|#footnr-3>>|3>.
        |<hidden-binding|<tuple|footnote-3>|3>|or permittivity of vacuum;
        <with|mode|<quote|math>|\<epsilon\><rsub|0>> is equal to
        <with|mode|<quote|math>|10<rsup|-7>/<around*|(|4*\<pi\>*c<rsup|2>|)>>>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%1BA8F708-F1BAB78>|<link|hyperlink|<id|%1BA8F708-F1BAB78>|<url|#footnr-4>>|4>.
        |<hidden-binding|<tuple|footnote-4>|4>|or standard acceleration of
        gravity>>>

        \;

        \;
      </surround>|<pageref|auto-7>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix
      A<space|2spc>Physical Contants> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>