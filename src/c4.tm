<TeXmacs|2.1.1>

<project|../../../git-OC/BK-2021-07-1..devoe_thermo/src/book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <chapter|The Second Law><label|Chap. 4><label|c4>

  The second law of thermodynamics concerns entropy and the spontaneity of
  processes. This chapter discusses theoretical aspects and practical
  applications.

  We have seen that the first law allows us to set up a balance sheet for
  energy changes during a process, but says nothing about why some processes
  occur spontaneously and others are impossible. The laws of physics explain
  some spontaneous changes. For instance, unbalanced forces on a body cause
  acceleration, and a temperature gradient at a diathermal boundary causes
  heat transfer. But how can we predict whether a phase change, a transfer of
  solute from one solution phase to another, or a chemical reaction will
  occur spontaneously under the existing conditions? The second law provides
  the principle we need to answer these and other questions\Va general
  criterion for spontaneity in a closed system.

  <section|Types of Processes><label|4-types of processes><label|c4 sec
  processes>

  Any conceivable process is either spontaneous, reversible, or impossible.
  These three possibilities were discussed in Sec. <reference|3-spont and rev
  processes> and are summarized below.

  <\itemize>
    <item>A <index|Spontaneous process><subindex|Process|spontaneous><em|spontaneous>
    process is a real process that can actually take place in a finite time
    period.

    <item>A <subindex|Reversible|process><subindex|Process|reversible><em|reversible>
    process is an imaginary, idealized process in which the system passes
    through a continuous sequence of equilibrium states. This sequence of
    states can be approached by a spontaneous process in the limit of
    infinite slowness, and so also can the reverse sequence of states.

    <item>An <subindex|Process|impossible><em|impossible><label|impossible
    process defn>process is a change that cannot occur under the existing
    conditions, even in a limiting sense. It is also known as an unnatural or
    disallowed process. Sometimes it is useful to describe a hypothetical
    impossible process that we can imagine but that does not occur in
    reality. The second law of thermodynamics will presently be introduced
    with two such impossible processes.
  </itemize>

  The <subindex|Process|spontaneous><index|Spontaneous process>spontaneous
  processes relevant to chemistry are <subindex|Process|irreversible><index|Irreversible
  process><em|irreversible>. An irreversible process is a spontaneous process
  whose reverse is an impossible process.

  There is also the special category, of little interest to chemists, of
  purely mechanical processes. A purely mechanical process is a spontaneous
  process whose reverse is also spontaneous.

  It is true that <subindex|Process|reversible><subindex|Reversible|process>reversible
  processes and purely mechanical processes are idealized processes that
  cannot occur in practice, but a spontaneous process can be <em|practically>
  reversible if carried out sufficiently slowly, or <em|practically> purely
  mechanical if friction and temperature gradients are negligible. In that
  sense, they are not impossible processes. This book will reserve the term
  \Pimpossible\Q for a process that cannot be approached by any spontaneous
  process, no matter how slowly or how carefully it is carried out.

  <section|Statements of the Second Law><label|4-second law
  statements><label|c4 sec second-law>

  A description of the <subindex|Second law of thermodynamics|mathematical
  statement><newterm|mathematical statement of the second law> is given in
  the box below.<label|second law box>

  <\framed>
    \;

    <\wide-tabular>
      <tformat|<table|<row|<\cell>
        \;
      </cell>|<\cell>
        <math|<dvar|S>=<frac|<dbar|q>|T<rsub|b>>> for a reversible change of
        a closed system;
      </cell>>|<row|<\cell>
        \;
      </cell>|<\cell>
        <math|<dvar|S>\<gtr\><frac|<dbar|q>|T<rsub|b>>> for an irreversible
        change of a closed system;
      </cell>>>>
    </wide-tabular>

    where <math|S> is an extensive state function, the entropy, and
    <math|<dbar|q>> is an infinitesimal quantity of energy transferred by
    heat at a portion of the boundary where the thermodynamic temperature is
    <math|T<rsub|b>>.

    \;
  </framed>

  \;

  The box includes three distinct parts. First, there is the assertion that a
  property called <index|Entropy><newterm|entropy>, <math|S>, is an extensive
  state function. Second, there is an equation for calculating the entropy
  change of a closed system during a <subindex|Reversible|process><subindex|Process|reversible>reversible
  change of state: <math|<dvar|S>> is equal to <label|T replaces
  T_b><math|<dbar|q>/T<rsub|<text|b>>>.<footnote|During a reversible process,
  the temperature usually has the same value <math|T> throughout the system,
  in which case we can simply write <math|<dvar|S>=<dbar|q>/T>. The equation
  <math|<dvar|S>=<dbar|q>/T<rsub|<text|b>>> allows for the possibility that
  in an equilibrium state the system has phases of different temperatures
  separated by internal adiabatic partitions.> Third, there is a criterion
  for spontaneity: <math|<dvar|S>> is greater than
  <math|<dbar|q>/T<rsub|<text|b>>> during an irreversible change of state.
  The temperature <math|T<rsub|<text|b>>> is a thermodynamic temperature,
  which will be defined in Sec. <reference|4-thermodynamic temperature>.

  Each of the three parts is an essential component of the second law, but is
  somewhat abstract. What fundamental principle, based on experimental
  observation, may we take as the starting point to obtain them? Two
  principles are available, one associated with <index|Clausius,
  Rudolf>Clausius and the other with <index|Kelvin, Baron of Largs>Kelvin and
  <index|Planck, Max>Planck. Both principles are equivalent statements of the
  second law. Each asserts that a certain kind of process is impossible, in
  agreement with common experience.

  <index-complex|<tuple|process|impossible>||c4 sec second-law proc-impos
  idx1|<tuple|Process|impossible>>Consider the process depicted in Fig.
  <reference|fig:4-impossible1>(a) <pageref|fig:4-impossible1>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|04-SUP/IMPOSS-1.eps|172pt|131pt||>>
        <label|fig:4-impossible1>Two impossible processes in isolated
        systems.

        \ (a)<nbsp>Heat transfer from a cool to a warm body.

        \ (b)<nbsp>The same, with a device that operates in a cycle.
      </big-figure>
    </framed>
  </float>

  The system is isolated, and consists of a cool body in thermal contact with
  a warm body. During the process, energy is transferred by means of heat
  from the cool to the warm body, causing the temperature of the cool body to
  decrease and that of the warm body to increase. Of course, this process is
  impossible; we never observe heat flow from a cooler to a warmer body. (In
  contrast, the reverse process, heat transfer from the warmer to the cooler
  body, is spontaneous and irreversible.) Note that this impossible process
  does not violate the first law, because energy is conserved.

  Suppose we attempt to bring about the same changes in the two bodies by
  interposing a device of some sort between them, as depicted in Fig.
  <reference|fig:4-impossible1>(b). Here is how we would like the device to
  operate in the isolated system: Heat should flow from the cool body to the
  device, an equal quantity of heat should flow from the device to the warm
  body, and the final state of the device should be the same as its initial
  state. In other words, we want the device to transfer energy quantitatively
  by means of heat from the cool body to the warm body while operating in a
  <em|cycle>. If the device could do this, there would be no limit to the
  quantity of energy that could be transferred by heat, because after each
  cycle the device would be ready to repeat the process. But experience shows
  that <em|it is impossible to build such a device>! The proposed process of
  Fig. <reference|fig:4-impossible1>(b) is impossible even in the limit of
  infinite slowness.

  The general principle was expressed by <index|Clausius, Rudolf>Rudolph
  Clausius<footnote|Ref. <cite|clausius-1854>, page 117.> in the words:
  \PHeat can never pass from a colder to a warmer body without some other
  change, connected therewith, occurring at the same time.\Q For use in the
  derivation to follow, the statement can be reworded as
  follows.<label|4-Clausius statement>

  <\description-paragraphs>
    <item*|The Clausius statement of the second
    law:><label|4-Clausius><subindex|Clausius|statement of the second
    law><subindex|Second law of thermodynamics|Clausius statement>It is
    impossible to construct a device whose only effect, when it operates in a
    cycle, is heat transfer from a body to the device and the transfer by
    heat of an equal quantity of energy from the device to a warmer body.
  </description-paragraphs>

  Next consider the impossible process shown in Fig.
  <reference|fig:4-impossible2>(a) <pageref|fig:4-impossible2>.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|04-SUP/IMPOSS-2.eps|256pt|122pt||>
      <|big-figure>
        <label|fig:4-impossible2>Two more impossible processes.

        \ (a)<nbsp>A weight rises as a liquid becomes cooler.

        \ (b)<nbsp>The same, with a heat engine.
      </big-figure>
    </framed>
  </float>

  A <subindex|Joule|paddle wheel><subindex|Paddle wheel|Joule>Joule paddle
  wheel rotates in a container of water as a weight rises. As the weight
  gains potential energy, the water loses thermal energy and its temperature
  decreases. Energy is conserved, so there is no violation of the first law.
  This process is just the reverse of the Joule paddle-wheel experiment (Sec.
  <reference|3-Joule paddle wheel>) and its impossibility was discussed on
  page <pageref|reverse paddle wheel>.

  We might again attempt to use some sort of device operating in a cycle to
  accomplish the same overall process, as in Fig.
  <reference|fig:4-impossible2>(b). A closed system that operates in a cycle
  and does net work on the surroundings is called a <index|Heat
  engine><newterm|heat engine>. The heat engine shown in Fig.
  <reference|fig:4-impossible2>(b) is a special one. During one cycle, a
  quantity of energy is transferred by heat from a
  <subindex|Heat|reservoir>heat reservoir to the engine, and the engine
  performs an <em|equal> quantity of work on a weight, causing it to rise. At
  the end of the cycle, the engine has returned to its initial state. This
  would be a very desirable engine, because it could convert thermal energy
  into an equal quantity of useful mechanical work with no other effect on
  the surroundings.<footnote|This hypothetical process is called
  <index|Perpetual motion of the second kind>\Pperpetual motion of the second
  kind.\Q> The engine could power a ship; it would use the ocean as a
  <subindex|Heat|reservoir>heat reservoir and require no fuel. Unfortunately,
  <em|it is impossible to construct such a heat engine>!

  The principle was expressed by <index|Thomson, William>William Thomson
  <index|Kelvin, Baron of Largs>(Lord Kelvin) in 1852 as follows: \PIt is
  impossible by means of inanimate material agency to derive mechanical
  effect from any portion of matter by cooling it below the temperature of
  the coldest of the surrounding objects.\Q <index|Planck, Max>Max
  Planck<footnote|Ref. <cite|planck-22>, p. 89.> gave this statement: \PIt is
  impossible to construct an engine which will work in a complete cycle, and
  produce no effect except the raising of a weight and the cooling of a
  heat-reservoir.\Q For the purposes of this chapter, the principle can be
  reworded as follows.

  <\description-paragraphs>
    <item*|The Kelvin\UPlanck statement of the second
    law:><index|Kelvin--Planck statement of the second law><subindex|Second
    law of thermodynamics|Kelvin--Planck statement>It is impossible to
    construct a heat engine whose only effect, when it operates in a cycle,
    is heat transfer from a <subindex|Heat|reservoir>heat reservoir to the
    engine and the performance of an equal quantity of work on the
    surroundings.
  </description-paragraphs>

  Both the Clausius statement and the Kelvin\UPlanck statement assert that
  certain processes, although they do not violate the first law, are
  nevertheless <em|impossible>.

  <\quote-env>
    These processes would not be impossible if we could control the
    trajectories of large numbers of individual particles. Newton's laws of
    motion are invariant to time reversal. Suppose we could measure the
    position and velocity of each molecule of a macroscopic system in the
    final state of an irreversible process. Then, if we could somehow arrange
    at one instant to place each molecule in the same position with its
    velocity reversed, and if the molecules behaved classically, they would
    retrace their trajectories in reverse and we would observe the reverse
    ``impossible'' process.
  </quote-env>

  <index-complex|<tuple|process|impossible>||c4 sec second-law proc-impos
  idx1|<tuple|Process|impossible>>The plan of the remaining sections of this
  chapter is as follows. In Sec. <reference|4-Carnot engines>, a hypothetical
  device called a Carnot engine is introduced and used to prove that the two
  physical statements of the second law (the Clausius statement and the
  Kelvin\UPlanck statement) are equivalent, in the sense that if one is true,
  so is the other. An expression is also derived for the efficiency of a
  Carnot engine for the purpose of defining thermodynamic temperature.
  Section <reference|4-rev processes> combines Carnot cycles and the
  Kelvin\UPlanck statement to derive the existence and properties of the
  state function called entropy. Section <reference|4-irrev processes> uses
  irreversible processes to complete the derivation of the mathematical
  statements given in the box on page <pageref|second law box>, Sec.
  <reference|4-applications> describes some applications, and Sec.
  <reference|4-summary> is a summary. Finally, Sec. <reference|4-statistical>
  briefly describes a microscopic, statistical interpretation of entropy.

  <\quote-env>
    Carnot engines and Carnot cycles are admittedly outside the normal
    experience of chemists, and using them to derive the mathematical
    statement of the second law may seem arcane. <index|Lewis, Gilbert
    Newton>G. N. Lewis and <index|Randall, Merle>M. Randall, in their classic
    1923 book <em|Thermodynamics and the Free Energy of Chemical
    Substances>,<footnote|Ref. <cite|lewis-23>, p. 2.> complained of the
    presentation of \P 'cyclical processes' limping about eccentric and not
    quite completed cycles.\Q There seems, however, to be no way to carry out
    a rigorous <em|general> derivation without invoking thermodynamic cycles.
    You may avoid the details by skipping Secs. <reference|4-Carnot
    engines>\U<reference|4-irrev processes>. (Incidently, the cycles
    described in these sections are complete!)
  </quote-env>

  <\bio-insert>
    <include|bio-CARNOT.tm>
  </bio-insert>

  <section|Concepts Developed with Carnot Engines><label|4-Carnot
  engines><label|c4 sec concepts>

  <subsection|Carnot engines and Carnot cycles><label|c4 sec concepts-carnot>

  <index-complex|<tuple|carnot|engine>||c4 sec concepts-carnot
  idx1|<tuple|Carnot|engine>><index-complex|<tuple|carnot|cycle>||c4 sec
  concepts-carnot idx2|<tuple|Carnot|cycle>>A <index|Heat engine>heat engine,
  as mentioned in Sec. <reference|4-second law statements>, is a closed
  system that converts heat to work and operates in a cycle. A
  <subindex|Carnot|engine><newterm|Carnot engine> is a particular kind of
  heat engine, one that performs <subindex|Carnot|cycle><newterm|Carnot
  cycles> with a <index|Working substance>working substance. A Carnot cycle
  has four reversible steps, alternating isothermal and adiabatic; see the
  examples in Figs. <reference|fig:4-ig Carnot engine><\float|float|hb>
    <\framed>
      <\big-figure>
        <image|04-SUP/CARNOT.eps|215pt|177pt||>
      <|big-figure>
        <label|fig:4-ig Carnot engine>Indicator diagram for a Carnot engine
        using an ideal gas as the working substance. In this example,
        <math|T<rsub|<text|h>>=400 <text|K>>, <math|T<rsub|<text|c>>=300
        <text|K>>, <math|\<epsilon\>=1/4>,
        <math|C<rsub|V,m>=<around|(|3/2|)>*R>, <math|n=2.41 <text|mmol>>. The
        processes of paths A<math|<ra>>B and C<math|<ra>>D are isothermal;
        those of paths B<math|<ra>>C, B<math|<rprime|'><ra>>C<math|<rprime|'>>,
        and D<math|<ra>>A are adiabatic. The cycle
        A<math|<ra>>B<math|<ra>>C<math|<ra>>D<math|<ra>>A has net work
        <math|w=-1.0 <text|J>>; the cycle
        A<math|<ra>>B<math|<rprime|'><ra>>C<math|<rprime|'><ra>>D<math|<ra>>A
        has net work <math|w=-0.5 <text|J>>.
      </big-figure>
    </framed>
  </float> and <reference|fig:4-water Carnot engine><\float|float|hb>
    <\framed>
      <\big-figure>
        <image|04-SUP/WATERCYL.eps|152pt|144pt||>
      <|big-figure>
        <label|fig:4-water Carnot engine>Indicator diagram for a Carnot
        engine using H<rsub|<math|2>>O as the working substance. In this
        example, <math|T<rsub|<text|h>>=400 <text|K>>,
        <math|T<rsub|<text|c>>=396 <text|K>>, <math|\<epsilon\>=1/100>,
        <math|w=-1.0 <text|J>>. In state A, the system consists of one mole
        of H<rsub|<math|2>>O(l). The processes (all carried out reversibly)
        are: A<math|<ra>>B, vaporization of <math|2.54 <text|mmol>>
        H<rsub|<math|2>>O at <math|400 <text|K>>; B<math|<ra>>C, adiabatic
        expansion, causing vaporization of an additional <math|7.68
        <text|mmol>>; C<math|<ra>>D, condensation of <math|2.50 <text|mmol>>
        at <math|396 <text|K>>; D<math|<ra>>A, adiabatic compression
        returning the system to the initial state.
      </big-figure>
    </framed>
  </float> in which the working substances are an ideal gas and
  H<rsub|<math|2>>O, respectively.

  The steps of a Carnot cycle are as follows. In this description, the
  <em|system> is the working substance.

  <\itemize>
    <item>Path A<math|<ra>>B: A quantity of heat <math|q<rsub|<text|h>>> is
    transferred reversibly and isothermally from a
    <subindex|Heat|reservoir>heat reservoir (the ``hot'' reservoir) at
    temperature <math|T<rsub|<text|h>>> to the system, also at temperature
    <math|T<rsub|<text|h>>>. <math|q<rsub|<text|h>>> is positive because
    energy is transferred into the system.

    <item>Path B<math|<ra>>C: The system undergoes a reversible adiabatic
    change that does work on the surroundings and reduces the system
    temperature to <math|T<rsub|<text|c>>>.

    <item>Path C<math|<ra>>D: A quantity of heat <math|q<rsub|<text|c>>> is
    transferred reversibly and isothermally from the system to a second
    <subindex|Heat|reservoir>heat reservoir (the \Pcold\Q reservoir) at
    temperature <math|T<rsub|<text|c>>>. <math|q<rsub|<text|c>>> is negative.

    <item>Path D<math|<ra>>A: The system undergoes a reversible adiabatic
    change in which work is done on the system, the temperature returns to
    <math|T<rsub|<text|h>>>, and the system returns to its initial state to
    complete the cycle.
  </itemize>

  In one cycle, a quantity of heat is transferred from the hot reservoir to
  the system, a portion of this energy is transferred as heat to the cold
  reservoir, and the remainder of the energy is the negative net work
  <math|w> done on the surroundings. (It is the heat transfer to the cold
  reservoir that keeps the Carnot engine from being an impossible
  Kelvin\UPlanck engine.) Adjustment of the length of path A<math|<ra>>B
  makes the magnitude of <math|w> as large or small as desired\Vnote the two
  cycles with different values of <math|w> described in the caption of Fig.
  <reference|fig:4-ig Carnot engine>.

  <\quote-env>
    The Carnot engine is an idealized heat engine because its paths are
    reversible processes. It does not resemble the design of any practical
    <index|Steam engine>steam engine. In a typical working steam engine, such
    as those once used for motive power in train locomotives and steamships,
    the cylinder contains an <em|open> system that undergoes the following
    irreversible steps in each cycle: (1) high-pressure steam enters the
    cylinder from a boiler and pushes the piston from the closed end toward
    the open end of the cylinder; (2) the supply valve closes and the steam
    expands in the cylinder until its pressure decreases to atmospheric
    pressure; (3) an exhaust valve opens to release the steam either to the
    atmosphere or to a condenser; (4) the piston returns to its initial
    position, driven either by an external force or by suction created by
    steam condensation.
  </quote-env>

  The energy transfers involved in one cycle of a Carnot engine are shown
  schematically in Fig. <reference|fig:4-one cycle>(a) on page
  <pageref|fig:4-one cycle>.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|04-SUP/ONECYCLE.eps|129pt|104pt||>
      <|big-figure>
        <label|fig:4-one cycle>(a)<nbsp>One cycle of a Carnot engine that
        does work on the surroundings.

        \ (b)<nbsp>The same system run in reverse as a Carnot heat pump.

        \ Figures <reference|fig:4-one cycle>--<reference|fig:4-impossible4>
        use the following symbols: A square box represents a system (a Carnot
        engine or Carnot heat pump). Vertical arrows indicate heat and
        horizontal arrows indicate work; each arrow shows the direction of
        energy transfer into or out of the system. The number next to each
        arrow is an absolute value of <math|q>/J or <math|w>/J in the cycle.
        For example, (a) shows <math|4> joules of heat transferred to the
        system from the hot reservoir, <math|3> joules of heat transferred
        from the system to the cold reservoir, and <math|1> joule of work
        done by the system on the surroundings.
      </big-figure>
    </framed>
  </float>

  When the cycle is reversed, as shown in Fig. <reference|fig:4-one
  cycle>(b), the device is called a <subindex|Carnot|heat
  pump><newterm|Carnot heat pump>. In each cycle of a Carnot heat pump,
  <math|q<rsub|<text|h>>> is negative and <math|q<rsub|<text|c>>> is
  positive. Since each step of a Carnot engine or Carnot heat pump is a
  reversible process, neither device is an impossible device.

  <index-complex|<tuple|carnot|engine>||c4 sec concepts-carnot
  idx1|<tuple|Carnot|engine>><index-complex|<tuple|carnot|cycle>||c4 sec
  concepts-carnot idx2|<tuple|Carnot|cycle>>

  <subsection|The equivalence of the Clausius and Kelvin\UPlanck
  statements><label|c4 sec concepts-clausius-kelvin-planck>

  <index-complex|<tuple|second law of thermodynamics|equivalence of clausius
  and kelvin-planck statements>||c4 sec concepts-clausius-kelvin-planck
  idx1|<tuple|Second law of thermodynamics|equivalence of Clausius and
  Kelvin\UPlanck statements>>We can use the logical tool of <em|reductio ad
  absurdum> to prove the equivalence of the Clausius and Kelvin\UPlanck
  statements of the second law.

  <\bio-insert>
    <include|bio-CLAUSIUS.tm>
  </bio-insert>

  Let us assume for the moment that the Clausius statement is incorrect, and
  that the device the Clausius statement claims is impossible (a \PClausius
  device\Q) is actually possible. If the Clausius device is possible, then we
  can combine one of these devices with a Carnot engine as shown in Fig.
  <reference|fig:4-impossible3>(a) on page <pageref|fig:4-impossible3>. We
  adjust the cycles of the Clausius device and Carnot engine to transfer
  equal quantities of heat from and to the cold reservoir. The combination of
  the Clausius device and Carnot engine is a system. When the Clausius device
  and Carnot engine each performs one cycle, the system has performed one
  cycle as shown in Fig. <reference|fig:4-impossible3>(b). There has been a
  transfer of heat into the system and the performance of an equal quantity
  of work on the surroundings, with no other net change. This system is a
  heat engine that according to the Kelvin\UPlanck statement is impossible.

  Thus, if the Kelvin\UPlanck statement is correct, it is impossible to
  operate the Clausius device as shown, and our provisional assumption that
  the Clausius statement is incorrect must be wrong. In conclusion, if the
  Kelvin\UPlanck statement is correct, then the Clausius statement must also
  be correct.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|04-SUP/IMPOSS-3.eps|359pt|121pt||>
      <|big-figure>
        <label|fig:4-impossible3>(a)<nbsp>A Clausius device combined with the
        Carnot engine of Fig. <reference|fig:4-one cycle>(a).

        \ (b)<nbsp>The resulting impossible Kelvin--Planck engine.

        \ (c)<nbsp>A Kelvin--Planck engine combined with the Carnot heat pump
        of Fig. <reference|fig:4-one cycle>(b).

        \ (d)<nbsp>The resulting impossible Clausius device.
      </big-figure>
    </framed>
  </float>

  We can apply a similar line of reasoning to the heat engine that the
  Kelvin\UPlanck statement claims is impossible (a \PKelvin\UPlanck engine\Q)
  by seeing what happens if we assume this engine is actually possible. We
  combine a Kelvin\UPlanck engine with a Carnot heat pump, and make the work
  performed on the Carnot heat pump in one cycle equal to the work performed
  by the Kelvin\UPlanck engine in one cycle, as shown in Fig.
  <reference|fig:4-impossible3>(c). One cycle of the combined system, shown
  in Fig. <reference|fig:4-impossible3>(d), shows the system to be a device
  that the Clausius statement says is impossible. We conclude that if the
  Clausius statement is correct, then the Kelvin\UPlanck statement must also
  be correct.

  These conclusions complete the proof that the Clausius and Kelvin\UPlanck
  statements are equivalent: the truth of one implies the truth of the other.
  We may take either statement as the fundamental physical principle of the
  second law, and use it as the starting point for deriving the mathematical
  statement of the second law. The derivation will be taken up in Sec.
  <reference|4-rev processes>.<index-complex|<tuple|second law of
  thermodynamics|equivalence of clausius and kelvin-planck statements>||c4
  sec concepts-clausius-kelvin-planck idx1|<tuple|Second law of
  thermodynamics|equivalence of Clausius and Kelvin\UPlanck statements>>

  <subsection|The efficiency of a Carnot engine><label|c4
  sec-concepts-carnot-efficiency>

  Integrating the first-law equation <math|<dvar|U>=<dbar|q>+<dbar|w>> over
  one cycle of a Carnot engine, we obtain

  <equation-cov2|<label|0=qh+qc+w>0=q<rsub|<text|h>>+q<rsub|<text|c>>+w|(one
  cycle of a Carnot engine)>

  The <index-complex|<tuple|efficiency|heat engine>|||<tuple|Efficiency|of a
  heat engine>><newterm|efficiency> <math|\<epsilon\>> of a heat engine is
  defined as the fraction of the heat input <math|q<rsub|<text|h>>> that is
  returned as net work done on the surroundings:

  <\equation>
    \<epsilon\><bk-equal-def><frac|-w|q<rsub|<text|h>>>
  </equation>

  By substituting for <math|w> from Eq. <reference|0=qh+qc+w>, we obtain

  <equation-cov2|<label|epsilon=1+qc/qh>\<epsilon\>=1+<frac|q<rsub|<text|c>>|q<rsub|<text|h>>>|(Carnot
  engine)>

  Because <math|q<rsub|<text|c>>> is negative, <math|q<rsub|<text|h>>> is
  positive, and <math|q<rsub|<text|c>>> is smaller in magnitude than
  <math|q<rsub|<text|h>>>, the efficiency is less than
  one.<label|efficiency\<less\>1>The example shown in Fig.
  <reference|fig:4-one cycle>(a) is a Carnot engine with
  <math|\<epsilon\>=1/4>.

  <index-complex|<tuple|efficiency|carnot engine>||c4
  sec-concepts-carnot-efficiency idx1|<tuple|Efficiency|of a Carnot
  engine>>We will be able to reach an important conclusion regarding
  efficiency by considering a Carnot engine operating between the
  temperatures <math|T<rsub|<text|h>>> and <math|T<rsub|<text|c>>>, combined
  with a Carnot heat pump operating between the same two temperatures. The
  combination is a supersystem, and one cycle of the engine and heat pump is
  one cycle of the supersystem. We adjust the cycles of the engine and heat
  pump to produce zero net work for one cycle of the supersystem.

  Could the efficiency of the Carnot engine be different from the efficiency
  the heat pump would have when run in reverse as a Carnot engine? If so,
  either the supersystem is an impossible Clausius device as shown in Fig.
  <reference|fig:4-impossible4>(b) on page
  <pageref|fig:4-impossible4>,<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|04-SUP/IMPOSS-4.eps|359pt|121pt||>
      <|big-figure>
        <label|fig:4-impossible4>(a)<nbsp>A Carnot engine of efficiency
        <math|\<epsilon\>=1/4> combined with a Carnot engine of efficiency
        <math|\<epsilon\>=1/5> run in reverse.

        \ (b)<nbsp>The resulting impossible Clausius device.

        \ (c)<nbsp>A Carnot engine of efficiency <math|\<epsilon\>=1/3>
        combined with the Carnot engine of efficiency <math|\<epsilon\>=1/4>
        run in reverse.

        \ (d)<nbsp>The resulting impossible Clausius device.
      </big-figure>
    </framed>
  </float>

  or the supersystem operated in reverse (with the engine and heat pump
  switching roles) is an impossible Clausius device as shown in Fig.
  <reference|fig:4-impossible4>(d). We conclude that <em|all Carnot engines
  operating between the same two temperatures have the same efficiency>.

  <\quote-env>
    This is a good place to pause and think about the meaning of this
    statement in light of the fact that the steps of a Carnot engine, being
    reversible changes, cannot take place in a real system (Sec.
    <reference|3-spont and rev processes>). How can an engine operate that is
    not real? The statement is an example of a common kind of thermodynamic
    shorthand. To express the same idea more accurately, one could say that
    all heat engines (real systems) operating between the same two
    temperatures have the same <em|limiting> efficiency, where the limit is
    the reversible limit approached as the steps of the cycle are carried out
    more and more slowly. You should interpret any statement involving a
    reversible process in a similar fashion: a reversible process is an
    idealized <em|limiting> process that can be approached but never quite
    reached by a real system.
  </quote-env>

  Thus, the efficiency of a Carnot engine must depend only on the values of
  <math|T<rsub|<text|c>>> and <math|T<rsub|<text|h>>> and not on the
  properties of the working substance. Since the efficiency is given by
  <math|\<epsilon\>=1+q<rsub|<text|c>>/q<rsub|<text|h>>>, the ratio
  <math|q<rsub|<text|c>>/q<rsub|<text|h>>> must be a unique function of
  <math|T<rsub|<text|c>>> and <math|T<rsub|<text|h>>> only. To find this
  function for temperatures on the ideal-gas temperature scale, it is
  simplest to choose as the working substance an ideal gas.

  An ideal gas has the equation of state <math|p*V=n*R*T>. Its internal
  energy change in a closed system is given by
  <math|<dvar|U>=C<rsub|V>*<dvar|T>> (Eq. <reference|dU=C_V dT (ig)>), where
  <math|C<rsub|V>> (a function only of <math|T>) is the heat capacity at
  constant volume. Reversible expansion work is given by
  <math|<dbar|w>=-p*<dvar|V>>, which for an ideal gas becomes
  <math|<dbar|w>=-<around|(|<frac|n*R*T|V>|)>*<dvar|V>>. Substituting these
  expressions for <math|<dvar|U>> and <math|<dbar|w>> in the first law,
  <math|<dvar|U>=<dbar|q>+<dbar|w>>, and solving for <math|<dbar|q>>, we
  obtain

  <\equation-cov2|<label|dq=CVdT+(nRT/V)dV><dbar|q=C<rsub|V>*<dvar|T>+<frac|n*R*T|V>*<dvar|V>>>
    (ideal gas, reversible

    expansion work only)
  </equation-cov2>

  Dividing both sides by <math|T> gives

  <\equation-cov2|<label|dq/T=CVdT/T+nRdV/V><frac|<dbar|q>|T>=<frac|C<rsub|V>*<dvar|T>|T>+n*R*<frac|<dvar|V>|V>>
    (ideal gas, reversible

    expansion work only)
  </equation-cov2>

  In the two adiabatic steps of the Carnot cycle, <math|<dbar|q>> is zero. We
  obtain a relation among the volumes of the four labeled states shown in
  Fig. <reference|fig:4-ig Carnot engine> by integrating Eq.
  <reference|dq/T=CVdT/T+nRdV/V> over these steps and setting the integrals
  equal to zero:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<tabular|<tformat|<table|<row|<cell|<text|Path
    <math|<text|B>\<rightarrow\><text|C>>:>>|<cell|<space|2em>>|<cell|<big|int><frac|<dbar|q>|T>>>>>>>|<cell|=>|<cell|<big|int><rsub|T<rsub|<text|h>>><rsup|T<rsub|<text|c>>><frac|C<rsub|V>*<dvar|T>|T>+n*R*<text|ln>
    <frac|V<rsub|<text|C>>|V<rsub|<text|B>>>=0<eq-number>>>|<row|<cell|<tabular|<tformat|<table|<row|<cell|<text|Path
    <math|<text|D>\<rightarrow\><text|A>>:>>|<cell|<space|2em>>|<cell|<big|int><frac|<dbar|q>|T>>>>>>>|<cell|=>|<cell|<big|int><rsub|T<rsub|<text|c>>><rsup|T<rsub|<text|h>>><frac|C<rsub|V>*<dvar|T>|T>+n*R*<text|ln>
    <frac|V<rsub|<text|A>>|V<rsub|<text|D>>>=0<eq-number>>>>>
  </eqnarray*>

  Adding these two equations (the integrals shown with limits cancel) gives
  the relation

  <\equation>
    n*R*<text|ln> <frac|V<rsub|<text|A>>V<rsub|<text|c>>|V<rsub|<text|b>>V<rsub|<text|D>>>=0
  </equation>

  which we can rearrange to

  <equation-cov2|<label|ln(V2/V1)=-ln(V4/V3)><text|ln><around*|(|<frac|V<rsub|<text|B>>|V<rsub|<text|A>>>|)>=-<text|ln>
  <around*|(|<frac|V<rsub|<text|D>>|V<rsub|<text|C>>>|)>|(ideal gas, Carnot
  cycle)>

  We obtain expressions for the heat in the two isothermal steps by
  integrating Eq. <reference|dq=CVdT+(nRT/V)dV> with <math|<dvar|T>> set
  equal to 0.

  <\eqnarray*>
    <tformat|<table|<row|<cell|<tabular|<tformat|<table|<row|<cell|<text|Path
    <math|<text|A>\<rightarrow\><text|B>>:>>|<cell|<space|2em>>|<cell|q<rsub|<text|h>>>>>>>>|<cell|=>|<cell|n*R*T<rsub|<text|h>>*<text|ln>
    <around*|(|<frac|V<rsub|<text|B>>|V<rsub|<text|A>>>|)><eq-number>>>|<row|<cell|<tabular|<tformat|<table|<row|<cell|<text|Path
    <math|<text|C>\<rightarrow\><text|D>>:>>|<cell|<space|2em>>|<cell|q<rsub|<text|c>>>>>>>>|<cell|=>|<cell|n*R*T<rsub|<text|c>>*<text|ln>
    <around*|(|<frac|V<rsub|<text|D>>|V<rsub|<text|C>>>|)><eq-number>>>>>
  </eqnarray*>

  The ratio of <math|q<rsub|<text|c>>> and <math|q<rsub|<text|h>>> obtained
  from these expressions is

  <\equation>
    <frac|q<rsub|<text|c>>|q<rsub|<text|h>>>=<frac|T<rsub|<text|c>>|T<rsub|<text|h>>>\<times\><frac|ln
    <around|(|V<rsub|<text|D>>/V<rsub|<text|c>>|)>|ln
    <around|(|V<rsub|<text|b>>/V<rsub|<text|A>>|)>>
  </equation>

  By means of Eq. <reference|ln(V2/V1)=-ln(V4/V3)>, this ratio becomes

  <equation-cov2|<label|qc/qh=-Tc/Th><frac|q<rsub|<text|c>>|q<rsub|<text|h>>>=-<frac|T<rsub|<text|c>>|T<rsub|<text|h>>>|(Carnot
  cycle)>

  Accordingly, the unique function of <math|T<rsub|<text|c>>> and
  <math|T<rsub|<text|h>>> we seek that is equal to
  <math|q<rsub|<text|c>>/q<rsub|<text|h>>> is the ratio
  <math|-T<rsub|<text|c>>/T<rsub|<text|h>>>. The efficiency, from Eq.
  <reference|epsilon=1+qc/qh>, is then given by

  <equation-cov2|<label|epsilon=1-Tc/Th>\<epsilon\>=1-<frac|T<rsub|<text|c>>|T<rsub|<text|h>>>|(Carnot
  engine)>

  \ Eqs. <reference|qc/qh=-Tc/Th> and <reference|epsilon=1-Tc/Th>,
  <math|T<rsub|<text|c>>> and <math|T<rsub|<text|h>>> are temperatures on the
  ideal-gas scale. As we have seen, these equations must be valid for
  <index|Working substance><em|any> working substance; it is not necessary to
  specify as a condition of validity that the system is an ideal gas.

  The ratio <math|T<rsub|<text|c>>/T<rsub|<text|h>>> is positive but less
  than one, so the efficiency is less than one as deduced earlier on page
  <pageref|efficiency\<less\>1>. This conclusion is an illustration of the
  Kelvin\UPlanck statement of the second law: A heat engine cannot have an
  efficiency of unity\Vthat is, it cannot in one cycle convert all of the
  energy transferred by heat from a single <subindex|Heat|reservoir>heat
  reservoir into work. The example shown in Fig. <vpageref|fig:4-one cycle>,
  with <math|\<epsilon\>=1/4>, must have <math|T<rsub|<text|c>>/T<rsub|<text|h>>=3/4>
  (e.g., <math|T<rsub|<text|c>>=300 <text|K>> and <math|T<rsub|<text|h>>=400
  <text|K>>).

  Keep in mind that a Carnot engine operates <em|reversibly> between two heat
  reservoirs. The expression of Eq. <reference|epsilon=1-Tc/Th> gives the
  efficiency of this kind of idealized heat engine only. If any part of the
  cycle is carried out irreversibly, <subindex|Energy|dissipation
  of><index|Dissipation of energy>dissipation of mechanical energy will cause
  the efficiency to be <em|lower> than the theoretical value given by Eq.
  <reference|epsilon=1-Tc/Th>.<index-complex|<tuple|efficiency|carnot
  engine>||c4 sec-concepts-carnot-efficiency idx1|<tuple|Efficiency|of a
  Carnot engine>>

  <subsection|Thermodynamic temperature><label|4-thermodynamic
  temperature><label|c4 sec concepts-temperature>

  <index-complex|<tuple|thermodynamic|temperature>||c4 sec
  concepts-temperature idx1|<tuple|Thermodynamic|temperature>><index-complex|<tuple|temperature|thermodynamic>||c4
  sec concepts-temperature idx2|<tuple|Temperature|thermodynamic>>The
  negative ratio <math|q<rsub|<text|c>>/q<rsub|<text|h>>> for a Carnot cycle
  depends only on the temperatures of the two <subindex|Heat|reservoir>heat
  reservoirs. <index|Kelvin, Baron of Largs>Kelvin (1848) proposed that this
  ratio be used to establish an \Pabsolute\Q temperature scale. The physical
  quantity now called <subindex|Thermodynamic|temperature><subindex|Temperature|thermodynamic><newterm|thermodynamic
  temperature> is defined by the relation

  <equation-cov2|<label|Tc/Th=-qc/qh><frac|T<rsub|<text|c>>|T<rsub|<text|h>>>=-<frac|q<rsub|<text|c>>|q<rsub|<text|h>>>|(Carnot
  cycle)>

  That is, the ratio of the thermodynamic temperatures of two heat reservoirs
  is equal, by definition, to the ratio of the absolute quantities of heat
  transferred in the isothermal steps of a Carnot cycle operating between
  these two temperatures. In principle, a measurement of
  <math|q<rsub|<text|c>>/q<rsub|<text|h>>> during a Carnot cycle, combined
  with a defined value of the thermodynamic temperature of one of the heat
  reservoirs, can establish the thermodynamic temperature of the other heat
  reservoir. This defined value is provided by the triple point of
  H<rsub|<math|2>>O; its thermodynamic temperature is defined as exactly
  <math|273.16> kelvins (page <pageref|273.16K=water triple pt>).

  Just as measurements with a gas thermometer in the limit of zero pressure
  establish the ideal-gas temperature scale (Sec. <reference|2-gas
  thermometry>), the behavior of a heat engine in the reversible limit
  establishes the thermodynamic temperature scale. Note, however, that a
  reversible Carnot engine used as a \Pthermometer\Q to measure thermodynamic
  temperature is only a theoretical concept and not a practical instrument,
  since a completely-reversible process cannot occur in practice.

  It is now possible to justify the statement in Sec.
  <reference|2-temperature> that the <index-complex|<tuple|temperature|ideal-gas>|||<tuple|Temperature|ideal
  gas>><index-complex|<tuple|temperature|ideal
  gas>|||<tuple|Temperature|ideal-gas>><index|Ideal-gas temperature>ideal-gas
  temperature scale is proportional to the thermodynamic temperature scale.
  Both Eq.

  <reference|qc/qh=-Tc/Th> and Eq. <reference|Tc/Th=-qc/qh> equate the ratio
  <math|T<rsub|<text|c>>/T<rsub|<text|h>>> to
  <math|-q<rsub|<text|c>>/q<rsub|<text|h>>>; but whereas
  <math|T<rsub|<text|c>>> and <math|T<rsub|<text|h>>> refer in Eq.
  <reference|qc/qh=-Tc/Th> to the <index|Ideal-gas
  temperature><index-complex|<tuple|temperature|ideal
  gas>|||<tuple|Temperature|ideal-gas>><em|ideal-gas> temperatures of the
  <subindex|Heat|reservoir>heat reservoirs, in Eq. <reference|Tc/Th=-qc/qh>
  they refer to the <em|thermodynamic> temperatures. This means that the
  ratio of the ideal-gas temperatures of two bodies is equal to the ratio of
  the thermodynamic temperatures of the same bodies, and therefore the two
  scales are proportional to one another. The proportionality factor is
  arbitrary, but must be unity if the same unit (e.g., kelvins) is used in
  both scales. Thus, as stated on page <pageref|identical temperature
  scales>, the two scales expressed in kelvins are
  identical.<index-complex|<tuple|thermodynamic|temperature>||c4 sec
  concepts-temperature idx1|<tuple|Thermodynamic|temperature>><index-complex|<tuple|temperature|thermodynamic>||c4
  sec concepts-temperature idx2|<tuple|Temperature|thermodynamic>>

  \;

  <\bio-insert>
    <include|bio-KELVIN.tm>
  </bio-insert>

  <section|The Second Law for Reversible Processes><label|4-rev
  processes><label|c4 sec 2lr>

  This section derives the existence and properties of the state function
  called entropy. To begin, a useful relation called the Clausius inequality
  will be derived.

  <subsection|The Clausius inequality><label|4-Clausius inequality><label|c4
  sec 2lr-clausius>

  Consider an arbitrary cyclic process of a closed system. To avoid
  confusion, this system will be the \Pexperimental system\Q and the process
  will be the \Pexperimental process\Q or \Pexperimental cycle.\Q There are
  no restrictions on the contents of the experimental system\Vit may have any
  degree of complexity whatsoever. The experimental process may involve more
  than one kind of work, phase changes and reactions may occur, there may be
  temperature and pressure gradients, constraints and external fields may be
  present, and so on. All parts of the process must be either irreversible or
  reversible, but not impossible.

  We imagine that the experimental cycle is carried out in a special way that
  allows us to apply the Kelvin\UPlanck statement of the second law. The heat
  transferred across the boundary of the experimental system in each
  infinitesimal path element of the cycle is exchanged with a hypothetical
  Carnot engine. The combination of the experimental system and the Carnot
  engine is a closed <index|Supersystem><em|supersystem> (see Fig.
  <vpageref|fig:4-supersystem>).<\float|float|hb>
    <\framed>
      <\big-figure>
        <image|04-SUP/SUPERSYS.eps|228pt|149pt||>
      <|big-figure>
        <label|fig:4-supersystem>Experimental system, Carnot engine
        (represented by a small square box), and heat reservoir. The dashed
        lines indicate the boundary of the supersystem.

        \ (a)<nbsp>Reversible heat transfer between heat reservoir and Carnot
        engine.

        \ (b)<nbsp>Heat transfer between Carnot engine and experimental
        system. The infinitesimal quantities <math|<dbar|q><rprime|'>> and
        <math|<dbar|q>> are positive for transfer in the directions indicated
        by the arrows.
      </big-figure>
    </framed>
  </float>

  In the surroundings of the supersystem is a <subindex|Heat|reservoir>heat
  reservoir of arbitrary constant temperature <math|T<rsub|<text|res>>>. By
  allowing the supersystem to exchange heat with only this single heat
  reservoir, we will be able to apply the Kelvin\UPlanck statement to a cycle
  of the supersystem.<footnote|This procedure is similar to ones described in
  Ref. <cite|hats-65>, Sec. 16.1; Ref. <cite|pippard-66>, p. 36; Ref.
  <cite|pauli-73>, p. 21-23; Ref. <cite|adkins-83>, p. 68-69; and Ref.
  <cite|norton-16>.>

  We assume that we are able to control changes of the work coordinates of
  the experimental system from the surroundings of the supersystem. We are
  also able to control the Carnot engine from these surroundings, for example
  by moving the piston of a cylinder-and-piston device containing the working
  substance. Thus the energy transferred by <em|work> across the boundary of
  the experimental system, and the work required to operate the Carnot
  engine, is exchanged with the surroundings of the supersystem.

  During each stage of the experimental process with nonzero heat, we allow
  the Carnot engine to undergo many infinitesimal Carnot cycles with
  infinitesimal quantities of heat and work. In one of the isothermal steps
  of each Carnot cycle, the Carnot engine is in thermal contact with the
  <subindex|Heat|reservoir>heat reservoir, as depicted in Fig.
  <reference|fig:4-supersystem>(a). In this step the Carnot engine has the
  same temperature as the heat reservoir, and reversibly exchanges heat
  <math|<dbar|q><rprime|'>> with it. The sign convention is that
  <math|<dbar|q><rprime|'>> is positive if heat is transferred in the
  direction of the arrow, from the heat reservoir to the Carnot engine.

  In the other isothermal step of the Carnot cycle, the Carnot engine is in
  thermal contact with the experimental system at a portion of the system's
  boundary as depicted in Fig. <reference|fig:4-supersystem>(b). The Carnot
  engine now has the same temperature, <math|T<rsub|<text|b>>>, as the
  experimental system at this part of the boundary, and exchanges heat with
  it. The heat <math|<dbar|q>> is positive if the transfer is into the
  experimental system.

  The relation between temperatures and heats in the isothermal steps of a
  Carnot cycle is given by Eq. <reference|Tc/Th=-qc/qh>. From this relation
  we obtain, for one infinitesimal Carnot cycle, the relation
  <math|T<rsub|<text|b>>/T<rsub|<text|res>>=<dbar|q>/<dbar|q><rprime|'>>, or

  <\equation>
    <label|dq'=Tres(dq/Tb)><dbar|q><rprime|'>=T<rsub|<text|res>><frac|<dbar|q>|T<rsub|<text|b>>>
  </equation>

  After many infinitesimal Carnot cycles, the experimental cycle is complete,
  the experimental system has returned to its initial state, and the Carnot
  engine has returned to its initial state in thermal contact with the
  <subindex|Heat|reservoir>heat reservoir. Integration of Eq.
  <reference|dq'=Tres(dq/Tb)> around the experimental cycle gives the net
  heat entering the <index|Supersystem>supersystem during the process:

  <\equation>
    <label|q'=(Tres)int(dq/Tb)>q<rprime|'>=T<rsub|<text|res>>*<big|oint><frac|<dbar|q>|T<rsub|<text|b>>>
  </equation>

  The integration here is over each path element of the experimental process
  and over each surface element of the boundary of the experimental system.

  Keep in mind that the value of the cyclic integral
  <math|<big|oint><around|(|<dbar|q>/T<rsub|<text|b>>|)>> depends only on the
  path of the experimental cycle, that this process can be reversible or
  irreversible, and that <math|T<rsub|<text|res>>> is a positive constant.

  In this experimental cycle, could the net heat <math|q<rprime|'>>
  transferred to the <index|Supersystem>supersystem be positive? If so, the
  net work would be negative (to make the internal energy change zero) and
  the supersystem would have converted heat from a single
  <subindex|Heat|reservoir>heat reservoir completely into work, a process the
  <index|Kelvin--Planck statement of the second law><subindex|Second law of
  thermodynamics|Kelvin--Planck statement>Kelvin\UPlanck statement of the
  second law says is impossible. Therefore it is impossible for
  <math|q<rprime|'>> to be positive, and from Eq.
  <reference|q'=(Tres)int(dq/Tb)> we obtain the relation

  <equation-cov2|<label|oint(dq/Tb)\<less\>=0><big|oint><frac|<dbar|q>|T<rsub|<text|b>>>\<leq\>0|(cyclic
  process of a closed system)>

  This relation is known as the <subindex|Clausius|inequality><newterm|Clausius
  inequality>. It is valid only if the integration is taken around a cyclic
  path in a direction with nothing but reversible and irreversible
  changes\Vthe path must not include an impossible change, such as the
  reverse of an irreversible change. The Clausius inequality says that if a
  cyclic path meets this specification, it is impossible for the cyclic
  integral <math|<big|oint><around|(|<dbar|q>/T<rsub|<text|b>>|)>> to be
  positive.

  If the entire experimental cycle is adiabatic (which is only possible if
  the process is reversible), the Carnot engine is not needed and Eq.
  <reference|oint(dq/Tb)\<less\>=0> can be replaced by
  <math|<big|oint><around|(|<dbar|q>/T<rsub|<text|b>>|)>=0>.

  <subsection|Using reversible processes to define the entropy><label|4-rev
  processes to define entropy><label|c4 sec-2lr-entropy-reversible>

  <index-complex|<tuple|second law of thermodynamics|mathematical
  statement|derivation>||c4 sec-2lr-entropy-reversible idx1|<tuple|Second law
  of thermodynamics|mathematical statement|derivation>>Next let us
  investigate a <em|reversible> nonadiabatic process of the closed
  experimental system. Starting with a particular equilibrium state A, we
  carry out a reversible process in which there is a net flow of heat into
  the system, and in which <math|<dbar|q>> is either positive or zero in each
  path element. The final state of this process is equilibrium state B. Let
  <math|<dbar|q<rsub|<text|rev>>>> denote an infinitesimal quantity of heat
  in a reversible process. If <math|<dbar|q<rsub|<text|rev>>>> is positive or
  zero during the process, then the integral
  <math|<big|int><rsub|<text|A>><rsup|B><rsub|><rsup|><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  must be positive. In this case the Clausius inequality tells us that if the
  system completes a cycle by returning from state B back to state A by a
  different path, the integral <math|<big|int><rsup|<text|A>><rsub|<text|B>><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  for this second path must be negative. Therefore the change B<math|<ra>>A
  cannot be carried out by any <em|adiabatic> process.

  Any reversible process can be carried out in reverse. Thus, by reversing
  the reversible nonadiabatic process, it is possible to change the state
  from B to A by a reversible process with a net flow of heat out of the
  system and with <math|<dbar|q<rsub|<text|rev>>>> either negative or zero in
  each element of the reverse path. In contrast, the absence of an adiabatic
  path from B to A means that it is impossible to carry out the change
  A<math|<ra>>B by a reversible adiabatic process.

  The general rule, then, is that whenever equilibrium state A of a closed
  system can be changed to equilibrium state B by a reversible process with
  finite \Pone-way\Q heat (i.e., the flow of heat is either entirely into the
  system or else entirely out of it), it is impossible for the system to
  change from either of these states to the other by a reversible adiabatic
  process.

  <\quote-env>
    A simple example will relate this rule to experience. We can increase the
    temperature of a liquid by allowing heat to flow reversibly into the
    liquid. It is impossible to duplicate this change of state by a
    reversible process without heat<emdash>that is, by using some kind of
    reversible work. The reason is that reversible work involves the change
    of a work coordinate that brings the system to a different final state.
    There is nothing in the rule that says we can't increase the temperature
    <em|irreversibly> without heat, as we can for instance with stirring
    work.
  </quote-env>

  tes A and B can be arbitrarily close. We conclude that <em|every
  equilibrium state of a closed system has other equilibrium states
  infinitesimally close to it that are inaccessible by a reversible adiabatic
  process>. This is <index|Carath�odory's principle of adiabatic
  inaccessibility>Carath�odory's principle of adiabatic
  inaccessibility.<footnote|Constantin Carath�odory in 1909 combined this
  principle with a mathematical theorem (Carath�odory's theorem) to deduce
  the existence of the entropy function. The derivation outlined here avoids
  the complexities of that mathematical treatment and leads to the same
  results.>

  Next let us consider the reversible adiabatic processes that <em|are>
  possible. To carry out a reversible adiabatic process, starting at an
  initial equilibrium state, we use an adiabatic boundary and slowly vary one
  or more of the <subindex|Work|coordinate>work coordinates. A certain final
  temperature will result. It is helpful in visualizing this process to think
  of an <math|N><space|-.18em>-<space|.05em>dimensional space in which each
  axis represents one of the <math|N> <index-complex|<tuple|independent
  variables|equilibrium state>|||<tuple|Independent variables|of an
  equilibrium state>>independent variables needed to describe an equilibrium
  state. A point in this space represents an equilibrium state, and the path
  of a reversible process can be represented as a curve in this space.

  A suitable set of independent variables for equilibrium states of a closed
  system of uniform temperature consists of the temperature <math|T> and each
  of the work coordinates (Sec. <reference|3-generalities>). We can vary the
  work coordinates independently while keeping the boundary adiabatic, so the
  paths for possible reversible adiabatic processes can connect any arbitrary
  combinations of work coordinate values.

  There is, however, the additional dimension of temperature in the
  <math|N><space|-.18em>-<space|.05em>dimensional space. Do the paths for
  possible reversible adiabatic processes, starting from a common initial
  point, lie in a <em|volume> in the <math|N><space|-.18em>-<space|.05em>dimensional
  space? Or do they fall on a <em|surface> described by <math|T> as a
  function of the work coordinates? If the paths lie in a volume, then every
  point in a volume element surrounding the initial point must be accessible
  from the initial point by a reversible adiabatic path. This accessibility
  is precisely what Carath�odory's principle of adiabatic inaccessibility
  denies. Therefore, the paths for all possible reversible adiabatic
  processes with a common initial state must lie on a unique <em|surface>.
  This is an <math|<around|(|N<space|-0.17em>-<space|-0.17em>1|)>>-<space|.05em>dimensional
  hypersurface in the <math|N><space|-.18em>-<space|.05em>dimensional space,
  or a curve if <math|N> is <math|2>. One of these surfaces or curves will be
  referred to as a <subindex|Reversible|adiabatic surface><newterm|reversible
  adiabatic surface>.<label|rev ad surface>

  Now consider the initial and final states of a reversible process with
  one-way heat (i.e., each nonzero infinitesimal quantity of heat
  <math|<dbar|q<rsub|<text|rev>>>> has the same sign). Since we have seen
  that it is impossible for there to be a reversible <em|adiabatic> path
  between these states, the points for these states must lie on different
  reversible adiabatic surfaces that do not intersect anywhere in the
  <math|N><space|-.18em>-<space|.05em>dimensional space. Consequently, there
  is an infinite number of nonintersecting reversible adiabatic surfaces
  filling the <math|N><space|-.18em>-<space|.05em>dimensional space. (To
  visualize this for <math|N=3>, think of a flexed stack of paper sheets;
  each sheet represents a different reversible adiabatic surface in
  three-dimensional space.) A reversible, nonadiabatic process with one-way
  heat is represented by a path beginning at a point on one reversible
  adiabatic surface and ending at a point on a different surface. If <math|q>
  is positive, the final surface lies on one side of the initial surface, and
  if <math|q> is negative, the final surface is on the opposite side.

  The existence of reversible adiabatic surfaces is the justification for
  defining a new state function <math|S>, the
  <index|Entropy><newterm|entropy>. <math|S> is specified to have the same
  value everywhere on one of these surfaces, and a different, unique value on
  each different surface. In other words, the reversible adiabatic surfaces
  are surfaces of <em|constant entropy> in the
  <math|N><space|-.18em>-<space|.05em>dimensional space. The fact that the
  surfaces fill this space without intersecting ensures that <math|S> is a
  state function for equilibrium states, because any point in this space
  represents an equilibrium state and also lies on a single reversible
  adiabatic surface with a definite value of <math|S>.

  We know the entropy function must exist, because the reversible adiabatic
  surfaces exist. For instance, Fig. <vpageref|fig:4-VT
  adiabats><\float|float|thb>
    <\framed>
      <\big-figure>
        <image|04-SUP/ADIABATS.eps|161pt|143pt||>
      <|big-figure>
        <label|fig:4-VT adiabats>A family of reversible adiabatic curves
        (two-dimensional reversible adiabatic surfaces) for an ideal gas with
        <math|V> and <math|T> as independent variables. A reversible
        adiabatic process moves the state of the system along a curve,
        whereas a reversible process with positive heat moves the state from
        one curve to another above and to the right. The curves are
        calculated for <math|n=1 <text|mol>> and
        <math|C<rsub|V,m>=<around|(|3/2|)>*R>. Adjacent curves differ in
        entropy by <math|1 <frac|<text|J>|<text|K>>>.
      </big-figure>
    </framed>
  </float>

  shows a family of these surfaces for a closed system of a pure substance in
  a single phase. In this system, <math|N> is equal to 2, and the surfaces
  are two-dimensional curves. Each curve is a contour of constant <math|S>.
  At this stage in the derivation, our assignment of values of <math|S> to
  the different curves is entirely arbitrary.

  How can we assign a unique value of <math|S> to each reversible adiabatic
  surface? We can order the values by letting a reversible process with
  <em|positive> one-way heat, which moves the point for the state to a new
  surface, correspond to an <em|increase> in the value of <math|S>. Negative
  one-way heat will then correspond to decreasing <math|S>. We can assign an
  arbitrary value to the entropy on one particular reversible adiabatic
  surface. <index|Third law of thermodynamics>(The third law of
  thermodynamics is used for this purpose\Vsee Sec. <reference|6-entropy
  zero>.) Then all that is needed to assign a value of <math|S> to each
  equilibrium state is a formula for evaluating the <em|difference> in the
  entropies of any two surfaces.

  Consider a reversible process with <em|positive> one-way heat that changes
  the system from state A to state B. The path for this process must move the
  system from a reversible adiabatic surface of a certain entropy to a
  different surface of greater entropy. An example is the path A<math|<ra>>B
  in Fig. <reference|fig:4-rev paths>(a) on page <pageref|fig:4-rev
  paths>.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|04-SUP/rev_paths.eps|296pt|151pt||>
      <|big-figure>
        <label|fig:4-rev paths>Reversible paths in <math|V>--<math|T> space.
        The thin curves are reversible adiabatic surfaces.

        \ (a)<nbsp>Two paths connecting the same pair of reversible adiabatic
        surfaces.

        \ (b)<nbsp>A cyclic path.
      </big-figure>
    </framed>
  </float>

  s in this figure are actually two-dimensional curves.) As before, we
  combine the experimental system with a Carnot engine to form a
  <index|Supersystem>supersystem that exchanges heat with a single
  <subindex|Heat|reservoir>heat reservoir of constant temperature
  <math|T<rsub|<text|res>>>. The net heat entering the supersystem, found by
  integrating Eq. <reference|dq'=Tres(dq/Tb)>, is

  <\equation>
    <label|q(ss)=int(AB)>q<rprime|'>=T<rsub|<text|res>><big|int><rsub|<text|A>><rsup|<text|B>><frac|<dbar|q<rsub|<text|rev>>>|T<rsub|<text|b>>>
  </equation>

  and it is positive.

  Suppose the same experimental system undergoes a second reversible process,
  not necessarily with one-way heat, along a different path connecting the
  same pair of reversible adiabatic surfaces. This could be path
  C<math|<ra>>D in Fig. <reference|fig:4-rev paths>(a). The net heat entering
  the supersystem during this second process is <math|q<rprime|''>>:

  <\equation>
    <label|q'(ss)=int(CD)>q<rprime|''>=T<rsub|<text|res>><big|int><rsub|<text|C>><rsup|<text|D>><frac|<dbar|q<rsub|<text|rev>>>|T<rsub|<text|b>>>
  </equation>

  We can then devise a <em|cycle> of the <index|Supersystem>supersystem in
  which the experimental system undergoes the reversible path
  A<math|<ra>>B<math|<ra>>D<math|<ra>>C<math|<ra>>A, as shown in Fig.
  <reference|fig:4-rev paths>(b). Step A<math|<ra>>B is the first process
  described above, step D<math|<ra>>C is the reverse of the second process
  described above, and steps B<math|<ra>>D and C<math|<ra>>A are reversible
  and adiabatic. The net heat entering the supersystem in the cycle is
  <math|q<rprime|'>-q<rprime|''>>. In the reverse cycle the net heat is
  <math|q<rprime|''>-q<rprime|'>>. In both of these cycles the heat is
  exchanged with a single <subindex|Heat|reservoir>heat reservoir; therefore,
  according to the <subindex|Second law of thermodynamics|Kelvin--Planck
  statement>Kelvin\UPlanck statement, neither cycle can have positive net
  heat. Therefore <math|q<rprime|'>> and <math|q<rprime|''>> must be equal,
  and Eqs. <reference|q(ss)=int(AB)> and <reference|q'(ss)=int(CD)> then show
  the integral <math|<big|int><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  has the same value when evaluated along either of the reversible paths from
  the lower to the higher entropy surface.

  Note that since the second path (C<math|<ra>>D) does not necessarily have
  one-way heat, it can take the experimental system through any sequence of
  intermediate entropy values, provided it starts at the lower entropy
  surface and ends at the higher. Furthermore, since the path is reversible,
  it can be carried out in reverse resulting in reversal of the signs of
  <math|\<Delta\>*S> and <math|<big|int><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>.

  It should now be apparent that a satisfactory formula for defining the
  entropy change of a reversible process in a closed system is

  <\equation-cov2|<label|delS=int(dq/Tb)>\<Delta\>*S=<big|int><frac|<dbar|q<rsub|<text|rev>>>|T<rsub|<text|b>>>>
    (reversible process,

    closed system)
  </equation-cov2>

  This formula satisfies the necessary requirements: it makes the value of
  <math|\<Delta\>*S> positive if the process has positive one-way heat,
  negative if the process has negative one-way heat, and zero if the process
  is adiabatic. It gives the same value of <math|\<Delta\>*S> for any
  reversible change between the same two reversible adiabatic surfaces, and
  it makes the sum of the <math|\<Delta\>*S> values of several consecutive
  reversible processes equal to <math|\<Delta\>*S> for the overall process.

  In Eq. <reference|delS=int(dq/Tb)>, <math|\<Delta\>*S> is the entropy
  change when the system changes from one arbitrary equilibrium state to
  another. If the change is an infinitesimal path element of a reversible
  process, the equation becomes

  <\equation-cov2|<label|dS=dq/Tb><dvar|S>=<frac|<dbar|q<rsub|<text|rev>>>|T<rsub|b>>>
    (reversible process,

    closed system)
  </equation-cov2>

  <\quote-env>
    In Eq. <reference|dS=dq/Tb>, the quantity <math|1/T<rsub|<text|b>>> is
    called an <index|Integrating factor><em|integrating factor> for
    <math|<dbar|q<rsub|<text|rev>>>>, a factor that makes the product
    <math|<around|(|1/T<rsub|<text|b>>|)>*<dbar|q<rsub|<text|rev>>>> be an
    exact differential and the infinitesimal change of a state function. The
    quantity <math|c/T<rsub|<text|b>>>, where <math|c> is any nonzero
    constant, would also be a satisfactory integrating factor; so the
    definition of entropy, using <math|c=1>, is actually one of an infinite
    number of possible choices for assigning values to the reversible
    adiabatic surfaces.
  </quote-env>

  \;

  <subsection|Alternative derivation of entropy as a state
  function><label|4-alternative derivation><label|c4 sec-2lr-entropy-state>

  The Clausius inequality <math|<big|oint><around|(|<dbar|q>/T<rsub|<text|b>>|)>\<leq\>0>
  (Eq. <reference|oint(dq/Tb)\<less\>=0>) can be used to show, by a more
  direct route than in the preceding section, that
  <math|<around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>> is an exact
  differential during a reversible process of a closed system. When we equate
  <math|\<Delta\>*S> to this differential, as in Eq. <reference|dS=dq/Tb>,
  the entropy <math|S> can be shown to be a state function.

  The proof uses the fact that when a reversible process is reversed and the
  system passes through the same continuous sequence of equilibrium states in
  reverse order, the heat <math|<dbar|q<rsub|<text|rev>>>> in each
  infinitesimal step changes its sign but not its magnitude (Sec.
  <reference|3-reversible processes>). As a result, the integral
  <math|<big|int><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  changes its sign but not its magnitude when the process is reversed.

  Consider an arbitrary reversible cyclic process of a closed system. Could
  the cyclic integral <math|<big|oint><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  for this process be <em|positive>? No, that is impossible according to the
  Clausius inequality. Could the cyclic integral be <em|negative>? No,
  because in this case <math|<big|oint><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  for the reverse cycle is positive, which is also impossible. Thus the value
  of the cyclic integral for a reversible cyclic process must be <em|zero>:

  <\equation-cov2|<label|oint(dq(rev)/Tb)=0><big|oint><frac|<dbar|q<rsub|<text|rev>>>|T<rsub|<text|b>>>=0>
    (reversible cyclic process

    of a closed system)
  </equation-cov2>

  Let A and B be any two equilibrium states. Let path 1 and path 2 be two
  arbitrary but different reversible paths starting at state A and ending at
  state B, and let path 3 be the path from state B to state A that is the
  reverse of path 2. When the system changes from state A to state B along
  path 1, and then changes back to state A along path 3, it has undergone a
  reversible cyclic process. From Eq. <reference|oint(dq(rev)/Tb)=0>, the sum
  of the integrals of <math|<around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  along paths 1 and 3 is zero. The integral of
  <math|<around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>> along path 3
  has the same magnitude and opposite sign of the integral of
  <math|<around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>> along path
  2. Therefore the integral <math|<big|int><rsup|<text|B>><rsub|<text|A>><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  must have the same value along paths 1 and 2. The result would be the same
  for a reversible cycle using any other two paths from state A to state B.
  We conclude that the value of <math|<around|(|<dbar|q><rsub|<text|rev>>/T<rsub|<text|b>>|)>>
  integrated over a reversible path between any two equilibrium states
  depends only on the initial and final states and not on the path; that is,
  <math|<around|(|<dbar|q><rsub|<text|rev>>/T<rsub|<text|b>>|)>> is an exact
  differential as defined on page <pageref|exact differential>.

  When we equate <math|\<Delta\>*S> to <math|<around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>,
  the entropy change along a reversible path from any initial equilibrium
  state A to any final equilibrium state B is given by

  <\equation>
    <label|delS=int(dS)=int(dq/Tb)>\<Delta\>*S<rsub|<text|A><ra><text|B>>=S<rsub|<text|B>>-S<rsub|<text|A>>=<big|int><rsub|<text|A><rsup|>><rsup|<text|B>><dvar|S>=<big|int><rsub|<text|A>><rsup|<text|B>><frac|<dbar|q<rsub|<text|rev>>>|T<rsub|<text|b>>>
  </equation>

  Since the value of <math|<big|int><rsup|<text|B>><rsub|<text|A>><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  depends only on the initial and final states A and B, so also does the
  value of <math|\<Delta\>*S<rsub|<text|A><ra><text|B>>>. If a value of
  <math|S> is assigned to a reference state, Eq.
  <reference|delS=int(dS)=int(dq/Tb)> in principle allows the value of
  <math|S> to be evaluated for any other equilibrium state of the system.
  Each value of <math|S> then depends only on the state and not on the past
  or future history of the system. Therefore, by the definition in Sec.
  <vpageref|2-state fncs \ ind variables>, the entropy is a state
  function.<index-complex|<tuple|second law of thermodynamics|mathematical
  statement|derivation>||c4 sec-2lr-entropy-reversible idx1|<tuple|Second law
  of thermodynamics|mathematical statement|derivation>>

  <subsection|Some properties of the entropy><label|4-properties of
  entropy><label|c4 sec-2lr-entropy-properties>

  It is not difficult to show that the entropy of a closed system in an
  equilibrium state is an <index-complex|<tuple|entropy|extensive>|||<tuple|Entropy|an
  extensive property>><em|extensive> property. Suppose a system of uniform
  temperature <math|T> is divided into two closed subsystems A and B. When a
  reversible infinitesimal change occurs, the entropy changes of the
  subsystems are <math|<dvar|S<rsub|<text|A>>>=<dbar|q><rsub|<text|A>>/T> and
  <math|<dvar|S<rsub|<text|b>>>=<dbar|q><rsub|<text|b>>/T> and of the system
  <math|<dvar|S>=<dbar|q<rsub|<text|rev>>>/T>. But
  <math|<dbar|q<rsub|<text|rev>>>> is the sum of
  <math|<dbar|q><rsub|<text|A>>> and <math|<dbar|q><rsub|<text|b>>>, which
  gives <math|<dvar|S>=<dvar|S<rsub|<text|A>>>+<dvar|S<rsub|<text|b>>>>.
  Thus, the entropy changes are additive, so that entropy must be extensive:
  <math|S>=S<rsub|<text|A>>+S<rsub|<text|b>>.<footnote|The argument is not
  quite complete, because we have not shown that when each subsystem has an
  entropy of zero, so does the entire system. The zero of entropy will be
  discussed in Sec. <reference|6-entropy zero>.>

  How can we evaluate the entropy of a particular equilibrium state of the
  system? We must assign an arbitrary value to one state and then evaluate
  the entropy change along a reversible path from this state to the state of
  interest using <math|\<Delta\>*S=<big|int><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>.

  We may need to evaluate the entropy of a
  <index-complex|<tuple|entropy|nonequilibrium>|||<tuple|Entropy|of a
  nonequilibrium state>><em|non>equilibrium state. To do this, we imagine
  imposing hypothetical internal constraints that change the nonequilibrium
  state to a constrained equilibrium state with the same internal structure.
  Some examples of such internal constraints were given in Sec.
  <reference|2-eq states>, and include rigid adiabatic partitions between
  phases of different temperature and pressure, semipermeable membranes to
  prevent transfer of certain species between adjacent phases, and inhibitors
  to prevent chemical reactions.

  We assume that we can, in principle, impose or remove such constraints
  reversibly without heat, so there is no entropy change. If the
  nonequilibrium state includes macroscopic internal motion, the imposition
  of internal constraints involves negative reversible work to bring moving
  regions of the system to rest.<footnote|This concept amounts to defining
  the entropy of a state with macroscopic internal motion to be the same as
  the entropy of a state with the same internal structure but without the
  motion, i.e., the same state frozen in time. By this definition,
  <math|\<Delta\>*S> for a purely mechanical process (Sec.
  <reference|3-purely mechanical processes>) is zero.> If the system is
  nonuniform over its extent, the internal constraints will partition it into
  practically-uniform regions whose entropy is additive. The entropy of the
  nonequilibrium state is then found from
  <math|\<Delta\>*S=<big|int><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  using a reversible path that changes the system from an equilibrium state
  of known entropy to the constrained equilibrium state with the same entropy
  as the state of interest. This procedure allows every possible state (at
  least conceptually) to have a definite value of <math|S>.

  <section|The Second Law for Irreversible Processes><label|4-irrev
  processes><label|c4 sec 2li>

  <index-complex|<tuple|irreversible process>||c4 sec 2li
  idx1|<tuple|Irreversible process>><index-complex|<tuple|process|irreversible>||c4
  sec 2li idx2|<tuple|Process|irreversible>>We know that during a reversible
  process of a closed system, each infinitesimal entropy change
  <math|\<Delta\>*S> is equal to <math|<dbar|q>/T<rsub|<text|b>>> and the
  finite change <math|\<Delta\>*S> is equal to the integral
  <math|<big|int><around|(|<dbar|q>/T<rsub|<text|b>>|)>>\Vbut what can we say
  about <math|<dvar|S>> and <math|\<Delta\>*S> for an <em|irreversible>
  process?

  The derivation of this section will show that for an infinitesimal
  irreversible change of a closed system, <math|<dvar|S>> is greater than
  <math|<dbar|q>/T<rsub|<text|b>>>, and for an entire irreversible process
  <math|\<Delta\>*S> is greater than <math|<big|int><around|(|<dbar|q>/T<rsub|<text|b>>|)>>.
  That is, the <em|equalities> that apply to a reversible process are
  replaced, for an irreversible process, by <em|inequalities>.

  The derivation begins with irreversible processes that are adiabatic, and
  is then extended to irreversible processes in general.

  <subsection|Irreversible adiabatic processes><label|4-irrev ad
  processes><label|c4 sec 2li-adiabatic>

  Consider an arbitrary irreversible adiabatic process of a closed system
  starting with a particular initial state A. The final state B depends on
  the path of this process. We wish to investigate the sign of the entropy
  change <math|\<Delta\>*S<rsub|<text|A><ra><text|B>>>. Our reasoning will
  depend on whether or not there is work during the process.

  If there is work along any infinitesimal path element of the irreversible
  adiabatic process (<math|<dbar|w>\<ne\>0>), we know from experience that
  this work would be different if the work coordinate or coordinates were
  changing at a different rate, because <subindex|Energy|dissipation
  of><index|Dissipation of energy>energy dissipation from internal friction
  would then be different. In the limit of infinite slowness, an adiabatic
  process with initial state A and the same change of work coordinates would
  become reversible, and the net work and final internal energy would differ
  from those of the irreversible process. Because the final state of the
  reversible adiabatic process is different from B, there is no reversible
  adiabatic path with work between states A and B.

  <\quote-env>
    All states of a reversible process, including the initial and final
    states, must be equilibrium states. There is therefore a conceptual
    difficulty in considering reversible paths between two states if either
    of these states are nonequilibrium states. In such a case we will assume
    that the state has been replaced by a constrained equilibrium state of
    the same entropy, as described in Sec. <reference|4-properties of
    entropy>.
  </quote-env>

  If, on the other hand, there is no work along any infinitesimal path
  element of the irreversible adiabatic process (<math|<dbar|w>\<neq\>0>),
  the process is taking place at constant internal energy <math|U> in an
  <em|isolated> system. A reversible limit cannot be reached without heat or
  work (page <pageref|no rev limit in isolated system>). Thus any reversible
  adiabatic change from state A would require work, causing a change of
  <math|U> and preventing the system from reaching state B by any reversible
  adiabatic path.

  So regardless of whether or not an irreversible adiabatic process
  A<math|<ra>>B involves work, there is no <em|reversible> adiabatic path
  between A and B. The only reversible paths between these states must be
  <em|non>adiabatic. It follows that the entropy change
  <math|\<Delta\>*S<rsub|<text|A><ra><text|B>>>, given by the value of
  <math|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>> integrated over a
  reversible path from A to B, cannot be zero.

  Next we ask whether <math|\<Delta\>*S<rsub|<text|A><ra><text|B>>> could be
  negative. In each infinitesimal path element of the irreversible adiabatic
  process A<math|<ra>>B, <math|<dbar|q>> is zero and the integral
  <math|<big|int><rsub|<text|A>><rsup|B><rsub|><rsup|><around|(|<dbar|q>/T<rsub|<text|b>>|)>>
  along the path of this process is zero. Suppose the system completes a
  cycle by returning along a different, reversible path from state B back to
  state A. The Clausius inequality (Eq. <reference|oint(dq/Tb)\<less\>=0>)
  tells us that in this case the integral
  <math|<big|int><rsup|<text|A>><rsub|<text|B>><rsub|><rsup|><around|(|<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>|)>>
  along the reversible path cannot be positive. But this integral for the
  reversible path is equal to <math|-\<Delta\>*S<rsub|<text|A><ra><text|B>>>,
  so <math|\<Delta\>*S<rsub|<text|A><ra><text|B>>> cannot be negative.

  We conclude that because the entropy change of the irreversible adiabatic
  process A<math|<ra>>B cannot be zero, and it cannot be negative, it must be
  <em|positive>.

  In this derivation, the initial state A is arbitrary and the final state B
  is reached by an irreversible adiabatic process. If the two states are only
  infinitesimally different, then the change is infinitesimal. Thus for an
  infinitesimal change that is irreversible and adiabatic, <math|<dvar|S>>
  must be <em|positive>.

  <subsection|Irreversible processes in general><label|4-irrev processes in
  general><label|c4 sec 2li-general>

  To treat an irreversible process of a closed system that is nonadiabatic,
  we proceed as follows. As in Sec. <reference|4-Clausius inequality>, we use
  a Carnot engine for heat transfer across the boundary of the experimental
  system. We move the boundary of the <index|Supersystem>supersystem of Fig.
  <reference|fig:4-supersystem> so that the supersystem now includes the
  experimental system, the Carnot engine, and a <subindex|Heat|reservoir>heat
  reservoir of constant temperature <math|T<rsub|<text|res>>>, as depicted in
  Fig. <vpageref|fig:4-adiabatic_supersystem>.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|04-SUP/SS-2.eps|228pt|128pt||>
      <|big-figure>
        <label|fig:4-adiabatic_supersystem>Supersystem including the
        experimental system, a Carnot engine (square box), and a heat
        reservoir. The dashed rectangle indicates the boundary of the
        supersystem.
      </big-figure>
    </framed>
  </float>

  During an irreversible change of the experimental system, the Carnot engine
  undergoes many infinitesimal cycles. During each cycle, the Carnot engine
  exchanges heat <math|<dbar|q><rprime|'>> at temperature
  <math|T<rsub|<text|res>>> with the heat reservoir and heat <math|<dbar|q>>
  at temperature <math|T<rsub|<text|b>>> with the experimental system, as
  indicated in the figure. We use the sign convention that
  <math|<dbar|q><rprime|'>> is positive if heat is transferred to the Carnot
  engine, and <math|<dbar|q>> is positive if heat is transferred to the
  experimental system, in the directions of the arrows in the figure.

  The <index|Supersystem>supersystem exchanges work, but not heat, with its
  surroundings. (The work involves the Carnot engine, but not necessarily the
  experimental system.) During one infinitesimal cycle of the Carnot engine,
  the net entropy change of the Carnot engine is zero, the entropy change of
  the experimental system is <math|\<Delta\>*S>, the heat transferred between
  the Carnot engine and the experimental system is <math|<dbar|q>>, and the
  heat transferred between the heat reservoir and the Carnot engine is given
  by <math|<dbar|q><rprime|'>=T<rsub|<text|res>><dbar|q>/T<rsub|<text|b>>>
  (Eq. <reference|dq'=Tres(dq/Tb)>). The heat transfer between the heat
  reservoir and Carnot engine is reversible, so the entropy change of the
  heat reservoir is

  <\equation>
    <label|dS(res)=>\<Delta\>*S<rsub|<text|res>>=-<frac|<dbar|q><rprime|'>|T<rsub|<text|res>>>=-<frac|<dbar|q>|T<rsub|<text|b>>>
  </equation>

  The entropy change of the supersystem is the sum of the entropy changes of
  its parts:

  <\equation>
    <label|dS(ss)=sum>\<Delta\>*S<rsub|<text|ss>>=<dvar|S>+<dvar|S<rsub|<text|res>>>=<dvar|S>-<frac|<dbar|q>|T<rsub|<text|b>>>
  </equation>

  The process within the <index|Supersystem>supersystem is adiabatic and
  includes an irreversible change within the experimental system, so
  according to the conclusions of Sec. <reference|4-irrev ad processes>,
  <math|<dvar|S<rsub|<text|ss>>>> is positive. Equation
  <reference|dS(ss)=sum> then shows that <math|<dvar|S>>, the infinitesimal
  entropy change during the irreversible change of the experimental system,
  must be greater than <math|<dbar|q>/T<rsub|<text|b>>>:

  <\equation-cov2|<label|dS\<gtr\>int(dq/Tb)><dvar|S>\<gtr\><frac|<dbar|q>|T<rsub|<text|b>>>>
    (irreversible change,

    closed system)
  </equation-cov2>

  This relation includes the case of an irreversible <em|adiabatic> change,
  because it shows that if <math|<dbar|q>> is zero, <math|<dvar|S>> is
  greater than zero.

  By integrating both sides of Eq. <reference|dS\<gtr\>int(dq/Tb)> between
  the initial and final states of the irreversible process, we obtain a
  relation for the finite entropy change corresponding to many infinitesimal
  cycles of the Carnot engine:

  <\equation-cov2|<label|del S\<gtr\>int(dq/Tb)>\<Delta\>*S\<gtr\><frac|<dbar|q>|T<rsub|<text|b>>>>
    (irreversible process,

    closed system)
  </equation-cov2>

  <index-complex|<tuple|irreversible process>||c4 sec 2li
  idx1|<tuple|Irreversible process>><index-complex|<tuple|process|irreversible>||c4
  sec 2li idx2|<tuple|Process|irreversible>>

  <section|Applications><label|4-applications><label|c4 sec apps>

  The lengthy derivation in Secs. <reference|4-Carnot
  engines>\U<reference|4-irrev processes> is based on the Kelvin\UPlanck
  statement describing the impossibility of converting completely into work
  the energy transferred into the system by heat from a single
  <subindex|Heat|reservoir>heat reservoir. The derivation has now given us
  all parts of the mathematical statement of the second law shown in the box
  on page <pageref|second law box>. The mathematical statement includes an
  equality, <math|<dvar|S>=<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>>, that
  applies to an infinitesimal <em|reversible> change, and an inequality,
  <math|<dvar|S>\<gtr\><dbar|q>/T<rsub|<text|b>>>, that applies to an
  infinitesimal <em|irreversible> change. It is convenient to combine the
  equality and inequality in a single relation that is a general mathematical
  statement of the second law: <subindex|Second law of
  thermodynamics|mathematical statement>

  <equation-cov2|<label|dS \<gtr\>= dq/Tb><dvar|S>\<geq\><frac|<dbar|q>|T<rsub|<text|b>>>|(
  <ir> ,closed system)>

  The inequality refers to an irreversible change and the equality to a
  reversible change, as indicated by the notation <ir> in the conditions of
  validity.<label|meaning of ir> The integrated form of this relation is

  <equation-cov2|<label|delS \<gtr\>= dq/Tb>\<Delta\>*S\<geq\><big|int><frac|<dbar|q>|T<rsub|<text|b>>>|(
  <ir>, closed system)>

  During a reversible process, the states are equilibrium states and the
  temperature is usually uniform throughout the system. The only exception is
  if the system happens to have internal adiabatic partitions that allow
  phases of different temperatures in an equilibrium state. As mentioned in
  the footnote on page <pageref|T replaces T_b>, when the process is
  reversible and the temperature is uniform, we can replace
  <math|<dvar|S>=<dbar|q<rsub|<text|rev>>>/T<rsub|<text|b>>> by
  <math|<dvar|S>=<dbar|q<rsub|<text|rev>>>/T>.

  The rest of Sec. <reference|4-applications> will apply Eqs. <reference|dS
  \<gtr\>= dq/Tb> and <reference|delS \<gtr\>= dq/Tb> to various reversible
  and irreversible processes.

  <subsection|Reversible heating><label|4-rev heating \ expansion><label|c4
  sec apps-heating>

  <index-complex|<tuple|heating|reversible>||c4 sec apps-heating
  idx1|<tuple|Heating|reversible>><index-complex|<tuple|reversible|heating>||c4
  sec apps-heating idx2|<tuple|Reversible|heating>>The definition of the heat
  capacity <math|C> of a closed system is given by Eq. <vpageref|heat
  capacity def>: <math|C<bk-equal-def><dbar|q>/<dvar|T>>. For reversible
  heating or cooling of a homogeneous phase, <math|<dbar|q>> is equal to
  <math|T*<dvar|S>> and we can write

  <\equation>
    <label|del S=int(C/T)dT>\<Delta\>*S=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em><frac|C|T>*<dvar|T>
  </equation>

  where <math|C> should be replaced by <math|C<rsub|V>> if the volume is
  constant, or by <math|C<rsub|p>> if the pressure is constant (Sec.
  <reference|3-heat capacity>). If the heat capacity has a constant value
  over the temperature range from <math|T<rsub|1>> to <math|T<rsub|2>>, the
  equation becomes

  <\equation>
    <label|Del S=Cln(T2/T1)>\<Delta\>*S=C*ln <frac|T<rsub|2>|T<rsub|1>>
  </equation>

  Heating increases the entropy, and cooling decreases
  it.<index-complex|<tuple|heating|reversible>||c4 sec apps-heating
  idx1|<tuple|Heating|reversible>><index-complex|<tuple|reversible|heating>||c4
  sec apps-heating idx2|<tuple|Reversible|heating>>

  <subsection|Reversible expansion of an ideal gas><label|4-rev
  expansion><label|c4 sec-apps-expansion-rev>

  <subindex|Expansion|reversible, of an ideal
  gas><subindex|Reversible|expansion of an ideal gas>When the volume of an
  ideal gas, or of any other fluid, is changed reversibly and
  <em|adiabatically>, there is of course no entropy change.

  When the volume of an ideal gas is changed reversibly and
  <em|isothermally>, there is expansion work given by <math|w=-n*R*T*ln
  <around|(|V<rsub|2>/V<rsub|1>|)>> (Eq. <reference|w=-nRT ln(V2/V1)>). Since
  the internal energy of an ideal gas is constant at constant temperature,
  there must be heat of equal magnitude and opposite sign: <math|q=n*R*T*ln
  <around|(|V<rsub|2>/V<rsub|1>|)>>. The entropy change is therefore

  <\equation-cov2|<label|Del S=nRln(V2/V1)>\<Delta\>*S=n*R*<text|ln>
  <frac|V<rsub|2>|V<rsub|1>>>
    (reversible isothermal volume

    change of an ideal gas)
  </equation-cov2>

  Isothermal expansion increases the entropy, and isothermal compression
  decreases it.

  Since the change of a state function depends only on the initial and final
  states, Eq. <reference|Del S=nRln(V2/V1)> gives a valid expression for
  <math|\<Delta\>*S> of an ideal gas under the less stringent condition
  <math|T<rsub|2>=T<rsub|1>>; it is not necessary for the intermediate states
  to be equilibrium states of the same temperature.

  <subsection|Spontaneous changes in an isolated system><label|c4
  sec-apps-spontaneous>

  <index-complex|<tuple|isolated system|spontaneous charges in>||c4
  sec-apps-spontaneous|<tuple|Isolated system|spontaneous changes in>>An
  isolated system is one that exchanges no matter or energy with its
  surroundings. Any change of state of an isolated system that actually
  occurs is spontaneous, and arises solely from conditions within the system,
  uninfluenced by changes in the surroundings\Vthe process occurs by itself,
  of its own accord. The initial state and the intermediate states of the
  process must be nonequilibrium states, because by definition an equilibrium
  state would not change over time in the isolated system.

  Unless the spontaneous change is purely mechanical, it is
  <subindex|Process|irreversible><index|Irreversible process>irreversible.
  According to the second law, during an infinitesimal change that is
  irreversible and adiabatic, the entropy increases. For the isolated system,
  we can therefore write

  <equation-cov2|<label|dS\<gtr\>0 (irrev,
  isolated)><dvar|S>\<gtr\>0|(irreversible change, isolated system)>

  In later chapters, the inequality of Eq. <reference|dS\<gtr\>0 (irrev,
  isolated)> will turn out to be one of the most useful for deriving
  conditions for spontaneity and equilibrium in chemical systems: <em|The
  entropy of an isolated system continuously increases during a spontaneous,
  irreversible process until it reaches a maximum value at equilibrium>.

  If we treat the universe as an isolated system (although cosmology provides
  no assurance that this is a valid concept), we can say that as spontaneous
  changes occur in the universe, its entropy continuously increases.
  <index|Clausius, Rudolf>Clausius summarized the first and second laws in a
  famous statement: <em|Die Energie der Welt ist constant; die Entropie der
  Welt strebt einem Maximum zu> (the energy of the universe is constant; the
  entropy of the universe strives toward a
  maximum).<index-complex|<tuple|isolated system|spontaneous charges in>||c4
  sec-apps-spontaneous|<tuple|Isolated system|spontaneous changes in>>

  <subsection|Internal heat flow in an isolated system><label|c4
  sec-apps-heatflow>

  <index-complex|<tuple|heat|flow in an isolated system>||c4
  sec-apps-heatflow idx1|<tuple|Heat|flow in an isolated system>>Suppose the
  system is a solid body whose temperature initially is nonuniform. Provided
  there are no internal adiabatic partitions, the initial state is a
  nonequilibrium state lacking internal thermal equilibrium. If the system is
  surrounded by thermal insulation, and volume changes are negligible, this
  is an isolated system. There will be a spontaneous, irreversible internal
  redistribution of thermal energy that eventually brings the system to a
  final equilibrium state of uniform temperature.

  In order to be able to specify internal temperatures at any instant, we
  treat the system as an assembly of phases, each having a uniform
  temperature that can vary with time. To describe a region that has a
  continuous temperature gradient, we approximate the region with a very
  large number of very small phases or parcels, each having a temperature
  infinitesimally different from its neighbors.

  We use Greek letters to label the phases. The temperature of phase \ <pha>
  at any given instant is <math|T<rsup|<pha>>>. We can treat each phase as a
  subsystem with a boundary across which there can be energy transfer in the
  form of heat. Let <math|<dbar|q><rsub|<pha>\<nocomma\><phb>>> represent an
  infinitesimal quantity of heat transferred during an infinitesimal interval
  of time to phase <math|<pha>> from phase <math|<phb>>. The heat transfer,
  if any, is to the cooler from the warmer phase. If phases <math|<pha>> and
  <math|<phb>> are in thermal contact and <math|T<aph>> is less than
  <math|T<bph>>, then <math|<dbar|q><rsub|<pha>\<nocomma\><phb>>> is
  positive; if the phases are in thermal contact and <math|T<aph>> is greater
  than <math|T<bph>>, <math|<dbar|q><rsub|<pha><phb>>> is negative; and if
  neither of these conditions is satisfied,
  <math|<dbar|q><rsub|<pha>\<nocomma\><phb>>> is zero.

  To evaluate the entropy change, we need a reversible path from the initial
  to the final state. The net quantity of heat transferred to phase
  <math|<pha>> during an infinitesimal time interval is
  <math|<dbar|q><aph>=<big|sum><rsub|<phb>\<neq\><pha>><dbar|q><rsub|<pha>\<nocomma\><phb>>>.
  The entropy change of phase <math|<pha>> is the same as it would be for the
  reversible transfer of this heat from a <subindex|Heat|reservoir>heat
  reservoir of temperature <math|T<aph>>:
  <math|<dvar|S<aph>>=<dbar|q><aph>/T<aph>>. The entropy change of the entire
  system along the reversible path is found by summing over all phases:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<dvar|S>>|<cell|<below|=|>>|<cell|<below|<big|sum>|<pha>><dvar|S<aph>>=<below|<big|sum>|<pha>><frac|<dbar|q<aph>>|T<aph>>=<below|<big|sum>|<pha>><below|<big|sum>|<phb>\<neq\><pha>><frac|<dbar|q<rsub|<pha>\<nocomma\><phb>>>|T<aph>>>>|<row|<cell|>|<cell|=>|<cell|<below|<big|sum>|<pha>><below|<big|sum>|<phb>\<gtr\><pha>><around*|(|<frac|<dbar|q<rsub|<pha>\<nocomma\><phb>>>|T<aph>>+<frac|<dbar|q<rsub|<phb>\<nocomma\><pha>>>|T<aph>>|)><eq-number><label|dS=sum(a)sum(b)>>>>>
  </eqnarray*>

  There is also the condition of quantitative energy transfer,
  <math|<dbar|q><rsub|<phb><pha>>=-<dbar|q><rsub|<pha><phb>>>, which we use
  to rewrite Eq. <reference|dS=sum(a)sum(b)> in the form

  <\equation>
    <label|dS=sum'(a)sum(b)><dvar|S>=<big|sum><rsub|<pha>><big|sum><rsub|<phb>\<gtr\><pha>><around*|(|<frac|1|T<aph>>-<frac|1|T<bph>>|)><dbar|q><rsub|<pha>\<nocomma\><phb>>
  </equation>

  Consider an individual term of the sum on the right side of Eq.
  <reference|dS=sum'(a)sum(b)> that has a nonzero value of
  <math|<dbar|q><rsub|<pha><phb>>> due to finite heat transfer between phases
  <math|<pha>> and <math|<phb>>. If <math|T<aph>> is less than <math|T<bph>>,
  then both <math|<dbar|q><rsub|<pha><phb>>> and
  <math|<around|(|1/T<aph>-1/T<bph>|)>> are positive. If, on the other hand,
  <math|T<aph>> is greater than <math|T<bph>>, both
  <math|<dbar|q><rsub|<pha>\<nocomma\><phb>>> and
  <math|<around|(|1/T<aph>-1/T<bph>|)>> are negative. Thus each term of the
  sum is either zero or positive, and as long as phases of different
  temperature are present, <math|<dvar|S>> is positive.

  This derivation shows that during a spontaneous thermal equilibration
  process in an isolated system, starting with any initial distribution of
  the internal temperatures, the entropy continuously increases until the
  system reaches a state of thermal equilibrium with a single uniform
  temperature throughout.<footnote|Leff, in Ref. <cite|leff-77>, obtains the
  same result by a more complicated derivation.> The result agrees with Eq.
  <reference|dS\<gtr\>0 (irrev, isolated)>.<index-complex|<tuple|heat|flow in
  an isolated system>||c4 sec-apps-heatflow idx1|<tuple|Heat|flow in an
  isolated system>>

  <subsection|Free expansion of a gas><label|c4 sec apps-expansion-free>

  <index-complex|<tuple|free expansion>||c4 sec apps-expansion-free
  idx1|<tuple|Free expansion>>Consider the free expansion of a gas shown in
  Fig. <vpageref|fig:3-free expansion>. The <em|system> is the gas. Assume
  that the vessel walls are rigid and adiabatic, so that the system is
  isolated. When the stopcock between the two vessels is opened, the gas
  expands irreversibly into the vacuum without heat or work and at constant
  internal energy. To carry out the same change of state reversibly, we
  confine the gas at its initial volume and temperature in a
  cylinder-and-piston device and use the piston to expand the gas
  adiabatically with negative work. Positive heat is then needed to return
  the internal energy reversibly to its initial value. Because the reversible
  path has positive heat, the entropy change is positive.

  This is an example of an <subindex|Process|irreversible><index|Irreversible
  process>irreversible process in an isolated system for which a reversible
  path between the initial and final states has both heat and
  work.<index-complex|<tuple|free expansion>||c4 sec apps-expansion-free
  idx1|<tuple|Free expansion>>

  <subsection|Adiabatic process with work><label|4-adiabatic processes with
  work><label|c4 sec apps-adiabatic-work>

  <subindex|Process|adiabatic><subindex|Adiabatic|process>In general (page
  <pageref|minimal work principle>), an adiabatic process with a given
  initial equilibrium state and a given change of a work coordinate has the
  least positive or most negative work in the reversible limit. Consider an
  irreversible adiabatic process with work <math|w<rsub|<text|irr>>>. The
  same change of state can be accomplished reversibly by the following two
  steps: (1) a reversible adiabatic change of the work coordinate with work
  <math|w<rsub|<text|rev>>>, followed by (2) reversible transfer of heat
  <math|q<rsub|<text|rev>>> with no further change of the work coordinate.
  Since <math|w<rsub|<text|rev>>> is algebraically less than
  <math|w<rsub|<text|irr>>>, <math|q<rsub|<text|rev>>> must be positive in
  order to make <math|\<Delta\>*U> the same in the irreversible and
  reversible paths. The positive heat increases the entropy along the
  reversible path, and consequently the irreversible adiabatic process has a
  positive entropy change. This conclusion agrees with the second-law
  inequality of Eq. <reference|dS \<gtr\>= dq/Tb>.

  <section|Summary><label|4-summary><label|c4 sec summary>

  Some of the important terms and definitions discussed in this chapter are
  as follows.

  <\itemize>
    <item>Any conceivable process is either spontaneous, reversible, or
    impossible.

    <item>A <subindex|Reversible|process><subindex|Process|reversible><em|reversible>
    process proceeds by a continuous sequence of equilibrium states.

    <item>A <index|Spontaneous process><subindex|Process|spontaneous><em|spontaneous>
    process is one that proceeds naturally at a finite rate.

    <item>An <index|Irreversible process><subindex|Process|irreversible><em|irreversible>
    process is a spontaneous process whose reverse is impossible.

    <item>A <subindex|Process|mechanical><em|purely mechanical process> is an
    idealized process without temperature gradients, and without friction or
    other <subindex|Energy|dissipation of><index|Dissipation of
    energy>dissipative effects, that is spontaneous in either direction. This
    kind of process will be ignored in the remaining chapters of this book.

    <item>Except for a purely mechanical process, the terms <em|spontaneous>
    and <em|irreversible> are equivalent.
  </itemize>

  The derivation of the mathematical statement of the second law shows that
  during a reversible process of a closed system, the infinitesimal quantity
  <math|<dbar|q>/T<rsub|<text|b>>> equals the infinitesimal change of a state
  function called the <index|Entropy>entropy, <math|S>. Here <math|<dbar|q>>
  is heat transferred at the boundary where the temperature is
  <math|T<rsub|<text|b>>>.

  In each infinitesimal path element of a process of a closed system,
  <math|<dvar|S>> is equal to <math|<dbar|q>/T<rsub|<text|b>>> if the process
  is reversible, and is greater than <math|<dbar|q>/T<rsub|<text|b>>> if the
  process is irreversible, as summarized by the relation
  <math|<dvar|S>\<geq\><dbar|q>/T<rsub|<text|b>>>.

  Consider two particular equilibrium states <math|1> and <math|2> of a
  closed system. The system can change from state <math|1> to state <math|2>
  by either a reversible process, with <math|\<Delta\>*S> equal to the
  integral <math|<big|int><around|(|<dbar|q>/T<rsub|<text|b>>|)>>, or an
  irreversible process, with <math|\<Delta\>*S> greater than
  <math|<big|int><around|(|<dbar|q>/T<rsub|<text|b>>|)>>. It is important to
  keep in mind the point made by Fig. <vpageref|fig:4-two
  paths>:<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|04-SUP/TWOPATHS.eps|145pt|95pt||>
      <|big-figure>
        Reversible and irreversible paths between the same initial and final
        equilibrium states of a closed system. The value of
        <math|\<Delta\>*S> is the same for both paths, but the values of the
        integral <math|<big|int><around|(|<dbar|q>/T<rsub|<text|b>>|)>> are
        different.<label|fig:4-two paths>
      </big-figure>
    </framed>
  </float>

  because <math|S> is a state function, it is the value of the integral that
  is different in the two cases, and not the value of <math|\<Delta\>*S>.

  The second law establishes no general relation between entropy changes and
  heat in an open system, or for an impossible process. The entropy of an
  open system may increase or decrease depending on whether matter enters or
  leaves. It is possible to imagine different impossible processes in which
  <math|<dvar|S>> is less than, equal to, and greater than
  <math|<dbar|q>/T<rsub|<text|b>>>.

  <section|The Statistical Interpretation of
  Entropy><label|4-statistical><label|c4 sec-entropy-statistical>

  Because entropy is such an important state function, it is natural to seek
  a description of its meaning on the microscopic level.

  <index-complex|<tuple|entropy|measure of disorder>|||<tuple|Entropy|as a
  measure of disorder>><index|Disorder>Entropy is sometimes said to be a
  measure of \Pdisorder.\Q According to this idea, the entropy increases
  whenever a closed system becomes more disordered on a microscopic scale.
  This description of entropy as a measure of disorder is highly misleading.
  It does not explain why entropy is increased by reversible heating at
  constant volume or pressure, or why it increases during the reversible
  isothermal expansion of an ideal gas. Nor does it seem to agree with the
  freezing of a supercooled liquid or the formation of crystalline solute in
  a supersaturated solution; these processes can take place spontaneously in
  an isolated system, yet are accompanied by an apparent <em|decrease> of
  disorder.

  Thus we should not interpret entropy as a measure of disorder. We must look
  elsewhere for a satisfactory microscopic interpretation of entropy.

  A rigorous interpretation is provided by the discipline of
  <index|Statistical mechanics><em|statistical mechanics>, which derives a
  precise expression for entropy based on the behavior of macroscopic amounts
  of microscopic particles. Suppose we focus our attention on a particular
  macroscopic equilibrium state. Over a period of time, while the system is
  in this equilibrium state, the system at each instant is in a
  <index|Microstate><em|microstate>, or stationary quantum state, with a
  definite energy. The microstate is one that is <em|accessible> to the
  system\Vthat is, one whose wave function is compatible with the system's
  volume and with any other conditions and constraints imposed on the system.
  The system, while in the equilibrium state, continually jumps from one
  accessible microstate to another, and the macroscopic state functions
  described by classical thermodynamics are time averages of these
  microstates.

  The fundamental assumption of <index|Statistical mechanics>statistical
  mechanics is that accessible microstates of equal energy are equally
  probable, so that the system while in an equilibrium state spends an equal
  fraction of its time in each such microstate. The statistical entropy of
  the equilibrium state then turns out to be given by the equation

  <\equation>
    <label|S=k ln W>S<rsub|<text|stat>>=k*ln W+C
  </equation>

  where <math|k> is the Boltzmann constant <math|k=R/N<rsub|<text|A>>>,
  <math|W> is the number of accessible microstates, and <math|C> is a
  constant.

  In the case of an equilibrium state of a perfectly-isolated system of
  constant internal energy <math|U>, the accessible microstates are the ones
  that are compatible with the constraints and whose energies all have the
  same value, equal to the value of <math|U>.

  It is more realistic to treat an equilibrium state with the assumption the
  system is in thermal equilibrium with an external constant-temperature
  <subindex|Heat|reservoir>heat reservoir. The internal energy then
  fluctuates over time with extremely small deviations from the average value
  <math|U>, and the accessible microstates are the ones with energies close
  to this average value. In the language of <index|Statistical
  mechanics>statistical mechanics, the results for an isolated system are
  derived with a microcanonical ensemble, and for a system of constant
  temperature with a canonical ensemble.

  A change <math|\<Delta\>*S<rsub|<text|stat>>> of the statistical entropy
  function given by Eq. <reference|S=k ln W> is the same as the change
  <math|\<Delta\>*S> of the macroscopic second-law entropy, because the
  derivation of Eq. <reference|S=k ln W> is based on the macroscopic relation
  <math|<dvar|S<rsub|<text|stat>>>=<dbar|q>/T=<around|(|<dvar|U>-<dbar|w>|)>/T>
  with <math|<dvar|U>> and <math|<dbar|w>> given by statistical theory. If
  the integration constant <math|C> is set equal to zero,
  <math|S<rsub|<text|stat>>> becomes the third-law entropy <math|S> to be
  described in Chap. <reference|Chap. 6>.

  Equation <reference|S=k ln W> shows that a reversible process in which
  entropy increases is accompanied by an increase in the number of accessible
  microstates of equal, or nearly equal, internal energies. This
  interpretation of entropy increase has been described as the spreading and
  sharing of energy<footnote|Ref. <cite|leff-96>.> and as the dispersal of
  energy.<footnote|Ref. <cite|lambert-02a>.> It has even been proposed that
  entropy should be thought of as a \Pspreading function\Q with its symbol
  <math|S> suggesting <em|spreading>.<footnote|Ref. <cite|lambert-09>.>
  <footnote|The symbol <math|S> for entropy seems originally to have been an
  arbitrary choice by <index|Clausius, Rudolf>Clausius; see Ref.
  <cite|howard-01>.>

  \;

  <\bio-insert>
    <include|bio-PLANCK.tm>
  </bio-insert>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|3>
    <associate|page-first|86>
    <associate|preamble|false>
    <associate|section-nr|11>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|0=qh+qc+w|<tuple|4.3.1|93>>
    <associate|4-Carnot engines|<tuple|4.3|90>>
    <associate|4-Clausius|<tuple|4.2.2|87>>
    <associate|4-Clausius inequality|<tuple|4.4.1|99>>
    <associate|4-Clausius statement|<tuple|4.2.2|87>>
    <associate|4-adiabatic processes with work|<tuple|4.6.6|110>>
    <associate|4-alternative derivation|<tuple|4.4.3|103>>
    <associate|4-applications|<tuple|4.6|107>>
    <associate|4-irrev ad processes|<tuple|4.5.1|105>>
    <associate|4-irrev processes|<tuple|4.5|105>>
    <associate|4-irrev processes in general|<tuple|4.5.2|106>>
    <associate|4-properties of entropy|<tuple|4.4.4|104>>
    <associate|4-rev expansion|<tuple|4.6.2|108>>
    <associate|4-rev heating \ expansion|<tuple|4.6.1|107>>
    <associate|4-rev processes|<tuple|4.4|99>>
    <associate|4-rev processes to define entropy|<tuple|4.4.2|100>>
    <associate|4-second law statements|<tuple|4.2|85>>
    <associate|4-statistical|<tuple|4.8|111>>
    <associate|4-summary|<tuple|4.7|110>>
    <associate|4-thermodynamic temperature|<tuple|4.3.4|96>>
    <associate|4-types of processes|<tuple|4.1|85>>
    <associate|Chap. 4|<tuple|4|85>>
    <associate|Del S=Cln(T2/T1)|<tuple|4.6.4|108>>
    <associate|Del S=nRln(V2/V1)|<tuple|4.6.5|108>>
    <associate|S=k ln W|<tuple|4.8.1|111>>
    <associate|T replaces T_b|<tuple|Process|86>>
    <associate|Tc/Th=-qc/qh|<tuple|4.3.15|96>>
    <associate|auto-1|<tuple|4|85>>
    <associate|auto-10|<tuple|Process|85>>
    <associate|auto-100|<tuple|1|97|bio-KELVIN.tm>>
    <associate|auto-101|<tuple|Kelvin, Baron of Largs|97|bio-KELVIN.tm>>
    <associate|auto-102|<tuple|Thomson, William|97|bio-KELVIN.tm>>
    <associate|auto-103|<tuple|4.4|99>>
    <associate|auto-104|<tuple|4.4.1|99>>
    <associate|auto-105|<tuple|Supersystem|99>>
    <associate|auto-106|<tuple|4.4.1|99>>
    <associate|auto-107|<tuple|Heat|99>>
    <associate|auto-108|<tuple|Heat|99>>
    <associate|auto-109|<tuple|Heat|100>>
    <associate|auto-11|<tuple|Irreversible process|85>>
    <associate|auto-110|<tuple|Supersystem|100>>
    <associate|auto-111|<tuple|Supersystem|100>>
    <associate|auto-112|<tuple|Heat|100>>
    <associate|auto-113|<tuple|Kelvin--Planck statement of the second
    law|100>>
    <associate|auto-114|<tuple|Second law of thermodynamics|100>>
    <associate|auto-115|<tuple|Clausius|100>>
    <associate|auto-116|<tuple|Clausius inequality|100>>
    <associate|auto-117|<tuple|4.4.2|100>>
    <associate|auto-118|<tuple|<tuple|second law of
    thermodynamics|mathematical statement|derivation>|100>>
    <associate|auto-119|<tuple|Carath�odory's principle of adiabatic
    inaccessibility|101>>
    <associate|auto-12|<tuple|Process|85>>
    <associate|auto-120|<tuple|Work|101>>
    <associate|auto-121|<tuple|<tuple|independent variables|equilibrium
    state>|101>>
    <associate|auto-122|<tuple|Reversible|101>>
    <associate|auto-123|<tuple|reversible adiabatic surface|101>>
    <associate|auto-124|<tuple|Entropy|101>>
    <associate|auto-125|<tuple|entropy|101>>
    <associate|auto-126|<tuple|4.4.2|102>>
    <associate|auto-127|<tuple|Third law of thermodynamics|102>>
    <associate|auto-128|<tuple|4.4.3|102>>
    <associate|auto-129|<tuple|Supersystem|102>>
    <associate|auto-13|<tuple|Reversible|85>>
    <associate|auto-130|<tuple|Heat|102>>
    <associate|auto-131|<tuple|Supersystem|103>>
    <associate|auto-132|<tuple|Heat|103>>
    <associate|auto-133|<tuple|Second law of thermodynamics|103>>
    <associate|auto-134|<tuple|Integrating factor|103>>
    <associate|auto-135|<tuple|4.4.3|103>>
    <associate|auto-136|<tuple|<tuple|second law of
    thermodynamics|mathematical statement|derivation>|104>>
    <associate|auto-137|<tuple|4.4.4|104>>
    <associate|auto-138|<tuple|<tuple|entropy|extensive>|104>>
    <associate|auto-139|<tuple|<tuple|entropy|nonequilibrium>|104>>
    <associate|auto-14|<tuple|4.2|85>>
    <associate|auto-140|<tuple|4.5|105>>
    <associate|auto-141|<tuple|<tuple|irreversible process>|105>>
    <associate|auto-142|<tuple|<tuple|process|irreversible>|105>>
    <associate|auto-143|<tuple|4.5.1|105>>
    <associate|auto-144|<tuple|Energy|105>>
    <associate|auto-145|<tuple|Dissipation of energy|105>>
    <associate|auto-146|<tuple|4.5.2|106>>
    <associate|auto-147|<tuple|Supersystem|106>>
    <associate|auto-148|<tuple|Heat|106>>
    <associate|auto-149|<tuple|4.5.1|106>>
    <associate|auto-15|<tuple|Second law of thermodynamics|85>>
    <associate|auto-150|<tuple|Supersystem|106>>
    <associate|auto-151|<tuple|Supersystem|107>>
    <associate|auto-152|<tuple|<tuple|irreversible process>|107>>
    <associate|auto-153|<tuple|<tuple|process|irreversible>|107>>
    <associate|auto-154|<tuple|4.6|107>>
    <associate|auto-155|<tuple|Heat|107>>
    <associate|auto-156|<tuple|Second law of thermodynamics|107>>
    <associate|auto-157|<tuple|4.6.1|107>>
    <associate|auto-158|<tuple|<tuple|heating|reversible>|107>>
    <associate|auto-159|<tuple|<tuple|reversible|heating>|107>>
    <associate|auto-16|<tuple|mathematical statement of the second law|85>>
    <associate|auto-160|<tuple|<tuple|heating|reversible>|108>>
    <associate|auto-161|<tuple|<tuple|reversible|heating>|108>>
    <associate|auto-162|<tuple|4.6.2|108>>
    <associate|auto-163|<tuple|Expansion|108>>
    <associate|auto-164|<tuple|Reversible|108>>
    <associate|auto-165|<tuple|4.6.3|108>>
    <associate|auto-166|<tuple|<tuple|isolated system|spontaneous charges
    in>|108>>
    <associate|auto-167|<tuple|Process|108>>
    <associate|auto-168|<tuple|Irreversible process|108>>
    <associate|auto-169|<tuple|Clausius, Rudolf|108>>
    <associate|auto-17|<tuple|Entropy|86>>
    <associate|auto-170|<tuple|<tuple|isolated system|spontaneous charges
    in>|108>>
    <associate|auto-171|<tuple|4.6.4|109>>
    <associate|auto-172|<tuple|<tuple|heat|flow in an isolated system>|109>>
    <associate|auto-173|<tuple|Heat|109>>
    <associate|auto-174|<tuple|<tuple|heat|flow in an isolated system>|109>>
    <associate|auto-175|<tuple|4.6.5|109>>
    <associate|auto-176|<tuple|<tuple|free expansion>|109>>
    <associate|auto-177|<tuple|Process|109>>
    <associate|auto-178|<tuple|Irreversible process|109>>
    <associate|auto-179|<tuple|<tuple|free expansion>|109>>
    <associate|auto-18|<tuple|entropy|86>>
    <associate|auto-180|<tuple|4.6.6|110>>
    <associate|auto-181|<tuple|Process|110>>
    <associate|auto-182|<tuple|Adiabatic|110>>
    <associate|auto-183|<tuple|4.7|110>>
    <associate|auto-184|<tuple|Reversible|110>>
    <associate|auto-185|<tuple|Process|110>>
    <associate|auto-186|<tuple|Spontaneous process|110>>
    <associate|auto-187|<tuple|Process|110>>
    <associate|auto-188|<tuple|Irreversible process|110>>
    <associate|auto-189|<tuple|Process|110>>
    <associate|auto-19|<tuple|Reversible|86>>
    <associate|auto-190|<tuple|Process|110>>
    <associate|auto-191|<tuple|Energy|110>>
    <associate|auto-192|<tuple|Dissipation of energy|110>>
    <associate|auto-193|<tuple|Entropy|110>>
    <associate|auto-194|<tuple|4.7.1|110>>
    <associate|auto-195|<tuple|4.8|111>>
    <associate|auto-196|<tuple|<tuple|entropy|measure of disorder>|111>>
    <associate|auto-197|<tuple|Disorder|111>>
    <associate|auto-198|<tuple|Statistical mechanics|111>>
    <associate|auto-199|<tuple|Microstate|111>>
    <associate|auto-2|<tuple|4.1|85>>
    <associate|auto-20|<tuple|Process|86>>
    <associate|auto-200|<tuple|Statistical mechanics|111>>
    <associate|auto-201|<tuple|Heat|111>>
    <associate|auto-202|<tuple|Statistical mechanics|111>>
    <associate|auto-203|<tuple|Clausius, Rudolf|111>>
    <associate|auto-204|<tuple|1|112|bio-PLANCK.tm>>
    <associate|auto-205|<tuple|Planck, Max|112|bio-PLANCK.tm>>
    <associate|auto-21|<tuple|Clausius, Rudolf|86>>
    <associate|auto-22|<tuple|Kelvin, Baron of Largs|86>>
    <associate|auto-23|<tuple|Planck, Max|86>>
    <associate|auto-24|<tuple|<tuple|process|impossible>|86>>
    <associate|auto-25|<tuple|4.2.1|86>>
    <associate|auto-26|<tuple|Clausius, Rudolf|87>>
    <associate|auto-27|<tuple|Clausius|87>>
    <associate|auto-28|<tuple|Second law of thermodynamics|87>>
    <associate|auto-29|<tuple|4.2.2|87>>
    <associate|auto-3|<tuple|Spontaneous process|85>>
    <associate|auto-30|<tuple|Joule|87>>
    <associate|auto-31|<tuple|Paddle wheel|87>>
    <associate|auto-32|<tuple|Heat engine|87>>
    <associate|auto-33|<tuple|heat engine|87>>
    <associate|auto-34|<tuple|Heat|87>>
    <associate|auto-35|<tuple|Perpetual motion of the second kind|87>>
    <associate|auto-36|<tuple|Heat|87>>
    <associate|auto-37|<tuple|Thomson, William|87>>
    <associate|auto-38|<tuple|Kelvin, Baron of Largs|87>>
    <associate|auto-39|<tuple|Planck, Max|87>>
    <associate|auto-4|<tuple|Process|85>>
    <associate|auto-40|<tuple|Kelvin--Planck statement of the second law|87>>
    <associate|auto-41|<tuple|Second law of thermodynamics|87>>
    <associate|auto-42|<tuple|Heat|87>>
    <associate|auto-43|<tuple|<tuple|process|impossible>|88>>
    <associate|auto-44|<tuple|Lewis, Gilbert Newton|88>>
    <associate|auto-45|<tuple|Randall, Merle|88>>
    <associate|auto-46|<tuple|1|89|bio-CARNOT.tm>>
    <associate|auto-47|<tuple|Carnot, Sadi|89|bio-CARNOT.tm>>
    <associate|auto-48|<tuple|4.3|90>>
    <associate|auto-49|<tuple|4.3.1|90>>
    <associate|auto-5|<tuple|Reversible|85>>
    <associate|auto-50|<tuple|<tuple|carnot|engine>|90>>
    <associate|auto-51|<tuple|<tuple|carnot|cycle>|90>>
    <associate|auto-52|<tuple|Heat engine|90>>
    <associate|auto-53|<tuple|Carnot|90>>
    <associate|auto-54|<tuple|Carnot engine|90>>
    <associate|auto-55|<tuple|Carnot|90>>
    <associate|auto-56|<tuple|Carnot cycles|90>>
    <associate|auto-57|<tuple|Working substance|90>>
    <associate|auto-58|<tuple|4.3.1|90>>
    <associate|auto-59|<tuple|4.3.2|90>>
    <associate|auto-6|<tuple|Process|85>>
    <associate|auto-60|<tuple|Heat|90>>
    <associate|auto-61|<tuple|Heat|91>>
    <associate|auto-62|<tuple|Steam engine|91>>
    <associate|auto-63|<tuple|4.3.3|91>>
    <associate|auto-64|<tuple|Carnot|91>>
    <associate|auto-65|<tuple|Carnot heat pump|91>>
    <associate|auto-66|<tuple|<tuple|carnot|engine>|91>>
    <associate|auto-67|<tuple|<tuple|carnot|cycle>|91>>
    <associate|auto-68|<tuple|4.3.2|91>>
    <associate|auto-69|<tuple|<tuple|second law of thermodynamics|equivalence
    of clausius and kelvin-planck statements>|91>>
    <associate|auto-7|<tuple|Process|85>>
    <associate|auto-70|<tuple|1|92|bio-CLAUSIUS.tm>>
    <associate|auto-71|<tuple|Clausius, Rudolf|92|bio-CLAUSIUS.tm>>
    <associate|auto-72|<tuple|4.3.4|93>>
    <associate|auto-73|<tuple|<tuple|second law of thermodynamics|equivalence
    of clausius and kelvin-planck statements>|93>>
    <associate|auto-74|<tuple|4.3.3|93>>
    <associate|auto-75|<tuple|<tuple|efficiency|heat engine>|93>>
    <associate|auto-76|<tuple|efficiency|93>>
    <associate|auto-77|<tuple|<tuple|efficiency|carnot engine>|94>>
    <associate|auto-78|<tuple|4.3.5|94>>
    <associate|auto-79|<tuple|Working substance|95>>
    <associate|auto-8|<tuple|Process|85>>
    <associate|auto-80|<tuple|Heat|95>>
    <associate|auto-81|<tuple|Energy|95>>
    <associate|auto-82|<tuple|Dissipation of energy|95>>
    <associate|auto-83|<tuple|<tuple|efficiency|carnot engine>|95>>
    <associate|auto-84|<tuple|4.3.4|96>>
    <associate|auto-85|<tuple|<tuple|thermodynamic|temperature>|96>>
    <associate|auto-86|<tuple|<tuple|temperature|thermodynamic>|96>>
    <associate|auto-87|<tuple|Heat|96>>
    <associate|auto-88|<tuple|Kelvin, Baron of Largs|96>>
    <associate|auto-89|<tuple|Thermodynamic|96>>
    <associate|auto-9|<tuple|Spontaneous process|85>>
    <associate|auto-90|<tuple|Temperature|96>>
    <associate|auto-91|<tuple|thermodynamic temperature|96>>
    <associate|auto-92|<tuple|<tuple|temperature|ideal-gas>|96>>
    <associate|auto-93|<tuple|<tuple|temperature|ideal gas>|96>>
    <associate|auto-94|<tuple|Ideal-gas temperature|96>>
    <associate|auto-95|<tuple|Ideal-gas temperature|96>>
    <associate|auto-96|<tuple|<tuple|temperature|ideal gas>|96>>
    <associate|auto-97|<tuple|Heat|96>>
    <associate|auto-98|<tuple|<tuple|thermodynamic|temperature>|96>>
    <associate|auto-99|<tuple|<tuple|temperature|thermodynamic>|96>>
    <associate|bio:carnot|<tuple|1|89|bio-CARNOT.tm>>
    <associate|bio:clausius|<tuple|1|92|bio-CLAUSIUS.tm>>
    <associate|bio:kelvin|<tuple|1|97|bio-KELVIN.tm>>
    <associate|bio:planck|<tuple|1|112|bio-PLANCK.tm>>
    <associate|c4|<tuple|4|85>>
    <associate|c4 sec 2li|<tuple|4.5|105>>
    <associate|c4 sec 2li-adiabatic|<tuple|4.5.1|105>>
    <associate|c4 sec 2li-general|<tuple|4.5.2|106>>
    <associate|c4 sec 2lr|<tuple|4.4|99>>
    <associate|c4 sec 2lr-clausius|<tuple|4.4.1|99>>
    <associate|c4 sec apps|<tuple|4.6|107>>
    <associate|c4 sec apps-adiabatic-work|<tuple|4.6.6|110>>
    <associate|c4 sec apps-expansion-free|<tuple|4.6.5|109>>
    <associate|c4 sec apps-heating|<tuple|4.6.1|107>>
    <associate|c4 sec concepts|<tuple|4.3|90>>
    <associate|c4 sec concepts-carnot|<tuple|4.3.1|90>>
    <associate|c4 sec concepts-clausius-kelvin-planck|<tuple|4.3.2|91>>
    <associate|c4 sec concepts-temperature|<tuple|4.3.4|96>>
    <associate|c4 sec processes|<tuple|4.1|85>>
    <associate|c4 sec second-law|<tuple|4.2|85>>
    <associate|c4 sec summary|<tuple|4.7|110>>
    <associate|c4 sec-2lr-entropy-properties|<tuple|4.4.4|104>>
    <associate|c4 sec-2lr-entropy-reversible|<tuple|4.4.2|100>>
    <associate|c4 sec-2lr-entropy-state|<tuple|4.4.3|103>>
    <associate|c4 sec-apps-expansion-rev|<tuple|4.6.2|108>>
    <associate|c4 sec-apps-heatflow|<tuple|4.6.4|109>>
    <associate|c4 sec-apps-spontaneous|<tuple|4.6.3|108>>
    <associate|c4 sec-concepts-carnot-efficiency|<tuple|4.3.3|93>>
    <associate|c4 sec-entropy-statistical|<tuple|4.8|111>>
    <associate|dS \<gtr\>= dq/Tb|<tuple|4.6.1|107>>
    <associate|dS(res)=|<tuple|4.5.1|106>>
    <associate|dS(ss)=sum|<tuple|4.5.2|106>>
    <associate|dS\<gtr\>0 (irrev, isolated)|<tuple|4.6.6|108>>
    <associate|dS\<gtr\>int(dq/Tb)|<tuple|4.5.3|107>>
    <associate|dS=dq/Tb|<tuple|4.4.7|103>>
    <associate|dS=sum'(a)sum(b)|<tuple|4.6.8|109>>
    <associate|dS=sum(a)sum(b)|<tuple|4.6.7|109>>
    <associate|del S\<gtr\>int(dq/Tb)|<tuple|4.5.4|107>>
    <associate|del S=int(C/T)dT|<tuple|4.6.3|107>>
    <associate|delS \<gtr\>= dq/Tb|<tuple|4.6.2|107>>
    <associate|delS=int(dS)=int(dq/Tb)|<tuple|4.4.9|104>>
    <associate|delS=int(dq/Tb)|<tuple|4.4.6|103>>
    <associate|dq'=Tres(dq/Tb)|<tuple|4.4.1|100>>
    <associate|dq/T=CVdT/T+nRdV/V|<tuple|4.3.5|95>>
    <associate|dq=CVdT+(nRT/V)dV|<tuple|4.3.4|94>>
    <associate|efficiency\<less\>1|<tuple|4.3.3|94>>
    <associate|epsilon=1+qc/qh|<tuple|4.3.3|93>>
    <associate|epsilon=1-Tc/Th|<tuple|4.3.14|95>>
    <associate|fig:4-VT adiabats|<tuple|4.4.2|102>>
    <associate|fig:4-adiabatic_supersystem|<tuple|4.5.1|106>>
    <associate|fig:4-ig Carnot engine|<tuple|4.3.1|90>>
    <associate|fig:4-impossible1|<tuple|4.2.1|86>>
    <associate|fig:4-impossible2|<tuple|4.2.2|87>>
    <associate|fig:4-impossible3|<tuple|4.3.4|93>>
    <associate|fig:4-impossible4|<tuple|4.3.5|94>>
    <associate|fig:4-one cycle|<tuple|4.3.3|91>>
    <associate|fig:4-rev paths|<tuple|4.4.3|102>>
    <associate|fig:4-supersystem|<tuple|4.4.1|99>>
    <associate|fig:4-two paths|<tuple|4.7.1|110>>
    <associate|fig:4-water Carnot engine|<tuple|4.3.2|90>>
    <associate|footnote-4.2.1|<tuple|4.2.1|86>>
    <associate|footnote-4.2.2|<tuple|4.2.2|87>>
    <associate|footnote-4.2.3|<tuple|4.2.3|87>>
    <associate|footnote-4.2.4|<tuple|4.2.4|87>>
    <associate|footnote-4.2.5|<tuple|4.2.5|88>>
    <associate|footnote-4.2.6|<tuple|4.2.6|89|bio-CARNOT.tm>>
    <associate|footnote-4.2.7|<tuple|4.2.7|89|bio-CARNOT.tm>>
    <associate|footnote-4.3.1|<tuple|4.3.1|92|bio-CLAUSIUS.tm>>
    <associate|footnote-4.3.2|<tuple|4.3.2|92|bio-CLAUSIUS.tm>>
    <associate|footnote-4.3.3|<tuple|4.3.3|92|bio-CLAUSIUS.tm>>
    <associate|footnote-4.3.4|<tuple|4.3.4|92|bio-CLAUSIUS.tm>>
    <associate|footnote-4.3.5|<tuple|4.3.5|97|bio-KELVIN.tm>>
    <associate|footnote-4.3.6|<tuple|4.3.6|97|bio-KELVIN.tm>>
    <associate|footnote-4.3.7|<tuple|4.3.7|97|bio-KELVIN.tm>>
    <associate|footnote-4.4.1|<tuple|4.4.1|99>>
    <associate|footnote-4.4.2|<tuple|4.4.2|101>>
    <associate|footnote-4.4.3|<tuple|4.4.3|104>>
    <associate|footnote-4.4.4|<tuple|4.4.4|105>>
    <associate|footnote-4.6.1|<tuple|4.6.1|109>>
    <associate|footnote-4.8.1|<tuple|4.8.1|111>>
    <associate|footnote-4.8.2|<tuple|4.8.2|111>>
    <associate|footnote-4.8.3|<tuple|4.8.3|111>>
    <associate|footnote-4.8.4|<tuple|4.8.4|111>>
    <associate|footnote-4.8.5|<tuple|4.8.5|112|bio-PLANCK.tm>>
    <associate|footnote-4.8.6|<tuple|4.8.6|112|bio-PLANCK.tm>>
    <associate|footnr-4.2.1|<tuple|4.2.1|86>>
    <associate|footnr-4.2.2|<tuple|4.2.2|87>>
    <associate|footnr-4.2.3|<tuple|Perpetual motion of the second kind|87>>
    <associate|footnr-4.2.4|<tuple|4.2.4|87>>
    <associate|footnr-4.2.5|<tuple|4.2.5|88>>
    <associate|footnr-4.2.6|<tuple|4.2.6|89|bio-CARNOT.tm>>
    <associate|footnr-4.2.7|<tuple|4.2.7|89|bio-CARNOT.tm>>
    <associate|footnr-4.3.1|<tuple|4.3.1|92|bio-CLAUSIUS.tm>>
    <associate|footnr-4.3.2|<tuple|4.3.2|92|bio-CLAUSIUS.tm>>
    <associate|footnr-4.3.3|<tuple|4.3.3|92|bio-CLAUSIUS.tm>>
    <associate|footnr-4.3.4|<tuple|4.3.4|92|bio-CLAUSIUS.tm>>
    <associate|footnr-4.3.5|<tuple|4.3.5|97|bio-KELVIN.tm>>
    <associate|footnr-4.3.6|<tuple|4.3.6|97|bio-KELVIN.tm>>
    <associate|footnr-4.3.7|<tuple|4.3.7|97|bio-KELVIN.tm>>
    <associate|footnr-4.4.1|<tuple|4.4.1|99>>
    <associate|footnr-4.4.2|<tuple|4.4.2|101>>
    <associate|footnr-4.4.3|<tuple|4.4.3|104>>
    <associate|footnr-4.4.4|<tuple|4.4.4|105>>
    <associate|footnr-4.6.1|<tuple|4.6.1|109>>
    <associate|footnr-4.8.1|<tuple|4.8.1|111>>
    <associate|footnr-4.8.2|<tuple|4.8.2|111>>
    <associate|footnr-4.8.3|<tuple|4.8.3|111>>
    <associate|footnr-4.8.4|<tuple|Clausius, Rudolf|111>>
    <associate|footnr-4.8.5|<tuple|4.8.5|112|bio-PLANCK.tm>>
    <associate|footnr-4.8.6|<tuple|4.8.6|112|bio-PLANCK.tm>>
    <associate|impossible process defn|<tuple|Process|85>>
    <associate|ln(V2/V1)=-ln(V4/V3)|<tuple|4.3.9|95>>
    <associate|meaning of ir|<tuple|4.6.1|107>>
    <associate|oint(dq(rev)/Tb)=0|<tuple|4.4.8|104>>
    <associate|oint(dq/Tb)\<less\>=0|<tuple|4.4.3|100>>
    <associate|part:bio-CARNOT.tm|<tuple|4.2.5|89>>
    <associate|part:bio-CLAUSIUS.tm|<tuple|<tuple|second law of
    thermodynamics|equivalence of clausius and kelvin-planck statements>|92>>
    <associate|part:bio-KELVIN.tm|<tuple|<tuple|temperature|thermodynamic>|97>>
    <associate|part:bio-PLANCK.tm|<tuple|Clausius, Rudolf|112>>
    <associate|q'(ss)=int(CD)|<tuple|4.4.5|103>>
    <associate|q'=(Tres)int(dq/Tb)|<tuple|4.4.2|100>>
    <associate|q(ss)=int(AB)|<tuple|4.4.4|102>>
    <associate|qc/qh=-Tc/Th|<tuple|4.3.13|95>>
    <associate|rev ad surface|<tuple|reversible adiabatic surface|101>>
    <associate|second law box|<tuple|mathematical statement of the second
    law|85>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      clausius-1854

      planck-22

      lewis-23

      carnot-1824

      mendoza-88

      planck-48

      clausius-1854

      clausius-1865

      clausius-1865

      kelvin-1848

      bottomley-1882

      everitt-67

      hats-65

      pippard-66

      pauli-73

      adkins-83

      norton-16

      leff-77

      leff-96

      lambert-02a

      lambert-09

      howard-01

      planck-22

      planck-48
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.2.1>|>
        Two impossible processes in isolated systems.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Heat
        transfer from a cool to a warm body.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>The
        same, with a device that operates in a cycle.
      </surround>|<pageref|auto-25>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.2.2>|>
        Two more impossible processes.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>A
        weight rises as a liquid becomes cooler.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>The
        same, with a heat engine.
      </surround>|<pageref|auto-29>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.3.1>|>
        Indicator diagram for a Carnot engine using an ideal gas as the
        working substance. In this example,
        <with|mode|<quote|math>|T<rsub|<with|mode|<quote|text>|h>>=400
        <with|mode|<quote|text>|K>>, <with|mode|<quote|math>|T<rsub|<with|mode|<quote|text>|c>>=300
        <with|mode|<quote|text>|K>>, <with|mode|<quote|math>|\<epsilon\>=1/4>,
        <with|mode|<quote|math>|C<rsub|V,m>=<around|(|3/2|)>*R>,
        <with|mode|<quote|math>|n=2.41 <with|mode|<quote|text>|mmol>>. The
        processes of paths A<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>B
        and C<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>D
        are isothermal; those of paths B<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>C,
        B<with|mode|<quote|math>|<rprime|'><with|mode|<quote|math>|\<rightarrow\>>>C<with|mode|<quote|math>|<rprime|'>>,
        and D<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>A
        are adiabatic. The cycle A<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>B<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>C<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>D<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>A
        has net work <with|mode|<quote|math>|w=-1.0
        <with|mode|<quote|text>|J>>; the cycle
        A<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>B<with|mode|<quote|math>|<rprime|'><with|mode|<quote|math>|\<rightarrow\>>>C<with|mode|<quote|math>|<rprime|'><with|mode|<quote|math>|\<rightarrow\>>>D<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>A
        has net work <with|mode|<quote|math>|w=-0.5
        <with|mode|<quote|text>|J>>.
      </surround>|<pageref|auto-58>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.3.2>|>
        Indicator diagram for a Carnot engine using
        H<rsub|<with|mode|<quote|math>|2>>O as the working substance. In this
        example, <with|mode|<quote|math>|T<rsub|<with|mode|<quote|text>|h>>=400
        <with|mode|<quote|text>|K>>, <with|mode|<quote|math>|T<rsub|<with|mode|<quote|text>|c>>=396
        <with|mode|<quote|text>|K>>, <with|mode|<quote|math>|\<epsilon\>=1/100>,
        <with|mode|<quote|math>|w=-1.0 <with|mode|<quote|text>|J>>. In state
        A, the system consists of one mole of
        H<rsub|<with|mode|<quote|math>|2>>O(l). The processes (all carried
        out reversibly) are: A<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>B,
        vaporization of <with|mode|<quote|math>|2.54
        <with|mode|<quote|text>|mmol>> H<rsub|<with|mode|<quote|math>|2>>O at
        <with|mode|<quote|math>|400 <with|mode|<quote|text>|K>>;
        B<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>C,
        adiabatic expansion, causing vaporization of an additional
        <with|mode|<quote|math>|7.68 <with|mode|<quote|text>|mmol>>;
        C<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>D,
        condensation of <with|mode|<quote|math>|2.50
        <with|mode|<quote|text>|mmol>> at <with|mode|<quote|math>|396
        <with|mode|<quote|text>|K>>; D<with|mode|<quote|math>|<with|mode|<quote|math>|\<rightarrow\>>>A,
        adiabatic compression returning the system to the initial state.
      </surround>|<pageref|auto-59>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.3.3>|>
        (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>One
        cycle of a Carnot engine that does work on the surroundings.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>The
        same system run in reverse as a Carnot heat pump.

        \ Figures <reference|fig:4-one cycle>--<reference|fig:4-impossible4>
        use the following symbols: A square box represents a system (a Carnot
        engine or Carnot heat pump). Vertical arrows indicate heat and
        horizontal arrows indicate work; each arrow shows the direction of
        energy transfer into or out of the system. The number next to each
        arrow is an absolute value of <with|mode|<quote|math>|q>/J or
        <with|mode|<quote|math>|w>/J in the cycle. For example, (a) shows
        <with|mode|<quote|math>|4> joules of heat transferred to the system
        from the hot reservoir, <with|mode|<quote|math>|3> joules of heat
        transferred from the system to the cold reservoir, and
        <with|mode|<quote|math>|1> joule of work done by the system on the
        surroundings.
      </surround>|<pageref|auto-63>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.3.4>|>
        (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>A
        Clausius device combined with the Carnot engine of Fig.
        <reference|fig:4-one cycle>(a).

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>The
        resulting impossible Kelvin--Planck engine.

        \ (c) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>A
        Kelvin--Planck engine combined with the Carnot heat pump of Fig.
        <reference|fig:4-one cycle>(b).

        \ (d) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>The
        resulting impossible Clausius device.
      </surround>|<pageref|auto-72>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.3.5>|>
        (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>A
        Carnot engine of efficiency <with|mode|<quote|math>|\<epsilon\>=1/4>
        combined with a Carnot engine of efficiency
        <with|mode|<quote|math>|\<epsilon\>=1/5> run in reverse.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>The
        resulting impossible Clausius device.

        \ (c) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>A
        Carnot engine of efficiency <with|mode|<quote|math>|\<epsilon\>=1/3>
        combined with the Carnot engine of efficiency
        <with|mode|<quote|math>|\<epsilon\>=1/4> run in reverse.

        \ (d) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>The
        resulting impossible Clausius device.
      </surround>|<pageref|auto-78>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.4.1>|>
        Experimental system, Carnot engine (represented by a small square
        box), and heat reservoir. The dashed lines indicate the boundary of
        the supersystem.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Reversible
        heat transfer between heat reservoir and Carnot engine.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Heat
        transfer between Carnot engine and experimental system. The
        infinitesimal quantities <with|mode|<quote|math>|<with|mode|<quote|math>|<with|mode|<quote|text>|�>*q><rprime|'>>
        and <with|mode|<quote|math>|<with|mode|<quote|math>|<with|mode|<quote|text>|�>*q>>
        are positive for transfer in the directions indicated by the arrows.
      </surround>|<pageref|auto-106>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.4.2>|>
        A family of reversible adiabatic curves (two-dimensional reversible
        adiabatic surfaces) for an ideal gas with <with|mode|<quote|math>|V>
        and <with|mode|<quote|math>|T> as independent variables. A reversible
        adiabatic process moves the state of the system along a curve,
        whereas a reversible process with positive heat moves the state from
        one curve to another above and to the right. The curves are
        calculated for <with|mode|<quote|math>|n=1
        <with|mode|<quote|text>|mol>> and
        <with|mode|<quote|math>|C<rsub|V,m>=<around|(|3/2|)>*R>. Adjacent
        curves differ in entropy by <with|mode|<quote|math>|1
        <frac|<with|mode|<quote|text>|J>|<with|mode|<quote|text>|K>>>.
      </surround>|<pageref|auto-126>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.4.3>|>
        Reversible paths in <with|mode|<quote|math>|V>--<with|mode|<quote|math>|T>
        space. The thin curves are reversible adiabatic surfaces.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Two
        paths connecting the same pair of reversible adiabatic surfaces.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>A
        cyclic path.
      </surround>|<pageref|auto-128>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.5.1>|>
        Supersystem including the experimental system, a Carnot engine
        (square box), and a heat reservoir. The dashed rectangle indicates
        the boundary of the supersystem.
      </surround>|<pageref|auto-149>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.7.1>|>
        Reversible and irreversible paths between the same initial and final
        equilibrium states of a closed system. The value of
        <with|mode|<quote|math>|\<Delta\>*S> is the same for both paths, but
        the values of the integral <with|mode|<quote|math>|<big|int><around|(|<with|mode|<quote|math>|<with|mode|<quote|text>|�>*q>/T<rsub|<with|mode|<quote|text>|b>>|)>>
        are different.
      </surround>|<pageref|auto-194>>
    </associate>
    <\associate|gly>
      <tuple|normal|mathematical statement of the second
      law|<pageref|auto-16>>

      <tuple|normal|entropy|<pageref|auto-18>>

      <tuple|normal|heat engine|<pageref|auto-33>>

      <tuple|normal|Carnot engine|<pageref|auto-54>>

      <tuple|normal|Carnot cycles|<pageref|auto-56>>

      <tuple|normal|Carnot heat pump|<pageref|auto-65>>

      <tuple|normal|efficiency|<pageref|auto-76>>

      <tuple|normal|thermodynamic temperature|<pageref|auto-91>>

      <tuple|normal|Clausius inequality|<pageref|auto-116>>

      <tuple|normal|reversible adiabatic surface|<pageref|auto-123>>

      <tuple|normal|entropy|<pageref|auto-125>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Spontaneous process>|<pageref|auto-3>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-4>>

      <tuple|<tuple|Reversible|process>|<pageref|auto-5>>

      <tuple|<tuple|Process|reversible>|<pageref|auto-6>>

      <tuple|<tuple|Process|impossible>|<pageref|auto-7>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-8>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-9>>

      <tuple|<tuple|Process|irreversible>|<pageref|auto-10>>

      <tuple|<tuple|Irreversible process>|<pageref|auto-11>>

      <tuple|<tuple|Process|reversible>|<pageref|auto-12>>

      <tuple|<tuple|Reversible|process>|<pageref|auto-13>>

      <tuple|<tuple|Second law of thermodynamics|mathematical
      statement>|<pageref|auto-15>>

      <tuple|<tuple|Entropy>|<pageref|auto-17>>

      <tuple|<tuple|Reversible|process>|<pageref|auto-19>>

      <tuple|<tuple|Process|reversible>|<pageref|auto-20>>

      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-21>>

      <tuple|<tuple|Kelvin, Baron of Largs>|<pageref|auto-22>>

      <tuple|<tuple|Planck, Max>|<pageref|auto-23>>

      <tuple|<tuple|process|impossible>||c4 sec second-law proc-impos
      idx1|<tuple|Process|impossible>|<pageref|auto-24>>

      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-26>>

      <tuple|<tuple|Clausius|statement of the second law>|<pageref|auto-27>>

      <tuple|<tuple|Second law of thermodynamics|Clausius
      statement>|<pageref|auto-28>>

      <tuple|<tuple|Joule|paddle wheel>|<pageref|auto-30>>

      <tuple|<tuple|Paddle wheel|Joule>|<pageref|auto-31>>

      <tuple|<tuple|Heat engine>|<pageref|auto-32>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-34>>

      <tuple|<tuple|Perpetual motion of the second kind>|<pageref|auto-35>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-36>>

      <tuple|<tuple|Thomson, William>|<pageref|auto-37>>

      <tuple|<tuple|Kelvin, Baron of Largs>|<pageref|auto-38>>

      <tuple|<tuple|Planck, Max>|<pageref|auto-39>>

      <tuple|<tuple|Kelvin--Planck statement of the second
      law>|<pageref|auto-40>>

      <tuple|<tuple|Second law of thermodynamics|Kelvin--Planck
      statement>|<pageref|auto-41>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-42>>

      <tuple|<tuple|process|impossible>||c4 sec second-law proc-impos
      idx1|<tuple|Process|impossible>|<pageref|auto-43>>

      <tuple|<tuple|Lewis, Gilbert Newton>|<pageref|auto-44>>

      <tuple|<tuple|Randall, Merle>|<pageref|auto-45>>

      <tuple|<tuple|Carnot, Sadi>|<pageref|auto-47>>

      <tuple|<tuple|carnot|engine>||c4 sec concepts-carnot
      idx1|<tuple|Carnot|engine>|<pageref|auto-50>>

      <tuple|<tuple|carnot|cycle>||c4 sec concepts-carnot
      idx2|<tuple|Carnot|cycle>|<pageref|auto-51>>

      <tuple|<tuple|Heat engine>|<pageref|auto-52>>

      <tuple|<tuple|Carnot|engine>|<pageref|auto-53>>

      <tuple|<tuple|Carnot|cycle>|<pageref|auto-55>>

      <tuple|<tuple|Working substance>|<pageref|auto-57>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-60>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-61>>

      <tuple|<tuple|Steam engine>|<pageref|auto-62>>

      <tuple|<tuple|Carnot|heat pump>|<pageref|auto-64>>

      <tuple|<tuple|carnot|engine>||c4 sec concepts-carnot
      idx1|<tuple|Carnot|engine>|<pageref|auto-66>>

      <tuple|<tuple|carnot|cycle>||c4 sec concepts-carnot
      idx2|<tuple|Carnot|cycle>|<pageref|auto-67>>

      <tuple|<tuple|second law of thermodynamics|equivalence of clausius and
      kelvin-planck statements>||c4 sec concepts-clausius-kelvin-planck
      idx1|<tuple|Second law of thermodynamics|equivalence of Clausius and
      Kelvin\UPlanck statements>|<pageref|auto-69>>

      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-71>>

      <tuple|<tuple|second law of thermodynamics|equivalence of clausius and
      kelvin-planck statements>||c4 sec concepts-clausius-kelvin-planck
      idx1|<tuple|Second law of thermodynamics|equivalence of Clausius and
      Kelvin\UPlanck statements>|<pageref|auto-73>>

      <tuple|<tuple|efficiency|heat engine>|||<tuple|Efficiency|of a heat
      engine>|<pageref|auto-75>>

      <tuple|<tuple|efficiency|carnot engine>||c4
      sec-concepts-carnot-efficiency idx1|<tuple|Efficiency|of a Carnot
      engine>|<pageref|auto-77>>

      <tuple|<tuple|Working substance>|<pageref|auto-79>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-80>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-81>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-82>>

      <tuple|<tuple|efficiency|carnot engine>||c4
      sec-concepts-carnot-efficiency idx1|<tuple|Efficiency|of a Carnot
      engine>|<pageref|auto-83>>

      <tuple|<tuple|thermodynamic|temperature>||c4 sec concepts-temperature
      idx1|<tuple|Thermodynamic|temperature>|<pageref|auto-85>>

      <tuple|<tuple|temperature|thermodynamic>||c4 sec concepts-temperature
      idx2|<tuple|Temperature|thermodynamic>|<pageref|auto-86>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-87>>

      <tuple|<tuple|Kelvin, Baron of Largs>|<pageref|auto-88>>

      <tuple|<tuple|Thermodynamic|temperature>|<pageref|auto-89>>

      <tuple|<tuple|Temperature|thermodynamic>|<pageref|auto-90>>

      <tuple|<tuple|temperature|ideal-gas>|||<tuple|Temperature|ideal
      gas>|<pageref|auto-92>>

      <tuple|<tuple|temperature|ideal gas>|||<tuple|Temperature|ideal-gas>|<pageref|auto-93>>

      <tuple|<tuple|Ideal-gas temperature>|<pageref|auto-94>>

      <tuple|<tuple|Ideal-gas temperature>|<pageref|auto-95>>

      <tuple|<tuple|temperature|ideal gas>|||<tuple|Temperature|ideal-gas>|<pageref|auto-96>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-97>>

      <tuple|<tuple|thermodynamic|temperature>||c4 sec concepts-temperature
      idx1|<tuple|Thermodynamic|temperature>|<pageref|auto-98>>

      <tuple|<tuple|temperature|thermodynamic>||c4 sec concepts-temperature
      idx2|<tuple|Temperature|thermodynamic>|<pageref|auto-99>>

      <tuple|<tuple|Kelvin, Baron of Largs>|<pageref|auto-101>>

      <tuple|<tuple|Thomson, William>|<pageref|auto-102>>

      <tuple|<tuple|Supersystem>|<pageref|auto-105>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-107>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-108>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-109>>

      <tuple|<tuple|Supersystem>|<pageref|auto-110>>

      <tuple|<tuple|Supersystem>|<pageref|auto-111>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-112>>

      <tuple|<tuple|Kelvin--Planck statement of the second
      law>|<pageref|auto-113>>

      <tuple|<tuple|Second law of thermodynamics|Kelvin--Planck
      statement>|<pageref|auto-114>>

      <tuple|<tuple|Clausius|inequality>|<pageref|auto-115>>

      <tuple|<tuple|second law of thermodynamics|mathematical
      statement|derivation>||c4 sec-2lr-entropy-reversible idx1|<tuple|Second
      law of thermodynamics|mathematical statement|derivation>|<pageref|auto-118>>

      <tuple|<tuple|Carath�odory's principle of adiabatic
      inaccessibility>|<pageref|auto-119>>

      <tuple|<tuple|Work|coordinate>|<pageref|auto-120>>

      <tuple|<tuple|independent variables|equilibrium
      state>|||<tuple|Independent variables|of an equilibrium
      state>|<pageref|auto-121>>

      <tuple|<tuple|Reversible|adiabatic surface>|<pageref|auto-122>>

      <tuple|<tuple|Entropy>|<pageref|auto-124>>

      <tuple|<tuple|Third law of thermodynamics>|<pageref|auto-127>>

      <tuple|<tuple|Supersystem>|<pageref|auto-129>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-130>>

      <tuple|<tuple|Supersystem>|<pageref|auto-131>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-132>>

      <tuple|<tuple|Second law of thermodynamics|Kelvin--Planck
      statement>|<pageref|auto-133>>

      <tuple|<tuple|Integrating factor>|<pageref|auto-134>>

      <tuple|<tuple|second law of thermodynamics|mathematical
      statement|derivation>||c4 sec-2lr-entropy-reversible idx1|<tuple|Second
      law of thermodynamics|mathematical statement|derivation>|<pageref|auto-136>>

      <tuple|<tuple|entropy|extensive>|||<tuple|Entropy|an extensive
      property>|<pageref|auto-138>>

      <tuple|<tuple|entropy|nonequilibrium>|||<tuple|Entropy|of a
      nonequilibrium state>|<pageref|auto-139>>

      <tuple|<tuple|irreversible process>||c4 sec 2li
      idx1|<tuple|Irreversible process>|<pageref|auto-141>>

      <tuple|<tuple|process|irreversible>||c4 sec 2li
      idx2|<tuple|Process|irreversible>|<pageref|auto-142>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-144>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-145>>

      <tuple|<tuple|Supersystem>|<pageref|auto-147>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-148>>

      <tuple|<tuple|Supersystem>|<pageref|auto-150>>

      <tuple|<tuple|Supersystem>|<pageref|auto-151>>

      <tuple|<tuple|irreversible process>||c4 sec 2li
      idx1|<tuple|Irreversible process>|<pageref|auto-152>>

      <tuple|<tuple|process|irreversible>||c4 sec 2li
      idx2|<tuple|Process|irreversible>|<pageref|auto-153>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-155>>

      <tuple|<tuple|Second law of thermodynamics|mathematical
      statement>|<pageref|auto-156>>

      <tuple|<tuple|heating|reversible>||c4 sec apps-heating
      idx1|<tuple|Heating|reversible>|<pageref|auto-158>>

      <tuple|<tuple|reversible|heating>||c4 sec apps-heating
      idx2|<tuple|Reversible|heating>|<pageref|auto-159>>

      <tuple|<tuple|heating|reversible>||c4 sec apps-heating
      idx1|<tuple|Heating|reversible>|<pageref|auto-160>>

      <tuple|<tuple|reversible|heating>||c4 sec apps-heating
      idx2|<tuple|Reversible|heating>|<pageref|auto-161>>

      <tuple|<tuple|Expansionreversible, of an ideal
      gas|<uninit>>|<pageref|auto-163>>

      <tuple|<tuple|Reversible|expansion of an ideal gas>|<pageref|auto-164>>

      <tuple|<tuple|isolated system|spontaneous charges in>||c4
      sec-apps-spontaneous|<tuple|Isolated system|spontaneous changes
      in>|<pageref|auto-166>>

      <tuple|<tuple|Process|irreversible>|<pageref|auto-167>>

      <tuple|<tuple|Irreversible process>|<pageref|auto-168>>

      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-169>>

      <tuple|<tuple|isolated system|spontaneous charges in>||c4
      sec-apps-spontaneous|<tuple|Isolated system|spontaneous changes
      in>|<pageref|auto-170>>

      <tuple|<tuple|heat|flow in an isolated system>||c4 sec-apps-heatflow
      idx1|<tuple|Heat|flow in an isolated system>|<pageref|auto-172>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-173>>

      <tuple|<tuple|heat|flow in an isolated system>||c4 sec-apps-heatflow
      idx1|<tuple|Heat|flow in an isolated system>|<pageref|auto-174>>

      <tuple|<tuple|free expansion>||c4 sec apps-expansion-free
      idx1|<tuple|Free expansion>|<pageref|auto-176>>

      <tuple|<tuple|Process|irreversible>|<pageref|auto-177>>

      <tuple|<tuple|Irreversible process>|<pageref|auto-178>>

      <tuple|<tuple|free expansion>||c4 sec apps-expansion-free
      idx1|<tuple|Free expansion>|<pageref|auto-179>>

      <tuple|<tuple|Process|adiabatic>|<pageref|auto-181>>

      <tuple|<tuple|Adiabatic|process>|<pageref|auto-182>>

      <tuple|<tuple|Reversible|process>|<pageref|auto-184>>

      <tuple|<tuple|Process|reversible>|<pageref|auto-185>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-186>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-187>>

      <tuple|<tuple|Irreversible process>|<pageref|auto-188>>

      <tuple|<tuple|Process|irreversible>|<pageref|auto-189>>

      <tuple|<tuple|Process|mechanical>|<pageref|auto-190>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-191>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-192>>

      <tuple|<tuple|Entropy>|<pageref|auto-193>>

      <tuple|<tuple|entropy|measure of disorder>|||<tuple|Entropy|as a
      measure of disorder>|<pageref|auto-196>>

      <tuple|<tuple|Disorder>|<pageref|auto-197>>

      <tuple|<tuple|Statistical mechanics>|<pageref|auto-198>>

      <tuple|<tuple|Microstate>|<pageref|auto-199>>

      <tuple|<tuple|Statistical mechanics>|<pageref|auto-200>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-201>>

      <tuple|<tuple|Statistical mechanics>|<pageref|auto-202>>

      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-203>>

      <tuple|<tuple|Planck, Max>|<pageref|auto-205>>
    </associate>
    <\associate|parts>
      <tuple|bio-CARNOT.tm|chapter-nr|4|section-nr|2|subsection-nr|0>

      <tuple|bio-CLAUSIUS.tm|chapter-nr|4|section-nr|3|subsection-nr|2>

      <tuple|bio-KELVIN.tm|chapter-nr|4|section-nr|3|subsection-nr|4>

      <tuple|bio-PLANCK.tm|chapter-nr|4|section-nr|8|subsection-nr|0>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>The
      Second Law> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      4.1<space|2spc>Types of Processes <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      4.2<space|2spc>Statements of the Second Law
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Sadi
      Carnot> (1796\U1832) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-46><vspace|0.15fn>>

      4.3<space|2spc>Concepts Developed with Carnot Engines
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-48>

      <with|par-left|<quote|1tab>|4.3.1<space|2spc>Carnot engines and Carnot
      cycles <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-49>>

      <with|par-left|<quote|1tab>|4.3.2<space|2spc>The equivalence of the
      Clausius and Kelvin\UPlanck statements
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-68>>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Rudolf
      Julius Emmanuel Clausius> (1822\U1888)
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-70><vspace|0.15fn>>

      <with|par-left|<quote|1tab>|4.3.3<space|2spc>The efficiency of a Carnot
      engine <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-74>>

      <with|par-left|<quote|1tab>|4.3.4<space|2spc>Thermodynamic temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-84>>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|William
      Thomson, Lord Kelvin> (1824\U1907) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-100><vspace|0.15fn>>

      4.4<space|2spc>The Second Law for Reversible Processes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-103>

      <with|par-left|<quote|1tab>|4.4.1<space|2spc>The Clausius inequality
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-104>>

      <with|par-left|<quote|1tab>|4.4.2<space|2spc>Using reversible processes
      to define the entropy <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-117>>

      <with|par-left|<quote|1tab>|4.4.3<space|2spc>Alternative derivation of
      entropy as a state function <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-135>>

      <with|par-left|<quote|1tab>|4.4.4<space|2spc>Some properties of the
      entropy <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-137>>

      4.5<space|2spc>The Second Law for Irreversible Processes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-140>

      <with|par-left|<quote|1tab>|4.5.1<space|2spc>Irreversible adiabatic
      processes <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-143>>

      <with|par-left|<quote|1tab>|4.5.2<space|2spc>Irreversible processes in
      general <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-146>>

      4.6<space|2spc>Applications <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-154>

      <with|par-left|<quote|1tab>|4.6.1<space|2spc>Reversible heating
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-157>>

      <with|par-left|<quote|1tab>|4.6.2<space|2spc>Reversible expansion of an
      ideal gas <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-162>>

      <with|par-left|<quote|1tab>|4.6.3<space|2spc>Spontaneous changes in an
      isolated system <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-165>>

      <with|par-left|<quote|1tab>|4.6.4<space|2spc>Internal heat flow in an
      isolated system <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-171>>

      <with|par-left|<quote|1tab>|4.6.5<space|2spc>Free expansion of a gas
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-175>>

      <with|par-left|<quote|1tab>|4.6.6<space|2spc>Adiabatic process with
      work <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-180>>

      4.7<space|2spc>Summary <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-183>

      4.8<space|2spc>The Statistical Interpretation of Entropy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-195>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Max
      Karl Ernst Ludwig Planck> (1858\U1947)
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-204><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>