<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <no-indent>BIOGRAPHICAL SKETCH

  <paragraph|<person|William Francis Giauque> (1895\U1982)>

  <\padded-center>
    <tabular|<tformat|<cwith|1|1|2|2|cell-hyphen|t>|<table|<row|<cell|<image|BIO/giauque.png|83pt|115pt||>>|<\cell>
      <\rotate|90>
        <\really-tiny>
          <\with|color|#a0a0a0>
            <\text>
              AIP EMILIO SEGR� VISUAL ARCHIVES

              BRITTLE BOOKOS COLLECTION
            </text>
          </with>

          \;
        </really-tiny>

        \;
      </rotate>
    </cell>>>>>
  </padded-center>

  <label|bio:giauque><index|Giauque, William><no-indent>William Giauque was
  an American chemist who made important contributions to the field of
  cryogenics. He received the 1949 Nobel Prize in Chemistry \Pfor his
  contributions in the field of chemical thermodynamics, particularly
  concerning the behaviour of substances at extremely low temperatures.\Q

  Giauque was born in Niagara Falls, Ontario, Canada, but as his father was a
  citizen of the United States, William was able to adopt American
  citizenship. His father worked as a weighmaster and station agent for the
  Michigan Central Railroad in Michigan.

  Giauque's initial career goal after high school was electrical engineering.
  After working for two years at the Hooker Electrochemical Company in
  Niagara Falls, New York, he left to continue his education with the idea of
  becoming a chemical engineer. Hearing of the scientific reputation of G. N.
  Lewis, the chair of the chemistry department at the University of
  California at Berkeley (page <pageref|bio:lewis>), and motivated by the
  free tuition there, he enrolled instead in that department.<footnote|Ref.
  <cite|pitzer-96>.>

  Giauque spent the rest of his life in the chemistry department at Berkeley,
  first as an undergraduate; then as a graduate student; and finally, after
  receiving his Ph.D. in 1922, as a faculty member. Some of his undergraduate
  courses were in engineering, which later helped him in the design and
  construction of the heavy equipment for producing the high magnetic fields
  and the liquid hydrogen and helium needed in his research.

  Beginning in 1928, with the help of his graduate students and
  collaborators, he began to publish papers on various aspects of the third
  law.<footnote|Ref. <cite|stranges-90>.> The research included the
  evaluation of third-law molar entropies and comparison to molar entropies
  calculated from spectroscopic data, and the study of the residual entropy
  of crystals. Faint unexplained lines in the absorption spectrum of gaseous
  oxygen led him to the discovery of the previously unknown <rsup|<math|17>>O
  and <rsup|<math|18>>O isotopes of oxygen.

  Giauque's best-known accomplishment is his invention and exploitation of
  cooling to very low temperatures by adiabatic demagnetization. In 1924, he
  learned of the unusual properties of gadolinium sulfate octahydrate at the
  temperature of liquid helium. In his Nobel Lecture, he said:<footnote|Ref.
  <cite|giauque-49>.>

  <quotation|I was greatly surprised to find, that the application of a
  magnetic field removes a large amount of entropy from this substance, at a
  temperature so low that it had been thought that there was practically no
  entropy left to remove. ...Those familiar with thermodynamics will realize
  that in principle any process involving an entropy change may be used to
  produce either cooling or heating. Accordingly it occurred to me that
  adiabatic demagnetization could be made the basis of a method for producing
  temperatures lower than those obtainable with liquid helium.>

  It wasn't until 1933 that he was able to build the equipment and publish
  the results from his first adiabatic demagnetization experiment, reporting
  a temperature of <math|0.25<K>>.<footnote|Ref. <cite|giauque-33>.>

  He was married to a botanist and had two sons. According to one
  biography:<footnote|Ref. <cite|pitzer-96>.>

  <quotation|Giauque's students remember pleasant Thanksgiving dinners at the
  Giauque home, with Muriel [his wife] as cook and Frank (as she called him)
  as raconteur, with a keen sense of humor. The stories he most enjoyed
  telling were those in which the joke was on him. ...Giauque's conservatism
  was legendary. He always appeared at the university dressed in an iron-gray
  tweed suit. ...A dominant personality himself, Giauque not only tolerated
  but respected students who disagreed with him, and he was especially
  pleased when they could prove their point.>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|6>
    <associate|font-base-size|9>
    <associate|page-first|128>
    <associate|page-medium|paper>
    <associate|par-columns|2>
    <associate|par-columns-sep|1fn>
    <associate|preamble|false>
    <associate|section-nr|3>
    <associate|subsection-nr|2>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|128>>
    <associate|auto-2|<tuple|Giauque, William|128>>
    <associate|bio:giauque|<tuple|1|128>>
    <associate|footnote-1|<tuple|1|128>>
    <associate|footnote-2|<tuple|2|128>>
    <associate|footnote-3|<tuple|3|128>>
    <associate|footnote-4|<tuple|4|128>>
    <associate|footnote-5|<tuple|5|128>>
    <associate|footnr-1|<tuple|1|128>>
    <associate|footnr-2|<tuple|2|128>>
    <associate|footnr-3|<tuple|3|128>>
    <associate|footnr-4|<tuple|4|128>>
    <associate|footnr-5|<tuple|5|128>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      pitzer-96

      stranges-90

      giauque-49

      giauque-33

      pitzer-96
    </associate>
    <\associate|idx>
      <tuple|<tuple|Giauque, William>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|William
      Francis Giauque> (1895\U1982) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>