<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk|comment>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <appendix|Symbols for Physical Quantities><label|app:sym>

  <index-complex|<tuple|symbols for physical quantities>||app:sym
  idx1|<tuple|Symbols physical quantities>><index-complex|<tuple|physical
  quantities, symbols for>||app:sym idx2|<tuple|Physical quantities, symbols
  for>>This appendix lists the symbols for most of the variable physical
  quantities used in this book. The symbols are those recommended in the
  IUPAC Green Book (Ref. <cite|greenbook-2>) except for quantities followed
  by an asterisk (<rsup|<math|\<ast\>>>).

  <\big-table>
    \;

    <bktable3|<tformat|<cwith|1|21|2|2|cell-hyphen|t>|<table|<row|<cell|Symbol>|<cell|Physical
    quantity>|<cell|SI unit>>|<row|<cell|<math|A>>|<cell|Helmholtz
    energy>|<cell|<math|<text|J>>>>|<row|<cell|<math|A<rsub|<text|r>>>>|<cell|relative
    atomic mass (atomic weight)>|<cell|(dimensionless)>>|<row|<cell|<math|A<rsub|s>>>|<cell|surface
    area>|<cell|<math|<text|m><rsup|2>>>>|<row|<cell|<math|a>>|<cell|activity>|<cell|(dimensionless)>>|<row|<cell|<math|B>>|<cell|second
    virial coefficient>|<cell|<math|<text|m><rsup|3>\<cdot\><text|mol><per>>>>|<row|<cell|<math|C>>|<cell|number
    of components<rsup|*>>|<cell|(dimensionless)>>|<row|<cell|<math|C<rsub|p>>>|<cell|heat
    capacity at constant pressure>|<cell|<math|<text|J>\<cdot\><text|K><per>>>>|<row|<cell|<math|C<rsub|V>>>|<cell|heat
    capacity at constant volume>|<cell|<math|<text|J>\<cdot\><text|K><per>>>>|<row|<cell|<math|c>>|<cell|concentration>|<cell|<math|<text|mol>\<cdot\><text|m><rsup|3>>>>|<row|<cell|<math|E>>|<\cell>
      energy
    </cell>|<cell|<math|<text|J>>>>|<row|<cell|>|<cell|electrode
    potential>|<cell|<math|<text|V>>>>|<row|<cell|<math|\<b-E\>>>|<cell|electric
    field strength>|<cell|<math|<text|V>\<cdot\><text|m><per>>>>|<row|<cell|<math|E<rsub|<text|cell>>>>|<cell|cell
    potential>|<cell|<math|<text|V>>>>|<row|<cell|<math|E<rsub|<text|j>>>>|<cell|liquid
    junction potential>|<cell|<math|<text|V>>>>|<row|<cell|<math|E<rsub|<text|sys>>>>|<cell|system
    energy in a lab frame>|<cell|<math|<text|J>>>>|<row|<cell|<math|F>>|<cell|force>|<cell|<math|<text|N>>>>|<row|<cell|<math|>>|<cell|number
    of degrees of freedom<rsup|*>>|<cell|(dimensionless)<math|>>>|<row|<cell|<math|f>>|<cell|fugacity>|<cell|<math|<text|Pa>>>>|<row|<cell|<math|g>>|<cell|acceleration
    of free fall>|<cell|<math|<text|m>\<cdot\><text|s><rsup|-2>>>>|<row|<cell|<math|G>>|<cell|Gibbs
    energy>|<cell|<math|<text|J>>>>>>>
  <|big-table>
    Symbols \U Roman letters (<math|A> through <math|G>)
  </big-table>

  <\big-table|<bktable3|<tformat|<cwith|1|21|2|2|cell-hyphen|t>|<cwith|1|21|1|3|cell-valign|c>|<cwith|16|16|1|3|cell-valign|c>|<cwith|17|17|1|3|cell-valign|c>|<cwith|18|18|1|3|cell-valign|c>|<cwith|19|19|1|3|cell-valign|c>|<cwith|20|20|1|3|cell-valign|c>|<table|<row|<cell|Symbol>|<\cell>
    Physical quantity
  </cell>|<cell|SI unit>>|<row|<cell|<math|h>>|<\cell>
    height, elevation
  </cell>|<cell|<math|<text|m>>>>|<row|<cell|<math|H>>|<\cell>
    enthalpy
  </cell>|<cell|<math|<text|J>>>>|<row|<cell|<math|\<b-H\>>>|<\cell>
    magnetic field strength
  </cell>|<cell|<math|<text|A>\<cdot\><text|m><per>>>>|<row|<cell|<math|I>>|<\cell>
    electric current
  </cell>|<cell|<math|<text|A>>>>|<row|<cell|<math|I<rsub|m>>>|<\cell>
    ionic strength, molality basis
  </cell>|<cell|<math|<text|mol>\<cdot\><text|kg><per>>>>|<row|<cell|<math|I<rsub|c>>>|<\cell>
    ionic strength, concentration basis
  </cell>|<cell|<math|<text|mol>\<cdot\><text|m><rsup|-3>>>>|<row|<cell|<math|K>>|<\cell>
    thermodynamic equilibrium constant
  </cell>|<cell|(dimensionless)<math|>>>|<row|<cell|<math|K<rsub|<text|a>>>>|<\cell>
    acid dissociation constant
  </cell>|<cell|<math|>(dimensionless)>>|<row|<cell|<math|K<rsub|p>>>|<\cell>
    equilibrium constant, pressure basis
  </cell>|<cell|<math|<text|Pa><rsup|<big|sum>\<nu\>>>>>|<row|<cell|<math|K<rsub|<text|s>>>>|<\cell>
    solubility product
  </cell>|<cell|(dimensionless)<math|>>>|<row|<cell|<math|k<rsub|<text|H>,i>>>|<\cell>
    Henry's law constant of species <math|i>,

    mole fraction basis
  </cell>|<cell|<math|<text|Pa>>>>|<row|<cell|<math|k<rsub|c,i>>>|<\cell>
    Henry's law constant of species <math|i>,

    concentration basis<rsup|*>
  </cell>|<cell|<math|<text|Pa>\<cdot\><text|m><rsup|3>\<cdot\><text|mol><per>>>>|<row|<cell|<math|k<rsub|m,i>>>|<\cell>
    Henry's law constant of species <math|i>,

    molality basis<rsup|*>
  </cell>|<cell|<math|<text|Pa>\<cdot\><text|kg>\<cdot\><text|mol><per>>>>|<row|<cell|<math|l>>|<\cell>
    length, distance
  </cell>|<cell|<math|<text|m>>>>|<row|<cell|<math|L>>|<\cell>
    relative partial molar enthalpy<rsup|*>
  </cell>|<cell|<math|<text|J>\<cdot\><text|mol><per>>>>|<row|<cell|<math|M>>|<\cell>
    molar mass
  </cell>|<cell|<math|<text|kg>\<cdot\><text|mol><per>>>>|<row|<cell|<math|\<b-M\>>>|<\cell>
    magnetization
  </cell>|<cell|<math|<text|A>\<cdot\><text|m><per>>>>|<row|<cell|<math|M<rsub|<text|r>>>>|<\cell>
    relative molecular mass (molecular weight)
  </cell>|<cell|(dimensionless)<math|>>>|<row|<cell|<math|m>>|<\cell>
    mass
  </cell>|<cell|<math|<text|kg>>>>|<row|<cell|<math|m<rsub|i>>>|<\cell>
    molality of species <math|i>
  </cell>|<cell|<math|<text|mol>\<cdot\><text|kg><per>>>>>>>>
    Symbols \U Roman letters (<math|h> through <math|m>)
  </big-table>

  <\big-table|<bktable3|<tformat|<cwith|1|22|2|2|cell-hyphen|t>|<cwith|1|22|1|3|cell-valign|c>|<table|<row|<cell|Symbol>|<\cell>
    Physical quantity
  </cell>|<cell|SI unit>>|<row|<cell|<math|N>>|<\cell>
    number of entities (molecules, atoms, ions,

    formula units, etc.)
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|n>>|<\cell>
    amount of substance
  </cell>|<cell|<math|<text|mol>>>>|<row|<cell|<math|P>>|<\cell>
    number of phases<rsup|*>
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|p>>|<\cell>
    pressure
  </cell>|<cell|<math|<text|Pa>>>>|<row|<cell|>|<\cell>
    partial pressure
  </cell>|<cell|<math|<text|Pa>>>>|<row|<cell|<math|\<b-P\>>>|<\cell>
    dialectric polarization
  </cell>|<cell|<math|<text|C>\<cdot\><text|m><rsup|-2>>>>|<row|<cell|<math|Q>>|<\cell>
    electric charge
  </cell>|<cell|<math|<text|C>>>>|<row|<cell|<math|Q<sys>>>|<\cell>
    charge entering system at right conductor<rsup|*>
  </cell>|<cell|<math|<text|C>>>>|<row|<cell|<math|Q<rsub|<text|rxn>>>>|<\cell>
    reaction quotient<rsup|*>
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|q>>|<\cell>
    heat
  </cell>|<cell|<math|<text|J>>>>|<row|<cell|<math|R<el>>>|<\cell>
    electric resistance<rsup|*>
  </cell>|<cell|<math|\<Omega\>>>>|<row|<cell|<math|S>>|<\cell>
    entropy
  </cell>|<cell|<math|<text|J>\<cdot\><text|K><per>>>>|<row|<cell|<math|s>>|<\cell>
    solubility
  </cell>|<cell|<math|<text|mol>\<cdot\><text|m><rsup|-3>>>>|<row|<cell|>|<\cell>
    number of species<rsup|*>
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|T>>|<\cell>
    thermodynamic temperature
  </cell>|<cell|<math|<text|K>>>>|<row|<cell|<math|t>>|<\cell>
    time
  </cell>|<cell|<math|<text|s>>>>|<row|<cell|>|<\cell>
    Celsius temperature
  </cell>|<cell|<degC>>>|<row|<cell|<math|U>>|<\cell>
    internal energy
  </cell>|<cell|J>>|<row|<cell|<math|V>>|<\cell>
    volume
  </cell>|<cell|<math|<text|m><rsup|3>>>>|<row|<cell|<math|v>>|<\cell>
    specific volume
  </cell>|<cell|<math|<text|m><rsup|3>\<cdot\><text|kg><per>>>>|<row|<cell|>|<\cell>
    velocity, speed
  </cell>|<cell|<math|<text|m>\<cdot\><text|s><per>>>>>>>>
    Symbols \U Roman letters (<math|N> through <math|v>)
  </big-table>

  <\big-table|<bktable3|<tformat|<cwith|1|14|2|2|cell-hyphen|t>|<cwith|1|14|1|3|cell-valign|c>|<table|<row|<cell|Symbol>|<\cell>
    Physical quantity
  </cell>|<cell|SI unit>>|<row|<cell|<math|w>>|<\cell>
    work
  </cell>|<cell|<math|<text|J>>>>|<row|<cell|>|<\cell>
    mass fraction (weight fraction)
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|w<el>>>|<\cell>
    electrical work<rsup|*>
  </cell>|<cell|<math|<text|J>>>>|<row|<cell|<math|w<rprime|'>>>|<\cell>
    nonexpansion work<rsup|*>
  </cell>|<cell|<math|<text|J>>>>|<row|<cell|<math|x>>|<\cell>
    mole fraction in a phase
  </cell>|<cell|(dimensionless)>>|<row|<cell|>|<\cell>
    Cartesian space coordinate
  </cell>|<cell|<math|<text|m>>>>|<row|<cell|<math|y>>|<\cell>
    mole fraction in gas phase
  </cell>|<cell|(dimensionless)>>|<row|<cell|>|<\cell>
    Cartesian space coordinate
  </cell>|<cell|<math|<text|m>>>>|<row|<cell|<math|Z>>|<\cell>
    compression factor (compressibility factor)
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|z>>|<\cell>
    mole fraction in multiphase system<rsup|*>
  </cell>|<cell|(dimensionless)>>|<row|<cell|>|<\cell>
    charge number of an ion
  </cell>|<cell|(dimensionless)>>|<row|<cell|>|<\cell>
    electron number of cell reaction
  </cell>|<cell|(dimensionless)>>|<row|<cell|>|<\cell>
    Cartesian space coordinate
  </cell>|<cell|(dimensionless)>>>>>>
    Symbols \U Roman letters (<math|w> through <math|z>)
  </big-table>

  \;

  <\big-table|<bktable3|<tformat|<cwith|1|17|2|2|cell-hyphen|t>|<cwith|1|17|1|3|cell-valign|c>|<table|<row|<cell|Symbol>|<\cell>
    Physical quantity
  </cell>|<cell|SI unit>>|<row|<cell|<em|alpha>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<alpha\>>>|<\cell>
    degree of reaction, dissociation, etc.
  </cell>|<cell|(dimensionless)>>|<row|<cell|>|<\cell>
    cubic expansion coefficient
  </cell>|<cell|<math|<text|K><per>>>>|<row|<cell|<em|gamma>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<gamma\>>>|<\cell>
    surface tension
  </cell>|<cell|<math|<text|N>\<cdot\><text|m><per>>,
  <math|<text|J>\<cdot\><text|m><rsup|-2>>>>|<row|<cell|<math|\<gamma\><rsub|i>>>|<\cell>
    activity coefficient of species <math|i>,

    pure liquid or solid standard state<rsup|*>
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|\<gamma\><rsub|m,i>>>|<\cell>
    activity coefficient of species <math|i>,

    molality basis
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|\<gamma\><rsub|c,i>>>|<\cell>
    activity coefficient of species <math|i>,

    concentration basis
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|\<gamma\><rsub|x,i>>>|<\cell>
    activity coefficient of species <math|i>,

    mole fraction basis
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|\<gamma\><rsub|\<pm\>>>>|<\cell>
    mean ionic activity coefficient
  </cell>|<cell|(dimensionless)>>|<row|<cell|<G>>|<\cell>
    pressure factor (activity of a refernce state)<rsup|*>
  </cell>|<cell|(dimensionless)>>|<row|<cell|<em|epsilon>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<epsilon\>>>|<\cell>
    efficiency of a heat engine
  </cell>|<cell|(dimensionless)>>|<row|<cell|<em|theta>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<vartheta\>>>|<\cell>
    angle of rotation
  </cell>|<cell|(dimensionless)>>>>>>
    Symbols \U Greek letters (<em|alpha> through <em|theta>)
  </big-table>

  <\big-table|<bktable3|<tformat|<cwith|1|28|2|2|cell-hyphen|t>|<cwith|1|28|1|3|cell-valign|c>|<table|<row|<cell|Symbol>|<\cell>
    Physical quantity
  </cell>|<cell|SI unit>>|<row|<cell|<em|kappa>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<kappa\>>>|<\cell>
    reciprocal radius of ionic atmosphere
  </cell>|<cell|<math|<text|m><per>>>>|<row|<cell|<math|\<kappa\><rsub|T>>>|<\cell>
    isothermal compressibility
  </cell>|<cell|<math|<text|Pa><per>>>>|<row|<cell|<em|mu>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<mu\>>>|<\cell>
    chemical potential
  </cell>|<cell|<math|<text|J>\<cdot\><text|mol><per>>>>|<row|<cell|<math|\<mu\><rsub|<text|JT>>>>|<\cell>
    Joule\UThomson coefficient
  </cell>|<cell|<math|<text|K>\<cdot\><text|Pa><per>>>>|<row|<cell|<em|nu>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<nu\>>>|<\cell>
    number of ions per formula unit

    stoichiometric number
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|\<nu\><rsub|+>>>|<\cell>
    number of cations per formula unit
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|\<nu\><rsub|->>>|<\cell>
    number of anions per formula unit
  </cell>|<cell|(dimensionless)>>|<row|<cell|<em|xi>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<xi\>>>|<\cell>
    advancement (extent of reaction)
  </cell>|<cell|<math|<text|mol>>>>|<row|<cell|<em|pi>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<P>>|<\cell>
    osmotic pressure
  </cell>|<cell|<math|<text|Pa>>>>|<row|<cell|<em|rho>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<rho\>>>|<\cell>
    density
  </cell>|<cell|<math|<text|kg>\<cdot\><text|m><rsup|-3>>>>|<row|<cell|<em|tau>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<tau\>>>|<\cell>
    torque<rsup|*>
  </cell>|<cell|<math|<text|J>>>>|<row|<cell|<em|phi>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<phi\>>>|<\cell>
    fugacity coefficient
  </cell>|<cell|(dimensionless)>>|<row|<cell|>|<\cell>
    electric potential
  </cell>|<cell|<math|<text|V>>>>|<row|<cell|<math|\<Delta\>*\<phi\>>>|<\cell>
    electric potential difference
  </cell>|<cell|<math|<text|V>>>>|<row|<cell|<math|\<phi\><rsub|m>>>|<\cell>
    osmotic coefficient, molality basis
  </cell>|<cell|(dimensionless)>>|<row|<cell|<math|<slanted|\<Phi\><rsub|<text|L>>>>>|<\cell>
    relative apparent molar enthalpy of solute<rsup|*>
  </cell>|<cell|<math|<text|J>\<cdot\><text|mol><rsup|-1>>>>|<row|<cell|<em|omega>>|<\cell>
    \;
  </cell>|<cell|>>|<row|<cell|<math|\<omega\>>>|<\cell>
    angular velocity
  </cell>|<cell|<math|<text|s><per>>>>>>>>
    Symbols \U Greek letters (<em|kappa> through <em|omega>)
  </big-table>

  <index-complex|<tuple|symbols for physical quantities>||app:sym
  idx1|<tuple|Symbols physical quantities>><index-complex|<tuple|physical
  quantities, symbols for>||app:sym idx2|<tuple|Physical quantities, symbols
  for>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|14>
    <associate|page-first|373>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|app:sym|<tuple|A|?>>
    <associate|auto-1|<tuple|A|?>>
    <associate|auto-10|<tuple|<tuple|symbols for physical quantities>|?>>
    <associate|auto-11|<tuple|<tuple|physical quantities, symbols for>|?>>
    <associate|auto-2|<tuple|<tuple|symbols for physical quantities>|?>>
    <associate|auto-3|<tuple|<tuple|physical quantities, symbols for>|?>>
    <associate|auto-4|<tuple|1|?>>
    <associate|auto-5|<tuple|2|?>>
    <associate|auto-6|<tuple|3|?>>
    <associate|auto-7|<tuple|4|?>>
    <associate|auto-8|<tuple|5|?>>
    <associate|auto-9|<tuple|6|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      greenbook-2
    </associate>
    <\associate|idx>
      <tuple|<tuple|symbols for physical quantities>||app:sym
      idx1|<tuple|Symbols physical quantities>|<pageref|auto-2>>

      <tuple|<tuple|physical quantities, symbols for>||app:sym
      idx2|<tuple|Physical quantities, symbols for>|<pageref|auto-3>>

      <tuple|<tuple|symbols for physical quantities>||app:sym
      idx1|<tuple|Symbols physical quantities>|<pageref|auto-10>>

      <tuple|<tuple|physical quantities, symbols for>||app:sym
      idx2|<tuple|Physical quantities, symbols for>|<pageref|auto-11>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Symbols \U Roman letters (<with|mode|<quote|math>|A> through
        <with|mode|<quote|math>|G>)
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        Symbols \U Roman letters (<with|mode|<quote|math>|h> through
        <with|mode|<quote|math>|m>)
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Symbols \U Roman letters (<with|mode|<quote|math>|N> through
        <with|mode|<quote|math>|v>)
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        Symbols \U Roman letters (<with|mode|<quote|math>|w> through
        <with|mode|<quote|math>|z>)
      </surround>|<pageref|auto-7>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|5>|>
        Symbols \U Greek letters (<with|font-shape|<quote|italic>|alpha>
        through <with|font-shape|<quote|italic>|theta>)
      </surround>|<pageref|auto-8>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|6>|>
        Symbols \U Greek letters (<with|font-shape|<quote|italic>|kappa>
        through <with|font-shape|<quote|italic>|omega>)
      </surround>|<pageref|auto-9>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix
      A<space|2spc>Symbols for Physical Quantities>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>