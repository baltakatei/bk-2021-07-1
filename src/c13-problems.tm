<TeXmacs|1.99.21>

<project|book.tm>

<style|<tuple|generic|style-bk>>

<\body>
  <new-page*><section|Problems>

  <\problem>
    Consider a single-phase system that is a gaseous mixture of
    N<rsub|<math|2>>, H<rsub|<math|2>>, and NH<rsub|<math|3>>. For each of
    the following cases, find the number of degrees of freedom and give an
    example of the independent intensive variables that could be used to
    specify the equilibrium state, apart from the total amount of gas.

    <\enumerate-alpha>
      <item>There is no reaction.

      <item>The reaction <math|<text|N<rsub|2>
      ><around|(|<text|g>|)>+3*<text|H<rsub|2>>
      <around|(|<text|g>|)><arrow>2*<text|NH<rsub|3>> <around|(|<text|g>|)>>
      is at equilibrium.

      <item>The reaction is at equilibrium and the system is prepared from
      NH<rsub|<math|3>> only.
    </enumerate-alpha>
  </problem>

  <\problem>
    How many components has a mixture of water and deuterium oxide in which
    the equilibrium <math|<text|H<rsub|2>O>+<text|D<rsub|2>O><arrows>2*<text|HDO>>
    exists?
  </problem>

  <\problem>
    Consider a system containing only NH<rsub|<math|4>>Cl(s),
    NH<rsub|<math|3>>(g), and HCl(g). Assume that the equilibrium
    <math|<text|NH<rsub|4>Cl> <around|(|<text|s>|)><arrows><text|NH<rsub|3>>
    <around|(|<text|g>|)>+<text|HCl> <around|(|<text|g>|)>> exists.

    <\enumerate-alpha>
      <item>Suppose you prepare the system by placing solid
      NH<rsub|<math|4>>Cl in an evacuated flask and heating to <math|400<K>>.
      Use the phase rule to decide whether you can vary the pressure while
      both phases remain in equilibrium at <math|400<K>>.

      <item>According to the phase rule, if the system is not prepared as
      described in part (a) could you vary the pressure while both phases
      remain in equilibrium at <math|400<K>>? Explain.

      <item>Rationalize your conclusions for these two cases on the basis of
      the thermodynamic equilibrium constant. Assume that the gas phase is an
      ideal gas mixture and use the approximate expression
      <math|K=p<rsub|<text|NH<rsub|3>>>*p<rsub|<text|HCl>>/<around|(|p<st>|)><rsup|2>>.
    </enumerate-alpha>
  </problem>

  <\problem>
    Consider the lime-kiln process <math|<text|CaCO<rsub|3>>
    <around|(|<text|s>|)><ra><text|CaO> <around|(|<text|s>|)>+<text|CO<rsub|2>>
    <around|(|<text|g>|)>>. Find the number of intensive variables that can
    be varied independently in the equilibrium system under the following
    conditions:

    <\enumerate-alpha>
      <item>The system is prepared by placing calcium carbonate, calcium
      oxide, and carbon dioxide in a container.

      <item>The system is prepared from calcium carbonate only.

      <item>The temperature is fixed at <math|1000<K>>.
    </enumerate-alpha>
  </problem>

  <\problem>
    What are the values of <math|C> and <math|F> in systems consisting of
    solid AgCl in equilibrium with an aqueous phase containing
    H<rsub|<math|2>>O, Ag<rsup|<math|+>>(aq), Cl<rsup|<math|->>(aq),
    Na<rsup|<math|+>>(aq), and NO<math|<rsub|3><rsup|->>(aq) prepared in the
    following ways? Give examples of intensive variables that could be varied
    independently.

    <\enumerate-alpha>
      <item>The system is prepared by equilibrating excess solid AgCl with an
      aqueous solution of NaNO<rsub|<math|3>>.

      <item>The system is prepared by mixing aqueous solutions of
      AgNO<rsub|<math|3>> and NaCl in arbitrary proportions; some solid AgCl
      forms by precipitation.
    </enumerate-alpha>
  </problem>

  <\problem>
    How many degrees of freedom has a system consisting of solid NaCl in
    equilibrium with an aqueous phase containing H<rsub|<math|2>>O,
    Na<rsup|<math|+>>(aq), Cl<rsup|<math|->>(aq), H<rsup|<math|+>>(aq), and
    OH<rsup|<math|->>(aq)? Would it be possible to independently vary
    <math|T>, <math|p>, and <math|m<rsub|<text|OH><rsup|<rsup|->>>>? If so,
    explain how you could do this.
  </problem>

  <\problem>
    Consult the phase diagram shown in Fig. <vpageref|fig:13-H2O-NaCl>.
    Suppose the system contains <math|36.0 <text|g>> (<math|2.00 <mol>>)
    H<rsub|<math|2>>O and <math|58.4 <text|g>> (<math|1.00 <mol>>) NaCl at
    <math|25 <degC>> and <math|1<br>>.

    <\enumerate-alpha>
      <item>Describe the phases present in the equilibrium system and their
      masses.

      <item>Describe the changes that occur at constant pressure if the
      system is placed in thermal contact with a heat reservoir at <math|-30
      <degC>>.

      <item>Describe the changes that occur if the temperature is raised from
      <math|25 <degC>> to <math|120 <degC>> at constant pressure.

      <item>Describe the system after <math|200 <text|g>> H<rsub|<math|2>>O
      is added at <math|25 <degC>>.
    </enumerate-alpha>
  </problem>

  \;

  <\big-table|<tabular*|<tformat|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|5|5|cell-rborder|0ln>|<cwith|3|3|1|-1|cell-tborder|1ln>|<cwith|7|7|1|-1|cell-bborder|2ln>|<cwith|3|7|1|-1|cell-halign|C.>|<cwith|2|2|1|2|cell-tborder|1ln>|<cwith|2|2|4|5|cell-tborder|1ln>|<table|<row|<cell|<math|<text|Na<rsub|2>SO<rsub|4>>\<cdot\>10*<text|H<rsub|2>O>>>|<cell|>|<cell|<space|2em>>|<cell|<math|<text|Na<rsub|2>SO<rsub|4>>>>|<cell|>>|<row|<cell|<math|t/<degC>>>|<cell|<math|x<B>>>|<cell|>|<cell|<math|t/<degC>>>|<cell|<math|x<B>>>>|<row|<cell|10>|<cell|0.011>|<cell|>|<cell|40>|<cell|0.058>>|<row|<cell|15>|<cell|0.016>|<cell|>|<cell|50>|<cell|0.056>>|<row|<cell|20>|<cell|0.024>|<cell|>|<cell|>|<cell|>>|<row|<cell|25>|<cell|0.034>|<cell|>|<cell|>|<cell|>>|<row|<cell|30>|<cell|0.048>|<cell|>|<cell|>|<cell|>>>>>>
    <label|tbl:13-H2O+Na2SO4>Aqueous solubilities of sodium sulfate
    decahydrate and anhydrous sodium sulfate<note-ref|+1XmfVJNylVc6970>

    <note-inline|Ref. <cite|findlay-45>, p. 179--180.|+1XmfVJNylVc6970>
  </big-table>

  <\problem>
    Use the following information to draw a temperature\Ucomposition phase
    diagram for the binary system of H<rsub|<math|2>>O (A) and
    Na<rsub|<math|2>>SO<rsub|<math|4>> (B) at <math|p=1<br>>, confining
    <math|t> to the range <math|-20> to <math|50 <degC>> and <math|z<B>> to
    the range <math|0>\U<math|0.2>. The solid decahydrate,
    <math|<text|Na<rsub|2>SO<rsub|4>>\<cdot\>10*<text|H<rsub|2>O>>, is stable
    below <math|32.4 <degC>>. The anhydrous salt,
    <math|<text|Na<rsub|2>SO<rsub|4>>>, is stable above this temperature.
    There is a peritectic point for these two solids and the solution at
    <math|x<B>=0.059> and <math|t=32.4 <degC>>. There is a eutectic point for
    ice, <math|<text|Na<rsub|2>SO<rsub|4>>\<cdot\>10*<text|H<rsub|2>O>>, and
    the solution at <math|x<B>=0.006> and <math|t=-1.3 <degC>>. Table
    <vpageref|tbl:13-H2O+Na2SO4> gives the temperature dependence of the
    solubilities of the ionic solids.
  </problem>

  <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|13|13|1|-1|cell-bborder|2ln>|<cwith|2|-1|1|-1|cell-halign|C.>|<cwith|1|-1|1|-1|cell-bsep|0.25fn>|<cwith|1|-1|1|-1|cell-tsep|0.25fn>|<table|<row|<cell|<math|x<A>>>|<cell|<math|t/<degC>>>|<cell|<space|2em>>|<cell|<math|x<A>>>|<cell|<math|t/<degC>>>|<cell|<space|2em>>|<cell|<math|x<A>>>|<cell|<math|t/<degC>>>>|<row|<cell|0.000>|<cell|0.0>|<cell|>|<cell|0.119>|<cell|35.0>|<cell|>|<cell|0.286>|<cell|56.0>>|<row|<cell|0.020>|<cell|<math|-10.0>>|<cell|>|<cell|0.143>|<cell|37.0>|<cell|>|<cell|0.289>|<cell|55.0>>|<row|<cell|0.032>|<cell|<math|-20.5>>|<cell|>|<cell|0.157>|<cell|36.0>|<cell|>|<cell|0.293>|<cell|60.0>>|<row|<cell|0.037>|<cell|<math|-27.5>>|<cell|>|<cell|0.173>|<cell|33.0>|<cell|>|<cell|0.301>|<cell|69.0>>|<row|<cell|0.045>|<cell|<math|-40.0>>|<cell|>|<cell|0.183>|<cell|30.0>|<cell|>|<cell|0.318>|<cell|72.5>>|<row|<cell|0.052>|<cell|<math|-55.0>>|<cell|>|<cell|0.195>|<cell|27.4>|<cell|>|<cell|0.333>|<cell|73.5>>|<row|<cell|0.053>|<cell|<math|-41.0>>|<cell|>|<cell|0.213>|<cell|32.0>|<cell|>|<cell|0.343>|<cell|72.5>>|<row|<cell|0.056>|<cell|<math|-27.0>>|<cell|>|<cell|0.222>|<cell|32.5>|<cell|>|<cell|0.358>|<cell|70.0>>|<row|<cell|0.076>|<cell|0.0>|<cell|>|<cell|0.232>|<cell|30.0>|<cell|>|<cell|0.369>|<cell|66.0>>|<row|<cell|0.083>|<cell|10.0>|<cell|>|<cell|0.238>|<cell|35.0>|<cell|>|<cell|0.369>|<cell|80.0>>|<row|<cell|0.093>|<cell|20.0>|<cell|>|<cell|0.259>|<cell|50.0>|<cell|>|<cell|0.373>|<cell|100.0>>|<row|<cell|0.106>|<cell|30.0>|<cell|>|<cell|0.277>|<cell|55.0>|<cell|>|<cell|>|<cell|>>>>>>
    <label|tbl:13-FeCl3-H2O>Data for Problem <reference|prb:13-FeCl3 solns>.
    Temperatures of saturated solutions of aqueous iron(III) chloride at
    <math|p=1<br>> (A = <math|<text|FeCl<rsub|3>>>, B =
    <math|<text|H<rsub|2>O>>)<note-ref|+1XmfVJNylVc6971>

    <note-inline|Data from Ref. <cite|findlay-45>, page
    193.|+1XmfVJNylVc6971>
  </big-table>

  <\problem>
    <label|prb:13-FeCl3 solns>Iron(III) chloride forms various solid
    hydrates, all of which melt congruently. Table
    <vpageref|tbl:13-FeCl3-H2O> lists the temperatures <math|t> of aqueous
    solutions of various compositions that are saturated with respect to a
    solid phase.

    <\enumerate-alpha>
      <item>Use these data to construct a <math|t>\U<math|z<B>> phase diagram
      for the binary system of <math|<text|FeCl<rsub|3>>> (A) and
      <math|<text|H<rsub|2>O>> (B). Identify the formula and melting point of
      each hydrate. Hint: derive a formula for the mole ratio
      <math|n<B>/n<A>> as a function of <math|x<A>> in a binary mixture.

      <item>For the following conditions, determine the phase or phases
      present at equilibrium and the composition of each.

      <\enumerate-numeric>
        <item><math|t=-70.0<degC>> and <math|z<A>=0.100>

        <item><math|t=50.0<degC>> and <math|z<A>=0.275>
      </enumerate-numeric>
    </enumerate-alpha>
  </problem>

  <\big-figure|<image|13-SUP/PHENOL.eps|222pt|141pt||>>
    Temperature--composition phase diagram for the binary system of water (A)
    and phenol (B) at <math|1<br>>.<note-ref|+1XmfVJNylVc6972> Only liquid
    phases are present.<label|fig:13-H2O-phenol>

    <note-inline|Ref. <cite|findlay-45>, p. 95.|+1XmfVJNylVc6972>
  </big-figure>

  <\problem>
    <label|prb:13-phenol> Figure <vpageref|fig:13-H2O-phenol> is a
    temperature\Ucomposition phase diagram for the binary system of water (A)
    and phenol (B) at <math|1<br>>. These liquids are partially miscible
    below <math|67 <degC>>. Phenol is more dense than water, so the layer
    with the higher mole fraction of phenol is the bottom layer. Suppose you
    place <math|4.0 <mol>> of H<rsub|<math|2>>O and <math|1.0<mol>> of phenol
    in a beaker at <math|30 <degC>> and gently stir to allow the layers to
    equilibrate.

    <\enumerate-alpha>
      <item>What are the compositions of the equilibrated top and bottom
      layers?

      <item>Find the amount of each component in the bottom layer.

      <item>As you gradually stir more phenol into the beaker, maintaining
      the temperature at <math|30 <degC>>, what changes occur in the volumes
      and compositions of the two layers? Assuming that one layer eventually
      disappears, what additional amount of phenol is needed to cause this to
      happen?
    </enumerate-alpha>
  </problem>

  <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|1|1|3|3|cell-col-span|1>|<cwith|1|1|3|3|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|4|4|1|-1|cell-bborder|2ln>|<cwith|1|-1|1|-1|cell-bsep|0.25fn>|<cwith|1|-1|1|-1|cell-tsep|0.25fn>|<cwith|2|-1|1|-1|cell-halign|C.>|<table|<row|<cell|<math|t/<degC>>>|<cell|<math|p<A><rsup|\<ast\>>/<text|bar>>>|<cell|<math|p<B><rsup|\<ast\>>/<text|bar>>>>|<row|<cell|-10.0>|<cell|3.360>|<cell|0.678>>|<row|<cell|-20.0>|<cell|2.380>|<cell|0.441>>|<row|<cell|-30.0>|<cell|1.633>|<cell|0.275>>>>>>
    <label|tbl:13-prop-but>Saturation vapor pressures of propane (A) and
    <em|n>-butane (B)
  </big-table>

  <\problem>
    The standard boiling point of propane is <math|-41.8 <degC>> and that of
    <em|n>-butane is <math|-0.2 <degC>>. Table <vpageref|tbl:13-prop-but>
    lists vapor pressure data for the pure liquids. Assume that the liquid
    mixtures obey Raoult's law.

    <\enumerate-alpha>
      <item>Calculate the compositions, <math|x<A>>, of the liquid mixtures
      with boiling points of <math|-10.0 <degC>>, <math|-20.0 <degC>>, and
      <math|-30.0 <degC>> at a pressure of <math|1<br>>.

      <item>Calculate the compositions, <math|y<A>>, of the equilibrium vapor
      at these three temperatures.

      <item>Plot the temperature\Ucomposition phase diagram at <math|p=1<br>>
      using these data, and label the areas appropriately.

      <item>Suppose a system containing <math|10.0<mol>> propane and
      <math|10.0<mol>> <em|n>-butane is brought to a pressure of <math|1<br>>
      and a temperature of <math|-25 <degC>>. From your phase diagram,
      estimate the compositions and amounts of both phases.
    </enumerate-alpha>
  </problem>

  <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|1|1|7|7|cell-col-span|1>|<cwith|1|1|7|7|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|8|8|1|-1|cell-bborder|2ln>|<cwith|2|-1|1|-1|cell-halign|C.>|<cwith|1|-1|1|-1|cell-bsep|0.25fn>|<cwith|1|-1|1|-1|cell-tsep|0.25fn>|<table|<row|<cell|<math|x<A>>>|<cell|<math|y<A>>>|<cell|<math|p/<text|kPa>>>|<cell|<space|2em>>|<cell|<math|x<A>>>|<cell|<math|y<A>>>|<cell|<math|p/<text|kPa>>>>|<row|<cell|0>|<cell|0>|<cell|29.89>|<cell|>|<cell|0.5504>|<cell|0.3692>|<cell|35.32>>|<row|<cell|0.0472>|<cell|0.1467>|<cell|33.66>|<cell|>|<cell|0.6198>|<cell|0.3951>|<cell|34.58>>|<row|<cell|0.0980>|<cell|0.2066>|<cell|35.21>|<cell|>|<cell|0.7096>|<cell|0.4378>|<cell|33.02>>|<row|<cell|0.2047>|<cell|0.2663>|<cell|36.27>|<cell|>|<cell|0.8073>|<cell|0.5107>|<cell|30.28>>|<row|<cell|0.2960>|<cell|0.2953>|<cell|36.45>|<cell|>|<cell|0.9120>|<cell|0.6658>|<cell|25.24>>|<row|<cell|0.3862>|<cell|0.3211>|<cell|36.29>|<cell|>|<cell|0.9655>|<cell|0.8252>|<cell|21.30>>|<row|<cell|0.4753>|<cell|0.3463>|<cell|35.93>|<cell|>|<cell|1.0000>|<cell|1.0000>|<cell|18.14>>>>>>
    <label|tbl:13-isoPrOH-benz>Liquid and gas compositions in the two-phase
    system of 2-propanol (A) and benzene at <math|45
    <degC>>.<note-ref|+1XmfVJNylVc6973>

    <note-inline|Ref. <cite|brown-56>.|+1XmfVJNylVc6973>
  </big-table>

  <\problem>
    Use the data in Table <vpageref|tbl:13-isoPrOH-benz> to draw a
    pressure\Ucomposition phase diagram for the 2-propanol\Ubenzene system at
    <math|45 <degC>>. Label the axes and each area.
  </problem>

  <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|7|7|cell-col-span|1>|<cwith|1|1|7|7|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|8|8|1|-1|cell-bborder|2ln>|<cwith|1|-1|1|-1|cell-bsep|0.25fn>|<cwith|1|-1|1|-1|cell-tsep|0.25fn>|<cwith|2|-1|1|-1|cell-halign|C.>|<table|<row|<cell|<math|x<A>>>|<cell|<math|y<A>>>|<cell|<math|p/<text|kPa>>>|<cell|<space|2em>>|<cell|<math|x<A>>>|<cell|<math|y<A>>>|<cell|<math|p/<text|kPa>>>>|<row|<cell|0>|<cell|0>|<cell|39.08>|<cell|>|<cell|0.634>|<cell|0.727>|<cell|36.29>>|<row|<cell|0.083>|<cell|0.046>|<cell|37.34>|<cell|>|<cell|0.703>|<cell|0.806>|<cell|38.09>>|<row|<cell|0.200>|<cell|0.143>|<cell|34.92>|<cell|>|<cell|0.815>|<cell|0.896>|<cell|40.97>>|<row|<cell|0.337>|<cell|0.317>|<cell|33.22>|<cell|>|<cell|0.877>|<cell|0.936>|<cell|42.62>>|<row|<cell|0.413>|<cell|0.437>|<cell|33.12>|<cell|>|<cell|0.941>|<cell|0.972>|<cell|44.32>>|<row|<cell|0.486>|<cell|0.534>|<cell|33.70>|<cell|>|<cell|1.000>|<cell|1.000>|<cell|45.93>>|<row|<cell|0.577>|<cell|0.662>|<cell|35.09>|<cell|>|<cell|>|<cell|>|<cell|>>>>>>
    <label|tbl:13-acetone-CHCl3>Liquid and gas compositions in the two-phase
    system of acetone (A) and chloroform at <math|35.2 <degC>><footnote|Ref.
    <cite|ICT-3>, p. 286.>
  </big-table>

  <\problem>
    Use the data in Table <vpageref|tbl:13-acetone-CHCl3> to draw a
    pressure\Ucomposition phase diagram for the acetone\Uchloroform system at
    <math|35.2 <degC>>. Label the axes and each area.
  </problem>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|13>
    <associate|page-first|326>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|3>
    <associate|subsection-nr|2>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|4|?>>
    <associate|auto-2|<tuple|4.1|?>>
    <associate|auto-3|<tuple|4.2|?>>
    <associate|auto-4|<tuple|4.1|?>>
    <associate|auto-5|<tuple|4.3|?>>
    <associate|auto-6|<tuple|4.4|?>>
    <associate|auto-7|<tuple|4.5|?>>
    <associate|fig:13-H2O-phenol|<tuple|4.1|?>>
    <associate|footnote-4.1|<tuple|4.1|?>>
    <associate|footnote-4.2|<tuple|4.2|?>>
    <associate|footnote-4.3|<tuple|4.3|?>>
    <associate|footnote-4.4|<tuple|4.4|?>>
    <associate|footnote-4.5|<tuple|4.5|?>>
    <associate|footnote-4.6|<tuple|4.6|?>>
    <associate|footnr-4.1|<tuple|4.1|?>>
    <associate|footnr-4.2|<tuple|4.2|?>>
    <associate|footnr-4.3|<tuple|4.1|?>>
    <associate|footnr-4.4|<tuple|4.4|?>>
    <associate|footnr-4.6|<tuple|4.6|?>>
    <associate|prb:13-FeCl3 solns|<tuple|4.9|?>>
    <associate|prb:13-phenol|<tuple|4.10|?>>
    <associate|tbl:13-FeCl3-H2O|<tuple|4.2|?>>
    <associate|tbl:13-H2O+Na2SO4|<tuple|4.1|?>>
    <associate|tbl:13-acetone-CHCl3|<tuple|4.5|?>>
    <associate|tbl:13-isoPrOH-benz|<tuple|4.4|?>>
    <associate|tbl:13-prop-but|<tuple|4.3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      findlay-45

      findlay-45

      findlay-45

      brown-56
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.1>|>
        Temperature--composition phase diagram for the binary system of water
        (A) and phenol (B) at <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.<space|0spc><assign|footnote-nr|3><hidden-binding|<tuple>|4.3><assign|fnote-+1XmfVJNylVc6972|<quote|4.3>><assign|fnlab-+1XmfVJNylVc6972|<quote|4.3>><rsup|<with|font-shape|<quote|right>|<reference|footnote-4.3>>>
        Only liquid phases are present.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%18D22B7A8-19D91ECE0>|<link|hyperlink|<id|%18D22B7A8-19D91ECE0>|<url|#footnr-4.3>>|4.3>.
        |<hidden-binding|<tuple|footnote-4.3>|4.3>|Ref.
        [<write|bib|findlay-45><reference|bib-findlay-45>], p. 95.>>>
      </surround>|<pageref|auto-4>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.1>|>
        Aqueous solubilities of sodium sulfate decahydrate and anhydrous
        sodium sulfate<space|0spc><assign|footnote-nr|1><hidden-binding|<tuple>|4.1><assign|fnote-+1XmfVJNylVc6970|<quote|4.1>><assign|fnlab-+1XmfVJNylVc6970|<quote|4.1>><rsup|<with|font-shape|<quote|right>|<reference|footnote-4.1>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%18D22B7A8-1A1F70D08>|<link|hyperlink|<id|%18D22B7A8-1A1F70D08>|<url|#footnr-4.1>>|4.1>.
        |<hidden-binding|<tuple|footnote-4.1>|4.1>|Ref.
        [<write|bib|findlay-45><reference|bib-findlay-45>], p. 179--180.>>>
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.2>|>
        Data for Problem <reference|prb:13-FeCl3 solns>. Temperatures of
        saturated solutions of aqueous iron(III) chloride at
        <with|mode|<quote|math>|p=1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>> (A =
        <with|mode|<quote|math>|<with|mode|<quote|text>|FeCl<rsub|3>>>, B =
        <with|mode|<quote|math>|<with|mode|<quote|text>|H<rsub|2>O>>)<space|0spc><assign|footnote-nr|2><hidden-binding|<tuple>|4.2><assign|fnote-+1XmfVJNylVc6971|<quote|4.2>><assign|fnlab-+1XmfVJNylVc6971|<quote|4.2>><rsup|<with|font-shape|<quote|right>|<reference|footnote-4.2>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%18D22B7A8-19E5B3988>|<link|hyperlink|<id|%18D22B7A8-19E5B3988>|<url|#footnr-4.2>>|4.2>.
        |<hidden-binding|<tuple|footnote-4.2>|4.2>|Data from Ref.
        [<write|bib|findlay-45><reference|bib-findlay-45>], page 193.>>>
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.3>|>
        Saturation vapor pressures of propane (A) and
        <with|font-shape|<quote|italic>|n>-butane (B)
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.4>|>
        Liquid and gas compositions in the two-phase system of 2-propanol (A)
        and benzene at <with|mode|<quote|math>|45
        <rsup|\<circ\>><with|mode|<quote|text>|C>>.<space|0spc><assign|footnote-nr|4><hidden-binding|<tuple>|4.4><assign|fnote-+1XmfVJNylVc6973|<quote|4.4>><assign|fnlab-+1XmfVJNylVc6973|<quote|4.4>><rsup|<with|font-shape|<quote|right>|<reference|footnote-4.4>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%18D22B7A8-19E2D93F8>|<link|hyperlink|<id|%18D22B7A8-19E2D93F8>|<url|#footnr-4.4>>|4.4>.
        |<hidden-binding|<tuple|footnote-4.4>|4.4>|Ref.
        [<write|bib|brown-56><reference|bib-brown-56>].>>>
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.5>|>
        \;
      </surround>|<pageref|auto-7>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Problems>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>