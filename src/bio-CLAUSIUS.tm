<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <no-indent>BIOGRAPHICAL SKETCH

  <paragraph|<person|Rudolf Julius Emmanuel Clausius> (1822\U1888)>

  <\padded-center>
    <image|BIO/clausius.png|97pt|115pt||>
  </padded-center>

  <label|bio:clausius><index|Clausius, Rudolf><no-indent>Rudolf Clausius was
  a German theoretical physicist who was the first to treat thermodynamics as
  a rigorous science, based on the earlier writings of Carnot and Clapeyron.

  He was born in K�slin, Prussia, into a large family. His father was an
  educator and church minister.

  Clausius was successively a professor at universities in Berlin, Zurich,
  W�rzburg, and Bonn. In addition to thermodynamics, he did work on
  electrodynamic theory and the kinetic theory of gases.

  Max Planck, referring to a time early in his own career,
  wrote:<footnote|Ref. <cite|planck-48>, page 16.>

  <quotation|One day, I happened to come across the treatises of Rudolf
  Clausius, whose lucid style and enlightening clarity of reasoning made an
  enormous impression on me, and I became deeply absorbed in his articles,
  with an ever increasing enthusiasm. I appreciated especially his exact
  formulation of the two Laws of Thermodynamics, and the sharp distinction
  which he was the first to establish between them.>

  Clausius based his exposition of the second law on the following principle
  that he published in 1854:<footnote|Ref. <cite|clausius-1854>, page 117.>

  <\quotation>
    ...it appears to me preferable to deduce the general form of the theorem
    immediately from the same principle which I have already employed in my
    former memoir, in order to demonstrate the modified theorem of Carnot.

    This principle, upon which the whole of the following development rests,
    is as follows:<emdash><em|Heat can never pass from a colder to a warmer
    body without some other change, connected therewith, occurring at the
    same time.> Everything we know concerning the interchange of heat between
    two bodies of different temperature confirms this; for heat everywhere
    manifests a tendency to equalize existing differences of temperature, and
    therefore to pass in a contrary direction, <em|i. e. >from warmer to
    colder bodies. Without further explanation, therefore, the truth of the
    principle will be granted.
  </quotation>

  In an 1865 paper, he introduced the symbol <math|U> for internal energy,
  and also coined the word <em|entropy> with symbol <math|S>:<footnote|Ref.
  <cite|clausius-1865>, page 357.>

  <quotation|We might call S the <em|transformational content> of the body,
  just as we termed the magnitude U its <em|thermal and ergonal content>. But
  as I hold it better to borrow terms for important magnitudes from the
  ancient languages, so that they may be adopted unchanged in all modern
  languages, I propose to call the magnitude S the <em|entropy> of the body,
  from the Greek word <math|\<tau\>*\<rho\>*o
  \<pi\><wide|\<eta\>|\<grave\>>>, <em|transformation>. I have intentionally
  formed the word <em|entropy> so as to be as similar as possible to the word
  <em|energy>; for the two magnitudes to be denoted by these words are so
  nearly allied in their physical meanings, that a certain similarity in
  designation appears to be desirable.>

  The 1865 paper concludes as follows, ending with Clausius's often-quoted
  summations of the first and second laws:<footnote|Ref.
  <cite|clausius-1865>, page 365.>

  <\quotation>
    If for the entire universe we conceive the same magnitude to be
    determined, consistently and with due regard to all circumstances, which
    for a single body I have called <em|entropy>, and if at the same time we
    introduce the other and simpler conception of energy, we may express in
    the following manner the fundamental laws of the universe which
    correspond to the two fundamental theorems of the mechanical theory of
    heat.

    1. <em|The energy of the universe is constant.>

    2. <em|The entropy of the universe tends to a maximum.>
  </quotation>

  Clausius was a patriotic German. During the Franco-Prussian war of
  1870\U71, he undertook the leadership of an ambulance corps composed of
  Bonn students, was wounded in the leg during the battles, and suffered
  disability for the rest of his life.
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|4>
    <associate|font-base-size|9>
    <associate|page-first|92>
    <associate|page-medium|paper>
    <associate|par-columns|2>
    <associate|par-columns-sep|1fn>
    <associate|preamble|false>
    <associate|section-nr|3>
    <associate|subsection-nr|2>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|92>>
    <associate|auto-2|<tuple|Clausius, Rudolf|92>>
    <associate|bio:clausius|<tuple|1|92>>
    <associate|footnote-1|<tuple|1|92>>
    <associate|footnote-2|<tuple|2|92>>
    <associate|footnote-3|<tuple|3|92>>
    <associate|footnote-4|<tuple|4|92>>
    <associate|footnr-1|<tuple|1|92>>
    <associate|footnr-2|<tuple|2|92>>
    <associate|footnr-3|<tuple|3|92>>
    <associate|footnr-4|<tuple|4|92>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      planck-48

      clausius-1854

      clausius-1865

      clausius-1865
    </associate>
    <\associate|idx>
      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <with|par-left|<quote|4tab>|Biographical
      Sketch<next-line><with|font-shape|<quote|small-caps>|Rudolf Julius
      Emmanuel Clausius> (1822\U1888) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>