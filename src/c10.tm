<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;
  </hide-preamble>

  <chapter|Electrolyte Solutions><label|10-electrolyte solutions><label|Chap.
  10><label|c10>

  <index-complex|<tuple|electrolyte|solution>||c10
  idx1|<tuple|Electrolyte|solution>>The thermodynamic properties of
  electrolyte solutions differ in significant ways from the properties of
  mixtures of nonelectrolytes.

  Here is an example. Pure HCl (hydrogen chloride) is a gas that is very
  soluble in water. A plot of the partial pressure of gaseous HCl in
  equilibrium with aqueous HCl, as a function of the solution molality (Fig.
  <vpageref|fig:10-HCl gas/soln>),<\float|float|hb>
    <\framed>
      <\big-figure|<image|10-SUP/HCL-P.eps|169pt|165pt||>>
        <label|fig:10-HCl gas/soln>Partial pressure of HCl in a gas phase
        equilibrated with aqueous HCl at <math|25 <degC>> and <math|1<br>>.
        Open circles: experimental data from Ref. <cite|bates-19>.
      </big-figure>
    </framed>
  </float> shows that the limiting slope at infinite dilution is not finite,
  but zero. What is the reason for this <subindex|Henry's law|not obeyed by
  electrolyte solute>non-Henry's law behavior? It must be because HCl is an
  electrolyte\Vit dissociates (ionizes) in the aqueous environment.

  It is customary to use a <em|molality> basis for the reference and standard
  states of electrolyte solutes. This is the only basis used in this chapter,
  even when not explicitly indicated for ions. The symbol
  <math|\<mu\><rsub|+><st>>, for instance, denotes the chemical potential of
  a cation in a standard state based on molality.

  In dealing with an electrolyte solute, we can refer to the solute (a
  substance) as a whole and to the individual charged ions that result from
  dissociation. We can apply the same general definitions of chemical
  potential, activity coefficient, and activity to these different species,
  but only the activity coefficient and activity of the solute as a whole can
  be evaluated experimentally.

  \;

  <section|Single-ion Quantities><label|10-single ion quantities><label|c10
  sec siq>

  Consider a solution of an electrolyte solute that dissociates completely
  into a cation species and an anion species. Subscripts <math|+> and
  <math|-> will be used to denote the cation and anion, respectively. The
  solute molality <math|m<B>> is defined as the amount of solute formula unit
  divided by the mass of solvent.

  We first need to investigate the relation between the chemical potential of
  an ion species and the <subindex|Electric|potential>electric potential of
  the solution phase.

  The electric potential <math|\<phi\>> in the interior of a phase is called
  the <subsubindex|Electric|potential|inner><em|inner electric potential>, or
  <index|Galvani potential><em|Galvani potential>. It is defined as the work
  needed to reversibly move an infinitesimal test charge into the phase from
  a position infinitely far from other charges, divided by the value of the
  test charge. The electrical potential energy of a charge in the phase is
  the product of <math|\<phi\>> and the charge.

  Consider a hypothetical process in which an infinitesimal amount
  <math|<dif>n<rsub|+>> of the cation is transferred into a solution phase at
  constant <math|T> and <math|p>. The quantity of charge transferred is
  <math|\<delta\>*<space|-0.17em>Q=z<rsub|+>*F<dif>n<rsub|+>>, where
  <math|z<rsub|+>> is the charge number (<math|+1>, <math|+2>, etc.) of the
  cation, and <math|F> is the <index|Faraday constant>Faraday
  constant.<footnote|The Faraday constant (page <pageref|Faraday const>) is
  the charge per amount of protons.> If the phase is at zero electric
  potential, the process causes no change in its electrical potential energy.
  However, if the phase has a finite electric potential <math|\<phi\>>, the
  transfer process changes its electrical potential energy by
  <math|\<phi\>*<space|0.17em>\<delta\>*<space|-0.17em>Q=z<rsub|+>*F*\<phi\><dif>n<rsub|+>>.
  Consequently, the internal energy change depends on <math|\<phi\>>
  according to

  <\equation>
    <dif>U<around|(|\<phi\>|)>=<dif>U<around|(|0|)>+z<rsub|+>*F*\<phi\><dif>n<rsub|+>
  </equation>

  where the electric potential is indicated in parentheses. The change in the
  Gibbs energy of the phase is given by <math|<dif>G=<dif><around|(|U-T*S+p*V|)>>,
  where <math|T>, <math|S>, <math|p>, and <math|V> are unaffected by the
  value of <math|\<phi\>>. The dependence of <math|<dif>G> on <math|\<phi\>>
  is therefore

  <\equation>
    <label|dG(phi)=dG(0)+.><dif>G<around|(|\<phi\>|)>=<dif>G<around|(|0|)>+z<rsub|+>*F*\<phi\><dif>n<rsub|+>
  </equation>

  The <index|Fundamental equations, Gibbs><subindex|Gibbs|fundamental
  equations>Gibbs fundamental equation for an open system,
  <math|<dif>G=-S<dif>T+V<difp>+<big|sum><rsub|i>\<mu\><rsub|i><dif>n<rsub|i>>
  (Eq. <reference|dG=-SdT+Vdp+sum(mu_i*dn_i)>), assumes the electric
  potential is zero. From this equation and Eq. <reference|dG(phi)=dG(0)+.>,
  the Gibbs energy change during the transfer process at constant <math|T>
  and <math|p> is found to depend on <math|\<phi\>> according to

  <\equation>
    <dif>G<around|(|\<phi\>|)>=<around*|[|<space|0.17em>\<mu\><rsub|+><around|(|0|)>+z<rsub|+>*F*\<phi\><space|0.17em>|]><dif>n<rsub|+>
  </equation>

  The chemical potential of the cation in a phase of electric potential
  <math|\<phi\>>, defined by the partial molar Gibbs energy
  <math|<bpd|G<around|(|\<phi\>|)>|n<rsub|+>|T,p>>, is therefore given by

  <\equation>
    <label|mu+(phi)=mu+(0)+z+Fphi>\<mu\><rsub|+><around|(|\<phi\>|)>=\<mu\><rsub|+><around|(|0|)>+z<rsub|+>*F*\<phi\>
  </equation>

  The corresponding relation for an anion is

  <\equation>
    <label|mu-(phi)=mu-(0)+z-Fphi>\<mu\><rsub|-><around|(|\<phi\>|)>=\<mu\><rsub|-><around|(|0|)>+z<rsub|->*F*\<phi\>
  </equation>

  where <math|z<rsub|->> is the charge number of the anion (<math|-1>,
  <math|-2>, etc.). For a charged species in general, we have

  <\equation>
    <label|mu_i(phi)=mu_i(0)+z+Fphi>\<mu\><rsub|i><around|(|\<phi\>|)>=\<mu\><rsub|i><around|(|0|)>+z<rsub|i>*F*\<phi\>
  </equation>

  <index-complex|<tuple|standard state|ion>|||<tuple|Standard state|of an
  ion>>We define the <em|standard state of an ion> on a molality basis in the
  same way as for a nonelectrolyte solute, with the additional stipulation
  that the ion is in a phase of zero electric potential. Thus, the standard
  state is a hypothetical state in which the ion is at molality <math|m<st>>
  with behavior extrapolated from infinite dilution on a molality basis, in a
  phase of pressure <math|p=p<st>> and electric potential <math|\<phi\>=0>.

  <index-complex|<tuple|chemical potential|standard|ion>|||<tuple|Chemical
  potential|standard|of an ion>>The <em|standard chemical potential>
  <math|\<mu\><rsub|+><st>> or <math|\<mu\><rsub|-><st>> of a cation or anion
  is the chemical potential of the ion in its standard state. Single-ion
  activities <math|a<rsub|+>> and <math|a<rsub|->> in a phase of zero
  electric potential are defined by relations having the form of Eq.
  <reference|act m,B>:

  <\equation>
    <label|mu(+)=muo(+)+..>\<mu\><rsub|+><around|(|0|)>=\<mu\><rsub|+><st>+R*T*ln
    a<rsub|+>*<space|2em>\<mu\><rsub|-><around|(|0|)>=\<mu\><rsub|-><st>+R*T*ln
    a<rsub|->
  </equation>

  As explained on page <pageref|a_i indep of h and phi>, <math|a<rsub|+>> and
  <math|a<rsub|->> should depend on the temperature, pressure, and
  composition of the phase, and not on the value of <math|\<phi\>>.

  From Eqs. <reference|mu+(phi)=mu+(0)+z+Fphi>,
  <reference|mu-(phi)=mu-(0)+z-Fphi>, and <reference|mu(+)=muo(+)+..>, the
  relations between the chemical potential of a cation or anion, its
  activity, and the electric potential of its phase, are found to be

  <\equation>
    \<mu\><rsub|+>=\<mu\><rsub|+><st>+R*T*ln
    a<rsub|+>+z<rsub|+>*F*\<phi\>*<space|2em>\<mu\><rsub|->=\<mu\><rsub|-><st>+R*T*ln
    a<rsub|->+z<rsub|i>*F*\<phi\>
  </equation>

  These relations are definitions of single-ion activities in a phase of
  electric potential <math|\<phi\>>.

  For a charged species in general, we can write<footnote|Some
  thermodynamicists call the quantity <math|<around|(|\<mu\><rsub|i><st>+R*T*ln
  a<rsub|i>|)>>, which depends only on <math|T>, <math|p>, and composition,
  the <em|chemical potential> of ion <math|i>, and the quantity
  <math|<around|(|\<mu\><rsub|i><st>+R*T*ln a<rsub|i>+z<rsub|i>*F*\<phi\>|)>>
  the <subindex|Electrochemical|potential><em|electrochemical potential> with
  symbol <math|<wide|\<mu\>|~><rsub|i>>.>

  <\equation>
    <label|mu_i(a_i,phi)=>\<mu\><rsub|i>=\<mu\><rsub|i><st>+R*T*ln
    a<rsub|i>+z<rsub|i>*F*\<phi\>
  </equation>

  Note that we can also apply this equation to an uncharged species, because
  the charge number <math|z<rsub|i>> is then zero and Eq.
  <reference|mu_i(a_i,phi)=> becomes the same as Eq.
  <vpageref|mu_i=mu_io+RTln(a_i)>.

  Of course there is no experimental way to evaluate either
  <math|\<mu\><rsub|+>> or <math|\<mu\><rsub|->> relative to a reference
  state or standard state, because it is impossible to add cations or anions
  by themselves to a solution. We can nevertheless write some theoretical
  relations involving <math|\<mu\><rsub|+>> and <math|\<mu\><rsub|->>.

  For a given temperature and pressure, we can write the dependence of the
  chemical potentials of the ions on their molalities in the same form as
  that given by Eq. <reference|act coeff m,B> for a nonelectrolyte solute:

  <\equation>
    <label|mu(+)=mu(ref)+..>\<mu\><rsub|+>=\<mu\><rsub|+><rf>+R*T*ln
    <around*|(|<g><rsub|+><frac|m<rsub|+>|m<st>>|)>*<space|2em>\<mu\><rsub|->=\<mu\><rsub|-><rf>+R*T*ln
    <around*|(|<g><rsub|-><frac|m<rsub|->|m<st>>|)>
  </equation>

  Here <math|\<mu\><rsub|+><rf>> and <math|\<mu\><rsub|-><rf>> are the
  chemical potentials of the cation and anion in
  <index-complex|<tuple|reference state|ion>|||<tuple|Reference state|of an
  ion>>solute reference states. Each reference state is defined as a
  hypothetical solution with the same temperature, pressure, and electric
  potential as the solution under consideration; in this solution, the
  molality of the ion has the standard value <math|m<st>>, and the ion
  behaves according to Henry's law based on molality. <math|<g><rsub|+>> and
  <math|<g><rsub|->> are single-ion <index-complex|<tuple|activity
  coefficient|ion>|||<tuple|Activity coefficient|of an ion>>activity
  coefficients on a molality basis.

  The single-ion activity coefficients approach unity in the limit of
  infinite dilution:

  <equation-cov2|<label|gamma(+)-\<gtr\>1><g><rsub|+><ra>1<space|1em><text|and><space|1em><g><rsub|-><ra>1<space|1em><text|as><space|1em>m<B><ra>0|(constant
  <math|T>, <math|p>, and <math|\<phi\>>)>

  In other words, we assume that in an extremely dilute electrolyte solution
  each individual ion behaves like a nonelectrolyte solute species in an
  ideal-dilute solution. At a finite solute molality, the values of
  <math|<g><rsub|+>> and <math|<g><rsub|->> are the ones that allow Eq.
  <reference|mu(+)=mu(ref)+..> to give the correct values of the quantities
  <math|<around|(|\<mu\><rsub|+>-\<mu\><rsub|+><rf>|)>> and
  <math|<around|(|\<mu\><rsub|->-\<mu\><rsub|-><rf>|)>>. We have no way to
  actually measure these quantities experimentally, so we cannot evaluate
  either <math|<g><rsub|+>> or <math|<g><rsub|->>.

  <index-complex|<tuple|pressure factor|ion>|||<tuple|Pressure factor|of an
  ion>>We can define single-ion pressure factors <math|<G><rsub|+>> and
  <math|<G><rsub|->> as follows:

  <\equation>
    <label|Gamma(+)=><G><rsub|+><defn>exp
    <around*|(|<frac|\<mu\><rsub|+><rf>-\<mu\><rsub|+><st>|R*T>|)>\<approx\>exp
    <around*|[|<frac|V<rsub|+><rsup|\<infty\>>*<around|(|p-p<st>|)>|R*T>|]>
  </equation>

  <\equation>
    <label|Gamma(-)=><G><rsub|-><defn>exp
    <around*|(|<frac|\<mu\><rsub|-><rf>-\<mu\><rsub|-><st>|R*T>|)>\<approx\>exp
    <around*|[|<frac|V<rsub|-><rsup|\<infty\>>*<around|(|p-p<st>|)>|R*T>|]>
  </equation>

  The approximations in these equations are like those in Table
  <reference|tbl:9-Gamma_i> for nonelectrolyte solutes; they are based on the
  assumption that the partial molar volumes <math|V<rsub|+>> and
  <math|V<rsub|->> are independent of pressure.

  <index-complex|<tuple|activity|ion>|||<tuple|Activity|of an ion>>From Eqs.
  <reference|mu(+)=muo(+)+..>, <reference|mu(+)=mu(ref)+..>,
  <reference|Gamma(+)=>, and <reference|Gamma(-)=>, the single-ion activities
  are related to the solution composition by

  <\equation>
    <label|a(+)=...>a<rsub|+>=<G><rsub|+><g><rsub|+><frac|m<rsub|+>|m<st>>*<space|2em>a<rsub|->=<G><rsub|-><g><rsub|-><frac|m<rsub|->|m<st>>
  </equation>

  Then, from Eq. <reference|mu_i(a_i,phi)=>, we have the following relations
  between the chemical potentials and molalities of the ions:

  <\equation>
    <label|mu+(phi)=>\<mu\><rsub|+>=\<mu\><rsub|+><st>+R*T*ln
    <around|(|<G><rsub|+><g><rsub|+>m<rsub|+>/m<st>|)>+z<rsub|+>*F*\<phi\>
  </equation>

  <\equation>
    <label|mu-(phi)=>\<mu\><rsub|->=\<mu\><rsub|-><st>+R*T*ln
    <around|(|<G><rsub|-><g><rsub|->m<rsub|->/m<st>|)>+z<rsub|->*F*\<phi\>
  </equation>

  Like the values of <math|<g><rsub|+>> and <math|<g><rsub|->>, values of the
  single-ion quantities <math|a<rsub|+>>, <math|a<rsub|->>,
  <math|<G><rsub|+>>, and <math|<G><rsub|->> cannot be determined by
  experiment.

  <section|Solution of a Symmetrical Electrolyte><label|c10 sec sse>

  <index-complex|<tuple|electrolyte|symmetrical>||c10 sec sse
  idx1|<tuple|Electrolyte|symmetrical>>Let us consider properties of an
  electrolyte solute as a whole. The simplest case is that of a binary
  solution in which the solute is a symmetrical strong electrolyte\Va
  substance whose formula unit has one cation and one anion that dissociate
  completely. This condition will be indicated by <math|\<nu\>=2>, where
  <math|\<nu\>> is the number of ions per formula unit. In an aqueous
  solution, the solute with <math|\<nu\>> equal to 2 might be a 1:1 salt such
  as NaCl, a 2:2 salt such as MgSO<rsub|<math|4>>, or a strong monoprotic
  acid such as HCl.

  In this binary solution, the chemical potential of the solute as a whole is
  defined in the usual way as the partial molar Gibbs energy

  <\equation>
    \<mu\><B><defn><Pd|G|n<B>|T,p,n<A>>
  </equation>

  and is a function of <math|T>, <math|p>, and the solute molality
  <math|m<B>>. Although <math|\<mu\><B>> under given conditions must in
  principle have a definite value, we are not able to actually evaluate it
  because we have no way to measure precisely the energy brought into the
  system by the solute. This energy contributes to the internal energy and
  thus to <math|G>. We can, however, evaluate the differences
  <math|\<mu\><B>-\<mu\><mbB><rf>> and <math|\<mu\><B>-\<mu\><mbB><st>>.

  We can write the <index|Additivity rule>additivity rule (Eq.
  <reference|X=sum(X_i*n_i)>) for <math|G> as either

  <\equation>
    G=n<A>\<mu\><A>+n<B>\<mu\><B>
  </equation>

  or

  <\equation>
    G=n<A>\<mu\><A>+n<rsub|+>*\<mu\><rsub|+>+n<rsub|->*\<mu\><rsub|->
  </equation>

  A comparison of these equations for a symmetrical electrolyte
  (<math|n<B>=n<rsub|+>=n<rsub|->>) gives us the relation

  <\equation-cov2|<label|muB=mu(+)+mu(-)>\<mu\><B>=\<mu\><rsub|+>+\<mu\><rsub|->>
    (<math|v=2>)
  </equation-cov2>

  <index-complex|<tuple|chemical potential|symmetrical
  electrolyte>|||<tuple|Chemical potential|of a symmetrical electrolyte>>We
  see that the solute chemical potential in this case is the <em|sum> of the
  single-ion chemical potentials.

  The solution is a phase of electric potential <math|\<phi\>>. From Eqs.
  <reference|mu+(phi)=mu+(0)+z+Fphi> and <reference|mu-(phi)=mu-(0)+z-Fphi>,
  the sum <math|\<mu\><rsub|+>+\<mu\><rsub|->> appearing in Eq.
  <reference|muB=mu(+)+mu(-)> is

  <\equation>
    \<mu\><rsub|+><around|(|\<phi\>|)>+\<mu\><rsub|-><around|(|\<phi\>|)>=\<mu\><rsub|+><around|(|0|)>+\<mu\><rsub|-><around|(|0|)>+<around|(|z<rsub|+>+z<rsub|->|)>*F*\<phi\>
  </equation>

  For the symmetrical electrolyte, the sum
  <math|<around|(|z<rsub|+>+z<rsub|->|)>> is zero, so that <math|\<mu\><B>>
  is equal to <math|\<mu\><rsub|+><around|(|0|)>+\<mu\><rsub|-><around|(|0|)>>.
  We substitute the expressions of Eq. <reference|mu(+)=mu(ref)+..>, use the
  relation <math|\<mu\><mbB><rf>=\<mu\><rsub|+><rf>+\<mu\><rsub|-><rf>> with
  reference states at <math|\<phi\>=0>, set the ion molalities
  <math|m<rsub|+>> and <math|m<rsub|->> equal to <math|m<B>>, and obtain

  <equation-cov2|<label|muB=muB(ref)+RTln(g(+)g(-)..>\<mu\><B>=\<mu\><mbB><rf>+R*T*ln
  <around*|[|<g><rsub|+><g><rsub|-><around*|(|<frac|m<B>|m<st>>|)><rsup|2>|]>|(<math|v=2>)>

  The important feature of this relation is the appearance of the <em|second>
  power of <math|m<B>/m<st>>, instead of the first power as in the case of a
  nonelectrolyte. Also note that <math|\<mu\><B>> does not depend on
  <math|\<phi\>>, unlike <math|\<mu\><rsub|+>> and <math|\<mu\><rsub|->>.

  Although we cannot evaluate <math|<g><rsub|+>> or <math|<g><rsub|->>
  individually, we can evaluate the product <math|<g><rsub|+><g><rsub|->>.
  This product is the square of the <index-complex|<tuple|mean ionic activity
  coefficient|activity coefficient>|||<tuple|Mean ionic activity
  coefficient|see Activity coefficient, mean
  ionic>><index-complex|<tuple|activity coefficient|mean ionic|symmetrical
  electrolyte>|||<tuple|Activity coefficient|mean ionic|of a symmetrical
  electrolyte>><newterm|mean ionic activity coefficient>
  <math|<g><rsub|\<pm\>>>, defined for a symmetrical electrolyte by

  <equation-cov2|<label|g(+-)=sqrt((g+)(g-))><g><rsub|\<pm\>><defn><sqrt|<g><rsub|+><g><rsub|->>|(<math|v=2>)>

  With this definition, Eq. <reference|muB=muB(ref)+RTln(g(+)g(-)..> becomes

  <equation-cov2|<label|muB=muB(ref)+RTln(g(+-)..>\<mu\><B>=\<mu\><mbB><rf>+R*T*ln
  <around*|[|<around*|(|<g><rsub|\<pm\>>|)><rsup|2><around*|(|<frac|m<B>|m<st>>|)><rsup|2>|]>|(<math|v=2>)>

  Since it is possible to determine the value of
  <math|\<mu\><B>-\<mu\><mbB><rf>> for a solution of known molality,
  <math|<g><rsub|\<pm\>>> is a measurable quantity.

  <\quote-env>
    \ If the electrolyte (e.g., HCl) is sufficiently volatile, its mean ionic
    activity coefficient in a solution can be evaluated from partial pressure
    measurements of an equilibrated gas phase. Section <reference|10-ionic
    act coeffs from osmotic coeffs> will describe a general method by which
    <math|<g><rsub|\<pm\>>> can be found from osmotic coefficients. Section
    <reference|14-eval Eo> describes how, in favorable cases, it is possible
    to evaluate <math|<g><rsub|\<pm\>>> from the equilibrium cell potential
    of a galvanic cell.
  </quote-env>

  The activity <math|a<mbB>> of a solute substance on a molality basis is
  defined by Eq. <vpageref|act m,B>:

  <\equation>
    <label|muB=muB^o+RTln(a)>\<mu\><B>=\<mu\><mbB><st>+R*T*ln a<mbB>
  </equation>

  Here <math|\<mu\><mbB><st>> is the chemical potential of the solute in its
  standard state, which is the solute reference state at the standard
  pressure. By equating the expressions for <math|\<mu\><B>> given by Eqs.
  <reference|muB=muB(ref)+RTln(g(+-)..> and <reference|muB=muB^o+RTln(a)> and
  solving for the activity, we obtain<index-complex|<tuple|activity|symmetrical
  electrolyte>|||<tuple|Activity|of a symmetrical electrolyte>>

  <equation-cov2|<label|a(mB),nu=2>a<mbB>=<G><mbB><around*|(|<g><rsub|\<pm\>>|)><rsup|2><around*|(|<frac|m<B>|m<st>>|)><rsup|2>|(<math|v=2>)>

  where <math|<G><mbB>> is the <index-complex|<tuple|pressure
  factor|symmetrical electrolyte>|||<tuple|Pressure factor|of a symmetrical
  electrolyte>>pressure factor defined by

  <\equation>
    <label|Gamma(m,B) defn><G><mbB><defn>exp
    <around*|(|<frac|\<mu\><mbB><rf>-\<mu\><mbB><st>|R*T>|)>
  </equation>

  We can use the appropriate expression in Table <vpageref|tbl:9-Gamma_i> to
  evaluate <math|<G><mbB>> at an arbitrary pressure <math|p<rprime|'>>:

  <\equation>
    <label|Gamma(m,B)=><G><mbB><around|(|p<rprime|'>|)>=exp
    <around*|(|<big|int><rsub|p<st>><rsup|p<rprime|'>><frac|V<B><rsup|\<infty\>>|R*T><difp>|)>\<approx\>exp
    <around*|[|<frac|V<B><rsup|\<infty\>><around|(|p<rprime|'>-p<st>|)>|R*T>|]>
  </equation>

  The value of <math|<G><mbB>> is <math|1> at the standard pressure, and
  close to <math|1> at any reasonably low pressure (page <pageref|pressure
  factor omitted>). For this reason it is common to see Eq.
  <reference|a(mB),nu=2> written as <math|a<mbB>=<g><rsub|\<pm\>><rsup|2><around|(|m<B>/m<st>|)><rsup|2>>,
  with <math|<G><mbB>> omitted.

  Equation <reference|a(mB),nu=2> predicts that the activity of HCl in
  aqueous solutions is proportional, in the limit of infinite dilution, to
  the <em|square> of the HCl molality. In contrast, the activity of a
  <em|non>electrolyte solute is proportional to the <em|first> power of the
  molality in this limit. This predicted behavior of aqueous HCl is
  consistent with the data plotted in Fig. <vpageref|fig:10-HCl gas/soln>,
  and is confirmed by the data for dilute HCl solutions shown in Fig.
  <reference|fig:10-aq HCl activity>(a).<\float|float|thb>
    <\framed>
      <\big-figure|<image|10-SUP/HCL-ACT.eps|361pt|130pt||>>
        <label|fig:10-aq HCl activity>Aqueous HCl at <math|25 <degC>> and
        <math|1<br>>.<note-ref|+1PualPXI2C0ZUoKK>

        <\enumerate-alpha>
          <item>HCl activity on a molality basis as a function of molality
          squared. The dashed line is the extrapolation of the ideal-dilute
          behavior.

          <item>Same as (a) at a greatly reduced scale; the filled circle
          indicates the solute reference state.

          <item>Mean ionic activity coefficient of HCl as a function of
          molality.
        </enumerate-alpha>

        \;

        <note-inline||+1PualPXI2C0ZUoKK>Curves based on experimental
        parameter values in Ref. <cite|harned-58>, Table 11-5-1.
      </big-figure>
    </framed>
  </float> The dashed line in Fig. <reference|fig:10-aq HCl activity>(a) is
  the extrapolation of the ideal-dilute behavior given by
  <math|a<mbB>=<around|(|m<B>/m<st>|)><rsup|2>>. The extension of this line
  to <math|m<B>=m<st>> establishes the hypothetical solute reference state
  based on molality, indicated by a filled circle in Fig.
  <reference|fig:10-aq HCl activity>(b). (Since the data are for solutions at
  the standard pressure of <math|1<br>>, the solute reference state shown in
  the figure is also the solute standard state.)

  The solid curve of Fig. <reference|fig:10-aq HCl activity>(c) shows how the
  mean ionic activity coefficient of HCl varies with molality in
  approximately the same range of molalities as the data shown in Fig.
  <reference|fig:10-aq HCl activity>(b). In the limit of infinite dilution,
  <math|<g><rsub|\<pm\>>> approaches unity. The slope of the curve approaches
  <math|-\<infty\>><label|electrolyte act coeff vs molality> in this limit,
  quite unlike the behavior described in Sec. <reference|9-nonideal dil
  solns> for the activity coefficient of a nonelectrolyte solute.

  For a symmetrical strong electrolyte, <math|<g><rsub|\<pm\>>> is the
  geometric average of the single-ion activity coefficients
  <math|<g><rsub|+>> and <math|<g><rsub|->>. We have no way of evaluating
  <math|<g><rsub|+>> or <math|<g><rsub|->> individually, even if we know the
  value of <math|<g><rsub|\<pm\>>>. For instance, we cannot assume that
  <math|<g><rsub|+>> and <math|<g><rsub|->> are
  equal.<index-complex|<tuple|electrolyte|symmetrical>||c10 sec sse
  idx1|<tuple|Electrolyte|symmetrical>>

  <section|Electrolytes in General><label|10-el in gen><label|c10 sec eig>

  The formula unit of a <em|non>symmetrical electrolyte solute has more than
  two ions. General formulas for the solute as a whole are more complicated
  than those for the symmetrical case treated in the preceding section, but
  are derived by the same reasoning.

  Again we assume the solute dissociates completely into its constituent
  ions. We define the following symbols:

  <\indent>
    <tabular|<tformat|<table|<row|<cell|<math|\<nu\><rsub|+>>>|<cell|<math|=>>|<cell|the
    number of cations per solute formula unit>>|<row|<cell|<math|\<nu\><rsub|->>>|<cell|<math|=>>|<cell|the
    number of anions per solute formula unit>>|<row|<cell|<math|\<nu\>>>|<cell|<math|=>>|<cell|the
    sum <math|\<nu\><rsub|+>+\<nu\><rsub|->>>>>>>
  </indent>

  <no-indent>For example, if the solute formula is
  Al<rsub|<math|2>>(SO<rsub|<math|4>>)<rsub|<math|3>>, the values are
  <math|\<nu\><rsub|+>=2>, <math|\<nu\><rsub|->=3>, and <math|\<nu\>=5>.

  <subsection|Solution of a single electrolyte><label|c10 sec eig-single>

  In a solution of a single electrolyte solute that is not necessarily
  symmetrical, the ion molalities are related to the overall solute molality
  by

  <\equation>
    <label|m+=nu+mB, m-=nu-mB>m<rsub|+>=\<nu\><rsub|+>*m<B><space|2em>m<rsub|->=\<nu\><rsub|->*m<B>
  </equation>

  From the <index|Additivity rule>additivity rule for the Gibbs energy, we
  have

  <\eqnarray*>
    <tformat|<table|<row|<cell|G>|<cell|=>|<cell|n<A>*\<mu\><A>+n<B>*\<mu\><B>>>|<row|<cell|>|<cell|=>|<cell|n<A>*\<mu\><A>+\<nu\><rsub|+>*n<B>*\<mu\><rsub|+>+\<nu\><rsub|->*n<B>*\<mu\><rsub|-><eq-number>>>>>
  </eqnarray*>

  giving the relation

  <\equation>
    <label|muB=nu+mu+ + nu-mu->\<mu\><B>=\<nu\><rsub|+>*\<mu\><rsub|+>+\<nu\><rsub|->*\<mu\><rsub|->
  </equation>

  in place of Eq. <reference|muB=mu(+)+mu(-)>. The cations and anions are in
  the same phase of electric potential <math|\<phi\>>. We use Eqs.
  <reference|mu+(phi)=mu+(0)+z+Fphi> and <reference|mu-(phi)=mu-(0)+z-Fphi>
  to obtain

  <\equation>
    \<nu\><rsub|+>*\<mu\><rsub|+><around|(|\<phi\>|)>+\<nu\><rsub|->*\<mu\><rsub|-><around|(|\<phi\>|)>=\<nu\><rsub|+>*\<mu\><rsub|+><around|(|0|)>+\<nu\><rsub|->*\<mu\><rsub|-><around|(|0|)>+<around|(|\<nu\><rsub|+>*z<rsub|+>+\<nu\><rsub|->*z<rsub|->|)>*F*\<phi\>
  </equation>

  Electrical neutrality requires that <math|<around|(|\<nu\><rsub|+>*z<rsub|+>+\<nu\><rsub|->*z<rsub|->|)>>
  be zero, giving

  <\equation>
    <label|muB(electrolyte)=>\<mu\><B>=\<nu\><rsub|+>*\<mu\><rsub|+><around|(|0|)>+\<nu\><rsub|->*\<mu\><rsub|-><around|(|0|)>
  </equation>

  By combining Eq. <reference|muB(electrolyte)=> with Eqs.
  <reference|mu(+)=mu(ref)+..>, <reference|m+=nu+mB, m-=nu-mB>, and
  <reference|muB=nu+mu+ + nu-mu->, we obtain

  <\equation>
    <label|muB, general electrolyte>\<mu\><B>=\<mu\><B><rf>+R*T*ln
    <around*|[|<around*|(|\<nu\><rsub|+><rsup|\<nu\><rsub|+>>*\<nu\><rsub|-><rsup|\<nu\><rsub|->>|)><around*|(|<g><rsub|+><rsup|\<nu\><rsub|+>>|)><around*|(|<g><rsub|-><rsup|\<nu\><rsub|->>|)><around*|(|<frac|m<B>|m<st>>|)><rsup|\<nu\>><space|0.17em>|]>
  </equation>

  where <math|\<mu\><B><rf>=\<nu\><rsub|+>*\<mu\><rsub|+><rf>+\<nu\><rsub|->*\<mu\><rsub|-><rf>>
  is the <index-complex|<tuple|chemical potential|electrolyte
  solute>|||<tuple|Chemical potential|of<space|1em>an electrolyte
  solute>>chemical potential of the solute in the hypothetical reference
  state at <math|\<phi\>=0> in which B is at the standard molality and
  behaves as at infinite dilution. Equation <reference|muB, general
  electrolyte> is the generalization of Eq.
  <reference|muB=muB(ref)+RTln(g(+)g(-)..>. It shows that although
  <math|\<mu\><rsub|+>> and <math|\<mu\><rsub|->> depend on <math|\<phi\>>,
  <math|\<mu\><B>> does not.

  The <index-complex|<tuple|activity coefficient|mean ionic|electrolyte
  solute>|||<tuple|Activity coefficient|mean ionic|of an electrolyte
  solute>>mean ionic activity coefficient <math|<g><rsub|\<pm\>>> is defined
  in general by

  <\equation>
    <label|g(+-)^nu=...><g><rsub|\<pm\>><rsup|\<nu\>>=<around*|(|<g><rsub|+><rsup|\<nu\><rsub|+>>|)><around*|(|<g><rsub|-><rsup|\<nu\><rsub|->>|)>
  </equation>

  or

  <\equation>
    <label|g(+-)=()^(1/nu)><g><rsub|\<pm\>>=<around*|(|<g><rsub|+><rsup|\<nu\><rsub|+>><g><rsub|-><rsup|\<nu\><rsub|->>|)><rsup|1/\<nu\>>
  </equation>

  Thus <math|<g><rsub|\<pm\>>> is a geometric average of <math|<g><rsub|+>>
  and <math|<g><rsub|->> weighted by the numbers of the cations and anions in
  the solute formula unit. With a substitution from Eq.
  <reference|g(+-)^nu=...>, Eq. <reference|muB, general electrolyte> becomes

  <\equation>
    <label|muB, gen. electrolyte>\<mu\><B>=\<mu\><B><rf>+R*T*ln
    <around*|[|<around*|(|\<nu\><rsub|+><rsup|\<nu\><rsub|+>>*\<nu\><rsub|-><rsup|\<nu\><rsub|->>|)><g><rsub|\<pm\>><rsup|\<nu\>><around*|(|<frac|m<B>|m<st>>|)><rsup|\<nu\>><space|0.17em>|]>
  </equation>

  Since <math|\<mu\><B>-\<mu\><B><rf>> is a measurable quantity, so also is
  <math|<g><rsub|\<pm\>>>.

  The solute activity, defined by <math|\<mu\><B>=\<mu\><mbB><st>+R*T*ln
  a<mbB>>, is<index-complex|<tuple|activity|electrolyte
  solute>|||<tuple|Activity|of an electrolyte solute>>

  <\equation>
    <label|a(mB),general>a<mbB>=<around*|(|\<nu\><rsub|+><rsup|\<nu\><rsub|+>>*\<nu\><rsub|-><rsup|\<nu\><rsub|->>|)><G><mbB><space|0.17em><g><rsub|\<pm\>><rsup|\<nu\>><around*|(|<frac|m<B>|m<st>>|)><rsup|\<nu\>>
  </equation>

  where <math|<G><mbB>> is the <index-complex|<tuple|pressure
  factor|electrolyte solute>|||<tuple|Pressure factor|of an electrolyte
  solute>>pressure factor that we can evaluate with Eq.
  <reference|Gamma(m,B)=>. Equation <reference|a(mB),general> is the
  generalization of Eq. <reference|a(mB),nu=2>. From Eqs.
  <reference|Gamma(+)=>, <reference|Gamma(-)=>, and <reference|Gamma(m,B)
  defn> and the relations <math|\<mu\><B><rf>=\<nu\><rsub|+>*\<mu\><rsub|+><rf>+\<nu\><rsub|->*\<mu\><rsub|-><rf>>
  and <math|\<mu\><B><st>=\<nu\><rsub|+>*\<mu\><rsub|+><st>+\<nu\><rsub|->*\<mu\><rsub|-><st>>,
  we obtain the relation

  <\equation>
    <label|Gamma(m,B)=Gamma+^(nu+)Gamma-^(nu-)><G><mbB>=<G><rsub|+><rsup|\<nu\><rsub|+>><G><rsub|-><rsup|\<nu\><rsub|->>
  </equation>

  <subsection|Multisolute solution><label|c10 sec eig-multi>

  <subindex|Solution|multisolute electrolyte>Equation <reference|muB=nu+mu+ +
  nu-mu-> relates the chemical potential of electrolyte B in a binary
  solution to the single-ion chemical potentials of its constituent ions:

  <\equation>
    <label|muB=nu+mu+ + nu-mu-(again)>\<mu\><B>=\<nu\><rsub|+>*\<mu\><rsub|+>+\<nu\><rsub|->*\<mu\><rsub|->
  </equation>

  This relation is valid for each individual solute substance in a
  multisolute solution, even when two or more of the electrolyte solutes have
  an ion species in common.

  As an illustration of this principle, consider a solution prepared by
  dissolving amounts <math|n<B>> of BaI<rsub|<math|2>> and <math|n<C>> of CsI
  in an amount <math|n<A>> of H<rsub|<math|2>>O. Assume the dissolved salts
  are completely dissociated into ions, with the I<rsup|<math|->> ion common
  to both. The <index|Additivity rule>additivity rule for the Gibbs energy of
  this solution can be written in the form

  <\equation>
    <label|G=...>G=n<A>*\<mu\><A>+n<B>*\<mu\><B>+n<C>*\<mu\><C>
  </equation>

  and also, using single-ion quantities, in the form

  <\equation>
    <label|G(common ion)>G=n<A>\<mu\><A>+n<B>\<mu\><around|(|<text|Ba><rsup|2+>|)>+2*n<B>*\<mu\><around|(|<text|I><rsup|->|)>+n<C>*\<mu\><around|(|<text|Cs><rsup|+>|)>+n<C>*\<mu\><around|(|<text|I><rsup|->|)>
  </equation>

  Comparing Eqs. <reference|G=...> and <reference|G(common ion)>, we find the
  following relations must exist between the chemical potentials of the
  solute substances and the ion species:

  <\equation>
    \<mu\><B>=\<mu\><around|(|<text|Ba><rsup|2+>|)>+2*\<mu\><around|(|<text|I><rsup|->|)>*<space|2em>\<mu\><C>=\<mu\><around|(|<text|Cs><rsup|+>|)>+\<mu\><around|(|<text|I><rsup|->|)>
  </equation>

  These relations agree with Eq. <reference|muB=nu+mu+ + nu-mu-(again)>. Note
  that <math|\<mu\><around|(|<text|I><rsup|->|)>>, the chemical potential of
  the ion common to both salts, appears in both relations.

  The solute activity <math|a<mbB>> is defined by the relation
  <math|\<mu\><B>=\<mu\><B><st>+R*T*ln a<mbB>> (Eq.
  <reference|muB=muB^o+RTln(a)>). Using this relation together with Eqs.
  <reference|mu(+)=muo(+)+..> and <reference|a(+)=...>, we find that the
  solute activity is related to ion molalities by

  <\equation>
    <label|a(B)(multisolute)>a<mbB>=<G><mbB><space|0.17em><g><rsub|\<pm\>><rsup|\<nu\>><around*|(|<frac|m<rsub|+>|m<st>>|)><rsup|\<nu\><rsub|+>><around*|(|<frac|m<rsub|->|m<st>>|)><rsup|\<nu\><rsub|->>
  </equation>

  where the pressure factor <math|<G><mbB>> is defined in Eq.
  <reference|Gamma(m,B) defn>. The ion molalities in this expression refer to
  the constituent ions of solute B, which in a multisolute solution are not
  necessarily present in the same stoichiometric ratio as in the solute
  substance.

  For instance, suppose we apply Eq. <reference|a(B)(multisolute)> to the
  solution of BaI<rsub|<math|2>> and CsI used above as an illustration of a
  multisolute solution, letting <math|a<mbB>> be the activity of solute
  substance BaI<rsub|<math|2>>. The quantities <math|m<rsub|+>> and
  <math|m<rsub|->> in the equation are then the molalities of the
  Ba<rsup|<math|2+>> and I<rsup|<math|->> ions, and <math|<g><rsub|\<pm\>>>
  is the mean ionic activity coefficient of the dissolved BaI<rsub|<math|2>>.
  Note that in this solution the Ba<rsup|<math|2+>> and I<rsup|<math|->> ions
  are not present in the 1:2 ratio found in BaI<rsub|<math|2>>, because
  I<rsup|<math|->> is a constituent of both solutes.

  <subsection|Incomplete dissociation><label|c10
  sec-eig-incomplete-dissociation>

  In the preceding sections of this chapter, the electrolyte solute or
  solutes have been assumed to be completely dissociated into their
  constituent ions at all molalities. Some solutions, however, contain
  <em|ion pairs>\Vclosely associated ions of opposite charge. Furthermore, in
  solutions of some electrolytes (often called \Pweak\Q electrolytes), an
  equilibrium is established between ions and electrically-neutral molecules.
  In these kinds of solutions, the relations between solute molality and ion
  molalities given by Eq. <reference|m+=nu+mB, m-=nu-mB> are no longer valid.
  When dissociation is not complete, the expression for <math|\<mu\><B>>
  given by Eq. <reference|muB, gen. electrolyte> can still be used. However,
  the quantity <math|<g><rsub|\<pm\>>> appearing in the expression no longer
  has the physical significance of being the geometric average of the
  activity coefficients of the actual dissociated ions, and is called the
  <subindex|Stoichiometric|activity coefficient><subindex|Activity
  coefficient|stoichiometric><newterm|stoichiometric activity coefficient> of
  the electrolyte.

  <section|The Debye-H�ckel Equation><label|10-Debye Huckel><label|c10 sec
  dhe>

  <index-complex|<tuple|debye-huckel|theory>||c10 sec dhe
  idx1|<tuple|Debye\UH�ckel|theory>>The theory of <index|Debye, Peter>Peter
  Debye and <index|H�ckel, Erich>Erich H�ckel (1923) provides theoretical
  expressions for single-ion activity coefficients and mean ionic activity
  coefficients in electrolyte solutions. The expressions in one form or
  another are very useful for extrapolation of quantities that include mean
  ionic activity coefficients to low solute molality or infinite dilution.

  The only interactions the theory considers are the electrostatic
  interactions between ions. These interactions are much stronger than those
  between uncharged molecules, and they die off more slowly with distance. If
  the positions of ions in an electrolyte solution were completely random,
  the net effect of electrostatic ion\Uion interactions would be zero,
  because each cation\Ucation or anion\Uanion repulsion would be balanced by
  a cation\Uanion attraction. The positions are not random, however: each
  cation has a surplus of anions in its immediate environment, and each anion
  has a surplus of neighboring cations. Each ion therefore has a net
  attractive interaction with the surrounding ion atmosphere. The result for
  a cation species at low electrolyte molality is a decrease of
  <math|\<mu\><rsub|+>> compared to the cation at same molality in the
  absence of ion\Uion interactions, meaning that the single-ion activity
  coefficient <math|<g><rsub|+>> becomes less than <math|1> as the
  electrolyte molality is increased beyond the ideal-dilute range. Similarly,
  <math|<g><rsub|->> also becomes less than <math|1>.

  According to the Debye\UH�ckel theory, the <index-complex|<tuple|activity
  coefficient|ion|debye huckel>|||<tuple|Activity coefficient|of an ion|from
  the Debye\UH�ckel theory>>single-ion activity coefficient
  <math|<g><rsub|i>> of ion <math|i> in a solution of one or more
  electrolytes is given by <index-complex|<tuple|debye huckel|equation|single
  ion>|||<tuple|Debye\UH�ckel|equation|for a single-ion activity
  coefficient>>

  <\equation>
    <label|ln(g_i) (DH)>ln <g><rsub|i>=-<frac|A<rsub|<text|D>\<nocomma\><text|H>>*z<rsub|i><rsup|2>*<sqrt|I<rsub|m>>|1+B<rsub|<text|D>\<nocomma\><text|H>>*a*<sqrt|I<rsub|m>>>
  </equation>

  where

  <\itemize>
    <item><subindex|Charge|number><math|z<rsub|i>=> the charge number of ion
    <math|i> (<math|+1>, <math|-2>, etc.);

    <item><math|I<rsub|m>=> the <index|Ionic strength><newterm|ionic
    strength> of the solution on a molality basis, defined
    by<footnote|<index|Lewis, Gilbert Newton>Lewis and <index|Randall,
    Merle>Randall (Ref. <cite|lewis-21>) introduced the term <em|ionic
    strength>, defined by this equation, two years before the Debye\UH�ckel
    theory was published. They found empirically that in dilute solutions,
    the mean ionic activity coefficient of a given strong electrolyte is the
    same in all solutions having the same ionic strength.>

    <\equation>
      <label|I(m) def>I<rsub|m><defn><tfrac|1|2>*<space|-0.17em><below|<big|sum>|<text|all
      ions>><space|-0.17em>m<rsub|j>*z<rsub|j><rsup|2>
    </equation>

    <item><math|A<rsub|<text|D>\<nocomma\><text|H>>> and
    <math|B<rsub|<text|D>\<nocomma\><text|H>>> are defined functions of the
    kind of solvent and the temperature;

    <item><math|a> is an adjustable parameter, equal to the mean effective
    distance of closest approach of other ions in the solution to one of the
    <math|i> ions.
  </itemize>

  <\quote-env>
    \ The definitions of the quantities <math|A<rsub|<text|D>\<nocomma\><text|H>>>
    and <math|B<rsub|<text|D>\<nocomma\><text|H>>> appearing in Eq.
    <reference|ln(g_i) (DH)> are<label|A(DH) defn>

    <\align>
      <tformat|<table|<row|<cell|A<rsub|<text|D>\<nocomma\><text|H>>>|<cell|<defn><around*|(|N<A><rsup|2>*e<rsup|3>/<around*|(|8*\<pi\>|)>|)>*<around*|(|2*\<rho\><A><rsup|\<ast\>>|)><rsup|1/2>*<around*|(|\<epsilon\><rsub|<text|r>>*\<epsilon\><rsub|0>*R*T|)><rsup|-3/2><eq-number>>>|<row|<cell|B<rsub|<text|D>\<nocomma\><text|H>>>|<cell|<defn>N<A>*e*<around*|(|2*\<rho\><A><rsup|\<ast\>>|)><rsup|1/2>*<around*|(|\<epsilon\><rsub|<text|r>>*\<epsilon\><rsub|0>*R*T|)><rsup|-1/2><eq-number>>>>>
    </align>

    where <math|N<A>> is the Avogadro constant, <math|e> is the elementary
    charge (the charge of a proton), <math|\<rho\><A><rsup|\<ast\>>> and
    <math|\<epsilon\><rsub|<text|r>>> are the density and relative
    permittivity (dielectric constant) of the solvent, and
    <math|\<epsilon\><rsub|0>> is the electric constant (or permittivity of
    vacuum).
  </quote-env>

  When the solvent is water at <math|25 <degC>>, the quantities
  <math|A<rsub|<text|D>\<nocomma\><text|H>>> and
  <math|B<rsub|<text|D>\<nocomma\><text|H>>> have the values

  <\eqnarray*>
    <tformat|<table|<row|<cell|A<rsub|<text|D>\<nocomma\><text|H>>>|<cell|=>|<cell|1.1744
    <text|kg><rsup|<frac*|1|2>>\<cdot\><text|mol><rsup|-<frac*|1|2>><eq-number>>>|<row|<cell|B<rsub|<text|D>\<nocomma\><text|H>>>|<cell|=>|<cell|3.285<timesten|9>
    <text|m><rsup|-1>\<cdot\><text|kg><rsup|<frac*|1|2>>\<cdot\><text|mol><rsup|-<frac*|1|2>><eq-number>>>>>
  </eqnarray*>

  From Eqs. <reference|g(+-)=()^(1/nu)> and <reference|ln(g_i) (DH)> and the
  electroneutrality condition <math|\<nu\><rsub|+>*z<rsub|+>=\<nu\><rsub|->*z<rsub|->>,
  <index-complex|<tuple|activity coefficient|mean ionic|debye
  huckel>|||<tuple|Activity coefficient|mean ionic|from the Debye\UH�ckel
  theory>>we obtain the following expression for the logarithm of the mean
  ionic activity coefficient of an electrolyte solute:
  <index-complex|<tuple|debye huckel|equation|mean
  ionic>|||<tuple|Debye\UH�ckel|equation|for a mean ionic activity
  coefficient>>

  <\equation>
    <label|ln(g+-) (DH)>ln <g><rsub|\<pm\>>=-<frac|A<rsub|<text|D>\<nocomma\><text|H>>*<around*|\||z<rsub|+>*z<rsub|->|\|>*<sqrt|I<rsub|m>>|1+B<rsub|<text|D>\<nocomma\><text|H>>*a*<sqrt|I<rsub|m>>>
  </equation>

  In this equation, <math|z<rsub|+>> and <math|z<rsub|->> are the charge
  numbers of the cation and anion of the solute. Since the right side of Eq.
  <reference|ln(g+-) (DH)> is negative at finite solute molalities, and zero
  at infinite dilution, the theory predicts that <math|<g><rsub|\<pm\>>> is
  less than <math|1> at finite solute molalities and approaches <math|1> at
  infinite dilution.

  Figure <vpageref|fig:10-aq HCl gamma+/-><\float|float|thb>
    <\framed>
      <\big-figure|<image|10-SUP/HCL-GAM.eps|268pt|180pt||>>
        <label|fig:10-aq HCl gamma+/->Mean ionic activity coefficient of
        aqueous HCl at <math|25 <degC>>. Solid curve:
        experiment;<note-ref|+1PualPXI2C0ZUoKL> dashed curve: Debye--H�ckel
        theory with <math|a=5<timesten|-10> <text|m>>; dotted curve:
        Debye--H�ckel limiting law.

        \;

        <note-inline|Ref. <cite|harned-58>, Table 11-5-1.|+1PualPXI2C0ZUoKL>
      </big-figure>
    </framed>
  </float> shows that with the proper choice of the parameter <math|a>, the
  mean ionic activity coefficient of HCl calculated from Eq.
  <reference|ln(g+-) (DH)> (dashed curve) agrees closely with experiment
  (solid curve) at low molalities.

  As the molalities of all solutes become small, Eq. <reference|ln(g+-) (DH)>
  becomes

  <equation-cov2|<label|DH limiting law>ln
  <g><rsub|\<pm\>>=-A<rsub|<text|D>\<nocomma\><text|H>>*<around*|\||z<rsub|+>*z<rsub|->|\|>*<sqrt|I<rsub|m>>|(infinite
  dilution)>

  This form is known as the <subindex|Debye--H�ckel|limiting
  law><newterm|Debye\UH�ckel limiting law>. Note that the limiting law
  contains no adjustable parameters. The dotted curve in Fig.
  <reference|fig:10-aq HCl gamma+/-> shows that the limiting law agrees with
  experiment only at quite low molality.

  <index|Ionic strength>The ionic strength <math|I<rsub|m>> is calculated
  from Eq. <reference|I(m) def> with the molalities of <em|all> ions in the
  solution, not just the molality of the ion or solute whose activity
  coefficient we are interested in. This is because, as explained above, the
  departure of <math|<g><rsub|+>> and <math|<g><rsub|->> from the
  ideal-dilute value of <math|1> is caused by the interaction of each ion
  with the ion atmosphere resulting from all other ions in the solution.

  In a binary solution of a single electrolyte solute, assumed to be
  completely dissociated, the relation between the ionic strength and the
  solute molality depends on <math|\<nu\>> (the number of ions per solute
  formula unit) and the charge numbers <math|z<rsub|+>> and <math|z<rsub|->>.
  The ionic strength is given by <math|I<rsub|m>=<around|(|1/2|)>*<big|sum><rsub|i>m<rsub|i>*z<rsub|i><rsup|2>=<around|(|1/2|)>*<around|(|\<nu\><rsub|+>*z<rsub|+><rsup|2>+\<nu\><rsub|->*z<rsub|-><rsup|2>|)>*m<B>>.
  With the help of the electroneutrality condition
  <math|\<nu\><rsub|+>*z<rsub|+>=-<around|(|\<nu\><rsub|->*z<rsub|->|)>>,
  this becomes

  <\eqnarray*>
    <tformat|<table|<row|<cell|I<rsub|m>>|<cell|=>|<cell|<tfrac|1|2>*<around*|[|-<around*|(|v<rsub|->*z<rsub|->|)>*z<rsub|+>-<around*|(|v<rsub|+>*z<rsub|+>|)>*z<rsub|->|]>*m<B>>>|<row|<cell|>|<cell|=>|<cell|<tfrac|1|2>*<around*|[|-<around*|(|v<rsub|->+v<rsub|+>|)>*z<rsub|+>*z<rsub|->|]>*m<B>>>|<row|<cell|>|<cell|=>|<cell|<tfrac|1|2>*v*<around*|\||z<rsub|+>*z<rsub|->|\|>*m<B><eq-number><label|I(m)=(1/2)nu\|x+z-\|mB>>>>>
  </eqnarray*>

  We find the following relations hold between <math|I<rsub|m>> and
  <math|m<B>> in the binary solution, depending on the stoichiometry of the
  solute formula unit:

  <\itemize>
    <item>For a 1:1 electrolyte, e.g., NaCl or HCl: <math|I<rsub|m>=m<B>>

    <item>For a 1:2 or 2:1 electrolyte, e.g.,
    Na<rsub|<math|2>>SO<rsub|<math|4>> or CaCl<rsub|<math|2>>:
    <math|I<rsub|m>=3*m<B>>

    <item>For a 2:2 electrolyte, e.g., MgSO<rsub|<math|4>>:
    <math|I<rsub|m>=4*m<B>>

    <item>For a 1:3 or 3:1 electrolyte, e.g., AlCl<rsub|<math|3>>:
    <math|I<rsub|m>=6*m<B>>

    <item>For a 3:2 or 2:3 electrolyte, e.g.,
    Al<rsub|<math|2>>(SO<rsub|<math|4>>)<rsub|<math|3>>:
    <math|I<rsub|m>=15*m<B>>
  </itemize>

  Figure <vpageref|fig:10-ln gamma+/-><\float|float|thb>
    <\framed>
      <\big-figure|<image|10-SUP/LN-GAM.eps|219pt|191pt||>>
        <label|fig:10-ln gamma+/->Dependence of <math|ln <g><rsub|\<pm\>>> on
        <math|<sqrt|I<rsub|m>>> for aqueous HCl (upper curves) and aqueous
        CaCl<rsub|<math|2>> (lower curves) at <math|25
        <degC>>.<note-ref|+1PualPXI2C0ZUoKM> Solid curves: experimental;
        dashed curves: Debye\UH�ckel equation (<math|a=5<timesten|-10>
        <text|m>> for HCl, <math|a=4.5<timesten|-10> <text|m>> for
        CaCl<rsub|<math|2>>); dotted lines: Debye\UH�ckel limiting law.

        \;

        <note-inline||+1PualPXI2C0ZUoKM>Experimental curves from parameter
        values in Ref. <cite|harned-58>, Tables 11-5-1 and 12-1-3a.
      </big-figure>
    </framed>
  </float> shows <math|ln <g><rsub|\<pm\>>> as a function of
  <math|<sqrt|I<rsub|m>>> for aqueous HCl and CaCl<rsub|<math|2>>. The
  experimental curves have the limiting slopes predicted by the Debye\UH�ckel
  limiting law (Eq. <reference|DH limiting law>), but at a low ionic strength
  the curves begin to deviate significantly from the linear relations
  predicted by that law. The full <index-complex|<tuple|debye-huckel|equation|mean
  ionic>|||<tuple|Debye\UH�ckel|equation|for a mean ionic activity
  coefficient>>Debye\UH�ckel equation (Eq. <reference|ln(g+-) (DH)>) fits the
  experimental curves over a wider range of ionic strength.

  <section|Derivation of the Debye\UH�ckel Equation>

  <index|Debye, Peter>Debye and <index|H�ckel, Erich>H�ckel derived Eq.
  <reference|ln(g_i) (DH)> using a combination of electrostatic theory,
  <subindex|Statistical mechanics|Debye\UH�ckel theory>statistical mechanical
  theory, and thermodynamics. This section gives a brief outline of their
  derivation.

  The derivation starts by focusing on an individual ion of species <math|i>
  as it moves through the solution; call it the central ion. Around this
  central ion, the time-average spatial distribution of any ion species
  <math|j> is not random, on account of the interaction of these ions of
  species <math|j> with the central ion. (Species <math|i> and <math|j> may
  be the same or different.) The distribution, whatever it is, must be
  spherically symmetric about the central ion; that is, a function only of
  the distance <math|r> from the center of the ion. The local concentration,
  <math|c<rprime|'><rsub|j>>, of the ions of species <math|j> at a given
  value of <math|r> depends on the ion charge <math|z<rsub|j>*e> and the
  <subindex|Electric|potential>electric potential <math|\<phi\>> at that
  position. The time-average electric potential in turn depends on the
  distribution of all ions and is symmetric about the central ion, so
  expressions must be found for <math|c<rprime|'><rsub|j>> and <math|\<phi\>>
  as functions of <math|r> that are mutually consistent.

  <index|Debye, Peter>Debye and <index|H�ckel, Erich>H�ckel assumed that
  <math|c<rprime|'><rsub|j>> is given by the Boltzmann distribution

  <\equation>
    c<rprime|'><rsub|j>=c<rsub|j>*e<rsup|-z<rsub|j>*e*\<phi\>/<around*|(|k*T|)>>
  </equation>

  where <math|z<rsub|j>*e*\<phi\>> is the electrostatic energy of an ion of
  species <math|j>, and <math|k> is the Boltzmann constant (<math|k=R/N<A>>).
  As <math|r> becomes large, <math|\<phi\>> approaches zero and
  <math|c<rprime|'><rsub|j>> approaches the macroscopic concentration
  <math|c<rsub|j>>. As <math|T> increases, <math|c<rprime|'><rsub|j>> at a
  fixed value of <math|r> approaches <math|c<rsub|j>> because of the
  randomizing effect of thermal energy. <index|Debye, Peter>Debye and
  <index|H�ckel, Erich>H�ckel expanded the exponential function in powers of
  <math|1/T> and retained only the first two terms:
  <math|c<rprime|'><rsub|j>\<approx\>c<rsub|j>*<around|(|1-z<rsub|j>*e*\<phi\>/<around*|(|k*T|)>|)>>.
  The distribution of each ion species is assumed to follow this relation.
  The electric potential function consistent with this distribution and with
  the electroneutrality of the solution as a whole is

  <\equation>
    \<phi\>=<around|(|z<rsub|i>*e/<around*|(|4*\<pi\>*\<epsilon\><rsub|<text|r>>*\<epsilon\><rsub|0>*r|)>|)>*e<rsup|\<kappa\>*<around|(|a-r|)>>/<around|(|1+\<kappa\>*a|)>
  </equation>

  Here <math|\<kappa\>> is defined by <math|\<kappa\><rsup|2>=2*N<A><rsup|2>*e<rsup|2>*I<rsub|c>/<around*|(|\<epsilon\><rsub|r>*\<epsilon\><rsub|0>*R*T|)>>,
  where <math|I<rsub|c>> is the <index|Ionic strength><em|ionic strength on a
  concentration basis> defined by <math|I<rsub|c>=<around|(|1/2|)>*<big|sum><rsub|i>c<rsub|i>*z<rsub|i><rsup|2>>.

  The <subindex|Electric|potential>electric potential <math|\<phi\>> at a
  point is assumed to be a sum of two contributions: the electric potential
  the central ion would cause at infinite dilution,
  <math|z<rsub|i>*e/<around*|(|4*\<pi\>*\<epsilon\><rsub|r>*\<epsilon\><rsub|0>*r|)>>,
  and the electric potential due to all other ions, <math|\<phi\><rprime|'>>.
  Thus, <math|\<phi\><rprime|'>> is equal to
  <math|\<phi\>-z<rsub|i>*e/<around*|(|4*\<pi\>*\<epsilon\><rsub|r>*\<epsilon\><rsub|0>*r|)>>,
  or

  <\equation>
    \<phi\><rprime|'>=<around|(|z<rsub|i>*e/<around*|(|4*\<pi\>*\<epsilon\><rsub|<text|r>>*\<epsilon\><rsub|0>*r|)>|)>*<around|[|e<rsup|\<kappa\>*<around|(|a-r|)>>/<around|(|1+\<kappa\>*a|)>-1|]>
  </equation>

  This expression for <math|\<phi\><rprime|'>> is valid for distances from
  the center of the central ion down to <math|a>, the distance of closest
  approach of other ions. At smaller values of <math|r>,
  <math|\<phi\><rprime|'>> is constant and equal to the value at <math|r=a>,
  which is <math|\<phi\><rprime|'><around|(|a|)>=-<around|(|z<rsub|i>*e/<around*|(|4*\<pi\>*\<epsilon\><rsub|r>*\<epsilon\><rsub|0>|)>|)>*\<kappa\>/<around|(|1+\<kappa\>*a|)>>.
  The interaction energy between the central ion and the surrounding ions
  (the ion atmosphere) is the product of the central ion charge and
  <math|\<phi\><rprime|'><around|(|a|)>>.

  The last step of the derivation is the calculation of the work of a
  hypothetical reversible process in which the surrounding ions stay in their
  final distribution, and the charge of the central ion gradually increases
  from zero to its actual value <math|z<rsub|i>*e>. Let
  <math|\<alpha\>*z<rsub|i>*e> be the charge at each stage of the process,
  where <math|\<alpha\>> is a fractional advancement that changes from
  <math|0> to <math|1>. Then the work <math|w<rprime|'>> due to the
  interaction of the central ion with its ion atmosphere is
  <math|\<phi\><rprime|'><around|(|a|)>> integrated over the charge:

  <\eqnarray*>
    <tformat|<table|<row|<cell|w<rprime|'>>|<cell|=>|<cell|-<big|int><rsub|\<alpha\>=0><rsup|\<alpha\>=1><around|[|<around|(|\<alpha\>*z<rsub|i>*e/<around*|(|4*\<pi\>*\<epsilon\><rsub|<text|r>>*\<epsilon\><rsub|0>|)>|)>*\<kappa\>/<around|(|1+\<kappa\>*a|)>|]><dif><around|(|\<alpha\>*z<rsub|i>*\<epsilon\>|)>>>|<row|<cell|>|<cell|=>|<cell|-<around|(|z<rsub|i><rsup|2>*e<rsup|2>/<around*|(|8*\<pi\>*\<epsilon\><rsub|<text|r>>*\<epsilon\><rsub|0>|)>|)>*\<kappa\>/<around|(|1+\<kappa\>*a|)><eq-number>>>>>
  </eqnarray*>

  Since the infinitesimal Gibbs energy change in a reversible process is
  given by <math|<dif>G=-S<dif>T+V<difp>+<dw><rprime|'>> (Eq.
  <reference|dG\<less\>=-SdT+Vdp+dw'>), this reversible nonexpansion work at
  constant <math|T> and <math|p> is equal to the Gibbs energy change. The
  Gibbs energy change per amount of species <math|i> is
  <math|w<rprime|'>*N<A>=-<around|(|z<rsub|i><rsup|2>*e<rsup|2>*N<A>/<around*|(|8*\<pi\>*\<epsilon\><rsub|<text|r>>*\<epsilon\><rsub|0>|)>|)>*\<kappa\>/<around|(|1+\<kappa\>*a|)>>.
  This quantity is <math|<Del>G/n<rsub|i>> for the process in which a
  solution of fixed composition changes from a hypothetical state lacking
  ion\Uion interactions to the real state with ion\Uion interactions present.
  <math|<Del>G/n<rsub|i>> may be equated to the difference of the chemical
  potentials of <math|i> in the final and initial states. If the chemical
  potential without ion\Uion interactions is taken to be that for
  ideal-dilute behavior on a molality basis,
  <math|\<mu\><rsub|i>=\<mu\><rsub|m,i><rf>+R*T*ln
  <around|(|m<rsub|i>/m<st>|)>>, then <math|-<around|(|z<rsub|i><rsup|2>*e<rsup|2>*N<A>/<around*|(|8*\<pi\>*\<epsilon\><rsub|<text|r>>*\<epsilon\><rsub|0>|)>|)>*\<kappa\>/<around|(|1+\<kappa\>*a|)>>
  is equal to <math|\<mu\><rsub|i>-<around|[|\<mu\><rsub|m,i><rf>+R*T*ln
  <around|(|m<rsub|i>/m<st>|)>|]>=R*T*ln <g><rsub|m,i>>. In a dilute
  solution, <math|c<rsub|i>> can with little error be set equal to
  <math|\<rho\><A><rsup|\<ast\>>*m<rsub|i>>, and <math|I<rsub|c>> to
  <math|\<rho\><A><rsup|\<ast\>>*I<rsub|m>>. Equation <reference|ln(g_i)
  (DH)> follows.<index-complex|<tuple|debye-huckel|theory>||c10 sec dhe
  idx1|<tuple|Debye\UH�ckel|theory>>

  <section|Mean Ionic Activity Coefficients from Osmotic
  Coefficients><label|10-ionic act coeffs from osmotic coeffs><label|c10 sec
  miacoc>

  <index-complex|<tuple|activity coefficient|mean ionic|osmotic
  coefficient>|||<tuple|Activity coefficient|mean ionic|from the osmotic
  coefficient>><index-complex|<tuple|osmotic coefficient|evaluation|mean
  ionic>|||<tuple|Osmotic coefficient|evaluation|of a mean ionic activity
  coefficient>>Recall that <math|<g><rsub|\<pm\>>> is the mean ionic activity
  coefficient of a strong electrolyte, or the stoichiometric activity
  coefficient of an electrolyte that does not dissociate completely.

  The general procedure described in this section for evaluating
  <math|<g><rsub|\<pm\>>> requires knowledge of the osmotic coefficient
  <math|\<phi\><rsub|m>> as a function of molality. <math|\<phi\><rsub|m>> is
  commonly evaluated by the isopiestic method (Sec. <reference|9-fugacity
  measurements>) or from measurements of freezing-point depression (Sec.
  <reference|12-solvent mu from phase eq>).

  The osmotic coefficient of a binary solution of an electrolyte is defined
  by

  <equation-cov2|<label|phi(m) electrolyte>\<phi\><rsub|m><defn><frac|\<mu\><A><rsup|\<ast\>>-\<mu\><A>|R*T*M<A>*\<nu\>*m<B>>|(binary
  electrolyte solution)>

  That is, for an electrolyte the sum <math|<big|sum><rsub|i\<neq\><text|A>>m<rsub|i>>
  appearing in the definition of <math|\<phi\><rsub|m>> for a nonelectrolyte
  solution (Eq. <vpageref|phi(m) def>) is replaced by <math|\<nu\>*m<B>>, the
  sum of the ion molalities assuming complete dissociation. It will now be
  shown that <math|\<phi\><rsub|m>> defined this way can be used to evaluate
  <math|<g><rsub|\<pm\>>>.

  The derivation is like that described in Sec. <reference|9-act coeffs from
  osmotic coeffs> for a binary solution of a nonelectrolyte. Solving Eq.
  <reference|phi(m) electrolyte> for <math|\<mu\><A>> and taking the
  differential of <math|\<mu\><A>> at constant <math|T> and <math|p>, we
  obtain

  <\equation>
    <dif>\<mu\><A>=-R*T*M<A>*\<nu\>*<around|(|\<phi\><rsub|m>*<dif>m<B>+m<B>*<dif>\<phi\><rsub|m>|)>
  </equation>

  From Eq. <vpageref|muB, gen. electrolyte>, we obtain

  <\equation>
    <dif>\<mu\><B>=R*T*\<nu\>*<around*|(|<dif>ln
    <g><rsub|\<pm\>>+<frac|<dif>m<B>|m<B>>|)>
  </equation>

  Substitution of these expressions in the <index|Gibbs--Duhem
  equation>Gibbs\UDuhem equation <math|n<A><dif>\<mu\><A>+n<B><dif>\<mu\><B>=0>,
  together with the substitution <math|n<A>M<A>=n<B>/m<B>>, yields

  <\equation>
    <label|dln(g+-)=dphi(m)+...><dif>ln <g><rsub|\<pm\>>=<dif>\<phi\><rsub|m>+<frac|\<phi\><rsub|m>-1|m<B>><dif>m<B>
  </equation>

  Then integration from <math|m<B>=0> to any desired molality
  <math|m<rprime|'><B>> gives the result

  <\equation>
    <label|ln(g+-)=phi(m)-1+...>ln <g><rsub|\<pm\>><around|(|m<rprime|'><B>|)>=\<phi\><rsub|m><around|(|m<rprime|'><B>|)>-1+<big|int><rsub|0><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>
  </equation>

  The right side of this equation is the same expression as derived for
  <math|ln <g><mbB>> for a nonelectrolyte (Eq. <vpageref|ln gamma(mB)=>).

  The integrand of the integral on the right side of Eq.
  <reference|ln(g+-)=phi(m)-1+...> approaches <math|-\<infty\>> as
  <math|m<B>> approaches zero, making it difficult to evaluate the integral
  by numerical integration starting at <math|m<B>=0>. (This difficulty does
  not exist when the solute is a nonelectrolyte.) Instead, we can split the
  integral into two parts

  <\equation>
    <label|int_0^m'B=><big|int><rsub|0><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>=<big|int><rsub|0><rsup|m<rprime|''><B>><frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>+<big|int><rsub|m<rprime|''><B>><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>
  </equation>

  where the integration limit <math|m<rprime|''><B>> is a low molality at
  which the value of <math|\<phi\><rsub|m>> is available and at which
  <math|<g><rsub|\<pm\>>> can either be measured or estimated from the
  Debye\UH�ckel equation.

  We next rewrite Eq. <reference|ln(g+-)=phi(m)-1+...> with
  <math|m<B><rprime|'>> replaced with <math|m<B><rprime|''>>:

  <\equation>
    <label|ln gamma+-(m''B)=>ln <g><rsub|\<pm\>><around|(|m<rprime|''><B>|)>=\<phi\><rsub|m><around|(|m<rprime|''><B>|)>-1+<big|int><rsub|0><rsup|m<rprime|''><B>><frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>
  </equation>

  By eliminating the integral with an upper limit of <math|m<B><rprime|''>>
  from Eqs. <reference|int_0^m'B=> and <reference|ln gamma+-(m''B)=>, we
  obtain

  <\equation>
    <big|int><rsub|0><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>=ln
    <g><rsub|\<pm\>><around|(|m<rprime|''><B>|)>-\<phi\><rsub|m><around|(|m<rprime|''><B>|)>+1+<big|int><rsub|m<rprime|''><B>><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>
  </equation>

  Equation <reference|ln(g+-)=phi(m)-1+...> becomes

  <\equation>
    <label|ln(g+-)=...>ln <g><rsub|\<pm\>><around|(|m<rprime|'><B>|)>=\<phi\><rsub|m><around|(|m<rprime|'><B>|)>-\<phi\><rsub|m><around|(|m<rprime|''><B>|)>+ln
    <g><rsub|\<pm\>><around|(|m<rprime|''><B>|)>+<big|int><rsub|m<rprime|''><B>><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>>*<dif>m<B>
  </equation>

  The integral on the right side of this equation can easily be evaluated by
  numerical integration.<index-complex|<tuple|electrolyte|solution>||c10
  idx1|<tuple|Electrolyte|solution>>

  \;

  <\bio-insert>
    <include|bio-DEBYE.tm>
  </bio-insert>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|9>
    <associate|page-first|236>
    <associate|preamble|false>
    <associate|section-nr|9>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|10-Debye Huckel|<tuple|10.4|?>>
    <associate|10-el in gen|<tuple|10.3|?>>
    <associate|10-electrolyte solutions|<tuple|10|?>>
    <associate|10-ionic act coeffs from osmotic coeffs|<tuple|10.6|?>>
    <associate|10-single ion quantities|<tuple|10.1|?>>
    <associate|A(DH) defn|<tuple|<with|mode|<quote|math>|\<bullet\>>|?>>
    <associate|Chap. 10|<tuple|10|?>>
    <associate|DH limiting law|<tuple|10.4.8|?>>
    <associate|G(common ion)|<tuple|10.3.14|?>>
    <associate|G=...|<tuple|10.3.13|?>>
    <associate|Gamma(+)=|<tuple|10.1.12|?>>
    <associate|Gamma(-)=|<tuple|10.1.13|?>>
    <associate|Gamma(m,B) defn|<tuple|10.2.11|?>>
    <associate|Gamma(m,B)=|<tuple|10.2.12|?>>
    <associate|Gamma(m,B)=Gamma+^(nu+)Gamma-^(nu-)|<tuple|10.3.11|?>>
    <associate|I(m) def|<tuple|10.4.2|?>>
    <associate|I(m)=(1/2)nu\|x+z-\|mB|<tuple|10.4.9|?>>
    <associate|a(+)=...|<tuple|10.1.14|?>>
    <associate|a(B)(multisolute)|<tuple|10.3.16|?>>
    <associate|a(mB),general|<tuple|10.3.10|?>>
    <associate|a(mB),nu=2|<tuple|10.2.10|?>>
    <associate|auto-1|<tuple|10|?>>
    <associate|auto-10|<tuple|Fundamental equations, Gibbs|?>>
    <associate|auto-11|<tuple|Gibbs|?>>
    <associate|auto-12|<tuple|<tuple|standard state|ion>|?>>
    <associate|auto-13|<tuple|<tuple|chemical potential|standard|ion>|?>>
    <associate|auto-14|<tuple|Electrochemical|?>>
    <associate|auto-15|<tuple|<tuple|reference state|ion>|?>>
    <associate|auto-16|<tuple|<tuple|activity coefficient|ion>|?>>
    <associate|auto-17|<tuple|<tuple|pressure factor|ion>|?>>
    <associate|auto-18|<tuple|<tuple|activity|ion>|?>>
    <associate|auto-19|<tuple|10.2|?>>
    <associate|auto-2|<tuple|<tuple|electrolyte|solution>|?>>
    <associate|auto-20|<tuple|<tuple|electrolyte|symmetrical>|?>>
    <associate|auto-21|<tuple|Additivity rule|?>>
    <associate|auto-22|<tuple|<tuple|chemical potential|symmetrical
    electrolyte>|?>>
    <associate|auto-23|<tuple|<tuple|mean ionic activity coefficient|activity
    coefficient>|?>>
    <associate|auto-24|<tuple|<tuple|activity coefficient|mean
    ionic|symmetrical electrolyte>|?>>
    <associate|auto-25|<tuple|mean ionic activity coefficient|?>>
    <associate|auto-26|<tuple|<tuple|activity|symmetrical electrolyte>|?>>
    <associate|auto-27|<tuple|<tuple|pressure factor|symmetrical
    electrolyte>|?>>
    <associate|auto-28|<tuple|10.2.1|?>>
    <associate|auto-29|<tuple|<tuple|electrolyte|symmetrical>|?>>
    <associate|auto-3|<tuple|1|?>>
    <associate|auto-30|<tuple|10.3|?>>
    <associate|auto-31|<tuple|10.3.1|?>>
    <associate|auto-32|<tuple|Additivity rule|?>>
    <associate|auto-33|<tuple|<tuple|chemical potential|electrolyte
    solute>|?>>
    <associate|auto-34|<tuple|<tuple|activity coefficient|mean
    ionic|electrolyte solute>|?>>
    <associate|auto-35|<tuple|<tuple|activity|electrolyte solute>|?>>
    <associate|auto-36|<tuple|<tuple|pressure factor|electrolyte solute>|?>>
    <associate|auto-37|<tuple|10.3.2|?>>
    <associate|auto-38|<tuple|Solution|?>>
    <associate|auto-39|<tuple|Additivity rule|?>>
    <associate|auto-4|<tuple|Henry's law|?>>
    <associate|auto-40|<tuple|10.3.3|?>>
    <associate|auto-41|<tuple|Stoichiometric|?>>
    <associate|auto-42|<tuple|Activity coefficient|?>>
    <associate|auto-43|<tuple|stoichiometric activity coefficient|?>>
    <associate|auto-44|<tuple|10.4|?>>
    <associate|auto-45|<tuple|<tuple|debye-huckel|theory>|?>>
    <associate|auto-46|<tuple|Debye, Peter|?>>
    <associate|auto-47|<tuple|H�ckel, Erich|?>>
    <associate|auto-48|<tuple|<tuple|activity coefficient|ion|debye
    huckel>|?>>
    <associate|auto-49|<tuple|<tuple|debye huckel|equation|single ion>|?>>
    <associate|auto-5|<tuple|10.1|?>>
    <associate|auto-50|<tuple|Charge|?>>
    <associate|auto-51|<tuple|Ionic strength|?>>
    <associate|auto-52|<tuple|ionic strength|?>>
    <associate|auto-53|<tuple|Lewis, Gilbert Newton|?>>
    <associate|auto-54|<tuple|Randall, Merle|?>>
    <associate|auto-55|<tuple|<tuple|activity coefficient|mean ionic|debye
    huckel>|?>>
    <associate|auto-56|<tuple|<tuple|debye huckel|equation|mean ionic>|?>>
    <associate|auto-57|<tuple|10.4.1|?>>
    <associate|auto-58|<tuple|Debye--H�ckel|?>>
    <associate|auto-59|<tuple|Debye\UH�ckel limiting law|?>>
    <associate|auto-6|<tuple|Electric|?>>
    <associate|auto-60|<tuple|Ionic strength|?>>
    <associate|auto-61|<tuple|10.4.2|?>>
    <associate|auto-62|<tuple|<tuple|debye-huckel|equation|mean ionic>|?>>
    <associate|auto-63|<tuple|10.5|?>>
    <associate|auto-64|<tuple|Debye, Peter|?>>
    <associate|auto-65|<tuple|H�ckel, Erich|?>>
    <associate|auto-66|<tuple|Statistical mechanics|?>>
    <associate|auto-67|<tuple|Electric|?>>
    <associate|auto-68|<tuple|Debye, Peter|?>>
    <associate|auto-69|<tuple|H�ckel, Erich|?>>
    <associate|auto-7|<tuple|Electric|?>>
    <associate|auto-70|<tuple|Debye, Peter|?>>
    <associate|auto-71|<tuple|H�ckel, Erich|?>>
    <associate|auto-72|<tuple|Ionic strength|?>>
    <associate|auto-73|<tuple|Electric|?>>
    <associate|auto-74|<tuple|<tuple|debye-huckel|theory>|?>>
    <associate|auto-75|<tuple|10.6|?>>
    <associate|auto-76|<tuple|<tuple|activity coefficient|mean ionic|osmotic
    coefficient>|?>>
    <associate|auto-77|<tuple|<tuple|osmotic coefficient|evaluation|mean
    ionic>|?>>
    <associate|auto-78|<tuple|Gibbs--Duhem equation|?>>
    <associate|auto-79|<tuple|<tuple|electrolyte|solution>|?>>
    <associate|auto-8|<tuple|Galvani potential|?>>
    <associate|auto-80|<tuple|1|?|bio-DEBYE.tm>>
    <associate|auto-81|<tuple|Debye, Peter|?|bio-DEBYE.tm>>
    <associate|auto-9|<tuple|Faraday constant|?>>
    <associate|bio:debye|<tuple|1|?|bio-DEBYE.tm>>
    <associate|c10|<tuple|10|?>>
    <associate|c10 sec dhe|<tuple|10.4|?>>
    <associate|c10 sec eig|<tuple|10.3|?>>
    <associate|c10 sec eig-multi|<tuple|10.3.2|?>>
    <associate|c10 sec eig-single|<tuple|10.3.1|?>>
    <associate|c10 sec miacoc|<tuple|10.6|?>>
    <associate|c10 sec siq|<tuple|10.1|?>>
    <associate|c10 sec sse|<tuple|10.2|?>>
    <associate|c10 sec-eig-incomplete-dissociation|<tuple|10.3.3|?>>
    <associate|dG(phi)=dG(0)+.|<tuple|10.1.2|?>>
    <associate|dln(g+-)=dphi(m)+...|<tuple|10.6.4|?>>
    <associate|electrolyte act coeff vs molality|<tuple|c|?>>
    <associate|fig:10-HCl gas/soln|<tuple|1|?>>
    <associate|fig:10-aq HCl activity|<tuple|10.2.1|?>>
    <associate|fig:10-aq HCl gamma+/-|<tuple|10.4.1|?>>
    <associate|fig:10-ln gamma+/-|<tuple|10.4.2|?>>
    <associate|footnote-10.1.1|<tuple|10.1.1|?>>
    <associate|footnote-10.1.2|<tuple|10.1.2|?>>
    <associate|footnote-10.2.1|<tuple|10.2.1|?>>
    <associate|footnote-10.4.1|<tuple|10.4.1|?>>
    <associate|footnote-10.4.2|<tuple|10.4.2|?>>
    <associate|footnote-10.4.3|<tuple|10.4.3|?>>
    <associate|footnote-10.6.1|<tuple|10.6.1|?|bio-DEBYE.tm>>
    <associate|footnote-10.6.2|<tuple|10.6.2|?|bio-DEBYE.tm>>
    <associate|footnote-10.6.3|<tuple|10.6.3|?|bio-DEBYE.tm>>
    <associate|footnr-10.1.1|<tuple|10.1.1|?>>
    <associate|footnr-10.1.2|<tuple|Electrochemical|?>>
    <associate|footnr-10.2.1|<tuple|10.2.1|?>>
    <associate|footnr-10.4.1|<tuple|Randall, Merle|?>>
    <associate|footnr-10.4.2|<tuple|10.4.1|?>>
    <associate|footnr-10.4.3|<tuple|10.4.2|?>>
    <associate|footnr-10.6.1|<tuple|10.6.1|?|bio-DEBYE.tm>>
    <associate|footnr-10.6.2|<tuple|10.6.2|?|bio-DEBYE.tm>>
    <associate|footnr-10.6.3|<tuple|10.6.3|?|bio-DEBYE.tm>>
    <associate|g(+-)=()^(1/nu)|<tuple|10.3.8|?>>
    <associate|g(+-)=sqrt((g+)(g-))|<tuple|10.2.7|?>>
    <associate|g(+-)^nu=...|<tuple|10.3.7|?>>
    <associate|gamma(+)-\<gtr\>1|<tuple|10.1.11|?>>
    <associate|int_0^m'B=|<tuple|10.6.6|?>>
    <associate|ln gamma+-(m''B)=|<tuple|10.6.7|?>>
    <associate|ln(g+-) (DH)|<tuple|10.4.7|?>>
    <associate|ln(g+-)=...|<tuple|10.6.9|?>>
    <associate|ln(g+-)=phi(m)-1+...|<tuple|10.6.5|?>>
    <associate|ln(g_i) (DH)|<tuple|10.4.1|?>>
    <associate|m+=nu+mB, m-=nu-mB|<tuple|10.3.1|?>>
    <associate|mu(+)=mu(ref)+..|<tuple|10.1.10|?>>
    <associate|mu(+)=muo(+)+..|<tuple|10.1.7|?>>
    <associate|mu+(phi)=|<tuple|10.1.15|?>>
    <associate|mu+(phi)=mu+(0)+z+Fphi|<tuple|10.1.4|?>>
    <associate|mu-(phi)=|<tuple|10.1.16|?>>
    <associate|mu-(phi)=mu-(0)+z-Fphi|<tuple|10.1.5|?>>
    <associate|muB(electrolyte)=|<tuple|10.3.5|?>>
    <associate|muB, gen. electrolyte|<tuple|10.3.9|?>>
    <associate|muB, general electrolyte|<tuple|10.3.6|?>>
    <associate|muB=mu(+)+mu(-)|<tuple|10.2.4|?>>
    <associate|muB=muB(ref)+RTln(g(+)g(-)..|<tuple|10.2.6|?>>
    <associate|muB=muB(ref)+RTln(g(+-)..|<tuple|10.2.8|?>>
    <associate|muB=muB^o+RTln(a)|<tuple|10.2.9|?>>
    <associate|muB=nu+mu+ + nu-mu-|<tuple|10.3.3|?>>
    <associate|muB=nu+mu+ + nu-mu-(again)|<tuple|10.3.12|?>>
    <associate|mu_i(a_i,phi)=|<tuple|10.1.9|?>>
    <associate|mu_i(phi)=mu_i(0)+z+Fphi|<tuple|10.1.6|?>>
    <associate|part:bio-DEBYE.tm|<tuple|<tuple|electrolyte|solution>|?>>
    <associate|phi(m) electrolyte|<tuple|10.6.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      bates-19

      harned-58

      lewis-21

      harned-58

      harned-58

      james-93

      debye-62

      vanginkel-06
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Partial pressure of HCl in a gas phase equilibrated with aqueous HCl
        at <with|mode|<quote|math>|25 <rsup|\<circ\>><with|mode|<quote|text>|C>>
        and <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>. Open circles: experimental data from
        Ref. [<write|bib|bates-19><reference|bib-bates-19>].
      </surround>|<pageref|auto-3>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|10.2.1>|>
        Aqueous HCl at <with|mode|<quote|math>|25
        <rsup|\<circ\>><with|mode|<quote|text>|C>> and
        <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.<space|0spc><assign|footnote-nr|1><hidden-binding|<tuple>|10.2.1><assign|fnote-+1PualPXI2C0ZUoKK|<quote|10.2.1>><assign|fnlab-+1PualPXI2C0ZUoKK|<quote|10.2.1>><rsup|<with|font-shape|<quote|right>|<reference|footnote-10.2.1>>>

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>HCl
                activity on a molality basis as a function of molality
                squared. The dashed line is the extrapolation of the
                ideal-dilute behavior.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Same
                as (a) at a greatly reduced scale; the filled circle
                indicates the solute reference state.

                <assign|item-nr|3><hidden-binding|<tuple>|c><assign|last-item-nr|3><assign|last-item|c><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|c<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Mean
                ionic activity coefficient of HCl as a function of molality.
              </surround>
            </with>
          </surround>
        </with>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7FFD034B8--7FE3D06A8>|<link|hyperlink|<id|%-7FFD034B8--7FE3D06A8>|<url|#footnr-10.2.1>>|10.2.1>.
        |<hidden-binding|<tuple|footnote-10.2.1>|10.2.1>|>>>Curves based on
        experimental parameter values in Ref.
        [<write|bib|harned-58><reference|bib-harned-58>], Table 11-5-1.
      </surround>|<pageref|auto-28>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|10.4.1>|>
        Mean ionic activity coefficient of aqueous HCl at
        <with|mode|<quote|math>|25 <rsup|\<circ\>><with|mode|<quote|text>|C>>.
        Solid curve: experiment;<space|0spc><assign|footnote-nr|2><hidden-binding|<tuple>|10.4.2><assign|fnote-+1PualPXI2C0ZUoKL|<quote|10.4.2>><assign|fnlab-+1PualPXI2C0ZUoKL|<quote|10.4.2>><rsup|<with|font-shape|<quote|right>|<reference|footnote-10.4.2>>>
        dashed curve: Debye--H�ckel theory with
        <with|mode|<quote|math>|a=5\<times\>10<rsup|-10>
        <with|mode|<quote|text>|m>>; dotted curve: Debye--H�ckel limiting
        law.

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7FFD034B8-7E4BD15A8>|<link|hyperlink|<id|%-7FFD034B8-7E4BD15A8>|<url|#footnr-10.4.2>>|10.4.2>.
        |<hidden-binding|<tuple|footnote-10.4.2>|10.4.2>|Ref.
        [<write|bib|harned-58><reference|bib-harned-58>], Table 11-5-1.>>>
      </surround>|<pageref|auto-57>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|10.4.2>|>
        Dependence of <with|mode|<quote|math>|ln \<gamma\><rsub|\<pm\>>> on
        <with|mode|<quote|math>|<sqrt|I<rsub|m>>> for aqueous HCl (upper
        curves) and aqueous CaCl<rsub|<with|mode|<quote|math>|2>> (lower
        curves) at <with|mode|<quote|math>|25
        <rsup|\<circ\>><with|mode|<quote|text>|C>>.<space|0spc><assign|footnote-nr|3><hidden-binding|<tuple>|10.4.3><assign|fnote-+1PualPXI2C0ZUoKM|<quote|10.4.3>><assign|fnlab-+1PualPXI2C0ZUoKM|<quote|10.4.3>><rsup|<with|font-shape|<quote|right>|<reference|footnote-10.4.3>>>
        Solid curves: experimental; dashed curves: Debye\UH�ckel equation
        (<with|mode|<quote|math>|a=5\<times\>10<rsup|-10>
        <with|mode|<quote|text>|m>> for HCl,
        <with|mode|<quote|math>|a=4.5\<times\>10<rsup|-10>
        <with|mode|<quote|text>|m>> for CaCl<rsub|<with|mode|<quote|math>|2>>);
        dotted lines: Debye\UH�ckel limiting law.

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7FFD034B8-7F869BB00>|<link|hyperlink|<id|%-7FFD034B8-7F869BB00>|<url|#footnr-10.4.3>>|10.4.3>.
        |<hidden-binding|<tuple|footnote-10.4.3>|10.4.3>|>>>Experimental
        curves from parameter values in Ref.
        [<write|bib|harned-58><reference|bib-harned-58>], Tables 11-5-1 and
        12-1-3a.
      </surround>|<pageref|auto-61>>
    </associate>
    <\associate|gly>
      <tuple|normal|mean ionic activity coefficient|<pageref|auto-25>>

      <tuple|normal|stoichiometric activity coefficient|<pageref|auto-43>>

      <tuple|normal|ionic strength|<pageref|auto-52>>

      <tuple|normal|Debye\UH�ckel limiting law|<pageref|auto-59>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|electrolyte|solution>||c10
      idx1|<tuple|Electrolyte|solution>|<pageref|auto-2>>

      <tuple|<tuple|Henry's law|not obeyed by electrolyte
      solute>|<pageref|auto-4>>

      <tuple|<tuple|Electric|potential>|<pageref|auto-6>>

      <tuple|<tuple|Electric|potential|inner>|<pageref|auto-7>>

      <tuple|<tuple|Galvani potential>|<pageref|auto-8>>

      <tuple|<tuple|Faraday constant>|<pageref|auto-9>>

      <tuple|<tuple|Fundamental equations, Gibbs>|<pageref|auto-10>>

      <tuple|<tuple|Gibbs|fundamental equations>|<pageref|auto-11>>

      <tuple|<tuple|standard state|ion>|||<tuple|Standard state|of an
      ion>|<pageref|auto-12>>

      <tuple|<tuple|chemical potential|standard|ion>|||<tuple|Chemical
      potential|standard|of an ion>|<pageref|auto-13>>

      <tuple|<tuple|Electrochemical|potential>|<pageref|auto-14>>

      <tuple|<tuple|reference state|ion>|||<tuple|Reference state|of an
      ion>|<pageref|auto-15>>

      <tuple|<tuple|activity coefficient|ion>|||<tuple|Activity
      coefficient|of an ion>|<pageref|auto-16>>

      <tuple|<tuple|pressure factor|ion>|||<tuple|Pressure factor|of an
      ion>|<pageref|auto-17>>

      <tuple|<tuple|activity|ion>|||<tuple|Activity|of an
      ion>|<pageref|auto-18>>

      <tuple|<tuple|electrolyte|symmetrical>||c10 sec sse
      idx1|<tuple|Electrolyte|symmetrical>|<pageref|auto-20>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-21>>

      <tuple|<tuple|chemical potential|symmetrical
      electrolyte>|||<tuple|Chemical potential|of a symmetrical
      electrolyte>|<pageref|auto-22>>

      <tuple|<tuple|mean ionic activity coefficient|activity
      coefficient>|||<tuple|Mean ionic activity coefficient|see Activity
      coefficient, mean ionic>|<pageref|auto-23>>

      <tuple|<tuple|activity coefficient|mean ionic|symmetrical
      electrolyte>|||<tuple|Activity coefficient|mean ionic|of a symmetrical
      electrolyte>|<pageref|auto-24>>

      <tuple|<tuple|activity|symmetrical electrolyte>|||<tuple|Activity|of a
      symmetrical electrolyte>|<pageref|auto-26>>

      <tuple|<tuple|pressure factor|symmetrical
      electrolyte>|||<tuple|Pressure factor|of a symmetrical
      electrolyte>|<pageref|auto-27>>

      <tuple|<tuple|electrolyte|symmetrical>||c10 sec sse
      idx1|<tuple|Electrolyte|symmetrical>|<pageref|auto-29>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-32>>

      <tuple|<tuple|chemical potential|electrolyte solute>|||<tuple|Chemical
      potential|of<space|1em>an electrolyte solute>|<pageref|auto-33>>

      <tuple|<tuple|activity coefficient|mean ionic|electrolyte
      solute>|||<tuple|Activity coefficient|mean ionic|of an electrolyte
      solute>|<pageref|auto-34>>

      <tuple|<tuple|activity|electrolyte solute>|||<tuple|Activity|of an
      electrolyte solute>|<pageref|auto-35>>

      <tuple|<tuple|pressure factor|electrolyte solute>|||<tuple|Pressure
      factor|of an electrolyte solute>|<pageref|auto-36>>

      <tuple|<tuple|Solution|multisolute electrolyte>|<pageref|auto-38>>

      <tuple|<tuple|Additivity rule>|<pageref|auto-39>>

      <tuple|<tuple|Stoichiometric|activity coefficient>|<pageref|auto-41>>

      <tuple|<tuple|Activity coefficient|stoichiometric>|<pageref|auto-42>>

      <tuple|<tuple|debye-huckel|theory>||c10 sec dhe
      idx1|<tuple|Debye\UH�ckel|theory>|<pageref|auto-45>>

      <tuple|<tuple|Debye, Peter>|<pageref|auto-46>>

      <tuple|<tuple|H�ckel, Erich>|<pageref|auto-47>>

      <tuple|<tuple|activity coefficient|ion|debye huckel>|||<tuple|Activity
      coefficient|of an ion|from the Debye\UH�ckel theory>|<pageref|auto-48>>

      <tuple|<tuple|debye huckel|equation|single
      ion>|||<tuple|Debye\UH�ckel|equation|for a single-ion activity
      coefficient>|<pageref|auto-49>>

      <tuple|<tuple|Charge|number>|<pageref|auto-50>>

      <tuple|<tuple|Ionic strength>|<pageref|auto-51>>

      <tuple|<tuple|Lewis, Gilbert Newton>|<pageref|auto-53>>

      <tuple|<tuple|Randall, Merle>|<pageref|auto-54>>

      <tuple|<tuple|activity coefficient|mean ionic|debye
      huckel>|||<tuple|Activity coefficient|mean ionic|from the Debye\UH�ckel
      theory>|<pageref|auto-55>>

      <tuple|<tuple|debye huckel|equation|mean
      ionic>|||<tuple|Debye\UH�ckel|equation|for a mean ionic activity
      coefficient>|<pageref|auto-56>>

      <tuple|<tuple|Debye--H�ckel|limiting law>|<pageref|auto-58>>

      <tuple|<tuple|Ionic strength>|<pageref|auto-60>>

      <tuple|<tuple|debye-huckel|equation|mean
      ionic>|||<tuple|Debye\UH�ckel|equation|for a mean ionic activity
      coefficient>|<pageref|auto-62>>

      <tuple|<tuple|Debye, Peter>|<pageref|auto-64>>

      <tuple|<tuple|H�ckel, Erich>|<pageref|auto-65>>

      <tuple|<tuple|Statistical mechanics|Debye\UH�ckel
      theory>|<pageref|auto-66>>

      <tuple|<tuple|Electric|potential>|<pageref|auto-67>>

      <tuple|<tuple|Debye, Peter>|<pageref|auto-68>>

      <tuple|<tuple|H�ckel, Erich>|<pageref|auto-69>>

      <tuple|<tuple|Debye, Peter>|<pageref|auto-70>>

      <tuple|<tuple|H�ckel, Erich>|<pageref|auto-71>>

      <tuple|<tuple|Ionic strength>|<pageref|auto-72>>

      <tuple|<tuple|Electric|potential>|<pageref|auto-73>>

      <tuple|<tuple|debye-huckel|theory>||c10 sec dhe
      idx1|<tuple|Debye\UH�ckel|theory>|<pageref|auto-74>>

      <tuple|<tuple|activity coefficient|mean ionic|osmotic
      coefficient>|||<tuple|Activity coefficient|mean ionic|from the osmotic
      coefficient>|<pageref|auto-76>>

      <tuple|<tuple|osmotic coefficient|evaluation|mean
      ionic>|||<tuple|Osmotic coefficient|evaluation|of a mean ionic activity
      coefficient>|<pageref|auto-77>>

      <tuple|<tuple|Gibbs--Duhem equation>|<pageref|auto-78>>

      <tuple|<tuple|electrolyte|solution>||c10
      idx1|<tuple|Electrolyte|solution>|<pageref|auto-79>>

      <tuple|<tuple|Debye, Peter>|<pageref|auto-81>>
    </associate>
    <\associate|parts>
      <tuple|bio-DEBYE.tm|chapter-nr|10|section-nr|6|subsection-nr|0>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|10<space|2spc>Electrolyte
      Solutions> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      10.1<space|2spc>Single-ion Quantities
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>

      10.2<space|2spc>Solution of a Symmetrical Electrolyte
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-19>

      10.3<space|2spc>Electrolytes in General
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-30>

      <with|par-left|<quote|1tab>|10.3.1<space|2spc>Solution of a single
      electrolyte <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-31>>

      <with|par-left|<quote|1tab>|10.3.2<space|2spc>Multisolute solution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-37>>

      <with|par-left|<quote|1tab>|10.3.3<space|2spc>Incomplete dissociation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-40>>

      10.4<space|2spc>The Debye-H�ckel Equation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-44>

      10.5<space|2spc>Derivation of the Debye\UH�ckel Equation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-63>

      10.6<space|2spc>Mean Ionic Activity Coefficients from Osmotic
      Coefficients <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-75>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Peter
      Josephus Wilhelmus Debye> (1884\U1966)
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-80><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>