<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|number-long-article|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;

    \;
  </hide-preamble>

  <chapter|The First Law><glossary-line|<strong|Chapter 3>><label|Chap.
  3><label|c3>

  In science, a <subindex|Law|scientific><newterm-explain|law|mathematical
  relation> is a statement or mathematical relation that concisely describes
  reproducible experimental observations. Classical thermodynamics is built
  on a foundation of three laws, none of which can be derived from principles
  that are any more fundamental. This chapter discusses theoretical aspects
  of the first law; gives examples of <em|reversible> and <em|irreversible>
  processes and the heat and work that occur in them; and introduces the
  extensive state function <em|heat capacity>.

  <section|Heat, Work, and the First Law><label|3-heat, work, first
  law><label|c3 sec hwfl>

  The box below gives two forms of the <index|First law of
  thermodynamics><newterm|first law of thermodynamics|first law of
  thermodynamics>.

  \;

  <\framed>
    <label|first law>In a closed system:

    <\equation*>
      d*U=<text|�>*q+<text|�>*w<space|2em>\<Delta\>*U=q+w
    </equation*>

    where

    <\enumerate-alpha>
      <item><math|U> is the internal energy of the system, a state function;

      <item><math|q> is heat; and

      <item><math|w> is thermodynamic work.
    </enumerate-alpha>
  </framed>

  The equation <math|d*U=<text|�>*q+<text|�>*w> is a <em|differential> form
  of the first law, and <math|\<Delta\>*U=q+w> is the <em|integrated> form.

  The heat and work appearing in the first law are two different modes of
  energy transfer. They can be defined in a general way as follows:

  \;

  <\description-paragraphs>
    <item*|Heat><index|Heat>refers to the transfer of energy across the
    boundary caused by a temperature gradient at the boundary.

    <item*|Work><index|Work>refers to the transfer of energy across the
    boundary caused by the displacement of a macroscopic portion of the
    system on which the surroundings exert a force, or because of other kinds
    of concerted, directed movement of entities (e.g., electrons) on which an
    external force is exerted.
  </description-paragraphs>

  An infinitesimal quantity of energy transferred as heat at a surface
  element of the boundary is written <math|<text|�>*q>, and a finite quantity
  is written <math|q> (Sec. <reference|c2 sec pap>). To obtain the total
  finite heat for a process from <math|q=<big|int><text|�>*q> (Eq.
  <reference|c2 eq int dq=q, int dw=w>), we must integrate over the total
  boundary surface and the entire path of the process.

  An infinitesimal quantity of work is <math|<text|�>*w>, and a finite
  quantity is <math|w=<big|int><text|�>*w>. To obtain <math|w> for a process,
  we integrate all kinds of work over the entire path of the process.

  Any of these quantities for heat and work is <em|positive> if the effect is
  to <em|increase> the internal energy, and <em|negative> if the effect is to
  <em|decrease> it. Thus, positive heat is energy entering the system, and
  negative heat is energy leaving the system. Positive work is work done by
  the surroundings on the system, and negative work is work done by the
  system on the surroundings.

  The first-law equation <math|\<Delta\>*U=q+w> sets up a balance sheet for
  the energy of the system, measured in the <index|Local
  frame><subindex|Frame|local>local frame, by equating its change during a
  process to the net quantity of energy transferred by means of heat and
  work. Note that the equation applies only to a <em|closed> system. If the
  system is open, energy can also be brought across the boundary by the
  transport of matter.

  An important part of the first law is the idea that heat and work are
  <em|quantitative> energy transfers. That is, when a certain quantity of
  energy enters the system in the form of heat, the same quantity leaves the
  surroundings. When the surroundings perform work on the system, the
  increase in the energy of the system is equal in magnitude to the decrease
  in the energy of the surroundings. The principle of conservation of energy
  is obeyed: the total energy (the sum of the energies of the system and
  surroundings) remains constant over time.<\footnote>
    Strictly speaking, it is the sum of the energies of the system, the
    surroundings, and any potential energy shared by both that is constant.
    The shared potential energy is usually negligible or essentially constant
    (Sec. <reference|app-forces work-done>).
  </footnote>

  Heat transfer may occur by conduction, convection, or radiation.<\footnote>
    Some thermodynamicists treat radiation as a separate contribution to
    <math|\<Delta\>*U>, in addition to <math|q> and <math|w>.
  </footnote> We can reduce conduction with good thermal insulation at the
  boundary, we can eliminate conduction and convection with a vacuum gap, and
  we can minimize radiation with highly reflective surfaces at both sides of
  the vacuum gap. The only way to completely prevent heat during a process is
  to arrange conditions in the surroundings so there is no temperature
  gradient at any part of the boundary. Under these conditions the process is
  <subindex|Adiabatic|process><subindex|Process|adiabatic>adiabatic, and any
  energy transfer in a closed system is then solely by means of work.

  <subsection|The concept of thermodynamic work><label|c3 sec
  hwfl-work><label|3-thermo work>

  <index-complex|<tuple|work>||c3 sec hwfl-work idx1|<tuple|Work>>Appendix
  <reference|appendix forces-energy-work> gives a detailed analysis of energy
  and work based on the behavior of a collection of interacting particles
  moving according to the principles of classical mechanics. The analysis
  shows how we should evaluate mechanical thermodynamic work. Suppose the
  displacement responsible for the work comes from linear motion of a portion
  of the boundary in the <math|+x> or <math|-x> direction of the <index|Local
  frame><subindex|Frame|local>local frame. The differential and integrated
  forms of the work are then given by<footnote|These equations are Eq.
  <reference|w=int F(sur)dx' dw=> with a change of notation.>

  <\equation>
    <label|dw=F(sur)dx w=><dbar|w>=F<rsub|x><rsup|<text|sur>>*<dvar|x><space|2em>w=<big|int><rsub|x<rsub|1>><rsup|x<rsub|2>>F<rsub|x><rsup|<text|sur>>*<dvar|x>
  </equation>

  Here <math|F<rsup|<text|sur>><rsub|x>> is the component in the <math|+x>
  direction of the force exerted by the surroundings on the system at the
  moving portion of the boundary, and <dvar|x> is the infinitesimal
  displacement of the boundary in the local frame. If the displacement is in
  the same direction as the force, <dbar|w> is positive; if the displacement
  is in the opposite direction, <dbar|w> is negative.

  The kind of force represented by <math|F<rsub|x><rsup|<text|sur>>> is a
  short-range contact force. Appendix <reference|appendix forces-energy-work>
  shows that the force exerted by a conservative time-independent
  <index|External field><subindex|Field|external>external field, such as a
  gravitational force, should not be included as part of
  <math|F<rsup|<text|sur>><rsub|x>>. This is because the work done by this
  kind of force causes changes of potential and kinetic energies that are
  equal and opposite in sign, with no net effect on the internal energy (see
  Sec. <reference|3-gravitational field>).

  Newton's third law of action and reaction says that a force exerted by the
  surroundings on the system is opposed by a force of equal magnitude exerted
  in the opposite direction by the system on the surroundings. Thus the
  expressions in Eq. <reference|dw=F(sur)dx w=> can be replaced by

  <\equation>
    <label|dw=-Fx(sys)dx w=><dbar|w>=-F<rsup|<text|sys>><rsub|x>*<dvar|x><space|2em>w=-<big|int><rsub|x<rsub|1>><rsup|x<rsub|2>>F<rsub|x><rsup|<text|sys>>*<dvar|x>
  </equation>

  where <math|F<rsub|x><rsup|<text|sys>>> is the component in the <math|+x>
  direction of the contact force exerted by the <em|system> on the
  surroundings at the moving portion of the boundary.

  \;

  <\quote-env>
    <label|work from external weight>An alternative to using the expressions
    in Eqs. <reference|dw=F(sur)dx w=> or <reference|dw=-Fx(sys)dx w=> for
    evaluating <math|w> is to imagine that the only effect of the work on the
    system's surroundings is a change in the elevation of a weight in the
    surroundings. The weight must be one that is linked mechanically to the
    source of the force <math|F<rsup|<text|sur>><rsub|x>>. Then, provided the
    local frame is a stationary<index|Lab frame><subindex|Frame|lab> lab
    frame, the work is equal in magnitude and opposite in sign to the change
    in the weight's potential energy: <math|w=-m*g*\<Delta\>*h> where
    <math|m> is the weight's mass, <math|g> is the acceleration of free fall,
    and <math|h> is the weight's elevation in the lab frame. This
    interpretation of work can be helpful for seeing whether work occurs and
    for deciding on its sign, but of course cannot be used to determine its
    <em|value> if the actual surroundings include no such weight.

    The procedure of evaluating <math|w> from the change of an external
    weight's potential energy requires that this change be the only
    mechanical effect of the process on the surroundings, a condition that in
    practice is met only approximately. For example, <index|Joule, James
    Prescott>Joule's paddle-wheel experiment using two weights linked to the
    system by strings and pulleys, described latter in Sec. <reference|c3 sec
    wgf-paddle>, required corrections for (1)<nbsp>the kinetic energy gained
    by the weights as they sank, (2)<nbsp>friction in the pulley bearings,
    and (3)<nbsp>elasticity of the strings (see Prob.
    <vpageref|prb:3-Joule_expt>).
  </quote-env>

  In the first-law relation <math|\<Delta\>*U=q+w>, the quantities
  <math|\<Delta\>*U>, <math|q>, and <math|w> are all measured in an arbitrary
  <index|Local frame><subindex|Frame|local><em|local> frame. We can write an
  analogous relation for measurements in a stationary <index|Lab
  frame><subindex|Frame|lab><em|lab> frame:

  <\equation>
    <label|DelEsys=>\<Delta\>*E<rsub|<text|sys>>=q<rsub|<text|lab>>+w<rsub|<text|lab>>
  </equation>

  Suppose the chosen local frame is not a lab frame, and we find it more
  convenient to measure the heat <math|q<rsub|<text|lab>>> and the work
  <math|w<rsub|<text|lab>>> in a lab frame than to measure <math|q> and
  <math|w> in the local frame. What corrections are needed to find <math|q>
  and <math|w> in this case?

  If the Cartesian axes of the local frame do not rotate relative to the lab
  frame, then the heat is the same in both frames:
  <math|q=q<rsub|<text|lab>>>.<footnote|Sec. <reference|app-forces
  nonrotating-frame>.>

  The expressions for <math|<dbar|w<rsub|<text|lab>>>> and
  <math|w<rsub|<text|lab>>> are the same as those for <math|<dbar|w>> and
  <math|w> in Eqs. <reference|dw=F(sur)dx w=> and <reference|dw=-Fx(sys)dx
  w=>, with <dvar|x> interpreted as the displacement in the <index|Lab
  frame><subindex|Frame|lab><em|lab> frame. There is an especially simple
  relation between <math|w> and w<rsub|<text|lab>> when the local frame is a
  <index|Center-of-mass frame><subindex|Frame|center-of-mass>center-of-mass
  frame\Vone whose origin moves with the system's center of mass and whose
  axes do not rotate relative to the lab frame:<footnote|Eq.
  <vpageref|w-w(lab) (cm)>.>

  <\equation>
    <label|w-w(lab)=>w=w<rsub|<text|lab>>-<onehalf>\<cdot\>m\<cdot\>*\<Delta\>\<cdot\><around*|(|v<rsub|<text|cm>><rsup|2>|)>-m*\<cdot\>g\<cdot\>\<Delta\>*z<rsub|<text|cm>>
  </equation>

  In this equation <math|m> is the mass of the system,
  <math|v<rsub|<text|cm>>> is the velocity of its center of mass in the lab
  frame, <math|g> is the acceleration of free fall, and
  <math|z<rsub|<text|cm>>> is the height of the center of mass above an
  arbitrary zero of elevation in the lab frame. In typical thermodynamic
  processes the quantities <math|v<rsub|<text|cm>>> and
  <math|z<rsub|<text|cm>>> change to only a negligible extent, if at all, so
  that usually to a good approximation <math|w> is equal to
  <math|w<rsub|<text|lab>>>.<label|w approx equal to w(lab)>

  When the local frame is a center-of-mass frame, we can combine the
  relations <math|\<Delta\>*U=q+w> and <math|q=q<rsub|<text|lab>>> with Eqs.
  <reference|DelEsys=> and <reference|w-w(lab)=> to obtain

  <\equation>
    <label|DelE=DelEk+DelEp+DelU>\<Delta\>*E<rsub|<text|sys>>=\<Delta\>*E<rsub|<text|k>>+\<Delta\>*E<rsub|<text|p>>+\<Delta\>*U
  </equation>

  where <math|E<rsub|<text|k>>=<onehalf>\<cdot\>m\<cdot\>v<rsup|2><rsub|<text|cm>>>
  and <math|E<rsub|<text|p>>=m\<cdot\>g\<cdot\>z<rsub|<text|cm>>> are the
  kinetic and potential energies of the system as a whole in the lab frame.

  A more general relation for <math|w> can be written for any local frame
  that has no rotational motion and whose origin has negligible acceleration
  in the lab frame:<footnote|Eq. <vpageref|w-w(lab)=-mg del z(loc)>.>

  <\equation>
    <label|w-w(lab) = -mg del z(loc)>w=w<rsub|<text|lab>>-m*g*\<Delta\>*z<rsub|<text|loc>>
  </equation>

  Here <math|z<rsub|<text|loc>>> is the elevation in the lab frame of the
  origin of the local frame. <math|\<Delta\>*z<rsub|<text|loc>>> is usually
  small or zero, so again <math|w> is approximately equal to
  <math|w<rsub|<text|lab>>>. The only kinds of processes for which we may
  need to use Eq. <reference|w-w(lab)=> or <reference|w-w(lab) = -mg del
  z(loc)> to calculate a non-negligible difference between <math|w> and
  <math|w<rsub|<text|lab>>> are those in which massive parts of the system
  undergo substantial changes in elevation in the lab frame.

  Simple relations such as these between <math|q> and
  <math|q<rsub|<text|lab>>>, and between <math|w> and
  <math|w<rsub|<text|lab>>>, do not exist if the local frame has rotational
  motion relative to a lab frame.

  Hereafter in this book, thermodynamic work <math|w> will be called simply
  <em|work>. For all practical purposes you can assume the local frames for
  most of the processes to be described are stationary <index|Lab
  frame><subindex|Frame|lab>lab frames. The discussion above shows that the
  values of heat and work measured in these frames are usually the same, or
  practically the same, as if they were measured in a local frame moving with
  the system's center of mass. A notable exception is the local frame needed
  to treat the thermodynamic properties of a liquid solution in a centrifuge
  cell. In this case the local frame is fixed in the spinning rotor of the
  centrifuge and has rotational motion. This special case will be discussed
  in Sec. <reference|c9 sec-mgcf-liq-centrifuge>.<index-complex|<tuple|work>||c3
  sec hwfl-work idx1|<tuple|Work>>

  <subsection|Work coefficients and work coordinates><label|c3 sec
  hwfl-work-coeff>

  If a process has only one kind of work, it can be expressed in the form

  <\equation>
    <label|dw=YdX><dbar|w>=Y*<dvar|X><space|2em><text|or><space|2em>w=<big|int><rsub|X<rsub|1>><rsup|X<rsub|2>>Y*<dvar|X>
  </equation>

  where <math|Y> is a generalized force called a
  <subindex|Work|coefficient><newterm|work coefficient> and <math|X> is a
  generalized displacement called a <subindex|Work|coordinate><newterm|work
  coordinate>. The work coefficient and work coordinate are
  <subindex|Conjugate|variables><em|conjugate> variables. They are not
  necessarily actual forces and displacements. For example, we shall see in
  Sec. <reference|c3 sec dw-expansion-gas> that reversible expansion work is
  given by <math|<dbar|w>=-p*<dvar|V>>; in this case, the work coefficient is
  <math|-p> and the work coordinate is <math|V>.

  A process may have more than one kind of work, each with its own work
  coefficient and conjugate work coordinate. In this case the work can be
  expressed as a sum over the different kinds labeled by the index <math|i>:

  <\equation>
    <label|dw=sum Y(i)dX(i)><dbar|w>=<big|sum><rsub|i>Y<rsub|i>*<dvar|X<rsub|i>><space|2em><text|or><space|2em>w=<big|sum><rsub|i><big|int><rsub|X<rsub|i,1>><rsup|X<rsub|i,2>><space|-0.17em>Y<rsub|i>*<dvar|X<rsub|i>>
  </equation>

  <subsection|Heat and work as path functions><label|3-Heat and work as path
  functions><label|c3 sec hwfl-work-path>

  Consider the apparatus shown in Fig. <vpageref|fig:3-paddle
  \ heater>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/PAD-HEAT.eps|180pt|110pt||>>
        <label|fig:3-paddle \ heater><label|c3 fig paddle-heater>System
        containing an electrical resistor and a paddle wheel immersed in
        water. Cross-hatched area: removable thermal insulation.
      </big-figure>
    </framed>
  </float>

  The <em|system> consists of the water together with the immersed parts:
  stirring paddles attached to a shaft (a <index|Paddle wheel>paddle wheel)
  and an <subindex|Electrical|resistor><index|Resistor, electrical>electrical
  resistor attached to wires. In equilibrium states of this system, the
  paddle wheel is stationary and the temperature and pressure are uniform.
  The system is open to the atmosphere, so the pressure is constrained to be
  constant. We may describe the equilibrium states of this system by a single
  independent variable, the temperature <math|T>. (The angular position of
  the shaft is irrelevant to the state and is not a state function for
  equilibrium states of this system.)

  Here are three experiments with different processes. Each process has the
  same initial state defined by <math|T<rsub|1>=300.0 <text|K>>, and each has
  the same final state.

  <\description-compact>
    <item*|Experiment 1><label|paddle wheel experiment>We surround the system
    with thermal insulation as shown in the figure and release the external
    weight, which is linked mechanically to the paddle wheel. The resulting
    paddle-wheel rotation causes turbulent churning of the water and an
    increase in its temperature. Assume that after the weight hits the stop
    and the paddle wheel comes to rest, the final angular position of the
    paddle wheel is the same as at the beginning of the experiment. We can
    calculate the work done on the system from the difference between the
    potential energy lost by the weight and the kinetic energy gained before
    it reaches the stop.<footnote|This calculation is an example of the
    procedure mentioned on page <pageref|work from external weight> in which
    the change in elevation of an external weight is used to evaluate work.>
    We wait until the water comes to rest and the system comes to thermal
    equilibrium, then measure the final temperature. Assume the final
    temperature is <math|T<rsub|2>=300.10 <text|K>>, an increase of
    <math|0.10> kelvins.

    <item*|Experiment 2>We start with the system in the same initial state as
    in experiment 1, and again surround it with thermal insulation. This
    time, instead of releasing the weight we close the switch to complete an
    electrical circuit with the resistor and allow the same quantity of
    <subindex|Electrical|work><subindex|Work|electrical>electrical work to be
    done on the system as the mechanical work done in experiment 1. We
    discover the final temperature (<math|300.10 <text|K>>) is exactly the
    same as at the end of experiment 1. The process and path are different
    from those in experiment 1, but the work and the initial and final states
    are the same.

    <item*|Experiment 3><label|third experiment>We return the system to its
    initial state, remove the thermal insulation, and place the system in
    thermal contact with a <subindex|Heat|reservoir>heat reservoir of
    temperature <math|300.10 <text|K>>. Energy can now enter the system in
    the form of heat, and does so because of the temperature gradient at the
    boundary. By a substitution of heat for mechanical or electrical work,
    the system changes to the same final state as in experiments 1 and 2.
  </description-compact>

  Although the paths in the three experiments are entirely different, the
  overall change of state is the same. In fact, a person who observes only
  the initial and final states and has no knowledge of the intermediate
  states or the changes in the surroundings will be ignorant of the path. Did
  the paddle wheel turn? Did an electric current pass through the resistor?
  How much energy was transferred by work and how much by heat? The observer
  cannot tell from the change of state, because heat and work are not state
  functions. The change of state depends on the <em|sum> of heat and work.
  This sum is the change in the state function <math|U<space|-0.17em>>, as
  expressed by the integrated form of the first law, <math|\<Delta\>*U=q+w>.

  It follows from this discussion that neither heat nor work are quantities
  possessed by the system. A system at a given instant does not <em|have> or
  <em|contain> a particular quantity of heat or a particular quantity of
  work. Instead, heat and work depend on the path of a process occurring over
  a period of time. They are <index|Path function><em|path> functions.

  <subsection|Heat and heating><label|c3 sec hwfl-heat>

  In thermodynamics, the <subindex|Heat|technical meaning of>technical
  meaning of the word \Pheat\Q when used as a noun is <em|energy transferred
  across the boundary because of a temperature gradient at the boundary>.

  In everyday speech the noun <em|heat> is often used somewhat differently.
  Here are three statements with similar meanings that could be misleading:

  \PHeat is transferred from a laboratory hot plate to a beaker of water.\Q

  \PHeat flows from a warmer body to a cooler body.\Q

  \PTo remove heat from a hot body, place it in cold water.\Q

  Statements such as these may give the false impression that heat is like a
  substance that retains its identity as it moves from one body to another.
  Actually heat, like work, does not exist as an entity once a process is
  completed. Nevertheless, the wording of statements such as these is
  embedded in our everyday language, and no harm is done if we interpret them
  correctly. This book, for conciseness, often refers to \Pheat transfer\Q
  and \Pheat flow,\Q instead of using the technically more correct phrase
  \Penergy transfer by means of heat.\Q

  Another common problem is failure to distinguish between thermodynamic
  \Pheat\Q and the process of \Pheating.\Q To <em|heat> a system is to cause
  its temperature to increase. A <em|heated> system is one that has become
  warmer. This process of <em|heating> does not necessarily involve
  thermodynamic heat; it can also be carried out with work as illustrated by
  experiments 1 and 2 of the preceding section.

  <\quote-env>
    The notion of heat as an indestructible substance was the essence of the
    <index|Caloric theory>caloric theory. This theory was finally disproved
    by the cannon-boring experiments of <index|Thompson,
    Benjamin><index|Rumford, Count>Benjamin Thompson (Count Rumford) in the
    late eighteenth century, and in a more quantitative way by the
    measurement of the mechanical equivalent of heat by James Joule
    <index|Joule, James Prescott>in the 1840s (see Sec. <reference|3-Joule
    paddle wheel>).
  </quote-env>

  <subsection|Heat capacity><label|3-heat capacity><label|c3 sec
  hwfl-heat-capacity>

  The <index|Heat capacity><newterm|heat capacity> of a closed system is
  defined as the ratio of an infinitesimal quantity of heat transferred
  across the boundary under specified conditions and the resulting
  infinitesimal temperature change:

  <equation-cov2|<label|heat capacity def><around*|(|<text|heat
  capacity>|)><bk-equal-def><frac|<dbar|q>|<dvar|T>>|(closed system)>

  Since <math|q> is a path function, the value of the heat capacity depends
  on the specified conditions, usually either constant volume or constant
  pressure. <math|C<rsub|V>> is the <index-complex|<tuple|heat
  capacity|constant volume>|||<tuple|Heat capacity|at constant
  volume>><em|heat capacity at constant volume> and <math|C<rsub|p>> is the
  <index-complex|<tuple|heat capacity|constant pressure>|||<tuple|Heat
  capacity|at constant pressure>><em|heat capacity at constant pressure>.
  These are extensive state functions that will be discussed more fully in
  Sec. <reference|c5 sec ehc>.

  <subsection|Thermal energy><label|3-thermal energy><label|c3 sec
  hwfl-energy-thermal>

  It is sometimes useful to use the concept of
  <subindex|Energy|thermal><subindex|Thermal|energy><newterm|thermal
  energy|thermal energy>. It can be defined as the kinetic energy of random
  translational motions of atoms and molecules relative to the local frame,
  plus the vibrational and rotational energies of molecules. The thermal
  energy of a body or phase depends on its temperature, and increases when
  the temperature increases. The thermal energy of a system is a contribution
  to the internal energy.

  It is important to understand that a change of the system's thermal energy
  during a process is not necessarily the same as energy transferred across
  the system boundary as heat. The two quantities are equal only if the
  system is closed and there is no work, volume change, phase change, or
  chemical reaction. This is illustrated by the three experiments described
  in Sec. <reference|3-Heat and work as path functions>: the thermal energy
  change is the same in each experiment, but only in experiment 3 is the work
  negligible and the thermal energy change equal to the heat.

  <\bio-insert>
    <include|bio-RUMFORD.tm>
  </bio-insert>

  <section|Spontaneous, Reversible, and Irreversible Processes><label|3-spont
  and rev processes><label|c3 sec-srip>

  A <index|Spontaneous process><subindex|Process|spontaneous><newterm|spontaneous
  process|spontaneous process> is a process that can actually occur in a
  finite time period under the existing conditions. Any change over time in
  the state of a system that we observe experimentally is a spontaneous
  process.

  A spontaneous process is sometimes called a natural process, feasible
  process, possible process, allowed process, or real process.

  <subsection|Reversible processes><label|3-reversible processes><label|c3
  sec srip-processes-reversible>

  <index-complex|<tuple|reversible|process>||c3 sec srip-processes-reversible
  idx1|<tuple|Reversible|process>><index-complex|<tuple|process|reversible>||c3
  sec srip-processes-reversible idx2|<tuple|Process|reversible>>A
  <newterm|reversible process> is an important concept in thermodynamics.
  This concept is needed for the chain of reasoning in the next chapter by
  which the existence of entropy as a state function is derived and its
  changes defined. The existence of entropy then leads on to the
  establishment of criteria for spontaneity and for various kinds of
  equilibria. Innumerable useful relations (equalities) among heat, work, and
  state functions such as Gibbs energy can be obtained for processes that are
  carried out reversibly.

  Before reversible processes can be discussed, it is necessary to explain
  the meaning of the <index|Reverse of a process><subindex|Process|reverse of
  a><em|reverse> of a process. If a particular process takes the system from
  an initial state A through a continuous sequence of intermediate states to
  a final state B, then the reverse of this process is a change over time
  from state B to state A with the same intermediate states occurring in the
  reverse time sequence. To visualize the reverse of any process, imagine
  making a movie film of the events of the process. Each frame of the film is
  a \Psnapshot\Q picture of the state at one instant. If you run the film
  backward through a movie projector, you see the reverse process: the values
  of system properties such as <math|p> and <math|V> appear to change in
  reverse chronological order, and each velocity changes sign.

  If a process is spontaneous, which implies its reverse cannot be observed
  experimentally, the process is <em|irreversible>.

  The concept of a reversible process is not easy to describe or to grasp.
  Perhaps the most confusing aspect is that a reversible process is not a
  process that ever actually occurs, but is only approached as a hypothetical
  limit.

  During a reversible process the system passes through a continuous sequence
  of equilibrium states. These states are ones that can be approached, as
  closely as desired, by the states of a spontaneous process carried out
  sufficiently slowly. The slower the process is, the more time there is
  between two successive intermediate states for equilibrium to be
  approached. As the spontaneous process is carried out more and more slowly,
  it approaches the reversible limit. Thus, a reversible process is an
  <em|idealized> process with a sequence of equilibrium states that are those
  of a spontaneous process in the <em|limit> of infinite slowness.

  <\quote-env>
    Fermi<footnote|Ref. <cite|fermi-56>, page 4.> describes a reversible
    process as follows: \PA transformation is said to be <em|reversible> when
    the successive states of the transformation differ by infinitesimals from
    <em|equilibrium states>. A reversible transformation can therefore
    connect only those initial and final states which are states of
    equilibrium. A reversible transformation can be realized in practice by
    changing the external conditions so slowly that the system has time to
    adjust itself gradually to the altered conditions.\Q
  </quote-env>

  This book has many equations expressing relations among heat, work, and
  state functions during various kinds of reversible processes. What is the
  use of an equation for a process that can never actually occur? The point
  is that the equation can describe a <subindex|Process|spontaneous><index|Spontaneous
  process>spontaneous process to a high degree of accuracy, if the process is
  carried out slowly enough for the intermediate states to depart only
  slightly from exact equilibrium states. For example, for many important
  spontaneous processes we can assume the temperature and pressure are
  uniform throughout the system, although this is only an approximation.

  A reversible process of a closed system, as used in this book, has all of
  the following characteristics:<label|3-rev list start>

  <\itemize>
    <item>It is an imaginary, idealized process in which the system passes
    through a continuous sequence of equilibrium states. That is, the state
    at each instant is one that in an isolated system would persist with no
    tendency to change over time. (This kind of process is sometimes called a
    <subindex|Process|quasistatic><index|Quasistatic process><em|quasistatic>
    process.)

    <item>The sequence of equilibrium states can be approximated, as closely
    as desired, by the intermediate states of a real
    <subindex|Process|spontaneous><index|Spontaneous process>spontaneous
    process carried out sufficiently slowly. The reverse sequence of
    equilibrium states can also be approximated, as closely as desired, by
    the intermediate states of another spontaneous process carried out
    sufficiently slowly. (This requirement prevents any spontaneous process
    with hysteresis, such as plastic deformation or the stretching of a metal
    wire beyond its elastic limit, from having a reversible limit.) During
    the approach to infinite slowness, very slow changes of the type
    described in item 3 on page <pageref|very slow changes> must be
    eliminated, i.e., prevented with hypothetical constraints.

    <item><label|no rev limit in isolated system> The
    <subindex|Process|spontaneous><index|Spontaneous process>spontaneous
    process of a closed system that has a reversible limit must be a process
    with heat, or work, or both\Vthe system cannot be an isolated one. It
    must be possible for an experimenter to use conditions in the
    surroundings to control the rate at which energy is transferred across
    the boundary by means of heat and work, and thus to make the process go
    as slowly as desired.

    <item>If energy is transferred by work during a reversible process, the
    work coefficient <math|Y> in the expression <math|<dbar|w>=Y*<dvar|X>>
    must be finite (nonzero) in equilibrium states of the system. For
    example, if the work is given by <math|<dbar|w>=-F<rsup|<text|sys>><rsub|x>*<dvar|x>>
    (Eq. <reference|dw=-Fx(sys)dx w=>), the force
    <math|F<rsup|<text|sys>><rsub|x>> exerted by the system on the
    surroundings must be present when the system is in an equilibrium state.

    <item>In the reversible limit, any energy dissipation within the system,
    such as that due to internal friction, vanishes. Internal energy
    dissipation is the situation in which energy transferred to the system by
    positive work is not fully recovered in the surroundings when the sign of
    the work coordinate change <math|<dvar|X>> is reversed.
    <index|Dissipation of energy><subindex|Energy|dissipation of>

    <item><label|heat and work recovered>When any infinitesimal step of a
    reversible process takes place in reverse, the magnitudes of the heat
    <math|<dbar|q>> and work <math|<dbar|w>> are unchanged and their signs
    are reversed. Thus, energy transferred as heat in one direction across
    the boundary during a reversible process is transferred as heat in the
    opposite direction during the reverse process. The same is true for the
    energy transferred as work.
  </itemize>

  <label|3-rev list end>We must imagine the reversible process to proceed at
  a finite rate, otherwise there would be no change of state over time. The
  precise rate of the change is not important. Imagine a gas whose volume,
  temperature, and pressure are changing at some finite rate while the
  temperature and pressure magically stay perfectly uniform throughout the
  system. This is an entirely imaginary process, because there is no
  temperature or pressure gradient\Vno physical \Pdriving force\Q\Vthat would
  make the change tend to occur in a particular direction. This imaginary
  process is a reversible process\Vone whose states of uniform temperature
  and pressure are approached by the states of a real process as the real
  process takes place more and more slowly.

  It is a good idea, whenever you see the word \Preversible,\Q to think \Pin
  the reversible limit.\Q Thus a <em|reversible process> is a process in the
  reversible limit, <em|reversible work> is work in the reversible limit, and
  so on.

  <subsection|Reversibility and the surroundings><label|3-reversibility
  \ surroundings><label|c3 sec-srip-reversibility>

  The reverse of a reversible process is itself a reversible process. As
  explained on page <pageref|heat and work recovered>, the quantities of
  energy transferred across the boundary as heat and work during a reversible
  process are returned across the boundary when the reversible process is
  followed by the reverse process.

  Some authors describe a reversible process as one that allows both the
  system and the surroundings to be restored to their initial
  states.<footnote|For example, Ref. <cite|macdougall-39>, page 73: \PA
  process in which a system goes from state A to state B is defined to be
  (thermodynamically) reversible, if it is possible to restore the system to
  the state A without producing permanent changes of any kind anywhere
  else.\Q> The problem with this description is that during the time period
  in which the process and its reverse take place, spontaneous irreversible
  changes inevitably occur in the surroundings.

  The textbook <em|Heat and Thermodynamics> by Zemansky and
  Dittman<footnote|Ref. <cite|zemansky-97>, page 158.> states that \Pa
  reversible process is one that is performed in such a way that, at the
  conclusion of the process, both the system and the local surroundings may
  be restored to their initial states without producing any changes in the
  rest of the universe.\Q The authors explain that by \Plocal surroundings\Q
  they mean parts of the surroundings that interact directly with the system
  to transfer energy across the boundary, and that \Pthe rest of the
  universe\Q consists of what they call \Pauxiliary surroundings\Q that
  <em|might> interact with the system.

  They give as an example of local surroundings a weight whose lowering or
  raising causes work to be done on or by the system, and a series of heat
  reservoirs placed in thermal contact with the system to cause heat
  transfer. The auxiliary surroundings presumably include a way to lower or
  raise the weight and to move the heat reservoirs to and away from the
  system. The control of these external operations would require a human
  operator or some sort of automated mechanism whose actions would be
  spontaneous and irreversible. If these are considered to be part of the
  auxiliary surroundings, as it seems they should be, then it would in fact
  not be possible for all the auxiliary surroundings to return to their
  initial states as claimed.

  The cylinder-and-piston device shown in Fig. <vpageref|fig:3-external
  friction> can be used to illustrate a reversible process whose reverse
  process does not restore the local surroundings.

  <\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/ext-fric.eps|147pt|124pt||>>
        <label|fig:3-external friction>Gas confined by a lubricated piston in
        a cylinder in contact with a heat reservoir (res).
      </big-figure>
    </framed>
  </float>

  The <em|system> in this example is the confined gas. The local surroundings
  are the piston (a weight), and the heat reservoir of temperature
  <math|T<rsub|<text|res>>> in thermal contact with the system. Initially,
  the gas pressure pushes the piston against the catches, which hold it in
  place at elevation <math|h<rsub|1>>. The gas is in an equilibrium state at
  temperature <math|T<rsub|<text|res>>>, volume <math|V<rsub|1>>, and
  pressure <math|p<rsub|1>>. To begin the process, the catches are removed.
  The piston moves upwards and comes to rest at an elevation greater than
  <math|h<rsub|1>>. The gas has now changed to a new equilibrium state with
  temperature <math|T<rsub|<text|res>>>, a volume greater than
  <math|V<rsub|1>>, and a pressure less than <math|p<rsub|1>>.

  The rate of this expansion process is influenced by sliding friction in the
  surroundings at the lubricated seal between the edge of the piston and the
  inner surface of the cylinder. Although the frictional drag force for a
  given lubricant viscosity approaches zero as the piston velocity decreases,
  model calculations<footnote|Ref. <cite|devoe-13>, Example 2.> show that the
  greater is the viscosity, the slower is the expansion. Assume the lubricant
  has a high viscosity that slows the expansion enough to make the
  intermediate states differ only slightly from equilibrium states. In the
  limit of infinite slowness, the process would be a reversible isothermal
  expansion of the gas. The friction at the piston, needed for the approach
  to a reversible expansion, produces thermal energy that is transferred as
  heat to the heat reservoir.

  To reverse the expansion process, a weight is placed on the piston, causing
  the piston to sink and eventually return to rest. Again friction at the
  piston causes heat transfer to the heat reservoir. The weight's mass is
  such that, after the gas has become equilibrated with the heat reservoir,
  the piston has returned to its initial elevation <math|h<rsub|1>>. The
  system has now returned to its initial state with
  <math|T=T<rsub|<text|res>>>, <math|V=V<rsub|1>>, and <math|p=p<rsub|1>>. In
  the limit of infinite slowness, this process is a reversible isothermal
  compression that is the reverse of the reversible expansion.

  Note that the local surroundings have not returned to their initial
  conditions: a weight has been added to the piston, and the heat reservoir's
  internal energy has increased due to the friction at the piston. It would
  be possible to restore these initial conditions, but the necessary
  operations would involve further irreversible changes in the auxiliary
  surroundings.

  Based on the above, it is apparent that it is neither useful nor valid to
  describe a reversible process as one for which the surroundings can be
  restored. Instead, this book defines a reversible process by the
  characteristics listed on pages <pageref|3-rev list start> and
  <pageref|3-rev list end>, involving only changes in the system itself,
  regardless of what happens in the surroundings. Such a process can be
  described as having <em|internal reversibility><subindex|Reversibility|internal>
  and as being <em|internally reversible>.<footnote|Ref. <cite|hats-65>,
  Section 14.7; Ref. <cite|sonntag-82>, page 182; Ref. <cite|deheer-86>,
  Section 5.4.>

  <index-complex|<tuple|reversible|process>||c3 sec srip-processes-reversible
  idx1|<tuple|Reversible|process>><index-complex|<tuple|process|reversible>||c3
  sec srip-processes-reversible idx2|<tuple|Process|reversible>>

  <subsection|Irreversible processes><label|3-irreversible
  processes><label|c3 sec-srip-processes-irreversible>

  An <index|Irreversible process><subindex|Process|irreversible><newterm|irreversible>
  process is a <subindex|Process|spontaneous><index|Spontaneous
  process>spontaneous process whose reverse is neither spontaneous nor
  reversible. That is, the reverse of an irreversible process can never
  actually occur and is <subindex|Process|impossible><index|Impossible
  process><em|impossible>. If a movie is made of a spontaneous process, and
  the time sequence of the events depicted by the film when it is run
  backward could not occur in reality, the spontaneous process is
  irreversible.

  A good example of a spontaneous, irreversible process is experiment 1 on
  page <pageref|paddle wheel experiment>, in which the sinking of an external
  weight causes a paddle wheel immersed in water to rotate and the
  temperature of the water to increase. During this experiment mechanical
  energy is <subindex|Energy|dissipation of><index|Dissipation of
  energy>dissipated into thermal energy. Suppose you insert a thermometer in
  the water and make a movie film of the experiment. Then when you run the
  film backward in a projector, you will see the paddle wheel rotating in the
  direction that raises the weight, and the water becoming cooler according
  to the thermometer. Clearly, this reverse process<label|reverse paddle
  wheel>is impossible in the real physical world, and the process occurring
  during the experiment is irreversible. It is not difficult to understand
  why it is irreversible when we consider events on the microscopic level: it
  is extremely unlikely that the H<rsub|<math|2>>O molecules next to the
  paddles would happen to move simultaneously over a period of time in the
  concerted motion needed to raise the weight.

  <subsection|Purely mechanical processes><label|3-purely mechanical
  processes><label|c3 sec-srip-processes-mechanical>

  There is a class of <subindex|Process|spontaneous><index|Spontaneous
  process>spontaneous processes that are also spontaneous in reverse; that
  is, spontaneous but not irreversible. These are <subindex|Process|purely
  mechanical><em|purely mechanical> processes involving the motion of
  perfectly-elastic macroscopic bodies without friction, temperature
  gradients, viscous flow, or other irreversible changes.

  A simple example of a purely mechanical process and its reverse is shown in
  Fig. <reference|fig:3-mechanical processes>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/MECHANIC.eps|126pt|69pt||>>
        <label|fig:3-mechanical processes>Two purely mechanical processes
        that are the reverse of one another: a thrown ball moving through a
        vacuum (a)<nbsp>to the right; (b)<nbsp>to the left.
      </big-figure>
    </framed>
  </float>

  The ball can move spontaneously in either direction. Another example is a
  flywheel with frictionless bearings rotating in a vacuum.

  A purely mechanical process proceeding at a finite rate is not reversible,
  for its states are not equilibrium states. Such a process is an
  idealization, of a different kind than a reversible process, and is of
  little interest in chemistry. Later chapters of this book will ignore such
  processes and will treat the terms <em|spontaneous> and <em|irreversible>
  as synonyms.

  <section|Heat Transfer><label|3-heat transfer><label|c3 sec ht>

  <index-complex|<tuple|heat|transfer>||c3 sec ht
  idx1|<tuple|Heat|transfer>>This section describes irreversible and
  reversible heat transfer. Keep in mind that when this book refers to
  <em|heat transfer> or <em|heat flow>, energy is being transferred across
  the boundary on account of a temperature gradient at the boundary. The
  transfer is always in the direction of decreasing temperature.

  We may sometimes wish to treat the temperature as if it is discontinuous at
  the boundary, with different values on either side. The transfer of energy
  is then from the warmer side to the cooler side. The temperature is not
  actually discontinuous; instead there is a thin zone with a temperature
  gradient.

  <subsection|Heating and cooling><label|3-heating \ cooling><label|c3 sec
  ht-heating-cooling>

  As an illustration of irreversible heat transfer, consider a system that is
  a solid metal sphere. This spherical body is immersed in a well-stirred
  water bath whose temperature we can control. The bath and the metal sphere
  are initially equilibrated at temperature <math|T<rsub|1>=300.0 <text|K>>,
  and we wish to raise the temperature of the sphere by one kelvin to a final
  uniform temperature <math|T<rsub|2>=301.0 <text|K>>.

  One way to do this is to rapidly increase the external bath temperature to
  <math|301.0 <text|K>> and keep it at that temperature. The temperature
  difference across the surface of the immersed sphere then causes a
  spontaneous flow of heat through the system boundary into the sphere. It
  takes time for all parts of the sphere to reach the higher temperature, so
  a temporary internal temperature gradient is established. Thermal energy
  flows spontaneously from the higher temperature at the boundary to the
  lower temperature in the interior. Eventually the temperature in the sphere
  becomes uniform and equal to the bath temperature of <math|301.0 <text|K>>.

  Figure <reference|fig:3-thermal sphere>(a) on page <pageref|fig:3-thermal
  sphere> graphically depicts temperatures within the sphere at different
  times during the heating process. Note the temperature gradient in the
  intermediate states. Because of the gradient, these states cannot be
  characterized by a single value of the temperature. If we were to suddenly
  isolate the system (the sphere) with a thermally-insulated jacket while it
  is in one of these states, the state would change as the temperature
  gradient rapidly disappears. Thus, the intermediate states of the
  spontaneous heating process are not equilibrium states, and the rapid
  heating process is not reversible.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/thermal_sphere.eps|385pt|140pt||>>
        <label|fig:3-thermal sphere>Temperature profiles in a copper sphere
        of radius 5<space|0.17em>cm immersed in a water bath. The temperature
        at each of the times indicated is plotted as a function of <math|r>,
        the distance from the center of the sphere. The temperature at
        distances greater than 5<space|0.17em>cm, to the right of the
        vertical dashed line in each graph, is that of the external water
        bath.

        \ (a)<nbsp>Bath temperature raised at the rate of <math|0.10
        <text|K>\<cdot\><text|s><rsup|-1>>.

        \ (b)<nbsp>Bath temperature raised infinitely slowly.

        \ (c)<nbsp>Bath temperature lowered at the rate of <math|0.10
        <text|K>\<cdot\><text|s><rsup|-1>>.
      </big-figure>
    </framed>
  </float>

  To make the intermediate states more nearly uniform in temperature, with
  smaller temperature gradients, we can raise the temperature of the bath at
  a slower rate. The sequence of states approached in the limit of infinite
  slowness is indicated in Fig. <reference|fig:3-thermal sphere>(b). In each
  intermediate state of this limiting sequence, the temperature is perfectly
  uniform throughout the sphere and is equal to the external bath
  temperature. That is, each state has thermal equilibrium both internally
  and with respect to the surroundings. A single temperature now suffices to
  define the state at each instant. Each state is an <em|equilibrium> state
  because it would have no tendency to change if we isolated the system with
  thermal insulation. This limiting sequence of states is a <em|reversible>
  heating process.

  The reverse of the reversible heating process is a reversible cooling
  process in which the temperature is again uniform in each state. The
  sequence of states of this reverse process is the limit of the spontaneous
  cooling process depicted in Fig. <reference|fig:3-thermal sphere>(c) as we
  decrease the bath temperature more and more slowly.

  In any real heating process occurring at a finite rate, the sphere's
  temperature could not be perfectly uniform in intermediate states. If we
  raise the bath temperature very slowly, however, the temperature in all
  parts of the sphere will be very close to that of the bath. At any point in
  this very slow heating process, it would then take only a small decrease in
  the bath temperature to start a <em|cooling> process; that is, the
  practically-reversible heating process would be reversed.

  The important thing to note about the temperature gradients shown in Fig.
  <reference|fig:3-thermal sphere>(c) for the spontaneous cooling process is
  that none resemble the gradients in Fig. <reference|fig:3-thermal
  sphere>(a) for the spontaneous heating process\Vthe gradients are in
  opposite directions. It is physically impossible for the sequence of states
  of either process to occur in the reverse chronological order, for that
  would have thermal energy flowing in the wrong direction along the
  temperature gradient. These considerations show that a spontaneous heat
  transfer is irreversible. Only in the reversible limits do the heating and
  cooling processes have the same intermediate states; these states have no
  temperature gradients.

  Although the spontaneous heating and cooling processes are irreversible,
  the energy transferred into the system during heating can be fully
  recovered as energy transferred back to the surroundings during cooling,
  provided there is no irreversible work. This recoverability of irreversible
  heat is in distinct contrast to the behavior of irreversible work.

  <subsection|Spontaneous phase transitions><label|c3
  sec-ht-phase-transitions>

  Consider a different kind of system, one consisting of the liquid and solid
  phases of a pure substance. At a given pressure, this kind of system can be
  in transfer equilibrium at only one temperature: for example, water and ice
  at <math|1.01 <text|bar>> and <math|273.15 <text|K>>. Suppose the system is
  initially at this pressure and temperature. Heat transfer into the system
  will then cause a phase transition from solid to liquid (Sec.
  <reference|2-phase coexistence>). We can carry out the heat transfer by
  placing the system in thermal contact with an external water bath at a
  higher temperature than the equilibrium temperature, which will cause a
  temperature gradient in the system and the melting of an amount of solid
  proportional to the quantity of energy transferred.

  The closer the external temperature is to the equilibrium temperature, the
  smaller are the temperature gradients and the closer are the states of the
  system to equilibrium states. In the limit as the temperature difference
  approaches zero, the system passes through a sequence of equilibrium states
  in which the temperature is uniform and constant, energy is transferred
  into the system by heat, and the substance is transformed from solid to
  liquid. This idealized process is an <subindex|Equilibrium|phase
  transition><subsubindex|Phase|transition|equilibrium><subindex|Reversible|phase
  transition><em|equilibrium> phase transition, and it is a reversible
  process.

  <index-complex|<tuple|heat|transfer>||c3 sec ht idx1|<tuple|Heat|transfer>>

  <section|Deformation Work><label|3-deformation><label|c3 sec dw>

  <index-complex|<tuple|work|deformation>||c3 sec dw
  idx1|<tuple|Work|deformation>><index-complex|<tuple|deformation|work>||c3
  sec dw idx2|<tuple|Deformation|work>>This and the four following sections
  (Secs. <reference|3-applications of exp work>\U<reference|3-electrical
  work>) describe some spontaneous, irreversible processes with various kinds
  of work and illustrate the concept of a reversible limit for the processes
  that have such a limit.

  The <em|deformation> of a system involves changes in the position, relative
  to the <index|Local frame><subindex|Frame|local>local frame, of portions of
  the system boundary. At a small surface element <math|\<tau\>> of the
  boundary, the work of deformation is given in general by the
  expression<footnote|From Eq. <vpageref|w=sum int F cos(alpha)ds dw=>.>

  <\equation>
    <label|dw(tau)=F(sur)cos(alpha)ds><dbar|w><rsub|\<tau\>>=F<rsup|<text|sur>><rsub|\<tau\>>*cos
    \<alpha\><rsub|\<tau\>>*<dvar|s<rsub|\<tau\>>>
  </equation>

  where <math|F<rsup|<text|sur>><rsub|\<tau\>>> is the magnitude of the
  contact force exerted by the surroundings on the surface element,
  <math|<dvar|s<rsub|\<tau\>>>> is the infinitesimal displacement of the
  surface element in the local frame, and <math|\<alpha\><rsub|\<tau\>>> is
  the angle between the directions of the force and the displacement. If the
  displacement is entirely parallel to the <math|x> axis, the expression
  becomes equivalent to that already given by Eq. <vpageref|dw=F(sur)dx w=>:
  <math|<dbar|w>=F<rsup|<text|sur>><rsub|x>*<dvar|x>>.

  <subsection|Gas in a cylinder-and-piston
  device><label|3-expansion><label|c3 sec dw-piston>

  A useful kind of deformation for the development of thermodynamic theory is
  a change in the volume of a gas or liquid.

  As a model for the work involved in changing the volume of a gas, consider
  the arrangement shown in Fig. <vpageref|fig:3-cylinder>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/CYLINDER.eps|160pt|79pt||>>
        <label|fig:3-cylinder>Forces acting on the piston (cross hatched) in
        a cylinder-and-piston device containing a gas (shaded). The direction
        of <math|F<rsub|<text|fric>>> shown here is for expansion.
      </big-figure>
    </framed>
  </float>

  A sample of gas is confined in a horizontal cylinder by a piston. The
  <em|system> is the gas. The piston is not part of the system, but its
  position given by the variable <math|x<rsub|<text|pis>>> determines the
  system's volume. Movement of the piston to the right, in the <math|+x>
  direction, expands the gas; movement to the left, in the <math|-x>
  direction, compresses it.

  We will find it instructive to look in detail at the forces acting on the
  piston. There are three kinds: the force <math|F<rsub|<text|gas>>> exerted
  in the <math|+x> direction by the gas; an external force
  <math|F<rsub|<text|ext>>> in the <math|-x> direction, which we can control
  in the surroundings; and a <index|Frictional
  force><subindex|Force|frictional>frictional force
  <math|F<rsub|<text|fric>>> in the direction opposite to the piston's
  velocity when the piston moves.

  The friction occurs at the seal between the edge of the piston and the
  cylinder wall. We will assume this seal is lubricated, and that
  <math|F<rsub|<text|fric>>> approaches zero as the piston velocity
  approaches zero.

  Let <math|p<rsub|<text|b>>> be the average pressure of the gas <em|at the
  piston>\Vthat is, at the moving portion of the system boundary (the
  subscript \Pb\Q stands for boundary). Then the force exerted by the gas on
  the piston is given by

  <\equation>
    <label|F(gas)=p(b)A(s)>F<rsub|<text|gas>>=p<rsub|<text|b>>*A<rsub|<text|s>>
  </equation>

  where <math|A<rsub|<text|s>>> is the cross-section area of the cylinder.

  The component in the <math|+x> direction of the net force
  <math|F<rsub|<text|net>>> acting on the piston is given by

  <\equation>
    <label|F(net)=F(gas)-F(ext)+F(fric)>F<rsub|<text|net>>=F<rsub|<text|gas>>-F<rsub|<text|ext>>+F<rsub|<text|fric>>
  </equation>

  Here, <math|F<rsub|<text|gas>>> and <math|F<rsub|<text|ext>>> are taken as
  positive. <math|F<rsub|<text|fric>>> is negative when the piston moves to
  the right, positive when the piston moves to the left, and zero when the
  piston is stationary.

  Suppose the system (the gas) initially is in an equilibrium state of
  uniform temperature <math|T<rsub|1>> and uniform pressure <math|p<rsub|1>>,
  and the piston is stationary, so that <math|F<rsub|<text|fric>>> is zero.
  According to <index|Newton's second law of motion>Newton's second law of
  motion, the net force <math|F<rsub|<text|net>>> is also zero, because
  otherwise the piston would be accelerating. Then, from Eqs.
  <reference|F(gas)=p(b)A(s)> and <reference|F(net)=F(gas)-F(ext)+F(fric)>,
  the external force needed to keep the piston from moving is
  <math|F<rsub|<text|ext>>=F<rsub|<text|gas>>=p<rsub|1>*A<rsub|<text|s>>>.

  To avoid complications of heat transfer, we confine our attention to a
  system with an adiabatic boundary. By reducing <math|F<rsub|<text|ext>>>
  from its initial value of <math|p<rsub|1>*A<rsub|<text|s>>>, we cause
  spontaneous expansion to begin. As the piston moves to the right, the
  pressure <math|p<rsub|<text|b>>> exerted on the left face of the piston
  becomes slightly <em|less> than the pressure on the stationary cylinder
  wall. The molecular explanation of this pressure gradient is that gas
  molecules moving to the right approach the moving piston at lower
  velocities relative to the piston than if the piston were stationary, so
  that they collide with the piston less frequently and with a smaller loss
  of momentum in each collision. The temperature and pressure within the gas
  become nonuniform, and we cannot describe intermediate states of this
  spontaneous process with single values of <math|T> and <math|p>. These
  intermediate states are not equilibrium states.

  The more slowly we allow the adiabatic expansion to take place, the more
  nearly uniform are the temperature and pressure. In the limit of infinite
  slowness, the gas passes through a continuous sequence of equilibrium
  states of uniform temperature and pressure.

  Let <math|p<rsub|2>> be the pressure in the final state of the
  infinitely-slow expansion. In this state, <math|F<rsub|<text|ext>>> is
  equal to <math|p<rsub|2>*A<rsub|<text|s>>>. By <em|increasing>
  <math|F<rsub|<text|ext>>> from this value, we cause spontaneous compression
  to begin. The gas pressure <math|p<rsub|<text|b>>> at the piston now
  becomes slightly <em|greater> than at the stationary cylinder wall, because
  the piston is moving to the left toward the molecules that are moving to
  the right. A different pressure gradient develops than during expansion.
  The states approached in the limit as we carry out the compression more and
  more slowly are equilibrium states, occurring in the reverse sequence of
  the states for expansion at infinite slowness. The sequence of equilibrium
  states, taken in either direction, is a <subindex|Reversible|expansion and
  compression><em|reversible> process.

  <\quote-env>
    <label|3-p at moving piston>

    The magnitude of the effect of piston velocity on <math|p<rsub|<text|b>>>
    can be estimated with the help of the kinetic-molecular theory of gases.
    This theory, of course, is not part of classical macroscopic
    thermodynamics.

    Consider the collision of a gas molecule of mass <math|m> with the left
    face of the piston shown in Fig. <reference|fig:3-cylinder>. Assume the
    piston moves at a constant velocity <math|u=<frac||><dvar|x<rsub|<text|pis>>>/<dvar|t>>,
    positive for expansion of the gas and negative for compression.

    Let <math|x> be the horizontal distance of the molecule from the left end
    of the cylinder, and <math|v<rsub|x>> be the component of its velocity in
    the <math|+x> direction measured in the cylinder-fixed lab frame:
    <math|v<rsub|x>=<dvar|x>/<dvar|t>>. Let <math|v<rprime|'><rsub|x>> be the
    component of its velocity in the <math|+x> direction measured in a
    reference frame moving with the piston:
    <math|v<rprime|'><rsub|x>=v<rsub|x>-u>.

    In one cycle of the molecule's motion, the molecule starts at the left
    end of the cylinder at time <math|t<rsub|1>>, moves to the right with
    velocity <math|v<rsub|x,1>\<gtr\>0>, collides with and is reflected from
    the piston face, moves to the left with velocity
    <math|v<rsub|x,2>\<less\>0>, and finally collides with the left end at
    time <math|t<rsub|2>>. In the piston-fixed frame, the collision with the
    piston changes the sign but not the magnitude of
    <math|v<rprime|'><rsub|x>>: <math|v<rprime|'><rsub|x,2>=-v<rprime|'><rsub|x,1>>.
    Consequently, the relation between the velocity components in the lab
    frame after and before the collision with the piston is

    <\eqnarray*>
      <tformat|<table|<row|<cell|v<rsub|x,2>-u>|<cell|=>|<cell|-<around|(|v<rsub|x,1>-u|)>>>|<row|<cell|v<rsub|x,2>>|<cell|=>|<cell|-v<rsub|x,1>+2*u<eq-number><label|vx2=-vx1+2u>>>>>
    </eqnarray*>

    At each instant during the collision itself, the interaction of the
    piston face with the gas molecule changes <math|v<rsub|x>>. From Newton's
    second law, the force exerted on the molecule equals its mass times its
    acceleration. From Newton's third law, the force <math|F<rsub|x>> exerted
    by the molecule on the piston has the same magnitude and opposite sign of
    the force exerted on the molecule: <math|F<rsub|x>=-m*<dvar|v<rsub|x>>/<dvar|t>>.
    <math|F<rsub|x>> is zero at times before and after the collision.
    Rearrangement to <math|F<rsub|x>*<dvar|t>=-m*<dvar|v<rsub|x>>> and
    integration over the time interval of the cycle yields

    <\equation>
      <label|int F_x=><big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><space|-0.17em><space|-0.17em>F<rsub|x>*<dvar|t>=-m*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><dvar|v<rsub|x>>=-m*<around|(|v<rsub|x,2>-v<rsub|x,1>|)>
    </equation>

    Then from the relation of Eq. <reference|vx2=-vx1+2u>,
    <math|<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><space|-0.17em>F<rsub|x>*<dvar|t>>
    equals <math|2*m*<around|(|v<rsub|x,1>-u|)>>.

    The time average <math|<around*|\<langle\>|F<rsub|x>|\<rangle\>>> of
    <math|F<rsub|x>> over the interval of the cycle is

    <\equation>
      <label|\<less\>Fx\<gtr\>=><around*|\<langle\>|F<rsub|x>|\<rangle\>>=<frac|1|<around|(|t<rsub|2>-t<rsub|1>|)>>*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>>F<rsub|x>*<dvar|t>=<frac|2*m*<around*|(|v<rsub|x,1>-u|)>|<around|(|t<rsub|2>-t<rsub|1>|)>>
    </equation>

    An expression for <math|t<rsub|2>-t<rsub|1>> as a function of
    <math|v<rsub|x,1>> and <math|u> can be derived using
    <math|\<Delta\>*t=\<Delta\>*x/v<rsub|x>>:

    <\equation>
      <label|t2-t1=>t<rsub|2>-t<rsub|1>=<frac|l|v<rsub|x,1>>+<frac|-l|v<rsub|x,2>>=<frac|l|v<rsub|x,1>>+<frac|-l|-v<rsub|x,1>+2*u>=<frac|2*l*<around|(|v<rsub|x,1>-u|)>|v<rsub|x,1><rsup|2>-2*u*v<rsub|x,1>>
    </equation>

    Here <math|l> is the interior length of the cylinder at the time the
    molecule collides with the piston.

    From Eqs. <reference|\<less\>Fx\<gtr\>=> and <reference|t2-t1=>, the time
    average during the cycle of the force exerted by the gas molecule on the
    piston is

    <\equation>
      <label|\<less\>F_x\<gtr\>><around*|\<langle\>|F<rsub|x>|\<rangle\>>=<frac|2*m*<around*|(|v<rsub|x,1>-u|)>*<around*|(|v<rsub|x,1><rsup|2>-2*u*v<rsub|x,1>|)>|2*l*<around*|(|v<rsub|x,1>-u|)>>=<frac|m|l>*<around*|(|v<rsub|x,1><rsup|2>-2*u*v<rsub|x,1>|)>
    </equation>

    The gas consists of <math|n*M/m> molecules, where <math|n> is the amount
    and <math|M> is the molar mass. There is a range of values of
    <math|v<rsub|x,1>>. The total pressure <math|p<rsub|<text|b>>> exerted by
    the gas on the piston is found by summing
    <math|<around*|\<langle\>|F<rsub|x>|\<rangle\>>> over all molecules and
    dividing by the piston area <math|A<rsub|<text|s>>>:

    <\equation>
      p<rsub|<text|b>>=<around*|(|<frac|1|A<rsub|<text|s>>>|)><around*|(|<frac|n*M|m>|)><around*|(|<frac|m|l>|)>*<around*|(|<around*|\<langle\>|v<rsub|x,1><rsup|2>|\<rangle\>>-2*u<around*|\<langle\>|v<rsub|x,1>|\<rangle\>>|)>
    </equation>

    The pressure <math|p> at the stationary cylinder wall is found by setting
    <math|u> equal to zero in the expression for <math|p<rsub|<text|b>>>.
    Thus <math|p<rsub|<text|b>>> is related to <math|p> by<footnote|A formula
    similar to this to the first order in <math|u> is given in Ref.
    <cite|bertrand-05>. A formula that yields similar values of
    <math|p<rsub|<text|b>>> appears in Ref. <cite|bauman-69>, Eq. 7.>

    <\equation>
      <label|p(b)=p(...)>p<rsub|<text|b>>=p*<around*|(|1-2*u*<frac|<around*|\<langle\>|v<rsub|x,1>|\<rangle\>>|<around*|\<langle\>|v<rsub|x,1><rsup|2>|\<rangle\>>>|)>
    </equation>

    From kinetic-molecular theory, the averages are given by
    <math|<around*|\<langle\>|v<rsub|x,1>|\<rangle\>>=<around|(|2*R*T/\<pi\>*M|)><rsup|1/2>>
    and <math|<around*|\<langle\>|v<rsub|x,1><rsup|2>|\<rangle\>>=R*T/M>.
    Suppose the piston moves at the considerable speed of <math|10
    <frac|<text|m>|<text|s>>> and the gas in the cylinder is nitrogen
    (N<rsub|<math|2>>) at <math|300 <text|K>>; then Eq.
    <reference|p(b)=p(...)> predicts the pressure <math|p<rsub|<text|b>>>
    exerted on the piston by the gas during expansion is only about <math|5%>
    lower than the pressure <math|p> at the stationary wall, and during
    compression about <math|5%> higher. At low piston speeds the percentage
    difference is proportional to the piston speed, so this example shows
    that for reasonably slow speeds the difference is quite small and for
    practical calculations can usually be ignored.
  </quote-env>

  <subsection|Expansion work of a gas><label|3-expansion work><label|c3 sec
  dw-expansion-gas>

  <index-complex|<tuple|expansion|work>||c3 sec dw-expansion-gas
  idx1|<tuple|Expansion|work>><index-complex|<tuple|work|expansion>||c3 sec
  dw-expansion-gas idx2|<tuple|Work|expansion>>We now consider the work
  involved in expansion and compression of the gas in the cylinder-and-piston
  device of Fig. <reference|fig:3-cylinder>. This kind of deformation work,
  for both expansion and compression, is called
  <subindex|Expansion|work><subindex|Work|expansion><newterm|expansion work>
  or pressure-volume work.

  Keep in mind that the <em|system> is just the gas. The only moving portion
  of the boundary of this system is at the inner surface of the piston, and
  this motion is in the <math|+x> or <math|-x> direction. The <math|x>
  component of the force exerted by the system on the surroundings at this
  portion of the boundary, <math|F<rsup|<text|sys>><rsub|x>>, is equal to
  <math|F<rsub|<text|gas>>>. (The other forces shown in Fig.
  <reference|fig:3-cylinder> are within the surroundings.) Applying the
  differential form of Eq. <reference|dw=-Fx(sys)dx w=>, we have
  <math|<dbar|w>=-F<rsub|<text|gas>>*<dvar|x<rsub|<text|pis>>>> which, with
  the substitution <math|F<rsub|<text|gas>>=p<rsub|<text|b>>*A<rsub|<text|s>>>
  (from Eq. <reference|F(gas)=p(b)A(s)>), becomes

  <\equation>
    <label|dw=-p(b)Adx><dbar|w>=-p<rsub|<text|b>>*A<rsub|<text|s>>*<dvar|x<rsub|<text|pis>>>
  </equation>

  It will be convenient to change the work coordinate from
  <math|x<rsub|<text|pis>>> to <math|V>. The gas volume is given by
  <math|V=A<rsub|<text|s>>*x<rsub|<text|pis>>> so that an infinitesimal
  change <dvar|x> changes the volume by <math|<dvar|V>=A<rsub|<text|s>>*<dvar|x<rsub|<text|pis>>>>.
  The infinitesimal quantity of work for an infinitesimal volume change is
  then given by

  <equation-cov2|<label|dw=-p(b)dV><dbar|w=-p<rsub|<text|b>>*<dvar|V>>|(expansion
  work, closed system)>

  and the finite work for a finite volume change, found by integrating from
  the initial to the final volume, is

  <equation-cov2|<label|w=-int(pb)dV>w=-<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>>p<rsub|<text|b>>*<dvar|V>|(expansion
  work, closed system)>

  During expansion (positive <dvar|*V>), <dbar|w> is negative and the system
  does work on the surroundings. During compression (negative
  <math|<dvar|V>>), <math|<dbar|w>> is positive and the surroundings do work
  on the system.

  <\quote-env>
    When carrying out dimensional analysis, you will find it helpful to
    remember that the product of two quantities with dimensions of pressure
    and volume (such as <math|p<rsub|<text|b>>*<dvar|V>>) has dimensions of
    energy, and that <math|1 <text|Pa>*\<cdot\><text|m><rsup|3>> is equal to
    <math|1 <text|J>>.
  </quote-env>

  The integral on the right side of Eq. <reference|w=-int(pb)dV> is a
  <index|Line integral><subindex|Integral|line><em|line integral> (Sec.
  <vpageref|app-line integrals>). In order to evaluate the integral, one must
  be able to express the integrand <math|p<rsub|<text|b>>> as a function of
  the integration variable <math|V> along the path of the expansion or
  compression process.

  If the piston motion during expansion or compression is sufficiently slow,
  we can with little error assume that the gas has a uniform pressure
  <math|p> throughout, and that the work can be calculated as if the process
  has reached its reversible limit. Under these conditions, Eq.
  <reference|dw=-p(b)dV> becomes

  <\equation-cov2|<label|dw=-pdV><dbar|w>=-p*<dvar|V>>
    (reversible expansion

    work, closed system)
  </equation-cov2>

  \;

  and Eq. <reference|w=-int(pb)dV> becomes

  <\equation-cov2|<label|w=-int(p)dV>w=-<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>>p*<dvar|V>>
    (reversible expansion

    work, closed system)
  </equation-cov2>

  The appearance of the symbol <math|p> in these equations, instead of
  <math|p<rsub|<text|b>>>, implies that the equations apply only to a process
  in which the system has at each instant a single uniform pressure. As a
  general rule,<label|uniformity rule><em|an equation containing the symbol
  of an intensive property not assigned to a specific phase is valid only if
  that property is uniform throughout the system>, and this will not be
  explicitly indicated as a condition of validity.

  <\quote-env>
    \ Some texts state that expansion work in a horizontal
    cylinder-and-piston device like that shown in Fig.
    <reference|fig:3-cylinder> should be calculated from
    <math|w=-<big|int>p<rsub|<text|ext>>*<dvar|V>>, where
    <math|p<rsub|<text|ext>>> is a pressure in the <em|surroundings> that
    exerts the external force <math|F<rsub|<text|ext>>> on the piston.
    However, if the system is the gas the correct general expression is the
    one given by Eq. <reference|w=-int(pb)dV>:
    <math|w=-<big|int>p<rsub|<text|b>>*<dvar|V>>. This is because it is the
    force <math|F<rsub|<text|gas>>=p<rsub|<text|b>>*A<rsub|<text|s>>> that is
    exerted by the system on the surroundings, whereas the force
    <math|F<rsub|<text|ext>>=p<rsub|<text|ext>>*A<rsub|<text|s>>> is exerted
    by one part of the surroundings on another part of the surroundings.

    In other words, if the integrals <math|<big|int>F<rsub|<text|gas>>*<dvar|x<rsub|<text|pis>>>>
    and <math|<big|int>F<rsub|<text|ext>>*<dvar|x<rsub|<text|pis>>>> have
    different values, it is the first of these two integrals that should be
    used to evaluate the work: <math|w=-<big|int><space|-0.17em><space|-0.17em>F<rsub|<text|gas>>*<dvar|x<rsub|<text|pis>>>>.
    Both integrals are equal if the expansion or compression process is
    carried out <em|reversibly>. This is because in the limit of infinite
    slowness the piston has neither friction (<math|F<rsub|<text|fric>>=0>)
    nor acceleration (<math|F<rsub|<text|net>>=0>), and therefore according
    to Eq. <reference|F(net)=F(gas)-F(ext)+F(fric)>,
    <math|F<rsub|<text|gas>>> and <math|F<rsub|<text|ext>>> are equal
    throughout the process. Another situation in which the two integrals are
    equal is when the piston is frictionless and is stationary in the initial
    and final states, because then both <math|F<rsub|<text|fric>>> and
    <math|<big|int>F<rsub|<text|net>>*<dvar|x<rsub|<text|pis>>>> are zero.
    (The integral <math|<big|int>F<rsub|<text|net>>*<dvar|x<rsub|<text|pis>>>>
    can be shown to be equal to the change in the kinetic energy of the
    piston, by a derivation similar to that leading to Eq.
    <vpageref|W(tot)=Del(KE)>.) In the general irreversible case, however,
    the integrals <math|<big|int>F<rsub|<text|gas>>*<dvar|x<rsub|<text|pis>>>>
    and <math|<big|int>F<rsub|<text|ext>>*<dvar|x<rsub|<text|pis>>>> are
    <em|not> equal.<footnote|For an informative discussion of this topic see
    Ref. <cite|bauman-64>; also comments in Refs. <cite|chesick-64>,
    <cite|bauman-64a>, <cite|kokes-64>, <cite|bauman-64b>, and
    <cite|mysels-64>; also Ref. <cite|kivelson-66>.>
  </quote-env>

  <subsection|Expansion work of an isotropic phase><label|c3
  sec-dw-expansion-isotropic>

  Expansion work does not require a cylinder-and-piston device. Suppose the
  system is an isotropic fluid or solid phase, and various portions of its
  boundary undergo displacements in different directions. Figure
  <vpageref|fig:3-deformation> shows an example of compression in a system of
  arbitrary shape. The deformation is considered to be carried out slowly, so
  that the pressure <math|p> of the phase remains uniform. Consider the
  surface element <math|\<tau\>> of the boundary, with area
  <math|A<rsub|<text|s>>,\<tau\>>, indicated in the figure by a short thick
  curve. Because the phase is isotropic, the force
  <math|F<rsup|<text|sys>><rsub|\<tau\>>=p*A<rsub|<text|s>,\<tau\>>> exerted
  by the system pressure on the surroundings is perpendicular to this surface
  element; that is, there is no shearing force. The force
  <math|F<rsup|<text|sur>><rsub|\<tau\>>> exerted by the surroundings on the
  system is equal in magnitude to <math|F<rsup|<text|sys>><rsub|\<tau\>>> and
  is directed in the opposite direction. The volume change for an
  infinitesimal displacement <dvar|s<rsub|\<tau\>>><math|> that reduces the
  volume is <math|<dvar|V<rsub|\<tau\>>>=-A<rsub|<text|s>,\<tau\>>*<dvar|s<rsub|\<tau\>>>>,
  so that the work at this surface element (from Eq.
  <reference|dw(tau)=F(sur)cos(alpha)ds> with
  <math|\<alpha\><rsub|\<tau\>>=0>) is <math|<dbar|w<rsub|\<tau\>>>=-p*<dvar|V<rsub|\<tau\>>>>.

  By summing the work over the entire boundary, we find the total reversible
  expansion work is given by the same expression as for a gas in a
  piston-and-cylinder device: <math|<dbar|w>=-p*<dvar|V>>. This expression
  can be used for deformation caused by reversible displacements of a
  confining wall, or for a volume change caused by slow temperature changes
  at constant pressure. It is valid if the system is an isotropic fluid phase
  in which other phases are immersed, provided the fluid phase contacts all
  parts of the system boundary. The expression is not necessarily valid for
  an <index|Anisotropic phase><subindex|Phase|anisotropic><em|anisotropic>
  fluid or solid, because the angle <math|\<alpha\><rsub|\<tau\>>> appearing
  in Eq. <reference|dw(tau)=F(sur)cos(alpha)ds> might not be
  zero.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/deformation.eps|248pt|104pt||>>
        <label|fig:3-deformation>Deformation of an isotropic phase (shaded)
        confined by a wall.

        \ (a)<nbsp>Equal and opposite forces exerted by the surroundings and
        system at surface element <math|\<tau\>> (thick curve) of the system
        boundary.

        \ (b)<nbsp>Change from initial volume (dotted curve) to a smaller
        volume.
      </big-figure>
    </framed>
  </float>

  \;

  <subsection|Generalities><label|c3 sec dw-generalities>

  The expression <math|<dbar|w>=-p*<dvar|V>> for reversible expansion work of
  an isotropic phase is the product of a work coefficient, <math|-p>, and the
  infinitesimal change of a work coordinate, <math|V>. In the reversible
  limit, in which all states along the path of the process are equilibrium
  states, the system has two independent variables, e.g., <math|p> and
  <math|V> or <math|T> and <math|V>. The number of independent variables is
  one greater than the number of work coordinates. This will turn out to be a
  general rule:<label|ind var rule> <em|The number of independent variables
  needed to describe equilibrium states of a closed system is one greater
  than the number of independent work coordinates for reversible work>.

  Another way to state the rule is as follows: The number of independent
  variables is one greater than the number of different <em|kinds> of
  reversible work, where each kind <math|i> is given by an expression of the
  form <math|<dbar|w<rsub|i>>=Y<rsub|i>*<dvar|X<rsub|i>>>.

  <index-complex|<tuple|work|deformation>||c3 sec dw
  idx1|<tuple|Work|deformation>><index-complex|<tuple|deformation|work>||c3
  sec dw idx2|<tuple|Deformation|work>>

  <section|Applications of Expansion Work><label|3-applications of exp
  work><label|c3 sec aew>

  This book uses <em|expansion work> as a general term that includes the work
  of both expansion and compression of an isotropic phase.

  <subsection|The internal energy of an ideal gas><label|3-U of ideal
  gas><label|c3 sec aew-ideal-u>

  <index-complex|<tuple|internal energy|ideal gas>|||<tuple|Internal
  energy|of an ideal gas>>The model of an ideal gas is used in many places in
  the development of thermodynamics. For examples to follow, the following
  definition is needed: <index|Ideal gas><subindex|Gas|ideal>An ideal gas is
  a gas

  <\enumerate-numeric>
    <item>whose equation of state is the <subindex|Ideal gas|equation>ideal
    gas equation, <math|p*V=n*R*T>; and

    <item>whose internal energy in a closed system is a function only of
    temperature.<footnote|A gas with this second property is sometimes called
    a <subindex|Gas|perfect>\Pperfect gas\Q. In Sec. <reference|7-int
    pressure> it will be shown that if a gas has the first property, it must
    also have the second.>
  </enumerate-numeric>

  On the molecular level, a gas with negligible intermolecular
  interactions<footnote|This book uses the terms \Pintermolecular
  interactions\Q and \Pintermolecular forces\Q for interactions or forces
  between either multi-atom molecules or unbonded atoms.> fulfills both of
  these requirements. Kinetic-molecular theory predicts that a gas containing
  noninteracting molecules obeys the ideal gas equation. If intermolecular
  forces (the only forces that depend on intermolecular distance) are
  negligible, the internal energy is simply the sum of the energies of the
  individual molecules. These energies are independent of volume but depend
  on temperature.

  The behavior of any real gas approaches ideal-gas behavior when the gas is
  expanded isothermally. As the molar volume <math|V<rsub|<text|m>>> becomes
  large and <math|p> becomes small, the average distance between molecules
  becomes large, and intermolecular forces become negligible.

  <subsection|Reversible isothermal expansion of an ideal gas><label|3-rev
  isothermal exp><label|c3 sec-aew-ideal-isothermal>

  <subindex|Reversible|isothermal expansion of an ideal gas>During reversible
  expansion or compression, the temperature and pressure remain uniform. If
  we substitute <math|p=n*R*T/V> from the ideal gas equation into Eq.
  <reference|w=-int(p)dV> and treat <math|n> and <math|T> as constants, we
  obtain

  <\equation-cov2|<label|w=-nRT ln(V2/V1)>w=-n*R*T*<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>><frac|<dvar|V>|V>=-n*R*T*<text|ln>
  <frac|V<rsub|2>|V<rsub|1>>>
    (reversible isothermal

    expansion work, ideal gas)
  </equation-cov2>

  In these expressions for <math|w> the amount <math|n> appears as a constant
  for the process, so it is not necessary to state as a condition of validity
  that the system is closed.

  <subsection|Reversible adiabatic expansion of an ideal gas><label|3-rev
  adiab expan><label|c3 sec-aew-ideal-adiabatic>

  <subindex|Reversible|adiabatic expansion of an ideal gas>This section
  derives temperature-volume and pressure-volume relations when a fixed
  amount of an ideal gas is expanded or compressed without heat.

  First we need a relation between internal energy and temperature. Since the
  value of the internal energy of a fixed amount of an ideal gas depends only
  on its temperature (Sec. <reference|3-U of ideal gas>), an infinitesimal
  change <dvar|T> will cause a change <math|<dvar|U>> that depends only on
  <math|T> and <dvar|T>:

  <\equation>
    <dvar|U>=f<around|(|T|)>*<dvar|T>
  </equation>

  where <math|f<around|(|T|)>=<dvar|U>/<dvar|T>> is a function of <math|T>.
  For a constant-volume process of a closed system without work, we know from
  the first law that <math|<dvar|U>> is equal to <math|<dbar|q>> and that
  <math|<dbar|q>/<dbar|T>> is equal to <math|C<rsub|V>>, the heat capacity at
  constant volume (Sec. <reference|3-heat capacity>). Thus we can identify
  the function <math|f<around|(|T|)>> as the <index-complex|<tuple|heat
  capacity|constant volume|ideal gas>|||<tuple|Heat capacity|at constant
  volume|of an ideal gas>>heat capacity at constant volume:

  <equation-cov2|<label|dU=C_V dT (ig)><dvar|U>=C<rsub|V>*<dvar|T>|(ideal
  gas, closed system)>

  The relation given by Eq. <reference|dU=C_V dT (ig)> is valid for any
  process of a closed system of an ideal gas of uniform temperature, even if
  the volume is not constant or if the process is adiabatic, because it is a
  general relation between state functions.

  In a reversible adiabatic expansion with expansion work only, the heat is
  zero and the first law becomes

  <\equation>
    <dvar|U>=<dbar|w>=-p<dvar|V>
  </equation>

  We equate these two expressions for <math|<dvar|U>> to obtain

  <\equation>
    C<rsub|V>*<dvar|T>=-p*<dvar|V>
  </equation>

  and substitute <math|p=n*R*T/V> from the ideal gas equation:

  <\equation>
    C<rsub|V>*<dvar|T>=-<frac|n*R*T|V>*<dvar|V>
  </equation>

  It is convenient to make the approximation that over a small temperature
  range, <math|C<rsub|V>> is constant. When we divide both sides of the
  preceding equation by <math|T> in order to separate the variables <math|T>
  and <math|V>, and then integrate between the initial and final states, we
  obtain

  <\equation>
    C<rsub|V>*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|<dvar|T>|T>=-n*R*<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>><frac|<dvar|V>|V>
  </equation>

  <\equation>
    C<rsub|V>*ln <frac|T<rsub|2>|T<rsub|1>>=-n*R*ln
    <frac|V<rsub|2>|V<rsub|1>>
  </equation>

  We can rearrange this result into the form

  <\equation>
    ln <frac|T<rsub|2>|T<rsub|1>>=-<frac|n*R|C<rsub|V>>*ln
    <frac|V<rsub|2>|V<rsub|1>>=ln <around*|(|<frac|V<rsub|1>|V<rsub|2>>|)><rsup|n*R/C<rsub|V>>
  </equation>

  and take the exponential of both sides:

  <\equation>
    <frac|T<rsub|2>|T<rsub|1>>=<around*|(|<frac|V<rsub|1>|V<rsub|2>>|)><rsup|n*R/C<rsub|V>>
  </equation>

  The final <em|temperature> is then given as a function of the initial and
  final volumes by

  <\equation-cov2|<label|T2= (ad rev expan of id
  gas)>T<rsub|2>=T<rsub|1>*<around*|(|<frac|V<rsub|1>|V<rsub|2>>|)><rsup|n*R/C<rsub|V>>>
    (reversible adiabatic

    expansion, ideal gas)
  </equation-cov2>

  This relation shows that the temperature decreases during an adiabatic
  expansion and increases during an adiabatic compression, as expected from
  expansion work on the internal energy.

  To find the <em|work> during the adiabatic volume change, we can use the
  relation

  <\eqnarray*>
    <tformat|<table|<row|<cell|w>|<cell|=>|<cell|\<Delta\>*U=<big|int><dvar|U>=C<rsub|V>*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><dvar|T>>>|<row|<cell|>|<cell|=>|<cell|C<rsub|V>*<around*|(|T<rsub|2>-T<rsub|1>|)><htab|5mm><tabular|<tformat|<cwith|2|2|1|1|cell-hyphen|t>|<table|<row|<cell|<next-number>>>|<row|<\cell>
      <\text>
        (reversible adiabatic

        expansion, ideal gas)
      </text>
    </cell>>>>>>>>>
  </eqnarray*>

  \;

  To express the final <em|pressure> as a function of the initial and final
  volumes, we make the substitutions <math|T<rsub|1>=p<rsub|1>*V<rsub|1>/n*R>
  and <math|T<rsub|2>=p<rsub|2>*V<rsub|2>/n*R> in Eq. <reference|T2= (ad rev
  expan of id gas)> and obtain

  <\equation>
    <frac|p<rsub|2>*V<rsub|2>|n*R>=<frac|p<rsub|1>*V<rsub|1>|n*R><around*|(|<frac|V<rsub|1>|V<rsub|2>>|)><rsup|n*R/C<rsub|V>>
  </equation>

  Solving this equation for <math|p<rsub|2>>, we obtain finally

  <\equation-cov2|<label|p2= (ad rev expan of id
  gas)>p<rsub|2>=p<rsub|1>*<around*|(|<frac|V<rsub|1>|V<rsub|2>>|)><rsup|1+<frac|n*R|C<rsub|V>>>>
    (reversible adiabatic

    expansion, ideal gas)
  </equation-cov2>

  The solid curve in Fig. <vpageref|fig:3-adiabat and isotherms> shows how
  the pressure of an ideal gas varies with volume during a reversible
  adiabatic expansion or compression. This curve is an
  <index|Adiabat><em|adiabat>. The dashed curves in the figure are
  <index|Isotherm><em|isotherms> showing how pressure changes with volume at
  constant temperature according to the equation of state <math|p=n*R*T/V>.
  In the direction of increasing <math|V> (expansion), the adiabat crosses
  isotherms of progressively lower temperatures. This cooling effect, of
  course, is due to the loss of energy by the gas as it does work on the
  surroundings without a compensating flow of heat into the
  system.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/adiabats-1.eps|208pt|200pt||>>
        <label|fig:3-adiabat and isotherms>An adiabat (solid curve) and four
        isotherms (dashed curves) for an ideal gas (<math|n=0.0120
        <text|mol>>, <math|C<rsub|V,<text|m>>=1.5*R>).
      </big-figure>
    </framed>
  </float>

  <subsection|Indicator diagrams><label|3-indicator diagrams><label|c3 sec
  aew-indicator>

  An <index|Indicator diagram><newterm|indicator diagram> (or
  <index-complex|<tuple|pressure volume diagram>|||<tuple|Pressure\Uvolume
  diagram>>pressure\Uvolume diagram) is usually a plot of <math|p> as a
  function of <math|V>. The curve describes the path of an expansion or
  compression process of a fluid that is essentially uniform. The area under
  the curve has the same value as the integral <math|<big|int>p*<dvar|V>>,
  which is the negative of the <subsubindex|Expansion|work|reversible><subindex|Work|reversible
  expansion><subindex|Reversible|expansion work>reversible expansion work
  given by <math|w=-<big|int>p*<dvar|V>>. For example, the area under the
  solid curve of Fig. <reference|fig:3-adiabat and isotherms> between any two
  points on the curve is equal to <math|-w> for reversible adiabatic
  expansion or compression. If the direction of the process is to the right
  along the path (expansion), the area is positive and the work is negative;
  but if the direction is to the left (compression), the area is taken as
  negative and the work is positive.

  More generally, an indicator diagram can be a plot of a work coefficient or
  its negative as a function of the work coordinate. For example, it could be
  a plot of the pressure <math|p<rsub|<text|b>>> at a moving boundary as a
  function of <math|V>. The area under this curve is equal to
  <math|<big|int>p<rsub|<text|b>>*<dvar|V>>, the negative of expansion work
  in general (Eq. <reference|w=-int(pb)dV>).<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/indicator.eps|227pt|200pt||>>
        <label|fig:3-indicator>Indicator with paper-covered roll at left and
        pressure gauge at right.<space|.15em><footnote|Ref.
        <cite|maxwell-1888>, page 104.>
      </big-figure>
    </framed>
  </float>

  <\quote-env>
    Historically, an indicator diagram was a diagram drawn by an
    ``indicator,'' an instrument invented by James Watt in the late 1700s to
    monitor the performance of steam engines. The steam engine indicator was
    a simple pressure gauge: a piston moving in a small secondary cylinder,
    with the steam pressure of the main cylinder on one side of the piston
    and a compressed spring opposing this pressure on the other side. A
    pointer attached to the small piston indicated the steam pressure. In
    later versions, the pointer was replaced with a pencil moving along a
    paper-covered roll, which in turn was mechanically linked to the piston
    of the main cylinder (see Fig. <vpageref|fig:3-indicator>). During each
    cycle of the engine, the pencil moved back and forth along the length of
    the roll and the roll rotated in a reciprocating motion, causing the
    pencil to trace a closed curve whose area was proportional to the net
    work performed by one cycle of the engine.
  </quote-env>

  <subsection|Spontaneous adiabatic expansion or compression><label|3-spont
  ad exp><label|c3 sec-aew-spontaneous-adiabatic>

  Section <reference|3-expansion> explained that during a rapid spontaneous
  expansion of the gas in the cylinder shown in Fig.
  <reference|fig:3-cylinder>, the pressure <math|p<rsub|<text|b>>> exerted by
  the gas at the moving piston is less than the pressure at the stationary
  wall. Consequently the work given by <math|w=-<big|int>p<rsub|<text|b>><dvar|V>>
  is less negative for a spontaneous adiabatic expansion than for a
  reversible adiabatic expansion with the same initial state and the same
  volume change.

  During a rapid spontaneous <em|compression>, <math|p<rsub|<text|b>>> is
  greater than the pressure at the stationary wall. The work is positive and
  greater for a spontaneous adiabatic compression than for a reversible
  adiabatic compression with the same initial state and the same volume
  change.

  These observations are summarized by the statement that, for an adiabatic
  expansion or compression with a given change of the work coordinate,
  starting at a given initial equilibrium state, the work is algebraically
  smallest (least positive or most negative) in the reversible limit. That
  is, in the reversible limit the surroundings do the least possible work on
  the system and the system does the maximum possible work on the
  surroundings. This behavior will turn out to be true of any adiabatic
  process of a closed system.

  <subsection|Free expansion of a gas into a vacuum><label|3-free
  expansion><label|c3 sec aew-vacuum>

  When we open the stopcock of the apparatus shown in Fig.
  <vpageref|fig:3-free expansion>, the gas expands from the vessel at the
  left into the evacuated vessel at the right. This process is called
  <index|Free expansion><subindex|Expansion|free><newterm|free expansion>.
  The <em|system> is the gas. The surroundings exert a contact force on the
  system only at the vessel walls, where there is no displacement. Thus,
  there is <em|no> work in free expansion: <math|<dbar|w>=0>.

  If the free expansion is carried out adiabatically in a thermally-insulated
  apparatus, there is neither heat nor work and therefore no change in the
  internal energy: <math|\<Delta\>*U=0>. If the gas is ideal, its internal
  energy depends only on temperature; thus the adiabatic free expansion of an
  ideal gas causes no temperature change.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/free-exp.eps|279pt|40pt||>>
        <label|fig:3-free expansion>Free expansion into a vacuum.
      </big-figure>
    </framed>
  </float>

  <index-complex|<tuple|expansion|work>||c3 sec dw-expansion-gas
  idx1|<tuple|Expansion|work>><index-complex|<tuple|work|expansion>||c3 sec
  dw-expansion-gas idx2|<tuple|Work|expansion>>

  <section|Work in a Gravitational Field><label|3-gravitational
  field><label|c3 sec wgf>

  <index-complex|<tuple|work|gravitational>||c3 sec wgf
  idx1|<tuple|Work|gravitational>><index-complex|<tuple|gravitational|work>||c3
  sec wgf idx2|<tuple|Gravitational|work>>Figure <vpageref|fig:3-grav work>
  depicts a spherical body, such as a glass marble, immersed in a liquid or
  gas in the presence of an external gravitational field. The vessel
  containing the fluid is stationary on a lab bench, and the <index|Local
  frame><subindex|Frame|local>local reference frame for work is a stationary
  <index|Lab frame><subindex|Frame|lab>lab frame. The variable <math|z> is
  the body's elevation above the bottom of the vessel. All displacements are
  parallel to the vertical <math|z> axis. From Eq. <reference|dw=F(sur)dx
  w=>, the work is given by <math|<dbar|w>=F<rsup|<text|sur>><rsub|z>*<dvar|z>>
  where <math|F<rsup|<text|sur>><rsub|z>> is the upward component of the net
  contact force exerted by the surroundings on the system at the moving
  portion of the boundary. There is also a downward gravitational force on
  the body, but as explained in Sec. <reference|3-thermo work>, this force
  does not contribute to <math|F<rsup|<text|sur>><rsub|z>>.

  <\big-figure|<image|03-SUP/GRAVWORK.eps|186pt|129pt||>>
    <label|fig:3-grav work>Spherical body (dark gray) in a gravitational
    field. The arrows indicate the directions and magnitudes of contact and
    gravitational forces exerted on the body.

    \ (a)<nbsp>The body falls freely through a fluid.

    \ (b)<nbsp>The body is lowered on a string through the fluid.
  </big-figure>

  Consider first the simple process in Fig. <reference|fig:3-grav work>(a) in
  which the body falls freely through the fluid. This process is clearly
  spontaneous. Here are two choices for the definition of the system:

  <\itemize>
    <item>The system is the combination of the spherical body and the fluid.
    The system boundary is where the fluid contacts the atmosphere and the
    vessel walls. Because there is no displacement of this boundary, <em|no>
    work is being done on or by the system: <math|<dbar|w>=0>. (We ignore
    expansion work caused by the small temperature increase.) If the process
    is adiabatic, the first law tells us the system's internal energy remains
    constant: as the body loses gravitational potential energy, the system
    gains an equal quantity of kinetic and thermal energy.

    <item>The system is the body; the fluid is in the surroundings. The
    upward components of the forces exerted on the body are (1) a
    gravitational force <math|-m*g>, where <math|m> is the body's mass and
    <math|g> is the acceleration of free fall; (2) a buoyant
    force<footnote|The buoyant force is a consequence of the pressure
    gradient that exists in the fluid in a gravitational field (see Sec.
    <reference|8-gas in gravity>). We ignore this gradient when we treat the
    fluid as a uniform phase.> <math|F<rsub|<text|buoy>>=\<rho\>*V<rprime|'>*g>,
    where <math|\<rho\>> is the fluid density and <math|V<rprime|'>> is the
    volume of the body; and (3) a frictional drag force
    <math|F<rsub|<text|fric>>> of opposite sign from the velocity
    <math|v=<dvar|z>/<dvar|t>>. As mentioned above, the gravitational force
    is not included in <math|F<rsup|<text|sur>><rsub|z>>. Therefore the
    gravitational work is given by

    <\equation>
      <label|dw=(F_buoy+F_fric)dh><dbar|w>=F<rsup|<text|sur>><rsub|z>*<dvar|z>=<around*|(|F<rsub|<text|buoy>>+F<rsub|<text|fric>>|)>*<dvar|z>
    </equation>

    and is negative because <math|<dvar|z>> is negative: the body as it falls
    does work on the fluid.<label|free-falling body>The positive quantity
    <math|<around*|\||F<rsub|<text|buoy>>*<dvar|z>|\|>> is the work of moving
    displaced fluid upward, and <math|<around*|\||F<rsub|<text|fric>>*<dvar|z>|\|>>
    is the <subindex|Energy|dissipation of><index|Dissipation of
    energy>energy dissipated by friction to thermal energy in the
    surroundings. This process has no reversible limit, because the rate of
    energy transfer cannot be controlled from the surroundings and cannot be
    made to approach zero.
  </itemize>

  Next, consider the arrangement in Fig. <reference|fig:3-grav work>(b) in
  which the body is suspended by a thin string. The string is in the
  surroundings and provides a means for the surroundings to exert an upward
  contact force on the body. As before, there are two appropriate choices for
  the system:

  <\itemize>
    <item>The system includes both the body and the fluid, but not the
    string. The moving part of the boundary is at the point where the string
    is attached to the body. The force exerted here by the string is an
    upward force , and the gravitational work is given by
    <math|<dbar|w>=F<rsup|<text|sur>><rsub|z>*<dvar|z>=F<rsub|<text|str>>*<dvar|z>>.
    According to Newton's second law, the net force on the body equals the
    product of its mass and acceleration:
    <math|<around|(|-m*g+F<rsub|<text|buoy>>+F<rsub|<text|fric>>+F<rsub|<text|str>>|)>=m*<dvar|v>/<dvar|t>>.
    Solving this equation for <math|F<rsub|<text|str>>>, we obtain

    <\equation>
      <label|F_str=>F<rsub|<text|str>>=<around*|(|m*g-F<rsub|<text|buoy>>-F<rsub|<text|fric>>+m*<dvar|v>/<dvar|t>|)>
    </equation>

    We can therefore express the work in the form

    <\equation>
      <label|dw=(mg-F_buoy-F_fric+mdv/dt)dh><dbar|w>=F<rsub|<text|str>>*<dvar|z>=<around*|(|m*g-F<rsub|<text|buoy>>-F<rsub|<text|fric>>+m*<dvar|v>/<dvar|t>|)>*<dvar|z>
    </equation>

    This work can be positive or negative, depending on whether the body is
    being pulled up or lowered by the string. The quantity
    <math|<around|(|m*<dvar|v>/<dvar|t>|)>*<dvar|z>> is an infinitesimal
    change of the body's kinetic energy <math|E<rsub|<text|k>>>,<footnote|To
    prove this, we write <math|m*<around|(|<dvar|v>/<dvar|t>|)>*<dvar|z>=m*<around|(|<dvar|z>/<dvar|t>|)>*<dvar|v>=m*v*<dvar|v>=<dvar|<around*|(|<onehalf>m*v<rsup|2>|)>>=<dvar|E><rsub|<text|k>>>.>
    so that the integral <math|<big|int><around|(|m*<dvar|v>/<dvar|t>|)>*<dvar|z>>
    is equal to <math|\<Delta\>*E<rsub|<text|k>>>. The finite quantity of
    work in a process that starts and ends in equilibrium states, so that
    <math|\<Delta\>*E<rsub|<text|k>>> is zero, is therefore

    <\equation>
      <label|w=(mg-F_buoy)del h-int(F_fric)dh>w=<big|int><space|-0.17em><dbar|w>=<around*|(|m*g-F<rsub|<text|buoy>>|)>*\<Delta\>*z-<big|int>F<rsub|<text|fric>>*<dvar|z>
    </equation>

    The work has a reversible limit, because the string allows the velocity
    <math|v> to be controlled from the surroundings. As <math|v> approaches
    zero from either direction, <math|F<rsub|<text|fric>>> approaches zero
    and the work approaches the reversible limit
    <math|w=<around|(|m*g-F<rsub|<text|buoy>>|)>*\<Delta\>*z>. (If the fluid
    is a gas whose density is much smaller than the density of the body,
    <math|F<rsub|<text|buoy>>> can be neglected in comparison with
    <math|m*g>, and the reversible work can be written
    <math|w=m*g*\<Delta\>*z>.) <math|F<rsub|<text|fric>>> and <math|<dvar|z>>
    have opposite signs, so <math|w> for a given change of the work
    coordinate <math|z> is least positive or most negative in the reversible
    limit.

    <item>The system is the body only. In this case,
    <math|F<rsup|<text|sur>><rsub|z>> is equal to
    <math|<around*|(|F<rsub|<text|buoy>>+F<rsub|<text|fric>>+F<rsub|<text|str>>|)>>
    which by substitution from Eq. <reference|F_str=> is
    <math|<around*|(|m*g+m*<dvar|v>/<dvar|t>|)>>. The work is then given by

    <\equation>
      <label|dw=(F_buoy+F_fric+F_str)dh=><dbar|w>=F<rsup|<text|sur>>*<dvar|z>=<around*|(|m*g+m*<dvar|v>/<dvar|t>|)>*<dvar|z>
    </equation>

    For a process that begins and ends in equilibrium states,
    <math|\<Delta\>*E<rsub|<text|k>>> is zero and the finite work is
    <math|w=m*g*\<Delta\>*z>,<label|grav work with Del E(k)=0>unaffected by
    the velocity <math|v> during the process. The expressions for
    infinitesimal and finite work in the reversible limit are

    <\equation-cov2|<dbar|w=m*g*<dvar|z><space|2em><text|and><space|2em>w=m*g*\<Delta\>*z>>
      (reversible gravitational

      work of a body)
    </equation-cov2>
  </itemize>

  \;

  When we compare Eqs. <reference|dw=(mg-F_buoy-F_fric+mdv/dt)dh> and
  <reference|dw=(F_buoy+F_fric+F_str)dh=>, we see that the work when the
  system is the body is greater by the quantity
  <math|<around*|(|F<rsub|<text|buoy>>+F<rsub|<text|fric>>|)>*<dvar|z>> than
  the work when the system is the combination of body and fluid, just as in
  the case of the freely-falling body. The difference in the quantity of work
  is due to the different choices of the system boundary where contact forces
  are exerted by the surroundings.

  <index-complex|<tuple|work|gravitational>||c3 sec wgf
  idx1|<tuple|Work|gravitational>><index-complex|<tuple|gravitational|work>||c3
  sec wgf idx2|<tuple|Gravitational|work>>

  <section|Shaft Work><label|3-shaft work><label|c3 sec wgf-shaft>

  <index-complex|<tuple|shaft work>||c3 sec wgf-shaft idx1|<tuple|Shaft
  Work>><index-complex|<tuple|work|shaft>||c3 sec wgf-shaft
  idx2|<tuple|Work|shaft>><newterm|Shaft work> refers to energy transferred
  across the boundary by a rotating shaft.

  The two systems shown in Fig. <vpageref|fig:3-shaft work> will be used to
  illustrate two different kinds of shaft work. Both systems have a straight
  cylindrical shaft passing through the system boundary. Let
  <math|\<vartheta\>><\footnote>
    The symbol \<#3D1\> is <verbatim|GREEK THETA SYMBOL> at Unicode point
    <verbatim|U+03D1>.
  </footnote> be the angle of rotation of the shaft in radians, and
  <math|\<omega\>> be the angular velocity
  <math|<dvar|\<vartheta\>>/<dvar|t>>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/shaft_work.eps|173pt|107pt||>>
        <label|fig:3-shaft work>Two systems with shaft work. The dashed
        rectangles indicate the system boundaries. System A has an internal
        weight, cord, and pulley wheel in air; system B has a stirrer
        immersed in water.
      </big-figure>
    </framed>
  </float>

  Tangential forces imposed on one of these shafts can create a torque
  <math|\<tau\><rsub|<text|sys>>> at the lower end within the system, and a
  torque <math|\<tau\><rsub|<text|sur>>> at the upper end in the
  surroundings.<footnote|A torque is a moment of tangential force with
  dimensions of force times distance.> The sign convention for a torque is
  that a positive value corresponds to tangential forces in the rotational
  direction in which the shaft turns as <math|\<vartheta\>> increases.

  The condition for <math|\<omega\>> to be zero, or finite and constant
  (i.e., no angular acceleration), is that the algebraic sum of the imposed
  torques be zero: <math|\<tau\><rsub|<text|sys>>=-\<tau\><rsub|<text|sur>>>.
  Under these conditions of constant <math|\<omega\>>, the torque couple
  creates rotational shear forces in the circular cross section of the shaft
  where it passes through the boundary. These shear forces are described by
  an <index|Torque><em|internal torque> with the same magnitude as
  <math|\<tau\><rsub|<text|sys>>> and <math|\<tau\><rsub|<text|sur>>>.
  Applying the condition for zero angular acceleration to just the part of
  the shaft within the system, we find that <math|\<tau\><rsub|<text|sys>>>
  is balanced by the internal torque <math|\<tau\><rsub|<text|b>>> exerted on
  this part of the shaft by the part of the shaft in the surroundings:
  <math|\<tau\><rsub|<text|b>>=-\<tau\><rsub|<text|sys>>>. The shaft work is
  then given by the formula

  <equation-cov2|<label|w=int[tau(b)d(theta)]>w=<big|int><rsub|\<vartheta\><rsub|1>><rsup|\<vartheta\><rsub|2>>\<tau\><rsub|b>*<dvar|\<vartheta\>>=-<big|int><rsub|\<vartheta\><rsub|1>><rsup|\<vartheta\><rsub|2>>\<tau\><rsub|<text|sys>>*<dvar|\<vartheta\>>|(shaft
  work, constant <math|\<omega\>>)>

  In system A of Fig. <reference|fig:3-shaft work>, when <math|\<omega\>> is
  zero the torque <math|\<tau\><rsub|<text|sys>>> is due to the tension in
  the cord from the weight of mass <math|m>, and is finite:
  <math|\<tau\><rsub|<text|sys>>=-m*g*r> where <math|r> is the radius of the
  shaft at the point where the cord is attached. When <math|\<omega\>> is
  finite and constant, frictional forces at the shaft and pulley bearings
  make <math|\<tau\><rsub|<text|sys>>> more negative than <math|-m*g*r> if
  <math|\<omega\>> is positive, and less negative than <math|-m*g*r> if
  <math|\<omega\>> is negative. Figure <reference|fig:3-rates-shaft_work>(a)
  on page <pageref|fig:3-rates-shaft_work> shows how the shaft work given by
  Eq. <reference|w=int[tau(b)d(theta)]> depends on the angular velocity for a
  fixed value of <math|<around|\||\<vartheta\><rsub|2>-\<vartheta\><rsub|1>|\|>>.
  The variation of <math|w> with <math|\<omega\>> is due to the frictional
  forces. System A has finite, reversible shaft work in the limit of infinite
  slowness (<math|\<omega\>\<rightarrow\>0><math|>) given by
  <math|w=m*g*r*\<Delta\>*\<vartheta\>>. The shaft work is least positive or
  most negative in the reversible limit.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/rates-shaft_work.eps|237pt|115pt||>>
        <label|fig:3-rates-shaft_work>Shaft work <math|w> for a fixed
        magnitude of shaft rotation <math|\<Delta\>*\<vartheta\>> as a
        function of the angular velocity <math|\<omega\>=<dvar|\<vartheta\>>/<dvar|t>>.
        The open circles indicate work in the limit of infinite slowness.
        (a)<nbsp>System A of Fig. <reference|fig:3-shaft work>.
        (b)<nbsp>System B of Fig. <reference|fig:3-shaft work>.
      </big-figure>
    </framed>
  </float>

  In contrast to system A, the shaft work in system B has no reversible
  limit, as discussed in the next section.

  <subsection|Stirring work><label|3-stirring work><label|c3 sec
  wgf-stirring>

  <index-complex|<tuple|work|stirring>||c3 sec wgf-stirring
  idx1|<tuple|Work|stirring>><index-complex|<tuple|stirring work>||c3 sec
  wgf-stirring idx2|<tuple|Stirring work>>The shaft work done when a shaft
  turns a stirrer or paddle to agitate a liquid, as in system B of Fig.
  <vpageref|fig:3-shaft work>, is called <subindex|Work|stirring><index|Stirring
  work><newterm|stirring work>.

  In system B, when the angular velocity <math|\<omega\>> is zero and the
  water in which the stirrer is immersed is at rest, the torques
  <math|\<tau\><rsub|<text|sys>>> and <math|\<tau\><rsub|<text|b>>> are both
  zero. When <math|\<omega\>> is finite and constant, the water is stirred in
  a turbulent manner and there is a frictional drag force at the stirrer
  blades, as well as frictional forces at the shaft bearings. These forces
  make the value of <math|\<tau\><rsub|<text|sys>>> have the opposite sign
  from <math|\<omega\>>, increasing in magnitude the greater is the magnitude
  of <math|\<omega\>>. As a result, the stirring work for a fixed value of
  <math|<around|\||\<vartheta\><rsub|2>-\<vartheta\><rsub|1>|\|>> depends on
  <math|\<omega\>> in the way shown in Fig.
  <reference|fig:3-rates-shaft_work>(b). The work is positive for finite
  values of <math|\<omega\>> of either sign, and approaches zero in the limit
  of infinite slowness.

  Stirring work is an example of <index|Dissipative
  work><subindex|Work|dissipative><newterm|dissipative work>. Dissipative
  work is work that is positive for both positive and negative changes of the
  work coordinate, and therefore cannot be carried out
  reversibly.<label|dissipative work definition> Energy transferred into the
  system by dissipative work is not recovered as work done on the
  surroundings when the work coordinate is reversed. In the case of stirring
  work, if the shaft rotates in one direction work is done on the system; if
  the rotation direction is reversed, still more work is done on the system.
  The energy transferred to the system by stirring work is converted by
  friction within the system into the random motion of thermal energy: the
  energy is completely <subindex|Energy|dissipation of><index|Dissipation of
  energy><em|dissipated>.

  Because energy transferred to the system by dissipative work is converted
  to thermal energy, we could replace this work with an equal quantity of
  positive heat and produce the same overall change. The replacement of
  stirring work with heat was illustrated by experiment 3 on page
  <pageref|third experiment>.

  The shaft rotation angle <math|\<vartheta\>>, which is the work coordinate
  for stirring work, is a property of the system but is not a state function,
  as we can see by the fact that the state of the system can be exactly the
  same for <math|\<vartheta\>=0> and <math|\<vartheta\>=2*\<pi\>>. The work
  coordinate and work coefficient of work with a reversible limit are always
  state functions,<label|rev work and state functions> whereas the work
  coordinate of any kind of dissipative work is <em|not> a state function.

  In system B of Fig. <reference|fig:3-shaft work>, there is in addition to
  the stirring work the possibility of expansion work given by
  <math|<dbar|w>=-p*<dvar|V>>. When we take both kinds of work into account,
  we must treat this system as having two work coordinates:
  <math|\<vartheta\>> for stirring work and <math|V> for expansion work. Only
  the expansion work can be carried out reversibly. The number of independent
  variables in equilibrium states of this system is two, which we could
  choose as <math|T> and <math|V>. Thus, the number of independent variables
  of the equilibrium states is one greater than the number of work
  coordinates for <em|reversible> work, in agreement with the general rule
  given on page <pageref|ind var rule>.

  <index-complex|<tuple|work|stirring>||c3 sec wgf-stirring
  idx1|<tuple|Work|stirring>><index-complex|<tuple|stirring work>||c3 sec
  wgf-stirring idx2|<tuple|Stirring work>>

  <subsection|The Joule paddle wheel><label|3-Joule paddle wheel><label|c3
  sec wgf-paddle>

  <index-complex|<tuple|joule|paddle wheel>||c3 sec wgf-paddle
  idx1|<tuple|Joule|paddle wheel>><index-complex|<tuple|paddle
  wheel|joule>||c3 sec wgf-paddle idx2|<tuple|Paddle wheel|Joule>>A good
  example of the quantitative measurement of stirring work is the set of
  experiments conducted by <index|Joule, James Prescott>James Joule in the
  1840s to determine the \Pmechanical equivalent of heat.\Q In effect, he
  determined the quantity of dissipative stirring work that could replace the
  heat needed for the same temperature increase.

  Joule's apparatus contained the paddle wheel shown in Fig.
  <vpageref|fig:3-Joule paddle>. It consisted of eight sets of metal paddle
  arms attached to a shaft in a water-filled copper vessel. When the shaft
  rotated, the arms moved through openings in four sets of stationary metal
  vanes fixed inside the vessel, and churned the water. The vanes prevented
  the water from simply moving around in a circle. The result was turbulent
  motion (shearing or viscous flow) in the water and an increase in the
  temperature of the entire assembly.<\float|float|thb>
    <\framed>
      <\big-figure>
        <tabular*|<tformat|<cwith|2|2|1|1|cell-halign|c>|<cwith|2|2|1|1|cell-hyphen|n>|<table|<row|<cell|<image|../img/c3_fig..Joule_Apparatus_photo_crop.png|127pt|147.5pt||>>|<cell|>|<cell|<image|03-SUP/Joule_paddle_drawing.eps|131pt|143pt||>>>|<row|<cell|<with|font-size|.5|<hlink|Photo|https://commons.wikimedia.org/wiki/File:Joule_Apparatus.jpg>
        by <hlink|Mirko Junge|https://commons.wikimedia.org/wiki/User:DrJunge>
        / <hlink|CC BY-SA|https://creativecommons.org/licenses/by-sa/3.0/> /
        cropped from original>>|<cell|>|<cell|>>|<row|<cell|(a)>|<cell|<space|2em>>|<cell|(b)>>>>>

        \;
      <|big-figure>
        <label|fig:3-Joule paddle>Joule paddle wheel.

        \ (a)<nbsp>Joule's original paddle wheel on exhibit at the Science
        Museum, London.

        \ (b)<nbsp>Cross-section elevation of paddle wheel and water in
        copper vessel. Dark shading: rotating shaft and paddle arms; light
        shading: stationary vanes.
      </big-figure>
    </framed>
  </float>

  The complete apparatus is depicted in Fig. <vpageref|fig:3-Joule
  apparatus>. In use, two lead weights sank and caused the paddle wheel to
  rotate. <index|Joule, James Prescott>Joule evaluated the stirring work done
  on the system (the vessel, its contents, and the lid) from the change of
  the vertical position <math|h> of the weights. To a first approximation,
  this work is the negative of the change of the weights' potential energy:
  <math|w=-m*g*\<Delta\>*h> where <math|m> is the combined mass of the two
  weights. Joule made corrections for the kinetic energy gained by the
  weights, the friction in the connecting strings and pulley bearings, the
  elasticity of the strings, and the heat gain from the air surrounding the
  system.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/Joule_apparatus.eps|378pt|174pt||>>
        <label|fig:3-Joule apparatus>Joule's apparatus for measuring the
        mechanical equivalent of heat (redrawn from a figure in Ref.
        <cite|joule-1850>).

        Key: A<emdash>paddle wheel and vessel (see Fig.
        <reference|fig:3-Joule paddle>); B<emdash>wood thermal insulator;
        C<emdash>pin used to engage paddle wheel shaft to roller;
        D<emdash>roller; E<emdash>crank used to wind up the weights; F,
        G<emdash>strings; H, I<emdash>pulley wheels; J, K<emdash>weights
        (round lead disks, viewed here edge-on).
      </big-figure>
    </framed>
  </float>

  A typical experiment performed by <index|Joule, James Prescott>Joule is
  described in Prob. <reference|prb:3-Joule_expt> on page
  <pageref|prb:3-Joule_expt>. His results for the mechanical equivalent of
  heat, based on 40 such experiments at average temperatures in the range
  <math|13 <rsup|\<circ\>><text|C>>\U<math|16 <rsup|\<circ\>><text|C>> and
  expressed as the work needed to increase the temperature of one gram of
  water by one kelvin, was <label|Joule's mech equiv heat><math|4.165
  <text|J>>. This value is close to the modern value of <math|4.1855
  <text|J>> for the \P<math|15 <rsup|\<circ\>><text|C>> calorie,\Q the energy
  needed to raise the temperature of one gram of water from <math|14.5
  <rsup|\<circ\>><text|C>> to <math|15.5 <rsup|\<circ\>><text|C>>.<footnote|The
  thermochemical <index|Calorie>calorie (cal), often used as an energy unit
  in the older literature, is defined as <math|4.184 <text|J>>. Thus <math|1
  <text|kcal>=4.184 <text|kJ>>.><index-complex|<tuple|joule|paddle wheel>||c3
  sec wgf-paddle idx1|<tuple|Joule|paddle wheel>><index-complex|<tuple|paddle
  wheel|joule>||c3 sec wgf-paddle idx2|<tuple|Paddle
  wheel|Joule>><index-complex|<tuple|shaft work>||c3 sec wgf-shaft
  idx1|<tuple|Shaft Work>><index-complex|<tuple|work|shaft>||c3 sec wgf-shaft
  idx2|<tuple|Work|shaft>>

  <\bio-insert>
    <include|bio-JOULE.tm>
  </bio-insert>

  <section|Electrical Work><label|3-electrical work><label|c3 sec ew>

  <index-complex|<tuple|electrical|work>||c3 sec ew
  idx1|<tuple|Electrical|work>><index-complex|<tuple|work|electrical>||c3 sec
  ew idx2|<tuple|Work|electrical>>The electric potential <math|\<phi\>> at a
  point in space is defined as the work needed to reversibly move an
  infinitesimal test charge from a position infinitely far from other charges
  to the point of interest, divided by the value of the test charge.<label|el
  pot defn>The electrical potential energy of a charge at this point is the
  product of <math|\<phi\>> and the charge.

  <subsection|Electrical work in a circuit><label|c3 sec ew-circuit>

  <subindex|Electric|current><index|Current, electric>Electric current is
  usually conducted in an <subindex|Electrical|circuit><subindex|Circuit|electrical>electrical
  circuit. Consider a thermodynamic system that is part of a circuit: in a
  given time period electrons enter the system through one wire, and an equal
  number of electrons leave through a second wire. To simplify the
  description, the wires are called the <em|right> conductor and the
  <em|left> conductor.

  The electric potentials experienced by a electron in the right and left
  conductors are <math|\<phi\><rsub|<text|R>>> and
  <math|\<phi\><rsub|<text|L>>>, respectively. The electron charge is
  <math|-e>, where <math|e> is the elementary charge (the charge of a
  proton). Thus the electrical potential energy of an electron is
  <math|-\<phi\><rsub|<text|R>>*e> in the right conductor and
  <math|-\<phi\><rsub|<text|L>>*e> in the left conductor. The difference in
  the energies of an electron in the two conductors is the difference in the
  electrical potential energies.

  The sum of charges of a small number of electrons can be treated as an
  infinitesimal negative charge. During a period of time in which an
  infinitesimal charge <math|<dbar|Q<rsub|<text|sys>>>> enters the system at
  the right conductor and an equal charge leaves at the left conductor, the
  contribution of the electric current to the internal energy change is the
  energy difference <math|<around|(|\<phi\><rsub|R>*<dbar|Q<rsub|<text|sys>>>-\<phi\><rsub|<text|L>>*<dbar|Q<rsub|<text|sys>>>|)>=<around|(|\<phi\><rsub|<text|R>>-\<phi\><rsub|<text|L>>|)><dbar|Q<rsub|<text|sys>>>>.
  (The notation is <math|<dbar|Q<rsub|<text|sys>>>> instead of
  <math|<dvar|Q<rsub|<text|sys>>>>, because <math|Q<rsub|<text|sys>>> is a
  path function.) This internal energy change is called <em|electrical work>.
  Thus the general formula for an infinitesimal quantity of electrical work
  when the system is part of an <subindex|Electrical|circuit><subindex|Circuit|electrical>electrical
  circuit is

  <equation-cov2|<label|dw=del(phi)dQ(el)><dbar|w<rsub|<text|el>>=\<Delta\>*\<phi\>*<dbar|Q<rsub|<text|sys>>>>|(electrical
  work in a circuit)>

  where <math|\<Delta\>*\<phi\>> is the <subindex|Electric|potential
  difference><em|electric potential difference> defined by

  <\equation>
    \<Delta\>*\<phi\><bk-equal-def>\<phi\><rsub|<text|R>>-\<phi\><rsub|<text|L>>
  </equation>

  <\quote-env>
    Note that in the expression <math|<around|(|\<phi\>*<text|R>*<dbar|Q<rsub|<text|sys>>>-\<phi\><rsub|<text|L>>*<dbar|Q><rsub|<text|sys>>|)>>
    for the energy difference, the term <math|\<phi\><rsub|<text|R>>*<dbar|Q<rsub|<text|sys>>>>
    does not represent the energy transferred across the boundary at the
    right conductor, and <math|-\<phi\><rsub|<text|L>>*<dbar|Q<rsub|<text|sys>>>>
    is not the energy transferred at the left conductor. These energies
    cannot be measured individually, because they include not just the
    electrical potential energy but also the energy of the rest mass of the
    electrons. The reason we can write Eq. <reference|dw=del(phi)dQ(el)> for
    the electrical work in a circuit is that equal numbers of electrons enter
    and leave the system, so that the net energy transferred across the
    boundary depends only on the difference of the electric potential
    energies. Because the number of electrons in the system remains constant,
    we can treat the system as if it were closed.

    Why should we regard the transfer of energy across the boundary by an
    electric current as a kind of work? One justification for doing so is
    that the energy transfer is consistent with the interpretation of work
    discussed on page <pageref|work from external weight>: the only effect on
    the surroundings could be a change in the elevation of an external
    weight. For example, the weight when it sinks could drive a generator in
    the surroundings that does electrical work on the system, and electrical
    work done by the system could run an external motor that raises the
    weight.
  </quote-env>

  What is the meaning of <math|Q<rsub|<text|sys>>> in the differential
  <math|<dbar|Q<rsub|<text|sys>>>>? We define <math|Q<rsub|<text|sys>>> as
  the total cumulative charge, positive or negative, that has entered the
  system at the right conductor since the beginning of the process:
  <math|Q<rsub|<text|sys>><bk-equal-def><big|int><dbar|Q<rsub|<text|sys>>>>.
  <math|Q<rsub|<text|sys>>> is a path function for charge, and
  <math|<dbar|Q<rsub|<text|sys>>>> is its inexact differential, analogous to
  <math|q> and <math|<dbar|q>> for heat. Because the charge of an electron is
  negative, <math|<dbar|Q<rsub|<text|sys>>>> is negative when electrons enter
  at the right conductor and positive when they leave there.

  The <subindex|Electric|current><index|Current, electric>electric current
  <math|I> is the rate at which charges pass a point in the
  <subindex|Electric|circuit><subindex|Circuit|electrical>circuit:
  <math|I=<dbar|Q<rsub|<text|sys>>>/<dbar|t>>, where <math|t> is time. We
  take <math|I> as negative if electrons enter at the right conductor and
  positive if electrons leave there. This relation provides an alternative
  form of Eq. <reference|dw=del(phi)dQ(el)>:

  <equation-cov2|<label|dw=I*del(phi)dt><dbar|w<rsub|<text|el>>=I*\<Delta\>*\<phi\>*<dvar|t>>|(electrical
  work in a circuit)>

  Equations <reference|dw=del(phi)dQ(el)> and <reference|dw=I*del(phi)dt> are
  general equations for electrical work in a system that is part of a
  <subindex|Electrical|circuit><subindex|Circuit|electrical>circuit. The
  electric potential difference <math|\<Delta\>*\<phi\>> which appears in
  these equations may have its source in the surroundings, as for electrical
  heating with a resistor discussed in the next section, or in the system, as
  in the case of a galvanic cell (Sec. <reference|3-galvanic cell>).

  <subsection|Electrical heating><label|3-Electrical heating><label|c3 sec
  ew-heating>

  <index-complex|<tuple|electrical|heating>||c3 sec ew-heating
  idx1|<tuple|Electrical|heating>><index-complex|<tuple|heating|electrical>||c3
  sec ew-heating idx2|<tuple|Heating|electrical>>Figure <vpageref|fig:3-el
  heating> shows an <subindex|Electrical|resistor><index|Resistor,
  electrical>electrical resistor immersed in a liquid. We will begin by
  defining the system to include both the resistor and the liquid. An
  external voltage source provides an <subindex|Electric|potential
  difference>electric potential difference <math|\<Delta\>*\<phi\>> across
  the wires. When electrons flow in the circuit, the resistor becomes warmer
  due to the ohmic resistance of the resistor. This phenomenon is variously
  called electrical heating, Joule heating, ohmic heating, or resistive
  heating. The heating is caused by inelastic collisions of the moving
  electrons with the stationary atoms of the
  <subindex|Electrical|resistor><index|Resistor, electrical>resistor, a type
  of friction. If the resistor becomes warmer than the surrounding liquid,
  there will be a transfer of thermal energy from the resistor to the
  liquid.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/el_heating.eps|76pt|96pt||>>
        <label|fig:3-el heating>System containing an electrical resistor
        immersed in a liquid. The dashed rectangle indicates the system
        boundary.
      </big-figure>
    </framed>
  </float>

  The electrical work performed on this system is given by the expressions
  <math|<dbar|w<rsub|<text|el>>>=\<Delta\>*\<phi\>*<dbar|Q<rsub|<text|sys>>>>
  and <math|<dbar|w<rsub|<text|el>>>=I*\<Delta\>*\<phi\>*<dvar|t>> (Eqs.
  <reference|dw=del(phi)dQ(el)> and <reference|dw=I*del(phi)dt>). The portion
  of the electrical <subindex|Electrical|circuit><subindex|Circuit|electrical>circuit
  inside the system has an <subindex|Electric|resistance><subindex|Resistance|electric>electric
  resistance given by <math|R<rsub|<text|el>>=\<Delta\>*\<phi\>/I> (Ohm's
  law). Making the substitution <math|\<Delta\>*\<phi\>=I*R<rsub|<text|el>>>
  in the work expressions gives two new expressions for electrical work in
  this system:

  <\equation>
    <label|dw=IRdQ(el)><dbar|w<rsub|<text|el>>>=I*R<rsub|<text|el>>*<dbar|Q<rsub|<text|sys>>>
  </equation>

  <\equation>
    <label|dw=I2*Rdt><dbar|w<rsub|<text|el>>>=I<rsup|2>*R<rsub|<text|el>>*<dvar|t>
  </equation>

  The integrated form of Eq. <reference|dw=IRdQ(el)> when <math|I> and
  <math|R<rsub|<text|el>>> are constant is
  <math|w<rsub|<text|el>>=I*R<rsub|<text|el>>*Q<rsub|<text|sys>>>. When the
  source of the electric potential difference is in the surroundings, as it
  is here, <math|I> and <math|Q<rsub|<text|sys>>> have the same sign, so
  <math|w<rsub|<text|el>>> is positive for finite current and zero when there
  is no current. Figure <vpageref|fig:3-el rates><\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/rates-el.eps|94pt|95pt||>>
        <label|fig:3-el rates>Work of electrical heating with a fixed
        magnitude of <math|Q<rsub|<text|sys>>> as a function of the electric
        current <math|I=<dbar|Q<rsub|<text|sys>>>/<dvar|t>>. The open circle
        indicates the limit of infinite slowness.
      </big-figure>
    </framed>
  </float> shows graphically how the work of electrical heating is positive
  for both positive and negative changes of the work coordinate
  <math|Q<rsub|<text|sys>>> and vanishes as <math|I>, the rate of change of
  the work coordinate, approaches zero. These are characteristic of
  irreversible <index|Dissipative work><subindex|Work|dissipative><em|dissipative>
  work (page <pageref|dissipative work definition>). Note the resemblance of
  Fig. <reference|fig:3-el rates> to Fig.
  <reference|fig:3-rates-shaft_work>(b) on page
  <pageref|fig:3-rates-shaft_work> for dissipative stirring work\Vthey are
  the same graphs with different labels.

  Suppose we redefine the system to be only the liquid. In this case,
  electric current passes through the resistor but not through the system
  boundary. There is no electrical work, and we must classify energy transfer
  between the resistor and the liquid as <index|Heat><em|heat>.

  \;

  <index-complex|<tuple|electrical|heating>||c3 sec ew-heating
  idx1|<tuple|Electrical|heating>><index-complex|<tuple|heating|electrical>||c3
  sec ew-heating idx2|<tuple|Heating|electrical>>

  <subsection|Electrical work with a galvanic cell><label|3-galvanic
  cell><label|c3 sec ew-galvanic>

  A <index|Galvanic cell><em|galvanic cell> is an electrochemical system
  that, when isolated, exhibits an electric potential difference between the
  two terminals at the system boundary. The potential difference has its
  source at the interfaces between phases within the cell.

  Consider the combination of galvanic cell and electrical resistor in Fig.
  <vpageref|fig:3-galv cell>, and let the <em|system> be the cell. When an
  electric current passes through the cell in either direction, a cell
  reaction takes place in one direction or the other.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/galvanic_cell.eps|187pt|82pt||>>
        <label|fig:3-galv cell>Galvanic cell and external electrical
        resistor.

        \ (a)<nbsp>Open circuit with isolated cell in an equilibrium state.

        \ (b)<nbsp>Closed circuit.
      </big-figure>
    </framed>
  </float>

  In a manner similar to the labeling of the conductors of a circuit, the
  cell terminals are called the <em|right> terminal and the <em|left>
  terminal. The <index|Cell potential><em|cell potential>
  <math|E<rsub|<text|cell>>> is the electric potential difference between the
  terminals, and is defined by

  <\equation>
    <label|cell pot defn>E*<rsub|<text|cell>><bk-equal-def>\<phi\><rsub|<text|R>>-\<phi\><rsub|<text|L>>
  </equation>

  When the cell is in an isolated zero-current equilibrium state, as in Fig.
  <reference|fig:3-galv cell>(a), the cell potential is the
  <index|Equilibrium cell potential><subindex|Cell
  potential|equilibrium><em|equilibrium cell potential>
  <math|E<rsub|<text|cell>,<text|eq>>>. When the cell is part of an
  electrical circuit with an electric current passing through the cell, as in
  Fig. <reference|fig:3-galv cell>(b), <math|E<rsub|<text|cell>>> is
  different from <math|E<rsub|<text|cell>,<text|eq>>> on account of the
  internal resistance <math|R<rsub|<text|cell>>> of the cell:

  <\equation>
    <label|Ecell=Ecell(eq)+IRcell>E=E<rsub|<text|cell>,<text|eq>>+I*R<rsub|<text|cell>>
  </equation>

  The sign of the current <math|I> is negative when electrons enter the cell
  at the right terminal, and positive when electrons leave there.

  In the circuit shown in Fig. <reference|fig:3-galv cell>(b), the cell does
  electrical work on the <subindex|Electrical|resistor><index|Resistor,
  electrical>resistor in the surroundings. The energy for this work comes
  from the cell reaction. The formula for the electrical work is given by Eq.
  <reference|dw=del(phi)dQ(el)> with <math|\<Delta\>*\<phi\>> replaced by
  <math|E<rsub|<text|cell>>>:

  <\equation>
    <label|dw(el)=E(cell)dQ(cell)><dbar|w<rsub|<text|el>>>=E<rsub|<text|cell>>*<dbar|Q<rsub|<text|sys>>>
  </equation>

  The figure shows <math|E<rsub|<text|cell>>> as positive and
  <math|<dbar|Q<rsub|<text|sys>>>> as negative, so for this arrangement
  <math|<dbar|w<rsub|<text|el>>>> is negative.

  When current passes through the cell, the work done is irreversible because
  the internal resistance causes <subindex|Energy|dissipation
  of><index|Dissipation of energy>energy dissipation. We can make this work
  approach a finite reversible limit by replacing the external resistor shown
  in Fig. <reference|fig:3-galv cell>(b) with an adjustable voltage source
  that we can use to control the cell potential <math|E<rsub|<text|cell>>>
  and the current <math|I>. According to Eq.
  <reference|Ecell=Ecell(eq)+IRcell>, <math|E<rsub|<text|cell>>> is greater
  than <math|E<rsub|<text|cell>,<text|eq>>> when <math|I> is positive, and is
  less than <math|E<rsub|<text|cell>,<text|eq>>> when <math|I> is negative.
  This behavior is shown graphically in Fig. <vpageref|fig:3-galv cell
  work>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/rates-galv_cell.eps|93pt|95pt||>>
        <label|fig:3-galv cell work>Electrical work of a galvanic cell for a
        fixed magnitude of <math|Q<rsub|<text|sys>>> as a function of the
        electric current <math|I=<dbar|Q<rsub|<text|sys>>>/<dvar|t>>. Open
        circles: reversible limits.
      </big-figure>
    </framed>
  </float>

  In the limit as the electric current approaches zero from either direction
  and the external adjustable voltage approaches
  <math|E<rsub|<text|cell>,<text|eq>>>, the electrical work approaches a
  reversible limit given by

  <\equation>
    <label|dw(el,rev)=><dbar|w<rsub|<text|el>,<text|rev>>>=E<rsub|<text|cell>,<text|eq>>*<dbar|Q<rsub|<text|sys>>>
  </equation>

  Note that the electrical work is the least positive or most negative in the
  reversible limit.

  Thus, unlike the dissipative work of stirring and electrical heating,
  electrical work with a galvanic cell has a nonzero reversible limit, as
  reflected by the difference in the appearance of Fig. <reference|fig:3-galv
  cell work> compared to Figs. <reference|fig:3-rates-shaft_work> and
  <reference|fig:3-el rates>. During irreversible electrical work of a
  galvanic cell, there is only <em|partial> <subindex|Energy|dissipation
  of><index|Dissipation of energy>dissipation of energy within the cell: the
  energy transferred across the boundary by the work can be partially
  recovered by returning the work coordinate <math|Q<rsub|<text|sys>>> to its
  initial value.

  <\quote-env>
    On page <pageref|rev work and state functions> the observation was made
    that the work coordinate of work with a reversible limit is always a
    state function. Electrical work with a galvanic cell does not contradict
    this statement, because the work coordinate <math|Q<rsub|<text|sys>>> is
    proportional to the extent of the cell reaction, a state function.
  </quote-env>

  The thermodynamics of galvanic cells will be treated in detail in Chap.
  <reference|Chap. 14>.<index-complex|<tuple|electrical|work>||c3 sec ew
  idx1|<tuple|Electrical|work>><index-complex|<tuple|work|electrical>||c3 sec
  ew idx2|<tuple|Work|electrical>>

  <section|Irreversible Work and Internal Friction><label|3-int
  friction><label|c3 sec iwif>

  <index-complex|<tuple|internal|friction>||c3 sec iwif
  idx1|<tuple|Internal|friction>><index-complex|<tuple|friction|internal>||c3
  sec iwif idx2|<tuple|Friction|internal>>Consider an irreversible adiabatic
  process of a closed system in which a work coordinate <math|X> changes at a
  finite rate along the path, starting and ending with equilibrium states.
  For a given initial state and a given change <math|\<Delta\>*X>, the work
  is found to be less positive or more negative the more slowly is the rate
  of change of <math|X>. The work is least positive or most negative in the
  limit of infinite slowness\Vthat is, the least work needs to be done on the
  system, or the most work can be done by the system on the surroundings.
  This <index|Minimal work principle><em|minimal work principle> is
  illustrated in Sec. <reference|3-spont ad exp> for expansion work, Sec.
  <reference|3-gravitational field> for work in a gravitational field, and
  Sec. <reference|3-galvanic cell> for electrical work with a galvanic cell.

  Let <math|w<rsub|<text|irr>>> be the work during an irreversible adiabatic
  process occurring at a finite rate, and <math|w<rsub|0>> be the adiabatic
  work for the same initial state and the same value of <math|\<Delta\>*X> in
  the limit of infinite slowness. According to the minimal work principle,
  the difference <math|w<rsub|<text|irr>>-w<rsub|0>> is positive.
  <math|w<rsub|0>> is the reversible work if the work has a reversible limit:
  compare Figs. <reference|fig:3-rates-shaft_work>(a) and
  <reference|fig:3-rates-shaft_work>(b) for shaft work with and without a
  reversible limit, respectively; also Figs. <reference|fig:3-el rates> and
  <reference|fig:3-galv cell work> for electrical work without and with a
  reversible limit.

  Conceptually, we can attribute the positive value of
  <math|w<rsub|<text|irr>>-w<rsub|0>> to <em|internal friction> that
  <subindex|Energy|dissipation of><index|Dissipation of energy>dissipates
  other forms of energy into thermal energy within the system. Internal
  friction is not involved when, for example, a temperature gradient causes
  heat to flow spontaneously across the system boundary, or an irreversible
  chemical reaction takes place spontaneously in a homogeneous phase. Nor is
  internal friction necessarily involved when positive work increases the
  thermal energy: during an infinitely slow adiabatic compression of a gas,
  the temperature and thermal energy increase but internal friction is
  absent\Vthe process is reversible.

  During a process with internal friction, <subindex|Energy|dissipation
  of><index|Dissipation of energy>energy dissipation can be either partial or
  complete. <index|Dissipative work><subindex|Work|dissipative><em|Dissipative
  work>, such as the stirring work and electrical heating described in Sec.
  <reference|3-stirring work> and Sec. <reference|3-Electrical heating>, is
  irreversible work with complete energy dissipation and no reversible limit.
  The final equilibrium state of an adiabatic process with dissipative work
  can also be reached by a path in which positive heat replaces the
  dissipative work. This is a special case of the minimal work principle.

  As a model for work with partial energy dissipation, consider the
  gas-filled cylinder-and-piston device depicted in Fig.
  <vpageref|fig:3-friction>. This device has an obvious source of internal
  friction in the form of a rod sliding through a bushing. The <em|system>
  consists of the contents of the cylinder to the left of the piston,
  including the gas, the rod, and the bushing; the piston and cylinder wall
  are in the surroundings.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/FRICTION.eps|160pt|78pt||>>
        <label|fig:3-friction>Cylinder and piston with internal sliding
        friction. The dashed rectangle indicates the system boundary.
        P<emdash>piston; R<emdash>internal rod attached to the piston;
        B<emdash>bushing fixed inside the cylinder. A fixed amount of gas
        fills the remaining space inside the cylinder.
      </big-figure>
    </framed>
  </float>

  From Eq. <reference|dw=-Fx(sys)dx w=>, the energy transferred as work
  across the boundary of this system is

  <\equation>
    <label|w=-int F(sys) dx>w=-<big|int><rsub|x<rsub|1>><rsup|x<rsub|2>>F<rsup|<text|sys>>*<dvar|x>
  </equation>

  where <math|x> is the piston position and <math|F<rsup|<text|sys>>> is the
  component in the direction of increasing <math|x> of the force exerted by
  the system on the surroundings at the moving boundary.

  For convenience, we let <math|V> be the volume of the gas rather than that
  of the entire system. The relation between changes of <math|V> and <math|x>
  is <math|<dvar|V>=A<rsub|<text|s>>*<dvar|x>> where <math|A<rsub|<text|s>>>
  is the cross-section area of the cylinder. We also define
  <math|p<rsup|<text|sys>>> to be the total force per unit area exerted by
  the system: <math|p<rsup|<text|sys>>=F<rsup|<text|sys>>/A<rsub|<text|s>>>.
  With <math|V> replacing <math|x> as the work coordinate, Eq.
  <reference|w=-int F(sys) dx> becomes

  <\equation>
    <label|w=-int(friction)>w=-<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>><around*|(|F<rsup|<text|sys>>/A<rsub|<text|s>>|)>*<dvar|V>=-<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>>p<rsup|<text|sys>>*<dvar|V>
  </equation>

  Equation <reference|w=-int(friction)> shows that a plot of
  <math|p<rsup|<text|sys>>> as a function of <math|V> is an indicator diagram
  (Sec. <reference|3-indicator diagrams>), and that the work is equal to the
  negative of the area under the curve of this plot.

  We can write the force <math|F<rsup|<text|sys>>> as the sum of two
  contributions:<footnote|This equation assumes the gas pressure is uniform
  and a term for the acceleration of the rod is negligible.>

  <\equation>
    <label|F(sys)=p(b)A+F(fric)>F<rsup|<text|sys>>=p*A<rsub|<text|s>>+F<rsub|<text|fric>>
  </equation>

  Here <math|p> is the gas pressure, and <math|F<rsub|<text|fric>>> is the
  force exerted on the rod due to internal friction with sign opposite to
  that of the piston velocity <math|<dvar|x>/<dvar|t>>. Substitution of this
  expression for <math|F<rsup|<text|sys>>> in Eq.
  <reference|w=-int(friction)> gives

  <\equation>
    w=-<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>>p*<dvar|V>-<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>><around|(|F<rsub|<text|fric>>/A<rsub|<text|s>>|)>*<dvar|V>
  </equation>

  The first term on the right is the work of expanding or compressing the
  gas. The second term is the frictional work:
  <math|w<rsub|<text|fric>>=-<big|int><around|(|F<rsub|<text|fric>>/A<rsub|<text|s>>|)>*<dvar|V>>.
  The frictional work is positive or zero, and represents the
  <subindex|Energy|dissipation of><index|Dissipation of energy>energy
  dissipated within the system by internal sliding friction.

  The motion of the piston is controlled by an external force applied to the
  right face of the piston. The internal friction at the bushing can be
  either <em|lubricated friction> or <em|dry friction>.

  If the contact between the rod and bushing is lubricated,
  <subindex|Friction|lubricated> a film of fluid lubricant separates the two
  solid surfaces and prevents them from being in direct contact. When the rod
  is in motion, the adjacent fluid layer moves with it, and the layer next to
  the bushing is stationary. Adjacent layers within the film move relative to
  one another. The result is shear stress (page <pageref|2-shear stress>) and
  a frictional force exerted on the moving rod. The frictional force depends
  on the lubricant viscosity, the area of the film, and the velocity of the
  rod. As the rod velocity approaches zero, the frictional force also
  approaches zero.

  In the limit of infinite slowness <math|F<rsub|<text|fric>>> and
  <math|w<rsub|<text|fric>>> vanish, and the process is reversible with
  expansion work given by <math|w=-<big|int>p*<dvar|V>>.

  The situation is different when the piston moves at an appreciable finite
  rate. The frictional work <math|w<rsub|<text|fric>>> is then positive. As a
  result, the irreversible work of expansion is less negative than the
  reversible work for the same volume increase, and the irreversible work of
  compression is more positive than the reversible work for the same volume
  decrease. These effects of piston velocity on the work are consistent with
  the minimal work principle.

  <\quote-env>
    \ The piston velocity, besides affecting the frictional force on the rod,
    has an effect on the force exerted by the gas on the piston as described
    in Sec. <reference|3-expansion>. At large finite velocities, this latter
    effect tends to further decrease <math|F<rsup|<text|sys>>> during
    expansion and increase it during compression, and so is an additional
    contribution to internal friction. If turbulent flow is present within
    the system, that is also a contribution.
  </quote-env>

  Figure <vpageref|fig:3-gas-dissip> shows indicator diagrams for adiabatic
  expansion and compression with internal lubricated friction. The solid
  curves are for irreversible processes at a constant piston velocity, and
  the dashed curves are for reversible processes with the same initial states
  as the irreversible processes. The areas under the curves confirm that the
  work for expansion is less negative along the irreversible path than along
  the reversible path, and that for compression the work is more positive
  along the irreversible path than along the reversible
  path.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/gas-dissip.eps|370pt|225pt||>>
        <label|fig:3-gas-dissip>Indicator diagrams for the system of Fig.
        <reference|fig:3-friction> with internal lubricated friction.

        \ Solid curves: <math|p<rsup|<text|sys>>> for irreversible adiabatic
        volume changes at finite rates in the directions indicated by the
        arrows.

        \ Dashed curves: <math|p<rsup|<text|sys>>=p> along a reversible
        adiabat.

        \ Open circles: initial and final equilibrium states.

        \ (a)<nbsp>Adiabatic expansion.

        \ (b)<nbsp>Adiabatic compression.
      </big-figure>
    </framed>
  </float>

  Because of these differences in work, the final states of the irreversible
  processes have greater internal energies and higher temperatures and
  pressures than the final states of the reversible processes with the same
  volume change, as can be seen from the positions on the indicator diagrams
  of the points for the final equilibrium states. The overall change of state
  during the irreversible expansion or compression is the same for a path in
  which the reversible adiabatic volume change is followed by positive heat
  at constant volume. Since <math|\<Delta\>*U> must be the same for both
  paths, the required heat equals <math|w<rsub|<text|irr>>-w<rsub|<text|rev>>>.
  This is not the value of the frictional work, because the thermal energy
  released by frictional work increases the gas pressure, making
  <math|w<rsub|<text|irr>>-w<rsub|<text|rev>>> less than
  <math|w<rsub|<text|fric>>> for expansion and greater than
  <math|w<rsub|<text|fric>>> for compression. There seems to be no general
  method by which the <subindex|Energy|dissipation of><index|Dissipation of
  energy>energy dissipated by internal friction can be evaluated, and it
  would be even more difficult for an irreversible process with both work and
  heat.

  Figure <vpageref|fig:3-rates-dissip> shows the effect of the rate of change
  of the volume on the adiabatic work for a fixed magnitude of the volume
  change. Note that the work of expansion and the work of compression have
  opposite signs, and that it is only in the reversible limit that they have
  the same <em|magnitude>. The figure resembles Fig. <reference|fig:3-galv
  cell work> for electrical work of a galvanic cell with the horizontal axis
  reversed, and is typical of irreversible work with partial
  <subindex|Energy|dissipation of><index|Dissipation of energy>energy
  dissipation.<\float|float|thb>
    <\framed>
      <\big-figure|<image|03-SUP/rates-dissip.eps|117pt|93pt||>>
        <label|fig:3-rates-dissip>Adiabatic expansion work with internal
        lubricated friction for a fixed magnitude of <math|\<Delta\>*V>, as a
        function of the average rate of volume change. The open circles
        indicate the reversible limits.
      </big-figure>
    </framed>
  </float>

  If the rod and bushing shown in Fig. <reference|fig:3-friction> are
  <em|not> lubricated, so that their surfaces are in direct contact, the
  frictional force does not approach zero in the limit of zero piston
  velocity, unlike the behavior of lubricated friction. This dry friction is
  due to the roughness, on a microscopic scale, of the contacting surfaces.
  <subindex|Friction|dry> The frictional force of dry friction is typically
  independent of the area of contact and the rate at which the solid surfaces
  slide past one another.

  The curves on indicator diagrams for adiabatic expansion and compression
  with internal dry friction are similar to the solid curves in Figs.
  <reference|fig:3-gas-dissip>(a) and <reference|fig:3-gas-dissip>(b), but
  their positions, unlike the curves for lubricated friction, change little
  as the average rate of volume change approaches zero. In the limit of
  infinite slowness, the work for a fixed magnitude of <math|\<Delta\>*V> is
  negative for expansion and positive for compression, but the expansion work
  is smaller in magnitude than the compression work. The internal dry
  friction prevents the expansion process from being reversed as a
  compression process, regardless of piston velocity, and these processes are
  therefore irreversible.<index-complex|<tuple|internal|friction>||c3 sec
  iwif idx1|<tuple|Internal|friction>><index-complex|<tuple|friction|internal>||c3
  sec iwif idx2|<tuple|Friction|internal>>

  <section|Reversible and Irreversible Processes:
  Generalities><label|3-generalities><label|c3 sec-ripg>

  <index-complex|<tuple|reversible|process>||c3 sec-ripg
  idx1|<tuple|Reversible|process>><index-complex|<tuple|process|reversible>||c3
  sec-ripg idx2|<tuple|Process|Reversible>>

  This section summarizes some general characteristics of processes in closed
  systems. Some of these statements will be needed to develop aspects of the
  second law in Chap. <reference|Chap. 4>.

  <\itemize>
    <item>Infinitesimal quantities of work during a process are calculated
    from an expression of the form <math|<dbar|w>=<big|sum><rsub|i>Y<rsub|i>*<dvar|X<rsub|i>>>,
    where <math|X<rsub|i>> is the work coordinate of kind of work <math|i>
    and <math|Y<rsub|i>> is the conjugate work coefficient.

    <item>The work coefficients and work coordinates of
    <subindex|Reversible|work><subindex|Work|reversible><em|reversible> work
    are state functions.

    <item>Energy transferred across the boundary by work in a reversible
    process is fully recovered as work of the opposite sign in the reverse
    reversible process. It follows from the first law that heat is also fully
    recovered in the reverse process.

    <item>When work occurs irreversibly at a finite rate, there is partial or
    complete <subindex|Energy|dissipation of><index|Dissipation of
    energy>dissipation of energy. The dissipation results in a change that
    could also be accomplished with positive heat, such as an increase of
    thermal energy within the system.

    <item><index|Dissipative work><subindex|Work|dissipative>Dissipative work
    is positive irreversible work with complete <subindex|Energy|dissipation
    of><index|Dissipation of energy>energy dissipation. The work coordinate
    for this type of work is not a state function. Examples are stirring work
    (Sec. <reference|3-stirring work>) and the work of electrical heating
    (Sec. <reference|3-Electrical heating>).

    <item>If a process is carried out <subindex|Process|adiabatic><subindex|Adiabatic|process>adiabatically
    and has a reversible limit, the work for a given initial equilibrium
    state and a given change in the work coordinate is least positive or most
    negative in the reversible limit.<label|minimal work principle>The
    dependence of work on the rate of change of the work coordinate is shown
    graphically for examples of dissipative work in Figs.
    <reference|fig:3-rates-shaft_work>(b) and <reference|fig:3-el rates>, and
    for examples of work with partial <subindex|Energy|dissipation
    of><index|Dissipation of energy>energy dissipation in Figs.
    <reference|fig:3-rates-shaft_work>(a), <reference|fig:3-galv cell work>,
    and <reference|fig:3-rates-dissip>.

    <item>The number of independent variables needed to describe equilibrium
    states of a closed system is one greater than the number of independent
    work coordinates for reversible work.<footnote|If the system has internal
    adiabatic partitions that allow different phases to have different
    temperatures in equilibrium states, then the number of independent
    variables is equal to the number of work coordinates plus the number of
    independent temperatures.> Thus, we could choose the independent
    variables to be each of the work coordinates and in addition either the
    temperature or the internal energy.<footnote|There may be exceptions to
    this statement in special cases. For example, along the
    <subindex|Triple|line>triple line of a pure substance the values of
    <math|V> and <math|T>, or of <math|V> and <math|U>, are not sufficient to
    determine the amounts in each of the three possible phases.> The number
    of independent variables needed to describe a nonequilibrium state is
    greater (often <em|much> greater) than this.
  </itemize>

  Table <vpageref|tbl:3-work> lists general formulas for various kinds of
  work, including those that were described in detail in Secs.
  <reference|3-deformation>\U<reference|3-electrical
  work>.<new-page><float|float|thb|<\big-table>
    <bktable3|<tformat|<cwith|2|-1|1|-1|cell-valign|c>|<cwith|2|-1|1|-1|cell-halign|l>|<cwith|2|10|1|3|cell-bsep|1fn>|<cwith|2|10|1|3|cell-tsep|1fn>|<table|<row|<cell|Kind>|<cell|Formula>|<cell|Definitions>>|<row|<\cell>
      <subindex|Work|mechanical>Linear mechanical work
    </cell>|<\cell>
      <math|<dbar|w>=F<rsup|<text|sur>><rsub|x>*<dvar|x>>
    </cell>|<\cell>
      <tabular|<tformat|<cwith|1|-1|3|3|cell-hyphen|t>|<table|<row|<cell|<math|F<rsup|<text|sur>><rsub|x>>>|<cell|<math|=>>|<\cell>
        <math|x>-component of force exerted

        by surroundings
      </cell>>|<row|<cell|<dvar|x>>|<cell|<math|=>>|<\cell>
        displacement in <math|x> direction
      </cell>>>>>
    </cell>>|<row|<\cell>
      <subindex|Work|shaft>Shaft work
    </cell>|<\cell>
      <math|<dbar|w>=\<tau\><rsub|<text|b>>*<dvar|\<vartheta\>>>
    </cell>|<\cell>
      <tabular|<tformat|<cwith|1|-1|3|3|cell-hyphen|t>|<table|<row|<cell|<math|\<tau\><rsub|<text|b>>>>|<cell|<math|=>>|<\cell>
        internal torque at boundary
      </cell>>|<row|<cell|<math|\<vartheta\>>>|<cell|<math|=>>|<\cell>
        angle of rotation
      </cell>>>>>
    </cell>>|<row|<\cell>
      <subindex|Expansion|work><subindex|Work|expansion>Expansion work
    </cell>|<\cell>
      <math|<dbar|w>=-p<rsub|<text|b>>*<dvar|V>>
    </cell>|<\cell>
      <tabular|<tformat|<cwith|1|-1|1|1|cell-hyphen|t>|<table|<row|<\cell>
        <math|p<rsub|<text|b>>=> average pressure at moving

        boundary
      </cell>>>>>
    </cell>>|<row|<\cell>
      <subindex|Work|surface>Surface work of a flat surface
    </cell>|<\cell>
      <math|<dbar|w>=\<gamma\>*<dvar|A<rsub|<text|s>>>>
    </cell>|<\cell>
      <tabular|<tformat|<cwith|1|-1|1|1|cell-hyphen|t>|<table|<row|<\cell>
        <math|\<gamma\>=> surface tension, <math|A<rsub|<text|s>>=> surface
        area
      </cell>>>>>
    </cell>>|<row|<\cell>
      <subindex|Work|stretching>Stretching or compression

      of a rod or spring
    </cell>|<\cell>
      <math|<dbar|w>=F*<dvar|l>>
    </cell>|<\cell>
      <tabular|<tformat|<cwith|1|-1|3|3|cell-hyphen|t>|<table|<row|<cell|<math|F>>|<cell|<math|=>>|<\cell>
        stress (positive for tension,

        negative for compression)
      </cell>>|<row|<cell|<math|l>>|<cell|<math|=>>|<\cell>
        length
      </cell>>>>>
    </cell>>|<row|<\cell>
      <subindex|Gravitational|work><subindex|Work|gravitational>Gravitational
      work
    </cell>|<\cell>
      <math|<dbar|w>=m*g*<dvar|h>>
    </cell>|<\cell>
      <tabular|<tformat|<cwith|1|-1|3|3|cell-hyphen|t>|<table|<row|<cell|<math|m>>|<cell|<math|=>>|<\cell>
        mass, <math|h=> height
      </cell>>|<row|<cell|<math|g>>|<cell|<math|=>>|<\cell>
        acceleration of free fall
      </cell>>>>>
    </cell>>|<row|<\cell>
      <subindex|Electrical|work><subindex|Work|electrical>Electrical work in
      a circuit
    </cell>|<\cell>
      <math|<dbar|w>=\<Delta\>*\<phi\>*<dbar|Q<rsub|<text|sys>>>>
    </cell>|<\cell>
      <tabular|<tformat|<cwith|1|-1|3|3|cell-hyphen|t>|<table|<row|<cell|<math|\<Delta\>*\<phi\>>>|<cell|<math|=>>|<\cell>
        electric potential difference
      </cell>>|<row|<cell|>|<cell|<math|=>>|<\cell>
        <math|\<phi\><rsub|<text|R>>-\<phi\><rsub|<text|L>>>
      </cell>>>>>
    </cell>>|<row|<\cell>
      <index-complex|<tuple|work|electric polarization>|||<tuple|Work|of
      electric polarization>>Electric polarization
    </cell>|<\cell>
      <math|<dbar|w>=\<b-E\>\<bullet\><dvar|\<b-p\>>>
    </cell>|<\cell>
      <tabular|<tformat|<cwith|1|-1|3|3|cell-hyphen|t>|<table|<row|<cell|<math|\<b-E\>>>|<cell|<math|=>>|<\cell>
        electric field strength
      </cell>>|<row|<cell|<math|\<b-p\>>>|<cell|<math|=>>|<\cell>
        electric dipole moment of system
      </cell>>>>>
    </cell>>|<row|<\cell>
      <index-complex|<tuple|work|magnetization>|||<tuple|Work|of
      magnetization>>Magnetization
    </cell>|<\cell>
      <math|<dbar|w>=\<b-B\>\<bullet\><dvar|\<b-m\>>>
    </cell>|<\cell>
      <tabular|<tformat|<cwith|1|-1|3|3|cell-hyphen|t>|<table|<row|<cell|<math|\<b-B\>>>|<cell|<math|=>>|<\cell>
        magnetic flux density
      </cell>>|<row|<cell|<math|\<b-m\>>>|<cell|<math|=>>|<\cell>
        magnetic dipole moment of system
      </cell>>>>>
    </cell>>>>>

    \;
  <|big-table>
    <label|tbl:3-work>Some kinds of work
  </big-table>>

  \;

  <index-complex|<tuple|reversible|process>||c3 sec-ripg
  idx1|<tuple|Reversible|process>><index-complex|<tuple|process|reversible>||c3
  sec-ripg idx2|<tuple|Process|Reversible>>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|45>
    <associate|page-height|auto>
    <associate|page-medium|papyrus>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|7>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|3-Electrical heating|<tuple|3.8.2|?>>
    <associate|3-Heat and work as path functions|<tuple|3.1.3|?>>
    <associate|3-Joule paddle wheel|<tuple|3.7.2|?>>
    <associate|3-U of ideal gas|<tuple|3.5.1|?>>
    <associate|3-applications of exp work|<tuple|3.5|?>>
    <associate|3-deformation|<tuple|3.4|?>>
    <associate|3-electrical work|<tuple|3.8|?>>
    <associate|3-expansion|<tuple|3.4.1|?>>
    <associate|3-expansion work|<tuple|3.4.2|?>>
    <associate|3-free expansion|<tuple|3.5.6|?>>
    <associate|3-galvanic cell|<tuple|3.8.3|?>>
    <associate|3-generalities|<tuple|3.10|?>>
    <associate|3-gravitational field|<tuple|3.6|?>>
    <associate|3-heat capacity|<tuple|3.1.5|?>>
    <associate|3-heat transfer|<tuple|3.3|?>>
    <associate|3-heat, work, first law|<tuple|3.1|?>>
    <associate|3-heating \ cooling|<tuple|3.3.1|?>>
    <associate|3-indicator diagrams|<tuple|3.5.4|?>>
    <associate|3-int friction|<tuple|3.9|?>>
    <associate|3-irreversible processes|<tuple|3.2.3|?>>
    <associate|3-p at moving piston|<tuple|Reversible|?>>
    <associate|3-purely mechanical processes|<tuple|3.2.4|?>>
    <associate|3-rev adiab expan|<tuple|3.5.3|?>>
    <associate|3-rev isothermal exp|<tuple|3.5.2|?>>
    <associate|3-rev list end|<tuple|<with|mode|<quote|math>|\<bullet\>>|?>>
    <associate|3-rev list start|<tuple|Spontaneous process|?>>
    <associate|3-reversibility \ surroundings|<tuple|3.2.2|?>>
    <associate|3-reversible processes|<tuple|3.2.1|?>>
    <associate|3-shaft work|<tuple|3.7|?>>
    <associate|3-spont ad exp|<tuple|3.5.5|?>>
    <associate|3-spont and rev processes|<tuple|3.2|?>>
    <associate|3-stirring work|<tuple|3.7.1|?>>
    <associate|3-thermal energy|<tuple|3.1.6|?>>
    <associate|3-thermo work|<tuple|3.1.1|?>>
    <associate|\<less\>F_x\<gtr\>|<tuple|3.4.8|?>>
    <associate|\<less\>Fx\<gtr\>=|<tuple|3.4.6|?>>
    <associate|Chap. 3|<tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Chapter
    3>|?>>
    <associate|DelE=DelEk+DelEp+DelU|<tuple|3.1.5|?>>
    <associate|DelEsys=|<tuple|3.1.3|?>>
    <associate|Ecell=Ecell(eq)+IRcell|<tuple|3.8.7|?>>
    <associate|F(gas)=p(b)A(s)|<tuple|3.4.2|?>>
    <associate|F(net)=F(gas)-F(ext)+F(fric)|<tuple|3.4.3|?>>
    <associate|F(sys)=p(b)A+F(fric)|<tuple|3.9.3|?>>
    <associate|F_str=|<tuple|3.6.2|?>>
    <associate|Joule's mech equiv heat|<tuple|Joule, James Prescott|?>>
    <associate|T2= (ad rev expan of id gas)|<tuple|3.5.11|?>>
    <associate|auto-1|<tuple|3|?>>
    <associate|auto-10|<tuple|Frame|?>>
    <associate|auto-100|<tuple|Dissipation of energy|?>>
    <associate|auto-101|<tuple|3.2.4|?>>
    <associate|auto-102|<tuple|Process|?>>
    <associate|auto-103|<tuple|Spontaneous process|?>>
    <associate|auto-104|<tuple|Process|?>>
    <associate|auto-105|<tuple|3.2.2|?>>
    <associate|auto-106|<tuple|3.3|?>>
    <associate|auto-107|<tuple|<tuple|heat|transfer>|?>>
    <associate|auto-108|<tuple|3.3.1|?>>
    <associate|auto-109|<tuple|3.3.1|?>>
    <associate|auto-11|<tuple|Adiabatic|?>>
    <associate|auto-110|<tuple|3.3.2|?>>
    <associate|auto-111|<tuple|Equilibrium|?>>
    <associate|auto-112|<tuple|Phase|?>>
    <associate|auto-113|<tuple|Reversible|?>>
    <associate|auto-114|<tuple|<tuple|heat|transfer>|?>>
    <associate|auto-115|<tuple|3.4|?>>
    <associate|auto-116|<tuple|<tuple|work|deformation>|?>>
    <associate|auto-117|<tuple|<tuple|deformation|work>|?>>
    <associate|auto-118|<tuple|Local frame|?>>
    <associate|auto-119|<tuple|Frame|?>>
    <associate|auto-12|<tuple|Process|?>>
    <associate|auto-120|<tuple|3.4.1|?>>
    <associate|auto-121|<tuple|3.4.1|?>>
    <associate|auto-122|<tuple|Frictional force|?>>
    <associate|auto-123|<tuple|Force|?>>
    <associate|auto-124|<tuple|Newton's second law of motion|?>>
    <associate|auto-125|<tuple|Reversible|?>>
    <associate|auto-126|<tuple|3.4.2|?>>
    <associate|auto-127|<tuple|<tuple|expansion|work>|?>>
    <associate|auto-128|<tuple|<tuple|work|expansion>|?>>
    <associate|auto-129|<tuple|Expansion|?>>
    <associate|auto-13|<tuple|3.1.1|?>>
    <associate|auto-130|<tuple|Work|?>>
    <associate|auto-131|<tuple|expansion work|?>>
    <associate|auto-132|<tuple|Line integral|?>>
    <associate|auto-133|<tuple|Integral|?>>
    <associate|auto-134|<tuple|3.4.3|?>>
    <associate|auto-135|<tuple|Anisotropic phase|?>>
    <associate|auto-136|<tuple|Phase|?>>
    <associate|auto-137|<tuple|3.4.2|?>>
    <associate|auto-138|<tuple|3.4.4|?>>
    <associate|auto-139|<tuple|<tuple|work|deformation>|?>>
    <associate|auto-14|<tuple|<tuple|work>|?>>
    <associate|auto-140|<tuple|<tuple|deformation|work>|?>>
    <associate|auto-141|<tuple|3.5|?>>
    <associate|auto-142|<tuple|3.5.1|?>>
    <associate|auto-143|<tuple|<tuple|internal energy|ideal gas>|?>>
    <associate|auto-144|<tuple|Ideal gas|?>>
    <associate|auto-145|<tuple|Gas|?>>
    <associate|auto-146|<tuple|Ideal gas|?>>
    <associate|auto-147|<tuple|Gas|?>>
    <associate|auto-148|<tuple|3.5.2|?>>
    <associate|auto-149|<tuple|Reversible|?>>
    <associate|auto-15|<tuple|Local frame|?>>
    <associate|auto-150|<tuple|3.5.3|?>>
    <associate|auto-151|<tuple|Reversible|?>>
    <associate|auto-152|<tuple|<tuple|heat capacity|constant volume|ideal
    gas>|?>>
    <associate|auto-153|<tuple|Adiabat|?>>
    <associate|auto-154|<tuple|Isotherm|?>>
    <associate|auto-155|<tuple|3.5.1|?>>
    <associate|auto-156|<tuple|3.5.4|?>>
    <associate|auto-157|<tuple|Indicator diagram|?>>
    <associate|auto-158|<tuple|indicator diagram|?>>
    <associate|auto-159|<tuple|<tuple|pressure volume diagram>|?>>
    <associate|auto-16|<tuple|Frame|?>>
    <associate|auto-160|<tuple|Expansion|?>>
    <associate|auto-161|<tuple|Work|?>>
    <associate|auto-162|<tuple|Reversible|?>>
    <associate|auto-163|<tuple|3.5.2|?>>
    <associate|auto-164|<tuple|3.5.5|?>>
    <associate|auto-165|<tuple|3.5.6|?>>
    <associate|auto-166|<tuple|Free expansion|?>>
    <associate|auto-167|<tuple|Expansion|?>>
    <associate|auto-168|<tuple|free expansion|?>>
    <associate|auto-169|<tuple|3.5.3|?>>
    <associate|auto-17|<tuple|External field|?>>
    <associate|auto-170|<tuple|<tuple|expansion|work>|?>>
    <associate|auto-171|<tuple|<tuple|work|expansion>|?>>
    <associate|auto-172|<tuple|3.6|?>>
    <associate|auto-173|<tuple|<tuple|work|gravitational>|?>>
    <associate|auto-174|<tuple|<tuple|gravitational|work>|?>>
    <associate|auto-175|<tuple|Local frame|?>>
    <associate|auto-176|<tuple|Frame|?>>
    <associate|auto-177|<tuple|Lab frame|?>>
    <associate|auto-178|<tuple|Frame|?>>
    <associate|auto-179|<tuple|3.6.1|?>>
    <associate|auto-18|<tuple|Field|?>>
    <associate|auto-180|<tuple|Energy|?>>
    <associate|auto-181|<tuple|Dissipation of energy|?>>
    <associate|auto-182|<tuple|<tuple|work|gravitational>|?>>
    <associate|auto-183|<tuple|<tuple|gravitational|work>|?>>
    <associate|auto-184|<tuple|3.7|?>>
    <associate|auto-185|<tuple|<tuple|shaft work>|?>>
    <associate|auto-186|<tuple|<tuple|work|shaft>|?>>
    <associate|auto-187|<tuple|Shaft work|?>>
    <associate|auto-188|<tuple|3.7.1|?>>
    <associate|auto-189|<tuple|Torque|?>>
    <associate|auto-19|<tuple|Lab frame|?>>
    <associate|auto-190|<tuple|3.7.2|?>>
    <associate|auto-191|<tuple|3.7.1|?>>
    <associate|auto-192|<tuple|<tuple|work|stirring>|?>>
    <associate|auto-193|<tuple|<tuple|stirring work>|?>>
    <associate|auto-194|<tuple|Work|?>>
    <associate|auto-195|<tuple|Stirring work|?>>
    <associate|auto-196|<tuple|stirring work|?>>
    <associate|auto-197|<tuple|Dissipative work|?>>
    <associate|auto-198|<tuple|Work|?>>
    <associate|auto-199|<tuple|dissipative work|?>>
    <associate|auto-2|<tuple|Law|?>>
    <associate|auto-20|<tuple|Frame|?>>
    <associate|auto-200|<tuple|Energy|?>>
    <associate|auto-201|<tuple|Dissipation of energy|?>>
    <associate|auto-202|<tuple|<tuple|work|stirring>|?>>
    <associate|auto-203|<tuple|<tuple|stirring work>|?>>
    <associate|auto-204|<tuple|3.7.2|?>>
    <associate|auto-205|<tuple|<tuple|joule|paddle wheel>|?>>
    <associate|auto-206|<tuple|<tuple|paddle wheel|joule>|?>>
    <associate|auto-207|<tuple|Joule, James Prescott|?>>
    <associate|auto-208|<tuple|3.7.3|?>>
    <associate|auto-209|<tuple|Joule, James Prescott|?>>
    <associate|auto-21|<tuple|Joule, James Prescott|?>>
    <associate|auto-210|<tuple|3.7.4|?>>
    <associate|auto-211|<tuple|Joule, James Prescott|?>>
    <associate|auto-212|<tuple|Calorie|?>>
    <associate|auto-213|<tuple|<tuple|joule|paddle wheel>|?>>
    <associate|auto-214|<tuple|<tuple|paddle wheel|joule>|?>>
    <associate|auto-215|<tuple|<tuple|shaft work>|?>>
    <associate|auto-216|<tuple|<tuple|work|shaft>|?>>
    <associate|auto-217|<tuple|1|?|bio-JOULE.tm>>
    <associate|auto-218|<tuple|Joule, James Prescott|?|bio-JOULE.tm>>
    <associate|auto-219|<tuple|3.8|?>>
    <associate|auto-22|<tuple|Local frame|?>>
    <associate|auto-220|<tuple|<tuple|electrical|work>|?>>
    <associate|auto-221|<tuple|<tuple|work|electrical>|?>>
    <associate|auto-222|<tuple|3.8.1|?>>
    <associate|auto-223|<tuple|Electric|?>>
    <associate|auto-224|<tuple|Current, electric|?>>
    <associate|auto-225|<tuple|Electrical|?>>
    <associate|auto-226|<tuple|Circuit|?>>
    <associate|auto-227|<tuple|Electrical|?>>
    <associate|auto-228|<tuple|Circuit|?>>
    <associate|auto-229|<tuple|Electric|?>>
    <associate|auto-23|<tuple|Frame|?>>
    <associate|auto-230|<tuple|Electric|?>>
    <associate|auto-231|<tuple|Current, electric|?>>
    <associate|auto-232|<tuple|Electric|?>>
    <associate|auto-233|<tuple|Circuit|?>>
    <associate|auto-234|<tuple|Electrical|?>>
    <associate|auto-235|<tuple|Circuit|?>>
    <associate|auto-236|<tuple|3.8.2|?>>
    <associate|auto-237|<tuple|<tuple|electrical|heating>|?>>
    <associate|auto-238|<tuple|<tuple|heating|electrical>|?>>
    <associate|auto-239|<tuple|Electrical|?>>
    <associate|auto-24|<tuple|Lab frame|?>>
    <associate|auto-240|<tuple|Resistor, electrical|?>>
    <associate|auto-241|<tuple|Electric|?>>
    <associate|auto-242|<tuple|Electrical|?>>
    <associate|auto-243|<tuple|Resistor, electrical|?>>
    <associate|auto-244|<tuple|3.8.1|?>>
    <associate|auto-245|<tuple|Electrical|?>>
    <associate|auto-246|<tuple|Circuit|?>>
    <associate|auto-247|<tuple|Electric|?>>
    <associate|auto-248|<tuple|Resistance|?>>
    <associate|auto-249|<tuple|3.8.2|?>>
    <associate|auto-25|<tuple|Frame|?>>
    <associate|auto-250|<tuple|Dissipative work|?>>
    <associate|auto-251|<tuple|Work|?>>
    <associate|auto-252|<tuple|Heat|?>>
    <associate|auto-253|<tuple|<tuple|electrical|heating>|?>>
    <associate|auto-254|<tuple|<tuple|heating|electrical>|?>>
    <associate|auto-255|<tuple|3.8.3|?>>
    <associate|auto-256|<tuple|Galvanic cell|?>>
    <associate|auto-257|<tuple|3.8.3|?>>
    <associate|auto-258|<tuple|Cell potential|?>>
    <associate|auto-259|<tuple|Equilibrium cell potential|?>>
    <associate|auto-26|<tuple|Lab frame|?>>
    <associate|auto-260|<tuple|Cell potential|?>>
    <associate|auto-261|<tuple|Electrical|?>>
    <associate|auto-262|<tuple|Resistor, electrical|?>>
    <associate|auto-263|<tuple|Energy|?>>
    <associate|auto-264|<tuple|Dissipation of energy|?>>
    <associate|auto-265|<tuple|3.8.4|?>>
    <associate|auto-266|<tuple|Energy|?>>
    <associate|auto-267|<tuple|Dissipation of energy|?>>
    <associate|auto-268|<tuple|<tuple|electrical|work>|?>>
    <associate|auto-269|<tuple|<tuple|work|electrical>|?>>
    <associate|auto-27|<tuple|Frame|?>>
    <associate|auto-270|<tuple|3.9|?>>
    <associate|auto-271|<tuple|<tuple|internal|friction>|?>>
    <associate|auto-272|<tuple|<tuple|friction|internal>|?>>
    <associate|auto-273|<tuple|Minimal work principle|?>>
    <associate|auto-274|<tuple|Energy|?>>
    <associate|auto-275|<tuple|Dissipation of energy|?>>
    <associate|auto-276|<tuple|Energy|?>>
    <associate|auto-277|<tuple|Dissipation of energy|?>>
    <associate|auto-278|<tuple|Dissipative work|?>>
    <associate|auto-279|<tuple|Work|?>>
    <associate|auto-28|<tuple|Center-of-mass frame|?>>
    <associate|auto-280|<tuple|3.9.1|?>>
    <associate|auto-281|<tuple|Energy|?>>
    <associate|auto-282|<tuple|Dissipation of energy|?>>
    <associate|auto-283|<tuple|Friction|?>>
    <associate|auto-284|<tuple|3.9.2|?>>
    <associate|auto-285|<tuple|Energy|?>>
    <associate|auto-286|<tuple|Dissipation of energy|?>>
    <associate|auto-287|<tuple|Energy|?>>
    <associate|auto-288|<tuple|Dissipation of energy|?>>
    <associate|auto-289|<tuple|3.9.3|?>>
    <associate|auto-29|<tuple|Frame|?>>
    <associate|auto-290|<tuple|Friction|?>>
    <associate|auto-291|<tuple|<tuple|internal|friction>|?>>
    <associate|auto-292|<tuple|<tuple|friction|internal>|?>>
    <associate|auto-293|<tuple|3.10|?>>
    <associate|auto-294|<tuple|<tuple|reversible|process>|?>>
    <associate|auto-295|<tuple|<tuple|process|reversible>|?>>
    <associate|auto-296|<tuple|Reversible|?>>
    <associate|auto-297|<tuple|Work|?>>
    <associate|auto-298|<tuple|Energy|?>>
    <associate|auto-299|<tuple|Dissipation of energy|?>>
    <associate|auto-3|<tuple|law|?>>
    <associate|auto-30|<tuple|Lab frame|?>>
    <associate|auto-300|<tuple|Dissipative work|?>>
    <associate|auto-301|<tuple|Work|?>>
    <associate|auto-302|<tuple|Energy|?>>
    <associate|auto-303|<tuple|Dissipation of energy|?>>
    <associate|auto-304|<tuple|Process|?>>
    <associate|auto-305|<tuple|Adiabatic|?>>
    <associate|auto-306|<tuple|Energy|?>>
    <associate|auto-307|<tuple|Dissipation of energy|?>>
    <associate|auto-308|<tuple|Triple|?>>
    <associate|auto-309|<tuple|Work|?>>
    <associate|auto-31|<tuple|Frame|?>>
    <associate|auto-310|<tuple|Work|?>>
    <associate|auto-311|<tuple|Expansion|?>>
    <associate|auto-312|<tuple|Work|?>>
    <associate|auto-313|<tuple|Work|?>>
    <associate|auto-314|<tuple|Work|?>>
    <associate|auto-315|<tuple|Gravitational|?>>
    <associate|auto-316|<tuple|Work|?>>
    <associate|auto-317|<tuple|Electrical|?>>
    <associate|auto-318|<tuple|Work|?>>
    <associate|auto-319|<tuple|<tuple|work|electric polarization>|?>>
    <associate|auto-32|<tuple|<tuple|work>|?>>
    <associate|auto-320|<tuple|<tuple|work|magnetization>|?>>
    <associate|auto-321|<tuple|<tuple|work|magnetization>|?>>
    <associate|auto-322|<tuple|<tuple|reversible|process>|?>>
    <associate|auto-323|<tuple|<tuple|process|reversible>|?>>
    <associate|auto-33|<tuple|3.1.2|?>>
    <associate|auto-34|<tuple|Work|?>>
    <associate|auto-35|<tuple|work coefficient|?>>
    <associate|auto-36|<tuple|Work|?>>
    <associate|auto-37|<tuple|work coordinate|?>>
    <associate|auto-38|<tuple|Conjugate|?>>
    <associate|auto-39|<tuple|3.1.3|?>>
    <associate|auto-4|<tuple|3.1|?>>
    <associate|auto-40|<tuple|3.1.1|?>>
    <associate|auto-41|<tuple|Paddle wheel|?>>
    <associate|auto-42|<tuple|Electrical|?>>
    <associate|auto-43|<tuple|Resistor, electrical|?>>
    <associate|auto-44|<tuple|Electrical|?>>
    <associate|auto-45|<tuple|Work|?>>
    <associate|auto-46|<tuple|Heat|?>>
    <associate|auto-47|<tuple|Path function|?>>
    <associate|auto-48|<tuple|3.1.4|?>>
    <associate|auto-49|<tuple|Heat|?>>
    <associate|auto-5|<tuple|First law of thermodynamics|?>>
    <associate|auto-50|<tuple|Caloric theory|?>>
    <associate|auto-51|<tuple|Thompson, Benjamin|?>>
    <associate|auto-52|<tuple|Rumford, Count|?>>
    <associate|auto-53|<tuple|Joule, James Prescott|?>>
    <associate|auto-54|<tuple|3.1.5|?>>
    <associate|auto-55|<tuple|Heat capacity|?>>
    <associate|auto-56|<tuple|heat capacity|?>>
    <associate|auto-57|<tuple|<tuple|heat capacity|constant volume>|?>>
    <associate|auto-58|<tuple|<tuple|heat capacity|constant pressure>|?>>
    <associate|auto-59|<tuple|3.1.6|?>>
    <associate|auto-6|<tuple|first law of thermodynamics|?>>
    <associate|auto-60|<tuple|Energy|?>>
    <associate|auto-61|<tuple|Thermal|?>>
    <associate|auto-62|<tuple|thermal energy|?>>
    <associate|auto-63|<tuple|1|?|bio-RUMFORD.tm>>
    <associate|auto-64|<tuple|Rumford, Count|?|bio-RUMFORD.tm>>
    <associate|auto-65|<tuple|Thompson, Benjamin|?|bio-RUMFORD.tm>>
    <associate|auto-66|<tuple|3.2|?>>
    <associate|auto-67|<tuple|Spontaneous process|?>>
    <associate|auto-68|<tuple|Process|?>>
    <associate|auto-69|<tuple|spontaneous process|?>>
    <associate|auto-7|<tuple|Heat|?>>
    <associate|auto-70|<tuple|3.2.1|?>>
    <associate|auto-71|<tuple|<tuple|reversible|process>|?>>
    <associate|auto-72|<tuple|<tuple|process|reversible>|?>>
    <associate|auto-73|<tuple|reversible process|?>>
    <associate|auto-74|<tuple|Reverse of a process|?>>
    <associate|auto-75|<tuple|Process|?>>
    <associate|auto-76|<tuple|Process|?>>
    <associate|auto-77|<tuple|Spontaneous process|?>>
    <associate|auto-78|<tuple|Process|?>>
    <associate|auto-79|<tuple|Quasistatic process|?>>
    <associate|auto-8|<tuple|Work|?>>
    <associate|auto-80|<tuple|Process|?>>
    <associate|auto-81|<tuple|Spontaneous process|?>>
    <associate|auto-82|<tuple|Process|?>>
    <associate|auto-83|<tuple|Spontaneous process|?>>
    <associate|auto-84|<tuple|Dissipation of energy|?>>
    <associate|auto-85|<tuple|Energy|?>>
    <associate|auto-86|<tuple|3.2.2|?>>
    <associate|auto-87|<tuple|3.2.1|?>>
    <associate|auto-88|<tuple|Reversibility|?>>
    <associate|auto-89|<tuple|<tuple|reversible|process>|?>>
    <associate|auto-9|<tuple|Local frame|?>>
    <associate|auto-90|<tuple|<tuple|process|reversible>|?>>
    <associate|auto-91|<tuple|3.2.3|?>>
    <associate|auto-92|<tuple|Irreversible process|?>>
    <associate|auto-93|<tuple|Process|?>>
    <associate|auto-94|<tuple|irreversible|?>>
    <associate|auto-95|<tuple|Process|?>>
    <associate|auto-96|<tuple|Spontaneous process|?>>
    <associate|auto-97|<tuple|Process|?>>
    <associate|auto-98|<tuple|Impossible process|?>>
    <associate|auto-99|<tuple|Energy|?>>
    <associate|bio:joule|<tuple|1|?|bio-JOULE.tm>>
    <associate|bio:rumford|<tuple|1|?|bio-RUMFORD.tm>>
    <associate|c3|<tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Chapter
    3>|?>>
    <associate|c3 fig paddle-heater|<tuple|3.1.1|?>>
    <associate|c3 sec aew|<tuple|3.5|?>>
    <associate|c3 sec aew-ideal-u|<tuple|3.5.1|?>>
    <associate|c3 sec aew-indicator|<tuple|3.5.4|?>>
    <associate|c3 sec aew-vacuum|<tuple|3.5.6|?>>
    <associate|c3 sec dw|<tuple|3.4|?>>
    <associate|c3 sec dw-expansion-gas|<tuple|3.4.2|?>>
    <associate|c3 sec dw-generalities|<tuple|3.4.4|?>>
    <associate|c3 sec dw-piston|<tuple|3.4.1|?>>
    <associate|c3 sec ew|<tuple|3.8|?>>
    <associate|c3 sec ew-circuit|<tuple|3.8.1|?>>
    <associate|c3 sec ew-galvanic|<tuple|3.8.3|?>>
    <associate|c3 sec ew-heating|<tuple|3.8.2|?>>
    <associate|c3 sec ht|<tuple|3.3|?>>
    <associate|c3 sec ht-heating-cooling|<tuple|3.3.1|?>>
    <associate|c3 sec hwfl|<tuple|3.1|?>>
    <associate|c3 sec hwfl-energy-thermal|<tuple|3.1.6|?>>
    <associate|c3 sec hwfl-heat|<tuple|3.1.4|?>>
    <associate|c3 sec hwfl-heat-capacity|<tuple|3.1.5|?>>
    <associate|c3 sec hwfl-work|<tuple|3.1.1|?>>
    <associate|c3 sec hwfl-work-coeff|<tuple|3.1.2|?>>
    <associate|c3 sec hwfl-work-path|<tuple|3.1.3|?>>
    <associate|c3 sec iwif|<tuple|3.9|?>>
    <associate|c3 sec srip-processes-reversible|<tuple|3.2.1|?>>
    <associate|c3 sec wgf|<tuple|3.6|?>>
    <associate|c3 sec wgf-paddle|<tuple|3.7.2|?>>
    <associate|c3 sec wgf-shaft|<tuple|3.7|?>>
    <associate|c3 sec wgf-stirring|<tuple|3.7.1|?>>
    <associate|c3 sec-aew-ideal-adiabatic|<tuple|3.5.3|?>>
    <associate|c3 sec-aew-ideal-isothermal|<tuple|3.5.2|?>>
    <associate|c3 sec-aew-spontaneous-adiabatic|<tuple|3.5.5|?>>
    <associate|c3 sec-dw-expansion-isotropic|<tuple|3.4.3|?>>
    <associate|c3 sec-ht-phase-transitions|<tuple|3.3.2|?>>
    <associate|c3 sec-ripg|<tuple|3.10|?>>
    <associate|c3 sec-srip|<tuple|3.2|?>>
    <associate|c3 sec-srip-processes-irreversible|<tuple|3.2.3|?>>
    <associate|c3 sec-srip-processes-mechanical|<tuple|3.2.4|?>>
    <associate|c3 sec-srip-reversibility|<tuple|3.2.2|?>>
    <associate|cell pot defn|<tuple|3.8.6|?>>
    <associate|dU=C_V dT (ig)|<tuple|3.5.3|?>>
    <associate|dissipative work definition|<tuple|dissipative work|?>>
    <associate|dw(el)=E(cell)dQ(cell)|<tuple|3.8.8|?>>
    <associate|dw(el,rev)=|<tuple|3.8.9|?>>
    <associate|dw(tau)=F(sur)cos(alpha)ds|<tuple|3.4.1|?>>
    <associate|dw=(F_buoy+F_fric)dh|<tuple|3.6.1|?>>
    <associate|dw=(F_buoy+F_fric+F_str)dh=|<tuple|3.6.5|?>>
    <associate|dw=(mg-F_buoy-F_fric+mdv/dt)dh|<tuple|3.6.3|?>>
    <associate|dw=-Fx(sys)dx w=|<tuple|3.1.2|?>>
    <associate|dw=-p(b)Adx|<tuple|3.4.11|?>>
    <associate|dw=-p(b)dV|<tuple|3.4.12|?>>
    <associate|dw=-pdV|<tuple|3.4.14|?>>
    <associate|dw=F(sur)dx w=|<tuple|3.1.1|?>>
    <associate|dw=I*del(phi)dt|<tuple|3.8.3|?>>
    <associate|dw=I2*Rdt|<tuple|3.8.5|?>>
    <associate|dw=IRdQ(el)|<tuple|3.8.4|?>>
    <associate|dw=YdX|<tuple|3.1.7|?>>
    <associate|dw=del(phi)dQ(el)|<tuple|3.8.1|?>>
    <associate|dw=sum Y(i)dX(i)|<tuple|3.1.8|?>>
    <associate|el pot defn|<tuple|<tuple|work|electrical>|?>>
    <associate|fig:3-Joule apparatus|<tuple|3.7.4|?>>
    <associate|fig:3-Joule paddle|<tuple|3.7.3|?>>
    <associate|fig:3-adiabat and isotherms|<tuple|3.5.1|?>>
    <associate|fig:3-cylinder|<tuple|3.4.1|?>>
    <associate|fig:3-deformation|<tuple|3.4.2|?>>
    <associate|fig:3-el heating|<tuple|3.8.1|?>>
    <associate|fig:3-el rates|<tuple|3.8.2|?>>
    <associate|fig:3-external friction|<tuple|3.2.1|?>>
    <associate|fig:3-free expansion|<tuple|3.5.3|?>>
    <associate|fig:3-friction|<tuple|3.9.1|?>>
    <associate|fig:3-galv cell|<tuple|3.8.3|?>>
    <associate|fig:3-galv cell work|<tuple|3.8.4|?>>
    <associate|fig:3-gas-dissip|<tuple|3.9.2|?>>
    <associate|fig:3-grav work|<tuple|3.6.1|?>>
    <associate|fig:3-indicator|<tuple|3.5.2|?>>
    <associate|fig:3-mechanical processes|<tuple|3.2.2|?>>
    <associate|fig:3-paddle \ heater|<tuple|3.1.1|?>>
    <associate|fig:3-rates-dissip|<tuple|3.9.3|?>>
    <associate|fig:3-rates-shaft_work|<tuple|3.7.2|?>>
    <associate|fig:3-shaft work|<tuple|3.7.1|?>>
    <associate|fig:3-thermal sphere|<tuple|3.3.1|?>>
    <associate|first law|<tuple|first law of thermodynamics|?>>
    <associate|footnote-3.1.1|<tuple|3.1.1|?>>
    <associate|footnote-3.1.2|<tuple|3.1.2|?>>
    <associate|footnote-3.1.3|<tuple|3.1.3|?>>
    <associate|footnote-3.1.4|<tuple|3.1.4|?>>
    <associate|footnote-3.1.5|<tuple|3.1.5|?>>
    <associate|footnote-3.1.6|<tuple|3.1.6|?>>
    <associate|footnote-3.1.7|<tuple|3.1.7|?>>
    <associate|footnote-3.1.8|<tuple|3.1.8|?|bio-RUMFORD.tm>>
    <associate|footnote-3.10.1|<tuple|3.10.1|?>>
    <associate|footnote-3.10.2|<tuple|3.10.2|?>>
    <associate|footnote-3.2.1|<tuple|3.2.1|?>>
    <associate|footnote-3.2.2|<tuple|3.2.2|?>>
    <associate|footnote-3.2.3|<tuple|3.2.3|?>>
    <associate|footnote-3.2.4|<tuple|3.2.4|?>>
    <associate|footnote-3.2.5|<tuple|3.2.5|?>>
    <associate|footnote-3.4.1|<tuple|3.4.1|?>>
    <associate|footnote-3.4.2|<tuple|3.4.2|?>>
    <associate|footnote-3.4.3|<tuple|3.4.3|?>>
    <associate|footnote-3.5.1|<tuple|3.5.1|?>>
    <associate|footnote-3.5.2|<tuple|3.5.2|?>>
    <associate|footnote-3.5.3|<tuple|3.5.3|?>>
    <associate|footnote-3.5.4|<tuple|3.5.4|?>>
    <associate|footnote-3.6.1|<tuple|3.6.1|?>>
    <associate|footnote-3.6.2|<tuple|3.6.2|?>>
    <associate|footnote-3.7.1|<tuple|3.7.1|?>>
    <associate|footnote-3.7.2|<tuple|3.7.2|?>>
    <associate|footnote-3.7.3|<tuple|3.7.3|?>>
    <associate|footnote-3.7.4|<tuple|3.7.4|?|bio-JOULE.tm>>
    <associate|footnote-3.7.5|<tuple|3.7.5|?|bio-JOULE.tm>>
    <associate|footnote-3.7.6|<tuple|3.7.6|?|bio-JOULE.tm>>
    <associate|footnote-3.9.1|<tuple|3.9.1|?>>
    <associate|footnr-3.1.1|<tuple|3.1.1|?>>
    <associate|footnr-3.1.2|<tuple|3.1.2|?>>
    <associate|footnr-3.1.3|<tuple|3.1.3|?>>
    <associate|footnr-3.1.4|<tuple|3.1.4|?>>
    <associate|footnr-3.1.5|<tuple|3.1.5|?>>
    <associate|footnr-3.1.6|<tuple|3.1.6|?>>
    <associate|footnr-3.1.7|<tuple|3.1.7|?>>
    <associate|footnr-3.1.8|<tuple|3.1.8|?|bio-RUMFORD.tm>>
    <associate|footnr-3.10.1|<tuple|3.10.1|?>>
    <associate|footnr-3.10.2|<tuple|Triple|?>>
    <associate|footnr-3.2.1|<tuple|3.2.1|?>>
    <associate|footnr-3.2.2|<tuple|3.2.2|?>>
    <associate|footnr-3.2.3|<tuple|3.2.3|?>>
    <associate|footnr-3.2.4|<tuple|3.2.4|?>>
    <associate|footnr-3.2.5|<tuple|3.2.5|?>>
    <associate|footnr-3.4.1|<tuple|3.4.1|?>>
    <associate|footnr-3.4.2|<tuple|3.4.2|?>>
    <associate|footnr-3.4.3|<tuple|3.4.3|?>>
    <associate|footnr-3.5.1|<tuple|Gas|?>>
    <associate|footnr-3.5.2|<tuple|3.5.2|?>>
    <associate|footnr-3.5.4|<tuple|3.5.4|?>>
    <associate|footnr-3.6.1|<tuple|3.6.1|?>>
    <associate|footnr-3.6.2|<tuple|3.6.2|?>>
    <associate|footnr-3.7.1|<tuple|3.7.1|?>>
    <associate|footnr-3.7.2|<tuple|3.7.2|?>>
    <associate|footnr-3.7.3|<tuple|Calorie|?>>
    <associate|footnr-3.7.4|<tuple|3.7.4|?|bio-JOULE.tm>>
    <associate|footnr-3.7.5|<tuple|3.7.5|?|bio-JOULE.tm>>
    <associate|footnr-3.7.6|<tuple|3.7.6|?|bio-JOULE.tm>>
    <associate|footnr-3.9.1|<tuple|3.9.1|?>>
    <associate|free-falling body|<tuple|3.6.1|?>>
    <associate|grav work with Del E(k)=0|<tuple|3.6.5|?>>
    <associate|heat and work recovered|<tuple|<with|mode|<quote|math>|\<bullet\>>|?>>
    <associate|heat capacity def|<tuple|3.1.9|?>>
    <associate|ind var rule|<tuple|3.4.4|?>>
    <associate|int F_x=|<tuple|3.4.5|?>>
    <associate|minimal work principle|<tuple|Adiabatic|?>>
    <associate|no rev limit in isolated system|<tuple|<with|mode|<quote|math>|\<bullet\>>|?>>
    <associate|p(b)=p(...)|<tuple|3.4.10|?>>
    <associate|p2= (ad rev expan of id gas)|<tuple|3.5.14|?>>
    <associate|paddle wheel experiment|<tuple|Experiment 1|?>>
    <associate|part:bio-JOULE.tm|<tuple|<tuple|work|shaft>|?>>
    <associate|part:bio-RUMFORD.tm|<tuple|thermal energy|?>>
    <associate|rev work and state functions|<tuple|Dissipation of energy|?>>
    <associate|reverse paddle wheel|<tuple|Dissipation of energy|?>>
    <associate|t2-t1=|<tuple|3.4.7|?>>
    <associate|tbl:3-work|<tuple|3.10.1|?>>
    <associate|third experiment|<tuple|Experiment 3|?>>
    <associate|uniformity rule|<tuple|3.4.15|?>>
    <associate|vx2=-vx1+2u|<tuple|3.4.4|?>>
    <associate|w approx equal to w(lab)|<tuple|3.1.4|?>>
    <associate|w-w(lab) = -mg del z(loc)|<tuple|3.1.6|?>>
    <associate|w-w(lab)=|<tuple|3.1.4|?>>
    <associate|w=(mg-F_buoy)del h-int(F_fric)dh|<tuple|3.6.4|?>>
    <associate|w=-int F(sys) dx|<tuple|3.9.1|?>>
    <associate|w=-int(friction)|<tuple|3.9.2|?>>
    <associate|w=-int(p)dV|<tuple|3.4.15|?>>
    <associate|w=-int(pb)dV|<tuple|3.4.13|?>>
    <associate|w=-nRT ln(V2/V1)|<tuple|3.5.1|?>>
    <associate|w=int[tau(b)d(theta)]|<tuple|3.7.1|?>>
    <associate|work from external weight|<tuple|3.1.2|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      rumford-1798

      fermi-56

      macdougall-39

      zemansky-97

      devoe-13

      hats-65

      sonntag-82

      deheer-86

      bertrand-05

      bauman-69

      bauman-64

      chesick-64

      bauman-64a

      kokes-64

      bauman-64b

      mysels-64

      kivelson-66

      maxwell-1888

      joule-1850

      joule-1850

      bottomley-1882

      joule-1892
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.1.1>|>
        System containing an electrical resistor and a paddle wheel immersed
        in water. Cross-hatched area: removable thermal insulation.
      </surround>|<pageref|auto-40>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.2.1>|>
        Gas confined by a lubricated piston in a cylinder in contact with a
        heat reservoir (res).
      </surround>|<pageref|auto-87>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.2.2>|>
        Two purely mechanical processes that are the reverse of one another:
        a thrown ball moving through a vacuum (a)
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>to
        the right; (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>to
        the left.
      </surround>|<pageref|auto-105>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.3.1>|>
        Temperature profiles in a copper sphere of radius 5<space|0.17em>cm
        immersed in a water bath. The temperature at each of the times
        indicated is plotted as a function of <with|mode|<quote|math>|r>, the
        distance from the center of the sphere. The temperature at distances
        greater than 5<space|0.17em>cm, to the right of the vertical dashed
        line in each graph, is that of the external water bath.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Bath
        temperature raised at the rate of <with|mode|<quote|math>|0.10
        <with|mode|<quote|text>|K>\<cdot\><with|mode|<quote|text>|s><rsup|-1>>.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Bath
        temperature raised infinitely slowly.

        \ (c) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Bath
        temperature lowered at the rate of <with|mode|<quote|math>|0.10
        <with|mode|<quote|text>|K>\<cdot\><with|mode|<quote|text>|s><rsup|-1>>.
      </surround>|<pageref|auto-109>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.4.1>|>
        Forces acting on the piston (cross hatched) in a cylinder-and-piston
        device containing a gas (shaded). The direction of
        <with|mode|<quote|math>|F<rsub|<with|mode|<quote|text>|fric>>> shown
        here is for expansion.
      </surround>|<pageref|auto-121>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.4.2>|>
        Deformation of an isotropic phase (shaded) confined by a wall.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Equal
        and opposite forces exerted by the surroundings and system at surface
        element <with|mode|<quote|math>|\<tau\>> (thick curve) of the system
        boundary.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Change
        from initial volume (dotted curve) to a smaller volume.
      </surround>|<pageref|auto-137>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.5.1>|>
        An adiabat (solid curve) and four isotherms (dashed curves) for an
        ideal gas (<with|mode|<quote|math>|n=0.0120
        <with|mode|<quote|text>|mol>>, <with|mode|<quote|math>|C<rsub|V,<with|mode|<quote|text>|m>>=1.5*R>).
      </surround>|<pageref|auto-155>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.5.2>|>
        Indicator with paper-covered roll at left and pressure gauge at
        right.<space|.15em><assign|footnote-nr|3><hidden-binding|<tuple>|3.5.3><\float|footnote|>
          <with|font-size|<quote|0.771>|<with|par-mode|<quote|justify>|par-left|<quote|0cm>|par-right|<quote|0cm>|font-shape|<quote|right>|dummy|<quote|1.0fn>|dummy|<quote|7.5fn>|<surround|<locus|<id|%7FDD4F448-7D2383B88>|<link|hyperlink|<id|%7FDD4F448-7D2383B88>|<url|#footnr-3.5.3>>|3.5.3>.
          |<hidden-binding|<tuple|footnote-3.5.3>|3.5.3><specific|texmacs|<htab|0fn|first>>|Ref.
          [<write|bib|maxwell-1888><reference|bib-maxwell-1888>], page
          104.>>>
        </float><space|0spc><rsup|<with|font-shape|<quote|right>|<reference|footnote-3.5.3>>>
      </surround>|<pageref|auto-163>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.5.3>|>
        Free expansion into a vacuum.
      </surround>|<pageref|auto-169>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.6.1>|>
        Spherical body (dark gray) in a gravitational field. The arrows
        indicate the directions and magnitudes of contact and gravitational
        forces exerted on the body.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>The
        body falls freely through a fluid.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>The
        body is lowered on a string through the fluid.
      </surround>|<pageref|auto-179>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.7.1>|>
        Two systems with shaft work. The dashed rectangles indicate the
        system boundaries. System A has an internal weight, cord, and pulley
        wheel in air; system B has a stirrer immersed in water.
      </surround>|<pageref|auto-188>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.7.2>|>
        Shaft work <with|mode|<quote|math>|w> for a fixed magnitude of shaft
        rotation <with|mode|<quote|math>|\<Delta\>*\<vartheta\>> as a
        function of the angular velocity <with|mode|<quote|math>|\<omega\>=<with|mode|<quote|math>|<with|mode|<quote|text>|d>*\<vartheta\>>/<with|mode|<quote|math>|<with|mode|<quote|text>|d>*t>>.
        The open circles indicate work in the limit of infinite slowness. (a)
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>System
        A of Fig. <reference|fig:3-shaft work>. (b)
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>System
        B of Fig. <reference|fig:3-shaft work>.
      </surround>|<pageref|auto-190>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.7.3>|>
        Joule paddle wheel.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Joule's
        original paddle wheel on exhibit at the Science Museum, London.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Cross-section
        elevation of paddle wheel and water in copper vessel. Dark shading:
        rotating shaft and paddle arms; light shading: stationary vanes.
      </surround>|<pageref|auto-208>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.7.4>|>
        Joule's apparatus for measuring the mechanical equivalent of heat
        (redrawn from a figure in Ref. [<write|bib|joule-1850><reference|bib-joule-1850>]).

        Key: A<with|font|<quote|roman>|\V>paddle wheel and vessel (see Fig.
        <reference|fig:3-Joule paddle>); B<with|font|<quote|roman>|\V>wood
        thermal insulator; C<with|font|<quote|roman>|\V>pin used to engage
        paddle wheel shaft to roller; D<with|font|<quote|roman>|\V>roller;
        E<with|font|<quote|roman>|\V>crank used to wind up the weights; F,
        G<with|font|<quote|roman>|\V>strings; H,
        I<with|font|<quote|roman>|\V>pulley wheels; J,
        K<with|font|<quote|roman>|\V>weights (round lead disks, viewed here
        edge-on).
      </surround>|<pageref|auto-210>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.8.1>|>
        System containing an electrical resistor immersed in a liquid. The
        dashed rectangle indicates the system boundary.
      </surround>|<pageref|auto-244>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.8.2>|>
        Work of electrical heating with a fixed magnitude of
        <with|mode|<quote|math>|Q<rsub|<with|mode|<quote|text>|sys>>> as a
        function of the electric current <with|mode|<quote|math>|I=<with|mode|<quote|math>|<with|mode|<quote|text>|�>*Q<rsub|<with|mode|<quote|text>|sys>>>/<with|mode|<quote|math>|<with|mode|<quote|text>|d>*t>>.
        The open circle indicates the limit of infinite slowness.
      </surround>|<pageref|auto-249>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.8.3>|>
        Galvanic cell and external electrical resistor.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Open
        circuit with isolated cell in an equilibrium state.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Closed
        circuit.
      </surround>|<pageref|auto-257>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.8.4>|>
        Electrical work of a galvanic cell for a fixed magnitude of
        <with|mode|<quote|math>|Q<rsub|<with|mode|<quote|text>|sys>>> as a
        function of the electric current <with|mode|<quote|math>|I=<with|mode|<quote|math>|<with|mode|<quote|text>|�>*Q<rsub|<with|mode|<quote|text>|sys>>>/<with|mode|<quote|math>|<with|mode|<quote|text>|d>*t>>.
        Open circles: reversible limits.
      </surround>|<pageref|auto-265>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.9.1>|>
        Cylinder and piston with internal sliding friction. The dashed
        rectangle indicates the system boundary.
        P<with|font|<quote|roman>|\V>piston;
        R<with|font|<quote|roman>|\V>internal rod attached to the piston;
        B<with|font|<quote|roman>|\V>bushing fixed inside the cylinder. A
        fixed amount of gas fills the remaining space inside the cylinder.
      </surround>|<pageref|auto-280>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.9.2>|>
        Indicator diagrams for the system of Fig. <reference|fig:3-friction>
        with internal lubricated friction.

        \ Solid curves: <with|mode|<quote|math>|p<rsup|<with|mode|<quote|text>|sys>>>
        for irreversible adiabatic volume changes at finite rates in the
        directions indicated by the arrows.

        \ Dashed curves: <with|mode|<quote|math>|p<rsup|<with|mode|<quote|text>|sys>>=p>
        along a reversible adiabat.

        \ Open circles: initial and final equilibrium states.

        \ (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Adiabatic
        expansion.

        \ (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Adiabatic
        compression.
      </surround>|<pageref|auto-284>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.9.3>|>
        Adiabatic expansion work with internal lubricated friction for a
        fixed magnitude of <with|mode|<quote|math>|\<Delta\>*V>, as a
        function of the average rate of volume change. The open circles
        indicate the reversible limits.
      </surround>|<pageref|auto-289>>
    </associate>
    <\associate|gly>
      <tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Chapter
      3>>

      <tuple|normal|law|mathematical relation|<pageref|auto-3>>

      <tuple|normal|first law of thermodynamics|<pageref|auto-6>>

      <tuple|normal|work coefficient|<pageref|auto-35>>

      <tuple|normal|work coordinate|<pageref|auto-37>>

      <tuple|normal|heat capacity|<pageref|auto-56>>

      <tuple|normal|thermal energy|<pageref|auto-62>>

      <tuple|normal|spontaneous process|<pageref|auto-69>>

      <tuple|normal|reversible process|<pageref|auto-73>>

      <tuple|normal|irreversible|<pageref|auto-94>>

      <tuple|normal|expansion work|<pageref|auto-131>>

      <tuple|normal|indicator diagram|<pageref|auto-158>>

      <tuple|normal|free expansion|<pageref|auto-168>>

      <tuple|normal|Shaft work|<pageref|auto-187>>

      <tuple|normal|stirring work|<pageref|auto-196>>

      <tuple|normal|dissipative work|<pageref|auto-199>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Law|scientific>|<pageref|auto-2>>

      <tuple|<tuple|First law of thermodynamics>|<pageref|auto-5>>

      <tuple|<tuple|Heat>|<pageref|auto-7>>

      <tuple|<tuple|Work>|<pageref|auto-8>>

      <tuple|<tuple|Local frame>|<pageref|auto-9>>

      <tuple|<tuple|Frame|local>|<pageref|auto-10>>

      <tuple|<tuple|Adiabatic|process>|<pageref|auto-11>>

      <tuple|<tuple|Process|adiabatic>|<pageref|auto-12>>

      <tuple|<tuple|work>||c3 sec hwfl-work
      idx1|<tuple|Work>|<pageref|auto-14>>

      <tuple|<tuple|Local frame>|<pageref|auto-15>>

      <tuple|<tuple|Frame|local>|<pageref|auto-16>>

      <tuple|<tuple|External field>|<pageref|auto-17>>

      <tuple|<tuple|Field|external>|<pageref|auto-18>>

      <tuple|<tuple|Lab frame>|<pageref|auto-19>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-20>>

      <tuple|<tuple|Joule, James Prescott>|<pageref|auto-21>>

      <tuple|<tuple|Local frame>|<pageref|auto-22>>

      <tuple|<tuple|Frame|local>|<pageref|auto-23>>

      <tuple|<tuple|Lab frame>|<pageref|auto-24>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-25>>

      <tuple|<tuple|Lab frame>|<pageref|auto-26>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-27>>

      <tuple|<tuple|Center-of-mass frame>|<pageref|auto-28>>

      <tuple|<tuple|Frame|center-of-mass>|<pageref|auto-29>>

      <tuple|<tuple|Lab frame>|<pageref|auto-30>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-31>>

      <tuple|<tuple|work>||c3 sec hwfl-work
      idx1|<tuple|Work>|<pageref|auto-32>>

      <tuple|<tuple|Work|coefficient>|<pageref|auto-34>>

      <tuple|<tuple|Work|coordinate>|<pageref|auto-36>>

      <tuple|<tuple|Conjugate|variables>|<pageref|auto-38>>

      <tuple|<tuple|Paddle wheel>|<pageref|auto-41>>

      <tuple|<tuple|Electrical|resistor>|<pageref|auto-42>>

      <tuple|<tuple|Resistor, electrical>|<pageref|auto-43>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-44>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-45>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-46>>

      <tuple|<tuple|Path function>|<pageref|auto-47>>

      <tuple|<tuple|Heat|technical meaning of>|<pageref|auto-49>>

      <tuple|<tuple|Caloric theory>|<pageref|auto-50>>

      <tuple|<tuple|Thompson, Benjamin>|<pageref|auto-51>>

      <tuple|<tuple|Rumford, Count>|<pageref|auto-52>>

      <tuple|<tuple|Joule, James Prescott>|<pageref|auto-53>>

      <tuple|<tuple|Heat capacity>|<pageref|auto-55>>

      <tuple|<tuple|heat capacity|constant volume>|||<tuple|Heat capacity|at
      constant volume>|<pageref|auto-57>>

      <tuple|<tuple|heat capacity|constant pressure>|||<tuple|Heat
      capacity|at constant pressure>|<pageref|auto-58>>

      <tuple|<tuple|Energy|thermal>|<pageref|auto-60>>

      <tuple|<tuple|Thermal|energy>|<pageref|auto-61>>

      <tuple|<tuple|Rumford, Count>|<pageref|auto-64>>

      <tuple|<tuple|Thompson, Benjamin>|<pageref|auto-65>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-67>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-68>>

      <tuple|<tuple|reversible|process>||c3 sec srip-processes-reversible
      idx1|<tuple|Reversible|process>|<pageref|auto-71>>

      <tuple|<tuple|process|reversible>||c3 sec srip-processes-reversible
      idx2|<tuple|Process|reversible>|<pageref|auto-72>>

      <tuple|<tuple|Reverse of a process>|<pageref|auto-74>>

      <tuple|<tuple|Process|reverse of a>|<pageref|auto-75>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-76>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-77>>

      <tuple|<tuple|Process|quasistatic>|<pageref|auto-78>>

      <tuple|<tuple|Quasistatic process>|<pageref|auto-79>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-80>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-81>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-82>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-83>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-84>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-85>>

      <tuple|<tuple|Reversibility|internal>|<pageref|auto-88>>

      <tuple|<tuple|reversible|process>||c3 sec srip-processes-reversible
      idx1|<tuple|Reversible|process>|<pageref|auto-89>>

      <tuple|<tuple|process|reversible>||c3 sec srip-processes-reversible
      idx2|<tuple|Process|reversible>|<pageref|auto-90>>

      <tuple|<tuple|Irreversible process>|<pageref|auto-92>>

      <tuple|<tuple|Process|irreversible>|<pageref|auto-93>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-95>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-96>>

      <tuple|<tuple|Process|impossible>|<pageref|auto-97>>

      <tuple|<tuple|Impossible process>|<pageref|auto-98>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-99>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-100>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-102>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-103>>

      <tuple|<tuple|Process|purely mechanical>|<pageref|auto-104>>

      <tuple|<tuple|heat|transfer>||c3 sec ht
      idx1|<tuple|Heat|transfer>|<pageref|auto-107>>

      <tuple|<tuple|Equilibrium|phase transition>|<pageref|auto-111>>

      <tuple|<tuple|Phase|transition|equilibrium>|<pageref|auto-112>>

      <tuple|<tuple|Reversible|phase transition>|<pageref|auto-113>>

      <tuple|<tuple|heat|transfer>||c3 sec ht
      idx1|<tuple|Heat|transfer>|<pageref|auto-114>>

      <tuple|<tuple|work|deformation>||c3 sec dw
      idx1|<tuple|Work|deformation>|<pageref|auto-116>>

      <tuple|<tuple|deformation|work>||c3 sec dw
      idx2|<tuple|Deformation|work>|<pageref|auto-117>>

      <tuple|<tuple|Local frame>|<pageref|auto-118>>

      <tuple|<tuple|Frame|local>|<pageref|auto-119>>

      <tuple|<tuple|Frictional force>|<pageref|auto-122>>

      <tuple|<tuple|Force|frictional>|<pageref|auto-123>>

      <tuple|<tuple|Newton's second law of motion>|<pageref|auto-124>>

      <tuple|<tuple|Reversible|expansion and compression>|<pageref|auto-125>>

      <tuple|<tuple|expansion|work>||c3 sec dw-expansion-gas
      idx1|<tuple|Expansion|work>|<pageref|auto-127>>

      <tuple|<tuple|work|expansion>||c3 sec dw-expansion-gas
      idx2|<tuple|Work|expansion>|<pageref|auto-128>>

      <tuple|<tuple|Expansion|work>|<pageref|auto-129>>

      <tuple|<tuple|Work|expansion>|<pageref|auto-130>>

      <tuple|<tuple|Line integral>|<pageref|auto-132>>

      <tuple|<tuple|Integral|line>|<pageref|auto-133>>

      <tuple|<tuple|Anisotropic phase>|<pageref|auto-135>>

      <tuple|<tuple|Phase|anisotropic>|<pageref|auto-136>>

      <tuple|<tuple|work|deformation>||c3 sec dw
      idx1|<tuple|Work|deformation>|<pageref|auto-139>>

      <tuple|<tuple|deformation|work>||c3 sec dw
      idx2|<tuple|Deformation|work>|<pageref|auto-140>>

      <tuple|<tuple|internal energy|ideal gas>|||<tuple|Internal energy|of an
      ideal gas>|<pageref|auto-143>>

      <tuple|<tuple|Ideal gas>|<pageref|auto-144>>

      <tuple|<tuple|Gas|ideal>|<pageref|auto-145>>

      <tuple|<tuple|Ideal gas|equation>|<pageref|auto-146>>

      <tuple|<tuple|Gas|perfect>|<pageref|auto-147>>

      <tuple|<tuple|Reversible|isothermal expansion of an ideal
      gas>|<pageref|auto-149>>

      <tuple|<tuple|Reversible|adiabatic expansion of an ideal
      gas>|<pageref|auto-151>>

      <tuple|<tuple|heat capacity|constant volume|ideal gas>|||<tuple|Heat
      capacity|at constant volume|of an ideal gas>|<pageref|auto-152>>

      <tuple|<tuple|Adiabat>|<pageref|auto-153>>

      <tuple|<tuple|Isotherm>|<pageref|auto-154>>

      <tuple|<tuple|Indicator diagram>|<pageref|auto-157>>

      <tuple|<tuple|pressure volume diagram>|||<tuple|Pressure\Uvolume
      diagram>|<pageref|auto-159>>

      <tuple|<tuple|Expansion|work|reversible>|<pageref|auto-160>>

      <tuple|<tuple|Work|reversible expansion>|<pageref|auto-161>>

      <tuple|<tuple|Reversible|expansion work>|<pageref|auto-162>>

      <tuple|<tuple|Free expansion>|<pageref|auto-166>>

      <tuple|<tuple|Expansion|free>|<pageref|auto-167>>

      <tuple|<tuple|expansion|work>||c3 sec dw-expansion-gas
      idx1|<tuple|Expansion|work>|<pageref|auto-170>>

      <tuple|<tuple|work|expansion>||c3 sec dw-expansion-gas
      idx2|<tuple|Work|expansion>|<pageref|auto-171>>

      <tuple|<tuple|work|gravitational>||c3 sec wgf
      idx1|<tuple|Work|gravitational>|<pageref|auto-173>>

      <tuple|<tuple|gravitational|work>||c3 sec wgf
      idx2|<tuple|Gravitational|work>|<pageref|auto-174>>

      <tuple|<tuple|Local frame>|<pageref|auto-175>>

      <tuple|<tuple|Frame|local>|<pageref|auto-176>>

      <tuple|<tuple|Lab frame>|<pageref|auto-177>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-178>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-180>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-181>>

      <tuple|<tuple|work|gravitational>||c3 sec wgf
      idx1|<tuple|Work|gravitational>|<pageref|auto-182>>

      <tuple|<tuple|gravitational|work>||c3 sec wgf
      idx2|<tuple|Gravitational|work>|<pageref|auto-183>>

      <tuple|<tuple|shaft work>||c3 sec wgf-shaft idx1|<tuple|Shaft
      Work>|<pageref|auto-185>>

      <tuple|<tuple|work|shaft>||c3 sec wgf-shaft
      idx2|<tuple|Work|shaft>|<pageref|auto-186>>

      <tuple|<tuple|Torque>|<pageref|auto-189>>

      <tuple|<tuple|work|stirring>||c3 sec wgf-stirring
      idx1|<tuple|Work|stirring>|<pageref|auto-192>>

      <tuple|<tuple|stirring work>||c3 sec wgf-stirring idx2|<tuple|Stirring
      work>|<pageref|auto-193>>

      <tuple|<tuple|Work|stirring>|<pageref|auto-194>>

      <tuple|<tuple|Stirring work>|<pageref|auto-195>>

      <tuple|<tuple|Dissipative work>|<pageref|auto-197>>

      <tuple|<tuple|Work|dissipative>|<pageref|auto-198>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-200>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-201>>

      <tuple|<tuple|work|stirring>||c3 sec wgf-stirring
      idx1|<tuple|Work|stirring>|<pageref|auto-202>>

      <tuple|<tuple|stirring work>||c3 sec wgf-stirring idx2|<tuple|Stirring
      work>|<pageref|auto-203>>

      <tuple|<tuple|joule|paddle wheel>||c3 sec wgf-paddle
      idx1|<tuple|Joule|paddle wheel>|<pageref|auto-205>>

      <tuple|<tuple|paddle wheel|joule>||c3 sec wgf-paddle idx2|<tuple|Paddle
      wheel|Joule>|<pageref|auto-206>>

      <tuple|<tuple|Joule, James Prescott>|<pageref|auto-207>>

      <tuple|<tuple|Joule, James Prescott>|<pageref|auto-209>>

      <tuple|<tuple|Joule, James Prescott>|<pageref|auto-211>>

      <tuple|<tuple|Calorie>|<pageref|auto-212>>

      <tuple|<tuple|joule|paddle wheel>||c3 sec wgf-paddle
      idx1|<tuple|Joule|paddle wheel>|<pageref|auto-213>>

      <tuple|<tuple|paddle wheel|joule>||c3 sec wgf-paddle idx2|<tuple|Paddle
      wheel|Joule>|<pageref|auto-214>>

      <tuple|<tuple|shaft work>||c3 sec wgf-shaft idx1|<tuple|Shaft
      Work>|<pageref|auto-215>>

      <tuple|<tuple|work|shaft>||c3 sec wgf-shaft
      idx2|<tuple|Work|shaft>|<pageref|auto-216>>

      <tuple|<tuple|Joule, James Prescott>|<pageref|auto-218>>

      <tuple|<tuple|electrical|work>||c3 sec ew
      idx1|<tuple|Electrical|work>|<pageref|auto-220>>

      <tuple|<tuple|work|electrical>||c3 sec ew
      idx2|<tuple|Work|electrical>|<pageref|auto-221>>

      <tuple|<tuple|Electric|current>|<pageref|auto-223>>

      <tuple|<tuple|Current, electric>|<pageref|auto-224>>

      <tuple|<tuple|Electrical|circuit>|<pageref|auto-225>>

      <tuple|<tuple|Circuit|electrical>|<pageref|auto-226>>

      <tuple|<tuple|Electrical|circuit>|<pageref|auto-227>>

      <tuple|<tuple|Circuit|electrical>|<pageref|auto-228>>

      <tuple|<tuple|Electric|potential difference>|<pageref|auto-229>>

      <tuple|<tuple|Electric|current>|<pageref|auto-230>>

      <tuple|<tuple|Current, electric>|<pageref|auto-231>>

      <tuple|<tuple|Electric|circuit>|<pageref|auto-232>>

      <tuple|<tuple|Circuit|electrical>|<pageref|auto-233>>

      <tuple|<tuple|Electrical|circuit>|<pageref|auto-234>>

      <tuple|<tuple|Circuit|electrical>|<pageref|auto-235>>

      <tuple|<tuple|electrical|heating>||c3 sec ew-heating
      idx1|<tuple|Electrical|heating>|<pageref|auto-237>>

      <tuple|<tuple|heating|electrical>||c3 sec ew-heating
      idx2|<tuple|Heating|electrical>|<pageref|auto-238>>

      <tuple|<tuple|Electrical|resistor>|<pageref|auto-239>>

      <tuple|<tuple|Resistor, electrical>|<pageref|auto-240>>

      <tuple|<tuple|Electric|potential difference>|<pageref|auto-241>>

      <tuple|<tuple|Electrical|resistor>|<pageref|auto-242>>

      <tuple|<tuple|Resistor, electrical>|<pageref|auto-243>>

      <tuple|<tuple|Electrical|circuit>|<pageref|auto-245>>

      <tuple|<tuple|Circuit|electrical>|<pageref|auto-246>>

      <tuple|<tuple|Electric|resistance>|<pageref|auto-247>>

      <tuple|<tuple|Resistance|electric>|<pageref|auto-248>>

      <tuple|<tuple|Dissipative work>|<pageref|auto-250>>

      <tuple|<tuple|Work|dissipative>|<pageref|auto-251>>

      <tuple|<tuple|Heat>|<pageref|auto-252>>

      <tuple|<tuple|electrical|heating>||c3 sec ew-heating
      idx1|<tuple|Electrical|heating>|<pageref|auto-253>>

      <tuple|<tuple|heating|electrical>||c3 sec ew-heating
      idx2|<tuple|Heating|electrical>|<pageref|auto-254>>

      <tuple|<tuple|Galvanic cell>|<pageref|auto-256>>

      <tuple|<tuple|Cell potential>|<pageref|auto-258>>

      <tuple|<tuple|Equilibrium cell potential>|<pageref|auto-259>>

      <tuple|<tuple|Cell potential|equilibrium>|<pageref|auto-260>>

      <tuple|<tuple|Electrical|resistor>|<pageref|auto-261>>

      <tuple|<tuple|Resistor, electrical>|<pageref|auto-262>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-263>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-264>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-266>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-267>>

      <tuple|<tuple|electrical|work>||c3 sec ew
      idx1|<tuple|Electrical|work>|<pageref|auto-268>>

      <tuple|<tuple|work|electrical>||c3 sec ew
      idx2|<tuple|Work|electrical>|<pageref|auto-269>>

      <tuple|<tuple|internal|friction>||c3 sec iwif
      idx1|<tuple|Internal|friction>|<pageref|auto-271>>

      <tuple|<tuple|friction|internal>||c3 sec iwif
      idx2|<tuple|Friction|internal>|<pageref|auto-272>>

      <tuple|<tuple|Minimal work principle>|<pageref|auto-273>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-274>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-275>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-276>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-277>>

      <tuple|<tuple|Dissipative work>|<pageref|auto-278>>

      <tuple|<tuple|Work|dissipative>|<pageref|auto-279>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-281>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-282>>

      <tuple|<tuple|Friction|lubricated>|<pageref|auto-283>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-285>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-286>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-287>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-288>>

      <tuple|<tuple|Friction|dry>|<pageref|auto-290>>

      <tuple|<tuple|internal|friction>||c3 sec iwif
      idx1|<tuple|Internal|friction>|<pageref|auto-291>>

      <tuple|<tuple|friction|internal>||c3 sec iwif
      idx2|<tuple|Friction|internal>|<pageref|auto-292>>

      <tuple|<tuple|reversible|process>||c3 sec-ripg
      idx1|<tuple|Reversible|process>|<pageref|auto-294>>

      <tuple|<tuple|process|reversible>||c3 sec-ripg
      idx2|<tuple|Process|Reversible>|<pageref|auto-295>>

      <tuple|<tuple|Reversible|work>|<pageref|auto-296>>

      <tuple|<tuple|Work|reversible>|<pageref|auto-297>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-298>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-299>>

      <tuple|<tuple|Dissipative work>|<pageref|auto-300>>

      <tuple|<tuple|Work|dissipative>|<pageref|auto-301>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-302>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-303>>

      <tuple|<tuple|Process|adiabatic>|<pageref|auto-304>>

      <tuple|<tuple|Adiabatic|process>|<pageref|auto-305>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-306>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-307>>

      <tuple|<tuple|Triple|line>|<pageref|auto-308>>

      <tuple|<tuple|Work|mechanical>|<pageref|auto-309>>

      <tuple|<tuple|Work|shaft>|<pageref|auto-310>>

      <tuple|<tuple|Expansion|work>|<pageref|auto-311>>

      <tuple|<tuple|Work|expansion>|<pageref|auto-312>>

      <tuple|<tuple|Work|surface>|<pageref|auto-313>>

      <tuple|<tuple|Work|stretching>|<pageref|auto-314>>

      <tuple|<tuple|Gravitational|work>|<pageref|auto-315>>

      <tuple|<tuple|Work|gravitational>|<pageref|auto-316>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-317>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-318>>

      <tuple|<tuple|work|electric polarization>|||<tuple|Work|of electric
      polarization>|<pageref|auto-319>>

      <tuple|<tuple|work|magnetization>|||<tuple|Work|of
      magnetization>|<pageref|auto-320>>

      <tuple|<tuple|reversible|process>||c3 sec-ripg
      idx1|<tuple|Reversible|process>|<pageref|auto-322>>

      <tuple|<tuple|process|reversible>||c3 sec-ripg
      idx2|<tuple|Process|Reversible>|<pageref|auto-323>>
    </associate>
    <\associate|parts>
      <tuple|bio-RUMFORD.tm|chapter-nr|3|section-nr|1|subsection-nr|6>

      <tuple|bio-JOULE.tm|chapter-nr|3|section-nr|7|subsection-nr|2>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|3.10.1>|>
        Some kinds of work
      </surround>|<pageref|auto-321>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>The
      First Law> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      3.1<space|2spc>Heat, Work, and the First Law
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>

      <with|par-left|<quote|1tab>|3.1.1<space|2spc>The concept of
      thermodynamic work <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <with|par-left|<quote|1tab>|3.1.2<space|2spc>Work coefficients and work
      coordinates <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-33>>

      <with|par-left|<quote|1tab>|3.1.3<space|2spc>Heat and work as path
      functions <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-39>>

      <with|par-left|<quote|1tab>|3.1.4<space|2spc>Heat and heating
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-48>>

      <with|par-left|<quote|1tab>|3.1.5<space|2spc>Heat capacity
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-54>>

      <with|par-left|<quote|1tab>|3.1.6<space|2spc>Thermal energy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-59>>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Benjamin
      Thompson, Count of Rumford> (1753\U1814)
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-63><vspace|0.15fn>>

      3.2<space|2spc>Spontaneous, Reversible, and Irreversible Processes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-66>

      <with|par-left|<quote|1tab>|3.2.1<space|2spc>Reversible processes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-70>>

      <with|par-left|<quote|1tab>|3.2.2<space|2spc>Reversibility and the
      surroundings <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-86>>

      <with|par-left|<quote|1tab>|3.2.3<space|2spc>Irreversible processes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-91>>

      <with|par-left|<quote|1tab>|3.2.4<space|2spc>Purely mechanical
      processes <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-101>>

      3.3<space|2spc>Heat Transfer <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-106>

      <with|par-left|<quote|1tab>|3.3.1<space|2spc>Heating and cooling
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-108>>

      <with|par-left|<quote|1tab>|3.3.2<space|2spc>Spontaneous phase
      transitions <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-110>>

      3.4<space|2spc>Deformation Work <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-115>

      <with|par-left|<quote|1tab>|3.4.1<space|2spc>Gas in a
      cylinder-and-piston device <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-120>>

      <with|par-left|<quote|1tab>|3.4.2<space|2spc>Expansion work of a gas
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-126>>

      <with|par-left|<quote|1tab>|3.4.3<space|2spc>Expansion work of an
      isotropic phase <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-134>>

      <with|par-left|<quote|1tab>|3.4.4<space|2spc>Generalities
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-138>>

      3.5<space|2spc>Applications of Expansion Work
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-141>

      <with|par-left|<quote|1tab>|3.5.1<space|2spc>The internal energy of an
      ideal gas <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-142>>

      <with|par-left|<quote|1tab>|3.5.2<space|2spc>Reversible isothermal
      expansion of an ideal gas <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-148>>

      <with|par-left|<quote|1tab>|3.5.3<space|2spc>Reversible adiabatic
      expansion of an ideal gas <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-150>>

      <with|par-left|<quote|1tab>|3.5.4<space|2spc>Indicator diagrams
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-156>>

      <with|par-left|<quote|1tab>|3.5.5<space|2spc>Spontaneous adiabatic
      expansion or compression <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-164>>

      <with|par-left|<quote|1tab>|3.5.6<space|2spc>Free expansion of a gas
      into a vacuum <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-165>>

      3.6<space|2spc>Work in a Gravitational Field
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-172>

      3.7<space|2spc>Shaft Work <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-184>

      <with|par-left|<quote|1tab>|3.7.1<space|2spc>Stirring work
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-191>>

      <with|par-left|<quote|1tab>|3.7.2<space|2spc>The Joule paddle wheel
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-204>>

      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|James
      Prescott Joule> (1818\U1889) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-217><vspace|0.15fn>>

      3.8<space|2spc>Electrical Work <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-219>

      <with|par-left|<quote|1tab>|3.8.1<space|2spc>Electrical work in a
      circuit <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-222>>

      <with|par-left|<quote|1tab>|3.8.2<space|2spc>Electrical heating
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-236>>

      <with|par-left|<quote|1tab>|3.8.3<space|2spc>Electrical work with a
      galvanic cell <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-255>>

      3.9<space|2spc>Irreversible Work and Internal Friction
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-270>

      3.10<space|2spc>Reversible and Irreversible Processes: Generalities
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-293>
    </associate>
  </collection>
</auxiliary>