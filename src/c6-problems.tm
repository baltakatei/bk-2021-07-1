<TeXmacs|1.99.20>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;
  </hide-preamble>

  <new-page*><section|Problems>

  <\problem>
    Calculate the molar entropy of carbon disulfide at <math|25.00 <degC>>
    and <math|1<br>> from the heat capacity data for the solid in Table
    <vpageref|tbl:6-CS2> and the following data for <math|p=1<br>>. At the
    melting point, <math|161.11<K>>, the molar enthalpy of fusion is
    <math|\<Delta\><rsub|<text|fus>>H=4.39<timesten|3>
    \ <text|J>\<cdot\><text|mol><rsup|-1>>. The molar heat capacity of the
    liquid in the range 161\U300<nbsp>K is described by <math|<Cpm>=a+b*T>,
    where the constants have the values <math|a=74.6
    <text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>> and
    <math|b=0.0034 <text|J>\<cdot\><text|K><rsup|-2>\<cdot\><text|mol><rsup|-1>>.

    <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|2|2|cell-col-span|1>|<cwith|1|1|2|2|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|11|11|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|T/K>>|<cell|<math|<Cpm>/<around*|(|<text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>|)>>>>|<row|<cell|15.05>|<cell|6.9>>|<row|<cell|20.15>|<cell|12.0>>|<row|<cell|29.76>|<cell|20.8>>|<row|<cell|42.22>|<cell|29.2>>|<row|<cell|57.52>|<cell|35.6>>|<row|<cell|75.54>|<cell|40.0>>|<row|<cell|94.21>|<cell|45.0>>|<row|<cell|108.93>|<cell|48.5>>|<row|<cell|131.54>|<cell|52.6>>|<row|<cell|156.83>|<cell|56.6>>>>>>
      <label|tbl:6-CS2>Molar heat capacity of CS<rsub|<math|2>>(s) at
      <math|p=1 <text|bar>><rsup|<note-inline||+FbTpX6q2UH75xAm>><space|.15em>

      <note-ref|+FbTpX6q2UH75xAm>Ref. <cite|brown-37>
    </big-table>
  </problem>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|6>
    <associate|page-first|120>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|3>
    <associate|subsection-nr|2>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|4|?>>
    <associate|auto-2|<tuple|4.1|?>>
    <associate|footnote-4.1|<tuple|4.1|?>>
    <associate|footnr-4.1|<tuple|4.1|?>>
    <associate|tbl:6-CS2|<tuple|4.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      brown-37
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|4.1>|>
        Molar heat capacity of CS<rsub|<with|mode|<quote|math>|2>>(s) at
        <with|mode|<quote|math>|p=1 <with|mode|<quote|text>|bar>><rsup|<surround|<assign|footnote-nr|1><hidden-binding|<tuple>|4.1><assign|fnote-+FbTpX6q2UH75xAm|<quote|4.1>><assign|fnlab-+FbTpX6q2UH75xAm|<quote|4.1>>||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-41DF046C8--421DB82B8>|<link|hyperlink|<id|%-41DF046C8--421DB82B8>|<url|#footnr-4.1>>|4.1>.
        |<hidden-binding|<tuple|footnote-4.1>|4.1>|>>>><space|.15em>

        <space|0spc><rsup|<with|font-shape|<quote|right>|<reference|footnote-4.1>>>Ref.
        [<write|bib|brown-37><reference|bib-brown-37>]
      </surround>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      4<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>