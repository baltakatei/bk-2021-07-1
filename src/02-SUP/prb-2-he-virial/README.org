* Problem 2 - Helium Virial Figure
** Summary
This directory contains files necessary to plot the constant-volume
gas thermometer data provided in one of the Chapter 2 problem sets.

** Contents:
- ~driver.m~: File containing script to process CSV file and create
  figure.
- ~data/tbl-2-he-virial.csv~: CSV file containing "Helium at a fixed
  temperature" data (i.e. "data obtained from a constant-volume gas
  thermometer containing samples of helium maintained at a certain
  fixed temperature T_2)

  - Column 1: (1/V_m)/(10^2 mol/m^3)
  - Column 2: (p_2*V_m/R)/(K)

- ~deliverables/~: Directory containing rendered figure

** Dependencies
- GNU Octave 4.4.1

** Procedure
1. Open and run ~driver.m~ in GNU Octave
