#!/usr/bin/env octave

clc, clear, close all
% Ref/Attrib [1] HowTo Perform Linear Regression in GNU Octave https://www.youtube.com/watch?v=umOpZMfOkfc
%            [2] csvread. https://octave.sourceforge.io/octave/function/csvread.html
%            [3] Polynomial Interpolation https://octave.org/doc/v4.4.1/Polynomial-Interpolation.html
%            [4] polyfit and R^2 value https://web.archive.org/web/20200804212109/https://www.mathworks.com/matlabcentral/answers/159369-polyfit-and-r-2-value#answer_349551

% load (1/V_m) vs ((p_2*V_m)/R)/10^2
data = csvread('data/tbl-2-he-virial.csv',1,0); % see [2]

x = data(:, 1); % x-values as column vector
y = data(:, 2); % y-values as volumn vector

% Calculate least-squares fit with error details; see [3]
poly_order=1;
[p, s] = polyfit(x, y, poly_order);

%% Calc coefficient of determination (i.e. R^2); see [4]
Rsq = 1 - (s.normr/norm(y - mean(y)))^2

% Plot data with least-squares fit first-order polynomial
figure
scatter(x,y,'b')
grid on; box on;
xlabel('(1/V_m)/10^2 mol*m^-3')
ylabel('(p_2*V_m/K)')
xlim([0, 6]);
ylim([2.5, 2.8]);
hold on
plot(x,p(1)*x+p(2),'g')


