<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <new-page*><section|Problems>

  <\problem>
    <label|prb:10-NaCl> The mean ionic activity coefficient of NaCl in a
    0.100 molal aqueous solution at <math|298.15<K>> has been evaluated with
    measurements of equilibrium cell potentials,<footnote|Ref.
    <cite|robinson-59>, Table 9.3.> with the result <math|ln
    <g><rsub|\<pm\>>=-0.2505>. Use this value in Eq. <reference|ln(g+-)=...>,
    together with the values of osmotic coefficients in Table
    <vpageref|tbl:10-NaCl>, to evaluate <math|<g><rsub|\<pm\>>> at each of
    the molalities shown in the table; then plot <math|<g><rsub|\<pm\>>> as a
    function of <math|m<B>>.
  </problem>

  <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|7|7|1|-1|cell-bborder|1ln>|<cwith|2|-1|1|-1|cell-halign|C.>|<table|<row|<cell|<math|m<B>/<around*|(|<text|mol>\<cdot\><text|kg><rsup|-1>|)>>>|<cell|<math|\<phi\><rsub|m>>>|<cell|>|<cell|<math|m<B>/<around*|(|<text|mol>\<cdot\><text|kg><rsup|-1>|)>>>|<cell|<math|\<phi\><rsub|m>>>>|<row|<cell|0.1>|<cell|0.9325>|<cell|>|<cell|2.0>|<cell|0.9866>>|<row|<cell|0.2>|<cell|0.9239>|<cell|>|<cell|3.0>|<cell|1.0485>>|<row|<cell|0.3>|<cell|0.9212>|<cell|>|<cell|4.0>|<cell|1.1177>>|<row|<cell|0.5>|<cell|0.9222>|<cell|>|<cell|5.0>|<cell|1.1916>>|<row|<cell|1.0>|<cell|0.9373>|<cell|>|<cell|6.0>|<cell|1.2688>>|<row|<cell|1.5>|<cell|0.9598>|<cell|>|<cell|>|<cell|>>>>>>
    <label|tbl:10-NaCl>Osmotic coefficients of aqueous NaCl at
    <math|298.15<K>>.<note-ref|+1PualPXI2C0ZUoKN>

    \;

    <note-inline||+1PualPXI2C0ZUoKN>Ref. <cite|clarke-85>.
  </big-table>

  <\problem>
    Rard and Miller<footnote|Ref. <cite|rard-81>.> used published
    measurements of the freezing points of dilute aqueous solutions of
    Na<rsub|<math|2>>SO<rsub|<math|4>> to calculate the osmotic coefficients
    of these solutions at <math|298.15<K>>. Use their values listed in Table
    <vpageref|tbl:10-Na2SO4> to evaluate the mean ionic activity coefficient
    of Na<rsub|<math|2>>SO<rsub|<math|4>> at <math|298.15<K>> and a molality
    of <math|m<B>=0.15 <text|mol>\<cdot\><text|kg><rsup|-1>>. For the
    parameter <math|a> in the Debye\UH�ckel equation (Eq. <reference|ln(g+-)
    (DH)>), use the value <math|a=3.0<timesten|-10> <text|m>>.
  </problem>

  <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|9|9|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|m<B>/<around*|(|<text|mol>\<cdot\><text|kg><rsup|-1>|)>>>|<cell|<math|\<phi\><rsub|m>>>|<cell|>|<cell|<math|m<B>/<around*|(|<text|mol>\<cdot\><text|kg><rsup|-1>|)>>>|<cell|<math|\<phi\><rsub|m>>>>|<row|<cell|0.0126>|<cell|0.8893>|<cell|>|<cell|0.0637>|<cell|0.8111>>|<row|<cell|0.0181>|<cell|0.8716>|<cell|>|<cell|0.0730>|<cell|0.8036>>|<row|<cell|0.0228>|<cell|0.8607>|<cell|>|<cell|0.0905>|<cell|0.7927>>|<row|<cell|0.0272>|<cell|0.8529>|<cell|>|<cell|0.0996>|<cell|0.7887>>|<row|<cell|0.0374>|<cell|0.8356>|<cell|>|<cell|0.1188>|<cell|0.7780>>|<row|<cell|0.0435>|<cell|0.8294>|<cell|>|<cell|0.1237>|<cell|0.7760>>|<row|<cell|0.0542>|<cell|0.8178>|<cell|>|<cell|0.1605>|<cell|0.7616>>|<row|<cell|0.0594>|<cell|0.8141>|<cell|>|<cell|>|<cell|>>>>>>
    <label|tbl:10-Na2SO4>Osmotic coefficients of aqueous
    Na<rsub|<math|2>>SO<rsub|<math|4>> at <math|298.15<K>>
  </big-table>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|10>
    <associate|page-first|249>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|6>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|7|?>>
    <associate|auto-2|<tuple|7.1|?>>
    <associate|auto-3|<tuple|7.2|?>>
    <associate|footnote-7.1|<tuple|7.1|?>>
    <associate|footnote-7.2|<tuple|7.2|?>>
    <associate|footnote-7.3|<tuple|7.3|?>>
    <associate|footnr-7.1|<tuple|7.1|?>>
    <associate|footnr-7.2|<tuple|7.1|?>>
    <associate|footnr-7.3|<tuple|7.3|?>>
    <associate|prb:10-NaCl|<tuple|7.1|?>>
    <associate|tbl:10-Na2SO4|<tuple|7.2|?>>
    <associate|tbl:10-NaCl|<tuple|7.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      robinson-59

      clarke-85

      rard-81
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.1>|>
        Osmotic coefficients of aqueous NaCl at
        <with|mode|<quote|math>|298.15<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>>.<space|0spc><assign|footnote-nr|2><hidden-binding|<tuple>|7.2><assign|fnote-+1PualPXI2C0ZUoKN|<quote|7.2>><assign|fnlab-+1PualPXI2C0ZUoKN|<quote|7.2>><rsup|<with|font-shape|<quote|right>|<reference|footnote-7.2>>>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7F827A618-7C94AA780>|<link|hyperlink|<id|%-7F827A618-7C94AA780>|<url|#footnr-7.2>>|7.2>.
        |<hidden-binding|<tuple|footnote-7.2>|7.2>|>>>Ref.
        [<write|bib|clarke-85><reference|bib-clarke-85>].
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.2>|>
        Osmotic coefficients of aqueous Na<rsub|<with|mode|<quote|math>|2>>SO<rsub|<with|mode|<quote|math>|4>>
        at <with|mode|<quote|math>|298.15<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>>
      </surround>|<pageref|auto-3>>
    </associate>
    <\associate|toc>
      7<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>