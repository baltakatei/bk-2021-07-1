<TeXmacs|1.99.20>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <new-page*><section|Problems>

  <\problem>
    <label|prb:8-droplet>Consider the system described in Sec.
    <reference|8-liquid droplet> containing a spherical liquid droplet of
    radius <math|r> surrounded by pure vapor. Starting with Eq. <reference|dU
    (droplet)>, find an expression for the total differential of <math|U>.
    Then impose conditions of isolation and show that the equilibrium
    conditions are <math|T<rsup|<text|g>>=T<rsup|<text|l>>>,
    <math|\<mu\><rsup|<text|g>>=\<mu\><rsup|<text|l>>>, and
    <math|p<rsup|<text|l>>=p<rsup|<text|g>>+2<g>/r>, where <math|<g>> is the
    surface tension.
  </problem>

  <\problem>
    This problem concerns diethyl ether at <math|T=298.15<K>>. At this
    temperature, the standard molar entropy of the gas calculated from
    spectroscopic data is <math|S<m><st><gas>=342.2
    <text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>. The
    saturation vapor pressure of the liquid at this temperature is
    <math|0.6691<br>>, and the molar enthalpy of vaporization is
    <math|\<Delta\><rsub|<text|vap>>*H=27.10
    <text|kJ>\<cdot\><text|mol><rsup|-1>>. The second virial coefficient of
    the gas at this temperature has the value <math|B=-1.227<timesten|-3>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>, and its variation with
    temperature is given by <math|<dif>B/<dif>T=1.50<timesten|-5>
    <text|m><rsup|3>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>.

    <\enumerate-alpha>
      <item>Use these data to calculate the standard molar entropy of liquid
      diethyl ether at <math|298.15<K>>. A small pressure change has a
      negligible effect on the molar entropy of a liquid, so that it is a
      good approximation to equate <math|S<m><st><liquid>> to
      <math|S<m><liquid>> at the saturation vapor pressure.

      <item>Calculate the standard molar entropy of vaporization and the
      standard molar enthalpy of vaporization of diethyl ether at
      <math|298.15<K>>. It is a good approximation to equate
      <math|H<m><st><liquid>> to <math|H<m><liquid>> at the saturation vapor
      pressure.
    </enumerate-alpha>
  </problem>

  <\problem>
    Explain why the chemical potential surfaces shown in Fig.
    <reference|fig:8-mu surfaces of H2O> are concave downward; that is, why
    <math|<pd|\<mu\>|T|p>> becomes more negative with increasing <math|T> and
    <math|<pd|\<mu\>|p|T>> becomes less positive with increasing <math|p>.
  </problem>

  <\problem>
    Potassium has a standard boiling point of <math|773 <degC>> and a molar
    enthalpy of vaporization <math|\<Delta\><rsub|<text|vap>>*H=84.9
    <text|kJ>\<cdot\><text|mol><rsup|-1>>. Estimate the saturation vapor
    pressure of liquid potassium at <math|400. <degC>>.
  </problem>

  <\problem>
    Naphthalene has a melting point of <math|78.2 <degC>> at <math|1<br>> and
    <math|81.7 <degC>> at <math|100<br>>. The molar volume change on melting
    is <math|\<Delta\><rsub|<text|fus>>*V=0.019
    <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>. Calculate the molar
    enthalpy of fusion to two significant figures.
  </problem>

  <\problem>
    <label|prb:8-Antoine> The dependence of the vapor pressure of a liquid on
    temperature, over a limited temperature range, is often represented by
    the <index|Antoine equation><em|Antoine equation>,
    <math|log<rsub|10><around|(|p/<text|Torr>|)>=A-B/<around|(|t+C|)>>, where
    <math|t> is the Celsius temperature and <math|A>, <math|B>, and <math|C>
    are constants determined by experiment. A variation of this equation,
    using a natural logarithm and the thermodynamic temperature, is

    <\equation*>
      ln <around|(|p/<text|bar>|)>=a-<frac|b|T+c>
    </equation*>

    The vapor pressure of liquid benzene at temperatures close to
    <math|298<K>> is adequately represented by the preceding equation with
    the following values of the constants:

    <\equation*>
      a=9.25092*<space|2em>b=2771.233<K><space|2em>c=-53.262<K>
    </equation*>

    <\enumerate-alpha>
      <item>Find the standard boiling point of benzene.

      <item>Use the Clausius\UClapeyron equation to evaluate the molar
      enthalpy of vaporization of benzene at <math|298.15<K>>.
    </enumerate-alpha>
  </problem>

  <\problem>
    At a pressure of one atmosphere, water and steam are in equilibrium at
    <math|99.97 <degC>> (the normal boiling point of water). At this pressure
    and temperature, the water density is <math|0.958
    <text|g>\<cdot\><text|cm><rsup|-3>>, the steam density is
    <math|5.98<timesten|-4> <text|g>\<cdot\><text|cm><rsup|-3>>, and the
    molar enthalpy of vaporization is <math|40.66
    <text|kJ>\<cdot\><text|mol><rsup|-1>>.

    <\enumerate-alpha>
      <item>Use the Clapeyron equation to calculate the slope
      <math|<difp>/<dif>T> of the liquid--gas coexistence curve at this
      point.

      <item>Repeat the calculation using the Clausius--Clapeyron equation.

      <item>Use your results to estimate the standard boiling point of water.
      (Note: The experimental value is <math|99.61 <degC>>.)
    </enumerate-alpha>
  </problem>

  <\problem>
    At the standard pressure of <math|1<br>>, liquid and gaseous
    H<rsub|<math|2>>O coexist in equilibrium at <math|372.76<K>>, the
    standard boiling point of water.

    <\enumerate-alpha>
      <item>Do you expect the standard molar enthalpy of vaporization to have
      the same value as the molar enthalpy of vaporization at this
      temperature? Explain.

      <item>The molar enthalpy of vaporization at <math|372.76<K>> has the
      value <math|\<Delta\><rsub|<text|vap>>*H=40.67
      <text|kJ>\<cdot\><text|mol><rsup|-1>>. Estimate the value of
      <math|\<Delta\><rsub|<text|vap>>*H<st>> at this temperature with the
      help of Table <reference|tbl:7-gas standard molar> and the following
      data for the second virial coefficient of gaseous H<rsub|<math|2>>O at
      <math|372.76<K>>:

      <\equation*>
        B=-4.60<timesten|-4> <text|m><rsup|3>\<cdot\><text|mol><rsup|-1><space|2em><dif>B/<dif>T=3.4<timesten|-6>
        <text|m><rsup|3>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>
      </equation*>

      <item>Would you expect the values of
      <math|\<Delta\><rsub|<text|fus>>*H> and
      <math|\<Delta\><rsub|<text|fus>>*H<st>> to be equal at the standard
      freezing point of water? Explain.

      \;
    </enumerate-alpha>
  </problem>

  <\problem>
    The standard boiling point of H<rsub|<math|2>>O is <math|99.61 <degC>>.
    The molar enthalpy of vaporization at this temperature is
    <math|\<Delta\><rsub|<text|vap>>*H=40.67
    <text|kJ>\<cdot\><text|mol><rsup|-1>>. The molar heat capacity of the
    liquid at temperatures close to this value is given by

    <\equation*>
      <Cpm>=a+b*<around|(|t-c|)>
    </equation*>

    where <math|t> is the Celsius temperature and the constants have the
    values

    <\equation*>
      a=75.94 <text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1><space|2em>b=0.022
      <text|J>\<cdot\><text|K><rsup|-2>\<cdot\><text|mol><rsup|-1><space|2em>c=99.61
      <degC>
    </equation*>

    Suppose <math|100.00 <text|mol>> of liquid H<rsub|<math|2>>O is placed in
    a container maintained at a constant pressure of <math|1<br>>, and is
    carefully heated to a temperature <math|5.00 <degC>> above the standard
    boiling point, resulting in an unstable phase of superheated water. If
    the container is enclosed with an adiabatic boundary and the system
    subsequently changes spontaneously to an equilibrium state, what amount
    of water will vaporize? (Hint: The temperature will drop to the standard
    boiling point, and the enthalpy change will be zero.)
  </problem>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|8>
    <associate|page-first|152>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|4>
    <associate|subsection-nr|3>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|5|?>>
    <associate|auto-2|<tuple|Antoine equation|?>>
    <associate|prb:8-Antoine|<tuple|5.6|?>>
    <associate|prb:8-droplet|<tuple|5.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Antoine equation>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      5<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>