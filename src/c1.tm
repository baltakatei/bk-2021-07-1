<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|number-long-article|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <chapter|Introduction><glossary-line|<strong|Chapter
  1>><label|c1><label|Chap. 1>

  <section|Physical Quantities, Units, and Symbols><label|c1 sec qus>

  Thermodynamics is a quantitative subject. It allows us to derive relations
  between the values of numerous physical quantities. Some physical
  quantities, such as mole fraction, are dimensionless; the value of one of
  these quantities is a pure number. Most quantities, however, are not
  dimensionless and their values must include one or more units. This chapter
  describes the <acronym|SI> system of units, which are the preferred units
  in science applications. The chapter then discusses some useful
  mathematical manipulations of physical quantities using quantity calculus,
  and certain general aspects of dimensional analysis.

  <subsection|The International System of Units><label|c1 sec qus-si>

  There is international agreement that the units used for physical
  quantities in science and technology should be those of the
  <index-line|International System of Units|International System of Units,
  <em|see> <acronym|SI>>International System of Units, or
  <index|SI><acronym|SI><glossary|SI> (standing for the French
  <index-line|Systeme International dUnites|Syst�me International d'Unit�s,
  <em|see> <acronym|SI>><strong|Syst�me International
  d'Unit�s><glossary|Syst�me International d'Unit�s>).

  Physical quantities and units are denoted by symbols. This book will, with
  a few exceptions, use symbols recommended in the third edition of what is
  known, from the color of its cover, as the <index|<acronym|IUPAC> Green
  Book><acronym|IUPAC> Green Book<\footnote>
    Ref. <cite|greenbook-3>. The references are listed in the Bibliography at
    the back of the book.
  </footnote>. 1 This publication is a manual of recommended symbols and
  terminology based on the SI and produced by the <index-line|International
  Union of Pure and Applied Chemistry|International Union of Pure and Applied
  Chemistry, <em|see> <acronym|IUPAC>>International Union of Pure and Applied
  Chemistry (<index|IUPAC><acronym|IUPAC>). The symbols for physical
  quantities are listed for convenient reference in Appendices
  <reference|app:sym> and <reference|app:abbrev>.

  The <acronym|SI> includes the seven <index|base units><strong|base
  units><glossary|base units> listed in Table <reference|c1 tab
  si_base_units>.<float|float|hb|<\big-table>
    <bktable3|<tformat|<table|<row|<cell|Physical
    quantity>|<cell|<acronym|SI> unit>|<cell|Symbol>>|<row|<cell|time>|<cell|second>|<cell|s>>|<row|<cell|length>|<cell|meter<math|<rsup|a>>>|<cell|m>>|<row|<cell|mass>|<cell|kilogram>|<cell|kg>>|<row|<cell|thermodynamic
    temperature>|<cell|kelvin>|<cell|K>>|<row|<cell|amount of
    substance>|<cell|mole>|<cell|mol>>|<row|<cell|electric
    current>|<cell|ampere>|<cell|A>>|<row|<cell|luminous
    intensity>|<cell|candela>|<cell|cd>>>>>

    \;

    <math|<rsup|a>>or metre
  <|big-table>
    <label|c1 tab si_base_units><subindex|Units|SI><subindex|SI|base units>SI
    base units
  </big-table>>

  These base units are for seven independent physical quantities that are
  sufficient to describe all other physical quantities. Definitions of the
  base units are given in Appendix <reference|app:SI>. (The candela, the
  <acronym|SI> unit of luminous intensity, is usually not needed in
  thermodynamics and is not used in this book.)

  <subsection|Amount of substance and amount><label|1-amount><label|c1 sec
  qus-amount>

  The physical quantity formally called <index-line|Amount of
  substance|Amount of substance, <em|see> Amount><strong|amount of
  substance><glossary|amount of substance> is a counting quantity for
  specified elementary entities. An <index|Elementary entity>elementary
  entity may be an atom, a molecule, an ion, an electron, any other particle
  or specified group of particles. The <acronym|SI> base unit for amount of
  substance is the <index|Mole><strong|mole><glossary|mole>.

  Before 2019, the mole was defined as the amount of substance containing as
  many elementary entities as the number of atoms in exactly 12 grams of pure
  carbon-12 nuclide, <math|<lsup|12><text|C>>. This definition was such that
  one mole of <math|<text|H><rsub|2><text|O>> molecules, for example, has a
  mass of <math|18.02> grams, where <math|18.02> is the relative molecular
  mass of <math|<text|H><rsub|2><text|O>>, and contains
  <math|6.022\<times\>10<rsup|23> <text|mol><rsup|-1>> is
  <math|N<rsub|<text|A>>>, the <index|Avogadro constant><em|Avogadro
  constant> (values given to four significant digits). The same statement can
  be made for any other substance if <math|18.02> is replaced by the
  appropriate relative atomic mass or molecular mass value (Sec.
  <reference|c2 sec prop-amount>).

  The <acronym|SI> revision of 2019 (Sec. <reference|c1 sec qus-2019>)
  redefines the mole as exactly <math|6.022<separating-space|0.2em>140<separating-space|0.2em>76\<times\>10<rsup|23>>
  elementary entities. The mass of this number of carbon-12 atoms is
  <math|12> grams to within <math|5\<times\>10<rsup|-9>> gram,<\footnote>
    Ref <cite|stock-19>. Appendix 2.
  </footnote> so the revision makes a negligible change to calculations
  involving the mole.

  The symbol for amount of substance is <math|n>. It is admittedly awkward to
  refer to <math|n*<around*|(|<text|H><rsub|2><text|O>|)>> as \Pthe amount of
  substance of water.\Q This book simply shortens \Pamount of substance\Q to
  <index|Amount><strong|amount><glossary|amount>, a usage condoned by the
  <acronym|IUPAC>.<\footnote>
    Ref <cite|mills-89>. An alternative name suggested for <math|n> is
    \P<index|Chemical amount>chemical amount\Q.
  </footnote> Thus, \Pthe amount of water in the system\Q refers not to the
  mass or volume of water, but to the <em|number> of
  <math|<text|H><rsub|2><text|O>> molecules expressed in a counting unit such
  as the mole.

  <subsection|The <acronym|SI> revision of 2019><label|1-SI
  revision><label|c1 sec qus-2019>

  <index-complex|<tuple|si|2019 revision>||c1 sec qus-2019
  idx1|<tuple|SI|2019 revision>>At a General Conference on Weights and
  Measures held in Versailles, France in November 2018, metrologists from
  over fifty countries agreed on a major revision of the International System
  of Units. The revision became official on 20 May 2019. It redefines the
  base units for mass, thermodynamic temperature, amount of substance, and
  electric current.

  The SI revision bases the definitions of the base units (Appendix
  <reference|app:SI>) on a set of six <index|Defining constants>defining
  constants with values (listed in Appendix <reference|app:const>) treated as
  exact, with no uncertainty.

  Previously, the kilogram had been defined as the mass of a physical
  artifact, the <index|International prototype><subindex|Kilogram|international
  prototype>international prototype of the kilogram. The international
  prototype is a platinum-iridium cylinder manufactured in 1879 in England
  and stored since 1889 in a vault of the International Bureau of Weights and
  Measures in S�vres, near Paris, France. As it is subject to surface
  contamination and other slow changes of mass, it is not entirely suitable
  as a standard.

  The 2019 <acronym|SI> revision instead defines the kilogram in terms of the
  <index|Planck constant>Planck constant <math|h>.<\footnote>
    The manner in which this is done using a Kibble balance is described on
    page <pageref|c2 note-kibble-balance>.
  </footnote> As a defining constant, the value of <math|h> was chosen to
  agree with the mass of the international prototype with an uncertainty of
  only several parts in <math|10<rsup|8>>. Thus, as apractical matter, the
  <acronym|SI> revision has a negligible effect on the value of a mass.

  The <acronym|SI> revision defines the kelvin in terms of the
  <index|Boltzmann constant>Boltzmann constant <math|k>, the mole in terms of
  the <index|Avogadro constant>Avogadro constant <math|N<rsub|<text|A>>>, and
  the ampere in terms of the <index|Elementary charge>elementary charge
  <math|e>. The values of these defining constants were chosen to closely
  agree with the previous base unit definitions. Consequently, the
  <acronym|SI> revision has a negligible effect on values of thermodynamic
  temperature, amount of substance, and electric
  current.<index-complex|<tuple|si|2019 revision>||c1 sec qus-2019
  idx1|<tuple|SI|2019 revision>>

  <subsection|Derived units and prefixes><label|c1 sec qus-derived>

  Table <reference|c1 tab si_derived_units> lists derived units for some
  physical quantities used in thermodynamics. The derived units have exact
  definitions in terms of <acronym|SI> base units, as given int he last
  column of the table.<float|float|thb|<\big-table>
    <bktable3|<tformat|<table|<row|<cell|Physical
    quantity>|<cell|Unit>|<cell|Symbol>|<cell|Definition of
    unit>>|<row|<cell|force>|<cell|newton>|<cell|N>|<cell|<math|1 <text|N>=1
    <frac|<text|m>\<cdot\><text|kg>|<text|s><rsup|2>>>>>|<row|<cell|pressure>|<cell|pascal>|<cell|Pa>|<cell|<math|1
    <text|Pa>=1 <frac|<text|N>|<text|m><rsup|2>>=1
    <frac|<text|kg>|<text|m>\<cdot\><text|s><rsup|2>>>>>|<row|<cell|Celsius
    temperature>|<cell|degree Celsius>|<cell|<math|<rsup|\<circ\>><text|C>>>|<cell|<math|<frac|t|<rsup|\<circ\>><text|C>>=<frac|T|<text|K>>-273.15>>>|<row|<cell|energy>|<cell|joule>|<cell|J>|<cell|<math|1
    <text|J>=1 <text|N>\<cdot\><text|m>=1
    <frac|<text|m><rsup|2>\<cdot\><text|kg>|<text|s><rsup|2>>>>>|<row|<cell|power>|<cell|watt>|<cell|W>|<cell|<math|1
    <text|W>=1 <frac|<text|J>|<text|s>>=1
    <frac|<text|m><rsup|2>\<cdot\><text|kg>|<text|s><rsup|3>>>>>|<row|<cell|frequency>|<cell|hertz>|<cell|Hz>|<cell|<math|1
    <text|Hz>=1 <frac|1|<text|s>>=1 <text|s><rsup|-1>>>>|<row|<cell|electric
    charge>|<cell|coulomb>|<cell|C>|<cell|<math|1 <text|C>=1
    <text|A>\<cdot\><text|s>>>>|<row|<cell|electric
    potential>|<cell|volt>|<cell|V>|<cell|<math|1 <text|V>=1
    <frac|<text|J>|<text|C>>=1 <frac|<text|m><rsup|2>\<cdot\><text|kg>|<text|s><rsup|3>\<cdot\><text|A>>>>>|<row|<cell|electric
    resistance>|<cell|ohm>|<cell|<math|\<Omega\>>>|<cell|<math|1 \<Omega\>=1
    <frac|<text|V>|<text|A>>=1 <frac|<text|m><rsup|2>\<cdot\><text|kg>|<text|s><rsup|3>\<cdot\><text|A><rsup|2>>>>>>>>

    \;
  <|big-table>
    <label|c1 tab si_derived_units><subindex|Units|SI
    derived><subindex|SI|derived units><acronym|SI> derived units
  </big-table>>

  The units listed in Table <reference|c1 tab non_si_derived_units> are
  sometimes used in thermodynamics but are not part of the <acronym|SI>. They
  do, however, have exact definitions in terms of <acronym|SI> units and so
  offer no problems of numerical conversion to or from <acronym|SI>
  units.<float|float|thb|<\big-table>
    <bktable3|<tformat|<table|<row|<cell|Physical
    quantity>|<cell|Unit>|<cell|Symbol>|<cell|Definition of
    unit>>|<row|<cell|volume>|<cell|liter<math|<rsup|a>>>|<cell|L<math|<rsup|b>>>|<cell|<math|1
    <text|L>=1 <text|dm><rsup|3>=10<rsup|-3>
    <text|m><rsup|3>>>>|<row|<cell|pressure>|<cell|bar>|<cell|bar>|<cell|<math|1
    <text|bar>=10<rsup|5> <text|Pa>>>>|<row|<cell|pressure>|<cell|atmosphere>|<cell|atm>|<cell|<math|1
    <text|atm>=101,325 <text|Pa>=1.01325 <text|bar>>>>|<row|<cell|pressure>|<cell|torr>|<cell|Torr>|<cell|<math|1
    <text|Torr>=<around*|(|<frac|1|760>|)>
    <text|atm>=<around*|(|<frac|101,325|760>|)>
    <text|Pa>>>>|<row|<cell|energy>|<cell|calorie<math|<rsup|c>>>|<cell|cal<math|<rsup|d>>>|<cell|<math|1
    <text|cal>=4.184 <text|J>>>>>>>

    \;

    <math|<rsup|a>>or litre<space|1em><math|<rsup|b>>or
    l<space|1em><math|<rsup|c>>or thermochemical
    calorie<space|1em><math|<rsup|d>>or cal<math|<rsub|<text|th>>>
  <|big-table>
    <label|c1 tab non_si_derived_units><subindex|Units|Non-SI>Non-<acronym|SI>
    derived units
  </big-table>>

  <float|float|thb|<\big-table>
    <bktable3|<tformat|<table|<row|<cell|Fraction>|<cell|Prefix>|<cell|Symbol>|<cell|<space|2em>>|<cell|Multiple>|<cell|Prefix>|<cell|Symbol>>|<row|<cell|<math|10<rsup|-1>>>|<cell|deci>|<cell|d>|<cell|>|<cell|<math|10>>|<cell|deka>|<cell|da>>|<row|<cell|<math|10<rsup|-2>>>|<cell|centi>|<cell|c>|<cell|>|<cell|<math|10<rsup|2>>>|<cell|hecto>|<cell|h>>|<row|<cell|<math|10<rsup|-3>>>|<cell|milli>|<cell|m>|<cell|>|<cell|<math|10<rsup|3>>>|<cell|kilo>|<cell|k>>|<row|<cell|<math|10<rsup|-6>>>|<cell|micro>|<cell|\<#3BC\>>|<cell|>|<cell|<math|10<rsup|6>>>|<cell|mega>|<cell|M>>|<row|<cell|<math|10<rsup|-9>>>|<cell|nano>|<cell|n>|<cell|>|<cell|<math|10<rsup|9>>>|<cell|giga>|<cell|G>>|<row|<cell|<math|10<rsup|-12>>>|<cell|pico>|<cell|p>|<cell|>|<cell|<math|10<rsup|12>>>|<cell|tera>|<cell|T>>|<row|<cell|<math|10<rsup|-15>>>|<cell|femto>|<cell|f>|<cell|>|<cell|<math|10<rsup|15>>>|<cell|peta>|<cell|P>>|<row|<cell|<math|10<rsup|-18>>>|<cell|atto>|<cell|a>|<cell|>|<cell|<math|10<rsup|18>>>|<cell|exa>|<cell|E>>|<row|<cell|<math|10<rsup|-21>>>|<cell|zepto>|<cell|z>|<cell|>|<cell|<math|10<rsup|21>>>|<cell|zetta>|<cell|Z>>|<row|<cell|<math|10<rsup|-24>>>|<cell|yocto>|<cell|y>|<cell|>|<cell|<math|10<rsup|24>>>|<cell|yotta>|<cell|Y>>>>>

    \;
  <|big-table>
    <label|c1 tab si_prefixes><acronym|SI> prefixes
  </big-table>>

  Any of the symbols for units listed in Tables <reference|c1 tab
  si_base_units>\U<reference|c1 tab non_si_derived_units>, except <math|kg>
  and <math|<rsup|\<circ\>><text|C>>, may be preceded by one of the prefix
  symbols of Table <reference|c1 tab si_prefixes> to construct a decimal
  fraction or multiple of the unit. (The symbol <math|g> may be preceded by a
  prefix symbol to construct a fraction or multiple of the gram.) The
  combination of prefix symbol and unit symbol is taken as a new symbol that
  can be raised to a power without parentheses, as in the following examples:

  <\eqnarray*>
    <tformat|<table|<row|<cell|1 <text|mg>>|<cell|=>|<cell|1\<times\>10<rsup|-3>
    <text|g>>>|<row|<cell|1 <text|cm>>|<cell|=>|<cell|1\<times\>10<rsup|-2>
    <text|m>>>|<row|<cell|1 <text|cm><rsup|3>>|<cell|=>|<cell|<around*|(|1\<times\>10<rsup|-2>
    <text|m>|)><rsup|3>=1\<times\>10<rsup|-6> <text|m><rsup|3>>>>>
  </eqnarray*>

  <section|Quantity Calculus><label|c1 sec qc>

  This section gives examples of how we may manipulate physical quantities by
  the rules of algebra. The method is called <index|Quantity
  calculus>quantity calculus, although a better term might be \Pquantity
  algebra.\Q

  Quantity calculus is based on the concept that a physical quantity, unless
  it is dimensionless, has a value equal to the product of a numerical value
  (a pure number) and one or more <index|Units>units:

  <\equation>
    <around*|(|<text|physical quantity>|)>=<around*|(|<text|numerical
    value>|)>\<times\><around*|(|<text|units>|)>
  </equation>

  (If the quantity is dimensionless, it is equal to a pure number without
  units.) The physical property may be denoted by a symbol, but the symbol
  does <em|not> imply a particular choice of units. For instance, this book
  uses the symbol <math|\<rho\>> for density, but <math|\<rho\>> can be
  expressed in any units having the dimensions of mass divided by volume.

  A simple example illustrates the use of quantity calculus. We may express
  the density of water at <math|25 <rsup|\<circ\>><text|C>> to four
  significant digits in <acronym|SI> base units by the equation

  <\equation>
    \<rho\>=9.970\<times\>10<rsup|2> <frac|<text|kg><rsup|2>|<text|m><rsup|3>>=9.970\<times\>10<rsup|2>
    <text|kg>\<cdot\><text|m><rsup|-3>
  </equation>

  and in different density units by the equation

  <\equation>
    \<rho\>=0.9970 <frac|<text|g>|<text|cm><rsup|3>>=0.9970
    <text|g>\<cdot\><text|cm><rsup|-3>
  </equation>

  We may divide both sides of the last equation by <math|1
  <text|g>\<cdot\><text|cm><rsup|-3>> to obtain a new equation

  <\equation>
    <frac|\<rho\>|<text|g>\<cdot\><text|cm><rsup|-3>>=\<rho\>/<text|g>\<cdot\><text|cm><rsup|-3>=0.9970
  </equation>

  Now the pure number <math|0.9970> appearing in this equation is the number
  of grams in one cubic centimeter of water, so we may call the ratio
  <math|\<rho\>/<text|g>\<cdot\><text|cm><rsup|-3>> \Pthe number of grams per
  cubic centimeter.\Q By the same reasoning,
  <math|\<rho\>/<text|kg>\<cdot\><text|m><rsup|-3>> is the number of
  kilograms per cubic meter. In general, a physical quantity divided by
  particular units for the physical quantity is a pure number representing
  the number of those units.

  <\quote-env>
    Just as it would be incorrect to call <math|\<rho\>> \Pthe number of
    grams per cubic centimeter,\Q because that would refer to a particular
    choice of units for <math|\<rho\>>, the common practice of calling
    <math|n> \Pthe number of moles\Q is also strictly speaking not correct.
    It is actually the ratio <math|<frac|n|<text|mol>>> that is the number of
    moles.
  </quote-env>

  In a table, the ratio <math|\<rho\>/<text|g>\<cdot\><text|cm><rsup|-3>>
  makes a convenient heading for a column of density values because the
  column can then show pure numbers. Likewise, it is convenient to use
  <math|\<rho\>/<text|g>\<cdot\><text|cm><rsup|-3>> as the label of a graph
  axis and to show pure numbers at the grid marks of the axis. You will see
  many examples of this usage in the tables and figures of this book.

  A major advantage of using <acronym|SI> base units and <acronym|SI> derived
  units is that they are <em|coherent>. That is, values of a physical
  quantity expressed in different combinations of these units have the same
  numerical value.

  For example, suppose we wish to evaluate the pressure of a gas according to
  the <subindex|Ideal gas|equation>ideal gas equation<\footnote>
    This is the first equation in this book that, like many others to follow,
    shows <index|Conditions of validity><em|conditions of validity> in
    parentheses immediately below the equation number ont he right. Thus, Eq.
    <reference|c1 eq ideal_gas_law> is valid for an ideal gas.
  </footnote>

  <equation-cov2|p=<frac|n*R*T|V>|(ideal gas)<label|c1 eq ideal_gas_law>>

  In this equation, <math|p>, <math|n>, <math|T>, and <math|V> are the
  symbols for the physical quantities pressure, amount (amount of substance),
  thermodynamic temperature, and volume, respectively, and <math|R> is the
  gas constant.

  The calculation of <math|p> for <math|5.000> moles of an ideal gas at a
  temperature of <math|298.15> kelvins, in a volume of <math|4.000> cubic
  meters, is

  <\equation>
    <label|c1 eq ideal_gas_example>p=<frac|<around*|(|5.000
    <text|mol>|)>\<cdot\><around*|(|8.3145
    <frac|<text|J>|<text|K>\<cdot\><text|mol>>|)>\<cdot\><around*|(|298.15
    <text|K>|)>|4.000 <text|m><rsup|3>>=3.099\<times\>10<rsup|3>
    <frac|<text|J>|<text|m><rsup|3>>
  </equation>

  The mole and kelvin units cancel, and we are left with units of
  <math|<frac|<text|J>|<text|m><rsup|3>>>, a combination of an <acronym|SI>
  derived unit (the joule) and an <acronym|SI> base unit (the meter). The
  units <math|<frac|<text|J>|<text|m><rsup|3>>> must have dimensions of
  pressure, but are not commonly used to express pressure.

  To convert <math|<frac|<text|J>|<text|m><rsup|3>>> to the <acronym|SI>
  derived unit of pressure, the pascal (Pa), we can use the following
  relations from Table <reference|c1 tab si_derived_units>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|1 <text|J>>|<cell|=>|<cell|1
    <text|N>\<cdot\><text|m><eq-number>>>|<row|<cell|1
    <text|Pa>>|<cell|=>|<cell|1 <frac|<text|N>|<text|m><rsup|2>><eq-number>>>>>
  </eqnarray*>

  When we divide both sides of the first relation by <math|1 <text|J>> and
  divide both sides of the second relation by <math|1
  <frac|<text|N>|<text|m><rsup|2>>>, we obtain two new relations

  <\eqnarray*>
    <tformat|<table|<row|<cell|1>|<cell|=>|<cell|<around*|(|1
    <frac|<text|N>\<cdot\><text|m>|<text|J>>|)><eq-number>>>|<row|<cell|<around*|(|<frac|1
    <text|Pa>|<frac|<text|N>|<text|m><rsup|2>>>|)>>|<cell|=>|<cell|1<eq-number>>>>>
  </eqnarray*>

  The ratios in the parentheses are <em|conversion factors>. When a physical
  quantity is multiplied by a conversion factor that, like these, is equal to
  the pure number <math|1>, the physical quantity changes its units but not
  its value. When we multiply Eq. <reference|c1 eq ideal_gas_example> by both
  of these conversion factors, all units cancel except <math|<text|Pa>>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|p>|<cell|=>|<cell|<around*|(|3.099\<times\>10<rsup|3>
    <frac|<text|J>|<text|m><rsup|3>>|)>\<cdot\><around*|(|1
    <frac|<text|N>\<cdot\><text|m>|<text|J>>|)>\<cdot\><around*|(|<frac|1
    <text|Pa>|<frac|<text|N>|<text|m><rsup|2>>>|)>>>|<row|<cell|>|<cell|=>|<cell|3.099
    <text|Pa><eq-number>>>>>
  </eqnarray*>

  \;

  This example illustrates the fact that to calculate a physical quantity, we
  can simply enter into a calculator numerical values expressed in
  <acronym|SI> units, and the result is the numerical value of the calculated
  quantity expressed in <acronym|SI> units. In other words, as long as we use
  only <acronym|SI> base units and <acronym|SI> derived units (without
  prefixes), <em|all conversion factors are unity>.

  Of course we do not have to limit the calculation to <acronym|SI> units.
  Suppose we wish to express the calculated pressure in torrs, a
  non-<acronym|SI> unit. In this case, using a conversion factor obtained
  from the definition of the torr in Table <reference|c1 tab
  non_si_derived_units>, the calculation becomes

  <\eqnarray*>
    <tformat|<table|<row|<cell|p>|<cell|=>|<cell|<around*|(|3.099\<times\>10<rsup|3>
    <text|Pa>|)>\<times\><around*|(|<frac|760 <text|Torr>|101,325
    <text|Pa>>|)>>>|<row|<cell|>|<cell|=>|<cell|23.24
    <text|Torr><eq-number>>>>>
  </eqnarray*>

  <section|Dimensional Analysis><label|c1 sec da>

  <index|Dimensional analysis>Sometimes you can catch an error in the form of
  an equation or expression, or in the dimensions of a quantity used for a
  calculation, by checking for dimensional consistency. Here are some rules
  that must be satisfied:

  <\itemize-dot>
    <item>both sides of an equation have the same dimensions

    <item>all terms of a sum or difference have the same dimensions

    <item>logarithms and exponentials, and arguments of logarithms and
    exponentials, are dimensionless

    <item>a quantity used as a power is dimensionless
  </itemize-dot>

  \;

  In this book the <index|Differential><em|differential> of a function, such
  as <math|<text|d>*f>, refers to an <em|infinitesimal> quantity. If one side
  of an equation is an <em|infinitesimal> quantity, the other side must also
  be. Thus, the equation <math|<text|d>*f=a*<text|d>*x+b*<text|d>*y> (where
  <math|a*x> and <math|b*y> have the same dimensions as <math|f>) makes
  mathematical sense, but <math|<text|d>*f=a*x+b*<text|d>*y> does not.

  Derivatives, partial derivatives, and integrals have dimensions that we
  must take into account when determining the overall dimensions of an
  expression that includes them. For instance:

  <\itemize-dot>
    <item>the derivative <math|<frac|<text|d>*p|<text|d>*T>> and the partial
    derivative <math|<around*|(|<frac|\<partial\>*p|\<partial\>*T>|)><rsub|V>>
    have the same dimeinsions as <math|<frac|p|T>>

    <item>the partial second derivative <math|<around*|(|<frac|\<partial\><rsup|2>*p|\<partial\>*T<rsup|2>>|)><rsub|V>>
    has the same dimensions as <math|<frac|p|T<rsup|2>>>

    <item>the integral <math|<big|int>T*<text|d>*T> has the same dimensions
    as <math|T<rsup|2>>
  </itemize-dot>

  \;

  Some examples of applying these principles are given here using symbols
  described in Sec. <reference|c1 sec qc>.

  \;

  <\example>
    \;

    Since the gas constant <math|R> may be expressed in units of
    <math|<frac|<text|J>|<text|K>\<cdot\><text|mol>>>, it has dimensions of
    energy divided by thermodynamic temperature and amount. Thus, <math|R*T>
    has dimensions of energy divided by amount, and <math|n*R*T> has
    dimensions of energy. The products <math|R*T> and <math|n*R*T> appear
    frequently in thermodynamic expressions.
  </example>

  <\example>
    \;

    What are the dimensions of the quantity
    <math|n*R*T*<text|ln><around*|(|<frac|p|p<rsup|\<circ\>>>|)>> and of
    <math|p<rsup|\<circ\>>> in this expression? The quantity has the same
    dimensions as <math|n*R*T> (or energy) because the logarithm is
    dimensionless. Furthermore, <math|p<rsup|\<circ\>>> in this expression
    has dimensions of pressure in order to make the argument of the
    logarithm, <math|<frac|p|p<rsup|\<circ\>>>>, dimensionless.
  </example>

  <\example>
    \;

    Find the dimensions of the constants <math|a> and <math|b> in the van der
    Waals equation

    <\equation*>
      p=<frac|n*R*T|V-n*b>-<frac|n<rsup|2>*a|V<rsup|2>>
    </equation*>

    Dimensional analysis tells us that, because <math|n*b> is subtracted from
    <math|V>, <math|n*b> has dimensions of volume and therefore <math|b> has
    dimensions of <math|<frac|<around*|(|<text|volume>|)>|<around*|(|<text|amount>|)>>>.
    Furthermore, since the right side of the equation is a difference of two
    terms, these terms have the same dimensions as th eleft side, which is
    pressure. Therefore, the second term <math|<frac|n<rsup|2>*a|V<rsup|2>>>
    has dimensions of pressure, and <math|a> has dimensions of
    <math|<around*|(|<text|pressure>|)>\<times\><around*|(|<text|volume>|)><rsup|2>\<times\><around*|(|<text|amount>|)><rsup|-2>>.
  </example>

  <\example>
    \;

    Consider an equation of the form

    <\equation*>
      <around*|(|<frac|\<partial\>*<text|ln>
      <around*|(|x|)>|\<partial\>*T>|)><rsub|p>=<frac|y|R>
    </equation*>

    What are the <acronym|SI> units of <math|y>? <math|<text|ln> x> is
    dimensionless, so the left side of the equation has the dimensions
    <math|<frac|1|T>>, and its <acronym|SI> units are
    <math|<text|K><rsup|-1>>. The <acronym|SI> units of the right side are
    therefore also <math|<text|K><rsup|-1>>. Since <math|R> has the units
    <math|<frac|<text|J>|<text|K>\<cdot\><text|mol>>>, the SI units of
    <math|y> are <math|<frac|<text|J>|<text|K><rsup|2>\<cdot\><text|mol>>>.<index|Dimensional
    analysis>
  </example>

  \;

  <\problem>
    <label|c1 problem>Consider the following equations for the pressure of a
    real gas. For each equation, find the dimensions of the constants
    <math|a> and <math|b> and express these dimensions in <acronym|SI> units.

    <\enumerate-alpha>
      <item>The Dieterici equation:

      <\equation*>
        p=<frac|R*T*e<rsup|-<around*|(|<frac|a*n|V*R*T>|)>>|<around*|(|<frac|V|n>|)>-b>
      </equation*>

      <item>The Redlich\UKwong equation:

      <\equation*>
        p=<frac|R*T|<around*|(|<frac|V|n>|)>-b>-<frac|a*n<rsup|2>|T<rsup|<frac*|1|2>>\<cdot\>V\<cdot\><around*|(|V+n*b|)>>
      </equation*>
    </enumerate-alpha>
  </problem>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|0>
    <associate|info-flag|detailed>
    <associate|page-first|13>
    <associate|page-height|auto>
    <associate|page-medium|papyrus>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|project-flag|false>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|1-SI revision|<tuple|1.1.3|?>>
    <associate|1-amount|<tuple|1.1.2|?>>
    <associate|Chap. 1|<tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Chapter
    1>|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|base units|?>>
    <associate|auto-11|<tuple|1.1.1|?>>
    <associate|auto-14|<tuple|Units|?>>
    <associate|auto-15|<tuple|SI|?>>
    <associate|auto-16|<tuple|1.1.2|?>>
    <associate|auto-17|<tuple|amount of substance|?>>
    <associate|auto-18|<tuple|Elementary entity|?>>
    <associate|auto-19|<tuple|Mole|?>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-20|<tuple|mole|?>>
    <associate|auto-21|<tuple|Avogadro constant|?>>
    <associate|auto-22|<tuple|Amount|?>>
    <associate|auto-23|<tuple|amount|?>>
    <associate|auto-24|<tuple|Chemical amount|?>>
    <associate|auto-25|<tuple|1.1.3|?>>
    <associate|auto-26|<tuple|<tuple|si|2019 revision>|?>>
    <associate|auto-27|<tuple|Defining constants|?>>
    <associate|auto-28|<tuple|International prototype|?>>
    <associate|auto-29|<tuple|Kilogram|?>>
    <associate|auto-3|<tuple|1.1.1|?>>
    <associate|auto-30|<tuple|Planck constant|?>>
    <associate|auto-31|<tuple|Boltzmann constant|?>>
    <associate|auto-32|<tuple|Avogadro constant|?>>
    <associate|auto-33|<tuple|Elementary charge|?>>
    <associate|auto-34|<tuple|<tuple|si|2019 revision>|?>>
    <associate|auto-35|<tuple|1.1.4|?>>
    <associate|auto-36|<tuple|1.1.2|?>>
    <associate|auto-39|<tuple|Units|?>>
    <associate|auto-4|<tuple|SI|?>>
    <associate|auto-40|<tuple|SI|?>>
    <associate|auto-41|<tuple|1.1.3|?>>
    <associate|auto-43|<tuple|Units|?>>
    <associate|auto-44|<tuple|1.1.4|?>>
    <associate|auto-45|<tuple|1.2|?>>
    <associate|auto-46|<tuple|Quantity calculus|?>>
    <associate|auto-47|<tuple|Units|?>>
    <associate|auto-48|<tuple|Ideal gas|?>>
    <associate|auto-49|<tuple|Conditions of validity|?>>
    <associate|auto-5|<tuple|SI|?>>
    <associate|auto-50|<tuple|1.3|?>>
    <associate|auto-51|<tuple|Dimensional analysis|?>>
    <associate|auto-52|<tuple|Differential|?>>
    <associate|auto-53|<tuple|Dimensional analysis|?>>
    <associate|auto-6|<tuple|Syst�me International d'Unit�s|?>>
    <associate|auto-7|<tuple|<with|font-shape|<quote|small-caps>|IUPAC> Green
    Book|?>>
    <associate|auto-8|<tuple|IUPAC|?>>
    <associate|auto-9|<tuple|base units|?>>
    <associate|c1|<tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Chapter
    1>|?>>
    <associate|c1 eq ideal_gas_example|<tuple|1.2.6|?>>
    <associate|c1 eq ideal_gas_law|<tuple|1.2.5|?>>
    <associate|c1 problem|<tuple|1.3.1|?>>
    <associate|c1 sec da|<tuple|1.3|?>>
    <associate|c1 sec qc|<tuple|1.2|?>>
    <associate|c1 sec qus|<tuple|1.1|?>>
    <associate|c1 sec qus-2019|<tuple|1.1.3|?>>
    <associate|c1 sec qus-amount|<tuple|1.1.2|?>>
    <associate|c1 sec qus-derived|<tuple|1.1.4|?>>
    <associate|c1 sec qus-si|<tuple|1.1.1|?>>
    <associate|c1 tab non_si_derived_units|<tuple|1.1.3|?>>
    <associate|c1 tab si_base_units|<tuple|1.1.1|?>>
    <associate|c1 tab si_derived_units|<tuple|1.1.2|?>>
    <associate|c1 tab si_prefixes|<tuple|1.1.4|?>>
    <associate|footnote-1.1.1|<tuple|1.1.1|?>>
    <associate|footnote-1.1.2|<tuple|1.1.2|?>>
    <associate|footnote-1.1.3|<tuple|1.1.3|?>>
    <associate|footnote-1.1.4|<tuple|1.1.4|?>>
    <associate|footnote-1.2.1|<tuple|1.2.1|?>>
    <associate|footnr-1.1.1|<tuple|1.1.1|?>>
    <associate|footnr-1.1.2|<tuple|1.1.2|?>>
    <associate|footnr-1.1.3|<tuple|Chemical amount|?>>
    <associate|footnr-1.1.4|<tuple|1.1.4|?>>
    <associate|footnr-1.2.1|<tuple|Conditions of validity|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|gly>
      <tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Chapter
      1>>

      <tuple|normal|SI|<pageref|auto-5>>

      <tuple|normal|Syst�me International d'Unit�s|<pageref|auto-6>>

      <tuple|normal|base units|<pageref|auto-10>>

      <tuple|normal|amount of substance|<pageref|auto-17>>

      <tuple|normal|mole|<pageref|auto-20>>

      <tuple|normal|amount|<pageref|auto-23>>
    </associate>
    <\associate|idx>
      <tuple|International System of Units||International System of Units,
      <with|font-shape|<quote|italic>|see>
      <with|font-shape|<quote|small-caps>|SI>>

      <tuple|<tuple|SI>|<pageref|auto-4>>

      <tuple|Systeme International dUnites||Syst�me International d'Unit�s,
      <with|font-shape|<quote|italic>|see>
      <with|font-shape|<quote|small-caps>|SI>>

      <tuple|<tuple|<with|font-shape|<quote|small-caps>|IUPAC> Green
      Book>|<pageref|auto-7>>

      <tuple|International Union of Pure and Applied Chemistry||International
      Union of Pure and Applied Chemistry,
      <with|font-shape|<quote|italic>|see>
      <with|font-shape|<quote|small-caps>|IUPAC>>

      <tuple|<tuple|IUPAC>|<pageref|auto-8>>

      <tuple|<tuple|base units>|<pageref|auto-9>>

      <tuple|<tuple|Units|SI>|<pageref|auto-14>>

      <tuple|<tuple|SI|base units>|<pageref|auto-15>>

      <tuple|Amount of substance||Amount of substance,
      <with|font-shape|<quote|italic>|see> Amount>

      <tuple|<tuple|Elementary entity>|<pageref|auto-18>>

      <tuple|<tuple|Mole>|<pageref|auto-19>>

      <tuple|<tuple|Avogadro constant>|<pageref|auto-21>>

      <tuple|<tuple|Amount>|<pageref|auto-22>>

      <tuple|<tuple|Chemical amount>|<pageref|auto-24>>

      <tuple|<tuple|si|2019 revision>||c1 sec qus-2019 idx1|<tuple|SI|2019
      revision>|<pageref|auto-26>>

      <tuple|<tuple|Defining constants>|<pageref|auto-27>>

      <tuple|<tuple|International prototype>|<pageref|auto-28>>

      <tuple|<tuple|Kilogram|international prototype>|<pageref|auto-29>>

      <tuple|<tuple|Planck constant>|<pageref|auto-30>>

      <tuple|<tuple|Boltzmann constant>|<pageref|auto-31>>

      <tuple|<tuple|Avogadro constant>|<pageref|auto-32>>

      <tuple|<tuple|Elementary charge>|<pageref|auto-33>>

      <tuple|<tuple|si|2019 revision>||c1 sec qus-2019 idx1|<tuple|SI|2019
      revision>|<pageref|auto-34>>

      <tuple|<tuple|Units|SI derived>|<pageref|auto-39>>

      <tuple|<tuple|SI|derived units>|<pageref|auto-40>>

      <tuple|<tuple|Units|Non-SI>|<pageref|auto-43>>

      <tuple|<tuple|Quantity calculus>|<pageref|auto-46>>

      <tuple|<tuple|Units>|<pageref|auto-47>>

      <tuple|<tuple|Ideal gas|equation>|<pageref|auto-48>>

      <tuple|<tuple|Conditions of validity>|<pageref|auto-49>>

      <tuple|<tuple|Dimensional analysis>|<pageref|auto-51>>

      <tuple|<tuple|Differential>|<pageref|auto-52>>

      <tuple|<tuple|Dimensional analysis>|<pageref|auto-53>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1.1.1>|>
        <assign|the-label|Units><flag|index|dark
        green|key><assign|auto-nr|12><write|idx|<tuple|<tuple|Units|SI>|<pageref|auto-12>>><assign|the-label|SI><flag|index|dark
        green|key><assign|auto-nr|13><write|idx|<tuple|<tuple|SI|base
        units>|<pageref|auto-13>>>SI base units
      </surround>|<pageref|auto-13>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|1.1.2>|>
        <assign|the-label|Units><flag|index|dark
        green|key><assign|auto-nr|37><write|idx|<tuple|<tuple|Units|SI
        derived>|<pageref|auto-37>>><assign|the-label|SI><flag|index|dark
        green|key><assign|auto-nr|38><write|idx|<tuple|<tuple|SI|derived
        units>|<pageref|auto-38>>><with|font-shape|<quote|small-caps>|SI>
        derived units
      </surround>|<pageref|auto-38>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|1.1.3>|>
        <assign|the-label|Units><flag|index|dark
        green|key><assign|auto-nr|42><write|idx|<tuple|<tuple|Units|Non-SI>|<pageref|auto-42>>>Non-<with|font-shape|<quote|small-caps>|SI>
        derived units
      </surround>|<pageref|auto-42>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|1.1.4>|>
        <with|font-shape|<quote|small-caps>|SI> prefixes
      </surround>|<pageref|auto-44>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Introduction>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Physical Quantities, Units, and Symbols
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      <with|par-left|<quote|1tab>|1.1.1<space|2spc>The International System
      of Units <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.1.2<space|2spc>Amount of substance and
      amount <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>

      <with|par-left|<quote|1tab>|1.1.3<space|2spc>The
      <with|font-shape|<quote|small-caps>|SI> revision of 2019
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-25>>

      <with|par-left|<quote|1tab>|1.1.4<space|2spc>Derived units and prefixes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-35>>

      1.2<space|2spc>Quantity Calculus <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-45>

      1.3<space|2spc>Dimensional Analysis
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-50>
    </associate>
  </collection>
</auxiliary>