<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <no-indent>BIOGRAPHICAL SKETCH

  <paragraph|<person|Sadi Carnot> (1796\U1832)>

  <htab|5mm><image|BIO/carnot.png|89pt|115pt||><htab|5mm>

  <label|bio:carnot><index|Carnot, Sadi><no-indent>Sadi Carnot was the eldest
  son of Lazare Carnot, a famous French anti-royalist politician, one of
  Napoleon's generals with a great interest in mathematics. As a boy Sadi was
  shy and sensitive. He studied at the �cole Polytechnique, a training school
  for army engineers, and became an army officer.

  Carnot is renowned for the one book he wrote: a treatise of 118 pages
  entitled <em|Reflections on the Motive Power of Fire and on Machines Fitted
  to Develop that Power>. This was published in 1824, when he was 28 and had
  retired from the army on half pay.

  The book was written in a nontechnical style and went virtually unnoticed.
  Its purpose was to show how the efficiency of a steam engine could be
  improved, a very practical matter since French power technology lagged
  behind that of Britain at the time:<footnote|Ref. <cite|carnot-1824>.>

  <\quotation>
    Notwithstanding the work of all kinds done by steam-engines,
    notwithstanding the satisfactory condition to which they have been
    brought today, their theory is very little understood, and the attempts
    to improve them are still directed almost by chance.

    ...We can easily conceive a multitude of machines fitted to develop the
    motive power of heat through the use of elastic fluids; but in whatever
    way we look at it, we should not lose sight of the following principles:

    (1) The temperature of the fluid should be made as high as possible, in
    order to obtain a great fall of caloric, and consequently a large
    production of motive power.

    (2) For the same reason the cooling should be carried as far as possible.

    (3) It should be so arranged that the passage of the elastic fluid from
    the highest to the lowest temperature should be due to increase of
    volume; that is, it should be so arranged that the cooling of the gas
    should occur spontaneously as the result of rarefaction [i.e., adiabatic
    expansion].
  </quotation>

  Carnot derived these principles from the abstract reversible cycle now
  called the Carnot cycle. He assumed the validity of the caloric theory
  (heat as an indestructible substance), which requires that the net heat in
  the cycle be zero, whereas today we would say that it is the net entropy
  change that is zero.

  Despite the flaw of assuming that heat is conserved, a view which there is
  evidence he was beginning to doubt, his conclusion was valid that the
  efficiency of a reversible cycle operating between two fixed temperatures
  is independent of the working substance. He based his reasoning on the
  impossibility of the perpetual motion which would result by combining the
  cycle with the reverse of a more efficient cycle. Regarding Carnot's
  accomplishment, William Thomson (later Lord Kelvin) wrote:

  <quotation|Nothing in the whole range of Natural Philosophy is more
  remarkable than the establishment of general laws by such a process of
  reasoning.>

  A biographer described Carnot's personality as follows:<footnote|Ref.
  <cite|mendoza-88>, page x.>

  <quotation|He was reserved, almost taciturn, with a hatred of any form of
  publicity. ...his friends all spoke of his underlying warmth and humanity.
  Passionately fond of music, he was an excellent violinist who preferred the
  classical Lully to the \Pmoderns\Q of the time; he was devoted to
  literature and all the arts.>

  Carnot came down with scarlet fever and, while convalescing,
  died<emdash>probably of the cholera epidemic then raging. He was only 36.

  Two years later his work was brought to public attention in a paper written
  by �mile Clapeyron (page <pageref|bio:clapeyron>), who used indicator
  diagrams to explain Carnot's ideas.
</body>

<\initial>
  <\collection>
    <associate|font-base-size|9>
    <associate|page-medium|paper>
    <associate|par-columns|2>
    <associate|par-columns-sep|1fn>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|Carnot, Sadi|1>>
    <associate|bio:carnot|<tuple|1|1>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|1>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      carnot-1824

      mendoza-88
    </associate>
    <\associate|idx>
      <tuple|<tuple|Carnot, Sadi>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Sadi
      Carnot> (1796\U1832) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>