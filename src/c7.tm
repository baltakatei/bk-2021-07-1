<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;
  </hide-preamble>

  <chapter|Pure Substances in Single Phases><label|Chap. 7><label|c7>

  This chapter applies concepts introduced in earlier chapters to the
  simplest kind of system, one consisting of a pure substance or a single
  component in a single phase. The system has three independent variables if
  it is open, and two if it is closed. Relations among various properties of
  a single phase are derived, including temperature, pressure, and volume.
  The important concepts of standard states and chemical potential are
  introduced.

  <section|Volume Properties><label|c7 sec vp>

  Two volume properties of a closed system are defined as follows:

  <alignat|2|<tformat|<table|<row|<cell|>|<cell|<index|Cubic expansion
  coefficient><newterm|cubic expansion coefficient>>|<cell|<space|1em>\<alpha\>>|<cell|<defn><frac|1|V><Pd|V|T|<space|-0.17em>p><eq-number><label|alpha
  def>>>|<row|<cell|>|<cell|<subindex|Isothermal|compressibility><newterm|isothermal
  compressibility>>|<cell|<space|1em><kT>>|<cell|<defn>-<frac|1|V><Pd|V|p|T><eq-number><label|kappaT
  def>>>>>>

  <\quote-env>
    The cubic expansion coefficient is also called the <index|Coefficient of
    thermal expansion>coefficient of thermal expansion and the
    <index|Expansivity coefficient>expansivity coefficient. Other symbols for
    the isothermal compressibility are <math|\<beta\>> and
    <math|<g><rsub|T>>.
  </quote-env>

  These definitions show that <math|\<alpha\>> is the fractional volume
  increase per unit temperature increase at constant pressure, and
  <math|<kT>> is the fractional volume decrease per unit pressure increase at
  constant temperature. Both quantities are <em|intensive> properties. Most
  substances have positive values of <math|\<alpha\>>,<footnote|The cubic
  expansion coefficient is not always positive. <math|\<alpha\>> is negative
  for liquid water below its temperature of maximum density, <math|3.98
  <degC>>. The crystalline ceramics zirconium tungstate
  (ZrW<rsub|<math|2>>O<rsub|<math|8>>) and hafnium tungstate
  (HfW<rsub|<math|2>>O<rsub|<math|8>>) have the remarkable behavior of
  contracting uniformly and continuously in all three dimensions when they
  are heated from <math|0.3 >K to about <math|1050 >K; <subindex|Cubic
  expansion coefficient|negative values of><math|\<alpha\>> is negative
  throughout this very wide temperature range (Ref. <cite|mary-96>). The
  intermetallic compound YbGaGe has been found to have a value of
  <math|\<alpha\>> that is practically zero in the range
  <math|100>\U<math|300 >K (Ref. <cite|salvador-03>).> and all substances
  have positive values of <math|<kT>>, because a pressure increase at
  constant temperature requires a volume decrease.

  If an amount <math|n> of a substance is in a single phase, we can divide
  the numerator and denominator of the right sides of Eqs. <reference|alpha
  def> and <reference|kappaT def> by <math|n> to obtain the alternative
  expressions

  <equation-cov2|<label|alpha(Vm)>\<alpha\>=<frac|1|V<m>><Pd|V<m>|T|<space|-0.17em>p>|(pure
  substance, <math|P=1>)>

  <equation-cov2|<label|kappaT(Vm)><kT>=-<frac|1|V<m>><Pd|V<m>|p|T>|(pure
  substance, <math|P=1>)>

  where <math|V<m>> is the molar volume. <math|P> in the conditions of
  validity is the number of phases. Note that only intensive properties
  appear in Eqs. <reference|alpha(Vm)> and <reference|kappaT(Vm)>; the amount
  of the substance is irrelevant. Figures <reference|fig:7-alpha vs T> and
  <reference|fig:7-kappaT vs T> show the temperature variation of
  <math|\<alpha\>> and <math|<kT>> for several substances.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|07-SUP/ALPHA.eps|243pt|228pt||>
      <|big-figure>
        <label|fig:7-alpha vs T>The cubic expansion coefficient of several
        substances and an ideal gas as functions of temperature at
        <math|p=1<br>>.<note-ref|+pnwft35dEGUGeY> Note that because liquid
        water has a density maximum at <math|4 <degC>>, <math|\<alpha\>> is
        zero at that temperature.

        \;

        <note-inline|Based on data in Ref. <cite|eisenberg-69>, p. 104; Ref.
        <cite|grigull-90>; and Ref. <cite|ICT-3>, p. 28.|+pnwft35dEGUGeY>
      </big-figure>
    </framed>
  </float><\float|float|thb>
    <\framed>
      <\big-figure>
        <image|07-SUP/KAPPAT.eps|211pt|197pt||>
      <|big-figure>
        <label|fig:7-kappaT vs T>The isothermal compressibility of several
        substances as a function of temperature at <math|p=1<br>>. (Based on
        data in Ref. <cite|eisenberg-69>; Ref. <cite|kell-75>; and Ref.
        <cite|ICT-3>, p. 28.)
      </big-figure>
    </framed>
  </float>

  If we choose <math|T> and <math|p> as the independent variables of the
  closed system, the total differential of <math|V> is given by

  <\equation>
    <dif>V=<Pd|V|T|<space|-0.17em>p><dif>T+<Pd|V|p|T><difp>
  </equation>

  With the substitutions <math|<pd|V|T|p>=\<alpha\>*V> (from Eq.

  <reference|alpha def>) and <math|<pd|V|p|T>=-<kT>V> (from Eq.

  <reference|kappaT def>), the expression for the total differential of
  <math|V> becomes

  <\equation-cov2|<label|dV=alpha*VdT-kappaT*Vdp><dif>V=\<alpha\>*V<dif>T-<kT>V<difp>>
    (closed system,

    <math|C=1>, <math|P=1>)
  </equation-cov2>

  To find how <math|p> varies with <math|T> in a closed system kept at
  constant volume, we set <math|<dif>V> equal to zero in Eq.
  <reference|dV=alpha*VdT-kappaT*Vdp>: <math|0=\<alpha\>*V<dif>T-\<kappa\><rsub|T>*V<difp>>,
  or <math|<difp>/<dif>T=\<alpha\>/\<kappa\><rsub|T>>. Since
  <math|<difp>/<dif>T> under the condition of constant volume is the partial
  derivative <math|<pd|p|T|V>>, we have the general relation

  <\equation-cov2|<label|dp/dT=alpha/kappaT><Pd|p|T|V>=<frac|\<alpha\>|<kT>>>
    (closed system,

    <math|C=1>,<math|P=1>)
  </equation-cov2>

  <section|Internal Pressure><label|7-int pressure><label|c7 sec ip>

  <index-complex|<tuple|internal|pressure>||c7 sec ip
  idx1|<tuple|Internal|pressure>><index-complex|<tuple|pressure|internal>||c7
  sec ip idx2|<tuple|Pressure|internal>>The partial derivative
  <math|<pd|U|V|T>> applied to a fluid phase in a closed system is called the
  <subindex|Internal|pressure><subindex|Pressure|internal><newterm|internal
  pressure>. (Note that <math|U> and <math|p*V> have dimensions of energy;
  therefore, <math|U/V> has dimensions of pressure.)

  To relate the internal pressure to other properties, we divide Eq.

  <reference|dU=TdS-pdV> by <math|<dif>V>:
  <math|<dif>U/<dif>V=T*<around|(|<dif>S/<dif>V|)>-p>. Then we impose a
  condition of constant <math|T>: <math|<pd|U|V|T>=T<pd|S|V|T>-p>. When we
  make a substitution for <math|<pd|S|V|T>> from the Maxwell relation of Eq.
  <reference|dS/dV=dp/dT>, we obtain

  <\equation-cov2|<label|dU/dV=Tdp/dT-p><Pd|U|V|T>=T<Pd|p|T|V>-p>
    (closed system,

    fluid phase, <math|C=1>)
  </equation-cov2>

  This equation is sometimes called the <subindex|Thermodynamic|equation of
  state><subindex|Equation of state|thermodynamic>\Pthermodynamic equation of
  state\Q of the fluid.

  For an ideal-gas phase, we can write <math|p=n*R*T/V> and then

  <\equation>
    <Pd|p|T|V>=<frac|n*R|V>=<frac|p|T>
  </equation>

  Making this substitution in Eq. <reference|dU/dV=Tdp/dT-p> gives us

  <\equation-cov2|<label|dU/dV=0 (id gas)><Pd|U|V|T>=0>
    (closed system

    of an ideal gas)
  </equation-cov2>

  showing that the <index-complex|<tuple|internal|pressure|ideal
  gas>|||<tuple|Internal|pressure|of an ideal gas>><subindex|Ideal
  gas|internal pressure>internal pressure of an ideal gas is zero.

  <\quote-env>
    \ In Sec. <reference|3-U of ideal gas>, an ideal gas was defined as a gas
    (1) that obeys the ideal gas equation, and (2) for which <math|U> in a
    closed system depends only on <math|T>. Equation <reference|dU/dV=0 (id
    gas)>, derived from the first part of this definition, expresses the
    second part. It thus appears that the second part of the definition is
    redundant, and that we could define an ideal gas simply as a gas obeying
    the ideal gas equation. This argument is valid only if we assume the
    <index-complex|<tuple|temperature|ideal
    gas>|||<tuple|Temperature|ideal-gas>>ideal-gas temperature is the same as
    the <subindex|Temperature|thermodynamic><subindex|Thermodynamic|temperature>thermodynamic
    temperature (Secs. <reference|2-temperature> and
    <reference|4-thermodynamic temperature>) since this assumption is
    required to derive Eq. <reference|dU/dV=0 (id gas)>. Without this
    assumption, we can't define an ideal gas solely by <math|p*V=n*R*T>,
    where <math|T> is the ideal gas temperature.
  </quote-env>

  Here is a simplified interpretation of the significance of the internal
  pressure. When the volume of a fluid increases, the average distance
  between molecules increases and the potential energy due to intermolecular
  forces changes. If attractive forces dominate, as they usually do unless
  the fluid is highly compressed, expansion causes the potential energy to
  <em|increase>. The internal energy is the sum of the potential energy and
  thermal energy. The internal pressure, <math|<pd|U|V|T>>, is the rate at
  which the internal energy changes with volume at constant temperature. At
  constant temperature, the thermal energy is constant so that the internal
  pressure is the rate at which just the potential energy changes with
  volume. Thus, the internal pressure is a measure of the strength of the
  intermolecular forces and is positive if attractive forces
  dominate.<footnote|These attractive intermolecular forces are the cohesive
  forces that can allow a <subindex|Pressure|negative>negative pressure to
  exist in a liquid; see page <pageref|neg p>.> In an ideal gas,
  intermolecular forces are absent and therefore the internal pressure of an
  ideal gas is zero.

  With the substitution <math|<pd|p|T|V>=\<alpha\>/<kT>> (Eq.
  <reference|dp/dT=alpha/kappaT>), Eq. <reference|dU/dV=Tdp/dT-p> becomes

  <\equation-cov2|<label|dU/dV=alpha*T/kappaT-p><Pd|U|V|T>=<frac|\<alpha\>*T|<kT>>-p>
    (closed system,

    fluid phase, <math|C=1>)
  </equation-cov2>

  The internal pressure of a liquid at <math|p=1 <text|bar>> is typically
  much larger than <math|1 <text|bar>> (see Prob. <reference|prb:7-aniline>).
  Equation <reference|dU/dV=alpha*T/kappaT-p> shows that, in this situation,
  the internal pressure is approximately equal to
  <math|\<alpha\>*T/<kT>>.<index-complex|<tuple|internal|pressure>||c7 sec ip
  idx1|<tuple|Internal|pressure>><index-complex|<tuple|pressure|internal>||c7
  sec ip idx2|<tuple|Pressure|internal>>

  <section|Thermal Properties><label|c7 sec tp>

  For convenience in derivations to follow, expressions from Chap.
  <reference|Chap. 5> are repeated here that apply to processes in a closed
  system in the absence of nonexpansion work (i.e., <math|<dw><rprime|'>=0>).
  For a process at <em|constant volume> we have<footnote|Eqs.
  <reference|dU=dq (dV=0)> and <reference|CV=dU/dT>.>

  <\equation>
    <label|dU=dq,C_V=dU/dT><dif>U=<dq><space|2em>C<rsub|V>=<Pd|U|T|V>
  </equation>

  and for a process at <em|constant pressure> we have<footnote|Eqs.
  <reference|dH=dq (dp=0)> and <reference|Cp=dH/dT>.>

  <\equation>
    <label|dH=dq,C_p=dH/dT><dif>H=<dq><space|2em>C<rsub|p>=<Pd|H|T|<space|-0.17em>p>
  </equation>

  A closed system of one component in a single phase has only two independent
  variables. In such a system, the partial derivatives above are complete and
  unambiguous definitions of <math|C<rsub|V>> and <math|C<rsub|p>> because
  they are expressed with two independent variables\V<math|T> and <math|V>
  for <math|C<rsub|V>>, and <math|T> and <math|p> for <math|C<rsub|p>>. As
  mentioned on page <pageref|ht cap conditions>, additional conditions would
  have to be specified to define <math|C<rsub|V>> for a more complicated
  system; the same is true for <math|C<rsub|p>>.

  For a closed system of an <em|ideal gas> we have<footnote|Eqs.
  <reference|CV=dU/dT (id gas)> and <reference|Cp=dH/dT (id gas)>.>

  <\equation>
    <label|C_V=,C_p=(id gas)>C<rsub|V>=<frac|<dif>U|<dif>T>*<space|2em>C<rsub|p>=<frac|<dif>H|<dif>T>
  </equation>

  <subsection|The relation between <math|C<rsub|V,<text|m>>> and
  <math|C<rsub|p,<text|m>>>><label|7-relation between Cpm and CVm><label|c7
  sec tp-cvcp>

  <index-complex|<tuple|heat capacity|constant volume and constant
  pressure>|||<tuple|Heat capacity|at constant volume and constant pressure,
  relation between>>The value of <math|<Cpm>> for a substance is greater than
  <math|<CVm>>. The derivation is simple in the case of a fixed amount of an
  <em|ideal gas>. Using substitutions from Eq. <reference|C_V=,C_p=(id gas)>,
  we write

  <\equation>
    C<rsub|p>-C<rsub|V>=<frac|<dif>H|<dif>T>-<frac|<dif>U|<dif>T>=<frac|<dif><around|(|H-U|)>|<dif>T>=<frac|<dif><around|(|p*V|)>|<dif>T>=n*R
  </equation>

  Division by <math|n> to obtain molar quantities and rearrangement then
  gives

  <equation-cov2|<label|Cpm=CVm+R><Cpm>=<CVm>+R|(ideal gas, pure substance)>

  For any phase in general, we proceed as follows. First we write

  <\equation>
    C<rsub|p>=<Pd|H|T|<space|-0.17em>p>=<bPd|<around|(|U+p*V|)>|T|p>=<Pd|U|T|<space|-0.17em>p>+p<Pd|V|T|<space|-0.17em>p>
  </equation>

  Then we write the total differential of <math|U> with <math|T> and <math|V>
  as independent variables and identify one of the coefficients as
  <math|C<rsub|V>>:

  <\equation>
    <dif>U=<Pd|U|T|V><dif>T+<Pd|U|V|T><dif>V=C<rsub|V><dif>T+<Pd|U|V|T><dif>V
  </equation>

  When we divide both sides of the preceding equation by <math|<dif>T> and
  impose a condition of constant <math|p>, we obtain

  <\equation>
    <Pd|U|T|<space|-0.17em>p>=C<rsub|V>+<Pd|U|V|T><Pd|V|T|<space|-0.17em>p>
  </equation>

  Substitution of this expression for <math|<pd|U|T|p>> in the equation for
  <math|C<rsub|p>> yields

  <\equation>
    C<rsub|p>=C<rsub|V>+<around*|[|<Pd|U|V|T>+p|]><Pd|V|T|<space|-0.17em>p>
  </equation>

  Finally we set the partial derivative <math|<pd|U|V|T>> (the
  <subindex|Pressure|internal><subindex|Internal|pressure>internal pressure)
  equal to <math|<around|(|\<alpha\>*T/<kT>|)>-p> (Eq.
  <reference|dU/dV=alpha*T/kappaT-p>) and <math|<pd|V|T|p>> equal to
  <math|\<alpha\>*V> to obtain

  <\equation>
    C<rsub|p>=C<rsub|V>+<frac|\<alpha\><rsup|2>*T*V|<kT>>
  </equation>

  and divide by <math|n> to obtain molar quantities:

  <\equation>
    <label|Cpm=CVm+alpha^2...><Cpm>=<CVm>+<frac|\<alpha\><rsup|2>*T*V<m>|<kT>>
  </equation>

  Since the quantity <math|\<alpha\><rsup|2>*T*V<m>/<kT>> must be positive,
  <math|<Cpm>> is greater than <math|<CVm>>.

  <subsection|The measurement of heat capacities><label|7-ht cap
  measurement><label|c7 sec tp-heatcap>

  The most accurate method of evaluating the <subindex|Heat
  capacity|measurement of, by calorimetry><index-complex|<tuple|calorimetry|measure
  heat capacities>|||<tuple|Calorimetry|to measure heat capacities>>heat
  capacity of a phase is by measuring the temperature change resulting from
  heating with <subindex|Electrical|work><subindex|Work|electrical><subindex|Electrical|heating><subindex|Heating|electrical>electrical
  work. The procedure in general is called calorimetry, and the apparatus
  containing the phase of interest and the electric heater is a
  <index|Calorimeter><newterm|calorimeter>. The principles of three
  commonly-used types of calorimeters with electrical heating are described
  below.

  <subsubsection|Adiabatic calorimeters><label|c7 sec tp-heatcap-ac>

  An <index-complex|<tuple|adiabatic|calorimeter>||c7 sec tp-heatcap-ac
  idx1|<tuple|Adiabatic|calorimeter>><index-complex|<tuple|calorimeter|adiabatic>||c7
  sec tp-heatcap-ac idx2|<tuple|Calorimeter|adiabatic>>adiabatic calorimeter
  is designed to have negligible heat flow to or from its surroundings. The
  calorimeter contains the phase of interest, kept at either constant volume
  or constant pressure, and also an electric heater and a
  temperature-measuring device such as a platinum resistance thermometer,
  thermistor, or quartz crystal oscillator. The contents may be stirred to
  ensure temperature uniformity.

  To minimize conduction and convection, the calorimeter usually is
  surrounded by a jacket separated by an air gap or an evacuated space. The
  outer surface of the calorimeter and inner surface of the jacket may be
  polished to minimize radiation emission from these surfaces. These
  measures, however, are not sufficient to ensure a completely adiabatic
  boundary, because energy can be transferred by heat along the mounting
  hardware and through the electrical leads. Therefore, the temperature of
  the jacket, or of an outer metal shield, is adjusted throughout the course
  of the experiment so as to be as close as possible to the varying
  temperature of the calorimeter. This goal is most easily achieved when the
  temperature change is slow.

  To make a heat capacity measurement, a constant
  <subindex|Electric|current><index|Current, electric>electric current is
  passed through the <index|Heater circuit><subindex|Circuit|heater>heater
  circuit for a known period of time. The <em|system> is the calorimeter and
  its contents. The <subindex|Electrical|work><subindex|Work|electrical>electrical
  work <math|w<el>> performed on the system by the heater circuit is
  calculated from the integrated form of Eq. <vpageref|dw=I2*Rdt>:
  <math|w<el>=I<rsup|2>*R<el><Del>t>, where <math|I> is the
  <subindex|Electric|current><index|Current, electric>electric current,
  <math|R<el>> is the <subindex|Electric|resistance><subindex|Resistance|electric>electric
  resistance, and <math|<Del>t> is the time interval. We assume the boundary
  is adiabatic and write the first law in the form

  <\equation>
    <label|dU=-pdV+dw(el)+dw(cont)><dif>U=-p<dif>V+<dw><el>+<dw><rsub|<text|cont>>
  </equation>

  where <math|-p<dif>V> is expansion work and <math|w<rsub|<text|cont>>> is
  any continuous mechanical work from stirring (the subscript \Pcont\Q stands
  for continuous). If <subindex|Electrical|work><subindex|Work|electrical>electrical
  work is done on the system by a thermometer using an external electrical
  circuit, such as a platinum resistance thermometer, this work is included
  in <math|w<rsub|<text|cont>>>.

  Consider first an adiabatic calorimeter in which the heating process is
  carried out at <subindex|Calorimeter|constant-volume><em|constant volume>.
  There is no expansion work, and Eq. <reference|dU=-pdV+dw(el)+dw(cont)>
  becomes

  <equation-cov2|<label|dU=dw(el)+dw(cont)><dif>U=<dw><el>+<dw><rsub|<text|cont>>|(constant
  <math|V>)>

  An example of a <subindex|Heating|curve, of a calorimeter>measured heating
  curve (temperature <math|T> as a function of time <math|t>) is shown in
  Fig. <reference|fig:7-ad heating curve>.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|07-SUP/ADHEAT.eps|207pt|119pt||>
      <|big-figure>
        <label|fig:7-ad heating curve>Typical heating curve of an adiabatic
        calorimeter.
      </big-figure>
    </framed>
  </float>

  We select two points on the heating curve, indicated in the figure by open
  circles. Time <math|t<rsub|1>> is at or shortly before the instant the
  <index|Heater circuit><subindex|Circuit|heater>heater circuit is closed and
  <subindex|Electrical|heating><subindex|Heating|electrical>electrical
  heating begins, and time <math|t<rsub|2>> is after the heater circuit has
  been opened and the slope of the curve has become essentially constant.

  In the time periods before <math|t<rsub|1>> and after <math|t<rsub|2>>, the
  temperature may exhibit a slow rate of increase due to the continuous work
  <math|w<rsub|<text|cont>>> from stirring and temperature measurement. If
  this work is performed at a constant rate throughout the course of the
  experiment, the slope is constant and the same in both time periods as
  shown in the figure.

  The relation between the slope and the rate of work is given by a quantity
  called the <index|Energy equivalent><newterm|energy equivalent>,
  <math|\<epsilon\>>.<label|energy equiv>The energy equivalent is the heat
  capacity of the calorimeter under the conditions of an experiment. The heat
  capacity of a constant-volume calorimeter is given by
  <math|\<epsilon\>=<pd|U|T|V>> (Eq. <reference|CV=dU/dT>). Thus, at times
  before <math|t<rsub|1>> or after <math|t<rsub|2>>, when <math|<dw><el>> is
  zero and <math|<dif>U> equals <math|<dw><rsub|<text|cont>>>, the slope
  <math|r> of the heating curve is given by

  <\equation>
    r=<frac|<dif>T|<dt>>=<frac|<dif>T|<dif>U>*<frac|<dif>U|<dt>>=<frac|1|\<epsilon\>>*<frac|<dw><rsub|<text|cont>>|<dt>>
  </equation>

  The rate of the continuous work is therefore
  <math|<dw><rsub|<text|cont>>/<dt>=\<epsilon\>*r>. This rate is constant
  throughout the experiment. In the time interval from <math|t<rsub|1>> to
  <math|t<rsub|2>>, the total quantity of continuous work is
  <math|w<rsub|<text|cont>>=\<epsilon\>*r*<around|(|t<rsub|2>-t<rsub|1>|)>>,
  where <math|r> is the slope of the heating curve measured <em|outside> this
  time interval.

  To find the energy equivalent, we integrate Eq.
  <reference|dU=dw(el)+dw(cont)> between the two points on the curve:

  <equation-cov2|<label|delU=w(el)+er(t2-t1)><Del>U=w<el>+w<rsub|<text|cont>>=w<el>+\<epsilon\>*r*<around|(|t<rsub|2>-t<rsub|1>|)>|(constant
  <math|V>)>

  Then the average heat capacity between temperatures <math|T<rsub|1>> and
  <math|T<rsub|2>> is

  <\equation>
    \<epsilon\>=<frac|<Del>U|T<rsub|2>-T<rsub|1>>=<frac|w<el>+\<epsilon\>*r*<around|(|t<rsub|2>-t<rsub|1>|)>|T<rsub|2>-T<rsub|1>>
  </equation>

  Solving for <math|\<epsilon\>>, we obtain

  <\equation>
    <label|e=w(el)/(T2-T1-r(t2-t1))>\<epsilon\>=<frac|w<el>|T<rsub|2>-T<rsub|1>-r*<around|(|t<rsub|2>-t<rsub|1>|)>>
  </equation>

  The value of the denominator on the right side is indicated by the vertical
  line in Fig. <reference|fig:7-ad heating curve>. It is the temperature
  change that would have been observed if the same quantity of
  <subindex|Electrical|work><subindex|Work|electrical>electrical work had
  been performed without the continuous work.

  Next, consider the heating process in a calorimeter at
  <subindex|Calorimeter|constant-pressure><em|constant pressure>. In this
  case the enthalpy change is given by <math|<dif>H=<dif>U+p<dif>V> which,
  with substitution from Eq. <reference|dU=-pdV+dw(el)+dw(cont)>, becomes

  <equation-cov2|<label|dH=dw(el)+dw(cont)><dif>H=<dw><el>+<dw><inactive|<rsub|<text|cont>>>|(constant
  <math|p>)>

  We follow the same procedure as for the constant-volume calorimeter, using
  Eq. <reference|dH=dw(el)+dw(cont)> in place of Eq.
  <reference|dU=dw(el)+dw(cont)> and equating the <index|Energy
  equivalent>energy equivalent <math|\<epsilon\>> to <math|<pd|H|T|p>>, the
  heat capacity of the calorimeter at constant pressure (Eq.
  <reference|Cp=dH/dT>). We obtain the relation

  <equation-cov2|<label|delH=w(el)+er(t2-t1)><Del>H=w<el>+w<rsub|<text|cont>>=w<el>+\<epsilon\>*r*<around|(|t<rsub|2>-t<rsub|1>|)>|(constant
  <math|p>)>

  in place of Eq. <reference|delU=w(el)+er(t2-t1)> and end up again with the
  expression of Eq. <reference|e=w(el)/(T2-T1-r(t2-t1))> for
  <math|\<epsilon\>>.

  The value of <math|\<epsilon\>> calculated from Eq.
  <reference|e=w(el)/(T2-T1-r(t2-t1))> is an <em|average> value for the
  temperature interval from <math|T<rsub|1>> to <math|T<rsub|2>>, and we can
  identify this value with the heat capacity at the temperature of the
  midpoint of the interval. By taking the difference of values of
  <math|\<epsilon\>> measured with and without the phase of interest present
  in the calorimeter, we obtain <math|C<rsub|V>> or <math|C<rsub|p>> for the
  phase alone.

  It may seem paradoxical that we can use an adiabatic process, one without
  heat, to evaluate a quantity defined by heat (<math|<text|heat
  capacity>=<dq>/<dif>T>). The explanation is that energy transferred into
  the adiabatic calorimeter as <subindex|Electrical|work><subindex|Work|electrical>electrical
  work, and <subindex|Energy|dissipation of><index|Dissipation of
  energy>dissipated completely to thermal energy, substitutes for the heat
  that would be needed for the same change of state without electrical
  work.<index-complex|<tuple|adiabatic|calorimeter>||c7 sec tp-heatcap-ac
  idx1|<tuple|Adiabatic|calorimeter>><index-complex|<tuple|calorimeter|adiabatic>||c7
  sec tp-heatcap-ac idx2|<tuple|Calorimeter|adiabatic>>

  <subsubsection|Isothermal\Ujacket calorimeters><label|7-isothermal-jacket><label|c7
  sec tp-heatcap-ijc>

  <index-complex|<tuple|calorimeter|isothermal-jacket>||c7 sec tp-heatcap-ijc
  idx1|<tuple|Calorimeter|isothermal\Ujacket>>A second common type of
  calorimeter is similar in construction to an adiabatic calorimeter, except
  that the surrounding jacket is maintained at constant temperature. It is
  sometimes called an <index|Isoperibol calorimeter><subindex|Calorimeter|isoperibol><em|isoperibol
  calorimeter>. A correction is made for heat transfer resulting from the
  difference in temperature across the gap separating the jacket from the
  outer surface of the calorimeter. It is important in making this correction
  that the outer surface have a uniform temperature without \Phot spots.\Q

  Assume the outer surface of the calorimeter has a uniform temperature
  <math|T> that varies with time, the jacket temperature has a constant value
  <math|T<rsub|<text|ext>>>, and convection has been eliminated by evacuating
  the gap. Then heat transfer is by conduction and radiation, and its rate is
  given by <index|Newton's law of cooling>Newton's law of cooling

  <\equation>
    <label|dq/dt=-k(T-T(ext))><frac|<dq>|<dt>>=-k*<around|(|T-T<rsub|<text|ext>>|)>
  </equation>

  where <math|k> is a constant (the <subindex|Thermal|conductance>thermal
  conductance). Heat flows from a warmer to a cooler body, so
  <math|<dq>/<dt>> is positive if <math|T> is less than
  <math|T<rsub|<text|ext>>> and negative if <math|T> is greater than
  <math|T<rsub|<text|ext>>>.

  The possible kinds of work are the same as for the adiabatic calorimeter:
  expansion work <math|-p<dif>V>, intermittent work <math|w<el>> done by the
  <index|Heater circuit><subindex|Circuit|heater>heater circuit, and
  continuous work <math|w<rsub|<text|cont>>>. By combining the first law and
  Eq. <reference|dq/dt=-k(T-T(ext))>, we obtain the following relation for
  the rate at which the internal energy changes:

  <\equation>
    <frac|<dif>U|<dt>>=<frac|<dq>|<dt>>+<frac|<dw>|<dt>>=-k*<around|(|T-T<rsub|<text|ext>>|)>-p*<frac|<dif>V|<dt>>+<frac|<dw><el>|<dt>>+<frac|<dw><rsub|<text|cont>>|<dt>>
  </equation>

  For heating at constant <em|volume> (<math|<dif>V/<dt>=0>), this relation
  becomes

  <equation-cov2|<label|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt><frac|<dif>U|<dt>>=-k*<around|(|T-T<rsub|<text|ext>>|)>+<frac|<dw><el>|<dt>>+<frac|<dw><rsub|<text|cont>>|<dt>>|(cosntant
  <math|V>)>

  An example of a <subindex|Heating|curve, of a calorimeter>heating curve is
  shown in Fig. <reference|fig:7-heating curve>.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|07-SUP/HEATCURV.eps|193pt|133pt||>
      <|big-figure>
        <label|fig:7-heating curve>Typical heating curve of an
        isothermal-jacket calorimeter.
      </big-figure>
    </framed>
  </float>

  In contrast to the curve of Fig. <reference|fig:7-ad heating curve>, the
  slopes are different before and after the heating interval due to the
  changed rate of heat flow. Times <math|t<rsub|1>> and <math|t<rsub|2>> are
  before and after the <index|Heater circuit><subindex|Circuit|heater>heater
  circuit is closed. In any time interval before time <math|t<rsub|1>> or
  after time <math|t<rsub|2>>, the system behaves as if it is approaching a
  steady state of constant temperature <math|T<rsub|\<infty\>>> (called the
  <index|Convergence temperature><subindex|Temperature|convergence>convergence
  temperature), which it would eventually reach if the experiment were
  continued without closing the heater circuit. <math|T<rsub|\<infty\>>> is
  greater than <math|T<rsub|<text|ext>>> because of the energy transferred to
  the system by stirring and electrical temperature measurement. By setting
  <math|<dif>U/<dt>> and <math|<dw><el>/<dt>> equal to zero and <math|T>
  equal to <math|T<rsub|\<infty\>>> in Eq.
  <reference|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt>, we obtain
  <math|<dw><rsub|<text|cont>>/<dt>=k*<around|(|T<rsub|\<infty\>>-T<rsub|<text|ext>>|)>>.
  We assume <math|<dw><rsub|<text|cont>>/<dt>> is constant. Substituting this
  expression into Eq. <reference|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt>
  gives us a general expression for the rate at which <math|U> changes in
  terms of the unknown quantities <math|k> and <math|T<rsub|\<infty\>>>:

  <equation-cov2|<label|dU/dt=-k(T-T(inf)+dw(el)/dt><frac|<dif>U|<dt>>=-k*<around|(|T-T<rsub|\<infty\>>|)>+<frac|<dw><el>|<dt>>|(constant
  <math|V>)>

  This relation is valid throughout the experiment, not only while the heater
  circuit is closed. If we multiply by <math|<dt>> and integrate from
  <math|t<rsub|1>> to <math|t<rsub|2>>, we obtain the internal energy change
  in the time interval from <math|t<rsub|1>> to <math|t<rsub|2>>:

  <equation-cov2|<label|delU=-k int((T-T(inf))dt+w(el)><Del>U=-k*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><around|(|T-T<rsub|\<infty\>>|)><dt>+w<el>|(constant
  <math|V>)>

  All the intermittent work <math|w<el>> is performed in this time interval.

  <\quote-env>
    \ The derivation of Eq. <reference|delU=-k int((T-T(inf))dt+w(el)> is a
    general one. The equation can be applied also to a isothermal-jacket
    calorimeter in which a reaction is occurring. Section <reference|11-bomb
    calorimeter> will mention the use of this equation for an internal energy
    correction of a reaction calorimeter with an isothermal jacket.
  </quote-env>

  The average value of the <index|Energy equivalent>energy equivalent in the
  temperature range <math|T<rsub|1>> to <math|T<rsub|2>> is

  <\equation>
    \<epsilon\>=<frac|<Del>U|T<rsub|2>-T<rsub|1>>=<frac|-\<epsilon\>*<around|(|k/\<epsilon\>|)>*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><around|(|T-T<rsub|\<infty\>>|)><dt>+w<el>|T<rsub|2>-T<rsub|1>>
  </equation>

  Solving for <math|\<epsilon\>>, we obtain

  <\equation>
    <label|e=w(el)/[(T2-T1+(k/e)int(T-T(inf))dt]>\<epsilon\>=<frac|w<el>|<around|(|T<rsub|2>-T<rsub|1>|)>+<around|(|k/\<epsilon\>|)>*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><around|(|T-T<rsub|\<infty\>>|)><dt>>
  </equation>

  The value of <math|w<el>> is known from <math|w<el>=I<rsup|2>*R<el><Del>t>,
  where <math|<Del>t> is the time interval during which the <index|Heater
  circuit><subindex|Circuit|heater>heater circuit is closed. The integral can
  be evaluated numerically once <math|T<rsub|\<infty\>>> is known.

  For heating at constant <em|pressure>, <math|<dif>H> is equal to
  <math|<dif>U+p<dif>V>, and we can write

  <equation-cov2|<frac|<dif>H|<dt>>=<frac|<dif>U|<dt>>+p*<frac|<dif>V|<dt>>=-k*<around|(|T-T<rsub|<text|ext>>|)>+<frac|<dw><el>|<dt>>+<frac|<dw><rsub|<text|cont>>|<dt>>|(constant
  <math|p>)>

  which is analogous to Eq. <reference|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt>.
  By the procedure described above for the case of constant <math|V>, we
  obtain

  <equation-cov2|<label|delH=-k int((T-T(inf))dt+w(el)><Del>H=-k*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><around|(|T-T<rsub|\<infty\>>|)><dt>+w<el>|(constant
  <math|p>)>

  At constant <math|p>, the <index|Energy equivalent>energy equivalent is
  equal to <math|C<rsub|p>=<Del>H/<around|(|T<rsub|2>-T<rsub|1>|)>>, and the
  final expression for <math|\<epsilon\>> is the same as that given by Eq.
  <reference|e=w(el)/[(T2-T1+(k/e)int(T-T(inf))dt]>.

  To obtain values of <math|k/\<epsilon\>> and <math|T<rsub|\<infty\>>> for
  use in Eq. <reference|e=w(el)/[(T2-T1+(k/e)int(T-T(inf))dt]>, we need the
  slopes of the <subindex|Heating|curve, of a calorimeter>heating curve in
  time intervals (rating periods) just before <math|t<rsub|1>> and just after
  <math|t<rsub|2>>. Consider the case of constant <em|volume>. In these
  intervals, <math|<dw><el>/<dt>> is zero and <math|<dif>U/<dt>> equals
  <math|-k*<around|(|T-T<rsub|\<infty\>>|)>> (from Eq.
  <reference|dU/dt=-k(T-T(inf)+dw(el)/dt>). The heat capacity at constant
  volume is <math|C<rsub|V>=<dif>U/<dif>T>. The slope <math|r> in general is
  then given by

  <\equation>
    r=<frac|<dif>T|<dt>>=<frac|<dif>T|<dif>U>*<frac|<dif>U|<dt>>=-<around|(|k/\<epsilon\>|)>*<around|(|T-T<rsub|\<infty\>>|)>
  </equation>

  Applying this relation to the points at times <math|t<rsub|1>> and
  <math|t<rsub|2>>, we have the following simultaneous equations in the
  unknowns <math|k/\<epsilon\>> and <math|T<rsub|\<infty\>>>:

  <\equation>
    r<rsub|1>=-<around|(|k/\<epsilon\>|)>*<around|(|T<rsub|1>-T<rsub|\<infty\>>|)>*<space|2em>r<rsub|2>=-<around|(|k/\<epsilon\>|)>*<around|(|T<rsub|2>-T<rsub|\<infty\>>|)>
  </equation>

  The solutions are

  <\equation>
    <around|(|k/\<epsilon\>|)>=<frac|r<rsub|1>-r<rsub|2>|T<rsub|2>-T<rsub|1>>*<space|2em>T<rsub|\<infty\>>=<frac|r<rsub|1>*T<rsub|2>-r<rsub|2>*T<rsub|1>|r<rsub|1>-r<rsub|2>>
  </equation>

  Finally, <math|k> is given by

  <\equation>
    <label|k=[(r1-r2)/(T2-T1)]e>k=<around|(|k/\<epsilon\>|)>*\<epsilon\>=<around*|(|<frac|r<rsub|1>-r<rsub|2>|T<rsub|2>-T<rsub|1>>|)>*\<epsilon\>
  </equation>

  When the <em|pressure> is constant, this procedure yields the same
  relations for <math|k/\<epsilon\>>, <math|T<rsub|\<infty\>>>, and
  <math|k>.<index-complex|<tuple|calorimeter|isothermal-jacket>||c7 sec
  tp-heatcap-ijc idx1|<tuple|Calorimeter|isothermal\Ujacket>>

  <subsubsection|Continuous\Uflow calorimeters><label|c7 sec tp-heatcap-cfc>

  A flow calorimeter <index-complex|<tuple|calorimeter|continuous-flow>||c7
  sec tp-heatcap-cfc idx1|<tuple|Calorimeter|continuous\Uflow>>is a third
  type of calorimeter used to measure the heat capacity of a fluid phase. The
  gas or liquid flows through a tube at a known constant rate past an
  <subindex|Electrical|heating><subindex|Heating|electrical>electrical heater
  of known constant power input. After a steady state has been achieved in
  the tube, the temperature increase <math|<Del>T> at the heater is measured.

  If <math|<dw><el>/<dt>> is the rate at which
  <subindex|Electrical|work><subindex|Work|electrical>electrical work is
  performed (the <subindex|Electric|power>electric power) and
  <math|<dif>m/<dt>> is the mass flow rate, then in time interval
  <math|<Del>t> a quantity <math|w=<around|(|<dw><el>/<dt>|)><Del>t> of work
  is performed on an amount <math|n=<around|(|<dif>m/<dt>|)><Del>t/M> of the
  fluid (where <math|M> is the molar mass). If heat flow is negligible, the
  molar heat capacity of the substance is given by

  <\equation>
    <Cpm>=<frac|w|n<Del>T>=<frac|M*<around|(|<dw><el>/<dt>|)>|<Del>T*<around|(|<dif>m/<dt>|)>>
  </equation>

  To correct for the effects of heat flow, <math|<Del>T> is usually measured
  over a range of flow rates and the results extrapolated to infinite flow
  rate.<index-complex|<tuple|calorimeter|continuous-flow>||c7 sec
  tp-heatcap-cfc idx1|<tuple|Calorimeter|continuous\Uflow>>

  <subsection|Typical values><label|c7 sec tp-typical>

  Figure <vpageref|fig:7-Cp vs T> shows the temperature dependence of
  <math|<Cpm>> for several substances. The discontinuities seen at certain
  temperatures occur at equilibrium phase transitions. At these temperatures
  the heat capacity is in effect infinite, since the phase transition of a
  pure substance involves finite heat with zero temperature
  change.<\float|float|thb>
    <\framed>
      <\big-figure>
        <image|07-SUP/CP-T.eps|211pt|198pt||>
      <|big-figure>
        <label|fig:7-Cp vs T>Temperature dependence of molar heat capacity at
        constant pressure (<math|p=1<br>>) of H<rsub|<math|2>>O,
        N<rsub|<math|2>>, and C(graphite).
      </big-figure>
    </framed>
  </float>

  <section|Heating at Constant Volume or Pressure><label|7-heating at const V
  or p><label|c7 sec hcvp>

  <index-complex|<tuple|heating|constant volume>||c7 sec hcvp
  idx1|<tuple|Heating|at constant volume or pressure>>Consider the process of
  changing the temperature of a phase at constant volume.<footnote|Keeping
  the volume exactly constant while increasing the temperature is not as
  simple as it may sound. Most solids expand when heated, unless we arrange
  to increase the external pressure at the same time. If we use solid walls
  to contain a fluid phase, the container volume will change with
  temperature. For practical purposes, these volume changes are usually
  negligible.> The rate of change of internal energy with <math|T> under
  these conditions is the heat capacity at constant volume:
  <math|C<rsub|V>=<pd|U|T|V>> (Eq. <reference|dU=dq,C_V=dU/dT>). Accordingly,
  an infinitesimal change of <math|U> is given by

  <\equation-cov2|<label|dU=CVdT><dif>U=C<rsub|V>*<dif>T>
    (closed system,

    <math|C=1>, <math|P=1>, constant <math|V>)
  </equation-cov2>

  and the finite change of <math|U> between temperatures <math|T<rsub|1>> and
  <math|T<rsub|2>> is

  <\equation-cov2|<label|delU=int(CV)dT><Del>U=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em>C<rsub|V>*<dif>T>
    (closed system,

    <math|C=1>, <math|P=1>, constant <math|V>)
  </equation-cov2>

  Three comments, relevant to these and other equations in this chapter, are
  in order:

  <\enumerate>
    <item>Equation <reference|delU=int(CV)dT> allows us to calculate the
    finite change of a state function, <math|U>, by integrating
    <math|C<rsub|V>> over <math|T>. The equation was derived under the
    condition that <math|V> is constant during the process, and the use of
    the integration variable <math|T> implies that the system has a single,
    uniform temperature at each instant during the process. The integrand
    <math|C<rsub|V>> may depend on both <math|V> and <math|T>, and we should
    integrate with <math|V> held constant and <math|C<rsub|V>> treated as a
    function only of <math|T>.

    <item>Suppose we want to evaluate <math|<Del>U> for a process in which
    the volume is the same in the initial and final states
    (<math|V<rsub|2>=V<rsub|1>>) but is different in some intermediate
    states, and the temperature is <em|not> uniform in some of the
    intermediate states. We know the change of a state function depends only
    on the initial and final states, so we can still use Eq.
    <reference|delU=int(CV)dT> to evaluate <math|<Del>U> for this process. We
    integrate with <math|V> held constant, although <math|V> was not constant
    during the actual process.

    In general: A finite change <math|<Del>X> of a state function, evaluated
    under the condition that another state function <math|Y> is constant, is
    the same as <math|<Del>X> under the less stringent condition
    <math|Y<rsub|2>=Y<rsub|1>>. (Another application of this principle was
    mentioned in Sec. <reference|4-rev expansion>.)

    <item>For a pure substance, we may convert an expression for an
    infinitesimal or finite change of an extensive property to an expression
    for the change of the corresponding <subindex|Molar|quantity><subindex|Property|molar><em|molar>
    property by dividing by <math|n>. For instance, Eq. <reference|dU=CVdT>
    becomes

    <\equation>
      <dif>U<m>=<CVm><dif>T
    </equation>

    and Eq. <reference|delU=int(CV)dT> becomes

    <\equation>
      <Del>U<m>=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em><CVm><dif>T
    </equation>
  </enumerate>

  If, at a fixed volume and over the temperature range <math|T<rsub|1>> to
  <math|T<rsub|2>>, the value of <math|C<rsub|V>> is essentially constant
  (i.e., independent of <math|T>), Eq. <reference|delU=int(CV)dT> becomes

  <\equation-cov2|<label|delU=CV(T2-T1)><Del>U=C<rsub|V>*<around|(|T<rsub|2>-T<rsub|1>|)>>
    (closed system, <math|C=1>

    <math|P=1>, constant <math|V> and <math|C<rsub|V>>)
  </equation-cov2>

  <index-complex|<tuple|entropy|change|constant
  volume>|||<tuple|Entropy|change|at constant volume>>An infinitesimal
  entropy change during a reversible process in a closed system is given
  according to the second law by <math|<dif>S=<dq>/T>. At constant volume,
  <math|<dq>> is equal to <math|<dif>U> which in turn equals
  <math|C<rsub|V><dif>T>. Therefore, the entropy change is

  <\equation-cov2|<label|dS=(CV/T)dT><dif>S=<frac|C<rsub|V>|T>*<dif>T>
    (closed system,

    <math|C=1>, <math|P=1>, constant <math|V>)
  </equation-cov2>

  Integration yields the finite change

  <\equation-cov2|<label|delS=int(CV/T)dT><Del>S=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|C<rsub|V>|T><dif>T>
    (closed system

    <math|C=1>, <math|P=1>, constant <math|V>)
  </equation-cov2>

  If <math|C<rsub|V>> is treated as constant, Eq.
  <reference|delS=int(CV/T)dT> becomes

  <\equation-cov2|<label|delS=CV*ln(T2/T1)><Del>S=C<rsub|V>*ln
  <frac|T<rsub|2>|T<rsub|1>>>
    (closed system, <math|C=1>

    <math|P=1>, constant <math|V> and <math|C<rsub|V>>)
  </equation-cov2>

  (More general versions of the two preceding equations have already been
  given in Sec. <reference|4-rev heating \ expansion>.)

  Since <math|C<rsub|V>> is positive, we see from Eqs.
  <reference|delU=int(CV)dT> and <reference|delS=int(CV/T)dT> that heating a
  phase at constant volume causes both <math|U> and <math|S> to increase.

  We may derive relations for a temperature change at constant <em|pressure>
  by the same methods. From <math|C<rsub|p>=<pd|H|T|p>> (Eq.
  <reference|dH=dq,C_p=dH/dT>), we obtain <subindex|Enthalpy|change at
  constant pressure>

  <\equation-cov2|<label|delH=int(Cp)dT><Del>H=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>>C<rsub|p>*<dif>T>
    (closed system,

    <math|C=1>, <math|P=1>, constant <math|p>)
  </equation-cov2>

  If <math|C<rsub|p>> is treated as constant, Eq. <reference|delH=int(Cp)dT>
  becomes

  <\equation-cov2|<label|delH=(Cp)delT><Del>H=C<rsub|p>*<around|(|T<rsub|2>-T<rsub|1>|)>>
    (closed system, <math|C=1>

    <math|P=1>, constant <math|p> and <math|C<rsub|p>>)
  </equation-cov2>

  From <math|<dif>S=<dq>/T> and Eq. <reference|dH=dq,C_p=dH/dT> we obtain for
  the entropy change at constant pressure
  <index-complex|<tuple|entropy|change|constant
  pressure>|||<tuple|Entropy|change|at constant pressure>>

  <\equation-cov2|<label|dS=(Cp/T)dT><dif>S=<frac|C<rsub|p>|T>*<dif>T>
    (closed system,

    <math|C=1>, <math|P=1>, constant <math|p>)
  </equation-cov2>

  Integration gives

  <\equation-cov2|<label|delS=int(Cp/T)dT><Del>S=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|C<rsub|p>|T>*<dif>T>
    (closed system,

    <math|C=1>, <math|P=1>, constant <math|p>)
  </equation-cov2>

  or, with <math|C<rsub|p>> treated as constant,

  <\equation-cov2|<label|delS=Cp*ln(T2/T1)><Del>S=C<rsub|p>*ln
  <frac|T<rsub|2>|T<rsub|1>>>
    (closed system, <math|C=1>,

    <math|P=1>, constant <math|p> and <math|C<rsub|p>>)
  </equation-cov2>

  <math|C<rsub|p>> is positive, so heating a phase at constant pressure
  causes <math|H> and <math|S> to increase.

  The Gibbs energy changes according to <math|<pd|G|T|p>=-S> (Eq.
  <reference|dG/dT=-S>), so heating at constant pressure causes <math|G> to
  decrease.<index-complex|<tuple|heating|constant volume>||c7 sec hcvp
  idx1|<tuple|Heating|at constant volume or pressure>>

  <section|Partial Derivatives with Respect to <math|T>, <math|p>, and
  <math|V>><label|c7 sec pdtpv>

  <subsection|Tables of partial derivatives><label|7-partial
  derivatives><label|c7 sec pdtpv-tables>

  <index-complex|<tuple|partial derivative|expressions at constant T, p, and
  V>||c7 sec pdtpv-tables idx1|<tuple|Partial derivative|expressions at
  constant <math|T>, <math|p>, and <math|V>>>The tables in this section
  collect useful expressions for partial derivatives of the eight state
  functions <math|T>, <math|p>, <math|V>, <math|U>, <math|H>, <math|A>,
  <math|G>, and <math|S> in a closed, single-phase system. Each derivative is
  taken with respect to one of the three easily-controlled variables
  <math|T>, <math|p>, or <math|V> while another of these variables is held
  constant. We have already seen some of these expressions, and the
  derivations of the others are indicated below.

  We can use these partial derivatives (1) for writing an expression for the
  total differential of any of the eight quantities, and (2) for expressing
  the finite change in one of these quantities as an integral under
  conditions of constant <math|T>, <math|p>, or <math|V>. For instance, given
  the expressions

  <\equation>
    <Pd|S|T|<space|-0.17em>p>=<frac|C<rsub|p>|T><space|2em><text|and><space|2em><Pd|S|p|T>=-\<alpha\>*V
  </equation>

  we may write the total differential of <math|S>, taking <math|T> and
  <math|p> as the independent variables, as

  <\equation>
    <dif>S=<frac|C<rsub|p>|T>*<dif>T-\<alpha\>*V*<difp>
  </equation>

  Furthermore, the first expression is equivalent to the differential form

  <\equation>
    <dif>S=<frac|C<rsub|p>|T>*<dif>T
  </equation>

  provided <math|p> is constant; we can integrate this equation to obtain the
  finite change <math|<Del>S> under isobaric conditions as shown in Eq.
  <reference|delS=int(Cp/T)dT>.

  Both general expressions and expressions valid for an ideal gas are given
  in Tables <reference|tbl:7-const T>, <reference|tbl:7-const p>, and
  <reference|tbl:7-const V>.<float|float|thb|<\big-table>
    <bktable3|<tformat|<cwith|1|7|1|7|cell-rsep|1fn>|||||||||||<table|<row|<\cell>
      <no-indent*>Partial

      derivative
    </cell>|<\cell>
      General

      expression
    </cell>|<\cell>
      Ideal

      gas
    </cell>|<cell|<space|4em>>|<\cell>
      Partial

      derivative
    </cell>|<\cell>
      General

      expression
    </cell>|<\cell>
      Ideal

      gas
    </cell>>|<row|<cell|<math|<Pd|p|V|T>>>|<cell|<math|-<dfrac|1|<kT>**V>>>|<cell|<math|-<dfrac|p|V>>>|<cell|>|<cell|<math|<Pd|A|p|T>>>|<cell|<math|<kT>*p*V>>|<cell|<math|V>>>|<row|<cell|<math|<Pd|V|p|T>>>|<cell|<math|-<kT>*V>>|<cell|<math|-<dfrac|V|p>>>|<cell|>|<cell|<math|<Pd|A|V|T>>>|<cell|<math|-p>>|<cell|<math|-p>>>|<row|<cell|<math|<Pd|U|p|T>>>|<cell|<math|<around|(|-\<alpha\>*T+<kT>**p|)>*V>>|<cell|<math|0>>|<cell|>|<cell|<math|<Pd|G|p|T>>>|<cell|<math|V>>|<cell|<math|V>>>|<row|<cell|<math|<Pd|U|V|T>>>|<cell|<math|<dfrac|\<alpha\>*T|<kT>>-p>>|<cell|<math|0>>|<cell|>|<cell|<math|<Pd|G|V|T>>>|<cell|<math|-<dfrac|1|<kT>>>>|<cell|<math|-p>>>|<row|<cell|<math|<Pd|H|p|T>>>|<cell|<math|<around|(|1-\<alpha\>*T|)>*V>>|<cell|<math|0>>|<cell|>|<cell|<math|<Pd|S|p|T>>>|<cell|<math|-\<alpha\>*V>>|<cell|<math|-<dfrac|V|T>>>>|<row|<cell|<math|<Pd|H|V|T>>>|<cell|<math|<dfrac|\<alpha\>*T-1|<kT>>>>|<cell|<math|0>>|<cell|>|<cell|<math|<Pd|S|V|T>>>|<cell|<math|<dfrac|\<alpha\>|<kT>>>>|<cell|<math|<dfrac|p|T>>>>>>>

    \;
  <|big-table>
    <label|tbl:7-const T><em|Constant temperature>: expressions for partial
    derivatives of state functions with respect to pressure and volume in a
    closed, single-phase system
  </big-table>>

  \ <float|float|thb|<\big-table>
    <bktable3|<tformat|<cwith|1|7|1|7|cell-rsep|1fn>|||||||||||<table|<row|<\cell>
      <no-indent*>Partial

      derivative
    </cell>|<\cell>
      General

      expression
    </cell>|<\cell>
      Ideal

      gas
    </cell>|<cell|<space|4em>>|<\cell>
      Partial

      derivative
    </cell>|<\cell>
      General

      expression
    </cell>|<\cell>
      Ideal

      gas
    </cell>>|<row|<cell|<math|<Pd|T|V|p>>>|<cell|<math|<dfrac|1|\<alpha\>**V>>>|<cell|<math|<dfrac|T|V>>>|<cell|>|<cell|<math|<Pd|A|T|p>>>|<cell|<math|-\<alpha\>*p*V-S>>|<cell|<math|-<dfrac|p*V|T>-S>>>|<row|<cell|<math|<Pd|V|T|p>>>|<cell|<math|\<alpha\>*V>>|<cell|<math|<dfrac|V|T>>>|<cell|>|<cell|<math|<Pd|A|V|p>>>|<cell|<math|-p-<dfrac|S|\<alpha\>*V>>>|<cell|<math|-p-<dfrac|T*S|V>>>>|<row|<cell|<math|<Pd|U|T|p>>>|<cell|<math|C<rsub|p>-\<alpha\>*p*V>>|<cell|<math|C<rsub|V>>>|<cell|>|<cell|<math|<Pd|G|T|p>>>|<cell|<math|-S>>|<cell|<math|-S>>>|<row|<cell|<math|<Pd|U|V|p>>>|<cell|<math|<dfrac|C<rsub|p>|\<alpha\>*V>-p>>|<cell|<math|<dfrac|C<rsub|V>*T|V>>>|<cell|>|<cell|<math|<Pd|G|V|p>>>|<cell|<math|-<dfrac|S|\<alpha\>*V>>>|<cell|<math|-<dfrac|T*S|V>>>>|<row|<cell|<math|<Pd|H|T|p>>>|<cell|<math|C<rsub|p>>>|<cell|<math|C<rsub|p>>>|<cell|>|<cell|<math|<Pd|S|T|p>>>|<cell|<math|<dfrac|C<rsub|p>|T>>>|<cell|<math|<dfrac|C<rsub|p>|T>>>>|<row|<cell|<math|<Pd|H|V|p>>>|<cell|<math|<dfrac|C<rsub|p>|\<alpha\>*V>>>|<cell|<math|<dfrac|C<rsub|p>*T|V>>>|<cell|>|<cell|<math|<Pd|S|V|p>>>|<cell|<math|<dfrac|C<rsub|p>|\<alpha\>*T*V>>>|<cell|<math|<dfrac|C<rsub|p>|V>>>>>>>

    \;
  <|big-table>
    <label|tbl:7-const p><em|Constant pressure>: expressions for partial
    derivatives of state functions with respect to temperature and volume in
    a closed, single-phase system
  </big-table>>

  \ <float|float|thb|<\big-table>
    <bktable3|<tformat|<cwith|1|7|1|7|cell-rsep|1fn>|||||||||||<table|<row|<\cell>
      <no-indent*>Partial

      derivative
    </cell>|<\cell>
      General

      expression
    </cell>|<\cell>
      Ideal

      gas
    </cell>|<cell|<space|4em>>|<\cell>
      Partial

      derivative
    </cell>|<\cell>
      General

      expression
    </cell>|<\cell>
      Ideal

      gas
    </cell>>|<row|<cell|<math|<Pd|T|p|V>>>|<cell|<math|-<dfrac|<kT>|\<alpha\>>>>|<cell|<math|<dfrac|T|p>>>|<cell|>|<cell|<math|<Pd|A|T|V>>>|<cell|<math|-S>>|<cell|<math|-S>>>|<row|<cell|<math|<Pd|p|T|V>>>|<cell|<math|<dfrac|\<alpha\>|<kT>>>>|<cell|<math|<dfrac|p|T>>>|<cell|>|<cell|<math|<Pd|A|p|V>>>|<cell|<math|-<dfrac|<kT>*S|\<alpha\>>>>|<cell|<math|-<dfrac|T*S|p>>>>|<row|<cell|<math|<Pd|U|T|V>>>|<cell|<math|C<rsub|V>>>|<cell|<math|C<rsub|V>>>|<cell|>|<cell|<math|<Pd|G|T|V>>>|<cell|<math|<dfrac|\<alpha\>*V|<kT>>-S>>|<cell|<math|<dfrac|p*V|T>-S>>>|<row|<cell|<math|<Pd|U|p|V>>>|<cell|<math|<dfrac|<kT>*C<rsub|p>|\<alpha\>>-\<alpha\>*T*V>>|<cell|<math|<dfrac|T*C<rsub|V>|p>>>|<cell|>|<cell|<math|<Pd|G|p|V>>>|<cell|<math|V-<dfrac|<kT>*S|\<alpha\>>>>|<cell|<math|V-<dfrac|T*S|p>>>>|<row|<cell|<math|<Pd|H|T|V>>>|<cell|<math|C<rsub|p>+<dfrac|\<alpha\>*V|<kT>>*<around*|(|1-\<alpha\>*T|)>>>|<cell|<math|C<rsub|p>>>|<cell|>|<cell|<math|<Pd|S|T|V>>>|<cell|<math|<dfrac|C<rsub|V>|T>>>|<cell|<math|<dfrac|C<rsub|V>|T>>>>|<row|<cell|<math|<Pd|H|p|V>>>|<cell|<math|<dfrac|<kT>*C<rsub|p>|\<alpha\>>+V*<around*|(|1-\<alpha\>*T|)>>>|<cell|<math|<dfrac|C<rsub|p>*T|p>>>|<cell|>|<cell|<math|<Pd|S|p|V>>>|<cell|<math|<dfrac|<kT>*C<rsub|p>|\<alpha\>*T>-\<alpha\>*V>>|<cell|<math|<dfrac|C<rsub|V>|p>>>>>>>

    \;
  <|big-table>
    <label|tbl:7-const V><em|Constant volume>: expressions for partial
    derivatives of state functions with respect to temperature and pressure
    in a closed, single-phase system
  </big-table>>

  <\quote-env>
    \ We may derive the general expressions as follows. We are considering
    differentiation with respect only to <math|T>, <math|p>, and <math|V>.
    Expressions for <math|<pd|V|T|p>>, <math|<pd|V|p|T>>, and
    <math|<pd|p|T|V>> come from Eqs. <reference|alpha def>, <reference|kappaT
    def>, and <reference|dp/dT=alpha/kappaT> and are shown as functions of
    <math|\<alpha\>> and <math|<kT>>. The reciprocal of each of these three
    expressions provides the expression for another partial derivative from
    the general relation

    <\equation>
      <pd|y|x|z>=<frac|1|<pd|x|y|z>>
    </equation>

    This procedure gives us expressions for the six partial derivatives of
    <math|T>, <math|p>, and <math|V>.

    The remaining expressions are for partial derivatives of <math|U>,
    <math|H>, <math|A>, <math|G>, and <math|S>. We obtain the expression for
    <math|<pd|U|T|V>> from Eq. <reference|dU=dq,C_V=dU/dT>, for
    <math|<pd|U|V|T>> from Eq. <reference|dU/dV=alpha*T/kappaT-p>, for
    <math|<pd|H|T|p>> from Eq. <reference|dH=dq,C_p=dH/dT>, for
    <math|<pd|A|T|V>> from Eq. <reference|dA/dT=-S>, for <math|<pd|A|V|T>>
    from Eq. <reference|dA/dV=-p>, for <math|<pd|G|p|T>> from Eq.
    <reference|dG/dp=V>, for <math|<pd|G|T|p>> from Eq. <reference|dG/dT=-S>,
    for <math|<pd|S|T|V>> from Eq. <reference|dS=(CV/T)dT>, for
    <math|<pd|S|T|p>> from Eq. <reference|dS=(Cp/T)dT>, and for
    <math|<pd|S|p|T>> from Eq. <reference|-dS/dp=dV/dT>.

    We can transform each of these partial derivatives, and others derived in
    later steps, to two other partial derivatives with the same variable held
    constant and the variable of differentiation changed. The transformation
    involves multiplying by an appropriate partial derivative of <math|T>,
    <math|p>, or <math|V>. For instance, from the partial derivative
    <math|<pd|U|V|T>=<around|(|\<alpha\>*T/<kT>|)>-p>, we obtain

    <\equation>
      <Pd|U|p|T>=<Pd|U|V|T><Pd|V|p|T>=<around*|(|<frac|\<alpha\>*T|<kT>>-p|)>*<around*|(|-<kT>V|)>=<around*|(|-\<alpha\>*T+<kT>p|)>*V
    </equation>

    The remaining partial derivatives can be found by differentiating
    <math|U=H-p*V>, <math|H=U+p*V>, <math|A=U-T*S>, and <math|G=H-T*S> and
    making appropriate substitutions. Whenever a partial derivative appears
    in a derived expression, it is replaced with an expression derived in an
    earlier step. The expressions derived by these steps constitute the full
    set shown in Tables <reference|tbl:7-const T>, <reference|tbl:7-const p>,
    and <reference|tbl:7-const V>.

    <index|Bridgman, Percy>Bridgman<footnote|Ref. <cite|bridgman-14>; Ref.
    <cite|bridgman-61>, p. 199\U241.> devised a simple method to obtain
    expressions for these and many other partial derivatives from a
    relatively small set of formulas.
  </quote-env>

  <index-complex|<tuple|partial derivative|expressions at constant T, p, and
  V>||c7 sec pdtpv-tables idx1|<tuple|Partial derivative|expressions at
  constant <math|T>, <math|p>, and <math|V>>>

  <subsection|The Joule\UThomson coefficient><label|7-JK coeff><label|c7 sec
  pdtpv-jt>

  <index-complex|<tuple|joule-thomson|coefficient>||c7 sec pdtpv-jt
  idx1|<tuple|Joule\UThomson|coefficient>>The Joule\UThomson coefficient of a
  gas was defined in Eq. <vpageref|mu(JT) def> by
  <math|\<mu\><rsub|<text|JT>>=<pd|T|p|H>>. It can be evaluated with
  measurements of <math|T> and <math|p> during adiabatic throttling processes
  as described in Sec. <reference|6-J-T expansion>.

  To relate <math|\<mu\><rsub|<text|JT>>> to other properties of the gas, we
  write the total differential of the enthalpy of a closed, single-phase
  system in the form

  <\equation>
    <dif>H=<Pd|H|T|<space|-0.17em>p><dif>T+<Pd|H|p|T><difp>
  </equation>

  and divide both sides by <math|<difp>>:

  <\equation>
    <frac|<dif>H|<difp>>=<Pd|H|T|<space|-0.17em>p><frac|<dif>T|<difp>>+<Pd|H|p|T>
  </equation>

  Next we impose a condition of constant <math|H>; the ratio
  <math|<dif>T/<difp>> becomes a partial derivative:

  <\equation>
    0=<Pd|H|T|<space|-0.17em>p><Pd|T|p|H>+<Pd|H|p|T>
  </equation>

  Rearrangement gives

  <\equation>
    <Pd|T|p|H>=-<frac|<pd|H|p|T>|<pd|H|T|p>>
  </equation>

  The left side of this equation is the Joule\UThomson coefficient. An
  expression for the partial derivative <math|<pd|H|p|T>> is given in Table
  <reference|tbl:7-const T>, and the partial derivative <math|<pd|H|T|p>> is
  the heat capacity at constant pressure (Eq. <reference|Cp=dH/dT>). These
  substitutions give us the desired relation

  <\equation>
    \<mu\><rsub|<text|JT>>=<frac|<around|(|\<alpha\>*T-1|)>*V|C<rsub|p>>=<frac|<around|(|\<alpha\>*T-1|)>*V<m>|<Cpm>>
  </equation>

  <index-complex|<tuple|joule-thomson|coefficient>||c7 sec pdtpv-jt
  idx1|<tuple|Joule\UThomson|coefficient>>

  <section|Isothermal Pressure Changes><label|7-isothermal p
  changes><label|c7 sec ipc>

  <index-complex|<tuple|isothermal|pressure changes>||c7 sec ipc
  idx1|<tuple|Isothermal|pressure changes>><index-complex|<tuple|pressure|changes>||idx2|<tuple|Pressure|changes,
  isothermal>>In various applications, we will need expressions for the
  effect of changing the pressure at constant temperature on the internal
  energy, enthalpy, entropy, and Gibbs energy of a phase. We obtain the
  expressions by integrating expressions found in Table
  <reference|tbl:7-const T>. For example, <math|<Del>U> is given by
  <math|<big|int><pd|U|p|T><difp>>. The results are listed in the second
  column of Table <vpageref|tbl:7-isothermal p
  change>.<float|float|thb|<\big-table>
    <bktable3|<tformat|<cwith|1|1|1|4|cell-hyphen|t>|<cwith|1|6|1|3|cell-rsep|1fn>||<cwith|1|1|1|1|cell-hyphen|n>||||||||||<table|<row|<\cell>
      <no-indent*>State function

      change
    </cell>|<\cell>
      <no-indent*>General expression
    </cell>|<\cell>
      <no-indent*>Ideal gas
    </cell>|<\cell>
      <no-indent*>Approximate expression

      for liquid or solid
    </cell>>|<row|<cell|<math|\<Delta\>*U>>|<cell|<math|<with|math-display|true|<big|int><rsub|p<rsub|1>><rsup|p<rsub|2>><around*|(|-\<alpha\>*T+<kT>*p|)>*V*<dvar|p>>>>|<cell|<math|<with|math-display|true|0>>>|<cell|<math|<with|math-display|true|-\<alpha\>*T*V*\<Delta\>*p>>>>|<row|<cell|<math|\<Delta\>*H>>|<cell|<math|<with|math-display|true|<big|int><rsub|p<rsub|1>><rsup|p<rsub|2>><around*|(|1-\<alpha\>*T|)>*V*<dvar|p>>>>|<cell|<math|<with|math-display|true|0>>>|<cell|<math|<with|math-display|true|<around*|(|1-\<alpha\>*T|)>*V*\<Delta\>*p>>>>|<row|<cell|<math|\<Delta\>*A>>|<cell|<math|<with|math-display|true|<big|int><rsub|p<rsub|1>><rsup|p<rsub|2>><kT>**p*V*<dvar|p>>>>|<cell|<math|<with|math-display|true|n*R*T*<text|ln>
    <dfrac|p<rsub|2>|p<rsub|1>>>>>|<cell|<math|<with|math-display|true|<kT>*V*<around*|(|p<rsub|2><rsup|2>-p<rsub|1><rsup|2>|)>/2>>>>|<row|<cell|<math|\<Delta\>*G>>|<cell|<math|<with|math-display|true|<big|int><rsub|p<rsub|1>><rsup|p<rsub|2>>V*<dvar|p>>>>|<cell|<math|<with|math-display|true|n*R*T*<text|ln>
    <dfrac|p<rsub|2>|p<rsub|1>>>>>|<cell|<math|<with|math-display|true|V*\<Delta\>*p>>>>|<row|<cell|<math|\<Delta\>*S>>|<cell|<math|<with|math-display|true|-<big|int><rsub|p<rsub|1>><rsup|p<rsub|2>>\<alpha\>*V*<dvar|p>>>>|<cell|<math|<with|math-display|true|-n*R*<text|ln>
    <dfrac|p<rsub|2>|p<rsub|1>>>>>|<cell|<math|<with|math-display|true|-\<alpha\>*V*\<Delta\>*p>>>>>>>

    \;
  <|big-table>
    <label|tbl:7-isothermal p change>Changes of state functions during an
    isothermal pressure change in a closed, single-phase system
  </big-table>>

  <subsection|Ideal gases><label|c7 sec ipc-ideal-gas>

  <index-complex|<tuple|isothermal|pressure changes|ideal
  gas>|||<tuple|Isothermal|pressure changes|of an ideal
  gas>><index-complex|<tuple|pressure|changes, isothermal|ideal
  gas>|||<tuple|Pressure|changes, isothermal|of an ideal gas>>Simplifications
  result when the phase is an ideal gas. In this case, we can make the
  substitutions <math|V=n*R*T/p>, <math|\<alpha\>=1/T>, and <math|<kT>=1/p>,
  resulting in the expressions in the third column of Table
  <reference|tbl:7-isothermal p change>.

  The expressions in the third column of Table <reference|tbl:7-isothermal p
  change> may be summarized by the statement that, when an ideal gas expands
  isothermally, the internal energy and enthalpy stay constant, the entropy
  increases, and the Helmholtz energy and Gibbs energy decrease.

  <subsection|Condensed phases><label|c7 sec ipc-condensed-phase>

  <index-complex|<tuple|isothermal|pressure changes|condensed
  phase>|||<tuple|Isothermal|pressure changes|of<space|1em>a condensed
  phase>><index-complex|<tuple|pressure|changes, isothermal|condensed
  phase>|||<tuple|Pressure|changes, isothermal|of a condensed phase>>Solids,
  and liquids under conditions of temperature and pressure not close to the
  critical point, are much less compressible than gases. Typically the
  <index-complex|<tuple|isothermal|compressibility|liquid or
  solid>|||<tuple|Isothermal|compressibility|of a liquid or solid>>isothermal
  compressibility, <math|<kT>>, of a liquid or solid at room temperature and
  atmospheric pressure is no greater than <math|1<timesten|-4>
  <text|bar><rsup|-1>> (see Fig. <vpageref|fig:7-kappaT vs T>), whereas an
  ideal gas under these conditions has <math|<kT>=1/p=1 <text|bar><rsup|-1>>.
  Consequently, it is frequently valid to treat <math|V> for a liquid or
  solid as essentially constant during a pressure change at constant
  temperature. Because <math|<kT>> is small, the product
  <math|<kT><space|-0.17em>p> for a liquid or solid is usually much smaller
  than the product <math|\<alpha\>*T>. Furthermore, <math|<kT>> for liquids
  and solids does not change rapidly with <math|p> as it does for gases, and
  neither does <math|\<alpha\>>.

  With the approximations that <math|V>, <math|\<alpha\>>, and <math|<kT>>
  are constant during an isothermal pressure change, and that
  <math|<kT><space|-0.17em>p> is negligible compared with <math|\<alpha\>*T>,
  we obtain the expressions in the last column of Table
  <reference|tbl:7-isothermal p change>.<index-complex|<tuple|isothermal|pressure
  changes>||c7 sec ipc idx1|<tuple|Isothermal|pressure
  changes>><index-complex|<tuple|pressure|changes>||idx2|<tuple|Pressure|changes,
  isothermal>>

  <section|Standard States of Pure Substances><label|7-st states of pure
  substances><label|c7 sec ssps>

  It is often useful to refer to a reference pressure, the
  <subindex|Standard|pressure><subindex|Pressure|standard><newterm|standard
  pressure>, denoted <math|p<st>>. The standard pressure has an arbitrary but
  constant value in any given application. Until 1982, chemists used a
  standard pressure of <math|1 <text|atm>> (<math|1.01325<timesten|5>
  <text|Pa>>). The IUPAC now recommends the value <math|p<st>=1 <text|bar>>
  (exactly <math|10<rsup|5> <text|Pa>>).<\footnote>
    See Ref. <cite|iupac-2014-pressure>.
  </footnote> This book uses the latter value unless stated otherwise. (Note
  that there is no defined standard <em|temperature>.)

  A superscript degree symbol (<with|font-size|0.71|mode|math|\<circ\>>)
  denotes a standard quantity or standard-state conditions. An alternative
  symbol for this purpose, used extensively outside the U.S., is a
  superscript <index|Plimsoll mark>Plimsoll mark
  (\<minuso\>).<with|font-size|1|<footnote|The Plimsoll mark is named after
  the British merchant Samuel Plimsoll, at whose instigation Parliament
  passed an act in 1875 requiring the symbol to be placed on the hulls of
  cargo ships to indicate the maximum depth for safe loading. The unicode
  glyph <verbatim|U+29B5, CIRCLE WITH HORIZONTAL BAR> (\<minuso\>)
  approximates the appearance of this mark.>>

  A <index-complex|<tuple|standard state|pure substance>|||<tuple|Standard
  state|of a pure substance>><index-complex|<tuple|state|standard>|||<tuple|State|standard,
  see Standard state>><newterm|standard state> of a pure substance is a
  particular reference state appropriate for the kind of phase and is
  described by intensive variables. This book follows the recommendations of
  the <index|IUPAC Green Book>IUPAC Green Book<footnote|Ref.
  <cite|greenbook-3>, p. 61\U62.> for various standard states.

  <\itemize>
    <item>The <index-complex|<tuple|standard state|gas>|||<tuple|Standard
    state|of a gas>>standard state of a <em|pure gas> is the hypothetical
    state in which the gas is at pressure <math|p<st>> and the temperature of
    interest, and the gas behaves as an ideal gas. The molar volume of a gas
    at <math|1 <text|bar>> may have a measurable deviation from the molar
    volume predicted by the ideal gas equation due to intermolecular forces.
    We must imagine the standard state in this case to consist of the gas
    with the intermolecular forces magically \Pturned off\Q and the molar
    volume adjusted to the ideal-gas value <math|R*T/p<st>>.

    <item>The standard state of a <index-complex|<tuple|standard state|pure
    liquid or solid>|||<tuple|Standard state|of a pure liquid o
    rsolid>><em|pure liquid or solid> is the unstressed liquid or solid at
    pressure <math|p<st>> and the temperature of interest. If the liquid or
    solid is stable under these conditions, this is a real (not hypothetical)
    state.
  </itemize>

  Section <reference|9-activities> will introduce additional standard states
  for constituents of mixtures.

  <section|Chemical Potential and Fugacity><label|7-chem pot><label|c7 sec
  cpf>

  The <index-complex|<tuple|chemical potential|pure
  substance>|||<tuple|Chemical potential|of a pure
  substance>><newterm|chemical potential>, <math|\<mu\>>, of a pure substance
  has as one of its definitions (page <pageref|mu = Gm>)

  <equation-cov2|<label|mu=Gm>\<mu\><defn>G<m>=<frac|G|n>|(pure substance)>

  That is, <math|\<mu\>> is equal to the <subindex|Gibbs energy|molar>molar
  Gibbs energy of the substance at a given temperature and pressure. (Section
  <reference|9-chem pot of species in a mixt> will introduce a more general
  definition of chemical potential that applies also to a constituent of a
  mixture.) The chemical potential is an intensive state function.

  The <index-complex|<tuple|total differential|gibbs energy of a pure
  substance>|||<tuple|Total differential|of the Gibbs energy of a pure
  bustance>>total differential of the Gibbs energy of a fixed amount of a
  pure substance in a single phase, with <math|T> and <math|p> as independent
  variables, is <math|<dif>G=-S*<dif>T+V*<difp>> (Eq.
  <reference|dG=-SdT+Vdp>). Dividing both sides of this equation by <math|n>
  gives the total differential of the chemical potential with these same
  independent variables:

  <equation-cov2|<label|dmu=-(Sm)dT+(Vm)dp><dif>\<mu\>=-S<m>*<dif>T+V<m>*<difp>|(pure
  substance, <math|P=1>)>

  (Since all quantities in this equation are intensive, it is not necessary
  to specify a closed system; the amount of the substance in the system is
  irrelevant.)

  We identify the coefficients of the terms on the right side of Eq.
  <reference|dmu=-(Sm)dT+(Vm)dp> as the partial derivatives

  <equation-cov2|<label|dmu/dT=-Sm><Pd|\<mu\>|T|<space|-0.17em>p>=-S<m>|(pure
  substance, <math|P=1>)>

  and

  <equation-cov2|<label|dmu/dp=Vm><Pd|\<mu\>|p|T>=V<m>|(pure substance,
  <math|P=1>)>

  Since <math|V<m>> is positive, Eq. <reference|dmu/dp=Vm> shows that the
  chemical potential increases with increasing pressure in an isothermal
  process.

  The <index-complex|<tuple|chemical potential|standard|pure
  substance>|||<tuple|Chemical potential|standard|of a pure
  substance>><index-complex|<tuple|standard|chemical
  potential>|||<tuple|Standard|chemical potential, see Chemical potential,
  standard>><newterm|standard chemical potential>, <math|\<mu\><st>>, of a
  pure substance in a given phase and at a given temperature is the chemical
  potential of the substance when it is in the standard state of the phase at
  this temperature and the standard pressure <math|p<st>>.

  There is no way we can evaluate the absolute value of <math|\<mu\>> at a
  given temperature and pressure, or of <math|\<mu\><st>> at the same
  temperature,<footnote|At least not to any useful degree of precision. The
  values of <math|\<mu\>> and <math|\<mu\><st>> include the molar internal
  energy whose absolute value can only be calculated from the <index|Einstein
  energy relation>Einstein relation; see Sec. <reference|2-internal energy>.>
  but we can measure or calculate the <em|difference>
  <math|\<mu\>-\<mu\><st>>. The general procedure is to integrate
  <math|<dif>\<mu\>=V<m><difp>> (Eq. <reference|dmu=-(Sm)dT+(Vm)dp> with
  <math|<dif>T> set equal to zero) from the standard state at pressure
  <math|p<st>> to the experimental state at pressure <math|p<rprime|'>>:

  <equation-cov2|<label|mu-mu^o=int(Vm dp)>\<mu\><around|(|p<rprime|'>|)>-\<mu\><st>=<big|int><rsub|p<st>><rsup|p<rprime|'>>V<m>*<difp>|(constant
  <math|T>)>

  <subsection|Gases><label|7-chem pot and fugacity - gases><label|c7 sec
  cpf-gases>

  For the <index-complex|<tuple|chemical potential|standard|gas>|||<tuple|Chemical
  potential|standard|of a gas>>standard chemical potential of a gas, this
  book will usually use the notation <math|\<mu\><st><gas>> to emphasize the
  choice of a <em|gas> standard state.

  An <em|ideal gas> is in its standard state at a given temperature when its
  pressure is the standard pressure. We find the relation of the chemical
  potential of an ideal gas to its pressure and its standard chemical
  potential at the same temperature by setting <math|V<m>> equal to
  <math|R*T/p> in Eq. <reference|mu-mu^o=int(Vm dp)>:
  <math|\<mu\><around|(|p<rprime|'>|)>-\<mu\><st>=<big|int><rsub|p<st>><rsup|p<rprime|'>><around|(|R*T/p|)>*<difp>=R*T*ln
  <around|(|p<rprime|'>/p<st>|)>>. The general relation for <math|\<mu\>> as
  a function of <math|p>, then, is

  <equation-cov2|<label|mu=muo(g)+RT*ln(p/po)>\<mu\>=\<mu\><st><gas>+R*T*ln
  <frac|p|p<st>>|(pure ideal gas, constant <math|T>)>

  This function is shown as the dashed curve in Fig. <vpageref|fig:7-mu vs
  p>.

  <\framed>
    <\big-figure>
      <image|07-SUP/MU-P.eps|183pt|180pt||>
    <|big-figure>
      <label|fig:7-mu vs p>Chemical potential as a function of pressure at
      constant temperature, for a real gas (solid curve) and the same gas
      behaving ideally (dashed curve). Point A is the gas standard state.
      Point B is a state of the real gas at pressure <math|p<rprime|'>>. The
      fugacity <math|<fug><around|(|p<rprime|'>|)>> of the real gas at
      pressure <math|p<rprime|'>> is equal to the pressure of the ideal gas
      having the same chemical potential as the real gas (point C).
    </big-figure>
  </framed>

  If a gas is <em|not> an ideal gas, its standard state is a hypothetical
  state. The <index-complex|<tuple|fugacity|gas>|||<tuple|Fugacity|of a
  gas>><newterm|fugacity>, <math|<fug>>, of a real gas (a gas that is not
  necessarily an ideal gas) is defined by an equation with the same form as
  Eq. <reference|mu=muo(g)+RT*ln(p/po)>:

  <equation-cov2|<label|mu=muo(g)+RT*ln(f/po)>\<mu\>=\<mu\><st><gas>+R*T*ln
  <frac|<fug>|p<st>>|(pure gas)>

  or

  <equation-cov2|<fug><defn>p<st>exp <around*|[|<frac|\<mu\>-\<mu\><st><gas>|R*T>|]>|(pure
  gas)>

  Note that fugacity has the dimensions of pressure. Fugacity is a kind of
  effective pressure. Specifically, it is the pressure that the hypothetical
  ideal gas (the gas with intermolecular forces \Pturned off\Q) would need to
  have in order for its chemical potential at the given temperature to be the
  same as the chemical potential of the real gas (see point C in Fig.
  <reference|fig:7-mu vs p>). If the gas is an ideal gas, its fugacity is
  equal to its pressure.

  To evaluate the fugacity of a real gas at a given <math|T> and <math|p>, we
  must relate the chemical potential to the pressure\Uvolume behavior. Let
  <math|\<mu\><rprime|'>> be the chemical potential and
  <math|<fug><rprime|'>> be the fugacity at the pressure <math|p<rprime|'>>
  of interest; let <math|\<mu\><rprime|''>> be the chemical potential and
  <math|<fug><rprime|''>> be the fugacity of the same gas at some low
  pressure <math|p<rprime|''>> (all at the same temperature). Then we use Eq.
  <reference|mu-mu^o=int(Vm dp)> to write
  <math|\<mu\><rprime|'>-\<mu\><st><gas>=R*T*ln
  <around|(|<fug><rprime|'>/p<st>|)>> and
  <math|\<mu\><rprime|''>-\<mu\><st><gas>=R*T*ln
  <around|(|<fug><rprime|''>/p<st>|)>>, from which we obtain

  <\equation>
    \<mu\><rprime|'>-\<mu\><rprime|''>=R*T*ln
    <frac|<fug><rprime|'>|<fug><rprime|''>>
  </equation>

  By integrating <math|<dif>\<mu\>=V<m><difp>> from pressure
  <math|p<rprime|''>> to pressure <math|p<rprime|'>>, we obtain

  <\equation>
    \<mu\><rprime|'>-\<mu\><rprime|''>=<big|int><rsub|\<mu\><rprime|''>><rsup|\<mu\><rprime|'>><dif>\<mu\>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>>V<m><difp>
  </equation>

  Equating the two expressions for <math|\<mu\><rprime|'>-\<mu\><rprime|''>>
  and dividing by <math|R*T> gives

  <\equation>
    <label|ln(f/f'')=int (Vm/RT)dp>ln <frac|<fug><rprime|'>|<fug><rprime|''>>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>><frac|V<m>|R*T><difp>
  </equation>

  In principle, we could use the integral on the right side of Eq.
  <reference|ln(f/f'')=int (Vm/RT)dp> to evaluate <math|<fug><rprime|'>> by
  choosing the lower integration limit <math|p<rprime|''>> to be such a low
  pressure that the gas behaves as an ideal gas and replacing
  <math|<fug><rprime|''>> by <math|p<rprime|''>>. However, because the
  integrand <math|V<m>/R*T> becomes very large at low pressure, the integral
  is difficult to evaluate. We avoid this difficulty by subtracting from the
  preceding equation the identity

  <\equation>
    ln <frac|p<rprime|'>|p<rprime|''>>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>><frac|<difp>|p>
  </equation>

  which is simply the result of integrating the function <math|1/p> from
  <math|p<rprime|''>> to <math|p<rprime|'>>. The result is

  <\equation>
    <label|ln(f'p''/f''p')=>ln <frac|<fug><rprime|'>p<rprime|''>|<fug><rprime|''>p<rprime|'>>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)><difp>
  </equation>

  Now we take the limit of both sides of Eq. <reference|ln(f'p''/f''p')=> as
  <math|p<rprime|''>> approaches zero. In this limit, the gas at pressure
  <math|p<rprime|''>> approaches ideal-gas behavior, <math|<fug><rprime|''>>
  approaches <math|p<rprime|''>>, and the ratio
  <math|<fug><rprime|'>p<rprime|''>/<fug><rprime|''>p<rprime|'>> approaches
  <math|<fug><rprime|'>/p<rprime|'>>:

  <\equation>
    <label|ln(f/p)=int((Vm/RT)-(1/p))dp>ln
    <frac|<fug><rprime|'>|p<rprime|'>>=<big|int><rsub|0><rsup|p<rprime|'>><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)><difp>
  </equation>

  The integrand <math|<around|(|V<m>/R*T-1/p|)>> of this integral approaches
  zero at low pressure, making it feasible to evaluate the integral from
  experimental data.

  The <index-complex|<tuple|fugacity coefficient|gas>|||<tuple|Fugacity
  coefficient|of a gas>><newterm|fugacity coefficient> <math|\<phi\>> of a
  gas is defined by

  <equation-cov2|<label|f=phi*p>\<phi\><defn><frac|<fug>|p><space|1em><text|or><space|1em><fug>=\<phi\>*p|(pure
  gas)>

  The fugacity coefficient at pressure <math|p<rprime|'>> is then given by
  Eq. <reference|ln(f/p)=int((Vm/RT)-(1/p))dp>:

  <equation-cov2|<label|ln(phi)=int(Vm/RT-1/p)dp>ln
  \<phi\><around|(|p<rprime|'>|)>=<big|int><rsub|0><rsup|p<rprime|'>><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)>*<difp>|(pure
  gas, constant <math|T>)>

  The isothermal behavior of real gases at low to moderate pressures (up to
  at least <math|1<br>>) is usually adequately described by a two-term
  equation of state of the form given in Eq. <reference|Vm=RT/p+B>:

  <\equation>
    <label|Vm approx RT/p + B>V<m>\<approx\><frac|R*T|p>+B
  </equation>

  Here <math|B> is the second virial coefficient, a function of <math|T>.
  With this equation of state, Eq. <reference|ln(phi)=int(Vm/RT-1/p)dp>
  becomes

  <\equation>
    <label|ln(phi)=Bp/RT>ln \<phi\>\<approx\><frac|B*p|R*T>
  </equation>

  For a real gas at temperature <math|T> and pressure <math|p>, Eq.
  <reference|ln(phi)=int(Vm/RT-1/p)dp> or <reference|ln(phi)=Bp/RT> allows us
  to evaluate the fugacity coefficient from an experimental equation of state
  or a second virial coefficient. We can then find the fugacity from
  <math|<fug>=\<phi\>*p>.

  <\quote-env>
    As we will see in Sec. <reference|9-activities>, the dimensionless ratio
    <math|\<phi\>=<fug>/p> is an example of an <index-complex|<tuple|activity
    coefficient|gas>|||<tuple|Activity coefficient|of a gas>><em|activity
    coefficient> and the dimensionless ratio <math|<fug>/p<st>> is an example
    of an <index-complex|<tuple|activity|gas>|||<tuple|Activity|of a
    gas>><em|activity>.
  </quote-env>

  <subsection|Liquids and solids><label|c7 sec cpf-liquids-solids>

  The dependence of the <index-complex|<tuple|chemical potential|liquid or
  solid>|||<tuple|Chemical potential|of a liquid or solid>>chemical potential
  on pressure at constant temperature is given by Eq.
  <reference|mu-mu^o=int(Vm dp)>. With an approximation of zero
  compressibility, this becomes

  <\equation-cov2|<label|mu=mo+Vm(p-po)>\<mu\>\<approx\>\<mu\><st>+V<m><around|(|p-p<st>|)>>
    (pure liquid or solid,

    constant <math|T>)
  </equation-cov2>

  <section|Standard Molar Quantities of a Gas><label|7-st molar fncs of a
  gas><label|c7 sec smqg>

  <index-complex|<tuple|standard molar|quantity|gas>||c7 sec smqg
  idx1|<tuple|Standard molar|quantity|of a gas>>A <subindex|Standard
  molar|quantity><newterm|standard molar quantity> of a substance is the
  molar quantity in the standard state at the temperature of interest. We
  have seen (Sec. <reference|7-st states of pure substances>) that the
  standard state of a pure <em|liquid> or <em|solid> is a real state, so any
  standard molar quantity of a pure liquid or solid is simply the molar
  quantity evaluated at the standard pressure and the temperature of
  interest.

  The standard state of a <em|gas>, however, is a hypothetical state in which
  the gas behaves ideally at the standard pressure without influence of
  intermolecular forces. The properties of the gas in this standard state are
  those of an ideal gas. We would like to be able to relate molar properties
  of the real gas at a given temperature and pressure to the molar properties
  in the standard state at the same temperature.

  We begin by using Eq. <reference|mu=muo(g)+RT*ln(f/po)> to write an
  expression for the chemical potential of the real gas at pressure
  <math|p<rprime|'>>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<mu\><around|(|p<rprime|'>|)>>|<cell|=>|<cell|\<mu\><st><gas>+R*T*ln
    <frac|<fug><around|(|p<rprime|'>|)>|p<st>>>>|<row|<cell|>|<cell|=>|<cell|\<mu\><st><gas>+R*T*ln
    <frac|p<rprime|'>|p<st>>+R*T*ln <frac|<fug><around|(|p<rprime|'>|)>|p<rprime|'>><eq-number>>>>>
  </eqnarray*>

  We then substitute from Eq. <reference|ln(f/p)=int((Vm/RT)-(1/p))dp> to
  obtain a relation between the chemical potential, the standard chemical
  potential, and measurable properties, all at the same temperature:

  <equation-cov2|<label|mu=muo+RT*ln(p/po)+int...>\<mu\><around|(|p<rprime|'>|)>=\<mu\><st><gas>+R*T*ln
  <frac|p<rprime|'>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><space|-0.17em><space|-0.17em><around*|(|V<m>-<frac|R*T|p>|)>*<difp>|(pure
  gas)>

  Note that this expression for <math|\<mu\>> is not what we would obtain by
  simply integrating <math|<dif>\<mu\>=V<m><difp>> from <math|p<st>> to
  <math|p<rprime|'>>, because the real gas is not necessarily in its standard
  state of ideal-gas behavior at a pressure of <math|1<br>>.

  Recall that the chemical potential <math|\<mu\>> of a pure substance is
  also its molar Gibbs energy <math|G<m>=G/n>. The standard chemical
  potential <math|\<mu\><st><gas>> of the gas is the standard molar Gibbs
  energy, <math|G<m><st><gas>>. Therefore Eq.
  <reference|mu=muo+RT*ln(p/po)+int...> can be rewritten in the form

  <\equation>
    <label|Gm=Gmo+RT*ln(p/po)+int...>G<m><around|(|p<rprime|'>|)>=G<m><st><gas>+R*T*ln
    <frac|p<rprime|'>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><space|-0.17em><space|-0.17em><around*|(|V<m>-<frac|R*T|p>|)><difp>
  </equation>

  The middle column of Table <vpageref|tbl:7-gas standard molar> contains an
  expression for <math|G<m><around|(|p<rprime|'>|)>-G<m><st><gas>> taken from
  this equation.<float|float|thb|<\big-table>
    <\equation*>
      <bktable3|<tformat|<cwith|1|1|1|3|cell-hyphen|b>|<cwith|2|7|1|2|cell-rsep|1fn>|<cwith|2|7|1|3|cell-bsep|0.5fn>|<cwith|2|7|1|3|cell-tsep|0.5fn>||||||||||<table|<row|<\cell>
        <text|Difference>
      </cell>|<\cell>
        <text|General expression at pressure> p\ 
      </cell>|<\cell>
        <text|<no-indent*>Equation of state>

        V=n*R*T/p+n*B
      </cell>>|<row|<cell|U<m>-U<m><st><gas>>|<cell|<big|int><rsub|0><rsup|p<rprime|'>><around*|[|V<m>-T<Pd|V<m>|T|p>|]>*<difp>+R*T-p<rprime|'>*V<m>>|<cell|-p*T*<frac|<dif>B|<dif>T>>>|<row|<cell|H<m>-H<m><st><gas>>|<cell|<big|int><rsub|0><rsup|p<rprime|'>><around*|[|V<m>-T<Pd|V<m>|T|<space|-0.17em>p>|]>*<difp>>|<cell|p*<around*|(|B-T*<frac|<dif>B|<dif>T>|)>>>|<row|<cell|A<m>-A<m><st><gas>>|<cell|R*T*ln
      <frac|p<rprime|'>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><around*|(|V<m>-<frac|R*T|p>|)>*<difp>+R*T-p<rprime|'>*V<m>>|<cell|R*T*ln
      <frac|p|p<st>>>>|<row|<cell|G<m>-G<m><st><gas>>|<cell|R*T*ln
      <frac|p<rprime|'>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><around*|(|V<m>-<frac|R*T|p>|)>*<difp>>|<cell|R*T*ln
      <frac|p|p<st>>+B*p>>|<row|<cell|S<m>-S<m><st><gas>>|<cell|-R*ln
      <frac|p<rprime|'>|p<st>>-<big|int><rsub|0><rsup|p<rprime|'>><around*|[|<Pd|V<m>|T|<space|-0.17em>p>-<frac|R|p>|]>*<difp>>|<cell|-R*ln
      <frac|p|p<st>>-p*<frac|<dif>B|<dif>T>>>|<row|<cell|<Cpm>-<Cpm><st><gas>>|<cell|-<big|int><rsub|0><rsup|p<rprime|'>>T<Pd|<rsup|2>V<m>|T<rsup|2>|<space|-0.17em>p>*<difp>>|<cell|-p*T*<frac|<dif><rsup|2>B|<dif>T<rsup|2>>>>>>>
    </equation*>

    \;
  <|big-table>
    <label|tbl:7-gas standard molar>Real gases: expressions for differences
    between molar properties and standard molar values at the same
    temperature
  </big-table>>

  This expression contains all the information needed to find a relation
  between any other molar property and its standard molar value in terms of
  measurable properties. The way this can be done is as follows.

  The relation between the chemical potential of a pure substance and its
  molar entropy is given by Eq. <reference|dmu/dT=-Sm>:

  <\equation>
    <label|Sm=-dmu/dT>S<m>=-<Pd|\<mu\>|T|<space|-0.17em>p>
  </equation>

  The <index-complex|<tuple|entropy|standard
  molar|gas>|||<tuple|Entropy|standard molar|of a gas>>standard molar entropy
  of the gas is found from Eq. <reference|Sm=-dmu/dT> by changing
  <math|\<mu\>> to <math|\<mu\><st><gas>>:

  <\equation>
    <label|Smo=-dmuo/dT>S<m><st><gas>=-<Pd|\<mu\><st><gas>|T|<space|-0.17em>p>
  </equation>

  By substituting the expression for <math|\<mu\>> given by Eq.
  <reference|mu=muo+RT*ln(p/po)+int...> into Eq. <reference|Sm=-dmu/dT> and
  comparing the result with Eq. <reference|Smo=-dmuo/dT>, we obtain

  <\equation>
    <label|Sm=Smo-R*ln(p/po)-int...>S<m><around|(|p<rprime|'>|)>=S<m><st><gas>-R*ln
    <frac|p<rprime|'>|p<st>>-<big|int><rsub|0><rsup|p<rprime|'>><around*|[|<Pd|V<m>|T|<space|-0.17em>p>-<frac|R|p>|]>*<difp>
  </equation>

  The expression for <math|S<m>-S<m><st><gas>> in the middle column of Table
  <reference|tbl:7-gas standard molar> comes from this equation. The
  equation, together with a value of <math|S<m>> for a real gas obtained by
  the calorimetric method described in Sec. <reference|6-third law molar
  entropies>, can be used to evaluate <math|S<m><st><gas>>.

  Now we can use the expressions for <math|G<m>> and <math|S<m>> to find
  expressions for molar quantities such as <math|H<m>> and <math|<Cpm>>
  relative to the respective standard molar quantities. The general procedure
  for a molar quantity <math|X<m>> is to write an expression for <math|X<m>>
  as a function of <math|G<m>> and <math|S<m>> and an analogous expression
  for <math|X<m><st><gas>> as a function of <math|G<m><st><gas>> and
  <math|S<m><st><gas>>. Substitutions for <math|G<m>> and <math|S<m>> from
  Eqs. <reference|Gm=Gmo+RT*ln(p/po)+int...> and
  <reference|Sm=Smo-R*ln(p/po)-int...> are then made in the expression for
  <math|X<m>>, and the difference <math|X<m>-X<m><st><gas>> taken.

  For example, the expression for <math|U<m>-U<m><st><gas>> in the middle
  column Table <reference|tbl:7-gas standard molar> was derived as follows.
  The equation defining the Gibbs energy, <math|G=U-T*S+p*V>, was divided by
  the amount <math|n> and rearranged to

  <\equation>
    <label|Um=Gm+TSm-pVm>U<m>=G<m>+T*S<m>-p*V<m>
  </equation>

  The standard-state version of this relation is

  <\equation>
    <label|Um^o(g)=>U<m><st><gas>=G<m><st><gas>+T*S<m><st><gas>-p<st>V<m><st><gas>
  </equation>

  where from the ideal gas law <math|p<st>V<m><st><gas>> can be replaced by
  <math|R*T>. Substitutions from Eqs. <reference|Gm=Gmo+RT*ln(p/po)+int...>
  and <reference|Sm=Smo-R*ln(p/po)-int...> were made in Eq.
  <reference|Um=Gm+TSm-pVm> and the expression for <math|U<m><st><gas>> in
  Eq. <reference|Um^o(g)=> was subtracted, resulting in the expression in the
  table.

  For a real gas at low to moderate pressures, we can approximate <math|V<m>>
  by <math|<around|(|R*T/p|)>+B> where <math|B> is the second virial
  coefficient (Eq. <reference|Vm approx RT/p + B>). Equation
  <reference|mu=muo+RT*ln(p/po)+int...> then becomes

  <\equation>
    \<mu\>\<approx\>\<mu\><st><gas>+R*T*ln <frac|p|p<st>>+B*p
  </equation>

  The expressions in the last column of Table <reference|tbl:7-gas standard
  molar> use this equation of state. We can see what the expressions look
  like if the gas is ideal simply by setting <math|B> equal to zero. They
  show that when the pressure of an ideal gas increases at constant
  temperature, <math|G<m>> and <math|A<m>> increase, <math|S<m>> decreases,
  and <math|U<m>>, <math|H<m>>, and <math|<Cpm>> are
  unaffected.<index-complex|<tuple|standard molar|quantity|gas>||c7 sec smqg
  idx1|<tuple|Standard molar|quantity|of a gas>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|6>
    <associate|page-first|137>
    <associate|preamble|false>
    <associate|section-nr|4>
    <associate|src-compact|normal>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|7-JK coeff|<tuple|7.5.2|?>>
    <associate|7-chem pot|<tuple|7.8|?>>
    <associate|7-chem pot and fugacity - gases|<tuple|7.8.1|?>>
    <associate|7-heating at const V or p|<tuple|7.4|?>>
    <associate|7-ht cap measurement|<tuple|7.3.2|?>>
    <associate|7-int pressure|<tuple|7.2|?>>
    <associate|7-isothermal p changes|<tuple|7.6|?>>
    <associate|7-isothermal-jacket|<tuple|7.3.2.2|?>>
    <associate|7-partial derivatives|<tuple|7.5.1|?>>
    <associate|7-relation between Cpm and CVm|<tuple|7.3.1|?>>
    <associate|7-st molar fncs of a gas|<tuple|7.9|?>>
    <associate|7-st states of pure substances|<tuple|7.7|?>>
    <associate|C_V=,C_p=(id gas)|<tuple|7.3.3|?>>
    <associate|Chap. 7|<tuple|7|?>>
    <associate|Cpm=CVm+R|<tuple|7.3.5|?>>
    <associate|Cpm=CVm+alpha^2...|<tuple|7.3.11|?>>
    <associate|Gm=Gmo+RT*ln(p/po)+int...|<tuple|7.9.3|?>>
    <associate|Sm=-dmu/dT|<tuple|7.9.4|?>>
    <associate|Sm=Smo-R*ln(p/po)-int...|<tuple|7.9.6|?>>
    <associate|Smo=-dmuo/dT|<tuple|7.9.5|?>>
    <associate|Um=Gm+TSm-pVm|<tuple|7.9.7|?>>
    <associate|Um^o(g)=|<tuple|7.9.8|?>>
    <associate|Vm approx RT/p + B|<tuple|7.8.17|?>>
    <associate|alpha def|<tuple|7.1.1|?>>
    <associate|alpha(Vm)|<tuple|7.1.3|?>>
    <associate|auto-1|<tuple|7|?>>
    <associate|auto-10|<tuple|7.1.1|?>>
    <associate|auto-100|<tuple|Electrical|?>>
    <associate|auto-101|<tuple|Work|?>>
    <associate|auto-102|<tuple|Electric|?>>
    <associate|auto-103|<tuple|<tuple|calorimeter|continuous-flow>|?>>
    <associate|auto-104|<tuple|7.3.3|?>>
    <associate|auto-105|<tuple|7.3.3|?>>
    <associate|auto-106|<tuple|7.4|?>>
    <associate|auto-107|<tuple|<tuple|heating|constant volume>|?>>
    <associate|auto-108|<tuple|Molar|?>>
    <associate|auto-109|<tuple|Property|?>>
    <associate|auto-11|<tuple|7.1.2|?>>
    <associate|auto-110|<tuple|<tuple|entropy|change|constant volume>|?>>
    <associate|auto-111|<tuple|Enthalpy|?>>
    <associate|auto-112|<tuple|<tuple|entropy|change|constant pressure>|?>>
    <associate|auto-113|<tuple|<tuple|heating|constant volume>|?>>
    <associate|auto-114|<tuple|7.5|?>>
    <associate|auto-115|<tuple|7.5.1|?>>
    <associate|auto-116|<tuple|<tuple|partial derivative|expressions at
    constant T, p, and V>|?>>
    <associate|auto-117|<tuple|7.5.1|?>>
    <associate|auto-118|<tuple|7.5.2|?>>
    <associate|auto-119|<tuple|7.5.3|?>>
    <associate|auto-12|<tuple|7.2|?>>
    <associate|auto-120|<tuple|Bridgman, Percy|?>>
    <associate|auto-121|<tuple|<tuple|partial derivative|expressions at
    constant T, p, and V>|?>>
    <associate|auto-122|<tuple|7.5.2|?>>
    <associate|auto-123|<tuple|<tuple|joule-thomson|coefficient>|?>>
    <associate|auto-124|<tuple|<tuple|joule-thomson|coefficient>|?>>
    <associate|auto-125|<tuple|7.6|?>>
    <associate|auto-126|<tuple|<tuple|isothermal|pressure changes>|?>>
    <associate|auto-127|<tuple|<tuple|pressure|changes>|?>>
    <associate|auto-128|<tuple|7.6.1|?>>
    <associate|auto-129|<tuple|7.6.1|?>>
    <associate|auto-13|<tuple|<tuple|internal|pressure>|?>>
    <associate|auto-130|<tuple|<tuple|isothermal|pressure changes|ideal
    gas>|?>>
    <associate|auto-131|<tuple|<tuple|pressure|changes, isothermal|ideal
    gas>|?>>
    <associate|auto-132|<tuple|7.6.2|?>>
    <associate|auto-133|<tuple|<tuple|isothermal|pressure changes|condensed
    phase>|?>>
    <associate|auto-134|<tuple|<tuple|pressure|changes, isothermal|condensed
    phase>|?>>
    <associate|auto-135|<tuple|<tuple|isothermal|compressibility|liquid or
    solid>|?>>
    <associate|auto-136|<tuple|<tuple|isothermal|pressure changes>|?>>
    <associate|auto-137|<tuple|<tuple|pressure|changes>|?>>
    <associate|auto-138|<tuple|7.7|?>>
    <associate|auto-139|<tuple|Standard|?>>
    <associate|auto-14|<tuple|<tuple|pressure|internal>|?>>
    <associate|auto-140|<tuple|Pressure|?>>
    <associate|auto-141|<tuple|standard pressure|?>>
    <associate|auto-142|<tuple|Plimsoll mark|?>>
    <associate|auto-143|<tuple|<tuple|standard state|pure substance>|?>>
    <associate|auto-144|<tuple|<tuple|state|standard>|?>>
    <associate|auto-145|<tuple|standard state|?>>
    <associate|auto-146|<tuple|IUPAC Green Book|?>>
    <associate|auto-147|<tuple|<tuple|standard state|gas>|?>>
    <associate|auto-148|<tuple|<tuple|standard state|pure liquid or
    solid>|?>>
    <associate|auto-149|<tuple|7.8|?>>
    <associate|auto-15|<tuple|Internal|?>>
    <associate|auto-150|<tuple|<tuple|chemical potential|pure substance>|?>>
    <associate|auto-151|<tuple|chemical potential|?>>
    <associate|auto-152|<tuple|Gibbs energy|?>>
    <associate|auto-153|<tuple|<tuple|total differential|gibbs energy of a
    pure substance>|?>>
    <associate|auto-154|<tuple|<tuple|chemical potential|standard|pure
    substance>|?>>
    <associate|auto-155|<tuple|<tuple|standard|chemical potential>|?>>
    <associate|auto-156|<tuple|standard chemical potential|?>>
    <associate|auto-157|<tuple|Einstein energy relation|?>>
    <associate|auto-158|<tuple|7.8.1|?>>
    <associate|auto-159|<tuple|<tuple|chemical potential|standard|gas>|?>>
    <associate|auto-16|<tuple|Pressure|?>>
    <associate|auto-160|<tuple|7.8.1|?>>
    <associate|auto-161|<tuple|<tuple|fugacity|gas>|?>>
    <associate|auto-162|<tuple|fugacity|?>>
    <associate|auto-163|<tuple|<tuple|fugacity coefficient|gas>|?>>
    <associate|auto-164|<tuple|fugacity coefficient|?>>
    <associate|auto-165|<tuple|<tuple|activity coefficient|gas>|?>>
    <associate|auto-166|<tuple|<tuple|activity|gas>|?>>
    <associate|auto-167|<tuple|7.8.2|?>>
    <associate|auto-168|<tuple|<tuple|chemical potential|liquid or solid>|?>>
    <associate|auto-169|<tuple|7.9|?>>
    <associate|auto-17|<tuple|internal pressure|?>>
    <associate|auto-170|<tuple|<tuple|standard molar|quantity|gas>|?>>
    <associate|auto-171|<tuple|Standard molar|?>>
    <associate|auto-172|<tuple|standard molar quantity|?>>
    <associate|auto-173|<tuple|7.9.1|?>>
    <associate|auto-174|<tuple|<tuple|entropy|standard molar|gas>|?>>
    <associate|auto-175|<tuple|<tuple|standard molar|quantity|gas>|?>>
    <associate|auto-18|<tuple|Thermodynamic|?>>
    <associate|auto-19|<tuple|Equation of state|?>>
    <associate|auto-2|<tuple|7.1|?>>
    <associate|auto-20|<tuple|<tuple|internal|pressure|ideal gas>|?>>
    <associate|auto-21|<tuple|Ideal gas|?>>
    <associate|auto-22|<tuple|<tuple|temperature|ideal gas>|?>>
    <associate|auto-23|<tuple|Temperature|?>>
    <associate|auto-24|<tuple|Thermodynamic|?>>
    <associate|auto-25|<tuple|Pressure|?>>
    <associate|auto-26|<tuple|<tuple|internal|pressure>|?>>
    <associate|auto-27|<tuple|<tuple|pressure|internal>|?>>
    <associate|auto-28|<tuple|7.3|?>>
    <associate|auto-29|<tuple|7.3.1|?>>
    <associate|auto-3|<tuple|Cubic expansion coefficient|?>>
    <associate|auto-30|<tuple|<tuple|heat capacity|constant volume and
    constant pressure>|?>>
    <associate|auto-31|<tuple|Pressure|?>>
    <associate|auto-32|<tuple|Internal|?>>
    <associate|auto-33|<tuple|7.3.2|?>>
    <associate|auto-34|<tuple|Heat capacity|?>>
    <associate|auto-35|<tuple|<tuple|calorimetry|measure heat capacities>|?>>
    <associate|auto-36|<tuple|Electrical|?>>
    <associate|auto-37|<tuple|Work|?>>
    <associate|auto-38|<tuple|Electrical|?>>
    <associate|auto-39|<tuple|Heating|?>>
    <associate|auto-4|<tuple|cubic expansion coefficient|?>>
    <associate|auto-40|<tuple|Calorimeter|?>>
    <associate|auto-41|<tuple|calorimeter|?>>
    <associate|auto-42|<tuple|7.3.2.1|?>>
    <associate|auto-43|<tuple|<tuple|adiabatic|calorimeter>|?>>
    <associate|auto-44|<tuple|<tuple|calorimeter|adiabatic>|?>>
    <associate|auto-45|<tuple|Electric|?>>
    <associate|auto-46|<tuple|Current, electric|?>>
    <associate|auto-47|<tuple|Heater circuit|?>>
    <associate|auto-48|<tuple|Circuit|?>>
    <associate|auto-49|<tuple|Electrical|?>>
    <associate|auto-5|<tuple|Isothermal|?>>
    <associate|auto-50|<tuple|Work|?>>
    <associate|auto-51|<tuple|Electric|?>>
    <associate|auto-52|<tuple|Current, electric|?>>
    <associate|auto-53|<tuple|Electric|?>>
    <associate|auto-54|<tuple|Resistance|?>>
    <associate|auto-55|<tuple|Electrical|?>>
    <associate|auto-56|<tuple|Work|?>>
    <associate|auto-57|<tuple|Calorimeter|?>>
    <associate|auto-58|<tuple|Heating|?>>
    <associate|auto-59|<tuple|7.3.1|?>>
    <associate|auto-6|<tuple|isothermal compressibility|?>>
    <associate|auto-60|<tuple|Heater circuit|?>>
    <associate|auto-61|<tuple|Circuit|?>>
    <associate|auto-62|<tuple|Electrical|?>>
    <associate|auto-63|<tuple|Heating|?>>
    <associate|auto-64|<tuple|Energy equivalent|?>>
    <associate|auto-65|<tuple|energy equivalent|?>>
    <associate|auto-66|<tuple|Electrical|?>>
    <associate|auto-67|<tuple|Work|?>>
    <associate|auto-68|<tuple|Calorimeter|?>>
    <associate|auto-69|<tuple|Energy equivalent|?>>
    <associate|auto-7|<tuple|Coefficient of thermal expansion|?>>
    <associate|auto-70|<tuple|Electrical|?>>
    <associate|auto-71|<tuple|Work|?>>
    <associate|auto-72|<tuple|Energy|?>>
    <associate|auto-73|<tuple|Dissipation of energy|?>>
    <associate|auto-74|<tuple|<tuple|adiabatic|calorimeter>|?>>
    <associate|auto-75|<tuple|<tuple|calorimeter|adiabatic>|?>>
    <associate|auto-76|<tuple|7.3.2.2|?>>
    <associate|auto-77|<tuple|<tuple|calorimeter|isothermal-jacket>|?>>
    <associate|auto-78|<tuple|Isoperibol calorimeter|?>>
    <associate|auto-79|<tuple|Calorimeter|?>>
    <associate|auto-8|<tuple|Expansivity coefficient|?>>
    <associate|auto-80|<tuple|Newton's law of cooling|?>>
    <associate|auto-81|<tuple|Thermal|?>>
    <associate|auto-82|<tuple|Heater circuit|?>>
    <associate|auto-83|<tuple|Circuit|?>>
    <associate|auto-84|<tuple|Heating|?>>
    <associate|auto-85|<tuple|7.3.2|?>>
    <associate|auto-86|<tuple|Heater circuit|?>>
    <associate|auto-87|<tuple|Circuit|?>>
    <associate|auto-88|<tuple|Convergence temperature|?>>
    <associate|auto-89|<tuple|Temperature|?>>
    <associate|auto-9|<tuple|Cubic expansion coefficient|?>>
    <associate|auto-90|<tuple|Energy equivalent|?>>
    <associate|auto-91|<tuple|Heater circuit|?>>
    <associate|auto-92|<tuple|Circuit|?>>
    <associate|auto-93|<tuple|Energy equivalent|?>>
    <associate|auto-94|<tuple|Heating|?>>
    <associate|auto-95|<tuple|<tuple|calorimeter|isothermal-jacket>|?>>
    <associate|auto-96|<tuple|7.3.2.3|?>>
    <associate|auto-97|<tuple|<tuple|calorimeter|continuous-flow>|?>>
    <associate|auto-98|<tuple|Electrical|?>>
    <associate|auto-99|<tuple|Heating|?>>
    <associate|c7|<tuple|7|?>>
    <associate|c7 sec cpf|<tuple|7.8|?>>
    <associate|c7 sec cpf-gases|<tuple|7.8.1|?>>
    <associate|c7 sec cpf-liquids-solids|<tuple|7.8.2|?>>
    <associate|c7 sec hcvp|<tuple|7.4|?>>
    <associate|c7 sec ip|<tuple|7.2|?>>
    <associate|c7 sec ipc|<tuple|7.6|?>>
    <associate|c7 sec ipc-condensed-phase|<tuple|7.6.2|?>>
    <associate|c7 sec ipc-ideal-gas|<tuple|7.6.1|?>>
    <associate|c7 sec pdtpv|<tuple|7.5|?>>
    <associate|c7 sec pdtpv-jt|<tuple|7.5.2|?>>
    <associate|c7 sec pdtpv-tables|<tuple|7.5.1|?>>
    <associate|c7 sec smqg|<tuple|7.9|?>>
    <associate|c7 sec ssps|<tuple|7.7|?>>
    <associate|c7 sec tp|<tuple|7.3|?>>
    <associate|c7 sec tp-cvcp|<tuple|7.3.1|?>>
    <associate|c7 sec tp-heatcap|<tuple|7.3.2|?>>
    <associate|c7 sec tp-heatcap-ac|<tuple|7.3.2.1|?>>
    <associate|c7 sec tp-heatcap-cfc|<tuple|7.3.2.3|?>>
    <associate|c7 sec tp-heatcap-ijc|<tuple|7.3.2.2|?>>
    <associate|c7 sec tp-typical|<tuple|7.3.3|?>>
    <associate|c7 sec vp|<tuple|7.1|?>>
    <associate|dH=dq,C_p=dH/dT|<tuple|7.3.2|?>>
    <associate|dH=dw(el)+dw(cont)|<tuple|7.3.18|?>>
    <associate|dS=(CV/T)dT|<tuple|7.4.6|?>>
    <associate|dS=(Cp/T)dT|<tuple|7.4.11|?>>
    <associate|dU/dV=0 (id gas)|<tuple|7.2.3|?>>
    <associate|dU/dV=Tdp/dT-p|<tuple|7.2.1|?>>
    <associate|dU/dV=alpha*T/kappaT-p|<tuple|7.2.4|?>>
    <associate|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt|<tuple|7.3.22|?>>
    <associate|dU/dt=-k(T-T(inf)+dw(el)/dt|<tuple|7.3.23|?>>
    <associate|dU=-pdV+dw(el)+dw(cont)|<tuple|7.3.12|?>>
    <associate|dU=CVdT|<tuple|7.4.1|?>>
    <associate|dU=dq,C_V=dU/dT|<tuple|7.3.1|?>>
    <associate|dU=dw(el)+dw(cont)|<tuple|7.3.13|?>>
    <associate|dV=alpha*VdT-kappaT*Vdp|<tuple|7.1.6|?>>
    <associate|delH=(Cp)delT|<tuple|7.4.10|?>>
    <associate|delH=-k int((T-T(inf))dt+w(el)|<tuple|7.3.28|?>>
    <associate|delH=int(Cp)dT|<tuple|7.4.9|?>>
    <associate|delH=w(el)+er(t2-t1)|<tuple|7.3.19|?>>
    <associate|delS=CV*ln(T2/T1)|<tuple|7.4.8|?>>
    <associate|delS=Cp*ln(T2/T1)|<tuple|7.4.13|?>>
    <associate|delS=int(CV/T)dT|<tuple|7.4.7|?>>
    <associate|delS=int(Cp/T)dT|<tuple|7.4.12|?>>
    <associate|delU=-k int((T-T(inf))dt+w(el)|<tuple|7.3.24|?>>
    <associate|delU=CV(T2-T1)|<tuple|7.4.5|?>>
    <associate|delU=int(CV)dT|<tuple|7.4.2|?>>
    <associate|delU=w(el)+er(t2-t1)|<tuple|7.3.15|?>>
    <associate|dmu/dT=-Sm|<tuple|7.8.3|?>>
    <associate|dmu/dp=Vm|<tuple|7.8.4|?>>
    <associate|dmu=-(Sm)dT+(Vm)dp|<tuple|7.8.2|?>>
    <associate|dp/dT=alpha/kappaT|<tuple|7.1.7|?>>
    <associate|dq/dt=-k(T-T(ext))|<tuple|7.3.20|?>>
    <associate|e=w(el)/(T2-T1-r(t2-t1))|<tuple|7.3.17|?>>
    <associate|e=w(el)/[(T2-T1+(k/e)int(T-T(inf))dt]|<tuple|7.3.26|?>>
    <associate|energy equiv|<tuple|energy equivalent|?>>
    <associate|f=phi*p|<tuple|7.8.15|?>>
    <associate|fig:7-Cp vs T|<tuple|7.3.3|?>>
    <associate|fig:7-ad heating curve|<tuple|7.3.1|?>>
    <associate|fig:7-alpha vs T|<tuple|7.1.1|?>>
    <associate|fig:7-heating curve|<tuple|7.3.2|?>>
    <associate|fig:7-kappaT vs T|<tuple|7.1.2|?>>
    <associate|fig:7-mu vs p|<tuple|7.8.1|?>>
    <associate|footnote-7.1.1|<tuple|7.1.1|?>>
    <associate|footnote-7.1.2|<tuple|7.1.2|?>>
    <associate|footnote-7.2.1|<tuple|7.2.1|?>>
    <associate|footnote-7.3.1|<tuple|7.3.1|?>>
    <associate|footnote-7.3.2|<tuple|7.3.2|?>>
    <associate|footnote-7.3.3|<tuple|7.3.3|?>>
    <associate|footnote-7.4.1|<tuple|7.4.1|?>>
    <associate|footnote-7.5.1|<tuple|7.5.1|?>>
    <associate|footnote-7.7.1|<tuple|7.7.1|?>>
    <associate|footnote-7.7.2|<tuple|7.7.2|?>>
    <associate|footnote-7.7.3|<tuple|7.7.3|?>>
    <associate|footnote-7.8.1|<tuple|7.8.1|?>>
    <associate|footnr-7.1.1|<tuple|Cubic expansion coefficient|?>>
    <associate|footnr-7.1.2|<tuple|7.1.1|?>>
    <associate|footnr-7.2.1|<tuple|Pressure|?>>
    <associate|footnr-7.3.1|<tuple|7.3.1|?>>
    <associate|footnr-7.3.2|<tuple|7.3.2|?>>
    <associate|footnr-7.3.3|<tuple|7.3.3|?>>
    <associate|footnr-7.4.1|<tuple|7.4.1|?>>
    <associate|footnr-7.5.1|<tuple|7.5.1|?>>
    <associate|footnr-7.7.1|<tuple|7.7.1|?>>
    <associate|footnr-7.7.2|<tuple|7.7.2|?>>
    <associate|footnr-7.7.3|<tuple|7.7.3|?>>
    <associate|footnr-7.8.1|<tuple|Einstein energy relation|?>>
    <associate|k=[(r1-r2)/(T2-T1)]e|<tuple|7.3.32|?>>
    <associate|kappaT def|<tuple|7.1.2|?>>
    <associate|kappaT(Vm)|<tuple|7.1.4|?>>
    <associate|ln(f'p''/f''p')=|<tuple|7.8.13|?>>
    <associate|ln(f/f'')=int (Vm/RT)dp|<tuple|7.8.11|?>>
    <associate|ln(f/p)=int((Vm/RT)-(1/p))dp|<tuple|7.8.14|?>>
    <associate|ln(phi)=Bp/RT|<tuple|7.8.18|?>>
    <associate|ln(phi)=int(Vm/RT-1/p)dp|<tuple|7.8.16|?>>
    <associate|mu-mu^o=int(Vm dp)|<tuple|7.8.5|?>>
    <associate|mu=Gm|<tuple|7.8.1|?>>
    <associate|mu=mo+Vm(p-po)|<tuple|7.8.19|?>>
    <associate|mu=muo(g)+RT*ln(f/po)|<tuple|7.8.7|?>>
    <associate|mu=muo(g)+RT*ln(p/po)|<tuple|7.8.6|?>>
    <associate|mu=muo+RT*ln(p/po)+int...|<tuple|7.9.2|?>>
    <associate|tbl:7-const T|<tuple|7.5.1|?>>
    <associate|tbl:7-const V|<tuple|7.5.3|?>>
    <associate|tbl:7-const p|<tuple|7.5.2|?>>
    <associate|tbl:7-gas standard molar|<tuple|7.9.1|?>>
    <associate|tbl:7-isothermal p change|<tuple|7.6.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      mary-96

      salvador-03

      eisenberg-69

      grigull-90

      ICT-3

      eisenberg-69

      kell-75

      ICT-3

      bridgman-14

      bridgman-61

      iupac-2014-pressure

      greenbook-3
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.1.1>|>
        The cubic expansion coefficient of several substances and an ideal
        gas as functions of temperature at
        <with|mode|<quote|math>|p=1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.<space|0spc><assign|footnote-nr|2><hidden-binding|<tuple>|7.1.2><assign|fnote-+pnwft35dEGUGeY|<quote|7.1.2>><assign|fnlab-+pnwft35dEGUGeY|<quote|7.1.2>><rsup|<with|font-shape|<quote|right>|<reference|footnote-7.1.2>>>
        Note that because liquid water has a density maximum at
        <with|mode|<quote|math>|4 <rsup|\<circ\>><with|mode|<quote|text>|C>>,
        <with|mode|<quote|math>|\<alpha\>> is zero at that temperature.

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-7FFCC02E8--7FFB47F80>|<link|hyperlink|<id|%-7FFCC02E8--7FFB47F80>|<url|#footnr-7.1.2>>|7.1.2>.
        |<hidden-binding|<tuple|footnote-7.1.2>|7.1.2>|Based on data in Ref.
        [<write|bib|eisenberg-69><reference|bib-eisenberg-69>], p. 104; Ref.
        [<write|bib|grigull-90><reference|bib-grigull-90>]; and Ref.
        [<write|bib|ICT-3><reference|bib-ICT-3>], p. 28.>>>
      </surround>|<pageref|auto-10>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.1.2>|>
        The isothermal compressibility of several substances as a function of
        temperature at <with|mode|<quote|math>|p=1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>. (Based on data in Ref.
        [<write|bib|eisenberg-69><reference|bib-eisenberg-69>]; Ref.
        [<write|bib|kell-75><reference|bib-kell-75>]; and Ref.
        [<write|bib|ICT-3><reference|bib-ICT-3>], p. 28.)
      </surround>|<pageref|auto-11>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.3.1>|>
        Typical heating curve of an adiabatic calorimeter.
      </surround>|<pageref|auto-59>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.3.2>|>
        Typical heating curve of an isothermal-jacket calorimeter.
      </surround>|<pageref|auto-85>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.3.3>|>
        Temperature dependence of molar heat capacity at constant pressure
        (<with|mode|<quote|math>|p=1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>) of
        H<rsub|<with|mode|<quote|math>|2>>O,
        N<rsub|<with|mode|<quote|math>|2>>, and C(graphite).
      </surround>|<pageref|auto-105>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.8.1>|>
        Chemical potential as a function of pressure at constant temperature,
        for a real gas (solid curve) and the same gas behaving ideally
        (dashed curve). Point A is the gas standard state. Point B is a state
        of the real gas at pressure <with|mode|<quote|math>|p<rprime|'>>. The
        fugacity <with|mode|<quote|math>|f<around|(|p<rprime|'>|)>> of the
        real gas at pressure <with|mode|<quote|math>|p<rprime|'>> is equal to
        the pressure of the ideal gas having the same chemical potential as
        the real gas (point C).
      </surround>|<pageref|auto-160>>
    </associate>
    <\associate|gly>
      <tuple|normal|cubic expansion coefficient|<pageref|auto-4>>

      <tuple|normal|isothermal compressibility|<pageref|auto-6>>

      <tuple|normal|internal pressure|<pageref|auto-17>>

      <tuple|normal|calorimeter|<pageref|auto-41>>

      <tuple|normal|energy equivalent|<pageref|auto-65>>

      <tuple|normal|standard pressure|<pageref|auto-141>>

      <tuple|normal|standard state|<pageref|auto-145>>

      <tuple|normal|chemical potential|<pageref|auto-151>>

      <tuple|normal|standard chemical potential|<pageref|auto-156>>

      <tuple|normal|fugacity|<pageref|auto-162>>

      <tuple|normal|fugacity coefficient|<pageref|auto-164>>

      <tuple|normal|standard molar quantity|<pageref|auto-172>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Cubic expansion coefficient>|<pageref|auto-3>>

      <tuple|<tuple|Isothermal|compressibility>|<pageref|auto-5>>

      <tuple|<tuple|Coefficient of thermal expansion>|<pageref|auto-7>>

      <tuple|<tuple|Expansivity coefficient>|<pageref|auto-8>>

      <tuple|<tuple|Cubic expansion coefficient|negative values
      of>|<pageref|auto-9>>

      <tuple|<tuple|internal|pressure>||c7 sec ip
      idx1|<tuple|Internal|pressure>|<pageref|auto-13>>

      <tuple|<tuple|pressure|internal>||c7 sec ip
      idx2|<tuple|Pressure|internal>|<pageref|auto-14>>

      <tuple|<tuple|Internal|pressure>|<pageref|auto-15>>

      <tuple|<tuple|Pressure|internal>|<pageref|auto-16>>

      <tuple|<tuple|Thermodynamic|equation of state>|<pageref|auto-18>>

      <tuple|<tuple|Equation of state|thermodynamic>|<pageref|auto-19>>

      <tuple|<tuple|internal|pressure|ideal
      gas>|||<tuple|Internal|pressure|of an ideal gas>|<pageref|auto-20>>

      <tuple|<tuple|Ideal gas|internal pressure>|<pageref|auto-21>>

      <tuple|<tuple|temperature|ideal gas>|||<tuple|Temperature|ideal-gas>|<pageref|auto-22>>

      <tuple|<tuple|Temperature|thermodynamic>|<pageref|auto-23>>

      <tuple|<tuple|Thermodynamic|temperature>|<pageref|auto-24>>

      <tuple|<tuple|Pressure|negative>|<pageref|auto-25>>

      <tuple|<tuple|internal|pressure>||c7 sec ip
      idx1|<tuple|Internal|pressure>|<pageref|auto-26>>

      <tuple|<tuple|pressure|internal>||c7 sec ip
      idx2|<tuple|Pressure|internal>|<pageref|auto-27>>

      <tuple|<tuple|heat capacity|constant volume and constant
      pressure>|||<tuple|Heat capacity|at constant volume and constant
      pressure, relation between>|<pageref|auto-30>>

      <tuple|<tuple|Pressure|internal>|<pageref|auto-31>>

      <tuple|<tuple|Internal|pressure>|<pageref|auto-32>>

      <tuple|<tuple|Heat capacity|measurement of, by
      calorimetry>|<pageref|auto-34>>

      <tuple|<tuple|calorimetry|measure heat
      capacities>|||<tuple|Calorimetry|to measure heat
      capacities>|<pageref|auto-35>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-36>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-37>>

      <tuple|<tuple|Electrical|heating>|<pageref|auto-38>>

      <tuple|<tuple|Heating|electrical>|<pageref|auto-39>>

      <tuple|<tuple|Calorimeter>|<pageref|auto-40>>

      <tuple|<tuple|adiabatic|calorimeter>||c7 sec tp-heatcap-ac
      idx1|<tuple|Adiabatic|calorimeter>|<pageref|auto-43>>

      <tuple|<tuple|calorimeter|adiabatic>||c7 sec tp-heatcap-ac
      idx2|<tuple|Calorimeter|adiabatic>|<pageref|auto-44>>

      <tuple|<tuple|Electric|current>|<pageref|auto-45>>

      <tuple|<tuple|Current, electric>|<pageref|auto-46>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-47>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-48>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-49>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-50>>

      <tuple|<tuple|Electric|current>|<pageref|auto-51>>

      <tuple|<tuple|Current, electric>|<pageref|auto-52>>

      <tuple|<tuple|Electric|resistance>|<pageref|auto-53>>

      <tuple|<tuple|Resistance|electric>|<pageref|auto-54>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-55>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-56>>

      <tuple|<tuple|Calorimeter|constant-volume>|<pageref|auto-57>>

      <tuple|<tuple|Heating|curve, of a calorimeter>|<pageref|auto-58>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-60>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-61>>

      <tuple|<tuple|Electrical|heating>|<pageref|auto-62>>

      <tuple|<tuple|Heating|electrical>|<pageref|auto-63>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-64>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-66>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-67>>

      <tuple|<tuple|Calorimeter|constant-pressure>|<pageref|auto-68>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-69>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-70>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-71>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-72>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-73>>

      <tuple|<tuple|adiabatic|calorimeter>||c7 sec tp-heatcap-ac
      idx1|<tuple|Adiabatic|calorimeter>|<pageref|auto-74>>

      <tuple|<tuple|calorimeter|adiabatic>||c7 sec tp-heatcap-ac
      idx2|<tuple|Calorimeter|adiabatic>|<pageref|auto-75>>

      <tuple|<tuple|calorimeter|isothermal-jacket>||c7 sec tp-heatcap-ijc
      idx1|<tuple|Calorimeter|isothermal\Ujacket>|<pageref|auto-77>>

      <tuple|<tuple|Isoperibol calorimeter>|<pageref|auto-78>>

      <tuple|<tuple|Calorimeter|isoperibol>|<pageref|auto-79>>

      <tuple|<tuple|Newton's law of cooling>|<pageref|auto-80>>

      <tuple|<tuple|Thermal|conductance>|<pageref|auto-81>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-82>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-83>>

      <tuple|<tuple|Heating|curve, of a calorimeter>|<pageref|auto-84>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-86>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-87>>

      <tuple|<tuple|Convergence temperature>|<pageref|auto-88>>

      <tuple|<tuple|Temperature|convergence>|<pageref|auto-89>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-90>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-91>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-92>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-93>>

      <tuple|<tuple|Heating|curve, of a calorimeter>|<pageref|auto-94>>

      <tuple|<tuple|calorimeter|isothermal-jacket>||c7 sec tp-heatcap-ijc
      idx1|<tuple|Calorimeter|isothermal\Ujacket>|<pageref|auto-95>>

      <tuple|<tuple|calorimeter|continuous-flow>||c7 sec tp-heatcap-cfc
      idx1|<tuple|Calorimeter|continuous\Uflow>|<pageref|auto-97>>

      <tuple|<tuple|Electrical|heating>|<pageref|auto-98>>

      <tuple|<tuple|Heating|electrical>|<pageref|auto-99>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-100>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-101>>

      <tuple|<tuple|Electric|power>|<pageref|auto-102>>

      <tuple|<tuple|calorimeter|continuous-flow>||c7 sec tp-heatcap-cfc
      idx1|<tuple|Calorimeter|continuous\Uflow>|<pageref|auto-103>>

      <tuple|<tuple|heating|constant volume>||c7 sec hcvp
      idx1|<tuple|Heating|at constant volume or pressure>|<pageref|auto-107>>

      <tuple|<tuple|Molar|quantity>|<pageref|auto-108>>

      <tuple|<tuple|Property|molar>|<pageref|auto-109>>

      <tuple|<tuple|entropy|change|constant
      volume>|||<tuple|Entropy|change|at constant volume>|<pageref|auto-110>>

      <tuple|<tuple|Enthalpy|change at constant pressure>|<pageref|auto-111>>

      <tuple|<tuple|entropy|change|constant
      pressure>|||<tuple|Entropy|change|at constant
      pressure>|<pageref|auto-112>>

      <tuple|<tuple|heating|constant volume>||c7 sec hcvp
      idx1|<tuple|Heating|at constant volume or pressure>|<pageref|auto-113>>

      <tuple|<tuple|partial derivative|expressions at constant T, p, and
      V>||c7 sec pdtpv-tables idx1|<tuple|Partial derivative|expressions at
      constant <with|mode|<quote|math>|T>, <with|mode|<quote|math>|p>, and
      <with|mode|<quote|math>|V>>|<pageref|auto-116>>

      <tuple|<tuple|Bridgman, Percy>|<pageref|auto-120>>

      <tuple|<tuple|partial derivative|expressions at constant T, p, and
      V>||c7 sec pdtpv-tables idx1|<tuple|Partial derivative|expressions at
      constant <with|mode|<quote|math>|T>, <with|mode|<quote|math>|p>, and
      <with|mode|<quote|math>|V>>|<pageref|auto-121>>

      <tuple|<tuple|joule-thomson|coefficient>||c7 sec pdtpv-jt
      idx1|<tuple|Joule\UThomson|coefficient>|<pageref|auto-123>>

      <tuple|<tuple|joule-thomson|coefficient>||c7 sec pdtpv-jt
      idx1|<tuple|Joule\UThomson|coefficient>|<pageref|auto-124>>

      <tuple|<tuple|isothermal|pressure changes>||c7 sec ipc
      idx1|<tuple|Isothermal|pressure changes>|<pageref|auto-126>>

      <tuple|<tuple|pressure|changes>||idx2|<tuple|Pressure|changes,
      isothermal>|<pageref|auto-127>>

      <tuple|<tuple|isothermal|pressure changes|ideal
      gas>|||<tuple|Isothermal|pressure changes|of an ideal
      gas>|<pageref|auto-130>>

      <tuple|<tuple|pressure|changes, isothermal|ideal
      gas>|||<tuple|Pressure|changes, isothermal|of an ideal
      gas>|<pageref|auto-131>>

      <tuple|<tuple|isothermal|pressure changes|condensed
      phase>|||<tuple|Isothermal|pressure changes|of<space|1em>a condensed
      phase>|<pageref|auto-133>>

      <tuple|<tuple|pressure|changes, isothermal|condensed
      phase>|||<tuple|Pressure|changes, isothermal|of a condensed
      phase>|<pageref|auto-134>>

      <tuple|<tuple|isothermal|compressibility|liquid or
      solid>|||<tuple|Isothermal|compressibility|of a liquid or
      solid>|<pageref|auto-135>>

      <tuple|<tuple|isothermal|pressure changes>||c7 sec ipc
      idx1|<tuple|Isothermal|pressure changes>|<pageref|auto-136>>

      <tuple|<tuple|pressure|changes>||idx2|<tuple|Pressure|changes,
      isothermal>|<pageref|auto-137>>

      <tuple|<tuple|Standard|pressure>|<pageref|auto-139>>

      <tuple|<tuple|Pressure|standard>|<pageref|auto-140>>

      <tuple|<tuple|Plimsoll mark>|<pageref|auto-142>>

      <tuple|<tuple|standard state|pure substance>|||<tuple|Standard state|of
      a pure substance>|<pageref|auto-143>>

      <tuple|<tuple|state|standard>|||<tuple|State|standard, see Standard
      state>|<pageref|auto-144>>

      <tuple|<tuple|IUPAC Green Book>|<pageref|auto-146>>

      <tuple|<tuple|standard state|gas>|||<tuple|Standard state|of a
      gas>|<pageref|auto-147>>

      <tuple|<tuple|standard state|pure liquid or solid>|||<tuple|Standard
      state|of a pure liquid o rsolid>|<pageref|auto-148>>

      <tuple|<tuple|chemical potential|pure substance>|||<tuple|Chemical
      potential|of a pure substance>|<pageref|auto-150>>

      <tuple|<tuple|Gibbs energy|molar>|<pageref|auto-152>>

      <tuple|<tuple|total differential|gibbs energy of a pure
      substance>|||<tuple|Total differential|of the Gibbs energy of a pure
      bustance>|<pageref|auto-153>>

      <tuple|<tuple|chemical potential|standard|pure
      substance>|||<tuple|Chemical potential|standard|of a pure
      substance>|<pageref|auto-154>>

      <tuple|<tuple|standard|chemical potential>|||<tuple|Standard|chemical
      potential, see Chemical potential, standard>|<pageref|auto-155>>

      <tuple|<tuple|Einstein energy relation>|<pageref|auto-157>>

      <tuple|<tuple|chemical potential|standard|gas>|||<tuple|Chemical
      potential|standard|of a gas>|<pageref|auto-159>>

      <tuple|<tuple|fugacity|gas>|||<tuple|Fugacity|of a
      gas>|<pageref|auto-161>>

      <tuple|<tuple|fugacity coefficient|gas>|||<tuple|Fugacity
      coefficient|of a gas>|<pageref|auto-163>>

      <tuple|<tuple|activity coefficient|gas>|||<tuple|Activity
      coefficient|of a gas>|<pageref|auto-165>>

      <tuple|<tuple|activity|gas>|||<tuple|Activity|of a
      gas>|<pageref|auto-166>>

      <tuple|<tuple|chemical potential|liquid or solid>|||<tuple|Chemical
      potential|of a liquid or solid>|<pageref|auto-168>>

      <tuple|<tuple|standard molar|quantity|gas>||c7 sec smqg
      idx1|<tuple|Standard molar|quantity|of a gas>|<pageref|auto-170>>

      <tuple|<tuple|Standard molar|quantity>|<pageref|auto-171>>

      <tuple|<tuple|entropy|standard molar|gas>|||<tuple|Entropy|standard
      molar|of a gas>|<pageref|auto-174>>

      <tuple|<tuple|standard molar|quantity|gas>||c7 sec smqg
      idx1|<tuple|Standard molar|quantity|of a gas>|<pageref|auto-175>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.5.1>|>
        <with|font-shape|<quote|italic>|Constant temperature>: expressions
        for partial derivatives of state functions with respect to pressure
        and volume in a closed, single-phase system
      </surround>|<pageref|auto-117>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.5.2>|>
        <with|font-shape|<quote|italic>|Constant pressure>: expressions for
        partial derivatives of state functions with respect to temperature
        and volume in a closed, single-phase system
      </surround>|<pageref|auto-118>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.5.3>|>
        <with|font-shape|<quote|italic>|Constant volume>: expressions for
        partial derivatives of state functions with respect to temperature
        and pressure in a closed, single-phase system
      </surround>|<pageref|auto-119>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.6.1>|>
        Changes of state functions during an isothermal pressure change in a
        closed, single-phase system
      </surround>|<pageref|auto-128>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.9.1>|>
        Real gases: expressions for differences between molar properties and
        standard molar values at the same temperature
      </surround>|<pageref|auto-173>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|7<space|2spc>Pure
      Substances in Single Phases> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      7.1<space|2spc>Volume Properties <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      7.2<space|2spc>Internal Pressure <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>

      7.3<space|2spc>Thermal Properties <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-28>

      <with|par-left|<quote|1tab>|7.3.1<space|2spc>The relation between
      <with|mode|<quote|math>|C<rsub|V,<with|mode|<quote|text>|m>>> and
      <with|mode|<quote|math>|C<rsub|p,<with|mode|<quote|text>|m>>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-29>>

      <with|par-left|<quote|1tab>|7.3.2<space|2spc>The measurement of heat
      capacities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-33>>

      <with|par-left|<quote|2tab>|7.3.2.1<space|2spc>Adiabatic calorimeters
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-42>>

      <with|par-left|<quote|2tab>|7.3.2.2<space|2spc>Isothermal\Ujacket
      calorimeters <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-76>>

      <with|par-left|<quote|2tab>|7.3.2.3<space|2spc>Continuous\Uflow
      calorimeters <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-96>>

      <with|par-left|<quote|1tab>|7.3.3<space|2spc>Typical values
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-104>>

      7.4<space|2spc>Heating at Constant Volume or Pressure
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-106>

      7.5<space|2spc>Partial Derivatives with Respect to
      <with|mode|<quote|math>|T>, <with|mode|<quote|math>|p>, and
      <with|mode|<quote|math>|V> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-114>

      <with|par-left|<quote|1tab>|7.5.1<space|2spc>Tables of partial
      derivatives <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-115>>

      <with|par-left|<quote|1tab>|7.5.2<space|2spc>The Joule\UThomson
      coefficient <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-122>>

      7.6<space|2spc>Isothermal Pressure Changes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-125>

      <with|par-left|<quote|1tab>|7.6.1<space|2spc>Ideal gases
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-129>>

      <with|par-left|<quote|1tab>|7.6.2<space|2spc>Condensed phases
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-132>>

      7.7<space|2spc>Standard States of Pure Substances
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-138>

      7.8<space|2spc>Chemical Potential and Fugacity
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-149>

      <with|par-left|<quote|1tab>|7.8.1<space|2spc>Gases
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-158>>

      <with|par-left|<quote|1tab>|7.8.2<space|2spc>Liquids and solids
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-167>>

      7.9<space|2spc>Standard Molar Quantities of a Gas
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-169>
    </associate>
  </collection>
</auxiliary>