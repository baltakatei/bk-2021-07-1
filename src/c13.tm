<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  \;

  <chapter|The Phase Rule and Phase Diagrams><label|Chap. 13><label|c13>

  We encountered the Gibbs phase rule and phase diagrams in Chap. 8 in
  connection with single-substance systems. The present chapter derives the
  full version of the Gibbs phase rule for multicomponent systems. It then
  discusses phase diagrams for some representative types of multicomponent
  systems, and shows how they are related to the phase rule and to
  equilibrium concepts developed in Chaps. 11 and 12.

  <section|The Gibbs Phase Rule for Multicomponent Systems><label|13-phase
  rule><label|c13 sec-gprms>

  <index-complex|<tuple|gibbs|phase rule|multicomponent>||c13 sec-gprms
  idx1|<tuple|Gibbs|phase rule|for a multicomponent system>>In Sec.
  <reference|8-Gibbs phase rule>, the Gibbs phase rule for a pure substance
  was written <math|F=3-P>. We now consider a system of more than one
  substance and more than one phase in an equilibrium state. The phase rule
  assumes the system is at thermal and mechanical equilibrium. We shall
  assume furthermore that in addition to the temperature and pressure, the
  only other state functions needed to describe the state are the amounts of
  the species in each phase; this means for instance that surface effects are
  ignored.

  The derivations to follow will show that the phase rule may be written
  either in the form

  <\equation>
    <label|F=2+C-P>F=2+C-P
  </equation>

  or

  <\equation>
    <label|F=2+s-r-P>F=2+s-r-P
  </equation>

  where the symbols have the following meanings:

  \;

  <\description-aligned>
    <item*|<math|F>=>the number of degrees of freedom (or variance)

    <item*|<phantom|<math|F>> =>the maximum number of intensive variables
    that can be varied independently while the system remains in an
    equilibrium state;

    <item*|<math|C> =>the number of components

    <item*|<phantom|<math|C>> =>the minimum number of substances (or
    fixed-composition mixtures of substances) that could be used to prepare
    each phase individually;

    <item*|<math|P> =>the number of different phases;

    <item*|<math|s> =>the number of different species;

    <item*|<math|r> =>the number of independent relations among intensive
    variables of individual phases other than relations needed for thermal,
    mechanical, and transfer equilibrium.
  </description-aligned>

  <no-indent>If we subdivide a phase, that does not change the number of
  phases <math|P>. That is, we treat noncontiguous regions of the system that
  have identical intensive properties as parts of the same phase.

  <subsection|Degrees of Freedom><label|c13 sec gprms-degrees-of-freedom>

  Consider a system in an equilibrium state. In this state, the system has
  one or more phases; each phase contains one or more species; and intensive
  properties such as <math|T>, <math|p>, and the mole fraction of a species
  in a phase have definite values. Starting with the system in this state, we
  can make changes that place the system in a new equilibrium state having
  the same kinds of phases and the same species, but different values of some
  of the intensive properties. The number of different independent intensive
  variables that we may change in this way is the <index|Degrees of
  freedom><newterm|number of degrees of freedom> or
  <index|Variance><newterm|variance>, <math|F>, of the system.

  Clearly, the system remains in equilibrium if we change the <em|amount> of
  a phase without changing its temperature, pressure, or composition. This,
  however, is the change of an extensive variable and is not counted as a
  degree of freedom.

  The phase rule, in the form to be derived, applies to a system that
  continues to have complete thermal, mechanical, and transfer equilibrium as
  intensive variables change. This means different phases are not separated
  by adiabatic or rigid partitions, or by semipermeable or impermeable
  membranes. Furthermore, every conceivable reaction among the species is
  either at reaction equilibrium or else is frozen at a fixed advancement
  during the time period we observe the system.

  The number of degrees of freedom is the maximum number of intensive
  properties of the equilibrium system we may independently vary, or fix at
  arbitrary values, without causing a change in the number and kinds of
  phases and species. We cannot, of course, change one of these properties to
  just any value whatever. We are able to vary the value only within a
  certain finite (sometimes quite narrow) range before a phase disappears or
  a new one appears.

  The number of degrees of freedom is also the number of independent
  intensive variables needed to specify the equilibrium state in all
  necessary completeness, aside from the amount of each phase. In other
  words, when we specify values of <math|F> different independent intensive
  variables, then the values of all other intensive variables of the
  equilibrium state have definite values determined by the physical nature of
  the system.

  Just as for a one-component system, we can use the terms <index|Bivariant
  system><em|bivariant>, <index|Univariant system><em|univariant>, and
  <index|Invariant system><em|invariant> depending on the value of <math|F>
  (Sec. <reference|8-Gibbs phase rule>).

  <subsection|Species approach to the phase rule><label|13-species
  approach><label|c13 sec-gprms-species>

  This section derives an expression for the number of degrees of freedom,
  <math|F>, based on <em|species>. Section <reference|13-components approach>
  derives an expression based on <em|components>. Both approaches yield
  equivalent versions of the phase rule.

  Recall that a <index|Species><em|species> is an entity, uncharged or
  charged, distinguished from other species by its chemical formula (Sec.
  <reference|9-species \ substances>). Thus, CO<rsub|<math|2>> and
  <chem|<text|CO<rsub|3>>|2-> are different species, but
  <math|CO<rsub|<math|2>> (<text|aq>)> and <math|CO<rsub|<math|2>>
  (<text|g>)> is the same species in different phases.

  Consider an equilibrium system of <math|P> phases, each of which contains
  the same set of species. Let the number of different species be <math|s>.
  If we could make changes while the system remains in thermal and mechanical
  equilibrium, but not necessarily in transfer equilibrium, we could
  independently vary the temperature and pressure of the system as a whole
  and the amount of each species in each phase; there would then be
  <math|2+P*s> independent variables.

  The equilibrium system is, however, in transfer equilibrium, which requires
  each species to have the same chemical potential in each phase:
  <math|\<mu\><rsub|i><bph>=\<mu\><rsub|i><aph>>,
  <math|\<mu\><rsub|i><gph>=\<mu\><rsub|i><aph>>, and so on. There are
  <math|P-1> independent relations like this for each species, and a total of
  <math|s*<around|(|P-1|)>> independent relations for all species. Each such
  independent relation introduces a constraint and reduces the number of
  independent variables by one. Accordingly, taking transfer equilibrium into
  account, the <subindex|Independent variables|number of>number of
  independent variables is <math|2+P*s-s*<around|(|P-1|)>=2+s>.

  We obtain the same result if a species present in one phase is totally
  excluded from another. For example, solvent molecules of a solution are not
  found in a pure perfectly-ordered crystal of the solute, undissociated
  molecules of a volatile strong acid such as HCl can exist in a gas phase
  but not in aqueous solution, and ions of an electrolyte solute are usually
  not found in a gas phase. For each such species absent from a phase, there
  is one fewer amount variable and also one fewer relation for transfer
  equilibrium; on balance, the number of independent variables is still
  <math|2+s>.

  Next, we consider the possibility that further independent relations exist
  among intensive variables in addition to the relations needed for thermal,
  mechanical, and transfer equilibrium.<footnote|Relations such as
  <math|<big|sum><rsub|i>p<rsub|i>=p> for a gas phase or
  <math|<big|sum><rsub|i>x<rsub|i>=1> for a phase in general have already
  been accounted for in the derivation by the specification of <math|p> and
  the amount of each species.> If there are <math|r> of these additional
  relations, the total number of independent variables is reduced to
  <math|2+s-r>. These relations may come from

  <\enumerate>
    <item>reaction equilibria,

    <item>the requirement of electroneutrality in a phase containing ions,
    and

    <item>initial conditions determined by the way the system is prepared.
  </enumerate>

  In the case of a reaction equilibrium, the relation is
  <math|\<Delta\><rsub|<text|r>>*G=<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>=0>,
  or the equivalent relation <math|K=<big|prod><rsub|i><around|(|a<rsub|i>|)><rsup|\<nu\><rsub|i>>>
  for the thermodynamic equilibrium constant. Thus, <math|r> is the sum of
  the number of independent reaction equilibria, the number of phases
  containing ions, and the number of independent initial conditions. Several
  examples will be given in Sec. <reference|13-examples>.

  There is an infinite variety of possible choices of the independent
  variables (both extensive and intensive) for the equilibrium system, but
  the total <em|number> of independent variables is fixed at <math|2+s-r>.
  Keeping intensive properties fixed, we can always vary how much of each
  phase is present (e.g., its volume, mass, or amount) without destroying the
  equilibrium. Thus, at least <math|P> of the independent variables, one for
  each phase, must be extensive. It follows that the maximum number of
  independent <em|intensive> variables is the difference
  <math|<around|(|2+s-r|)>-P>.

  <\quote-env>
    \ It may be that initial conditions establish relations among the amounts
    of phases, as will be illustrated in example 2 on page <pageref|example
    2>. If present, these are relations among <em|extensive> variables that
    are not counted in <math|r>. Each such independent relation decreases the
    total number of independent variables without changing the number of
    independent intensive variables calculated from
    <math|<around|(|2+s-r|)>-P>.
  </quote-env>

  Since the maximum number of independent intensive variables is the number
  of degrees of freedom, our expression for <math|F> based on species is

  <\equation>
    F=2+s-r-P
  </equation>

  <subsection|Components approach to the phase rule><label|13-components
  approach><label|c13 sec-gprms-components>

  The derivation of the phase rule in this section uses the concept of
  <index|Component><newterm|components>. The number of components, <math|C>,
  is the minimum number of substances or mixtures of fixed composition from
  which we could in principle prepare each individual phase of an equilibrium
  state of the system, using methods that may be hypothetical. These methods
  include the addition or removal of one or more of the substances or
  fixed-composition mixtures, and the conversion of some of the substances
  into others by means of a reaction that is at equilibrium in the actual
  system.

  It is not always easy to decide on the number of components of an
  equilibrium system. The number of components may be less than the number of
  substances present, on account of the existence of reaction equilibria that
  produce some substances from others. When we use a reaction to prepare a
  phase, nothing must remain unused. For instance, consider a system
  consisting of solid phases of CaCO<rsub|<math|3>> and CaO and a gas phase
  of CO<rsub|<math|2>>. Assume the reaction <math|<text|CaCO<rsub|<math|3>>>
  <around*|(|<text|s>|)><ra><text|CaO> <around*|(|<text|s>|)>+<text|CO<rsub|2>>
  <around*|(|<text|g>|)>> is at equilibrium. We could prepare the
  CaCO<rsub|<math|3>> phase from CaO and CO<rsub|<math|2>> by the reverse of
  this reaction, but we can only prepare the CaO and CO<rsub|<math|2>> phases
  from the individual substances. We could not use CaCO<rsub|<math|3>> to
  prepare either the CaO phase or the CO<rsub|<math|2>> phase, because
  CO<rsub|<math|2>> or CaO would be left over. Thus this system has three
  substances but only two components, namely CaO and CO<rsub|<math|2>>.

  In deriving the phase rule by the components approach, it is convenient to
  consider only intensive variables. Suppose we have a system of <math|P>
  phases in which each substance present is a component (i.e., there are no
  reactions) and each of the <math|C> components is present in each phase. If
  we make changes to the system while it remains in thermal and mechanical
  equilibrium, but not necessarily in transfer equilibrium, we can
  independently vary the temperature and pressure of the whole system, and
  for each phase we can independently vary the mole fraction of all but one
  of the substances (the value of the omitted mole fraction comes from the
  relation <math|<big|sum><rsub|i>x<rsub|i>=1>). This is a total of
  <math|2+P*<around|(|C-1|)>> independent intensive variables.

  When there also exist transfer and reaction equilibria, not all of these
  variables are independent. Each substance in the system is either a
  component, or else can be formed from components by a reaction that is in
  reaction equilibrium in the system. Transfer equilibria establish
  <math|P-1> independent relations for each component
  (<math|\<mu\><rsub|i><bph>=\<mu\><rsub|i><aph>>,
  <math|\<mu\><rsub|i><gph>=\<mu\><rsub|i><aph>>, etc.) and a total of
  <math|C*<around|(|P-1|)>> relations for all components. Since these are
  relations among chemical potentials, which are intensive properties, each
  relation reduces the number of independent intensive variables by one. The
  resulting number of independent intensive variables is

  <\equation>
    F=<around|[|2+P*<around|(|C-1|)>|]>-C*<around|(|P-1|)>=2+C-P
  </equation>

  If the equilibrium system lacks a particular component in one phase, there
  is one fewer mole fraction variable and one fewer relation for transfer
  equilibrium. These changes cancel in the calculation of <math|F>, which is
  still equal to <math|2+C-P>. If a phase contains a substance that is formed
  from components by a reaction, there is an additional mole fraction
  variable and also the additional relation
  <math|<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i>=0> for the reaction;
  again the changes cancel.

  <\quote-env>
    \ We may need to <em|remove> a component from a phase to achieve the
    final composition. Note that it is not necessary to consider additional
    relations for electroneutrality or initial conditions; they are implicit
    in the definitions of the components. For instance, since each component
    is a substance of zero electric charge, the
    <subindex|Electrical|neutrality><index|Neutrality, electrical>electrical
    neutrality of the phase is assured.
  </quote-env>

  We conclude that, regardless of the kind of system, the expression for
  <math|F> based on components is given by <math|F=2+C-P>. By comparing this
  expression and <math|F=2+s-r-P>, we see that the number of components is
  related to the number of species by

  <\equation>
    <label|C=s-r>C=s-r
  </equation>

  <subsection|Examples><label|13-examples><label|c13 sec gprms-examples>

  <subsubsection|Example 1: liquid water>

  For a single phase of pure water, <math|P> equals <math|1>. If we treat the
  water as the single species H<rsub|<math|2>>O, <math|s> is 1 and <math|r>
  is 0. The phase rule then predicts two degrees of freedom:

  <\eqnarray*>
    <tformat|<table|<row|<cell|F>|<cell|=>|<cell|2+s-r-P>>|<row|<cell|>|<cell|=>|<cell|2+1-0-1=2<eq-number>>>>>
  </eqnarray*>

  Since <math|F> is the number of intensive variables that can be varied
  independently, we could for instance vary <math|T> and <math|p>
  independently, or <math|T> and <math|\<rho\>>, or any other pair of
  independent intensive variables.

  Next let us take into account the proton transfer equilibrium

  <\equation*>
    2*<text|H<rsub|2>O> <around*|(|<text|l>|)><arrows><chem|<text|H<rsub|3>O>|+>
    <around*|(|<text|aq>|)>+<chem|<text|OH>|-> <around*|(|<text|aq>|)>
  </equation*>

  and consider the system to contain the three species H<rsub|<math|2>>O,
  <chem|<text|H<rsub|3>O>|+>, and <chem|<text|OH>|->. Then for the species
  approach to the phase rule, we have <math|s=3>. We can write two
  independent relations:

  <\enumerate>
    <item>for reaction equilibrium, <math|-2*\<mu\><rsub|<text|H<rsub|2>O>>+\<mu\><rsub|<chem|<text|H<rsub|3>O>|+>>+\<mu\><rsub|<chem|<text|OH>|->>=0>;

    <item>for electroneutrality, <math|m<rsub|<chem|<text|H<rsub|3>O>|+>>=m<rsub|<chem|<text|OH>|->>>.
  </enumerate>

  Thus, we have two relations involving intensive variables only. Now
  <math|s> is 3, <math|r> is 2, <math|P> is 1, and the number of degrees of
  freedom is given by

  <\equation>
    F=2+s-r-P=2
  </equation>

  which is the same value of <math|F> as before.

  If we consider water to contain additional cation species (e.g.,
  <math|<text|H<rsub|5>O<rsub|2>><rsupout|+>>), each such species would add
  <math|1> to <math|s> and <math|1> to <math|r>, but <math|F> would remain
  equal to 2. Thus, no matter how complicated are the equilibria that
  actually exist in liquid water, the number of degrees of freedom remains
  <math|2>.

  Applying the components approach to water is simple. All species that may
  exist in pure water are formed, in whatever proportions actually exist,
  from the single substance H<rsub|<math|2>>O. Thus, there is only one
  component: <math|C=1>. The component version of the phase rule,
  <math|F=2+C-P>, gives the same result as the species version: <math|F=2>.

  <subsubsection|Example 2: carbon, oxygen, and carbon oxides>

  Consider a system containing solid carbon (graphite) and a gaseous mixture
  of O<rsub|<math|2>>, CO, and CO<rsub|<math|2>>. There are four species and
  two phases. If reaction equilibrium is absent, as might be the case at low
  temperature in the absence of a catalyst, we have <math|r=0> and
  <math|C=s-r=4>. The four components are the four substances. The phase rule
  tells us the system has four degrees of freedom. We could, for instance,
  arbitrarily vary <math|T>, <math|p>, <math|y<rsub|<text|O<rsub|2>>>>, and
  <math|y<rsub|<text|CO>>>.

  Now suppose we raise the temperature or introduce an appropriate catalyst
  to allow the following reaction equilibria to exist:

  <\enumerate>
    <item><math|2*<text|C> <around|(|<text|s>|)>+<text|O<rsub|2>>
    <around|(|<text|g>|)><arrows>2*<text|CO> <around|(|<text|g>|)>>

    <item><math|<text|C> <around|(|<text|s>|)>+<text|O<rsub|2>>
    <around|(|<text|g>|)><arrows><text|CO<rsub|2>> <around|(|<text|g>|)>>
  </enumerate>

  These equilibria introduce two new independent relations among chemical
  potentials and among activities. We could also consider the equilibrium
  <math|2*<text|CO> <around|(|<text|g>|)>+<text|O<rsub|2>>
  <around|(|<text|g>|)><arrows>2*<text|CO<rsub|2>> <around|(|<text|g>|)>>,
  but it does not contribute an additional independent relation because it
  depends on the other two equilibria: the reaction equation is obtained by
  subtracting the reaction equation for equilibrium 1 from twice the reaction
  equation for equilibrium 2. By the species approach, we have <math|s=4>,
  <math|r=2>, and <math|P=2>; the number of degrees of freedom from these
  values is

  <\equation>
    F=2+s-r-P=2
  </equation>

  If we wish to calculate <math|F> by the components approach, we must decide
  on the minimum number of substances we could use to prepare each phase
  separately. (This does not refer to how we actually prepare the two-phase
  system, but to a hypothetical preparation of each phase with any of the
  compositions that can actually exist in the equilibrium system.) Assume
  equilibria 1 and 2 are present. We prepare the solid phase with carbon, and
  we can prepare any possible equilibrium composition of the gas phase from
  carbon and O<rsub|<math|2>> by using the reactions of both equilibria.
  Thus, there are two components (C and O<rsub|<math|2>>) giving the same
  result of two degrees of freedom.

  What is the significance of there being two degrees of freedom when the
  reaction equilibria are present? There are two ways of viewing the
  situation:

  <\enumerate>
    <item>We can arbitrarily vary the two intensive variables <math|T> and
    <math|p>. When we do, the mole fractions of the three substances in the
    gas phase change in a way determined by equilibria 1 and 2.

    <item>If we specify arbitrary values of <math|T> and <math|p>, each of
    the mole fractions has only one possible value that will allow the two
    phases and four substances to be in equilibrium.
  </enumerate>

  Now to introduce an additional complexity:<label|example 2>Suppose we
  prepare the system by placing a certain amount of O<rsub|<math|2>> and
  twice this amount of carbon in an evacuated container, and wait for the
  reactions to come to equilibrium. This method of preparation imposes an
  initial condition on the system, and we must decide whether the number of
  degrees of freedom is affected. Equating the total amount of carbon atoms
  to the total amount of oxygen atoms in the equilibrated system gives the
  relation

  <\equation>
    n<rsub|<text|C>>+n<rsub|<text|CO>>+n<rsub|<text|CO<rsub|2>>>=2*n<rsub|<text|O<rsub|2>>>+n<rsub|<text|CO>>+2*n<rsub|<text|CO<rsub|2>>><space|2em><text|or><space|2em>n<rsub|<text|C>>=2*n<rsub|<text|O<rsub|2>>>+n<rsub|<text|CO<rsub|2>>>
  </equation>

  Either equation is a relation among extensive variables of the two phases.
  From them, we are unable to obtain any relation among <em|intensive>
  variables of the phases. Therefore, this particular initial condition does
  not change the value of <math|r>, and <math|F> remains equal to 2.

  <subsubsection|Example 3: a solid salt and saturated aqueous solution>

  In this example, the equilibrium system consists of crystalline
  PbCl<rsub|2> and an aqueous phase containing the species H<rsub|<math|2>>O,
  <math|<text|Pb><rsupout|2+> <around*|(|<text|aq>|)>>, and
  <math|<text|Cl><rsupout|-> <around*|(|<text|aq>|)>>.

  Applying the components approach to this system is straightforward. The
  solid phase is prepared from PbCl<rsub|<math|2>> and the aqueous phase
  could be prepared by dissolving solid PbCl<rsub|<math|2>> in
  H<rsub|<math|2>>O. Thus, there are two components and two phases:

  <\equation>
    F=2+C-P=2
  </equation>

  For the species approach, we note that there are four species
  (PbCl<rsub|<math|2>>, Pb<rsup|<math|2+>>, Cl<rsup|<math|->>, and
  H<rsub|<math|2>>O) and two independent relations among intensive variables:

  <\enumerate>
    <item>equilibrium for the dissolution process,
    <math|-\<mu\><rsub|<text|PbCl<rsub|2>>>+\<mu\><rsub|<text|Pb><rsupout|2+>>+2*\<mu\><rsub|<text|Cl><rsupout|->>=0>;

    <item>electroneutrality of the aqueous phase,
    <math|2*m<rsub|<text|Pb><rsupout|2+>>=m<rsub|<text|Cl><rsupout|->>>.
  </enumerate>

  We have <math|s=4>, <math|r=2>, and <math|P=2>, giving the same result as
  the components approach:

  <\equation>
    F=2+s-r-P=2
  </equation>

  <subsubsection|Example 4: liquid water and water-saturated air>

  For simplicity, let \Pair\Q be a gaseous mixture of N<rsub|<math|2>> and
  O<rsub|<math|2>>. The equilibrium system in this example has two phases:
  liquid water saturated with the dissolved constituents of air, and air
  saturated with gaseous H<rsub|<math|2>>O.

  If there is no special relation among the total amounts of N<rsub|<math|2>>
  and O<rsub|<math|2>>, there are three components and the phase rule gives

  <\equation>
    F=2+C-P=3
  </equation>

  Since there are three degrees of freedom, we could, for instance, specify
  arbitrary values<footnote|Arbitrary, that is, within the limits that would
  allow the two phases to coexist.> of <math|T>, <math|p>, and
  <math|y<rsub|<text|N<rsub|2>>>>; then the values of other intensive
  variables such as the mole fractions <math|y<rsub|<text|H<rsub|2>O>>> and
  <math|x<rsub|<text|N<rsub|2>>>> would have definite values.

  Now suppose we impose an initial condition by preparing the system with
  water and dry air of a <em|fixed> composition. The mole ratio of
  N<rsub|<math|2>> and O<rsub|<math|2>> in the aqueous solution is not
  necessarily the same as in the equilibrated gas phase; consequently, the
  air does not behave like a single substance. The number of components is
  still three: H<rsub|<math|2>>O, N<rsub|<math|2>>, and O<rsub|<math|2>> are
  all required to prepare each phase individually, just as when there was no
  initial condition, giving <math|F=3> as before.<footnote|The fact that the
  compositions of both phases depend on the relative amounts of the phases is
  illustrated in Prob. <reference|prb:9-air>.>

  We can reach the same conclusion with the species approach. The initial
  condition can be expressed by an equation such as

  <\equation>
    <label|init cond><frac|<around|(|n<rsub|<text|N<rsub|2>>><rsup|<text|l>>+n<rsub|<text|N<rsub|2>>><rsup|<text|g>>|)>|<around|(|n<rsub|<text|O<rsub|2>>><rsup|<text|l>>+n<rsub|<text|O<rsub|2>>><rsup|<text|g>>|)>>=a
  </equation>

  where <math|a> is a constant equal to the mole ratio of N<rsub|<math|2>>
  and O<rsub|<math|2>> in the dry air. This equation cannot be changed to a
  relation between intensive variables such as
  <math|x<rsub|<text|N<rsub|2>>>> and <math|x<rsub|<text|O<rsub|2>>>>, so
  that <math|r> is zero and there are still three degrees of freedom.

  Finally, let us assume that we prepare the system with dry air of fixed
  composition, as before, but consider the solubilities of N<rsub|<math|2>>
  and O<rsub|<math|2>> in water to be negligible. Then
  <math|n<rsub|<text|N<rsub|2>>><rsup|<text|l>>> and
  <math|n<rsub|<text|O<rsub|2>>><rsup|<text|l>>> are zero and Eq.
  <reference|init cond> becomes <math|n<rsub|<text|N<rsub|2>>><rsup|<text|g>>/n<rsub|<text|O<rsub|2>>><rsup|<text|g>>=a>,
  or <math|y<rsub|<text|N<rsub|2>>>=a*y<rsub|<text|O<rsub|2>>>>, which is a
  relation between intensive variables. In this case, <math|r> is 1 and the
  phase rule becomes

  <\equation>
    F=2+s-r-P=2
  </equation>

  The reduction in the value of <math|F> from 3 to 2 is a consequence of our
  inability to detect any dissolved N<rsub|<math|2>> or O<rsub|<math|2>>.
  According to the components approach, we may prepare the liquid phase with
  H<rsub|<math|2>>O and the gas phase with H<rsub|<math|2>>O and air of fixed
  composition that behaves as a single substance; thus, there are only two
  components.

  <subsubsection|Example 5: equilibrium between two solid phases and a gas
  phase>

  Consider the following reaction equilibrium:

  <\equation*>
    3*<text|CuO> <around|(|<text|s>|)>+2*<text|NH<rsub|3>>
    <around|(|<text|g>|)><arrows>3*<text|Cu>
    <around|(|<text|s>|)>+3*<text|H<rsub|2>O>
    <around|(|<text|g>|)>+<text|N<rsub|2>><around|(|<text|g>|)>
  </equation*>

  According to the species approach, there are five species, one relation
  (for reaction equilibrium), and three phases. The phase rule gives

  <\equation>
    F=2+s-r-P=3
  </equation>

  It is more difficult to apply the components approach to this example. As
  components, we might choose CuO and Cu (from which we could prepare the
  solid phases) and also NH<rsub|<math|3>> and H<rsub|<math|2>>O. Then to
  obtain the N<rsub|<math|2>> needed to prepare the gas phase, we could use
  CuO and NH<rsub|<math|3>> as reactants in the reaction
  <math|3*<text|CuO>+2*<text|NH<rsub|3>><arrow>3*<text|Cu>+3*<text|H<rsub|2>O>+<text|N<rsub|2>>>
  and remove the products Cu and H<rsub|<math|2>>O. In the components
  approach, we are allowed to remove substances from the system provided they
  are counted as components.<index-complex|<tuple|gibbs|phase
  rule|multicomponent>||c13 sec-gprms idx1|<tuple|Gibbs|phase rule|for a
  multicomponent system>>

  <section|Phase Diagrams: Binary Systems><label|c13 sec pdbs>

  As explained in Sec. <reference|8-phase diagrams>, a phase diagram is a
  kind of two-dimensional map that shows which phase or phases are stable
  under a given set of conditions. This section discusses some common kinds
  of binary systems, and Sec. <reference|13-ternary> will describe some
  interesting ternary systems.

  <subsection|Generalities><label|13-binary general><label|c13 sec
  pdbs-generalities>

  A binary system has two components; <math|C> equals <math|2>, and the
  number of degrees of freedom is <math|F=4-P>. There must be at least one
  phase, so the maximum possible value of <math|F> is 3. Since <math|F>
  cannot be negative, the equilibrium system can have no more than four
  phases.

  We can independently vary the temperature, pressure, and composition of the
  system as a whole. Instead of using these variables as the coordinates of a
  three-dimensional <index-complex|<tuple|phase diagram|binary
  system>|||<tuple|Phase diagram|for a binary system>>phase diagram, we
  usually draw a two-dimensional phase diagram that is either a
  temperature\Ucomposition diagram at a fixed pressure or a
  pressure\Ucomposition diagram at a fixed temperature. The position of the
  system point on one of these diagrams then corresponds to a definite
  temperature, pressure, and overall composition. The composition variable
  usually varies along the horizontal axis and can be the mole fraction, mass
  fraction, or mass percent of one of the components, as will presently be
  illustrated by various examples.

  The way in which we interpret a two-dimensional phase diagram to obtain the
  compositions of individual phases depends on the number of phases present
  in the system.

  <\itemize>
    <item>If the system point falls within a <em|one-phase> area of the phase
    diagram, the composition variable is the composition of that single
    phase. There are three degrees of freedom. On the phase diagram, the
    value of either <math|T> or <math|p> has been fixed, so there are two
    other independent intensive variables. For example, on a
    temperature\Ucomposition phase diagram, the pressure is fixed and the
    temperature and composition can be changed independently within the
    boundaries of the one-phase area of the diagram.

    <item>If the system point is in a <em|two-phase> area of the phase
    diagram, we draw a horizontal <index-complex|<tuple|tie
    line|binary>|||<tuple|Tie line|on a binary phase diagram>><em|tie line>
    of constant temperature (on a temperature\Ucomposition phase diagram) or
    constant pressure (on a pressure\Ucomposition phase diagram). The
    <index|Lever rule>lever rule applies. The position of the point at each
    end of the tie line, at the boundary of the two-phase area, gives the
    value of the composition variable of one of the phases and also the
    physical state of this phase: either the state of an adjacent one-phase
    area, or the state of a phase of fixed composition when the boundary is a
    vertical line. Thus, a boundary that separates a two-phase area for
    phases <math|<pha>> and <math|<phb>> from a one-phase area for phase
    <math|<pha>> is a curve that describes the composition of phase
    <math|<pha>> as a function of <math|T> or <math|p> when it is in
    equilibrium with phase <math|<phb>>. The curve is called a <index|Solidus
    curve for a binary system><em|solidus>, <index-complex|<tuple|liquidus
    curve|binary>|||<tuple|Liquidus curve|for a binary system>><em|liquidus>,
    or <index-complex|<tuple|vaporus curve|binary>|||<tuple|Vaporus curve|for
    a binary system>><em|vaporus> depending on whether phase <math|<pha>> is
    a solid, liquid, or gas.

    <item>A binary system with <em|three> phases has only one degree of
    freedom and cannot be represented by an area on a two-dimensional phase
    diagram. Instead, there is a horizontal boundary line between areas, with
    a special point along the line at the junction of several areas. The
    compositions of the three phases are given by the positions of this point
    and the points at the two ends of the line. The position of the system
    point on this line does not uniquely specify the relative amounts in the
    three phases.
  </itemize>

  The examples that follow show some of the simpler kinds of phase diagrams
  known for binary systems.

  <subsection|Solid\Uliquid systems><label|c13 sec pdbs-solid-liquid>

  Figure <vpageref|fig:13-solid-l><\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/SOL-LIQ.eps|197pt|165pt||>>
        Temperature--composition phase diagram for a binary system exhibiting
        a eutectic point.<label|fig:13-solid-l>
      </big-figure>
    </framed>
  </float> is a temperature\Ucomposition <index-complex|<tuple|phase
  diagram|binary solid liquid>|||<tuple|Phase diagram|for a binary
  solid\Uliquid system>>phase diagram at a fixed pressure. The composition
  variable <math|z<B>> is the mole fraction of component B in the system as a
  whole. The phases shown are a binary liquid mixture of A and B, pure solid
  A, and pure solid B.

  The one-phase liquid area is bounded by two curves, which we can think of
  either as <index-complex|<tuple|freezing point|curve|binary solid
  liquid>|||<tuple|Freezing point|curve|for a binary solid\Uliquid
  system>>freezing-point curves for the liquid or as
  <index-complex|<tuple|solubility|curve|binary solid
  liquid>|||<tuple|Solubility|curve|for a binary solid\Uliquid
  system>>solubility curves for the solids. These curves comprise the
  <index-complex|<tuple|liquidus curve|binary solid liquid>|||<tuple|Liquidus
  curve|for a binary solid\Uliquid system>>liquidus. As the mole fraction of
  either component in the liquid phase decreases from unity, the freezing
  point decreases. The curves meet at point a, which is a
  <subindex|Eutectic|point><newterm|eutectic point>. At this point, both
  solid A and solid B can coexist in equilibrium with a binary liquid
  mixture. The composition at this point is the
  <subindex|Eutectic|composition><em|eutectic composition>, and the
  temperature here (denoted <math|T<rsub|<text|e>>>) is the
  <subindex|Eutectic|temperature><em|eutectic temperature>.
  <math|T<rsub|<text|e>>> is the lowest temperature for the given pressure at
  which the liquid phase is stable.<footnote|\PEutectic\Q comes from the
  Greek for <em|easy melting>.>

  Suppose we combine <math|0.60<mol>> A and <math|0.40<mol>> B
  (<math|z<B>=0.40>) and adjust the temperature so as to put the system point
  at b. This point is in the one-phase liquid area, so the equilibrium system
  at this temperature has a single liquid phase. If we now place the system
  in thermal contact with a cold reservoir, heat is transferred out of the
  system and the system point moves down along the
  <index|Isopleth><em|isopleth> (path of constant overall composition) b\Uh.
  The cooling rate depends on the temperature gradient at the system boundary
  and the system's heat capacity.

  At point c on the isopleth, the system point reaches the boundary of the
  one-phase area and is about to enter the two-phase area labeled
  A(s)<space|0.17em>+<space|0.17em>liquid. At this point in the cooling
  process, the liquid is saturated with respect to solid A, and solid A is
  about to freeze out from the liquid. There is an abrupt decrease (break) in
  the cooling rate at this point, because the freezing process involves an
  extra enthalpy decrease.

  At the still lower temperature at point d, the system point is within the
  two-phase solid\Uliquid area. The tie line through this point is line e\Uf.
  The compositions of the two phases are given by the values of <math|z<B>>
  at the ends of the tie line: <math|x<B><rsup|<text|s>>=0> for the solid and
  <math|x<B><rsup|<text|l>>=0.50> for the liquid. From the
  <index-complex|<tuple|lever rule|binary>|||<tuple|Lever rule|for a binary
  phase diagram>>general lever rule (Eq. <vpageref|lever rule>), the ratio of
  the amounts in these phases is

  <\equation>
    <frac|n<rsup|<text|l>>|n<rsup|<text|s>>>=<frac|z<B>-x<B><rsup|<text|s>>|x<B><rsup|<text|l>>-z<B>>=<frac|0.40-0|0.50-0.40>=4.0
  </equation>

  Since the total amount is <math|n<rsup|<text|s>>+n<rsup|<text|l>>=1.00<mol>>,
  the amounts of the two phases must be <math|n<rsup|<text|s>>=0.20 <mol>>
  and <math|n<rsup|<text|l>>=0.80 <mol>>.

  When the system point reaches the <subindex|Eutectic|temperature>eutectic
  temperature at point g, cooling halts until all of the liquid freezes.
  Solid B freezes out as well as solid A. During this
  <subindex|Eutectic|halt><em|eutectic halt>, there are at first three
  phases: liquid with the eutectic composition, solid A, and solid B. As heat
  continues to be withdrawn from the system, the amount of liquid decreases
  and the amounts of the solids increase until finally only <math|0.60 <mol>>
  of solid A and <math|0.40 <mol>> of solid B are present. The temperature
  then begins to decrease again and the system point enters the two-phase
  area for solid A and solid B; tie lines in this area extend from
  <math|z<B|=>=0> to <math|z<B|=>=1>.

  Temperature\Ucomposition phase diagrams such as this are often mapped out
  experimentally by observing the cooling curve (temperature as a function of
  time) along isopleths of various compositions. This procedure is
  <subindex|Thermal|analysis><em|thermal analysis>. A break in the slope of a
  cooling curve at a particular temperature indicates the system point has
  moved from a one-phase liquid area to a two-phase area of liquid and solid.
  A temperature halt indicates the temperature is either the freezing point
  of the liquid to form a solid of the same composition, or else a
  <subindex|Eutectic|temperature>eutectic temperature.

  Figure <vpageref|fig:13-eutectics> <\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/EUTECTIC.eps|363pt|173pt||>>
        <label|fig:13-eutectics>Temperature--composition phase diagrams with
        single eutectics.

        <\enumerate-alpha>
          <item>Two pure solids and a liquid
          mixture.<note-ref|+1XmfVJNylVc696r>

          <item>Two solid solutions and a liquid mixture.
        </enumerate-alpha>

        <note-inline||+1XmfVJNylVc696r>Ref. <cite|ICT-4>, p. 98.
      </big-figure>
    </framed>
  </float> shows two temperature\Ucomposition phase diagrams with single
  eutectic points. The left-hand diagram is for the binary system of
  chloroform and carbon tetrachloride, two liquids that form nearly ideal
  mixtures. The solid phases are pure crystals, as in Fig.
  <reference|fig:13-solid-l>. The right-hand diagram is for the
  silver\Ucopper system and involves solid phases that are
  <subindex|Solution|solid>solid solutions (substitutional alloys of variable
  composition). The area labeled s<math|<aph>> is a solid solution that is
  mostly silver, and s<math|<bph>> is a solid solution that is mostly copper.
  Tie lines in the two-phase areas do not end at a vertical line for a pure
  solid component as they do in the system shown in the left-hand diagram.
  The three phases that can coexist at the eutectic temperature of
  <math|1\<comma\>052 <K>> are the melt of the eutectic composition and the
  two solid solutions.

  Section <reference|12-solid cmpds> discussed the possibility of the
  appearance of a <index|Solid compound><em|solid compound> when a binary
  liquid mixture is cooled. An example of this behavior is shown in Fig.
  <vpageref|fig:13-solid cmpd AB>,<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/ANAP-PH.eps|182pt|169pt||>>
        <label|fig:13-solid cmpd AB>Temperature--composition phase diagram
        for the binary system of <math|\<alpha\>>-naphthylamine (A) and
        phenol (B) at <math|1<br>> (Ref. <cite|philip-1903>).
      </big-figure>
    </framed>
  </float> in which the solid compound contains equal amounts of the two
  components <math|\<alpha\>>-naphthylamine and phenol. The possible solid
  phases are pure A, pure B, and the solid compound AB. Only one or two of
  these solids can be present simultaneously in an equilibrium state. The
  vertical line in the figure at <math|z<B>=0.5> represents the solid
  compound. The temperature at the upper end of this line is the melting
  point of the solid compound, <math|29 <degC>>. The solid melts
  <index|Congruent melting><em|congruently> to give a liquid of the same
  composition. A melting process with this behavior is called a <em|dystectic
  reaction>. The cooling curve for liquid of this composition would display a
  halt at the melting point.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/H2O-NACL.eps|273pt|256pt||>>
        <label|fig:13-H2O-NaCl>Temperature--composition phase diagram for the
        binary system of H<rsub|<math|2>>O and NaCl at <math|1<br>>. (Data
        from Refs. <cite|cohen-91> and <cite|ICT-3>.)
      </big-figure>
    </framed>
  </float>

  The phase diagram in Fig. <reference|fig:13-solid cmpd AB> has two
  <subindex|Eutectic|point>eutectic points. It resembles two simple phase
  diagrams like Fig. <reference|fig:13-solid-l> placed side by side. There is
  one important difference: the slope of the freezing-point curve
  <index|Liquidus curve>(liquidus curve) is nonzero at the composition of a
  pure component, but is zero at the composition of a solid compound that is
  completely dissociated in the liquid (as derived theoretically on
  page<nbsp><pageref|zero slope>). Thus, the curve in Fig.
  <reference|fig:13-solid cmpd AB> has a relative maximum at the composition
  of the solid compound (<math|z<B>=0.5>) and is rounded there, instead of
  having a cusp\Vlike a Romanesque arch rather than a Gothic arch.

  An example of a solid compound that does not melt congruently is shown in
  Fig. <vpageref|fig:13-H2O-NaCl>. The solid hydrate
  <math|<text|NaCl>\<cdot\>2*<text|H<rsub|2>O>> is 61.9% NaCl by mass. It
  decomposes at <math|0 <degC>> to form an aqueous solution of composition
  26.3% NaCl by mass and a solid phase of anhydrous NaCl. These three phases
  can coexist at equilibrium at <math|0 <degC>>. A phase transition like
  this, in which a solid compound changes into a liquid and a different
  solid, is called <em|incongruent> or <em|peritectic> melting, and the point
  on the phase diagram at this temperature at the composition of the liquid
  is a <index|Peritectic point><em|peritectic point>.

  Figure <reference|fig:13-H2O-NaCl> shows there are two other temperatures
  at which three phases can be present simultaneously: <math|-21 <degC>>,
  where the phases are ice, the solution at its
  <subindex|Eutectic|point>eutectic point, and the solid hydrate; and
  <math|109 <degC>>, where the phases are gaseous H<rsub|<math|2>>O, a
  solution of composition 28.3% NaCl by mass, and solid NaCl. Note that both
  segments of the right-hand boundary of the one-phase solution area have
  positive slopes, meaning that the solubilities of the solid hydrate and the
  anhydrous salt both increase with increasing temperature.

  <subsection|Partially-miscible liquids><label|13-l-l eqm><label|c13
  sec-pdbs-liq-partially-miscible>

  When two liquids that are partially miscible are combined in certain
  proportions, <subindex|Phase|separation of a liquid mixture>phase
  separation occurs (Sec. <reference|11-phase sep>). Two liquid phases in
  equilibrium with one another are called
  <index-complex|<tuple|conjugate|phases|binary
  system>|||<tuple|Conjugate|phases|in a binary system>><em|conjugate
  phases>. Obviously the two phases must have different compositions or they
  would be identical; the difference is called a
  <index-complex|<tuple|miscibility gap|binary system>|||<tuple|Miscibility
  gap|in a binary system>><em|miscibility gap>. A binary system with two
  phases has two degrees of freedom, so that at a given temperature and
  pressure each conjugate phase has a fixed composition.

  <index-complex|<tuple|phase diagram|binary liquid liquid>|||<tuple|Phase
  diagram|for a binary liquid\Uliquid system>>The typical dependence of a
  miscibility gap on temperature is shown in Fig.
  <vpageref|fig:13-liqliq>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/LIQ-LIQ.eps|151pt|140pt||>>
        <label|fig:13-liqliq>Temperature--composition phase diagram for the
        binary system of methyl acetate (A) and carbon disulfide (B) at
        <math|1<br>>.<note-ref|+1XmfVJNylVc696s> All phases are liquids. The
        open circle indicates the critical point.

        <note-inline||+1XmfVJNylVc696s>Data from Ref. <cite|ferloni-74>.
      </big-figure>
    </framed>
  </float> The <index-complex|<tuple|miscibility gap|binary
  system>|||<tuple|Miscibility gap|in a binary system>>miscibility gap (the
  difference in compositions at the left and right boundaries of the
  two-phase area) decreases as the temperature increases until at the
  <index|Upper consolute temperature><subindex|Temperature|upper
  consolute><em|upper consolute temperature>, also called the <index|Upper
  critical solution temperature><subindex|Temperature|upper critical
  solution><em|upper critical solution temperature>, the gap vanishes. The
  point at the maximum of the boundary curve of the two-phase area, where the
  temperature is the upper consolute temperature, is the <index|Consolute
  point><index-complex|<tuple|critical|point|partially>|||<tuple|Critical|point|of
  partially-miscible liquids>><em|consolute point> or <em|critical point>. At
  this point, the two liquid phases become identical, just as the liquid and
  gas phases become identical at the critical point of a pure substance.
  Critical opalescence (page <pageref|crit opal>) is observed in the vicinity
  of this point, caused by large local composition fluctuations. At
  temperatures at and above the critical point, the system is a single binary
  liquid mixture.

  Suppose we combine <math|6.0<mol>> of component A (methyl acetate) and
  <math|4.0<mol>> of component B (carbon disulfide) in a cylindrical vessel
  and adjust the temperature to <math|200<K>>. The overall mole fraction of B
  is <math|z<B>=0.40>. The system point is at point a in the two-phase
  region. From the positions of points b and c at the ends of the tie line
  through point a, we find the two liquid layers have compositions
  <math|x<B><aph>=0.20> and <math|x<B><bph>=0.92>. Since carbon disulfide is
  the more dense of the two pure liquids, the bottom layer is phase
  <math|<phb>>, the layer that is richer in carbon disulfide. According to
  the <index-complex|<tuple|lever rule|partially miscible>|||<tuple|Lever
  rule|for partially-miscible liquids>>lever rule, the ratio of the amounts
  in the two phases is given by

  <\equation>
    <frac|n<bph>|n<aph>>=<frac|z<B>-x<B><aph>|x<B><bph>-z<B>>=<frac|0.40-0.20|0.92-0.40>=0.38
  </equation>

  Combining this value with <math|n<aph>+n<bph>=10.0<mol>> gives us
  <math|n<aph>=7.2<mol>> and <math|n<bph>=2.8<mol>>.

  If we gradually add more carbon disulfide to the vessel while gently
  stirring and keeping the temperature constant, the system point moves to
  the right along the tie line. Since the ends of this tie line have fixed
  positions, neither phase changes its composition, but the amount of phase
  <math|<phb>> increases at the expense of phase <math|<pha>>. The
  liquid\Uliquid interface moves up in the vessel toward the top of the
  liquid column until, at overall composition <math|z<B>=0.92> (point c),
  there is only one liquid phase.

  Now suppose the system point is back at point a and we raise the
  temperature while keeping the overall composition constant at
  <math|z<B>=0.40>. The system point moves up the isopleth a\Ud. The phase
  diagram shows that the ratio <math|<around|(|z<B>-x<B><aph>|)>/<around|(|x<B><bph>-z<B>|)>>
  decreases during this change. As a result, the amount of phase <math|<pha>>
  increases, the amount of phase <math|<phb>> decreases, and the
  liquid\Uliquid interface moves down toward the bottom of the vessel until
  at <math|217<K>> (point d) there again is only one liquid phase.

  <subsection|Liquid\Ugas systems with ideal liquid
  mixtures><label|13-liq-gas ideal><label|c13 sec-pdbs-liq-gas-ideal>

  Toluene and benzene form liquid mixtures that are practically ideal and
  closely obey Raoult's law for partial pressure. For the binary system of
  these components, we can use the vapor pressures of the pure liquids to
  generate the <index-complex|<tuple|liquidus curve|binary liquid
  gas>|||<tuple|Liquidus curve|for a binary liquid\Ugas system>>liquidus and
  <index-complex|<tuple|vaporus curve|binary liquid gas>|||<tuple|Vaporus
  curve|for a binary liquid\Ugas system>>vaporus curves of the
  pressure\Ucomposition and temperature\Ucomposition
  <index-complex|<tuple|phase diagram|binary liquid gas>|||<tuple|Phase
  diagram|for a binary liquid\Ugas system>>phase diagram. The results are
  shown in Fig. <vpageref|fig:13-benztol>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/BENZTOL.eps|362pt|173pt||>>
        <label|fig:13-benztol>Phase diagrams for the binary system of toluene
        (A) and benzene (B). The curves are calculated from Eqs.
        <reference|xA(l)=> and <reference|xA(g)=> and the saturation vapor
        pressures of the pure liquids.

        <\enumerate-alpha>
          <item>Pressure\Ucomposition diagram at <math|T=340<K>>.

          <item>Temperature\Ucomposition diagram at <math|p=1<br>>.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float> The composition variable <math|z<A>> is the overall mole fraction
  of component A (toluene).

  The equations needed to generate the curves can be derived as follows.
  Consider a binary liquid mixture of components A and B and mole fraction
  composition <math|x<A>> that obeys <index-complex|<tuple|raoult's
  law|partial|binary>|||<tuple|Raoult's law|for partial pressure|in a binary
  system>>Raoult's law for partial pressure (Eq.
  <reference|p_i=(x_i)(p_i*)>):

  <\equation>
    <label|pA=xApA*,pB=(1-xA)pB*>p<A>=x<A>*p<A><rsup|\<ast\>><space|2em>p<B>=<around|(|1-x<A>|)>*p<B><rsup|\<ast\>>
  </equation>

  Strictly speaking, Raoult's law applies to a liquid\Ugas system maintained
  at a constant pressure by means of a third gaseous component, and
  <math|p<A><rsup|\<ast\>>> and <math|p<B><rsup|\<ast\>>> are the vapor
  pressures of the pure liquid components at this pressure and the
  temperature of the system. However, when a liquid phase is equilibrated
  with a gas phase, the partial pressure of a constituent of the liquid is
  practically independent of the total pressure (Sec. <reference|12-effect of
  p on fug>), so that it is a good approximation to apply the equations to a
  <em|binary> liquid\Ugas system and treat <math|p<A><rsup|\<ast\>>> and
  <math|p<B><rsup|\<ast\>>> as functions only of <math|T>.

  When the binary system contains a liquid phase and a gas phase in
  equilibrium, the pressure is the sum of <math|p<A>> and <math|p<B>>, which
  from Eq. <reference|pA=xApA*,pB=(1-xA)pB*> is given by

  <\eqnarray*>
    <tformat|<table|<row|<cell|p>|<cell|=>|<cell|x<A>*p<rsup|\<ast\>><A>+<around*|(|1-x<A>|)>*p<rsup|\<ast\>><B>>>|<row|<cell|>|<cell|=>|<cell|p<rsup|\<ast\>><B>+<around*|(|p<rsup|\<ast\>><A>-p<B><rsup|\<ast\>>|)>*x<A><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<text|(<math|C=2>,
    ideal liquid mixture)>>>>>><label|p=pB*+(pA*-pB*)xA(l)>>>>>
  </eqnarray*>

  where <math|x<A>> is the mole fraction of A in the liquid phase. Equation
  <reference|p=pB*+(pA*-pB*)xA(l)> shows that in the two-phase system,
  <math|p> has a value between <math|p<A><rsup|\<ast\>>> and
  <math|p<B><rsup|\<ast\>>>, and that if <math|T> is constant, <math|p> is a
  linear function of <math|x<A>>. The mole fraction composition of the gas in
  the two-phase system is given by

  <\equation>
    y<A>=<frac|p<A>|p>=<frac|x<A>*p<A><rsup|\<ast\>>|p<B><rsup|\<ast\>>+<around|(|p<A><rsup|\<ast\>>-p<B><rsup|\<ast\>>|)>*x<A>>
  </equation>

  A binary two-phase system has two degrees of freedom. At a given <math|T>
  and <math|p>, each phase must have a fixed composition. We can calculate
  the liquid composition by rearranging Eq. <reference|p=pB*+(pA*-pB*)xA(l)>:

  <equation-cov2|<label|xA(l)=>x<A>=<frac|p-p<B><rsup|\<ast\>>|p<A><rsup|\<ast\>>-p<B><rsup|\<ast\>>>|(<math|C=2>,
  ideal liquid mixture)>

  The gas composition is then given by

  <\eqnarray*>
    <tformat|<table|<row|<cell|y<A>>|<cell|=>|<cell|<frac|p<A>|p>=<frac|x<A>*p<A><rsup|\<ast\>>|p>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|p-p<B><rsup|\<ast\>>|p<A><rsup|\<ast\>>-p<B><rsup|\<ast\>>>|)>*<frac|p<A><rsup|\<ast\>>|p><htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|r>|<table|<row|<cell|<eq-number>>>|<row|<cell|<text|(<math|C=2>,
    ideal liquid mixture)>>>>>><label|xA(g)=>>>>>
  </eqnarray*>

  If we know <math|p<A><rsup|\<ast\>>> and <math|p<B><rsup|\<ast\>>> as
  functions of <math|T>, we can use Eqs. <reference|xA(l)=> and
  <reference|xA(g)=> to calculate the compositions for any combination of
  <math|T> and <math|p> at which the liquid and gas phases can coexist, and
  thus construct a pressure\Ucomposition or temperature\Ucomposition phase
  diagram.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/BENZTOL3.eps|237pt|265pt||>>
        <label|fig:13-zTp-benztol>Liquidus and vaporus surfaces for the
        binary system of toluene (A) and benzene. Cross-sections through the
        two-phase region are drawn at constant temperatures of <math|340<K>>
        and <math|370<K>> and at constant pressures of <math|1<br>> and
        <math|2<br>>. Two of the cross-sections intersect at a tie line at
        <math|T=370<K>> and <math|p=1<br>>, and the other cross-sections are
        hatched in the direction of the tie lines.
      </big-figure>
    </framed>
  </float>

  In Fig. <reference|fig:13-benztol>(a), the <index-complex|<tuple|liquidus
  curve|binary liquid gas>|||<tuple|Liquidus curve|for a binary liquid\Ugas
  system>>liquidus curve shows the relation between <math|p> and <math|x<A>>
  for equilibrated liquid and gas phases at constant <math|T>, and the
  vaporus curve shows the relation between <math|p> and <math|y<A>> under
  these conditions. We see that <math|p> is a linear function of <math|x<A>>
  but not of <math|y<A>>.

  In a similar fashion, the liquidus curve in Fig.
  <reference|fig:13-benztol>(b) shows the relation between <math|T> and
  <math|x<A>>, and the vaporus curve shows the relation between <math|T> and
  <math|y<A>>, for equilibrated liquid and gas phases at constant <math|p>.
  Neither curve is linear.

  A liquidus curve is also called a <index|Bubble-point
  curve><em|bubble-point> curve or a <subindex|Boiling
  point|curve><em|boiling-point> curve. Other names for a vaporus curve are
  <index|Dew-point curve><em|dew-point> curve and <index|Condensation
  curve><em|condensation> curve. These curves are actually cross-sections of
  liquidus and vaporus <em|surfaces> in a three-dimensional
  <math|T>\U<math|p>\U<math|z<A>> phase diagram, as shown in Fig.
  <vpageref|fig:13-zTp-benztol>. In this figure, the liquidus surface is in
  view at the front and the vaporus surface is hidden behind it.

  <subsection|Liquid\Ugas systems with nonideal liquid
  mixtures><label|13-liq-gas nonideal><label|c13 sec-pdbs-liq-gas-nonideal>

  Most binary liquid mixtures do not behave ideally. The most common
  situation is <subindex|Raoult's law|deviations from><em|positive>
  deviations from Raoult's law.<footnote|In the molecular model of Sec.
  <reference|11-mol model of id mixt>, positive deviations correspond to a
  less negative value of <math|k<rsub|<text|A>\<nocomma\><text|B>>> than the
  average of <math|k<rsub|<text|A>\<nocomma\><text|A>>> and
  <math|k<rsub|<text|B>\<nocomma\><text|B>>>.> Some mixtures, however, have
  specific A\UB interactions, such as solvation or molecular association,
  that prevent random mixing of the molecules of A and B, and the result is
  then <em|negative> deviations from Raoult's law. If the deviations from
  Raoult's law, either positive or negative, are large enough, the
  constant-temperature <index-complex|<tuple|liquidus curve|binary liquid
  gas>|||<tuple|Liquidus curve|for a binary liquid\Ugas system>>liquidus
  curve exhibits a maximum or minimum and <index|Azeotropic
  behavior><em|azeotropic> behavior results.

  Figure <vpageref|MeOH-benzene curves><\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/MEOHBENP.eps|315pt|183pt||>>
        <label|MeOH-benzene curves>Binary system of methanol (A) and benzene
        at <math|45 <degC>>.<note-ref|+1XmfVJNylVc696t>

        <\enumerate-alpha>
          <item>Partial pressures and total pressure in the gas phase
          equilibrated with liquid mixtures. The dashed lines indicate
          Raoult's law behavior.

          <item>Pressure\Ucomposition phase diagram at <math|45 <degC>>. Open
          circle: azeotropic point at <math|z<A>=0.59> and <math|p=60.5
          <text|kPa>>.
        </enumerate-alpha>

        <note-inline||+1XmfVJNylVc696t>Ref. <cite|toghiani-94>.
      </big-figure>
    </framed>
  </float> shows the azeotropic behavior of the binary methanol-benzene
  system at constant temperature. In Fig. <reference|MeOH-benzene curves>(a),
  the experimental partial pressures in a gas phase equilibrated with the
  nonideal liquid mixture are plotted as a function of the liquid
  composition. The partial pressures of both components exhibit positive
  deviations from Raoult's law,<footnote|This behavior is consistent with the
  statement in Sec. <reference|12-partial pressure over a liquid> that if one
  constituent of a binary liquid mixture exhibits positive deviations from
  Raoult's law, with only one inflection point in the curve of fugacity
  versus mole fraction, the other constituent also has positive deviations
  from Raoult's law.> and the total pressure (equal to the sum of the partial
  pressures) has a maximum value greater than the vapor pressure of either
  pure component. The curve of <math|p> versus <math|x<A>> becomes the
  <index-complex|<tuple|liquidus curve|binary liquid gas>|||<tuple|Liquidus
  curve|for a binary liquid\Ugas system>>liquidus curve of the
  pressure\Ucomposition phase diagram shown in Fig. <reference|MeOH-benzene
  curves>(b). Points on the <index-complex|<tuple|vaporus curve|binary liquid
  gas>|||<tuple|Vaporus curve|for a binary liquid\Ugas system>>vaporus curve
  are calculated from <math|p=p<A>/y<A>>.

  <\quote-env>
    \ In practice, the data needed to generate the liquidus and vaporus
    curves of a nonideal binary system are usually obtained by allowing
    liquid mixtures of various compositions to boil in an equilibrium still
    at a fixed temperature or pressure. When the liquid and gas phases have
    become equilibrated, samples of each are withdrawn for analysis. The
    partial pressures shown in Fig. <reference|MeOH-benzene curves>(a) were
    calculated from the experimental gas-phase compositions with the
    relations <math|p<A>=y<A>p> and <math|p<B>=p-p<A>>.
  </quote-env>

  <\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/MEOHBENZ.eps|181pt|225pt||>>
        Liquidus and vaporus surfaces for the binary system of methanol (A)
        and benzene.<note-ref|+1XmfVJNylVc696u> Cross-sections are hatched in
        the direction of the tie lines. The dashed curve is the azeotrope
        vapor-pressure curve.<label|fig:13-zTp-MeOHBenz>

        <note-inline||+1XmfVJNylVc696u>Ref. <cite|toghiani-94>.
      </big-figure>
    </framed>
  </float>If the constant-temperature <index-complex|<tuple|liquidus
  curve|binary liquid gas>|||<tuple|Liquidus curve|for a binary liquid\Ugas
  system>>liquidus curve has a maximum pressure at a liquid composition not
  corresponding to one of the pure components, which is the case for the
  methanol\Ubenzene system, then the liquid and gas phases are mixtures of
  identical compositions at this pressure. This behavior was deduced on page
  <pageref|binary azeotrope> at the end of Sec. <reference|12-Duhem-Margules
  eqn>. On the pressure\Ucomposition phase diagram, the liquidus and
  <index-complex|<tuple|vaporus curve|binary liquid gas>|||<tuple|Vaporus
  curve|for a binary liquid\Ugas system>>vaporus curves both have maxima at
  this pressure, and the two curves coincide at an <em|azeotropic point>. A
  binary system with negative deviations from Raoult's law can have an
  isothermal liquidus curve with a <em|minimum> pressure at a particular
  mixture composition, in which case the liquidus and vaporus curves coincide
  at an azeotropic point at this minimum. The general phenomenon in which
  equilibrated liquid and gas mixtures have identical compositions is called
  <index|Azeotropy><em|azeotropy>, and the liquid with this composition is an
  azeotropic mixture or <index|Azeotrope><newterm|azeotrope> (Greek:
  <em|boils unchanged>). An azeotropic mixture vaporizes as if it were a pure
  substance, undergoing an equilibrium phase transition to a gas of the same
  composition.

  If the <index-complex|<tuple|liquidus curve|binary liquid
  gas>|||<tuple|Liquius curve|for a binary liquid\Ugas system>>liquidus and
  <index-complex|<tuple|vaporus curve|binary liquid gas>|||<tuple|Vaporus
  curve|for a binary liquid\Ugas system>>vaporus curves exhibit a
  <em|maximum> on a pressure\Ucomposition phase diagram, then they exhibit a
  <em|minimum> on a temperature\Ucomposition phase diagram. This relation is
  explained for the methanol\Ubenzene system by the three-dimensional
  liquidus and vaporus surfaces drawn in Fig. <vpageref|fig:13-zTp-MeOHBenz>.
  In this diagram, the vaporus surface is hidden behind the liquidus surface.
  The hatched cross-section at the front of the figure is the same as the
  pressure\Ucomposition diagram of Fig. <reference|MeOH-benzene curves>(b),
  and the hatched cross-section at the top of the figure is a
  temperature\Ucomposition phase diagram in which the system exhibits a
  <subindex|Azeotrope|minimum-boiling><em|minimum-boiling azeotrope>.

  A binary system containing an azeotropic mixture in equilibrium with its
  vapor has two species, two phases, and one relation among intensive
  variables: <math|x<A>=y<A>>. The number of degrees of freedom is then
  <math|F=2+s-r-P=2+2-1-2=1>; the system is univariant. At a given
  temperature, the azeotrope can exist at only one pressure and have only one
  composition. As <math|T> changes, so do <math|p> and <math|z<A>> along an
  <subindex|Azeotrope|vapor-pressure curve><em|azeotrope vapor-pressure
  curve> as illustrated by the dashed curve in Fig.
  <reference|fig:13-zTp-MeOHBenz>.

  Figure <vpageref|fig:13-l-gas><\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/LG.eps|342pt|125pt||>>
        <label|fig:13-l-gas>Temperature\Ucomposition phase diagrams of binary
        systems exhibiting (a)<nbsp>no azeotropy, (b)<nbsp>a minimum-boiling
        azeotrope, and (c)<nbsp>a maximum-boiling azeotrope. Only the
        one-phase areas are labeled; two-phase areas are hatched in the
        direction of the tie lines.
      </big-figure>
    </framed>
  </float> summarizes the general appearance of some relatively simple
  temperature\Ucomposition phase diagrams of binary systems. If the system
  does not form an azeotrope <index|Zeotropic behavior>(<em|zeotropic>
  behavior), the equilibrated gas phase is richer in one component than the
  liquid phase at all liquid compositions, and the liquid mixture can be
  separated into its two components by fractional distillation. The gas in
  equilibrium with an azeotropic mixture, however, is not enriched in either
  component. Fractional distillation of a system with an azeotrope leads to
  separation into one pure component and the azeotropic mixture.

  More complicated behavior is shown in the phase diagrams of Fig.
  <reference|fig:13-l-l-gas>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/LLG.eps|343pt|125pt||>>
        <label|fig:13-l-l-gas>Temperature\Ucomposition phase diagrams of
        binary systems with partially-miscible liquids exhibiting
        (a)<nbsp>the ability to be separated into pure components by
        fractional distillation, (b)<nbsp>a minimum-boiling azeotrope, and
        (c)<nbsp>boiling at a lower temperature than the boiling point of
        either pure component. Only the one-phase areas are labeled;
        two-phase areas are hatched in the direction of the tie lines.
      </big-figure>
    </framed>
  </float> These are binary systems with partially-miscible liquids in which
  the boiling point is reached before an upper consolute temperature can be
  observed.

  <subsection|Solid\Ugas systems><label|c13 sec pdbs-solid-gas>

  <index-complex|<tuple|phase diagram|binary solid gas>|||<tuple|Phase
  diagram|for a binary solid\Ugas system>>As an example of a two-component
  system with equilibrated solid and gas phases, consider the components
  <math|<text|CuSO<rsub|4>>> and <math|<text|H<rsub|2>O>>, denoted A and B
  respectively. In the pressure\Ucomposition phase diagram shown in Fig.
  <vpageref|fig:13-CuSO4-water>, the composition variable <math|z<B>> is as
  usual the mole fraction of component B in the system as a whole.

  The anhydrous salt and its hydrates <index|Solid compound>(solid compounds)
  form the series of solids <math|<text|CuSO<rsub|4>>>,
  <math|<text|CuSO<rsub|4>>\<cdot\><text|H<rsub|2>O>>,
  <math|<text|CuSO<rsub|4>>\<cdot\>3*<text|H<rsub|2>O>>, and
  <math|<text|CuSO<rsub|4>>\<cdot\>5*<text|H<rsub|2>O>>. In the phase diagram
  these formulas are abbreviated A, AB, AB<rsub|<math|3>>, and
  AB<rsub|<math|5>>. The following dissociation equilibria (dehydration
  equilibria) are possible:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<text|CuSO<rsub|4>>\<cdot\><text|H<rsub|2>O>
    <around|(|<text|s>|)>>|<cell|<arrows>>|<cell|<text|CuSO<rsub|4>>
    <around|(|<text|s>|)>+<text|H<rsub|2>O>
    <around|(|<text|g>|)>>>|<row|<cell|<tfrac|1|2>**<text|CuSO<rsub|4>>\<cdot\>3*<text|H<rsub|2>O>
    <around|(|<text|s>|)>>|<cell|<arrows>>|<cell|<tfrac|1|2>*<text|CuSO<rsub|4>>\<cdot\><text|H<rsub|2>O>
    <around*|(|<text|s>|)>+<text|H<rsub|2>O>
    <around*|(|<text|g>|)>>>|<row|<cell|<tfrac|1|2>*<text|CuSO<rsub|4>>\<cdot\>5*<text|H<rsub|2>O>
    <around|(|<text|s>|)>>|<cell|<arrows>>|<cell|<tfrac|1|2>*<text|CuSO<rsub|4>>\<cdot\>3*<text|H<rsub|2>O>
    <around*|(|<text|s>|)>+<text|H<rsub|2>O> <around*|(|<text|g>|)>>>>>
  </eqnarray*>

  The equilibria are written above with coefficients that make the
  coefficient of H<rsub|<math|2>>O(g) unity. When one of these equilibria is
  established in the system, there are two components and three phases; the
  phase rule then tells us the system is univariant and the pressure has only
  one possible value at a given temperature. This pressure is called the
  <index|Dissociation pressure of a hydrate><subindex|Pressure|dissociation,
  of a hydrate><em|dissociation pressure> of the higher hydrate.

  The dissociation pressures of the three hydrates are indicated by
  horizontal lines in Fig. <reference|fig:13-CuSO4-water>. For instance, the
  dissociation pressure of <math|<text|CuSO<rsub|4>>\<cdot\>5*<text|H<rsub|2>O>>
  is <math|1.05<timesten|-2> <text|bar>>. At the pressure of each horizontal
  line, the equilibrium system can have one, two, or three phases, with
  compositions given by the intersections of the line with vertical lines. A
  fourth three-phase equilibrium is shown at <math|p=3.09<timesten|-2>
  <text|bar>>; this is the equilibrium between solid
  <math|<text|CuSO<rsub|4>>\<cdot\>5*<text|H<rsub|2>O>>, the saturated
  aqueous solution of this hydrate, and water vapor.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/CUSO4-W.eps|302pt|219pt||>>
        <label|fig:13-CuSO4-water>Pressure--composition phase diagram for the
        binary system of CuSO<rsub|<math|4>> (A) and H<rsub|<math|2>>O (B) at
        <math|25 <degC>>.<note-ref|+1XmfVJNylVc696v>

        <note-inline||+1XmfVJNylVc696v>Ref. <cite|logan-58>; Ref.
        <cite|ICT-7>, p. 263.
      </big-figure>
    </framed>
  </float>

  Consider the thermodynamic equilibrium constant of one of the dissociation
  reactions. At the low pressures shown in the phase diagram, the activities
  of the solids are practically unity and the fugacity of the water vapor is
  practically the same as the pressure, so the equilibrium constant is almost
  exactly equal to <math|p<rsub|<text|d>>/p<st>>, where
  <math|p<rsub|<text|d>>> is the dissociation pressure of the higher hydrate
  in the reaction. Thus, a hydrate cannot exist in equilibrium with water
  vapor at a pressure below the dissociation pressure of the hydrate because
  dissociation would be spontaneous under these conditions. Conversely, the
  salt formed by the dissociation of a hydrate cannot exist in equilibrium
  with water vapor at a pressure above the dissociation pressure because
  hydration would be spontaneous.

  <quote-env| If the system contains dry air as an additional gaseous
  component and one of the dissociation equilibria is established, the
  partial pressure <math|p<rsub|<text|H<rsub|2>O>>> of H<rsub|<math|2>>O is
  equal (approximately) to the dissociation pressure <math|p<rsub|<text|d>>>
  of the higher hydrate. The prior statements regarding dissociation and
  hydration now depend on the value of <math|p<rsub|<text|H<rsub|2>O>>>. If a
  hydrate is placed in air in which <math|p<rsub|<text|H<rsub|2>O>>> is less
  than <math|p<rsub|<text|d>>>, dehydration is spontaneous; this phenomenon
  is called <index|Efflorescence><newterm|efflorescence> (Latin:
  <em|blossoming>). If <math|p<rsub|<text|H<rsub|2>O>>> is greater than the
  vapor pressure of the saturated solution of the highest hydrate that can
  form in the system, the anhydrous salt and any of its hydrates will
  spontaneously absorb water and form the saturated solution; this is
  <index|Deliquescence><newterm|deliquescence> (Latin: <em|becoming fluid>).>

  If the two-component equilibrium system contains only two phases, it is
  bivariant corresponding to one of the areas in Fig.
  <reference|fig:13-CuSO4-water>. Here both the temperature and the pressure
  can be varied. In the case of areas labeled with two <em|solid> phases, the
  pressure has to be applied to the solids by a fluid (other than
  H<rsub|<math|2>>O) that is not considered part of the system.

  <subsection|Systems at high pressure><label|13-high p><label|c13 sec
  pdbs-high-pressure>

  <index-complex|<tuple|phase diagram|high pressure>|||<tuple|Phase
  diagram|at high pressure>>Binary phase diagrams begin to look different
  when the pressure is greater than the critical pressure of either of the
  pure components. Various types of behavior have been observed in this
  region. One common type, that found in the binary system of heptane and
  ethane, is shown in Fig. <vpageref|fig:13-ethane-heptane>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/ETH-HEP.eps|269pt|262pt||>>
        <label|fig:13-ethane-heptane>Pressure--temperature--composition
        behavior in the binary heptane--ethane
        system.<note-ref|+1XmfVJNylVc696w> The open circles are critical
        points; the dashed curve is the critical curve. The dashed line a\Ub
        illustrates retrograde condensation at <math|450<K>>.

        <note-inline||+1XmfVJNylVc696w>Ref. <cite|kay-38>.
      </big-figure>
    </framed>
  </float> This figure shows sections of a three-dimensional phase diagram at
  five temperatures. Each section is a pressure\Ucomposition phase diagram at
  constant <math|T>. The two-phase areas are hatched in the direction of the
  tie lines. At the left end of each tie line (at low <math|z<A>>) is a
  <index-complex|<tuple|vaporus curve|high pressure>|||<tuple|Vaporus
  curve|at high pressure>>vaporus curve, and at the right end is a
  <index-complex|<tuple|liquidus curve|high pressure>|||<tuple|Liquidus
  curve|at high pressure>>liquidus curve. The vapor pressure curve of pure
  ethane (<math|z<A|=>=0>) ends at the critical point of ethane at
  <math|305.4<K>>; between this point and the critical point of heptane at
  <math|540.5<K>>, there is a continuous <subindex|Critical|curve><em|critical
  curve>, which is the locus of critical points at which gas and liquid
  mixtures become identical in composition and density.

  Consider what happens when the system point is at point a in Fig.
  <reference|fig:13-ethane-heptane> and the pressure is then increased by
  isothermal compression along line a\Ub. The system point moves from the
  area for a gas phase into the two-phase gas\Uliquid area and then out into
  the gas-phase area again. This curious phenomenon, condensation followed by
  vaporization, is called <subindex|Retrograde|condensation><em|retrograde
  condensation>.

  Under some conditions, an isobaric increase of <math|T> can result in
  vaporization followed by condensation; this is
  <subindex|Retrograde|vaporization><em|retrograde vaporization>.

  A different type of high-pressure behavior, that found in the xenon\Uhelium
  system, is shown in Fig. <vpageref|fig:13-helium-xenon>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/HE-XE.eps|233pt|244pt||>>
        <label|fig:13-helium-xenon>Pressure--temperature\Ucomposition
        behavior in the binary xenon--helium
        system.<note-ref|+1XmfVJNylVc696x> The open circles are critical
        points; the dashed curve is the critical curve.

        <note-inline||+1XmfVJNylVc696x>Ref. <cite|deswann-66>.
      </big-figure>
    </framed>
  </float> Here, the critical curve begins at the critical point of the less
  volatile component (xenon) and continues to <em|higher> temperatures and
  pressures than the critical temperature and pressure of either pure
  component. The two-phase region at pressures above this critical curve is
  sometimes said to represent <subindex|Equilibrium|gas--gas><em|gas\Ugas
  equilibrium>, or <index|Gas--gas immiscibility><em|gas\Ugas immiscibility>,
  because we would not usually consider a liquid to exist beyond the critical
  points of the pure components. Of course, the coexisting phases in this
  two-phase region are not gases in the ordinary sense of being tenuous
  fluids, but are instead high-pressure fluids of liquid-like densities. If
  we want to call both phases gases, then we have to say that pure gaseous
  substances at high pressure do not necessarily mix spontaneously in all
  proportions as they do at ordinary pressures.

  If the pressure of a system is increased isothermally, eventually solid
  phases will appear; these are not shown in Figs.
  <reference|fig:13-ethane-heptane> and Fig. <reference|fig:13-helium-xenon>.

  <section|Phase Diagrams: Ternary Systems><label|13-ternary><label|c13 sec
  pdts>

  A ternary system is one with three components. We can independently vary
  the temperature, the pressure, and two independent composition variables
  for the system as a whole. A two-dimensional <index-complex|<tuple|phase
  diagram|ternary system>|||<tuple|Phase diagram|for a ternary system>>phase
  diagram for a ternary system is usually drawn for conditions of constant
  <math|T> and <math|p>.

  Although we could draw a two-dimensional phase diagram with Cartesian
  coordinates to express the mole fractions of two of the components, there
  are advantages in using instead the triangular coordinates shown in Fig.
  <vpageref|fig:13-triangles>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/TRIANG-1.eps|346pt|156pt||>>
        <label|fig:13-triangles>Representing the composition of a ternary
        system by a point in an equilateral triangle.
      </big-figure>
    </framed>
  </float> Each vertex of the equilateral triangle represents one of the pure
  components A, B, or C. A point on the side of the triangle opposite a
  vertex represents a binary system of the other two components, and a point
  within the triangle represents a ternary system with all three components.

  To determine the mole fraction <math|z<A>> of component A in the system as
  a whole represented by a point within the triangle, we measure the distance
  to the point from the side of the triangle that is opposite the vertex for
  pure A, then express this distance as a fraction of the height of the
  triangle. We follow the same procedure to determine <math|z<B>> and
  <math|z<C>>. The concept is shown in Fig. <reference|fig:13-triangles>(a).

  As an aid for the conversion between the position of a point and the
  overall composition, we can draw equally-spaced lines within the triangle
  parallel to the sides as shown in Fig. <reference|fig:13-triangles>(b). One
  of these lines, being at a constant distance from one side of the triangle,
  represents a constant mole fraction of one component. In the figure, the
  lines divide the distance from each side to the opposite vertex into ten
  equal parts; thus, adjacent parallel lines represent a difference of
  <math|0.1> in the mole fraction of a component, starting with <math|0> at
  the side of the triangle and ending with <math|1> at the vertex. Using the
  lines, we see that the filled circle in the figure represents the overall
  composition <math|z<A>=0.20>, <math|z<B>=0.30>, and <math|z<C>=0.50>.

  The sum of <math|z<A>>, <math|z<B>>, and <math|z<C>> must be <math|1>. The
  method of representing composition with a point in an equilateral triangle
  works because the sum of the lines drawn from the point to the three sides,
  perpendicular to the sides, equals the height of the triangle. The proof is
  shown in Fig. <vpageref|fig:13-trianglesum>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/TRIANG-2.eps|171pt|140pt||>>
        Proof that the sum of the lengths <math|a>, <math|b>, and <math|c> is
        equal to the height <math|h> of the large equilateral triangle ABC.
        ADE and FDP are two smaller equilateral triangles. The height of
        triangle ADE is equal to <math|h-a>. The height of triangle FDP is
        equal to the height of triangle ADE minus length <math|b>, and is
        also equal to length <math|c>: <math|h-a-b=c>. Therefore,
        <math|a+b+c=h>.<label|fig:13-trianglesum>
      </big-figure>
    </framed>
  </float>

  Two useful properties of this way of representing a ternary composition are
  as follows:

  <\enumerate-numeric>
    <item>Points on a line parallel to a side of the triangle represent
    systems in which one of the mole fractions remains constant.

    <item>Points on a line passing through a vertex represent systems in
    which the ratio of two of the mole fractions remains constant.
  </enumerate-numeric>

  <subsection|Three liquids><label|13-ternary, 3 liquids><label|c13 sec
  pdts-three-liquids>

  Figure <vpageref|fig:13-EtOH-benz-H2O> is the ternary phase diagram of a
  system of ethanol, benzene, and water at a temperature and pressure at
  which the phases are liquids. When the system point is in the area labeled
  <math|P=1>, there is a single liquid phase whose composition is described
  by the position of the point. The one-phase area extends to the side of the
  triangle representing binary mixtures of ethanol and benzene, and to the
  side representing binary mixtures of ethanol and water. In other words,
  ethanol and benzene mix in all proportions, and so also do ethanol and
  water.

  When the overall composition is such that the system point falls in the
  area labeled <math|P=2>, two liquid phases are present. The compositions of
  these phases are given by the positions of the ends of a
  <index-complex|<tuple|tie line|ternary>|||<tuple|Tie line|on a ternary
  phase diagram>>tie line through the system point. Four representative tie
  lines are included in the diagram, and these must be determined
  experimentally. The relative amounts of the two phases can be determined
  from the <index-complex|<tuple|lever rule|ternary>|||<tuple|Lever rule|for
  a ternary system>>lever rule.<footnote|The lever rule works, according to
  the general derivation in Sec. <reference|8-lever rule>, because the ratio
  <math|n<A>/n>, which is equal to <math|z<A>>, varies linearly with the
  position of the system point along a tie line on the triangular phase
  diagram.> In the limit of zero mole fraction of ethanol, the tie line falls
  along the horizontal base of the triangle and displays a
  <index-complex|<tuple|miscibility gap|ternary>|||<tuple|Miscibility gap|in
  a ternary system>>miscibility gap for the binary system of benzene and
  water. (The conjugate phases are very nearly pure benzene and pure water).

  The <index|Plait point><em|plait point> shown as an open circle in the
  figure is also called a <em|critical solution point>. As the system point
  approaches the plait point from within the two-phase area, the length of
  the tie line through the system point approaches zero, the miscibility gap
  disappears, and the compositions of the two
  <index-complex|<tuple|conjugate|phases|ternary>|||<tuple|Conjugate|phases|in
  a ternary system>>conjugate liquid phases become identical.

  Suppose we have the binary system of benzene and water represented by point
  a. Two liquid phases are present: one is wet benzene and the other is water
  containing a very small mole fraction of benzene. If we gradually stir
  ethanol into this system, the system point moves along the dotted line from
  point a toward the vertex for pure ethanol, but can never quite reach the
  vertex. At point b, there are still two phases, and we can consider the
  ethanol to have distributed itself between two partially-miscible solvents,
  benzene and water (Sec. <reference|12-solute distribution>). From the
  position of point b relative to the ends of the tie line passing through
  point b, we see that the mole fraction of ethanol is greater in the
  water-rich phase. As we continue to add ethanol, the amount of the
  water-rich phase increases and the amount of the benzene-rich phase
  decreases, until at point c the benzene-rich phase completely disappears.
  The added ethanol has increased the mutual solubilities of benzene and
  water and resulted in a single liquid phase.<\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/E-B-W.eps|156pt|133pt||>>
        <label|fig:13-EtOH-benz-H2O>Ternary phase diagram for ethanol,
        benzene, and water at <math|30 <degC>> and
        <math|1<br>>.<note-ref|+1XmfVJNylVc696y> The dashed lines are tie
        lines; the open circle indicates the plait point.

        <note-inline||+1XmfVJNylVc696y>Ref. <cite|brandani-85>.
      </big-figure>
    </framed>
  </float>

  <subsection|Two solids and a solvent><label|13-ternary, 2 solids
  \ solvent><label|c13 sec pdts-2solids-1solvent>

  The phase diagram in Fig. <vpageref|fig:13-H2O-NaCl-KCl><\float|float|thb>
    <\framed>
      <\big-figure|<image|13-SUP/NACL-KCL.eps|162pt|146pt||>>
        <label|fig:13-H2O-NaCl-KCl>Ternary phase diagram for NaCl, KCl, and
        water at <math|25 <degC>> and <math|1<br>>.<note-ref|+1XmfVJNylVc696z>
        The dashed lines are tie lines in the two-phase areas.

        <note-inline||+1XmfVJNylVc696z>Data from Ref. <cite|ICT-4>, p. 314.
      </big-figure>
    </framed>
  </float> is for a ternary system of water and two salts with an ion in
  common. There is a one-phase area for solution, labeled sln; a pair of
  two-phase areas in which the phases are a single solid salt and the
  saturated solution; and a triangular three-phase area. The upper vertex of
  the three-phase area, the <subindex|Eutonic|point><em|eutonic point>,
  represents the composition of solution saturated with respect to both
  salts. Some representative tie lines are drawn in the two-phase areas.

  A system of three components and three phases has two degrees of freedom;
  at fixed values of <math|T> and <math|p>, each phase must have a fixed
  composition. The fixed compositions of the phases that are present when the
  system point falls in the three-phase area are the compositions at the
  three vertices of the inner triangle: solid NaCl, solid KCl, and solution
  of the <subindex|Eutonic|composition>eutonic composition
  <math|x<rsub|<text|NaCl>>=0.20> and <math|x<rsub|<text|KCl>>=0.11>.

  From the position of the curved boundary that separates the one-phase
  solution area from the two-phase area for solution and solid KCl, we can
  see that adding NaCl to the saturated solution of KCl decreases the mole
  fraction of KCl in the saturated solution. Although it is not obvious in
  the phase diagram, adding KCl to a saturated solution of NaCl decreases the
  mole fraction of NaCl. These decreases in solubility when a common ion is
  added are examples of the <index|Common ion effect><em|common ion effect>
  mentioned in Sec. <reference|12-electrolyte solubility>.

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|12>
    <associate|page-first|340>
    <associate|preamble|false>
    <associate|section-nr|11>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|13-binary general|<tuple|13.2.1|?>>
    <associate|13-components approach|<tuple|13.1.3|?>>
    <associate|13-examples|<tuple|13.1.4|?>>
    <associate|13-high p|<tuple|13.2.7|?>>
    <associate|13-l-l eqm|<tuple|13.2.3|?>>
    <associate|13-liq-gas ideal|<tuple|13.2.4|?>>
    <associate|13-liq-gas nonideal|<tuple|13.2.5|?>>
    <associate|13-phase rule|<tuple|13.1|?>>
    <associate|13-species approach|<tuple|13.1.2|?>>
    <associate|13-ternary|<tuple|13.3|?>>
    <associate|13-ternary, 2 solids \ solvent|<tuple|13.3.2|?>>
    <associate|13-ternary, 3 liquids|<tuple|13.3.1|?>>
    <associate|C=s-r|<tuple|13.1.5|?>>
    <associate|Chap. 13|<tuple|13|?>>
    <associate|F=2+C-P|<tuple|13.1.1|?>>
    <associate|F=2+s-r-P|<tuple|13.1.2|?>>
    <associate|MeOH-benzene curves|<tuple|13.2.8|?>>
    <associate|auto-1|<tuple|13|?>>
    <associate|auto-10|<tuple|Univariant system|?>>
    <associate|auto-100|<tuple|<tuple|liquidus curve|binary liquid gas>|?>>
    <associate|auto-101|<tuple|<tuple|vaporus curve|binary liquid gas>|?>>
    <associate|auto-102|<tuple|Azeotrope|?>>
    <associate|auto-103|<tuple|Azeotrope|?>>
    <associate|auto-104|<tuple|13.2.10|?>>
    <associate|auto-105|<tuple|Zeotropic behavior|?>>
    <associate|auto-106|<tuple|13.2.11|?>>
    <associate|auto-107|<tuple|13.2.6|?>>
    <associate|auto-108|<tuple|<tuple|phase diagram|binary solid gas>|?>>
    <associate|auto-109|<tuple|Solid compound|?>>
    <associate|auto-11|<tuple|Invariant system|?>>
    <associate|auto-110|<tuple|Dissociation pressure of a hydrate|?>>
    <associate|auto-111|<tuple|Pressure|?>>
    <associate|auto-112|<tuple|13.2.12|?>>
    <associate|auto-113|<tuple|Efflorescence|?>>
    <associate|auto-114|<tuple|efflorescence|?>>
    <associate|auto-115|<tuple|Deliquescence|?>>
    <associate|auto-116|<tuple|deliquescence|?>>
    <associate|auto-117|<tuple|13.2.7|?>>
    <associate|auto-118|<tuple|<tuple|phase diagram|high pressure>|?>>
    <associate|auto-119|<tuple|13.2.13|?>>
    <associate|auto-12|<tuple|13.1.2|?>>
    <associate|auto-120|<tuple|<tuple|vaporus curve|high pressure>|?>>
    <associate|auto-121|<tuple|<tuple|liquidus curve|high pressure>|?>>
    <associate|auto-122|<tuple|Critical|?>>
    <associate|auto-123|<tuple|Retrograde|?>>
    <associate|auto-124|<tuple|Retrograde|?>>
    <associate|auto-125|<tuple|13.2.14|?>>
    <associate|auto-126|<tuple|Equilibrium|?>>
    <associate|auto-127|<tuple|Gas--gas immiscibility|?>>
    <associate|auto-128|<tuple|13.3|?>>
    <associate|auto-129|<tuple|<tuple|phase diagram|ternary system>|?>>
    <associate|auto-13|<tuple|Species|?>>
    <associate|auto-130|<tuple|13.3.1|?>>
    <associate|auto-131|<tuple|13.3.2|?>>
    <associate|auto-132|<tuple|13.3.1|?>>
    <associate|auto-133|<tuple|<tuple|tie line|ternary>|?>>
    <associate|auto-134|<tuple|<tuple|lever rule|ternary>|?>>
    <associate|auto-135|<tuple|<tuple|miscibility gap|ternary>|?>>
    <associate|auto-136|<tuple|Plait point|?>>
    <associate|auto-137|<tuple|<tuple|conjugate|phases|ternary>|?>>
    <associate|auto-138|<tuple|13.3.3|?>>
    <associate|auto-139|<tuple|13.3.2|?>>
    <associate|auto-14|<tuple|Independent variables|?>>
    <associate|auto-140|<tuple|13.3.4|?>>
    <associate|auto-141|<tuple|Eutonic|?>>
    <associate|auto-142|<tuple|Eutonic|?>>
    <associate|auto-143|<tuple|Common ion effect|?>>
    <associate|auto-15|<tuple|13.1.3|?>>
    <associate|auto-16|<tuple|Component|?>>
    <associate|auto-17|<tuple|components|?>>
    <associate|auto-18|<tuple|Electrical|?>>
    <associate|auto-19|<tuple|Neutrality, electrical|?>>
    <associate|auto-2|<tuple|13.1|?>>
    <associate|auto-20|<tuple|13.1.4|?>>
    <associate|auto-21|<tuple|13.1.4.1|?>>
    <associate|auto-22|<tuple|13.1.4.2|?>>
    <associate|auto-23|<tuple|13.1.4.3|?>>
    <associate|auto-24|<tuple|13.1.4.4|?>>
    <associate|auto-25|<tuple|13.1.4.5|?>>
    <associate|auto-26|<tuple|<tuple|gibbs|phase rule|multicomponent>|?>>
    <associate|auto-27|<tuple|13.2|?>>
    <associate|auto-28|<tuple|13.2.1|?>>
    <associate|auto-29|<tuple|<tuple|phase diagram|binary system>|?>>
    <associate|auto-3|<tuple|<tuple|gibbs|phase rule|multicomponent>|?>>
    <associate|auto-30|<tuple|<tuple|tie line|binary>|?>>
    <associate|auto-31|<tuple|Lever rule|?>>
    <associate|auto-32|<tuple|Solidus curve for a binary system|?>>
    <associate|auto-33|<tuple|<tuple|liquidus curve|binary>|?>>
    <associate|auto-34|<tuple|<tuple|vaporus curve|binary>|?>>
    <associate|auto-35|<tuple|13.2.2|?>>
    <associate|auto-36|<tuple|13.2.1|?>>
    <associate|auto-37|<tuple|<tuple|phase diagram|binary solid liquid>|?>>
    <associate|auto-38|<tuple|<tuple|freezing point|curve|binary solid
    liquid>|?>>
    <associate|auto-39|<tuple|<tuple|solubility|curve|binary solid
    liquid>|?>>
    <associate|auto-4|<tuple|13.1.1|?>>
    <associate|auto-40|<tuple|<tuple|liquidus curve|binary solid liquid>|?>>
    <associate|auto-41|<tuple|Eutectic|?>>
    <associate|auto-42|<tuple|eutectic point|?>>
    <associate|auto-43|<tuple|Eutectic|?>>
    <associate|auto-44|<tuple|Eutectic|?>>
    <associate|auto-45|<tuple|Isopleth|?>>
    <associate|auto-46|<tuple|<tuple|lever rule|binary>|?>>
    <associate|auto-47|<tuple|Eutectic|?>>
    <associate|auto-48|<tuple|Eutectic|?>>
    <associate|auto-49|<tuple|Thermal|?>>
    <associate|auto-5|<tuple|Degrees of freedom|?>>
    <associate|auto-50|<tuple|Eutectic|?>>
    <associate|auto-51|<tuple|13.2.2|?>>
    <associate|auto-52|<tuple|Solution|?>>
    <associate|auto-53|<tuple|Solid compound|?>>
    <associate|auto-54|<tuple|13.2.3|?>>
    <associate|auto-55|<tuple|Congruent melting|?>>
    <associate|auto-56|<tuple|13.2.4|?>>
    <associate|auto-57|<tuple|Eutectic|?>>
    <associate|auto-58|<tuple|Liquidus curve|?>>
    <associate|auto-59|<tuple|Peritectic point|?>>
    <associate|auto-6|<tuple|number of degrees of freedom|?>>
    <associate|auto-60|<tuple|Eutectic|?>>
    <associate|auto-61|<tuple|13.2.3|?>>
    <associate|auto-62|<tuple|Phase|?>>
    <associate|auto-63|<tuple|<tuple|conjugate|phases|binary system>|?>>
    <associate|auto-64|<tuple|<tuple|miscibility gap|binary system>|?>>
    <associate|auto-65|<tuple|<tuple|phase diagram|binary liquid liquid>|?>>
    <associate|auto-66|<tuple|13.2.5|?>>
    <associate|auto-67|<tuple|<tuple|miscibility gap|binary system>|?>>
    <associate|auto-68|<tuple|Upper consolute temperature|?>>
    <associate|auto-69|<tuple|Temperature|?>>
    <associate|auto-7|<tuple|Variance|?>>
    <associate|auto-70|<tuple|Upper critical solution temperature|?>>
    <associate|auto-71|<tuple|Temperature|?>>
    <associate|auto-72|<tuple|Consolute point|?>>
    <associate|auto-73|<tuple|<tuple|critical|point|partially>|?>>
    <associate|auto-74|<tuple|<tuple|lever rule|partially miscible>|?>>
    <associate|auto-75|<tuple|13.2.4|?>>
    <associate|auto-76|<tuple|<tuple|liquidus curve|binary liquid gas>|?>>
    <associate|auto-77|<tuple|<tuple|vaporus curve|binary liquid gas>|?>>
    <associate|auto-78|<tuple|<tuple|phase diagram|binary liquid gas>|?>>
    <associate|auto-79|<tuple|13.2.6|?>>
    <associate|auto-8|<tuple|variance|?>>
    <associate|auto-80|<tuple|<tuple|raoult's law|partial|binary>|?>>
    <associate|auto-81|<tuple|13.2.7|?>>
    <associate|auto-82|<tuple|<tuple|liquidus curve|binary liquid gas>|?>>
    <associate|auto-83|<tuple|Bubble-point curve|?>>
    <associate|auto-84|<tuple|Boiling point|?>>
    <associate|auto-85|<tuple|Dew-point curve|?>>
    <associate|auto-86|<tuple|Condensation curve|?>>
    <associate|auto-87|<tuple|13.2.5|?>>
    <associate|auto-88|<tuple|Raoult's law|?>>
    <associate|auto-89|<tuple|<tuple|liquidus curve|binary liquid gas>|?>>
    <associate|auto-9|<tuple|Bivariant system|?>>
    <associate|auto-90|<tuple|Azeotropic behavior|?>>
    <associate|auto-91|<tuple|13.2.8|?>>
    <associate|auto-92|<tuple|<tuple|liquidus curve|binary liquid gas>|?>>
    <associate|auto-93|<tuple|<tuple|vaporus curve|binary liquid gas>|?>>
    <associate|auto-94|<tuple|13.2.9|?>>
    <associate|auto-95|<tuple|<tuple|liquidus curve|binary liquid gas>|?>>
    <associate|auto-96|<tuple|<tuple|vaporus curve|binary liquid gas>|?>>
    <associate|auto-97|<tuple|Azeotropy|?>>
    <associate|auto-98|<tuple|Azeotrope|?>>
    <associate|auto-99|<tuple|azeotrope|?>>
    <associate|c13|<tuple|13|?>>
    <associate|c13 sec gprms-degrees-of-freedom|<tuple|13.1.1|?>>
    <associate|c13 sec gprms-examples|<tuple|13.1.4|?>>
    <associate|c13 sec pdbs|<tuple|13.2|?>>
    <associate|c13 sec pdbs-generalities|<tuple|13.2.1|?>>
    <associate|c13 sec pdbs-high-pressure|<tuple|13.2.7|?>>
    <associate|c13 sec pdbs-solid-gas|<tuple|13.2.6|?>>
    <associate|c13 sec pdbs-solid-liquid|<tuple|13.2.2|?>>
    <associate|c13 sec pdts|<tuple|13.3|?>>
    <associate|c13 sec pdts-2solids-1solvent|<tuple|13.3.2|?>>
    <associate|c13 sec pdts-three-liquids|<tuple|13.3.1|?>>
    <associate|c13 sec-gprms|<tuple|13.1|?>>
    <associate|c13 sec-gprms-components|<tuple|13.1.3|?>>
    <associate|c13 sec-gprms-species|<tuple|13.1.2|?>>
    <associate|c13 sec-pdbs-liq-gas-ideal|<tuple|13.2.4|?>>
    <associate|c13 sec-pdbs-liq-gas-nonideal|<tuple|13.2.5|?>>
    <associate|c13 sec-pdbs-liq-partially-miscible|<tuple|13.2.3|?>>
    <associate|example 2|<tuple|2|?>>
    <associate|fig:13-CuSO4-water|<tuple|13.2.12|?>>
    <associate|fig:13-EtOH-benz-H2O|<tuple|13.3.3|?>>
    <associate|fig:13-H2O-NaCl|<tuple|13.2.4|?>>
    <associate|fig:13-H2O-NaCl-KCl|<tuple|13.3.4|?>>
    <associate|fig:13-benztol|<tuple|13.2.6|?>>
    <associate|fig:13-ethane-heptane|<tuple|13.2.13|?>>
    <associate|fig:13-eutectics|<tuple|13.2.2|?>>
    <associate|fig:13-helium-xenon|<tuple|13.2.14|?>>
    <associate|fig:13-l-gas|<tuple|13.2.10|?>>
    <associate|fig:13-l-l-gas|<tuple|13.2.11|?>>
    <associate|fig:13-liqliq|<tuple|13.2.5|?>>
    <associate|fig:13-solid cmpd AB|<tuple|13.2.3|?>>
    <associate|fig:13-solid-l|<tuple|13.2.1|?>>
    <associate|fig:13-triangles|<tuple|13.3.1|?>>
    <associate|fig:13-trianglesum|<tuple|13.3.2|?>>
    <associate|fig:13-zTp-MeOHBenz|<tuple|13.2.9|?>>
    <associate|fig:13-zTp-benztol|<tuple|13.2.7|?>>
    <associate|footnote-13.1.1|<tuple|13.1.1|?>>
    <associate|footnote-13.1.2|<tuple|13.1.2|?>>
    <associate|footnote-13.1.3|<tuple|13.1.3|?>>
    <associate|footnote-13.2.1|<tuple|13.2.1|?>>
    <associate|footnote-13.2.10|<tuple|13.2.10|?>>
    <associate|footnote-13.2.2|<tuple|13.2.2|?>>
    <associate|footnote-13.2.3|<tuple|13.2.3|?>>
    <associate|footnote-13.2.4|<tuple|13.2.4|?>>
    <associate|footnote-13.2.5|<tuple|13.2.5|?>>
    <associate|footnote-13.2.6|<tuple|13.2.6|?>>
    <associate|footnote-13.2.7|<tuple|13.2.7|?>>
    <associate|footnote-13.2.8|<tuple|13.2.8|?>>
    <associate|footnote-13.2.9|<tuple|13.2.9|?>>
    <associate|footnote-13.3.1|<tuple|13.3.1|?>>
    <associate|footnote-13.3.2|<tuple|13.3.2|?>>
    <associate|footnote-13.3.3|<tuple|13.3.3|?>>
    <associate|footnr-13.1.1|<tuple|13.1.1|?>>
    <associate|footnr-13.1.2|<tuple|13.1.2|?>>
    <associate|footnr-13.1.3|<tuple|13.1.3|?>>
    <associate|footnr-13.2.1|<tuple|13.2.1|?>>
    <associate|footnr-13.2.10|<tuple|13.2.14|?>>
    <associate|footnr-13.2.2|<tuple|a|?>>
    <associate|footnr-13.2.3|<tuple|13.2.5|?>>
    <associate|footnr-13.2.4|<tuple|13.2.4|?>>
    <associate|footnr-13.2.5|<tuple|13.2.8|?>>
    <associate|footnr-13.2.6|<tuple|13.2.6|?>>
    <associate|footnr-13.2.7|<tuple|13.2.9|?>>
    <associate|footnr-13.2.8|<tuple|13.2.12|?>>
    <associate|footnr-13.2.9|<tuple|13.2.13|?>>
    <associate|footnr-13.3.1|<tuple|13.3.1|?>>
    <associate|footnr-13.3.2|<tuple|13.3.3|?>>
    <associate|footnr-13.3.3|<tuple|13.3.4|?>>
    <associate|init cond|<tuple|13.1.13|?>>
    <associate|p=pB*+(pA*-pB*)xA(l)|<tuple|13.2.4|?>>
    <associate|pA=xApA*,pB=(1-xA)pB*|<tuple|13.2.3|?>>
    <associate|xA(g)=|<tuple|13.2.7|?>>
    <associate|xA(l)=|<tuple|13.2.6|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      ICT-4

      philip-1903

      cohen-91

      ICT-3

      ferloni-74

      Toghiani-94

      Toghiani-94

      logan-58

      ICT-7

      kay-38

      deswann-66

      brandani-85

      ICT-4
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.1>|>
        Temperature--composition phase diagram for a binary system exhibiting
        a eutectic point.
      </surround>|<pageref|auto-36>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.2>|>
        Temperature--composition phase diagrams with single eutectics.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Two
                pure solids and a liquid mixture.<space|0spc><assign|footnote-nr|2><hidden-binding|<tuple>|13.2.2><assign|fnote-+1XmfVJNylVc696r|<quote|13.2.2>><assign|fnlab-+1XmfVJNylVc696r|<quote|13.2.2>><rsup|<with|font-shape|<quote|right>|<reference|footnote-13.2.2>>>

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Two
                solid solutions and a liquid mixture.
              </surround>
            </with>
          </surround>
        </with>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-B307C5D8--DCF6D8B8>|<link|hyperlink|<id|%-B307C5D8--DCF6D8B8>|<url|#footnr-13.2.2>>|13.2.2>.
        |<hidden-binding|<tuple|footnote-13.2.2>|13.2.2>|>>>Ref.
        [<write|bib|ICT-4><reference|bib-ICT-4>], p. 98.
      </surround>|<pageref|auto-51>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.3>|>
        Temperature--composition phase diagram for the binary system of
        <with|mode|<quote|math>|\<alpha\>>-naphthylamine (A) and phenol (B)
        at <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>> (Ref.
        [<write|bib|philip-1903><reference|bib-philip-1903>]).
      </surround>|<pageref|auto-54>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.4>|>
        Temperature--composition phase diagram for the binary system of
        H<rsub|<with|mode|<quote|math>|2>>O and NaCl at
        <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>. (Data from Refs.
        [<write|bib|cohen-91><reference|bib-cohen-91>] and
        [<write|bib|ICT-3><reference|bib-ICT-3>].)
      </surround>|<pageref|auto-56>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.5>|>
        Temperature--composition phase diagram for the binary system of
        methyl acetate (A) and carbon disulfide (B) at
        <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.<space|0spc><assign|footnote-nr|3><hidden-binding|<tuple>|13.2.3><assign|fnote-+1XmfVJNylVc696s|<quote|13.2.3>><assign|fnlab-+1XmfVJNylVc696s|<quote|13.2.3>><rsup|<with|font-shape|<quote|right>|<reference|footnote-13.2.3>>>
        All phases are liquids. The open circle indicates the critical point.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-B307C5D8--E491CE90>|<link|hyperlink|<id|%-B307C5D8--E491CE90>|<url|#footnr-13.2.3>>|13.2.3>.
        |<hidden-binding|<tuple|footnote-13.2.3>|13.2.3>|>>>Data from Ref.
        [<write|bib|ferloni-74><reference|bib-ferloni-74>].
      </surround>|<pageref|auto-66>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.6>|>
        Phase diagrams for the binary system of toluene (A) and benzene (B).
        The curves are calculated from Eqs. <reference|xA(l)=> and
        <reference|xA(g)=> and the saturation vapor pressures of the pure
        liquids.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Pressure\Ucomposition
                diagram at <with|mode|<quote|math>|T=340<with|mode|<quote|math>|
                <with|mode|<quote|text>|K>>>.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Temperature\Ucomposition
                diagram at <with|mode|<quote|math>|p=1<with|mode|<quote|math>|
                <with|mode|<quote|text>|bar>>>.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-79>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.7>|>
        Liquidus and vaporus surfaces for the binary system of toluene (A)
        and benzene. Cross-sections through the two-phase region are drawn at
        constant temperatures of <with|mode|<quote|math>|340<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>> and <with|mode|<quote|math>|370<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>> and at constant pressures of
        <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>> and
        <with|mode|<quote|math>|2<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>. Two of the cross-sections intersect
        at a tie line at <with|mode|<quote|math>|T=370<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>> and <with|mode|<quote|math>|p=1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>, and the other cross-sections are
        hatched in the direction of the tie lines.
      </surround>|<pageref|auto-81>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.8>|>
        Binary system of methanol (A) and benzene at
        <with|mode|<quote|math>|45 <rsup|\<circ\>><with|mode|<quote|text>|C>>.<space|0spc><assign|footnote-nr|5><hidden-binding|<tuple>|13.2.5><assign|fnote-+1XmfVJNylVc696t|<quote|13.2.5>><assign|fnlab-+1XmfVJNylVc696t|<quote|13.2.5>><rsup|<with|font-shape|<quote|right>|<reference|footnote-13.2.5>>>

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Partial
                pressures and total pressure in the gas phase equilibrated
                with liquid mixtures. The dashed lines indicate Raoult's law
                behavior.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Pressure\Ucomposition
                phase diagram at <with|mode|<quote|math>|45
                <rsup|\<circ\>><with|mode|<quote|text>|C>>. Open circle:
                azeotropic point at <with|mode|<quote|math>|z<rsub|<with|mode|<quote|text>|A>>=0.59>
                and <with|mode|<quote|math>|p=60.5
                <with|mode|<quote|text>|kPa>>.
              </surround>
            </with>
          </surround>
        </with>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-B307C5D8--E33F7920>|<link|hyperlink|<id|%-B307C5D8--E33F7920>|<url|#footnr-13.2.5>>|13.2.5>.
        |<hidden-binding|<tuple|footnote-13.2.5>|13.2.5>|>>>Ref.
        [<write|bib|Toghiani-94><reference|bib-Toghiani-94>].
      </surround>|<pageref|auto-91>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.9>|>
        Liquidus and vaporus surfaces for the binary system of methanol (A)
        and benzene.<space|0spc><assign|footnote-nr|7><hidden-binding|<tuple>|13.2.7><assign|fnote-+1XmfVJNylVc696u|<quote|13.2.7>><assign|fnlab-+1XmfVJNylVc696u|<quote|13.2.7>><rsup|<with|font-shape|<quote|right>|<reference|footnote-13.2.7>>>
        Cross-sections are hatched in the direction of the tie lines. The
        dashed curve is the azeotrope vapor-pressure curve.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-B307C5D8--E412B750>|<link|hyperlink|<id|%-B307C5D8--E412B750>|<url|#footnr-13.2.7>>|13.2.7>.
        |<hidden-binding|<tuple|footnote-13.2.7>|13.2.7>|>>>Ref.
        [<write|bib|Toghiani-94><reference|bib-Toghiani-94>].
      </surround>|<pageref|auto-94>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.10>|>
        Temperature\Ucomposition phase diagrams of binary systems exhibiting
        (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>no
        azeotropy, (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>a
        minimum-boiling azeotrope, and (c)
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>a
        maximum-boiling azeotrope. Only the one-phase areas are labeled;
        two-phase areas are hatched in the direction of the tie lines.
      </surround>|<pageref|auto-104>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.11>|>
        Temperature\Ucomposition phase diagrams of binary systems with
        partially-miscible liquids exhibiting (a)
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>the
        ability to be separated into pure components by fractional
        distillation, (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>a
        minimum-boiling azeotrope, and (c)
        <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>boiling
        at a lower temperature than the boiling point of either pure
        component. Only the one-phase areas are labeled; two-phase areas are
        hatched in the direction of the tie lines.
      </surround>|<pageref|auto-106>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.12>|>
        Pressure--composition phase diagram for the binary system of
        CuSO<rsub|<with|mode|<quote|math>|4>> (A) and
        H<rsub|<with|mode|<quote|math>|2>>O (B) at <with|mode|<quote|math>|25
        <rsup|\<circ\>><with|mode|<quote|text>|C>>.<space|0spc><assign|footnote-nr|8><hidden-binding|<tuple>|13.2.8><assign|fnote-+1XmfVJNylVc696v|<quote|13.2.8>><assign|fnlab-+1XmfVJNylVc696v|<quote|13.2.8>><rsup|<with|font-shape|<quote|right>|<reference|footnote-13.2.8>>>

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-B307C5D8--E989DDD8>|<link|hyperlink|<id|%-B307C5D8--E989DDD8>|<url|#footnr-13.2.8>>|13.2.8>.
        |<hidden-binding|<tuple|footnote-13.2.8>|13.2.8>|>>>Ref.
        [<write|bib|logan-58><reference|bib-logan-58>]; Ref.
        [<write|bib|ICT-7><reference|bib-ICT-7>], p. 263.
      </surround>|<pageref|auto-112>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.13>|>
        Pressure--temperature--composition behavior in the binary
        heptane--ethane system.<space|0spc><assign|footnote-nr|9><hidden-binding|<tuple>|13.2.9><assign|fnote-+1XmfVJNylVc696w|<quote|13.2.9>><assign|fnlab-+1XmfVJNylVc696w|<quote|13.2.9>><rsup|<with|font-shape|<quote|right>|<reference|footnote-13.2.9>>>
        The open circles are critical points; the dashed curve is the
        critical curve. The dashed line a\Ub illustrates retrograde
        condensation at <with|mode|<quote|math>|450<with|mode|<quote|math>|
        <with|mode|<quote|text>|K>>>.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-B307C5D8--D1055D10>|<link|hyperlink|<id|%-B307C5D8--D1055D10>|<url|#footnr-13.2.9>>|13.2.9>.
        |<hidden-binding|<tuple|footnote-13.2.9>|13.2.9>|>>>Ref.
        [<write|bib|kay-38><reference|bib-kay-38>].
      </surround>|<pageref|auto-119>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.2.14>|>
        Pressure--temperature\Ucomposition behavior in the binary
        xenon--helium system.<space|0spc><assign|footnote-nr|10><hidden-binding|<tuple>|13.2.10><assign|fnote-+1XmfVJNylVc696x|<quote|13.2.10>><assign|fnlab-+1XmfVJNylVc696x|<quote|13.2.10>><rsup|<with|font-shape|<quote|right>|<reference|footnote-13.2.10>>>
        The open circles are critical points; the dashed curve is the
        critical curve.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-B307C5D8--E3AE3040>|<link|hyperlink|<id|%-B307C5D8--E3AE3040>|<url|#footnr-13.2.10>>|13.2.10>.
        |<hidden-binding|<tuple|footnote-13.2.10>|13.2.10>|>>>Ref.
        [<write|bib|deswann-66><reference|bib-deswann-66>].
      </surround>|<pageref|auto-125>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.3.1>|>
        Representing the composition of a ternary system by a point in an
        equilateral triangle.
      </surround>|<pageref|auto-130>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.3.2>|>
        Proof that the sum of the lengths <with|mode|<quote|math>|a>,
        <with|mode|<quote|math>|b>, and <with|mode|<quote|math>|c> is equal
        to the height <with|mode|<quote|math>|h> of the large equilateral
        triangle ABC. ADE and FDP are two smaller equilateral triangles. The
        height of triangle ADE is equal to <with|mode|<quote|math>|h-a>. The
        height of triangle FDP is equal to the height of triangle ADE minus
        length <with|mode|<quote|math>|b>, and is also equal to length
        <with|mode|<quote|math>|c>: <with|mode|<quote|math>|h-a-b=c>.
        Therefore, <with|mode|<quote|math>|a+b+c=h>.
      </surround>|<pageref|auto-131>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.3.3>|>
        Ternary phase diagram for ethanol, benzene, and water at
        <with|mode|<quote|math>|30 <rsup|\<circ\>><with|mode|<quote|text>|C>>
        and <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.<space|0spc><assign|footnote-nr|2><hidden-binding|<tuple>|13.3.2><assign|fnote-+1XmfVJNylVc696y|<quote|13.3.2>><assign|fnlab-+1XmfVJNylVc696y|<quote|13.3.2>><rsup|<with|font-shape|<quote|right>|<reference|footnote-13.3.2>>>
        The dashed lines are tie lines; the open circle indicates the plait
        point.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-B307C5D8--ECD37F70>|<link|hyperlink|<id|%-B307C5D8--ECD37F70>|<url|#footnr-13.3.2>>|13.3.2>.
        |<hidden-binding|<tuple|footnote-13.3.2>|13.3.2>|>>>Ref.
        [<write|bib|brandani-85><reference|bib-brandani-85>].
      </surround>|<pageref|auto-138>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|13.3.4>|>
        Ternary phase diagram for NaCl, KCl, and water at
        <with|mode|<quote|math>|25 <rsup|\<circ\>><with|mode|<quote|text>|C>>
        and <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>.<space|0spc><assign|footnote-nr|3><hidden-binding|<tuple>|13.3.3><assign|fnote-+1XmfVJNylVc696z|<quote|13.3.3>><assign|fnlab-+1XmfVJNylVc696z|<quote|13.3.3>><rsup|<with|font-shape|<quote|right>|<reference|footnote-13.3.3>>>
        The dashed lines are tie lines in the two-phase areas.

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-B307C5D8--E1BFF770>|<link|hyperlink|<id|%-B307C5D8--E1BFF770>|<url|#footnr-13.3.3>>|13.3.3>.
        |<hidden-binding|<tuple|footnote-13.3.3>|13.3.3>|>>>Data from Ref.
        [<write|bib|ICT-4><reference|bib-ICT-4>], p. 314.
      </surround>|<pageref|auto-140>>
    </associate>
    <\associate|gly>
      <tuple|normal|number of degrees of freedom|<pageref|auto-6>>

      <tuple|normal|variance|<pageref|auto-8>>

      <tuple|normal|components|<pageref|auto-17>>

      <tuple|normal|eutectic point|<pageref|auto-42>>

      <tuple|normal|azeotrope|<pageref|auto-99>>

      <tuple|normal|efflorescence|<pageref|auto-114>>

      <tuple|normal|deliquescence|<pageref|auto-116>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|gibbs|phase rule|multicomponent>||c13 sec-gprms
      idx1|<tuple|Gibbs|phase rule|for a multicomponent
      system>|<pageref|auto-3>>

      <tuple|<tuple|Degrees of freedom>|<pageref|auto-5>>

      <tuple|<tuple|Variance>|<pageref|auto-7>>

      <tuple|<tuple|Bivariant system>|<pageref|auto-9>>

      <tuple|<tuple|Univariant system>|<pageref|auto-10>>

      <tuple|<tuple|Invariant system>|<pageref|auto-11>>

      <tuple|<tuple|Species>|<pageref|auto-13>>

      <tuple|<tuple|Independent variables|number of>|<pageref|auto-14>>

      <tuple|<tuple|Component>|<pageref|auto-16>>

      <tuple|<tuple|Electrical|neutrality>|<pageref|auto-18>>

      <tuple|<tuple|Neutrality, electrical>|<pageref|auto-19>>

      <tuple|<tuple|gibbs|phase rule|multicomponent>||c13 sec-gprms
      idx1|<tuple|Gibbs|phase rule|for a multicomponent
      system>|<pageref|auto-26>>

      <tuple|<tuple|phase diagram|binary system>|||<tuple|Phase diagram|for a
      binary system>|<pageref|auto-29>>

      <tuple|<tuple|tie line|binary>|||<tuple|Tie line|on a binary phase
      diagram>|<pageref|auto-30>>

      <tuple|<tuple|Lever rule>|<pageref|auto-31>>

      <tuple|<tuple|Solidus curve for a binary system>|<pageref|auto-32>>

      <tuple|<tuple|liquidus curve|binary>|||<tuple|Liquidus curve|for a
      binary system>|<pageref|auto-33>>

      <tuple|<tuple|vaporus curve|binary>|||<tuple|Vaporus curve|for a binary
      system>|<pageref|auto-34>>

      <tuple|<tuple|phase diagram|binary solid liquid>|||<tuple|Phase
      diagram|for a binary solid\Uliquid system>|<pageref|auto-37>>

      <tuple|<tuple|freezing point|curve|binary solid
      liquid>|||<tuple|Freezing point|curve|for a binary solid\Uliquid
      system>|<pageref|auto-38>>

      <tuple|<tuple|solubility|curve|binary solid
      liquid>|||<tuple|Solubility|curve|for a binary solid\Uliquid
      system>|<pageref|auto-39>>

      <tuple|<tuple|liquidus curve|binary solid liquid>|||<tuple|Liquidus
      curve|for a binary solid\Uliquid system>|<pageref|auto-40>>

      <tuple|<tuple|Eutectic|point>|<pageref|auto-41>>

      <tuple|<tuple|Eutectic|composition>|<pageref|auto-43>>

      <tuple|<tuple|Eutectic|temperature>|<pageref|auto-44>>

      <tuple|<tuple|Isopleth>|<pageref|auto-45>>

      <tuple|<tuple|lever rule|binary>|||<tuple|Lever rule|for a binary phase
      diagram>|<pageref|auto-46>>

      <tuple|<tuple|Eutectic|temperature>|<pageref|auto-47>>

      <tuple|<tuple|Eutectic|halt>|<pageref|auto-48>>

      <tuple|<tuple|Thermal|analysis>|<pageref|auto-49>>

      <tuple|<tuple|Eutectic|temperature>|<pageref|auto-50>>

      <tuple|<tuple|Solution|solid>|<pageref|auto-52>>

      <tuple|<tuple|Solid compound>|<pageref|auto-53>>

      <tuple|<tuple|Congruent melting>|<pageref|auto-55>>

      <tuple|<tuple|Eutectic|point>|<pageref|auto-57>>

      <tuple|<tuple|Liquidus curve>|<pageref|auto-58>>

      <tuple|<tuple|Peritectic point>|<pageref|auto-59>>

      <tuple|<tuple|Eutectic|point>|<pageref|auto-60>>

      <tuple|<tuple|Phase|separation of a liquid mixture>|<pageref|auto-62>>

      <tuple|<tuple|conjugate|phases|binary
      system>|||<tuple|Conjugate|phases|in a binary
      system>|<pageref|auto-63>>

      <tuple|<tuple|miscibility gap|binary system>|||<tuple|Miscibility
      gap|in a binary system>|<pageref|auto-64>>

      <tuple|<tuple|phase diagram|binary liquid liquid>|||<tuple|Phase
      diagram|for a binary liquid\Uliquid system>|<pageref|auto-65>>

      <tuple|<tuple|miscibility gap|binary system>|||<tuple|Miscibility
      gap|in a binary system>|<pageref|auto-67>>

      <tuple|<tuple|Upper consolute temperature>|<pageref|auto-68>>

      <tuple|<tuple|Temperature|upper consolute>|<pageref|auto-69>>

      <tuple|<tuple|Upper critical solution temperature>|<pageref|auto-70>>

      <tuple|<tuple|Temperature|upper critical solution>|<pageref|auto-71>>

      <tuple|<tuple|Consolute point>|<pageref|auto-72>>

      <tuple|<tuple|critical|point|partially>|||<tuple|Critical|point|of
      partially-miscible liquids>|<pageref|auto-73>>

      <tuple|<tuple|lever rule|partially miscible>|||<tuple|Lever rule|for
      partially-miscible liquids>|<pageref|auto-74>>

      <tuple|<tuple|liquidus curve|binary liquid gas>|||<tuple|Liquidus
      curve|for a binary liquid\Ugas system>|<pageref|auto-76>>

      <tuple|<tuple|vaporus curve|binary liquid gas>|||<tuple|Vaporus
      curve|for a binary liquid\Ugas system>|<pageref|auto-77>>

      <tuple|<tuple|phase diagram|binary liquid gas>|||<tuple|Phase
      diagram|for a binary liquid\Ugas system>|<pageref|auto-78>>

      <tuple|<tuple|raoult's law|partial|binary>|||<tuple|Raoult's law|for
      partial pressure|in a binary system>|<pageref|auto-80>>

      <tuple|<tuple|liquidus curve|binary liquid gas>|||<tuple|Liquidus
      curve|for a binary liquid\Ugas system>|<pageref|auto-82>>

      <tuple|<tuple|Bubble-point curve>|<pageref|auto-83>>

      <tuple|<tuple|Boiling point|curve>|<pageref|auto-84>>

      <tuple|<tuple|Dew-point curve>|<pageref|auto-85>>

      <tuple|<tuple|Condensation curve>|<pageref|auto-86>>

      <tuple|<tuple|Raoult's law|deviations from>|<pageref|auto-88>>

      <tuple|<tuple|liquidus curve|binary liquid gas>|||<tuple|Liquidus
      curve|for a binary liquid\Ugas system>|<pageref|auto-89>>

      <tuple|<tuple|Azeotropic behavior>|<pageref|auto-90>>

      <tuple|<tuple|liquidus curve|binary liquid gas>|||<tuple|Liquidus
      curve|for a binary liquid\Ugas system>|<pageref|auto-92>>

      <tuple|<tuple|vaporus curve|binary liquid gas>|||<tuple|Vaporus
      curve|for a binary liquid\Ugas system>|<pageref|auto-93>>

      <tuple|<tuple|liquidus curve|binary liquid gas>|||<tuple|Liquidus
      curve|for a binary liquid\Ugas system>|<pageref|auto-95>>

      <tuple|<tuple|vaporus curve|binary liquid gas>|||<tuple|Vaporus
      curve|for a binary liquid\Ugas system>|<pageref|auto-96>>

      <tuple|<tuple|Azeotropy>|<pageref|auto-97>>

      <tuple|<tuple|Azeotrope>|<pageref|auto-98>>

      <tuple|<tuple|liquidus curve|binary liquid gas>|||<tuple|Liquius
      curve|for a binary liquid\Ugas system>|<pageref|auto-100>>

      <tuple|<tuple|vaporus curve|binary liquid gas>|||<tuple|Vaporus
      curve|for a binary liquid\Ugas system>|<pageref|auto-101>>

      <tuple|<tuple|Azeotrope|minimum-boiling>|<pageref|auto-102>>

      <tuple|<tuple|Azeotrope|vapor-pressure curve>|<pageref|auto-103>>

      <tuple|<tuple|Zeotropic behavior>|<pageref|auto-105>>

      <tuple|<tuple|phase diagram|binary solid gas>|||<tuple|Phase
      diagram|for a binary solid\Ugas system>|<pageref|auto-108>>

      <tuple|<tuple|Solid compound>|<pageref|auto-109>>

      <tuple|<tuple|Dissociation pressure of a hydrate>|<pageref|auto-110>>

      <tuple|<tuple|Pressure|dissociation, of a hydrate>|<pageref|auto-111>>

      <tuple|<tuple|Efflorescence>|<pageref|auto-113>>

      <tuple|<tuple|Deliquescence>|<pageref|auto-115>>

      <tuple|<tuple|phase diagram|high pressure>|||<tuple|Phase diagram|at
      high pressure>|<pageref|auto-118>>

      <tuple|<tuple|vaporus curve|high pressure>|||<tuple|Vaporus curve|at
      high pressure>|<pageref|auto-120>>

      <tuple|<tuple|liquidus curve|high pressure>|||<tuple|Liquidus curve|at
      high pressure>|<pageref|auto-121>>

      <tuple|<tuple|Critical|curve>|<pageref|auto-122>>

      <tuple|<tuple|Retrograde|condensation>|<pageref|auto-123>>

      <tuple|<tuple|Retrograde|vaporization>|<pageref|auto-124>>

      <tuple|<tuple|Equilibrium|gas--gas>|<pageref|auto-126>>

      <tuple|<tuple|Gas--gas immiscibility>|<pageref|auto-127>>

      <tuple|<tuple|phase diagram|ternary system>|||<tuple|Phase diagram|for
      a ternary system>|<pageref|auto-129>>

      <tuple|<tuple|tie line|ternary>|||<tuple|Tie line|on a ternary phase
      diagram>|<pageref|auto-133>>

      <tuple|<tuple|lever rule|ternary>|||<tuple|Lever rule|for a ternary
      system>|<pageref|auto-134>>

      <tuple|<tuple|miscibility gap|ternary>|||<tuple|Miscibility gap|in a
      ternary system>|<pageref|auto-135>>

      <tuple|<tuple|Plait point>|<pageref|auto-136>>

      <tuple|<tuple|conjugate|phases|ternary>|||<tuple|Conjugate|phases|in a
      ternary system>|<pageref|auto-137>>

      <tuple|<tuple|Eutonic|point>|<pageref|auto-141>>

      <tuple|<tuple|Eutonic|composition>|<pageref|auto-142>>

      <tuple|<tuple|Common ion effect>|<pageref|auto-143>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|13<space|2spc>The
      Phase Rule and Phase Diagrams> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      13.1<space|2spc>The Gibbs Phase Rule for Multicomponent Systems
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      <with|par-left|<quote|1tab>|13.1.1<space|2spc>Degrees of Freedom
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1tab>|13.1.2<space|2spc>Species approach to the
      phase rule <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <with|par-left|<quote|1tab>|13.1.3<space|2spc>Components approach to
      the phase rule <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|1tab>|13.1.4<space|2spc>Examples
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-20>>

      <with|par-left|<quote|2tab>|13.1.4.1<space|2spc>Example 1: liquid water
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21>>

      <with|par-left|<quote|2tab>|13.1.4.2<space|2spc>Example 2: carbon,
      oxygen, and carbon oxides <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-22>>

      <with|par-left|<quote|2tab>|13.1.4.3<space|2spc>Example 3: a solid salt
      and saturated aqueous solution <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-23>>

      <with|par-left|<quote|2tab>|13.1.4.4<space|2spc>Example 4: liquid water
      and water-saturated air <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-24>>

      <with|par-left|<quote|2tab>|13.1.4.5<space|2spc>Example 5: equilibrium
      between two solid phases and a gas phase
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-25>>

      13.2<space|2spc>Phase Diagrams: Binary Systems
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-27>

      <with|par-left|<quote|1tab>|13.2.1<space|2spc>Generalities
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-28>>

      <with|par-left|<quote|1tab>|13.2.2<space|2spc>Solid\Uliquid systems
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-35>>

      <with|par-left|<quote|1tab>|13.2.3<space|2spc>Partially-miscible
      liquids <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-61>>

      <with|par-left|<quote|1tab>|13.2.4<space|2spc>Liquid\Ugas systems with
      ideal liquid mixtures <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-75>>

      <with|par-left|<quote|1tab>|13.2.5<space|2spc>Liquid\Ugas systems with
      nonideal liquid mixtures <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-87>>

      <with|par-left|<quote|1tab>|13.2.6<space|2spc>Solid\Ugas systems
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-107>>

      <with|par-left|<quote|1tab>|13.2.7<space|2spc>Systems at high pressure
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-117>>

      13.3<space|2spc>Phase Diagrams: Ternary Systems
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-128>

      <with|par-left|<quote|1tab>|13.3.1<space|2spc>Three liquids
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-132>>

      <with|par-left|<quote|1tab>|13.3.2<space|2spc>Two solids and a solvent
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-139>>
    </associate>
  </collection>
</auxiliary>