<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;

    <assign|rsupchem|<macro|body|<rsup|><rsup|<math|<arg|body>>>>>
  </hide-preamble>

  <appendix|Standard Molar Thermodynamic Properties><label|app:props>

  <index-complex|<tuple|standard molar|properties, values of>||app:props
  idx1|<tuple|Standard molar|properties, values of>>The values in this table
  are for a temperature of <math|298.15 <text|K>> (<math|25.00 <degC>>) and
  the standard pressure <math|p<st>=1 <text|bar>>. Solute standard states are
  based on molality. A crystalline solid is denoted by cr.

  Most of the values in this table come from a project of the
  <index|CODATA>Committee on Data for Science and Technology (CODATA) to
  establish a set of recommended, internally consistent values of
  thermodynamic properties. The values of <math|<Delsub|f>H<st>> and
  <math|S<m><st>> shown with uncertainties are values recommended by
  CODATA.<footnote|Ref. <cite|cox-89>; also available online at
  <slink|http://www.codata.info/resources/databases/key1.html>.>

  \;

  <\big-table|<bktable3|<tformat|<cwith|2|-1|2|-1|cell-halign|L.>|<table|<row|<cell|Species>|<cell|<math|<dfrac|\<Delta\><rsub|<text|f>>*H<st>|<text|kJ>\<cdot\><text|mol><per>>>>|<cell|<math|<dfrac|S<m><st>|<text|J>\<cdot\><text|K><per>\<cdot\><text|mol><per>>>>|<cell|<math|<dfrac|\<Delta\><rsub|<text|f>>*G<st>|<text|kJ>\<cdot\><text|mol><per>>>>>|<row|<cell|<em|Inorganic
  substances>>|<cell|>|<cell|>|<cell|>>|<row|<cell|<math|<text|Ag>
  <around*|(|<text|cr>|)>>>|<cell|<math|0>>|<cell|<math|42.55\<pm\>0.20>>|<cell|<math|0>>>|<row|<cell|<math|<text|AgCl>
  <around*|(|<text|cr>|)>>>|<cell|<math|-127.01\<pm\>0.05>>|<cell|<math|96.25\<pm\>0.20>>|<cell|<math|-109.77>>>|<row|<cell|<math|<text|C>
  <around*|(|<text|cr>,<text|graphite>|)>>>|<cell|<math|0>>|<cell|<math|5.74\<pm\>0.10>>|<cell|<math|0>>>|<row|<cell|<math|<text|CO>
  <around*|(|<text|g>|)>>>|<cell|<math|-110.53\<pm\>0.17>>|<cell|197.660<math|\<pm\>0.004>>|<cell|<math|-137.17>>>|<row|<cell|<math|<text|CO<rsub|2>>
  <around*|(|<text|g>|)>>>|<cell|<math|-393.51\<pm\>0.13>>|<cell|<math|213.785\<pm\>0.010>>|<cell|<math|-394.41>>>|<row|<cell|<math|<text|Ca>
  <around*|(|<text|cr>|)>>>|<cell|<math|0>>|<cell|<math|41.59\<pm\>0.40>>|<cell|<math|0>>>|<row|<cell|<math|<text|CaCO<rsub|3>>
  <around*|(|<text|cr>,<text|calcite>|)>>>|<cell|<math|-1206.9>>|<cell|<math|92.9>>|<cell|<math|-1128.8>>>|<row|<cell|<math|<text|CaO>
  <around*|(|<text|cr>|)>>>|<cell|<math|-634.92\<pm\>0.90>>|<cell|<math|38.1\<pm\>0.4>>|<cell|<math|-603.31>>>|<row|<cell|<math|<text|Cl<rsub|2>>
  <around*|(|<text|g>|)>>>|<cell|<math|0>>|<cell|<math|223.081\<pm\>0.010>>|<cell|<math|0>>>|<row|<cell|<math|<text|F<rsub|2>>
  <around*|(|<text|g>|)>>>|<cell|<math|0>>|<cell|<math|202.791\<pm\>0.005>>|<cell|<math|0>>>|<row|<cell|<math|<text|H<rsub|2>>
  <around*|(|<text|g>|)>>>|<cell|<math|0>>|<cell|<math|130.680\<pm\>0.003>>|<cell|<math|0>>>|<row|<cell|<math|<text|HCl>
  <around*|(|<text|g>|)>>>|<cell|<math|-92.31\<pm\>0.10>>|<cell|<math|186.902\<pm\>0.005>>|<cell|<math|-95.30>>>|<row|<cell|<math|<text|HF>
  <around*|(|<text|g>|)>>>|<cell|<math|-273.30\<pm\>0.70>>|<cell|<math|173.779\<pm\>0.003>>|<cell|<math|-275.40>>>|<row|<cell|<math|<text|HI>
  <around*|(|<text|g>|)>>>|<cell|<math|26.50\<pm\>0.10>>|<cell|<math|206.590\<pm\>0.004>>|<cell|<math|1.70>>>|<row|<cell|<math|<text|H<rsub|2>O>
  <around*|(|<text|l>|)>>>|<cell|<math|-285.830\<pm\>0.040>>|<cell|<math|69.96\<pm\>0.03>>|<cell|<math|-237.16>>>|<row|<cell|<math|<text|H<rsub|2>O>
  <around*|(|<text|g>|)>>>|<cell|<math|-241.826\<pm\>0.040>>|<cell|<math|188.835\<pm\>0.010>>|<cell|<math|-228.58>>>|<row|<cell|<math|<text|H<rsub|2>S>
  <around*|(|<text|g>|)>>>|<cell|<math|-20.6\<pm\>0.5>>|<cell|<math|205.81\<pm\>0.05>>|<cell|<math|-33.44>>>|<row|<cell|<math|<text|Hg>
  <around*|(|<text|l>|)>>>|<cell|<math|0>>|<cell|<math|75.90\<pm\>0.12>>|<cell|<math|0>>>|<row|<cell|<math|<text|Hg>
  <around*|(|<text|g>|)>>>|<cell|<math|61.38\<pm\>0.04>>|<cell|<math|174.971\<pm\>0.005>>|<cell|<math|31.84>>>|<row|<cell|<math|<text|HgO><around*|(|<text|cr>,<text|red>|)>>>|<cell|<math|-90.79\<pm\>0.12>>|<cell|<math|70.25\<pm\>0.30>>|<cell|<math|-58.54>>>>>>>
    Inorganic substances, <math|<text|Ag> <around*|(|<text|cr>|)>> through
    <math|<text|HgO> <around*|(|<text|cr>,<text|red>|)>>
  </big-table>

  <\big-table|<bktable3|<tformat|<cwith|2|-1|2|-1|cell-halign|L.>|<table|<row|<cell|Species>|<cell|<math|<dfrac|\<Delta\><rsub|<text|f>>*H<st>|<text|kJ>\<cdot\><text|mol><per>>>>|<cell|<math|<dfrac|S<m><st>|<text|J>\<cdot\><text|K><per>\<cdot\><text|mol><per>>>>|<cell|<math|<dfrac|\<Delta\><rsub|<text|f>>*G<st>|<text|kJ>\<cdot\><text|mol><per>>>>>|<row|<cell|<em|Inorganic
  substances>>|<cell|>|<cell|>|<cell|>>|<row|<cell|<math|<text|Hg<rsub|2>Cl<rsub|2>>
  <around*|(|<text|cr>|)>>>|<cell|<math|-265.37\<pm\>0.40>>|<cell|<math|191.6\<pm\>0.8>>|<cell|<math|-210.72>>>|<row|<cell|<math|<text|I<rsub|2>>
  <around*|(|<text|cr>|)>>>|<cell|<math|0>>|<cell|<math|116.14\<pm\>0.30>>|<cell|<math|0>>>|<row|<cell|<math|<text|K>
  <around*|(|<text|cr>|)>>>|<cell|<math|0>>|<cell|<math|64.68\<pm\>0.20>>|<cell|<math|0>>>|<row|<cell|<math|<text|KI>
  <around*|(|<text|cr>|)>>>|<cell|<math|-327.90>>|<cell|<math|106.37>>|<cell|<math|-323.03>>>|<row|<cell|<math|<text|KOH>
  <around*|(|<text|cr>|)>>>|<cell|<math|-424.72>>|<cell|<math|78.90>>|<cell|<math|-378.93>>>|<row|<cell|<math|<text|N<rsub|2>>
  <around*|(|<text|g>|)>>>|<cell|<math|0>>|<cell|<math|191.609\<pm\>0.004>>|<cell|<math|0>>>|<row|<cell|<math|<text|NH<rsub|3>>
  <around*|(|<text|g>|)>>>|<cell|<math|-45.94\<pm\>0.35>>|<cell|<math|192.77\<pm\>0.05>>|<cell|<math|-16.41>>>|<row|<cell|<math|<text|NO<rsub|2>>
  <around*|(|<text|g>|)>>>|<cell|<math|33.10>>|<cell|<math|240.04>>|<cell|<math|51.22>>>|<row|<cell|<math|<text|N<rsub|2>O<rsub|4>>
  <around*|(|<text|g>|)>>>|<cell|<math|9.08>>|<cell|<math|304.38>>|<cell|<math|97.72>>>|<row|<cell|<math|<text|Na>
  <around*|(|<text|cr>|)>>>|<cell|<math|0>>|<cell|<math|51.30>>|<cell|<math|0>>>|<row|<cell|<math|<text|NaCl>
  <around*|(|<text|cr>|)>>>|<cell|<math|-411.12>>|<cell|<math|72.10>>|<cell|<math|-384.02>>>|<row|<cell|<math|<text|O<rsub|2>>
  <around*|(|<text|g>|)>>>|<cell|<math|0>>|<cell|<math|205.152\<pm\>0.005>>|<cell|<math|0>>>|<row|<cell|<math|<text|O<rsub|3>>
  <around*|(|<text|g>|)>>>|<cell|<math|142.67>>|<cell|<math|238.92>>|<cell|<math|163.14>>>|<row|<cell|<math|<text|P>
  <around*|(|<text|cr>,<text|white>|)>>>|<cell|<math|0>>|<cell|<math|41.09\<pm\>0.25>>|<cell|<math|0>>>|<row|<cell|<math|<text|S>
  <around*|(|<text|cr>,<text|rhombic>|)>>>|<cell|<math|0>>|<cell|<math|32.054\<pm\>0.050>>|<cell|<math|0>>>|<row|<cell|<math|<text|SO<rsub|2>>
  <around*|(|<text|g>|)>>>|<cell|<math|-296.81\<pm\>0.20>>|<cell|<math|248.223\<pm\>0.050>>|<cell|<math|-300.09>>>|<row|<cell|<math|<text|Si>
  <around*|(|<text|cr>|)>>>|<cell|<math|0>>|<cell|<math|18.81\<pm\>0.08>>|<cell|<math|0>>>|<row|<cell|<math|<text|SiF<rsub|4>>
  <around*|(|<text|g>|)>>>|<cell|<math|-1615.0\<pm\>0.8>>|<cell|<math|282.76\<pm\>0.50>>|<cell|<math|-1572.8>>>|<row|<cell|<math|<text|SiO<rsub|2>>
  <around*|(|<text|cr>,<text|<math|\<alpha\>>-quartz>|)>>>|<cell|<math|-910.7\<pm\>1.0>>|<cell|<math|41.46\<pm\>0.20>>|<cell|<math|-856.3>>>|<row|<cell|<math|<text|Zn>
  <around*|(|<text|cr>|)>>>|<cell|<math|0>>|<cell|<math|41.63\<pm\>0.15>>|<cell|<math|0>>>|<row|<cell|<math|<text|ZnO>
  <around*|(|<text|cr>|)>>>|<cell|<math|-350.46\<pm\>0.27>>|<cell|<math|43.65\<pm\>0.40>>|<cell|<math|-320.48>>>>>>>
    Inorganic substances (cont'd), <math|<text|HgO>
    <around*|(|<text|cr>,<text|red>|)>> through <math|<text|ZnO>
    <around*|(|<text|cr>|)>>
  </big-table>

  <\big-table|<bktable3|<tformat|<cwith|2|-1|2|-1|cell-halign|L.>|<table|<row|<cell|Species>|<cell|<math|<dfrac|\<Delta\><rsub|<text|f>>*H<st>|<text|kJ>\<cdot\><text|mol><per>>>>|<cell|<math|<dfrac|S<m><st>|<text|J>\<cdot\><text|K><per>\<cdot\><text|mol><per>>>>|<cell|<math|<dfrac|\<Delta\><rsub|<text|f>>*G<st>|<text|kJ>\<cdot\><text|mol><per>>>>>|<row|<cell|<em|Organic
  compounds>>|<cell|>|<cell|>|<cell|>>|<row|<cell|<chemspec|CH<rsub|4>|g>>|<cell|<math|-74.87>>|<cell|<math|186.25>>|<cell|<math|-50.77>>>|<row|<cell|<chemspec|CH<rsub|3>OH|l>>|<cell|<math|-238.9>>|<cell|<math|127.2>>|<cell|<math|-166.6>>>|<row|<cell|<chemspec|CH<rsub|3>CH<rsub|2>OH|l>>|<cell|<math|-277.0>>|<cell|<math|159.9>>|<cell|<math|-173.8>>>|<row|<cell|<chemspec|C<rsub|2>H<rsub|2>|g>>|<cell|<math|226.73>>|<cell|<math|200.93>>|<cell|<math|209.21>>>|<row|<cell|<chemspec|C<rsub|2>H<rsub|4>|g>>|<cell|<math|52.47>>|<cell|<math|219.32>>|<cell|<math|68.43>>>|<row|<cell|<chemspec|C<rsub|2>H<rsub|6>|g>>|<cell|<math|-83.85>>|<cell|<math|229.6>>|<cell|<math|-32.00>>>|<row|<cell|<chemspec|C<rsub|2>H<rsub|8>|g>>|<cell|<math|-104.7>>|<cell|<math|270.31>>|<cell|<math|-24.3>>>|<row|<cell|<chemspec|C<rsub|2>H<rsub|6>|<math|<text|l>,<text|benzene>>>>|<cell|<math|49.04>>|<cell|<math|173.26>>|<cell|<math|124.54>>>>>>>
    Organic compounds, <math|<text|CH<rsub|4>> <around*|(|<text|g>|)>>
    through <math|<text|C<rsub|6>H<rsub|6>>
    <around*|(|<text|l>,<text|benzene>|)>>
  </big-table>

  <\big-table|<bktable3|<tformat|<cwith|2|-1|2|-1|cell-halign|L.>|<cwith|9|15|2|2|cell-halign|L.>|<table|<row|<cell|Species>|<cell|<math|<dfrac|\<Delta\><rsub|<text|f>>*H<st>|<text|kJ>\<cdot\><text|mol><per>>>>|<cell|<math|<dfrac|S<m><st>|<text|J>\<cdot\><text|K><per>\<cdot\><text|mol><per>>>>|<cell|<math|<dfrac|\<Delta\><rsub|<text|f>>*G<st>|<text|kJ>\<cdot\><text|mol><per>>>>>|<row|<cell|<em|Ionic
  solutes>>|<cell|>|<cell|>|<cell|>>|<row|<cell|<chemspec|Ag<rsupchem|+>|aq>>|<cell|<math|105.79\<pm\>0.08>>|<cell|<math|73.45\<pm\>0.40>>|<cell|<math|77.10>>>|<row|<cell|<chemspec|<rsup|<math|>>CO<rsub|3><rsupchem|2->|>>|<cell|<math|-675.23\<pm\>0.25>>|<cell|<math|-50.0\<pm\>1.0>>|<cell|<math|-527.90>>>|<row|<cell|<chemspec|Ca<rsupchem|2+>|aq>>|<cell|<math|-543.0\<pm\>1.0>>|<cell|<math|-56.2\<pm\>1.0>>|<cell|<math|-552.8>>>|<row|<cell|<chemspec|Cl<rsupchem|->|aq>>|<cell|<math|-167.08\<pm\>0.10>>|<cell|<math|56.60\<pm\>0.20>>|<cell|<math|-131.22>>>|<row|<cell|<chemspec|F<rsupchem|->|aq>>|<cell|<math|-335.35\<pm\>0.65>>|<cell|<math|-13.8\<pm\>0.8>>|<cell|<math|-281.52>>>|<row|<cell|<chemspec|H<rsupchem|+>|aq>>|<cell|<math|0>>|<cell|<math|0>>|<cell|<math|0>>>|<row|<cell|<chemspec|HCO<rsub|3><rsupchem|->|aq>>|<cell|<math|-689.93\<pm\>2.0>>|<cell|<math|98.4\<pm\>0.5>>|<cell|<math|-586.90>>>|<row|<cell|<chemspec|HS<rsupchem|->|aq>>|<cell|<math|-16.3\<pm\>1.5>>|<cell|<math|67\<pm\>5>>|<cell|<math|12.2>>>|<row|<cell|<chemspec|HSO<rsub|4><rsupchem|->|aq>>|<cell|<math|-886.9\<pm\>1.0>>|<cell|<math|131.7\<pm\>3.0>>|<cell|<math|-755.4>>>|<row|<cell|<chemspec|Hg<rsub|2><rsupchem|2+>|aq>>|<cell|<math|116.87\<pm\>0.50>>|<cell|<math|65.74\<pm\>0.80>>|<cell|<math|153.57>>>|<row|<cell|<chemspec|I<rsupchem|->|aq>>|<cell|<math|-56.78\<pm\>0.05>>|<cell|<math|106.45\<pm\>0.30>>|<cell|<math|-51.72>>>|<row|<cell|<chemspec|K<rsupchem|+>|aq>>|<cell|<math|-252.14\<pm\>0.08>>|<cell|<math|101.20\<pm\>0.20>>|<cell|<math|-282.52>>>|<row|<cell|<chemspec|NH<rsub|4><rsupchem|+>|aq>>|<cell|<math|-133.26\<pm\>0.25>>|<cell|<math|111.17\<pm\>0.40>>|<cell|<math|-79.40>>>|<row|<cell|<chemspec|NO<rsub|3><rsupchem|->|aq>>|<cell|<math|-206.85\<pm\>0.40>>|<cell|<math|146.70\<pm\>0.40>>|<cell|<math|-110.84>>>|<row|<cell|<chemspec|Na<rsupchem|+>|aq>>|<cell|<math|-240.34\<pm\>0.06>>|<cell|<math|58.45\<pm\>0.15>>|<cell|<math|-261.90>>>|<row|<cell|<chemspec|OH<rsupchem|->|aq>>|<cell|<math|-230.015\<pm\>0.040>>|<cell|<math|-10.90\<pm\>0.20>>|<cell|<math|-157.24>>>|<row|<cell|<chemspec|S<rsupchem|2->|aq>>|<cell|<math|33.1>>|<cell|<math|-14.6>>|<cell|<math|86.0>>>|<row|<cell|<chemspec|SO<rsub|4><rsupchem|2->|aq>>|<cell|<math|-909.34\<pm\>0.40>>|<cell|<math|18.50\<pm\>0.40>>|<cell|<math|-744.00>>>>>>>
    Ionic solutes, <chemspec|Ag<rsup|<math|+>>|aq> through
    <chemspec|SO<rsub|2><rsup|><rsup|<math|2->>|aq>
  </big-table>

  <index-complex|<tuple|standard molar|properties, values of>||app:props
  idx1|<tuple|Standard molar|properties, values of>>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|14>
    <associate|page-first|416>
    <associate|preamble|false>
    <associate|section-nr|10>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|app:props|<tuple|A|405>>
    <associate|auto-1|<tuple|A|405>>
    <associate|auto-2|<tuple|<tuple|standard molar|properties, values
    of>|405>>
    <associate|auto-3|<tuple|CODATA|405>>
    <associate|auto-4|<tuple|1|405>>
    <associate|auto-5|<tuple|2|406>>
    <associate|auto-6|<tuple|3|406>>
    <associate|auto-7|<tuple|4|407>>
    <associate|auto-8|<tuple|<tuple|standard molar|properties, values of>|?>>
    <associate|footnote-1|<tuple|1|405>>
    <associate|footnr-1|<tuple|1|405>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      cox-89
    </associate>
    <\associate|idx>
      <tuple|<tuple|standard molar|properties, values of>||app:props
      idx1|<tuple|Standard molar|properties, values of>|<pageref|auto-2>>

      <tuple|<tuple|CODATA>|<pageref|auto-3>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        Inorganic substances, <with|mode|<quote|math>|<with|mode|<quote|text>|Ag>
        <around*|(|<with|mode|<quote|text>|cr>|)>> through
        <with|mode|<quote|math>|<with|mode|<quote|text>|HgO>
        <around*|(|<with|mode|<quote|text>|cr>,<with|mode|<quote|text>|red>|)>>
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        Inorganic substances (cont'd), <with|mode|<quote|math>|<with|mode|<quote|text>|HgO>
        <around*|(|<with|mode|<quote|text>|cr>,<with|mode|<quote|text>|red>|)>>
        through <with|mode|<quote|math>|<with|mode|<quote|text>|ZnO>
        <around*|(|<with|mode|<quote|text>|cr>|)>>
      </surround>|<pageref|auto-5>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        Organic compounds, <with|mode|<quote|math>|<with|mode|<quote|text>|CH<rsub|4>>
        <around*|(|<with|mode|<quote|text>|g>|)>> through
        <with|mode|<quote|math>|<with|mode|<quote|text>|C<rsub|6>H<rsub|6>>
        <around*|(|<with|mode|<quote|text>|l>,<with|mode|<quote|text>|benzene>|)>>
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|4>|>
        Ionic solutes, <with|mode|<quote|math>|<with|mode|<quote|text>|Ag<rsup|<with|mode|<quote|math>|+>>>
        <around*|(|<with|mode|<quote|text>|aq>|)>> through
        <with|mode|<quote|math>|<with|mode|<quote|text>|SO<rsub|2><rsup|><rsup|<with|mode|<quote|math>|2->>>
        <around*|(|<with|mode|<quote|text>|aq>|)>>
      </surround>|<pageref|auto-7>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix
      A<space|2spc>Standard Molar Thermodynamic Properties>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>