<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|number-long-article|style-bk>>

<\body>
  <\hide-preamble>
    \;
  </hide-preamble>

  <new-page*><section|Problems>

  <\problem>
    Let <math|X> represent the quantity <math|V<rsup|2>> with dimensions
    <math|<around*|(|<text|length>|)><rsup|6>>. Give a reason that <math|X>
    is or is not an extensive property. Give a reason that <math|X> is or is
    not an intensive property.
  </problem>

  <\problem>
    Calculate the <em|relative uncertainty> (the uncertainty divided by the
    value) for each of the measurement methods listed in Table <vpageref|c2
    tab measurement-methods>, using the typical values shown. For each of the
    five physical quantities listed, which measurement method has the
    smallest relative uncertainty?
  </problem>

  <\problem>
    <label|prb:2-He-virial>Table <reference|c2 tab-he-virial> lists data
    obtained from a constant-volume gas thermometer containing samples of
    varying amounts of helium maintained at a certain fixed temperature
    <math|T<rsub|2>> in the gas bulb.<footnote|Ref. <cite|berry-79>.> The
    molar volume <math|V<rsub|<text|m>>> of each sample was evaluated from
    its pressure in the bulb at a reference temperature of
    <math|T<rsub|1>=7.1992 <text|K>>, corrected for gas nonideality with the
    known value of the second virial coefficient at that temperature.

    Use these data and Eq. <vpageref|c2 eq virial-vm> to evaluate
    <math|T<rsub|2>> and the second virial coefficient of helium at
    temperature <math|T<rsub|2>>. (You can assume the third and higher virial
    coefficients are negligible.)
  </problem>

  <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|12|12|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|<around*|(|<frac|1|V<rsub|<text|m>>>|)>/<around*|(|10<rsup|2>
  <frac|<text|mol>|<text|m><rsup|3>>|)>>>|<cell|<math|<around*|(|<frac|p<rsub|2>*V<rsub|<text|m>>|R>|)>/<around*|(|<text|K>|)>>>>|<row|<cell|1.0225>|<cell|2.7106>>|<row|<cell|1.3202>|<cell|2.6994>>|<row|<cell|1.5829>|<cell|2.6898>>|<row|<cell|1.9042>|<cell|2.6781>>|<row|<cell|2.4572>|<cell|2.6580>>|<row|<cell|2.8180>|<cell|2.6447>>|<row|<cell|3.4160>|<cell|2.6228>>|<row|<cell|3.6016>|<cell|2.6162>>|<row|<cell|4.1375>|<cell|2.5965>>|<row|<cell|4.6115>|<cell|2.5790>>|<row|<cell|5.1717>|<cell|2.5586>>>>>>
    <label|c2 tab-he-virial><label|tbl:2-He-virial>Helium at a fixed
    temperature
  </big-table>

  <\problem>
    Discuss the proposition that, to a certain degree of approximation, a
    living organism is a steady-state system.
  </problem>

  <\problem>
    The value of <math|\<Delta\>*U> for the formation of one mole of
    crystalline potassium iodide from its elements at <math|25
    <rsup|\<circ\>><text|C>> and <math|1 <text|bar>> is <math|-327.9
    <text|kJ>>. Calculate <math|\<Delta\>*m> for this process. Comment on the
    feasibility of measuring this mass change.
  </problem>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|2>
    <associate|page-first|?>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|6>
    <associate|subsection-nr|2>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|7|0>>
    <associate|auto-2|<tuple|7.1|0>>
    <associate|c2 tab-he-virial|<tuple|7.1|0>>
    <associate|footnote-7.1|<tuple|7.1|0>>
    <associate|footnr-7.1|<tuple|7.1|0>>
    <associate|prb:2-He-virial|<tuple|7.3|?>>
    <associate|tbl:2-He-virial|<tuple|7.1|0>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      berry-79
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.1>|>
        Helium at a fixed temperature
      </surround>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      7<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>