<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|number-long-article|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <chapter|Systems and Their Properties><glossary-line|<strong|Chapter
  2>><label|c2><label|Chap. 2>

  This chapter begins by explaining some basic terminology of thermodynamics.
  It discusses macroscopic properties of matter in general and properties
  distinguishing different physical states of matter in particular. Virial
  equations of state of a pure gas are introduced. The chapter goes on to
  discuss some basic macroscopic properties and their measurement. Finally,
  several important concepts needed in later chapters are described:
  thermodynamic states and state functions, independent and dependent
  variables, processes, and internal energy.

  <section|The System, Surroundings, and Boundary><label|2-system><label|c2
  sec ssb>

  Chemists are interested in systems containing matter\Vthat which has mass
  and occupies physical space. Classical thermodynamics looks at
  <em|macroscopic> aspects of matter. It deals with the properties of
  aggregates of vast numbers of microscopic particles (molecules, atoms, and
  ions). The macroscopic viewpoint, in fact, treats matter as a
  <em|continuous> material medium rather than as the collection of discrete
  microscopic particles we know are actually present. Although this book is
  an exposition of classical thermodynamics, at times it will point out
  connections between macroscopic properties and molecular structure and
  behavior.

  A thermodynamic <index|System><strong|system><glossary|system> is any
  three-dimensional region of physical space on which we wish to focus our
  attention. Usually we consider only one system at a time and call it simply
  \Pthe system.\Q The rest of the physical universe constitutes the
  <index|Surroundings><strong|surroundings><glossary|surroundings> of the
  system.

  The <index|Boundary><strong|boundary><glossary|boundary> is the closed
  three-dimensional surface that encloses the system and separates it from
  the surroundings. The boundary may (and usually does) coincide with real
  physical surfaces: the interface between two phases, the inner or outer
  surface of the wall of a flask or other vessel, and so on. Alternatively,
  part or all of the boundary may be an imagined intangible surface in space,
  unrelated to any physical structure. The size and shape of the system, as
  defined by its boundary, may change in time. In short, our choice of the
  three-dimensional region that constitutes the system is arbitrary\Vbut it
  is essential that we know exactly what this choice is.

  We usually think of the system as a part of the physical universe that we
  are able to influence only indirectly through its interaction with the
  surroundings, and the surroundings as the part of the universe that we are
  able to directly manipulate with various physical devices under our
  control. That is, we (the experimenters) are part of the surroundings, not
  the system.

  For some purposes we may wish to treat the system as being divided into
  <index|Subsystem><em|subsystems>, or to treat the combination of two or
  more systems as a <index|Supersystem><em|supersystem>.

  If over the course of time matter is transferred in either direction across
  the boundary, the system is <subindex|System|open><strong|open><glossary|open>;
  otherwise it is <subindex|System|closed><strong|closed><glossary|closed>.
  If the system is open, matter may pass through a stationary boundary, or
  the boundary may move through matter that is fixed in space.

  If the boundary allows heat transfer between the system and surroundings,
  the boundary is <index|Diathermal boundary><subindex|Boundary|diathermal><strong|diathermal><glossary|diathermal>.
  An <subindex|Adiabatic|boundary><subindex|Boundary|adiabatic><strong|adiabatic><glossary|adiabatic><\footnote>
    Greek: <em|impassable>
  </footnote> boundary, on the other hand, is a boundary that does not allow
  heat transfer. We can, in principle, ensure that the boundary is adiabatic
  by surrounding the system with an adiabatic wall\Vone with perfect thermal
  insulation and a perfect radiation shield.

  An <index|Isolated system><subindex|System|isolated><strong|isolated><glossary|isolated>
  system is one that exchanges no matter, heat, or work with the
  surroundings, so that the system's mass and total energy remain constant
  over time.<\footnote>
    The energy in this definition of an isolated system is measured in a
    local <index|Reference frame><subindex|Frame|reference>reference frame,
    as will be explained in Sec. <reference|c2 sec eots-internal>.
  </footnote> A closed system with an adiabatic boundary, constrained to do
  no work and to have no work done on it, is an isolated system.

  <\quote-env>
    The constraints required to prevent work usually involve forces between
    the system and surroundings. In that sense a system may interact with the
    surroundings even though it is isolated. For instance, a gas contained
    within rigid, thermally-insulated walls is an isolated system; the gas
    exerts a force on each wall, and the wall exerts an equal and opposite
    force on the gas. An isolated system may also experience a constant
    external field, such as a gravitational field.
  </quote-env>

  The term <index|Body><strong|body><glossary|body> usually implies a system,
  or part of a system, whose mass and chemical composition are constant over
  time.

  <subsection|Extensive and intensive properties><label|2-extensive
  \ intensive><label|c2 sec ssb-extintprop>

  A quantitative <em|property> of a system describes some macroscopic feature
  that, although it may vary with time, has a particular value at any given
  instant of time.

  Table <reference|c2 tab symbols> lists the symbols of some of the
  properties discussed in this chapter and the <acronym|SI> units in which
  they may be expressed. A much more complete table is found in Appendix
  <reference|app:sym>.<float|float|thb|<\big-table>
    <bktable3|<tformat|<table|<row|<cell|Symbol>|<cell|Physical
    quantity>|<cell|<acronym|SI> unit>>|<row|<cell|<math|E>>|<cell|energy>|<cell|J>>|<row|<cell|<math|m>>|<cell|mass>|<cell|kg>>|<row|<cell|<math|n>>|<cell|amount
    of substance>|<cell|mol>>|<row|<cell|<math|p>>|<cell|pressure>|<cell|Pa>>|<row|<cell|<math|T>>|<cell|thermodynamic
    temperature>|<cell|K>>|<row|<cell|<math|V>>|<cell|volume>|<cell|m<rsup|<math|3>>>>|<row|<cell|<math|U>>|<cell|internal
    energy>|<cell|J>>|<row|<cell|<math|\<rho\>>>|<cell|density>|<cell|<math|<frac|<text|kg>|<text|m><rsup|3>>>>>>>>
  <|big-table>
    <label|c2 tab symbols>Symbols and SI units for some common properties
  </big-table>>

  Most of the properties studied by thermodynamics may be classified as
  either <em|extensive> or <em|intensive>. We can distinguish these two types
  of properties by the following considerations.

  If we imagine the system to be divided by an imaginary surface into two
  parts, any property of the system that is the sum of the property for the
  two parts is an <index|Extensive property><subindex|Property|extensive><strong|extensive
  property><glossary|extensive property>. That is, an additive property is
  extensive. Examples are mass, volume, amount, energy, and the surface area
  of a solid.

  <\quote-env>
    Sometimes a more restricted definition of an extensive property is used:
    The property must be not only additive, but also proportional to the mass
    or the amount when intensive properties remain constant. According to
    this definition, mass, volume, amount, and energy are extensive, but
    surface area is not.
  </quote-env>

  If we imagine a homogeneous region of space to be divided into two or more
  parts of arbitrary size, any property that has the same value in each part
  and the whole is an <index|Intensive property><subindex|Property|intensive><strong|intensive
  property><glossary|intensive property>; for example density, concentration,
  pressure (in a fluid), and temperature. The value of an intensive property
  is the same everywhere in a homogeneous region, but may vary from point to
  point in a heterogeneous region\Vit is a <em|local> property.

  Since classical thermodynamics treats matter as a continuous medium,
  whereas matter actually contains discrete microscopic particles, the value
  of an intensive property at a point is a statistical average of the
  behavior of many particles. For instance, the <index|Density>density of a
  gas at one point in space is the average mass of a small volume element at
  that point, large enough to contain many molecules, divided by the volume
  of that element.

  Some properties are defined as the ratio of two extensive quantities. If
  both extensive quantities refer to a homogeneous region of the system or to
  a small volume element, the ratio is an <em|intensive> property. For
  example <index|Concentration>concentration, defined as the ratio
  amount/volume, is intensive. A mathematical derivative of one such
  extensive quantity with respect to another is also intensive.

  A special case is an extensive quantity divided by the mass, giving an
  intensive <subindex|Specific|quantity><subindex|Quantity|specific><strong|specific
  quantity><glossary|specific quantity>; for example

  <\equation>
    <subindex|Specific|volume><subindex|Volume|specific><around*|(|<text|Specific
    volume>|)>=<frac|V|m>=<frac|1|\<rho\>>
  </equation>

  If the symbol for the extensive quantity is a capital letter, it is
  customary to use the corresponding lower-case letter as the symbol for the
  specific quantity. Thus the symbol for specific volume is <math|v>.

  Another special case encountered frequently in this book is an extensive
  property for a pure, homogeneous substance divided by the amount <math|n>.
  The resulting intensive property is called, in general, a
  <subindex|Molar|quantity><subindex|Quantity|molar><strong|molar
  quantity><glossary|molar quantity> or molar property. <label|c2
  molar-quantity-subscript>To symbolize a molar quantity, this book follows
  the recommendation of the <acronym|IUPAC>: The symbol of the extensive
  quantity is followed by subscript <math|<text|m>>, and optionally the
  identity of the substance is indicated either by a subscript or a formula
  in parentheses. Examples are<math|<subindex|Volume|molar>>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<around*|(|<text|Molar
    volume>|)>>|<cell|=>|<cell|<frac|V|n>=V<rsub|<text|m>><eq-number><label|c2
    eq molar-volume>>>|<row|<cell|<around*|(|<text|Molar volume of substance
    <math|i>>|)>>|<cell|=>|<cell|<frac|V|n<rsub|i>>=V<rsub|<text|m>,i><eq-number>>>|<row|<cell|<around*|(|<text|Molar
    volume of <math|<text|H><rsub|2><text|O>>>|)>>|<cell|=>|<cell|V<rsub|<text|m>>
    <around*|(|<text|H><rsub|2><text|O>|)><eq-number>>>>>
  </eqnarray*>

  In the past, especially in the United States, molar quantities were
  commonly denoted with an overbar (e.g., <math|<wide|V|\<bar\>><rsub|i>>).

  <section|Phases and Physical States of Matter><label|c2 sec states>

  A <index|Phase><strong|phase><glossary|phase> is a region of the system in
  which each intensive property (such as temperature and pressure) has at
  each instant either the same value throughout (a <em|uniform> or
  <index|Homogeneous phase><subindex|Phase|homogeneous><em|homogeneous>
  phase), or else a value that varies continuously from one point to another.
  Whenever this book mentions a phase, it is a <em|uniform> phase unless
  otherwise stated. Two different phases meet at an <index|Interface
  surface><strong|interface surface><glossary|interface surface>, where
  intensive properties have a discontinuity or change value over a small
  distance.

  Some intensive properties (e.g., refractive index and polarizability) can
  have directional characteristics. A uniform phase may be either
  <index|Isotropic phase><subindex|Phase|isotropic><em|isotropic>, exhibiting
  the same values of these properties in all directions, or
  <index|Anisotropic phase><subindex|Phase|anisotropic><em|anisotropic>, as
  in the case of some solids and liquid crystals. A vacuum is a uniform phase
  of zero density.

  Suppose we have to deal with a <em|nonuniform> region in which intensive
  properties vary continuously in space along one or more directions\Vfor
  example, a tall column of gas in a gravitational field whose density
  decreases with increasing altitude. There are two ways we may treat such a
  nonuniform, continuous region: either as a single nonuniform phase, or else
  as an infinite number of uniform phases, each of infinitesimal size in one
  or more dimensions.

  <subsection|Physical states of matter><label|2-physical states><label|c2
  sec states-physical>

  <index|Physical state><subindex|State|physical><index-complex|<tuple|State|aggregation>|||<tuple|State|of
  aggregation>>We are used to labeling phases by physical state, or state of
  aggregation. It is common to say that a phase is a <em|solid> if it is
  relatively rigid, a <em|liquid> if it is easily deformed and relatively
  incompressible, and a <em|gas> if it is easily deformed and easily
  compressed. Since these descriptions of responses to external forces differ
  only in degree, they are inadequate to classify intermediate cases.

  A more rigorous approach is to make a primary distinction between a
  <em|solid> and a <em|fluid>, based on the phase's response to an applied
  shear stress, and then use additional criteria to classify a fluid as a
  <em|liquid>, <em|gas>, or <em|supercritical fluid>. <index|Shear
  stress><strong|Shear stress><glossary|shear stress><label|2-shear stress>
  is a tangential force per unit area that is exerted on matter on one side
  of an interior plane by the matter on the other side. We can produce shear
  stress in a phase by applying tangential forces to parallel surfaces of the
  phase as shown in Fig. <reference|c2 fig shear>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|02-SUP/SHEAR.eps|92pt|47pt||>>
        <label|c2 fig shear>Experimental procedure for producing shear stress
        in a phase (shaded). Blocks at the upper and lower surfaces of the
        phase are pushed in opposite directions, dragging the adjacent
        portions of the phase with them.
      </big-figure>
    </framed>
  </float>

  <\itemize-dot>
    <item>A <index|solid><strong|solid><glossary|solid> responds to shear
    stress by undergoing momentary relative motion of its parts, resulting in
    <index|Deformation><em|deformation>\Va change of shape. If the applied
    shear stress is constant and small (not large enough to cause creep or
    fracture), the solid quickly reaches a certain degree of deformation that
    depends on the magnitude of the stress and maintains this deformation
    without further change as long as the shear stress continues to be
    applied. On the microscopic level, deformation requires relative movement
    of adjacent layers of particles (atoms, molecules, or ions). The shape of
    an unstressed solid is determined by the attractive and repulsive forces
    between the particles; these forces make it difficult for adjacent layers
    to slide past one another, so that the solid resists deformation.

    <item>A <index|Fluid><strong|fluid><glossary|fluid> responds to shear
    stress differently, by undergoing continuous relative motion (flow) of
    its parts. The flow continues as long as there is any shear stress, no
    matter how small, and stops only when the shear stress is removed.
  </itemize-dot>

  Thus, a constant applied shear stress causes a fixed deformation in a solid
  and continuous flow in a fluid. We say that a phase under constant shear
  stress is a solid if, after the initial deformation, we are unable to
  detect a further change in shape during the period we observe the phase.

  Usually this criterion allows us to unambiguously classify a phase as
  either a solid or a fluid. Over a sufficiently long time period, however,
  detectable flow is likely to occur in <em|any> material under shear stress
  of <em|any> magnitude. Thus, the distinction between solid and fluid
  actually depends on the time scale of observation. This fact is obvious
  when we observe the behavior of certain materials (such as Silly Putty, or
  a paste of water and cornstarch) that exhibit solid-like behavior over a
  short time period and fluid-like behavior over a longer period. Such
  materials, that resist deformation by a suddenly-applied shear stress but
  undergo flow over a longer time period, are called <index|Viscoelastic
  solid><subindex|Solid|viscoelastic><em|viscoelastic solids>.

  <subsection|Phase coexistence and phase transitions><label|2-phase
  coexistence><label|c2 sec states-transitions>

  This section considers some general characteristics of systems containing
  more than one phase.

  Suppose we bring two uniform phases containing the same constituents into
  physical contact at an interface surface. If we find that the phases have
  no tendency to change over time while both have the same temperature and
  the same pressure, but differ in other intensive properties such as density
  and composition, we say that they <subindex|Phase|coexistence><strong|coexist><glossary|coexist>
  in equilibrium with one another. The conditions for such phase coexistence
  are the subject of later sections in this book, but they tend to be quite
  restricted. For instance, the liquid and gas phases of pure
  <math|<text|H><rsub|2><text|O>> at a pressure of <math|1 <text|bar>> can
  coexist at only one temperature, <math|99.61 <rsup|\<circ\>><text|C>>.

  A <subindex|Phase|transition><strong|phase transition><glossary|phase
  transition> of a pure substance is a change over time in which there is a
  continuous transfer of the substance from one phase to another. Eventually
  one phase can completely disappear, and the substance has been completely
  transferred to the other phase. If both phases coexist in equilibrium with
  one another, and the temperature and pressure of both phases remain equal
  and constant during the phase transition, the change is an
  <subsubindex|Phase|transition|equilibrium><em|equilibrium phase
  transition>. For example, <math|<text|H><rsub|2><text|O>> at <math|99.61
  <rsup|\<circ\>><text|C>> and <math|1 <text|bar>> can undergo an equilibrium
  phase transition from liquid to gas (vaporization) or from gas to liquid
  (condensation). During an equilibrium phase transition, there is a transfer
  of energy between the system and its surroundings by means of heat or work.

  <subsection|Fluids><label|2-fluids><label|c2 sec states-fluids>

  <index-complex|fluid||c2 fluids|<tuple|Fluids>>It is usual to classify a
  fluid as either a <em|liquid> or a <em|gas>. The distinction is important
  for a pure substance because the choice determines the treatment of the
  phase's standard state (see Sec. <reference|c7 sec ssps>). To complicate
  matters, a fluid at high pressure may be a <index|Supercritical
  fluid><subindex|Fluid|supercritical><em|supercritical fluid>. Sometimes a
  <index|Plasma><em|plasma> (a highly ionized, electrically conducting
  medium) is considered a separate kind of fluid state; it is the state found
  in the earth's ionosphere and in stars.

  In general, and provided the pressure is not high enough for supercritical
  phenomena to exist\Vusually true of pressures below <math|25 <text|bar>>
  except in the case of <math|<text|He>> or <math|<text|H><rsub|2>>\Vwe can
  make the distinction between liquid and gas simply on the basis of density.
  A <index|Liquid><strong|liquid><glossary|liquid> has a relatively high
  density that is insensitive to changes in temperature and pressure. A
  <index|Gas><strong|gas><glossary|gas>, on the other hand, has a relatively
  low density that is sensitive to temperature and pressure and that
  approaches zero as pressure is reduced at constant temperature.

  This simple distinction between liquids and gases fails at high pressures,
  where liquid and gas phases may have similar densities at the same
  temperature. Figure <reference|c2 fig phases> shows how we can classify
  stable fluid states of a pure substance in relation to a liquid\Ugas
  coexistence curve and a critical point. If raising the temperature of a
  fluid at constant pressure causes a phase transition to a second fluid
  phase, the original fluid was a liquid and the transition occurs at the
  liquid\Ugas <subindex|Coexistence curve|liquid\Ugas><em|coexistence curve>.
  This curve ends at a <index-complex|<tuple|critical|point|pure
  substance>|||<tuple|Critical|point|of a pure substance>><strong|critical
  point><glossary|critical point>, at which all intensive properties of the
  coexisting liquid and gas phases become identical. The fluid state of a
  pure substance at a temperature greater than the critical temperature and a
  pressure greater than the critical pressure is called a
  <index|Supercritical fluid><subindex|Fluid|supercritical><strong|supercritical
  fluid><glossary|supercritical fluid>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|02-SUP/PHASES.eps|166pt|141pt||>>
        <label|c2 fig phases>Pressure\Utemperature phase diagram of a pure
        substance (schematic). Point cp is the critical point, and point tp
        is the triple point. Each area is labeled with the physical state
        that is stable under the pressure-temperature conditions that fall
        within the area. A solid curve (coexistence curve) separating two
        areas is the locus of pressure-temperature conditions that allow the
        phases of these areas to coexist at equilibrium. Path ABCD
        illustrates <em|continuity of states>.
      </big-figure>
    </framed>
  </float>

  The term <index|Vapor><strong|vapor><glossary|vapor> is sometimes used for
  a gas that can be condensed to a liquid by increasing the pressure at
  constant temperature. By this definition, the vapor state of a substance
  exists only at temperatures below the critical temperature.

  The designation of a supercritical fluid state of a substance is used more
  for convenience than because of any unique properties compared to a liquid
  or gas. If we vary the temperature or pressure in such a way that the
  substance changes from what we call a liquid to what we call a
  supercritical fluid, we observe only a continuous density change of a
  single phase, and no phase transition with two coexisting phases. The same
  is true for a change from a supercritical fluid to a gas. Thus, by making
  the changes described by the path ABCD shown in Fig. <reference|c2 fig
  phases>, we can transform a pure substance from a liquid at a certain
  pressure to a gas at the same pressure without ever observing an interface
  between two coexisting phases! This curious phenomenon is called
  <index|Continuity of states><em|continuity of states>.

  Chapter 6 will take up the discussion of further aspects of the physical
  states of pure substances.

  If we are dealing with a fluid <em|mixture> (instead of a pure substance)
  at a high pressure, it may be difficult to classify the phase as either
  liquid or gas. The complexity of classification at high pressure is
  illustrated by the <index|Barotropic effect><em|barotropic effect>,
  observed in some mixtures, in which a small change of temperature or
  pressure causes what was initially the more dense of two coexisting fluid
  phases to become the less dense phase. In a gravitational field, the two
  phases switch positions.<index-complex|fluid||c2 fluids|<tuple|Fluids>>

  <subsection|The equation of state of a fluid><label|2-eqn of
  state><label|c2 sec states-eos-fluids>

  Suppose we prepare a uniform fluid phase containing a known amount
  <math|n<rsub|i>> of each constituent substance <math|i>, and adjust the
  temperature <math|T> and pressure <math|p> to definite known values. We
  expect this phase to have a definite, fixed volume <math|V>. If we change
  any one of the properties <math|T>, <math|p>, or <math|n<rsub|i>>, there is
  usually a change in <math|V>. The value of <math|V> is dependent on the
  other properties and cannot be varied independently of them. Thus, for a
  given substance or mixture of substances in a uniform fluid phase, <math|V>
  is a unique function of <math|T>, <math|p>, and
  <math|<around*|{|n<rsub|i>|}>>, where <math|<around*|{|n<rsub|i>|}>> stands
  for the set of amounts of all substances in the phase. We may be able to
  express this relation in an explicit equation:
  <math|V=f<around*|(|T,p,<around*|{|n<rsub|i>|}>|)>>. This equation (or a
  rearranged form) that gives a relation among <math|V>, <math|T>, <math|p>,
  and <math|<around*|{|n<rsub|i>|}>> is the <index-complex|<tuple|Equation of
  state|fluid>|||<tuple|of a fluid>><strong|equation of
  state><glossary|equation of state> of the fluid.

  We may solve the equation of state, implicitly or explicitly, for any one
  of the quantities <math|V>, <math|T>, <math|p>, <math|n<rsub|i>> in terms
  of the other quantities. Thus, of the <math|3+s> quantities (where <math|s>
  is the number of substances), only <math|2+s> are independent.

  The <subindex|Ideal gas|equation><index-complex|<tuple|Equation of
  state|ideal gas>|||<tuple|of an ideal gas>><em|ideal gas equation>,
  <math|p=<frac|n*R*T|V>> (Eq. <reference|c1 eq ideal_gas_law> on page
  <pageref|c1 eq ideal_gas_law>), is an equation of state. It is found
  experimentally that the behavior of any gas in the limit of low pressure,
  as temperature is held constant, approaches this equation of state. This
  limiting behavior is also predicted by kinetic-molecular theory.

  If the fluid has only one constituent (i.e., is a pure substance rather
  than a mixture), then at a fixed <math|T> and <math|p> the volume is
  proportional to the amount. In this case, the equation of state may be
  expressed as a relation among <math|T>, <math|p>, and the molar volume
  <math|V<rsub|<text|m>>=<frac|V|<text|n>>>. The equation of state for a pure
  ideal gas may be written <math|p=<frac|R*T|V<rsub|<text|m>>>>.

  The <index|Redlich\UKwong equation><em|Redlich\UKwong equation> is a
  two-parameter equation of state frequently used to describe, to good
  accuracy, the behavior of a pure gas at a pressure where the ideal gas
  equation fails:

  <\equation>
    p=<frac|R*T|V<rsub|<text|m>>-b>-<frac|a|V<rsub|<text|m>>\<cdot\><around*|(|V<rsub|<text|m>>+b|)>\<cdot\>T<rsup|<frac*|1|2>>>
  </equation>

  In this equation, <math|a> and <math|b> are constants that are independent
  of temperature and depend on the substance.

  The next section describes features of <em|virial> equations, an important
  class of equations of state for real (nonideal) gases.

  <subsection|Virial equations of state for pure gases><label|2-virial
  eqn><label|c2 sec states-eos-gases>

  <subindex|Equation of state|virial><index-complex|<tuple|virial|equation|pure
  gas>|||<tuple|Virial|equation|for a pure gas>>In later chapters of this
  book there will be occasion to apply thermodynamic derivations to virial
  equations of state of a pure gas or gas mixture. These formulas accurately
  describe the gas at low and moderate pressures using empirically
  determined, temperature-dependent parameters. The equations may be derived
  from <subindex|Statistical mechanics|virial equations>statistical
  mechanics, so they have a theoretical as well as empirical foundation.

  There are two forms of virial equations for a pure gas: one a series in
  powers of <math|<frac|1|V<rsub|<text|m>>>>:

  <\equation>
    <label|pVm=RT(1+B/Vm...)><label|c2 eq
    virial-vm>p\<cdot\>V<rsub|<text|m>>=R\<cdot\>T\<cdot\><around*|(|1+<frac|B|V<rsub|<text|m>>>+<frac|C|V<rsub|<text|m>><rsup|2>>+\<cdots\>|)>
  </equation>

  and the other a series in powers of <math|p>:

  <\equation>
    <label|pVm=RT(1+B_p p...)><label|c2 eq
    virial-p>p\<cdot\>V<rsub|<text|m>>=R\<cdot\>T\<cdot\><around*|(|1+B<rsub|p>*p+C<rsub|p>*p<rsup|2>+\<cdots\>|)>
  </equation>

  The parameters <math|B,C,\<ldots\>> are called the
  <math|<text|<em|second>>,<text|<em|third>>,\<ldots\>><subindex|Virial|coefficient><em|virial
  coefficients>, and the parameters <math|B<rsub|p>,C<rsub|p>,\<ldots\>> are
  a set of pressure virial coefficients. Their values depend on the substance
  and are functions of temperature. (The <em|first> virial coefficient in
  both power series is <math|1>, because <math|p*V<rsub|<text|m>>> must
  approach <math|R*T> as <math|<frac|1|V<rsub|<text|m>>>> or <math|p>
  approach zero at constant <math|T>.) Coefficients beyond the third virial
  coefficient are small and rarely evaluated.

  The values of the virial coefficients for a gas at a given temperature can
  be determined from the dependence of <math|p> on <math|V<rsub|<text|m>>> at
  this temperature. The value of the second virial coefficient <math|B>
  depends on pairwise interactions between the atoms or molecules of the gas,
  and in some cases can be calculated to good accuracy from
  <index|Statistical mechanics>statistical mechanics theory and a realistic
  intermolecular potential function.

  To find the relation between the virial coefficients of Eq. <reference|c2
  eq virial-vm> and the parameters <math|B<rsub|p>,C<rsub|p>,\<ldots\>> in
  Eq. <reference|c2 eq virial-p>, we solve Eq. <reference|c2 eq virial-vm>
  for <math|p> in terms of <math|V<rsub|m>>

  <\equation>
    p=R\<cdot\>T\<cdot\><around*|(|<frac|1|V<rsub|<text|m>>>+<frac|B|V<rsub|<text|m>><rsup|2>>+\<cdots\>|)>
  </equation>

  and substitute in the right side of Eq. <reference|c2 eq virial-p>:

  <\equation>
    <label|c2 eq virial-vm-subp><label|pVm=RT(1+B_p
    RT(1/Vm...)...)>p\<cdot\>V<rsub|<text|m>>=R\<cdot\>T\<cdot\><around*|[|1+B<rsub|p>\<cdot\>R\<cdot\>T\<cdot\><around*|(|<frac|1|V<rsub|<text|m>>>+<frac|B|V<rsub|<text|m>><rsup|2>>+\<cdots\>|)>+C<rsub|p>\<cdot\><around*|(|R\<cdot\>T|)><rsup|2>\<cdot\><around*|(|<frac|1|V<rsub|<text|m>>>+<frac|B|V<rsub|<text|m>><rsup|2>>+\<cdots\>|)><rsup|2>+\<cdots\>|]>
  </equation>

  Then we equate coefficients of equal powers of
  <math|<frac|1|V<rsub|<text|m>>>> in Eqs. <reference|c2 eq virial-vm> and
  <reference|c2 eq virial-vm-subp> (since both equations must yield the same
  value of <math|p\<cdot\>V<rsub|<text|m>>> for any value of
  <math|<frac|1|V<rsub|<text|m>>>>):

  <\equation>
    <label|c2 eq virial-coeff-b><label|B=RTB_p>B=R\<cdot\>T\<cdot\>B<rsub|p>
  </equation>

  <\equation>
    <label|c2 eq virial-coeff-c>C=B<rsub|p>\<cdot\>R\<cdot\>T\<cdot\>B+C<rsub|p>\<cdot\><around*|(|R\<cdot\>T|)><rsup|2>=<around*|(|R\<cdot\>T|)><rsup|2>\<cdot\><around*|(|B<rsub|p><rsup|2>+C<rsub|p>|)>
  </equation>

  In the last equation, we have substituted <math|B> from Eq. <reference|c2
  eq virial-coeff-b>.

  At pressures up to at least one bar, the terms beyond
  <math|B<rsub|p>\<cdot\>p> in the pressure power series of Eq. <reference|c2
  eq virial-p> are negligible; then <math|p\<cdot\>V<rsub|<text|m>>> may be
  approximated by <math|R\<cdot\>T\<cdot\><around*|(|1+B<rsub|p>\<cdot\>p|)>>,
  giving, with the help of Eq. <reference|c2 eq virial-coeff-b>, the simple
  approximate equation of state<\footnote>
    Guggenheim (Ref <cite|guggen-85>) calls a gas with this equation of state
    a <em|slightly imperfect gas>.
  </footnote>

  <equation-cov2|<label|c2 eq virial-eos-simple><label|Vm=RT/p+B>V<rsub|<text|m>>\<approx\><frac|R\<cdot\>T|p>+B|(pure
  gas, <math|p\<leq\>1 <text|bar>>)>

  The <strong|compression factor> (or compressibility factor) <math|Z> of a
  gas is defined by

  <equation-cov2|Z<bk-equal-def><frac|p\<cdot\>V|n\<cdot\>R\<cdot\>T>=<frac|p\<cdot\>V<rsub|<text|m>>|R\<cdot\>T>|(gas)>

  When a gas is at a particular temperature and pressure satisfies the ideal
  gas equation, the value of <math|Z> is <math|1>. The virial equations
  rewritten using <math|Z> are

  <\equation>
    Z=1+<frac|B|V<rsub|<text|m>>>+<frac|C|V<rsub|<text|m>><rsup|2>>+\<cdots\>
  </equation>

  <\equation>
    Z=1+B<rsub|p>\<cdot\>p+C<rsub|p>\<cdot\>p<rsup|2>+\<cdots\>
  </equation>

  These equations show that the second virial coefficient <math|B> is the
  initial slope of the curve of a plot of <math|Z> versus
  <math|<frac|1|V<rsub|<text|m>>>> at constant <math|T>, and <math|B<rsub|p>>
  is the initial slope of <math|Z> versus <math|p> at constant <math|T>.

  The way in which <math|Z> varies with <math|p> at different temperatures is
  shown for the case of carbon dioxide in Fig. <reference|c2
  fig-co2>(a).<\float|float|thb>
    <\framed>
      <\big-figure|<image|02-SUP/CO2.eps|346pt|165pt||>>
        <label|c2 fig-co2><label|fig:2-CO2>

        <\enumerate-alpha>
          <item>Compression factor of <math|<text|CO><rsub|2>> as a function
          of pressure at three temperatures. At <math|700 <text|K>>, the
          Boyle temperature, the initial slope is zero.

          <item>Second virial coefficient of <math|<text|CO><rsub|2>> as a
          function of temperature.
        </enumerate-alpha>

        \;
      </big-figure>
    </framed>
  </float>

  A temperature at which the initial slope is zero is called the <index|Boyle
  temperature><subindex|Temperature|Boyle><strong|Boyle
  temperature><glossary|Boyle temperature>, which for
  <math|<text|CO><rsub|2>> is <math|700 <text|K>>. Both <math|B> and
  <math|B<rsub|p>> must be zero at the Boyle temperature. At lower
  temperatures <math|B> and <math|B<rsub|p>> are negative, and at higher
  temperatures they are positive\Vsee Fig. <reference|c2 fig-co2>(b). This
  kind of temperature dependence is typical for other gases. Experimentally,
  and also according to <subindex|Statistical mechanics|Boyle
  temperature>statistical mechanical theory, <math|B> and <math|B<rsub|p>>
  for a gas can be zero only at a single Boyle temperature.

  <\quote-env>
    The fact that at any temperature other than the Boyle temperature
    <math|B> is nonzero is significant since it means that in the limit as
    <math|p> approaches zero at constant <math|T> and the gas approaches
    ideal-gas behavior, the difference between the actual molar volume
    <math|V<rsub|<text|m>>> and the ideal-gas molar volume
    <math|<frac|R\<cdot\>T|p>> does not approach zero. Instead,
    <math|V<rsub|<text|m>>-<frac|R\<cdot\>T|p>> approaches the nonzero value
    <math|B> (see Eq. <reference|c2 eq virial-eos-simple>). However, the
    <em|ratio> of the actual and ideal molar volumes,
    <math|<frac|V<rsub|<text|m>>|<around*|(|<frac|R\<cdot\>T|p>|)>>>,
    approaches unity in this limit.
  </quote-env>

  Virial equations of gas <em|mixtures> will be discussed in Sec.
  <reference|c9 sec gm-real>.

  <subsection|Solids><label|2-solids><label|c2 sec states-solids>

  <index-complex|<tuple|Solid>||c2 sec solids|<tuple|Solid>>A solid phase
  responds to a small applied stress by undergoing a small <index|Elastic
  deformation><subindex|Deformation|elastic><em|elastic deformation>. When
  the stress is removed, the solid returns to its initial shape and the
  properties return to those of the unstressed solid. Under these conditions
  of small stress, the solid has an equation of state just as a fluid does,
  in which p is the pressure of a fluid surrounding the solid (the
  hydrostatic pressure) as explained in Sec. <reference|c2 sec
  prop-pressure>. The stress is an additional independent variable. For
  example, the length of a metal spring that is elastically deformed is a
  unique function of the temperature, the pressure of the surrounding air,
  and the stretching force.

  If, however, the stress applied to the solid exceeds its elastic limit, the
  response is <index|Plastic deformation><subindex|Deformation|plastic><em|plastic
  deformation>. This deformation persists when the stress is removed, and the
  unstressed solid no longer has its original properties. Plastic deformation
  is a kind of hysteresis, and is caused by such microscopic behavior as the
  slipping of crystal planes past one another in a crystal subjected to shear
  stress, and conformational rearrangements about single bonds in a stretched
  macromolecular fiber. Properties of a solid under plastic deformation
  depend on its past history and are not unique functions of a set of
  independent variables; an equation of state does not
  exist.<index-complex|<tuple|Solid>||c2 sec solids|<tuple|Solid>>

  <section|Some Basic Properties and Their Measurement><label|c2 sec prop>

  This section discusses aspects of the macroscopic properties mass, amount
  of substance, volume, density, pressure, and temperature, with examples of
  how these properties can be measured.

  <subsection|Mass><label|2-mass><label|c2 sec prop-mass>

  <index-complex|<tuple|mass>||c2 sec prop-mass|<tuple|Mass, measurement
  of>>The <acronym|SI> unit of mass is the <strong|kilogram>. The practical
  measurement of the mass of a body is with a balance utilizing the downward
  force exerted on the body by the earth's gravitational field. The classic
  balance has a beam and knife-edge arrangement to compare the gravitational
  force on the body with the gravitational force on a weight of known mass. A
  modern balance (strictly speaking a <em|scale>) incorporates a strain gauge
  or comparable device to directly measure the gravitational force on the
  unknown mass; this type must be calibrated with known masses. The most
  accurate measurements take into account the effect of the buoyancy of the
  body and the calibration masses in air.

  The accuracy of the calibration masses should be traceable to a national
  standard kilogram (which in the United States is maintained at
  <acronym|NIST>, the National Institute of Standards and Technology, in
  Gaithersburg, Maryland) and ultimately to the international prototype (page
  <pageref|c1 sec qus-2019>).

  <\quote-env>
    <label|c2 note-kibble-balance>The 2019 revision of the <acronym|SI>
    replaces the international prototype with a new definition of the
    kilogram (Appendix <reference|appendix definitions-si-units>). The
    present method of choice for applying this definition to the precise
    measurement of a mass, with an uncertainty of several parts in 10 8 ,
    uses an elaborate apparatus called a watt balance or <em|Kibble
    balance>.<\footnote>
      Ref <cite|chao-15>.
    </footnote> By this method, the mass of the international prototype is
    found to be <math|1 <text|kg>> to within <math|1\<times\>10<rsup|-8>
    <text|kg>>.<\footnote>
      Ref <cite|stock-19>, Appendix 2
    </footnote>

    The NIST-4 Kibble balance<\footnote>
      Ref <cite|haddad-16>.
    </footnote> at <acronym|NIST> has a balance wheel, from one side of which
    is suspended a coil of wire placed in a magnetic field, and from the
    other side a tare weight. In use, the balance position of the wheel is
    established. The test weight of unknown mass <math|m> is added to the
    coil side and a current passed through the coil, generating an upward
    force on this side due to the magnetic field. The current <math|I> is
    adjusted to reestablish the balance position. The balance condition is
    that the downward gravitational force on the test weight be equal in
    magnitude to the upward electromagnetic force:
    <math|m\<cdot\>g=B\<cdot\>l\<cdot\>I>, where <math|g> is the acceleration
    of free fall, <math|B> is the magnetic flux density, <math|l> is the wire
    length of the coil, and <math|I> is the current carried by the wire.

    <math|B> and <math|I> can't be measured precisely, so in a second
    calibration step the test weight is removed, the current is turned off,
    and the coil is moved vertically through the magnetic field at a constant
    precisely-measured speed <math|v>. This motion induces an electric
    potential difference between the two ends of the coil wire given by
    <math|\<Delta\>*\<phi\>=B\<cdot\>l\<cdot\>v>.

    By eliminating the product <math|B\<cdot\>l> from between the two
    preceding equations, the mass of the test weight can be calculated from
    <math|m=<frac|I\<cdot\>\<Delta\>*\<phi\>|g\<cdot\>v>>. For this
    calculation, <math|I> and <math|\<mathLaplace\>*\<phi\>> are measured to
    a very high degree of precision during the balance operations by
    instrumental methods (Josephson and quantum Hall effects) requiring the
    defined value of the Planck constant <math|h>; the value of <math|g> at
    the location of the apparatus is measured with a gravimeter.
  </quote-env>

  <index-complex|<tuple|mass>||c2 sec prop-mass|<tuple|Mass, measurement of>>

  <subsection|Amount of substance><label|2-amount><label|c2 sec prop-amount>

  <index-complex|<tuple|amount>||c2 sec prop-amount|<tuple|Amount|measurement
  of>>The SI unit of <index|Amount>amount of substance (called simply the
  amount in this book) is the <index|Mole><strong|mole> (Sec. <reference|c1
  sec qus-amount>). Chemists are familiar with the fact that, although the
  mole is a counting unit, an amount in moles is measured not by counting but
  by weighing. The <acronym|SI> revision of 2019 makes a negligible change to
  calculations involving the mole (page <pageref|c1 sec qus-amount>), so the
  previous definition of the mole remains valid for most purposes: twelve
  grams of carbon-12, the most abundant isotope of carbon, contains one mole
  of atoms.

  The <index|Relative atomic mass><index|Atomic mass, relative><em|relative
  atomic mass> or <index|Atomic weight><em|atomic weight>
  <math|A<rsub|<text|r>>> of an atom is a dimensionless quantity equal to the
  atomic mass relative to <math|A<rsub|<text|r>>=12> for carbon-12. The
  <index|Relative atomic mass><index|Molecular mass, relative><em|relative
  molecular mass> or <index|Molecular weight><label|2-molecular
  weight><em|molecular weight> <math|M<rsub|<text|r>>> of a molecular
  substance, also dimensionless, is the molecular mass relative to carbon-12.
  Thus the amount <math|n> of a substance of mass <math|m> can be calculated
  from

  <\equation>
    <tabular|<tformat|<table|<row|<cell|n=<frac|m|A<rsub|<text|r>>
    <text|g>\<cdot\><text|mol><rsup|-1>>>|<cell|<text|<space|1em>or<space|1em>>>|<cell|<frac|m|M<rsub|<text|r>>
    <text|g>\<cdot\><text|mol><rsup|-1>>>>>>>
  </equation>

  A related quantity is the <strong|molar mass> <math|M> of a substance,
  defined as the mass divided by the amount:

  <\equation>
    <label|c2 eq molar-mass><label|M=m/n>M<above|=|<text|def>><frac|m|n>
  </equation>

  (The symbol <math|M> for molar mass is an exception to the rule given on
  page <pageref|c2 molar-quantity-subscript> that a subscript m is used to
  indicate a molar quantity.) The numerical value of the molar mass expressed
  in units of <math|<text|g>\<cdot\><text|mol><rsup|-1>> is equal to the
  relative atomic or molecular mass:

  <\equation>
    <tabular|<tformat|<table|<row|<cell|<frac|M|<text|g>\<cdot\><text|mol><rsup|-1>>=A<rsub|<text|r>>>|<cell|<text|<space|1em>or<space|1em>>>|<cell|<frac|M|<text|g>\<cdot\><text|mol><rsup|-1>>=M<rsub|<text|r>>>>>>>
  </equation>

  <index-complex|<tuple|amount>||c2 sec prop-amount|<tuple|Amount|measurement
  of>>

  <subsection|Volume><label|c2 sec prop-volume>

  <index-complex|<tuple|volume>||c2 sec prop-volume|<tuple|Volume|measurement
  of>>Liquid volumes are commonly measured with precision volumetric
  glassware such as burets, pipets, and volumetric flasks. The National
  Institute of Standards and Technology in the United States has established
  specifications for \PClass A\Q glassware; two examples are listed in Table
  <reference|c2 tab measurement-methods>. The volume of a vessel at one
  temperature may be accurately determined from the mass of a liquid of known
  density, such as water, that fills the vessel at this
  temperature.<float|float|thb|<\big-table>
    <bktable3|<tformat|<table|<row|<cell|Physical
    quantity>|<cell|Method>|<cell|Typical value>|<cell|Approximate
    uncertainty>>|<row|<cell|Mass>|<cell|analytical balance>|<cell|<math|100
    <text|g>>>|<cell|<math|0.1 <text|mg>>>>|<row|<cell|>|<cell|microbalance>|<cell|<math|20
    <text|mg>>>|<cell|<math|0.1 <text|\<#3BC\>g>>>>|<row|<cell|Volume>|<cell|pipet,
    Class A>|<cell|<math|10 <text|mL>>>|<cell|<math|0.02
    <text|mL>>>>|<row|<cell|>|<cell|volumetric flask, Class A>|<cell|<math|1
    <text|L>>>|<cell|<math|0.3 <text|mL>>>>|<row|<cell|Density>|<cell|pycnometer,
    25-mL capacity>|<cell|<math|1 <frac|<text|g>|<text|mL>>>>|<cell|<math|2
    <frac|<text|mg>|<text|mL>>>>>|<row|<cell|>|<cell|magnetic float
    densimeter>|<cell|<math|1 <frac|<text|g>|<text|mL>>>>|<cell|<math|0.1
    <frac|<text|mg>|<text|mL>>>>>|<row|<cell|>|<cell|vibrating-tube
    densimeter>|<cell|<math|><math|1 <frac|<text|g>|<text|mL>>>>|<cell|<math|0.01
    <frac|<text|mg>|<text|mL>>>>>|<row|<cell|Pressure>|<cell|mercury
    manometer or barometer>|<cell|<math|760 <text|Torr>>>|<cell|<math|0.001
    <text|Torr>>>>|<row|<cell|>|<cell|diaphragm gauge>|<cell|<math|100
    <text|Torr>>>|<cell|<math|1 <text|Torr>>>>|<row|<cell|Temperature>|<cell|constant-volume
    gas thermometer>|<cell|<math|10 <text|K>>>|<cell|<math|0.001
    <text|K>>>>|<row|<cell|>|<cell|mercury-in-glass
    thermometer>|<cell|<math|300 <text|K>>>|<cell|<math|0.01
    <text|K>>>>|<row|<cell|>|<cell|platinum resistance
    thermometer>|<cell|<math|300 <text|K>>>|<cell|<math|0.0001
    <text|K>>>>|<row|<cell|>|<cell|monochromatic optical
    pyrometer>|<cell|<math|1300 <text|K>>>|<cell|<math|0.03 <text|K>>>>>>>

    \;
  <|big-table>
    <label|c2 tab measurement-methods><label|tbl:2-methods>Representative
    measurement methods
  </big-table>>

  The SI unit of volume is the cubic meter, but chemists commonly express
  volumes in units of liters and milliliters. The
  <index|Liter><strong|liter><glossary|liter> is defined as one cubic
  decimeter (Table <reference|c1 tab non_si_derived_units>). One cubic meter
  is the same as <math|10<rsup|3>> liters and <math|10<rsup|6>> milliliters.
  The <index|Milliliter><em|milliliter> is identical to the cubic centimeter.

  <\quote-env>
    Before 1964, the liter had a different definition: it was the volume of
    <math|1> kilogram of water at <math|3.98 <rsup|\<circ\>><text|C>>, the
    temperature of maximum density. This definition made one liter equal to
    <math|1.000028 <text|dm><rsup|3>>. Thus, a numerical value of volume (or
    density) reported before 1964 and based on the liter as then defined may
    need a small correction in order to be consistent with the present
    definition of the liter.
  </quote-env>

  <index-complex|<tuple|volume>||c2 sec prop-volume|<tuple|Volume|measurement
  of>>

  <subsection|Density><label|c2 sec prop-density>

  <index-complex|<tuple|density>||c2 sec prop-density|<tuple|Density>>Density,
  an intensive property, is defined as the ratio of the two extensive
  properties mass and volume:

  <\equation>
    <label|c2 eq density>\<rho\><above|=|<text|def>><frac|m|V>
  </equation>

  <subindex|Volume|molar>The molar volume <math|V<rsub|<text|m>>> of a
  homogeneous pure substance is inversely proportional to its density. From
  Eqs. <reference|c2 eq molar-volume>, <reference|c2 eq molar-mass>, and
  <reference|c2 eq density>, we obtain the relation

  <\equation>
    V<rsub|<text|m>>=<frac|M|\<rho\>>
  </equation>

  \;

  Various methods are available for determining the density of a phase, many
  of them based on the measurement of the mass of a fixed volume or on a
  buoyancy technique. Three examples are shown in Fig. <reference|c2 fig
  density>. Similar apparatus may be used for gases. The density of a solid
  may be determined from the volume of a nonreacting liquid (e.g., mercury)
  displaced by a known mass of the solid, or from the loss of weight due to
  buoyancy when the solid is suspended by a thread in a liquid of known
  density.<\float|float|thb>
    <\framed>
      <\big-figure|<image|02-SUP/density.eps|296pt|120pt||>>
        <label|c2 fig density><label|fig:2-density>Three methods for
        measuring liquid density by comparison with samples of known density.
        The liquid is indicated by gray shading.

        <\enumerate-alpha>
          <item>Glass pycnometer vessel with capillary stopper. The filled
          pycnometer is brought to the desired temperature in a thermostat
          bath, dried, and weighed.

          <item>Magnetic float densimeter.<note-ref|+2BTH32sy1b4NrHu5> Buoy
          <math|B>, containing a magnet, is pulled down and kept in position
          with solenoid <math|S> by means of position detector <math|D> and
          servo control system <math|C>. The solenoid current required
          depends on the liquid density.

          <item>Vibrating-tube densimeter. The ends of a liquid-filled metal
          <with|font-family|ss|U>-tube are clamped to a stationary block. An
          oscillating magnetic field at the tip of the tube is used to make
          it vibrate in the direction perpendicular to the page. The measured
          resonance frequency is a function of the mass of the liquid in the
          tube.
        </enumerate-alpha>

        \;

        <note-inline|Ref. <cite|greer-74>|+2BTH32sy1b4NrHu5>
      </big-figure>
    </framed>
  </float>

  <index-complex|<tuple|density>||c2 sec prop-density|<tuple|Density>>

  <subsection|Pressure><label|2-pressure><label|c2 sec prop-pressure>

  <index-complex|<tuple|pressure|measurement of>||c2 sec
  prop-pressure|<tuple|Pressure|measurement of>>Pressure is a force per unit
  area. Specifically, it is the normal component of stress exerted by an
  <index|Isotropic fluid><subindex|Fluid|isotropic>isotropic fluid on a
  surface element.<\footnote>
    A liquid crystal and a polar liquid in a electric field are examples of
    fluids that are not isotropic, because they have different macroscopic
    properties in different directions.
  </footnote> The surface can be an interface surface between the fluid and
  another phase, or an imaginary dividing plane within the fluid.

  Pressure is usually a positive quantity. Because cohesive forces exist in a
  liquid, it may be possible to place the liquid under tension and create a
  <em|negative><label|neg p> pressure. For instance, the pressure is negative
  at the top of a column of liquid mercury suspended below the closed end of
  a capillary tube that has no vapor bubble. Negative pressure in a liquid is
  an unstable condition that can result in spontaneous vaporization.

  The <acronym|SI> unit of pressure is the <index|Pascal
  (unit)><strong|pascal><glossary|pascal>. Its symbol is <math|<text|Pa>>.
  One pascal is a force of one newton per square meter (Table <reference|c1
  tab si_derived_units>).

  Chemists are accustomed to using the non-<acronym|SI> units of millimeters
  of mercury, torr, and atmosphere. One millimeter of mercury (symbol
  <math|<text|mmHg>>) is the pressure exerted by a column exactly <math|1
  <text|mm>> high of a fluid of density equal to exactly <math|13.5951
  <text|g>\<cdot\><text|cm><rsup|-3>> (the density of mercury at <math|0
  <rsup|\<circ\>><text|C>>) in a place where the acceleration of free fall,
  <math|g>, has its standard value <math|g<rsub|<text|n>>> (see Appendix
  <reference|appendix physical-constants>). One atmosphere is defined as
  exactly <math|1.01325\<times\>10<rsup|5> <text|Pa>> (Table <reference|c1
  tab non_si_derived_units>). The <math|<text|torr>> is defined by letting
  one atmosphere equal exactly <math|760 <text|Torr>>. One atmosphere is
  approximately <math|760 <text|mmHg>>. In other words, the millimeter of
  mercury and the torr are practically identical; they differ from one
  another by less than <math|2\<times\>10<rsup|-7> <text|Torr>>.

  Another non-<acronym|SI> pressure unit is the
  <index|Bar><strong|bar><glossary-explain|bar|unit of pressure>, equal to
  exactly <math|10<rsup|5> <text|Pa>>. A pressure of one bar is approximately
  one percent smaller than one atmosphere. This book often refers to a
  <subindex|Standard|pressure><subindex|Pressure|standard><strong|standard
  pressure><glossary|standard pressure>, <math|p<rsup|\<circ\>>>. In the
  past, the value of <math|p<rsup|\<circ\>>> was usually taken to be <math|1
  <text|atm>>, but since 1982 the <acronym|IUPAC> has recommended the value
  <math|p<rsup|\<circ\>>=1 <text|bar>>.

  A variety of manometers and other devices is available to measure the
  pressure of a fluid, each type useful in a particular pressure range. Some
  devices measure the pressure of the fluid directly. Others measure the
  differential pressure between the fluid and the atmosphere; the fluid
  pressure is obtained by combining this measurement with the atmospheric
  pressure measured with a barometer.

  Within a <em|solid>, pressure cannot be defined simply as a force per unit
  area. Macroscopic forces at a point within a solid are described by the
  nine components of a stress tensor. The statement that a solid <em|has> or
  <em|is at> a certain pressure means that this is the hydrostatic pressure
  exerted on the solid's exterior surface. Thus, a solid immersed in a
  uniform isotropic fluid of pressure <math|p> is at pressure <math|p>; if
  the fluid pressure is constant over time, the solid is at constant
  pressure.<index-complex|<tuple|pressure|measurement of>||c2 sec
  prop-pressure|<tuple|Pressure|measurement of>>

  <subsection|Temperature><label|2-temperature><label|c2 sec
  prop-temperature>

  <index-complex|<tuple|Temperature>||c2 sec prop-temperature
  idx1|<tuple|Temperature|measurement of>>Temperature and thermometry are of
  fundamental importance in thermodynamics. Unlike the other physical
  quantities discussed in this chapter, temperature does not have a single
  unique definition. The chosen definition, whatever it may be, requires a
  <index|Temperature scale><em|temperature scale> described by an operational
  method of measuring temperature values. For the scale to be useful, the
  values should increase monotonically with the increase of what we
  experience physiologically as the degree of \Photness.\Q We can define a
  satisfactory scale with any measuring method that satisfies this
  requirement. The values on a particular temperature scale correspond to a
  particular physical quantity and a particular temperature unit.

  For example, suppose you construct a simple
  <subindex|Thermometer|liquid-in-glass>liquid-in-glass thermometer with
  equally spaced marks along the stem and number the marks consecutively. To
  define a temperature scale and a temperature unit, you could place the
  thermometer in thermal contact with a body whose temperature is to be
  measured, wait until the indicating liquid reaches a stable position, and
  read the meniscus position by linear interpolation between two marks. Of
  course, placing the thermometer and body in thermal contact may affect the
  body's temperature. The measured temperature is that of the body <em|after>
  thermal equilibrium is achieved.

  Thermometry is based on the principle that the temperatures of different
  bodies may be compared with a thermometer. For example, if you find by
  separate measurements with your thermometer that two bodies give the same
  reading, you know that within experimental error both have the same
  temperature. The significance of two bodies having the same temperature (on
  any scale) is that if they are placed in thermal contact with one another,
  they will prove to be in thermal equilibrium with one another as evidenced
  by the absence of any changes in their properties. This principle is
  sometimes called the <index|Zeroth law of thermodynamics><em|zeroth law of
  thermodynamics>, and was first stated as follows by <index|Maxwell, James
  Clerk>J. C. Maxwell (1872): \PBodies whose temperatures are equal to that
  of the same body have themselves equal temperatures.\Q

  <subsubsection|Equilibrium systems for fixed temperatures><label|c2 sec
  prop-temperature-equilibrium>

  <index-complex|<tuple|temperature|equilibrium systems for fixed values>||c2
  sec-temperature-equilibrium|<tuple|Temperature|equilibrium systems for
  fixed values>>The <subindex|Ice point|for fixed temperature><em|ice point>
  is the temperature at which ice and air-saturated water coexist in
  equilibrium at a pressure of one atmosphere. The <index|Steam
  point><em|steam point> is the temperature at which liquid and gaseous
  <math|<text|H><rsub|2><text|O>> coexist in equilibrium at one atmosphere.
  Neither of these temperatures has sufficient reproducibility for
  high-precision work. The temperature of the ice-water-air system used to
  define the ice point is affected by air bubbles in the ice and by varying
  concentrations of air in the water around each piece of ice. The steam
  point is uncertain because the temperature of coexisting liquid and gas is
  a sensitive function of the experimental pressure.

  The <subindex|Melting point|for fixed temperature>melting point of the
  solid phase of a pure substance is a more reproducible temperature. When
  the solid and liquid phases of a pure substance coexist at a controlled,
  constant pressure, the temperature has a definite fixed value.

  <subsubindex|Triple|point|for fixed temperature>Triple points of pure
  substances provide the most reproducible temperatures. Both temperature and
  pressure have definite fixed values in a system containing coexisting
  solid, liquid, and gas phases of a pure substance.

  Figure <reference|c2 fig-tp-cell> illustrates a
  <subsubindex|Triple|point|cell>triple-point cell for water whose
  temperature is capable of a reproducibility within <math|10<rsup|-4>
  <text|K>>. When ice, liquid water, and water vapor are in equilibrium in
  this cell, the cell is at the triple point of water.<\float|float|thb>
    <\framed>
      <\big-figure|<image|02-SUP/TP-CELL.eps|175pt|171pt||>>
        <label|c2 fig-tp-cell><label|fig:2-tp cell>Cross-section of a water
        triple-point cell. The cell has cylindrical symmetry about a vertical
        axis. Pure water of the same isotopic composition as
        <math|<text|H><rsub|2><text|O>> in ocean water is distilled into the
        cell. The air is pumped out and the cell is sealed. A freezing
        mixture is placed in the inner well to cause a thin layer of ice to
        form next to the inner wall. The freezing mixture is removed, and
        some of the ice is allowed to melt to a film of very pure water
        between the ice and inner wall. The thermometer bulb is placed in the
        inner well as shown, together with ice water (not shown) for good
        thermal contact.
      </big-figure>
    </framed>
  </float>

  <index-complex|<tuple|temperature|equilibrium systems for fixed values>||c2
  sec-temperature-equilibrium|<tuple|Temperature|equilibrium systems for
  fixed values>>

  <subsubsection|Temperature scales><label|c2 sec prop-temperature-scales>

  <index-complex|<tuple|temperature|scales>||c2 sec prop-temperature-scales
  idx1|<tuple|Temperature|scales>>Six different temperature scales are
  described below: the ideal-gas temperature scale, the thermodynamic
  temperature scale, the obsolete centigrade scale, the Celsius scale, the
  International Temperature Scale of 1990, and the Provisional Low
  Temperature Scale of 2000.

  The <em|ideal-gas temperature scale> is defined by gas thermometry
  measurements, as described on page <pageref|c2 note-gas-thermometry>. The
  <em|thermodynamic temperature scale> is defined by the behavior of a
  theoretical Carnot engine, as explained in Sec. <reference|c4 sec
  concepts-temperature>. These temperature scales correspond to the physical
  quantities called <index|Ideal-gas temperature><index-complex|<tuple|Temperature|ideal-gas>|||<tuple|Temperature|ideal
  gas>>ideal-gas temperature and <subindex|Thermodynamic|temperature><subindex|Temperature|thermodynamic>thermodynamic
  temperature, respectively. Although the two scales have different
  definitions, the two temperatures turn out (Sec. <reference|c4 sec
  concepts-temperature>) to be proportional to one another. Their values
  become identical when the same unit of temperature is used for both.

  Prior to the 2019 <acronym|SI> revision, the <index|Kelvin
  (unit)><strong|kelvin><glossary|kelvin> was defined by specifying that a
  system containing the solid, liquid, and gaseous phases of
  <math|<text|H><rsub|2><text|O>> coexisting at equilibrium with one another
  (the <index-complex|<tuple|triple|point|H2O>|||<tuple|Triple|point|of
  H<math|<rsub|2>>O>><em|triple point of water>) has a thermodynamic
  temperature of exactly <math|273.16> kelvins.<label|273.16K=water triple
  pt> (This value was chosen to make the steam point approximately one
  hundred kelvins greater than the ice point.) The ideal-gas temperature of
  this system was set equal to the same value, <math|273.16> kelvins, making
  temperatures measured on the two scales identical.<label|identical
  temperature scales>

  The <subindex|SI|2019 revision>2019 <acronym|SI> revision treats the triple
  point temperature of water as a value to be determined experimentally by
  primary thermometry (page <pageref|c2 sec prop-temperature-thermometry>).
  The result is <math|273.16> kelvins to within <math|1\<times\>10<rsup|-7>
  <text|K>>.<\footnote>
    Ref. <cite|stock-19>, Appendix 2.
  </footnote> Thus there is no practical difference between the old and new
  definitions of the kelvin.

  Formally, the symbol <math|T> refers to thermodynamic temperature. Strictly
  speaking, a different symbol should be used for ideal-gas temperature.
  Since the two kinds of temperatures have identical values, this book will
  use the symbol <math|T> for both and refer to both physical quantities
  simply as \Ptemperature\Q except when it is necessary to make a
  distinction.

  The obsolete <index|Centrigrade scale><em|centigrade scale> was defined to
  give a value of exactly <math|0> degrees centigrade at the ice point and a
  value of exactly <math|100> degrees centigrade at the steam point, and to
  be a linear function of an ideal-gas temperature scale.

  The centigrade scale has been replaced by the
  <subindex|Celsius|scale><em|Celsius> scale, the thermodynamic (or
  ideal-gas) temperature scale shifted by exactly <math|273.15> kelvins. The
  temperature unit is the degree Celsius (<math|<rsup|\<circ\>><text|C>>),
  identical in size to the kelvin. Thus, <subindex|Celsius|temperature>Celsius
  temperature <math|t> is related to thermodynamic temperature T by

  <\equation>
    <label|t/oC=T/K-273.15><frac|t|<rsup|\<circ\>><text|C>>=<frac|T|<text|K>>-273.15
  </equation>

  On the Celsius scale, the <index-complex|<tuple|triple|point|H2O>|||<tuple|Triple|point|of
  H<math|<rsub|2>>O>>triple point of water is exactly <math|0.01
  <rsup|\<circ\>><text|C>>. The <index|Ice point>ice point is <math|0
  <rsup|\<circ\>><text|C>> to within <math|0.0001 <rsup|\<circ\>><text|C>>,
  and the <index|Steam point>steam point is <math|99.97
  <rsup|\<circ\>><text|C>>.

  The <index-line|international temperature scale of 1990|International
  Temperature Scale of 1990, <em|see> ITS-90><index-line|<tuple|temperature|international
  scale of 1990>|International scale of 1990, <em|see>
  ITS-90><em|International Temperature Scale of 1990> (abbreviated
  <index|ITS-90>ITS-90) defines the physical quantity called international
  temperature, with symbol <math|T<rsub|90>>.<\footnote>
    Refs. <cite|mcglashan-90> and <cite|preston-90>.
  </footnote> Each value of <math|T<rsub|90>> is intended to be very close to
  the corresponding thermodynamic temperature <math|T>.

  The ITS-90 scale is defined over a very wide temperature range, from
  <math|0.65 <text|K>> up to at least <math|1358 <text|K>>. There is a
  specified procedure for each measurement of <math|T<rsub|90>>, depending on
  the range in which <math|T> falls: vapor-pressure thermometry (<math|0.65>
  \U <math|5.0 <text|K>>), gas thermometry (<math|3.0> \U <math|24.5561
  <text|K>>), platinum-resistance thermometry (<math|13.8033> \U
  <math|1234.93 <text|K>>), or optical pyrometry (above <math|1234.93
  <text|K>>). For vapor-pressure thermometry, the ITS-90 scale provides
  formulas for <math|T<rsub|90>> in terms of the vapor pressure of the helium
  isotopes <math|<lsup|3><text|He>> and <math|<lsup|4><text|He>>. For the
  other methods, it assigns values of fourteen fixed calibration temperatures
  achieved with the reproducible equilibrium systems listed in Table
  <reference|c2 tab-its-90>, and provides interpolating functions for
  intermediate temperatures.<float|float|thb|<\big-table>
    <bktable3|<tformat|<cwith|2|15|1|1|cell-halign|L.>|<table|<row|<cell|<math|T<rsub|90>/<text|K>>>|<cell|Equilibrium
    system>>|<row|<cell|13.8033>|<cell|H<rsub|<math|2>> triple
    point>>|<row|<cell|24.5561>|<cell|Ne triple
    point>>|<row|<cell|54.3584>|<cell|O<rsub|<math|2>> triple
    point>>|<row|<cell|83.8058>|<cell|Ar triple
    point>>|<row|<cell|234.3156>|<cell|Hg triple
    point>>|<row|<cell|273.16>|<cell|H<rsub|<math|2>>O triple
    point>>|<row|<cell|302.9146>|<cell|Ga melting point at <math|1
    <text|atm>>>>|<row|<cell|429.7485>|<cell|In melting point at <math|1
    <text|atm>>>>|<row|<cell|505.078>|<cell|Sn melting point at <math|1
    <text|atm>>>>|<row|<cell|692.677>|<cell|Zn melting point at <math|1
    <text|atm>>>>|<row|<cell|933.473>|<cell|Al melting point at <math|1
    <text|atm>>>>|<row|<cell|1234.93>|<cell|Ag melting point at <math|1
    <text|atm>>>>|<row|<cell|1337.33>|<cell|Au melting point at <math|1
    <text|atm>>>>|<row|<cell|1357.77>|<cell|Cu melting point at <math|1
    <text|atm>>>>>>>

    \;
  <|big-table>
    <label|c2 tab-its-90><label|tbl:2-ITS-90>Fixed temperatures of the
    International Temperature Scale of 1990
  </big-table>>

  The <index-line|provisional low temperature scale of 2000|Provisional low
  temperature scale of 2000, <em|see> PLST-2000><em|Provisional Low
  Temperature Scale of 2000> (PLST-2000) is for temperatures between
  <math|0.0009 <text|K>> and <math|1 <text|K>>. This scale is based on the
  melting temperature of solid <math|<lsup|3><text|He>> as a function of
  pressure. For <math|<lsup|3><text|He>> at these temperatures, the required
  pressures are in the range <math|30> \U <math|40 <text|bar>>.<\footnote>
    Ref. <cite|rusby-02>.
  </footnote>

  The temperatures defined by the ITS-90 and PLST-2000 temperature scales are
  exact with respect to the respective scale\Vtheir values remain unchanged
  during the life of the scale.<\footnote>
    Ref. <cite|fellmuth-16>.
  </footnote>

  \;

  <index-complex|<tuple|temperature|scales>||c2 sec prop-temperature-scales
  idx1|<tuple|Temperature|scales>>

  <subsubsection|Primary thermometry><label|2-primary thermometry><label|c2
  sec prop-temperature-thermometry>

  <index-complex|<tuple|primary thermometry>||c2 sec
  prop-temperature-thermometry idx1|<tuple|Primary
  thermometry>><index-complex|<tuple|thermometry|primary>||c2 sec
  prop-temperature-thermometry idx2|<tuple|Thermometry|primary>>Primary
  thermometry is the measurement of temperature based on fundamental physical
  principles. Until about 1960, primary measurements of <math|T> involved gas
  thermometry. Other more accurate methods are now being used; they require
  elaborate equipment and are not convenient for routine measurements of
  <math|T>.

  The methods of primary thermometry require the value of the
  <index|Boltzmann constant>Boltzmann constant <math|k> or the <index|Gas
  constant>gas constant <math|R=N<rsub|<text|A>>\<cdot\>k>, where
  <math|N<rsub|<text|A>>> is the Avogadro constant. <math|k> and
  <math|N<rsub|<text|A>>> are defining constants of the 2019 revision of the
  <acronym|SI>. Using these fixed values (Appendix <reference|appendix
  physical-constants>) in the calculations results in values of <math|T>
  consistent with the definition of the kelvin according to the 2019
  revision.

  <label|c2 note-gas-thermometry><label|2-gas thermometry><em|Gas
  thermometry><index-complex|<tuple|gas|thermometry>||c2 sec
  prop-temperature-thermometry-gas|<tuple|Gas|thermometry>><index-complex|<tuple|thermometry|gas>||c2
  sec prop-temperature-thermometry-gas idx2|<tuple|Thermometry|gas>> is based
  on the ideal gas equation <math|T=<frac|p*V|n*R>>. It is most commonly
  carried out with a <subindex|Thermometer|constant-volume
  gas><em|constant-volume gas thermometer>. This device consists of a bulb or
  vessel containing a thermometric gas and a means of measuring the pressure
  of this gas. The thermometric gas is usually helium, because it has minimal
  deviations from ideal-gas behavior.

  The simple constant-volume gas thermometer depicted in Fig. <reference|c2
  fig-gas-thermometer> uses a mercury manometer to measure the pressure. More
  sophisticated versions have a diaphragm pressure transducer between the gas
  bulb and the pressure measurement system.<\float|float|thb>
    <\framed>
      <\big-figure|<image|02-SUP/GASTHERM.eps|238pt|160pt||>>
        <label|c2 fig-gas-thermometer><label|fig:2-gas thermometer>Simple
        version of a constant-volume gas thermometer. The leveling bulb is
        raised or lowered to place the left-hand meniscus at the level
        indicator. The gas pressure is then determined from
        <math|\<Delta\>*h> and the density of the mercury:
        <math|p=p<rsub|<text|atm>>+\<rho\>\<cdot\>g\<cdot\>\<Delta\>*h>.
      </big-figure>
    </framed>
  </float>

  One procedure for determining the value of an unknown temperature involves
  a pair of pressure measurements. The gas is brought successively into
  thermal equilibrium with two different systems: a reference system of known
  temperature <math|T<rsub|1>> (such as one of the systems listed in Table
  <reference|c2 tab-its-90>), and the system whose temperature
  <math|T<rsub|2>> is to be measured. The pressures <math|p<rsub|1>> and
  <math|p<rsub|2>> are measured at these temperatures. In the two
  equilibrations the amount of gas is the same and the gas volume is the same
  except for a small change due to effects of <math|T> and <math|p> on the
  gas bulb dimensions.

  If the gas exactly obeyed the ideal gas equation in both measurements, we
  would have <math|n\<cdot\>R=<frac|p<rsub|1>\<cdot\>V<rsub|1>|T<rsub|1>>=<frac|p<rsub|2>\<cdot\>V<rsub|2>|T<rsub|2>>>
  or <math|T<rsub|2>=T<rsub|1>\<cdot\><around*|(|<frac|p<rsub|2>\<cdot\>V<rsub|2>|p<rsub|1>\<cdot\>V<rsub|1>>|)>>.
  Since, however, the gas approaches ideal behavior only in the limit of low
  pressure, it is necessary to make a series of the paired measurements,
  changing the amount of gas in the bulb before each new pair so as to change
  the measured pressures in discrete steps. Thus, the operational equation
  for evaluating the unknown temperature is

  <equation-cov2|<label|T2(gas thermometer)>T<rsub|2>=T<rsub|1>*<below|<text|lim>|p<rsub|1>\<rightarrow\>0>
  <frac|p<rsub|2>\<cdot\>V<rsub|2>|p<rsub|1>\<cdot\>V<rsub|1>>|(gas)>

  (The ratio <math|<frac|V<rsub|2>|V<rsub|1>>> differs from unity only
  because of any change in the gas bulb volume when <math|T> and <math|p>
  change.) The limiting value of <math|<frac|p<rsub|2>\<cdot\>V<rsub|2>|p<rsub|1>\<cdot\>V<rsub|1>>>
  can be obtained by plotting this quantity against <math|p<rsub|1>>,
  <math|<frac|1|V<rsub|<text|m>>>>, or another appropriate extrapolating
  function. Note that values of <math|n> and <math|R> are not needed.

  Another method is possible if the value of the second virial coefficient at
  the reference temperature <math|T<rsub|1>> is known. This value can be used
  in the virial equation (Eq. <reference|c2 eq virial-vm>) together with the
  values of <math|T<rsub|1>> and <math|p<rsub|1>> to evaluate the molar
  volume <math|V<rsub|<text|m>>>. Then, assuming <math|V<rsub|<text|m>>> is
  the same in both equilibrations of a measurement pair, it is possible to
  evaluate <math|<frac|p<rsub|2>\<cdot\>V<rsub|<text|m>>|R>> at temperature
  <math|T<rsub|2>>, and <math|T<rsub|2>> can be found from

  <equation-cov2|T<rsub|2>=<below|<text|lim>|p<rsub|2>\<rightarrow\>0>
  <frac|p<rsub|2>\<cdot\>V<rsub|<text|m>>|R>|(gas)>

  Constant-volume gas thermometry can be used to evaluate the second virial
  coefficient of the gas at temperature <math|T<rsub|2>> if the value at
  <math|T<rsub|1>> is known (Prob. <reference|prb:2-He-virial>).

  The principles of measurements with a gas thermometer are simple, but in
  practice great care is needed to obtain adequate precision and accuracy.
  Corrections or precautions are required for such sources of error as
  thermal expansion of the gas bulb, \Pdead volume\Q between the bulb and
  pressure measurement system, adsorption of the thermometric gas on interior
  surfaces, and desorption of surface contaminants.

  <index-complex|<tuple|gas|thermometry>||c2 sec
  prop-temperature-thermometry-gas|<tuple|Gas|thermometry>><index-complex|<tuple|thermometry|gas>||c2
  sec prop-temperature-thermometry-gas idx2|<tuple|Thermometry|gas>>

  Since 1960 primary methods with lower uncertainty than gas thermometry have
  been developed and improved. <index|Acoustic gas
  thermometry><subindex|Thermometry|acoustic gas><em|Acoustic gas
  thermometry> is based on the speed of sound in an ideal monatomic gas
  (helium or argon).<\footnote>
    Ref. <cite|moldover-14>.
  </footnote> The gas is confined in a metal cavity resonator of known
  internal dimensions. The thermodynamic temperature of the gas is calculated
  from <math|T=<around*|(|<frac|3|5>|)>\<cdot\><frac|M\<cdot\>v<rsup|2>|R>>,
  where <math|M> is the average molar mass of the gas and <math|v> is the
  measured speed of sound in the limit of zero frequency. To evaluate
  <math|T> in a phase of interest, small thermometers such as platinum
  resistor thermometers (page <pageref|c2 sec prop-temperature-thermometry>)
  are moved from thermal contact with the phase to the outside of the metal
  resonator shell, and the readings compared.

  Values of thermodynamic temperatures <math|T> in the range <math|118
  <text|K>> to <math|323 <text|K>> obtained by acoustic gas thermometry agree
  with <math|T<rsub|90>> on the ITS-90 scale to within <math|0.006
  <text|K>>.<\footnote>
    Ref. <cite|underwood-17>.
  </footnote> The agreement becomes better the closer <math|T> is to the
  water triple point <math|273.16 <text|K>>. <math|T> and <math|T<rsub|90>>
  are equal at <math|273.16 <text|K>>.

  Other kinds of primary thermometry capable of low uncertainty
  include<\footnote>
    Ref. <cite|fischer-16>.
  </footnote>

  <\itemize-dot>
    <item><index|Dielectric constant gas thermometry><subindex|Thermometry|dielectric
    constant gas><em|Dielectric constant gas thermometry>, based on the
    variation of the dielectric constant of an ideal gas with temperature;

    <item><index|Johnson noise thermometry><subindex|Thermometry|Johnson
    noise><em|Johnson noise thermometry>, based on measurements of the
    mean-square noise voltage developed in a resistor;

    <item><index|Doppler broadening thermometry><subindex|Thermometry|Doppler
    broadening><em|Doppler broadening thermometry>, based on measurements of
    the Doppler width of the absorption line when a laser beam passes through
    a gas.
  </itemize-dot>

  <index-complex|<tuple|primary thermometry>||c2 sec
  prop-temperature-thermometry idx1|<tuple|Primary
  thermometry>><index-complex|<tuple|thermometry|primary>||c2 sec
  prop-temperature-thermometry idx2|<tuple|Thermometry|primary>>

  <subsubsection|Practical thermometers><label|c2 sec
  prop-temperature-thermometers><label|2-practical thermometers>

  <subindex|Thermometer|liquid-in-glass><em|Liquid-in-glass thermometers> use
  indicating liquids whose volume change with temperature is much greater
  than that of the glass. A mercury-in-glass thermometer can be used in the
  range <math|234 <text|K>> (the freezing point of mercury) to <math|600
  <text|K>>, and typically can be read to <math|0.01 <text|K>>. A
  <subindex|Thermometer|Beckmann>Beckmann thermometer covers a range of only
  a few kelvins but can be read to <math|0.001 <text|K>>.

  A <subindex|Thermometer|resistance><em|resistance thermometer> is included
  in a circuit that measures the thermometer's electric resistance.
  <subindex|Thermometer|platinum resistance>Platinum resistance thermometers
  are widely used because of their stability and high sensitivity
  (<math|0.0001 <text|K>>). <subindex|Thermometer|thermistor>Thermistors use
  metal oxides and can be made very small; they have greater sensitivity than
  platinum resistance thermometers but are not as stable over time.

  A <subindex|Thermometer|thermocouple><em|thermocouple> consists of wires of
  two dissimilar metals (e.g., constantan alloy and copper) connected in
  series at soldered or welded junctions. A many-junction thermocouple is
  called a <subindex|Thermometer|thermopile><em|thermopile>. When adjacent
  junctions are placed in thermal contact with bodies of different
  temperatures, an electric potential develops that is a function of the two
  temperatures.

  Finally, two other temperature-measuring devices are the
  <subindex|Thermometer|quartz crystal><em|quartz crystal thermometer>,
  incorporating a quartz crystal whose resonance frequency is temperature
  dependent, and <subindex|Thermometer|optical pyrometer><em|optical
  pyrometers>, which are useful above about <math|1300 <text|K>> to measure
  the radiant intensity of a black body emitter.

  The national laboratories of several countries, including the National
  Institute of Standards and Technology in the United States, maintain stable
  secondary thermometers (e.g., platinum resistance thermometers and
  thermocouples) that have been calibrated according to the ITS-90 scale.
  These secondary thermometers are used as working standards to calibrate
  other laboratory and commercial temperature-measuring devices.

  The <index|PLTS-2000>PLTS-2000 scale from <math|0.9 <text|mK>> to <math|1
  <text|K>> and the <index|ITS-90>ITS-90 scale from <math|0.65 <text|K>>
  upwards are expected to continue to be used for precise, reproducible and
  practical approximations to thermodynamic temperature. In the temperature
  range <math|23 <text|K>> \U <math|1233 <text|K>>, the most precise
  measurements will be traceable to platinum resistance thermometers
  calibrated according to the ITS-90 scale.<\footnote>
    Ref. <cite|fellmuth-16>.
  </footnote>

  <index-complex|<tuple|Temperature>||c2 sec prop-temperature
  idx1|<tuple|Temperature|measurement of>>

  <section|The State of the System><label|c2 sec sots><label|2-state of the
  system>

  <index-complex|<tuple|state|system>||c2 sec sots idx1|<tuple|State|of a
  system>><index-complex|<tuple|system|state of>||c2 sec sots
  idx2|<tuple|System|state of>>The thermodynamic
  <index-complex|<tuple|state|system>|||<tuple|State|of a
  system>><index-complex|<tuple|system|state>|||<tuple|System|state
  of>><strong|state><glossary|state> of the system is an important and subtle
  concept.

  <\quote-env>
    Do not confuse the <em|state> of the system with the kind of <em|physical
    state> or state of aggregation of a phase discussed in Sec. <reference|c2
    sec states-physical>. A <em|change of state> refers to a change in the
    state of the system, not necessarily to a phase transition.
  </quote-env>

  At each instant of time, the system is in some definite state that we may
  describe with values of the macroscopic properties we consider to be
  relevant for our purposes. The values of these properties at any given
  instant define the state at that instant. Whenever the value of any of
  these properties changes, the state has changed. If we subsequently find
  that each of the relevant properties has the value it had at a certain
  previous instant, then the system has returned to its previous state.

  <subsection|State functions and independent variables><label|2-state fncs
  \ ind variables><label|c2 sec sots-functions>

  <index-complex|<tuple|state function>||c2 sec sots-functions|<tuple|State
  function>>The properties whose values at each instant depend only on the
  state of the system at that instant, and not on the past or future history
  of the system, are called <index|State function><strong|state
  functions><glossary|state functions> (or state variables, or state
  parameters). There may be other system properties that we consider to be
  irrelevant to the state, such as the shape of the system, and these are
  <em|not> state functions.

  Various conditions determine what states of a system are physically
  possible. If a uniform phase has an equation of state, property values must
  be consistent with this equation. The system may have certain built-in or
  externally-imposed conditions or constraints that keep some properties from
  changing with time. For instance, a closed system has constant mass; a
  system with a rigid boundary has constant volume. We may know about other
  conditions that affect the properties during the time the system is under
  observation.

  We can define the state of the system with the values of a certain minimum
  number of state functions which we treat as the <index|Independent
  variables><subindex|Variables|independent><strong|independent
  variables><glossary|independent variables>. Once we have selected a set of
  independent variables, consistent with the physical nature of the system
  and any conditions or constraints, we can treat all other state functions
  as <index|Dependent variable><subindex|Variables|dependent><strong|dependent
  variables><glossary|dependent variables> whose values depend on the
  independent variables.

  Whenever we adjust the independent variables to particular values, every
  other state function is a dependent variable that can have only one
  definite, reproducible value. For example, in a single-phase system of a
  pure substance with <math|T>, <math|p>, and <math|n> as the independent
  variables, the volume is determined by an equation of state in terms of
  <math|T>, <math|p>, and <math|n>; the mass is equal to <math|n\<cdot\>M>;
  the molar volume is given by <math|V<rsub|<text|m>>=<frac|V|n>>; and the
  density is given by <math|\<rho\>=<frac|n\<cdot\>M|V>>.

  <subsection|An example: state functions of a mixture><label|c2 sec
  sots-functions-example>

  Table <reference|c2 tab-state> lists the values of ten state functions of
  an aqueous sucrose solution in a particular state. The first four
  properties (<math|T>, <math|p>, <math|n<rsub|<text|A>>>,
  <math|n<rsub|<text|B>>>) are ones that we can vary independently, and their
  values suffice to define the state for most purposes. Experimental
  measurements will convince us that, whenever these four properties have
  these particular values, each of the other properties has the one definite
  value listed\Vwe cannot alter any of the other properties without changing
  one or more of the first four variables. Thus we can take <math|T>,
  <math|p>, <math|n<rsub|<text|A>>>, and <math|n<rsub|<text|B>>> as the
  independent variables, and the six other properties as dependent variables.
  The other properties include one (<math|V>) that is determined by an
  <index|Equation of state>equation of state; three (<math|m>,
  <math|\<rho\>>, and <math|x<rsub|<text|B>>>) that can be calculated from
  the independent variables and the equation of state; a solution property
  (<math|\<Pi\>>) treated by thermodynamics (Sec. <reference|c12 sec
  cpds-pressure-osmotic>); and an optical property (<math|n<rsub|<text|D>>>).
  In addition to these six dependent variables, this system has innumerable
  others: energy, isothermal compressibility, heat capacity at constant
  pressure, and so on.<float|float|thb|<\big-table>
    <bktable3|<tformat|<cwith|1|-1|2|2|cell-halign|L=>|<table|<row|<cell|temperature>|<cell|<math|T=293.15
    <text|K>>>>|<row|<cell|pressure>|<cell|<math|p=1.01
    <text|bar>>>>|<row|<cell|amount of water>|<cell|<math|n<rsub|<text|A>>=39.18
    <text|mol>>>>|<row|<cell|amount of sucrose>|<cell|<math|n<rsub|<text|B>>=1.375
    <text|mol>>>>|<row|<cell|volume>|<cell|<math|V=1000
    <text|cm><rsup|<math|3>>>>>|<row|<cell|mass>|<cell|<math|m=1176.5
    <text|g>>>>|<row|<cell|density>|<cell|<math|\<rho\>=1.1765
    <frac|<text|g>|<text|cm><rsup|3>>>>>|<row|<cell|mole fraction of
    sucrose>|<cell|<math|x<rsub|<text|B>>=0.03390>>>|<row|<cell|osmotic
    pressure>|<cell|<math|\<Pi\>=58.2 <text|bar>>>>|<row|<cell|refractive
    index, sodium D line>|<cell|<math|n<rsub|<text|D>>=1.400>>>>>>

    \;
  <|big-table>
    <label|c2 tab-state><label|tbl:2-state>Values of state functions of an
    aqueous sucrose solution (A = water, B = sucrose)
  </big-table>>

  We could make other choices of the independent variables for the aqueous
  sucrose system. For instance, we could choose the set <math|T>, <math|p>,
  <math|V>, and <math|x<rsub|<text|B>>>, or the set <math|p>, <math|V>,
  <math|\<rho\>>, and <math|x<rsub|<text|B>>>. If there are no imposed
  conditions, the <em|number> of <subindex|Variables|number of
  independent>independent variables of this system is always four. (Note that
  we could not arbitrarily choose just any four variables. For instance,
  there are only three independent variables in the set <math|p>, <math|V>,
  <math|m>, and <math|\<rho\>> because of the relation
  <math|\<rho\>=<frac|m|V>>.)

  If the system has imposed conditions, there will be fewer independent
  variables. Suppose the sucrose solution is in a closed system with fixed,
  known values of <math|n<rsub|<text|A>>> and <math|n<rsub|<text|B>>>; then
  there are only two independent variables and we could describe the state by
  the values of just <math|T> and <math|p>.

  <index-complex|<tuple|state function>||c2 sec sots-functions|<tuple|State
  function>>

  <subsection|More about independent variables><label|2-more about ind
  variables><label|c2 sec stos-indvars>

  A closed system containing a single substance in a single phase has two
  independent variables, as we can see by the fact that the state is
  completely defined by values of <math|T> and <math|p> or of <math|T> and
  <math|V>.

  A closed single-phase system containing a mixture of several nonreacting
  substances, or a mixture of reactants and products in reaction equilibrium,
  also has two independent variables. Examples are

  <\itemize-dot>
    <item>air, a mixture of gases in fixed proportions;

    <item>an aqueous ammonia solution containing
    <math|<text|H><rsub|2><text|O>>, <chem|<text|NH><rsub|4>|+>,
    <chem|<text|H>|+>, <chem|OH|->, and probably other species, all in rapid
    continuous equilibrium.\ 
  </itemize-dot>

  The systems in these two examples contain more than one substance, but only
  one <em|component>. The <index|Components, number of>number of components
  of a system is the minimum number of substances or mixtures of fixed
  composition needed to form each phase.<\footnote>
    The concept of the number of components is discussed in more detail in
    Chap. <reference|c13>.
  </footnote> A system of a single pure substance is a special case of a
  system of one component. In an <em|open> system, the amount of each
  component can be used as an independent variable.

  Consider a system with more than one uniform phase. In principle, for each
  phase we could independently vary the temperature, the pressure, and the
  amount of each substance or component. There would then be <math|2+C>
  independent variables for each phase, where <math|C> is the number of
  components in the phase.

  There usually are, however, various equilibria and other conditions that
  reduce the number of independent variables. For instance, each phase may
  have the same temperature and the same pressure; equilibrium may exist with
  respect to chemical reaction and transfer between phases (Sec.
  <reference|c2 sec sots-equilibrium>); and the system may be closed. (While
  these various conditions do not <em|have> to be present, the relations
  among <math|T>, <math|p>, <math|V>, and amounts described by an
  <index|Equation of state>equation of state of a phase are <em|always>
  present.) On the other hand, additional independent variables are required
  if we consider properties such as the surface area of a liquid to be
  relevant.<\footnote>
    The important topic of the number of independent <em|intensive> variables
    is treated by the Gibbs phase rule, which will be discussed in Sec.
    <reference|c8 sec pe-gibbs-phase-pure> for systems of a single substance
    and in Sec. <reference|c13 sec-gprms> for systems of more than one
    substance.
  </footnote>

  <\quote-env>
    We must be careful to choose a set of independent variables that defines
    the state without ambiguity. For a closed system of liquid water, the set
    <math|p> and <math|V> might be a poor choice because the molar volume of
    water passes through a minimum as <math|T> is varied at constant
    <math|p>. Thus, the values <math|p=1.000 <text|bar>> and <math|V=18.016
    <text|cm><rsup|3>> would describe one mole of water at both <math|2
    <rsup|\<circ\>><text|C>> and <math|6 <rsup|\<circ\>><text|C>>, so these
    values would not uniquely define the state. Better choices of independent
    variables in this case would be either <math|T> and <math|p>, or else
    <math|T> and <math|V>.
  </quote-env>

  How may we describe the state of a system that has nonuniform regions? In
  this case we may imagine the regions to be divided into many small volume
  elements or parcels, each small enough to be essentially uniform but large
  enough to contain many molecules. We then describe the state by specifying
  values of independent variables for each volume element. If there is
  internal macroscopic motion (e.g., flow), then velocity components can be
  included among the independent variables. Obviously, the quantity of
  information needed to describe a complicated state may be enormous.

  We can imagine situations in which classical thermodynamics would be
  completely incapable of describing the state. For instance, turbulent flow
  in a fluid or a shock wave in a gas may involve inhomogeneities all the way
  down to the molecular scale. Macroscopic variables would not suffice to
  define the states in these cases.

  Whatever our choice of independent variables, all we need to know to be
  sure a system is in the same state at two different times is that <em|the
  value of each independent variable is the same at both times>.

  <subsection|Equilibrium states><label|2-eq states><label|c2 sec
  sots-equilibrium>

  <index-complex|<tuple|equilibrium state>||c2 sec sots-equilibrium
  idx1|<tuple|Equilibrium state>><index-complex|<tuple|state|equilibrium>||c2
  sec sots-equilibrium idx2|<tuple|State|equilibrium>>An <index|Equilibrium
  state><subindex|State|equilibrium><strong|equilibrium
  state><glossary|equilibrium state> is a state that, when present in an
  <index|Isolated system><subindex|System|isolated>isolated system, remains
  unchanged indefinitely as long as the system remains isolated. (Recall that
  an isolated system is one that exchanges no matter or energy with the
  surroundings.) An equilibrium state of an isolated system has no natural
  tendency to change over time. If changes <em|do> occur in an isolated
  system, they continue until an equilibrium state is reached.

  A system in an equilibrium state may have some or all of the following
  kinds of internal equilibria:<label|kinds of internal equilibria>

  <\description-paragraphs>
    <item*|Thermal equilibrium:><subindex|Equilibrium|thermal>the temperature
    is uniform throughout.
  </description-paragraphs>

  <\description-paragraphs>
    <item*|Mechanical equilibrium:><subindex|Equilibrium|mechanical>the
    temperature is uniform throughout
  </description-paragraphs>

  <\description-paragraphs>
    <item*|Transfer equilibrium:><subindex|Equilibrium|transfer>there is
    equilibrium with respect to the transfer of each species from one phase
    to another.
  </description-paragraphs>

  <\description-paragraphs>
    <item*|Reaction equilibrium:><subindex|Equilibrium|reaction>every
    possible chemical reaction is at equilibrium
  </description-paragraphs>

  A <em|homogeneous> system has a single phase of uniform temperature and
  pressure, and so has thermal and mechanical equilibrium. It is in an
  equilibrium state if it also has reaction equilibrium.

  A heterogeneous system is in an equilibrium state if each of the four kinds
  of internal equilibrium is present.

  The meaning of internal equilibrium in the context of an equilibrium state
  is that no perceptible change of state occurs during the period we keep the
  isolated system under observation. For instance, a system containing a
  homogeneous mixture of gaseous <math|<text|H><rsub|2>> and
  <math|<text|O><rsub|2>> at <math|25 <rsup|\<circ\>><text|C>> and <math|1
  <text|bar>> is in a state of reaction equilibrium on a time scale of hours
  or days; but if a measurable amount of <math|<text|H><rsub|2><text|O>>
  forms over a longer period, the state is not an equilibrium state on this
  longer time scale. This consideration of time scale is similar to the one
  we apply to the persistence of deformation in distinguishing a solid from a
  fluid (Sec. <reference|c2 sec states-physical>).

  Even if a system is not in internal equilibrium, it can be in an
  equilibrium state if a change of state is prevented by an imposed internal
  constraint or the influence of an <index|External
  field><subindex|Field|external>external field. Here are five examples of
  such states:

  <\itemize-dot>
    <item>A system with an internal adiabatic partition separating two phases
    can be in an equilibrium state that is not in thermal equilibrium. The
    adiabatic partition allows the two phases to remain indefinitely at
    different temperatures. If the partition is rigid, it can also allow the
    two phases to have different pressures, so that the equilibrium state
    lacks mechanical equilibrium.

    <item>An experimental system used to measure osmotic pressure (Fig.
    <vpageref|fig:12-osmotic pressure>) has a <index|Membrane,
    semipermeable>semipermeable membrane separating a liquid solution phase
    and a pure solvent phase. The membrane prevents the transfer of solute
    from the solution to the pure solvent. In the equilibrium state of this
    system, the solution has a higher pressure than the pure solvent; the
    system is then in neither transfer equilibrium nor mechanical
    equilibrium.

    <item>In the equilibrium state of a <index-complex|<tuple|galvanic
    cell|equilibrium state>|||<tuple|Galvanic cell|in an equilibrium
    state>>galvanic cell that is not part of a closed electrical circuit
    (Sec. <reference|c3 sec ew-galvanic>), the separation of the reactants
    and products and the open circuit are constraints that prevent the cell
    reaction from coming to reaction equilibrium.

    <item>A system containing mixed reactants of a reaction can be in an
    equilibrium state without reaction equilibrium if we withhold a catalyst
    or initiator or introduce an inhibitor that prevents reaction. In the
    example above of a mixture of <math|<text|H><rsub|2>> and
    <math|<text|O><rsub|2>> gases, we could consider the high activation
    energy barrier for the formation of <math|<text|H><rsub|2><text|O>> to be
    an internal constraint. If we remove the constraint by adding a catalyst,
    the reaction will proceed explosively.

    <item>An example of a system influenced by an external field is a tall
    column of gas in a <subindex|Gravitational|field><subindex|Field|gravitational>gravitational
    field (Sec. <reference|c8 sec pe-gas-column-gravity>). In order for an
    equilibrium state to be established in this field, the pressure must
    decrease continuously with increasing elevation.
  </itemize-dot>

  Keep in mind that regardless of the presence or absence of internal
  constraints and external fields, the essential feature of an equilibrium
  state is this: if we isolate the system while it is in this state, <em|the
  state functions do not change over time>.

  Three additional comments can be made regarding the properties of
  equilibrium states.

  <\enumerate-numeric>
    <item>It should be apparent that a system with thermal equilibrium has a
    single value of <math|T> , and one with mechanical equilibrium has a
    single value of <math|p>, and this allows the state to be described by a
    minimal number of independent variables. In contrast, the definition of a
    nonequilibrium state with nonuniform intensive properties may require a
    very large number of independent variables.

    <item>Strictly speaking, during a time period in which the system
    exchanges energy with the surroundings its state cannot be an equilibrium
    state. Energy transfer at a finite rate causes nonuniform temperature and
    pressure within the system and prevents internal thermal and mechanical
    equilibrium. If, however, the rate of energy transfer is small, then at
    each instant the state can closely approximate an equilibrium state. This
    topic will be discussed in detail in the next chapter.

    <item><label|very slow changes>The concept of an equilibrium state
    assumes that when the system is in this state and isolated, no observable
    change occurs during the period of experimental observation.
  </enumerate-numeric>

  <\quote-env>
    If the state does, in fact, undergo a slow change that is too small to be
    detected during the experimental observation period
    <math|\<mathLaplace\>*t<rsub|<text|exp>>>, the state is <index|Metastable
    state><subindex|State|metastable><em|metastable>\Vthe relaxation time of
    the slow change is much greater than <math|\<mathLaplace\>*t<rsub|<text|exp>>>.
    There is actually no such thing as a true equilibrium state, because very
    slow changes inevitably take place that we have no way of controlling.
    One example was mentioned above: the slow formation of water from its
    elements in the absence of a catalyst. Atoms of radioactive elements with
    long half-lives slowly change to other elements. More generally, all
    elements are presumably subject to eventual transmutation to iron-58 or
    nickel-62, the nuclei with the greatest binding energy per nucleon. When
    we use the concept of an equilibrium state, we are in effect assuming
    that rapid changes that have come to equilibrium have relaxation times
    much shorter than <math|\<mathLaplace\>*t<rsub|<text|exp>>> and that the
    relaxation times of all other changes are infinite.
  </quote-env>

  <index-complex|<tuple|equilibrium state>||c2 sec sots-equilibrium
  idx1|<tuple|Equilibrium state>><index-complex|<tuple|state|equilibrium>||c2
  sec sots-equilibrium idx2|<tuple|State|equilibrium>>

  <subsection|Steady states><label|c2 sec sots-steady>

  It is important not to confuse an equilibrium state with a <label|steady
  state><index|Steady state><subindex|State|steady><strong|steady
  state><glossary|steady state>, a state that is constant during a time
  period during which the system exchanges matter or energy with the
  surroundings.

  The heat-conducting metal rod shown in Fig. <reference|c2 fig-steady-state>
  is a system in such a steady state. Each end of the rod is in thermal
  contact with a <subindex|Heat|reservoir><strong|heat
  reservoir><glossary|heat reservoir> (or
  <subindex|Thermal|reservoir><strong|thermal reservoir><glossary|thermal
  reservoir><label|2-heat reservoir>), which is a body or external system
  whose temperature remains constant and uniform when there is heat transfer
  to or from it.<\footnote>
    A heat reservoir can be a body that is so large that its temperature
    changes only imperceptibly during heat transfer, or an external system of
    coexisting phases of a pure substance (e.g., ice and water) at constant
    pressure.
  </footnote> The two heat reservoirs in the figure have different
  temperatures, causing a temperature gradient to form along the length of
  the rod and energy to be transferred by heat from the warmer reservoir to
  the rod and from the rod to the cooler reservoir. Although the properties
  of the steady state of the rod remain constant, the rod is clearly not in
  an equilibrium state because the temperature gradient will quickly
  disappear when we isolate the rod by removing it from contact with the heat
  reservoirs.<float|float|thb|<\big-figure|<image|../img/c2_fig..heat_conduction_steady_state_rod_example.eps|229pt|44pt||>>
    <label|c2 fig-steady-state><label|fig:2-steady state>Steady state in a
    metal rod (shaded) with heat conduction. The boxes at the ends represent
    heat reservoirs of constant temperature.
  </big-figure>>

  <index-complex|<tuple|state|system>||c2 sec sots idx1|<tuple|State|of a
  system>><index-complex|<tuple|system|state of>||c2 sec sots
  idx2|<tuple|System|state of>>

  <section|Processes and Paths><label|2-processes and paths><label|c2 sec
  pap>

  A <index|Process><strong|process><glossary|process> is a change in the
  state of the system over time, starting with a definite initial state and
  ending with a definite final state. The process is defined by a
  <index|Path><strong|path><glossary|path>, which is the continuous sequence
  of consecutive states through which the system passes, including the
  initial state, the intermediate states, and the final state. The process
  has a direction along the path. The path could be described by a curve in
  an <math|N>-dimensional space in which each coordinate axis represents one
  of the <math|N> independent variables.

  This book takes the view that a thermodynamic process is defined by what
  happens <em|within> the system, in the three-dimensional region up to and
  including the boundary, and by the forces exerted on the system by the
  surroundings and any external field. Conditions and changes in the
  surroundings are not part of the process except insofar as they affect
  these forces. For example, consider a process in which the system
  temperature decreases from <math|300 <text|K>> to <math|273 <text|K>>. We
  could accomplish this temperature change by placing the system in thermal
  contact with either a refrigerated thermostat bath or a mixture of ice and
  water. The process is the same in both cases, but the surroundings are
  different.

  <index|Expansion><subindex|Process|expansion><strong|Expansion><glossary|expansion>
  is a process in which the system volume increases; in
  <index|Compression><subindex|Process|compression><strong|compression><glossary|compression>,
  the volume decreases.

  An <subindex|Isothermal|process><subindex|Process|isothermal><strong|isothermal><glossary|isothermal>
  process is one in which the temperature of the system remains uniform and
  constant. An <index|Isobaric Process><subindex|Process|isobaric><strong|isobaric><glossary|isobaric>
  or <subindex|Isopiestic|process><subindex|Process|isopiestic><strong|isopiestic><glossary|isopiestic>
  process refers to uniform constant pressure, and an <index|Isochoric
  process><subindex|Process|isochoric><strong|isochoric><glossary|isochoric>
  process refers to constant volume. Paths for these processes of an ideal
  gas are shown in Fig. <reference|c2 fig-paths>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|02-SUP/3-PATHS.eps|267pt|88pt||>>
        <label|c2 fig-paths><label|fig:2-paths>Paths of three processes of a
        closed ideal-gas system with <math|p> and <math|V> as the independent
        variables. (a)<nbsp>Isothermal expansion. (b)<nbsp>Isobaric
        expansion. (c)<nbsp>Isochoric pressure reduction.
      </big-figure>
    </framed>
  </float>

  An <subindex|Adiabatic|process><subindex|Process|adiabatic><strong|adiabatic><glossary|adiabatic>
  process is one in which there is no heat transfer across any portion of the
  boundary. We may ensure that a process is adiabatic either by using an
  adiabatic boundary or, if the boundary is diathermal, by continuously
  adjusting the external temperature to eliminate a temperature gradient at
  the boundary.

  Recall that a state function is a property whose value at each instant
  depends only on the state of the system at that instant. The
  <subindex|State function|change of>finite change of a state function
  <math|X> in a process is written <math|\<Delta\>*X>. The notation
  <math|\<Delta\>*X> always has the meaning <math|X<rsub|2>-X<rsub|1>>, where
  <math|X<rsub|1>> is the value in the initial state and <math|X<rsub|2>> is
  the value in the final state. Therefore, the value of <math|\<Delta\>*X>
  depends only on the values of <math|X<rsub|1>> and <math|X<rsub|2>>.
  <em|The change of a state function during a process depends only on the
  initial and final states of the system, not on the path of the process>.

  An infinitesimal <subindex|State function|change of>change of the state
  function <math|X> is written <math|<text|d>*X>. The mathematical operation
  of summing an infinite number of infinitesimal changes is integration, and
  the sum is an integral (see the brief calculus review in Appendix
  <reference|appendix calculus-review>). The sum of the infinitesimal changes
  of <math|X> along a path is a definite integral equal to
  <math|\<Delta\>*X>:

  <\equation>
    <big|int><rsub|X<rsub|1>><rsup|X<rsub|2>><text|d>*X=X<rsub|2>-X<rsub|1>=\<Delta\>*X
  </equation>

  If <math|<text|d>*X> obeys this relation\Vthat is, if its integral for
  given limits has the same value regardless of the path\Vit is called an
  <index|Exact differential><strong|exact differential><glossary|exact
  differential><label|exact differential>. The differential of a state
  function is always an exact differential.

  A <index|Cyclic process><subindex|Process|cyclic><strong|cyclic
  process><glossary|cyclic process> is a process in which the state of the
  system changes and then returns to the initial state. In this case the
  integral of <math|<text|d>*X> is written with a cyclic integral sign:
  <math|<big|oint><text|d>*X>. Since a state function <math|X> has the same
  initial and final values in a cyclic process, <math|X<rsub|2>> is equal to
  <math|X<rsub|1>> and the cyclic integral of <math|<text|d>*X> is zero:

  <\equation>
    <label|oint(dX)=0><big|oint><text|d>*X=0
  </equation>

  Heat (<math|q>) and work (<math|w>) are examples of quantities that are
  <em|not> state functions. They are not even properties of the system;
  instead they are quantities of energy transferred across the boundary over
  a period of time. It would therefore be incorrect to write
  \P<math|\<Delta\>*q>\Q or \P<math|\<Delta\>*w>.\Q Instead, the values of
  <math|q> and <math|w> depend in general on the path and are called
  <index|Path function><strong|path functions><glossary|path functions>.

  This book uses the symbol � (lowercase letter \Pd\Q with a bar through the
  stem<\footnote>
    The Unicode number for the glyph � is <verbatim|U+0111> and its name is
    \P<verbatim|LATIN SMALL LETTER D WITH STROKE>\Q.
  </footnote>l) for an infinitesimal quantity of a path function. Thus,
  <math|<text|�>*q> and <math|<text|�>*w> are infinitesimal quantities of
  heat and work. The sum of many infinitesimal quantities of a path function
  is the <em|net> quantity:

  <\equation>
    <label|c2 eq int dq=q, int dw=w><label|int dq=q,
    intdw=w><tabular|<tformat|<table|<row|<cell|<big|int><text|�>*q=q>|<cell|<text|<space|2em>>>|<cell|<big|int><text|�>*w=w>>>>>
  </equation>

  The infinitesimal quantities <math|<text|�>*q> and <math|<text|�>*w>,
  because the values of their integrals depend on the path, are
  <index|Inexact differential><subindex|Differential|inexact><strong|inexact
  differentials><glossary|inexact differentials>.<\footnote>
    Chemical thermodynamicists often write these quantities as
    <math|<text|d>*q> and <math|<text|d>*w>. Mathematicians, however, frown
    on using the same notation for inexact and exact differentials. Other
    notations sometimes used to indicate that heat and work are path
    functions are <math|<text|D>*q> and <math|<text|D>*w>, and also
    <math|\<delta\>*q> and <math|\<delta\>*w>.
  </footnote>

  There is a fundamental difference between a state function (such as
  temperature or volume) and a path function (such as heat or work): <em|The
  value of a state function refers to one instant of time; the value of a
  path function refers to an interval of time>.

  <\quote-env>
    State function and path function in thermodynamics are analogous to
    elevation and distance in climbing a mountain. Suppose there are several
    trails of different lengths from the trailhead to the summit. The climber
    at each instant is at a definite elevation, and the elevation change
    during the climb depends only on the trailhead and summit elevations and
    not on the trail used. Thus elevation is like a state function. The
    distance traveled by the climber depends on the trail, and is like a path
    function.
  </quote-env>

  <section|The Energy of the System><label|2-energy of the system><label|c2
  sec eots>

  <index-complex|<tuple|energy|system>||c2 sec eots idx1|<tuple|Energy|of the
  system>>A large part of classical thermodynamics is concerned with the
  energy of the system. The total energy of a system is an extensive property
  whose value at any one instant cannot be measured in any practical way, but
  whose change is the focus of the first law of thermodynamics
  (<reference|c3>).

  <subsection|Energy and reference frames><label|c2 sec eots-frames>

  Classical thermodynamics ignores microscopic properties such as the
  behavior of individual atoms and molecules. Nevertheless, a consideration
  of the classical mechanics of particles will help us to understand the
  sources of the potential and kinetic energy of a thermodynamic system.

  In classical mechanics, the <index|Energy>energy of a collection of
  interacting point particles is the sum of the kinetic energy
  <math|<frac|1|2>*m*v<rsup|2>> of each particle (where <math|m> is the
  particle's mass and <math|v> is its velocity), and of various kinds of
  potential energies. The potential energies are defined in such a way that
  if the particles are isolated from the rest of the universe, as the
  particles move and interact with one another the total energy (kinetic plus
  potential) is constant over time. This principle of the conservation of
  energy also holds for real atoms and molecules whose electronic,
  vibrational, and rotational energies, absent in point particles, are
  additional contributions to the total energy.

  The positions and velocities of particles must be measured in a specified
  system of coordinates called a <index|Reference
  frame><subindex|Frame|reference><em|reference frame>. This book will use
  reference frames with <em|Cartesian> axes. Since the kinetic energy of a
  particle is a function of velocity, the kinetic energy depends on the
  choice of the reference frame. A particularly important kind is an
  <index|Inertial reference frame><subsubindex|Frame|reference|inertial><em|inertial>
  frame, one in which Newton's laws of motion are obeyed (see Sec.
  <reference|app-forces between-part> in Appendix <reference|appendix
  forces-energy-work>).

  A reference frame whose axes are fixed relative to the earth's surface is
  what this book will call a <index|Lab frame><subindex|Frame|lab><em|lab
  frame>. A lab frame for all practical purposes is inertial (Sec.
  <vpageref|app-forces earth-fixed-frame>). It is in this kind of stationary
  frame that the laws of thermodynamics have been found by experiment to be
  valid.

  The energy <math|E> of a thermodynamic system is the sum of the energies of
  the particles contained in it and the potential energies of interaction
  between these particles. Just as for an individual particle, the energy of
  the system depends on the reference frame in which it is measured. The
  energy of the system may change during a process, but the principle of the
  conservation of energy ensures that the sum of the energy of the system,
  the energy of the surroundings, and any energy shared by both, all measured
  in the same reference frame, remains constant over time.

  This book uses the symbol <math|E<rsub|<text|sys>>> for the energy of the
  system measured in a specified inertial frame. The system could be located
  in a weightless environment in outer space, and the inertial frame could be
  one that is either fixed or moving at constant velocity relative to local
  stars. Usually, however, the system is located in the earth's gravitational
  field, and the appropriate inertial frame is then an earth-fixed lab frame.

  If during a process the system as a whole undergoes motion or rotation
  relative to the inertial frame, then <math|E<rsub|<text|sys>>> depends in
  part on coordinates that are not properties of the system. In such
  situations <math|E<rsub|<text|sys>>> is not a state function, and we need
  the concept of internal energy.

  <subsection|Internal energy><label|2-internal energy><label|c2 sec
  eots-internal>

  <index-complex|<tuple|internal energy>||c2 sec eots-internal
  idx1|<tuple|Internal energy>><index-complex|<tuple|energy|internal>||c2 sec
  eots-internal idx2|<tuple|Energy|internal>>The <index|Internal
  energy><subindex|Energy|internal><strong|internal energy><glossary|internal
  energy>, <math|U>, is the energy of the system measured in a reference
  frame that allows <math|U> to be a state function\Vthat is, at each instant
  the value of <math|U> depends only on the state of the system. This book
  will call a reference frame with this property a <index|Local
  frame><subindex|Frame|local><em|local frame>. A local frame may also be,
  but is not necessarily, an earth-fixed lab frame.

  Here is a simple illustration of the distinction between the energy
  <math|E<rsub|<text|sys>>> of a system measured in a lab frame and the
  internal energy <math|U> measured in a local frame. Let the <em|system> be
  a fixed amount of water of uniform temperature <math|T> and pressure
  <math|p> contained in a glass beaker. (The glass material of the beaker is
  part of the surroundings.) The state of this system is defined by the
  independent variables <math|T> and <math|p>. The most convenient local
  frame in which to measure <math|U> in this case is a frame fixed with
  respect to the beaker.

  <\itemize-dot>
    <item>When the beaker is at rest on the lab bench, the local frame is a
    lab frame; then the energies <math|E<rsub|<text|sys>>> and <math|U> are
    equal and depend only on <math|T> and <math|p>.

    <item>If we place the beaker on a laboratory hot plate and use the hot
    plate to raise the temperature of the water, the values of
    <math|E<rsub|<text|sys>>> and <math|U> increase equally.

    <item>Suppose we slide the beaker horizontally along the lab bench while
    <math|T> and <math|p> stay constant. While the system is in motion, its
    kinetic energy is greater in the lab frame than in the local frame. Now
    <math|E<rsub|<text|sys>>> is greater than when the beaker was at rest,
    although the state of the system and the value of <math|U> are unchanged.

    <item>If we slowly lift the beaker above the bench, the potential energy
    of the water in the earth's <subindex|Gravitational|field><subindex|Field|gravitational>gravitational
    field increases, again with no change in <math|T> and <math|p>. The value
    of <math|E<rsub|<text|sys>>> has increased, but there has been no change
    in the state of the system or the value of <math|U>.
  </itemize-dot>

  Section <reference|c3 sec hwfl-work> will show that the relation between
  changes of the system energy and the internal energy in this example is
  <math|\<Delta\>*E<rsub|<text|sys>>=\<Delta\>*E<rsub|<text|k>>+\<Delta\>*E<rsub|<text|p>>+\<Delta\>*U>,
  where <math|E<rsub|<text|k>>> and <math|E<rsub|<text|p>>> are the kinetic
  and potential energies of the system as a whole measured in the lab frame.

  Our choice of the local frame used to define the internal energy <math|U>
  of any particular system during a given process is to some extent
  arbitrary. Three possible choices are as follows.

  <\itemize-dot>
    <item>If the system as a whole does not move or rotate in the laboratory,
    a lab frame is an appropriate local frame. Then <math|U> is the same as
    the system energy <math|E<rsub|<text|sys>>> measured in the lab frame.

    <item>If the system's center of mass moves in the lab frame during the
    process, we can let the local frame be a <index|Center-of-mass
    frame><subindex|Frame|center-of-mass><em|center-of-mass frame> whose
    origin moves with the center of mass and whose Cartesian axes are
    parallel to the axes of the lab frame.

    <item>If the system consists of the contents of a rigid container that
    moves or rotates in the lab, as in the illustration above, it may be
    convenient to choose a local frame that has its origin and axes fixed
    with respect to the container.
  </itemize-dot>

  Is it possible to determine a numerical <em|value> for the internal energy
  of a system? The total energy of a body of mass <math|m> when it is at rest
  is given by the <index|Einstein energy relation>Einstein relation
  <math|E=m*c<rsup|2>>, where <math|c> is the speed of light in vacuum. In
  principle, then, we could calculate the internal energy <math|U> of a
  system at rest from its mass, and we could determine <math|\<Delta\>*U> for
  a process from the change in mass. In practice, however, an absolute value
  of <math|U> calculated from a measured mass has too much uncertainty to be
  of any practical use. For example, the typical uncertainty of the mass of
  an object measured with a microbalance, about <math|0.1 <text|\<#3BC\>g>>
  (Table <reference|c2 tab measurement-methods>), would introduce the
  enormous uncertainty in energy of about <math|10<rsup|10>> joules. Only
  values of the <em|change> <math|\<Delta\>*U> are useful, and these values
  cannot be calculated from <math|\<Delta\>*m> because the change in mass
  during an ordinary chemical process is much too small to be
  detected.<index-complex|<tuple|internal energy>||c2 sec eots-internal
  idx1|<tuple|Internal energy>><index-complex|<tuple|energy|internal>||c2 sec
  eots-internal idx2|<tuple|Energy|internal>>

  <index-complex|<tuple|energy|system>||c2 sec eots idx1|<tuple|Energy|of the
  system>>
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|1>
    <associate|info-flag|detailed>
    <associate|page-first|20>
    <associate|page-height|auto>
    <associate|page-medium|papyrus>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|3>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|2-amount|<tuple|2.3.2|?>>
    <associate|2-energy of the system|<tuple|2.6|?>>
    <associate|2-eq states|<tuple|2.4.4|?>>
    <associate|2-eqn of state|<tuple|2.2.4|?>>
    <associate|2-extensive \ intensive|<tuple|2.1.1|?>>
    <associate|2-fluids|<tuple|2.2.3|?>>
    <associate|2-gas thermometry|<tuple|Gas constant|?>>
    <associate|2-heat reservoir|<tuple|thermal reservoir|?>>
    <associate|2-internal energy|<tuple|2.6.2|?>>
    <associate|2-mass|<tuple|2.3.1|?>>
    <associate|2-molecular weight|<tuple|Molecular weight|?>>
    <associate|2-more about ind variables|<tuple|2.4.3|?>>
    <associate|2-phase coexistence|<tuple|2.2.2|?>>
    <associate|2-physical states|<tuple|2.2.1|?>>
    <associate|2-practical thermometers|<tuple|2.3.6.4|?>>
    <associate|2-pressure|<tuple|2.3.5|?>>
    <associate|2-primary thermometry|<tuple|2.3.6.3|?>>
    <associate|2-processes and paths|<tuple|2.5|?>>
    <associate|2-shear stress|<tuple|shear stress|?>>
    <associate|2-solids|<tuple|2.2.6|?>>
    <associate|2-state fncs \ ind variables|<tuple|2.4.1|?>>
    <associate|2-state of the system|<tuple|2.4|?>>
    <associate|2-system|<tuple|2.1|?>>
    <associate|2-temperature|<tuple|2.3.6|?>>
    <associate|2-virial eqn|<tuple|2.2.5|?>>
    <associate|273.16K=water triple pt|<tuple|<tuple|triple|point|H2O>|?>>
    <associate|B=RTB_p|<tuple|2.2.6|?>>
    <associate|Chap. 2|<tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Chapter
    2>|?>>
    <associate|M=m/n|<tuple|2.3.2|?>>
    <associate|T2(gas thermometer)|<tuple|2.3.7|?>>
    <associate|Vm=RT/p+B|<tuple|2.2.8|?>>
    <associate|auto-1|<tuple|2|?>>
    <associate|auto-10|<tuple|Supersystem|?>>
    <associate|auto-100|<tuple|<tuple|Equation of state|fluid>|?>>
    <associate|auto-101|<tuple|equation of state|?>>
    <associate|auto-102|<tuple|Ideal gas|?>>
    <associate|auto-103|<tuple|<tuple|Equation of state|ideal gas>|?>>
    <associate|auto-104|<tuple|Redlich\UKwong equation|?>>
    <associate|auto-105|<tuple|2.2.5|?>>
    <associate|auto-106|<tuple|Equation of state|?>>
    <associate|auto-107|<tuple|<tuple|virial|equation|pure gas>|?>>
    <associate|auto-108|<tuple|Statistical mechanics|?>>
    <associate|auto-109|<tuple|Virial|?>>
    <associate|auto-11|<tuple|System|?>>
    <associate|auto-110|<tuple|Statistical mechanics|?>>
    <associate|auto-111|<tuple|2.2.3|?>>
    <associate|auto-112|<tuple|Boyle temperature|?>>
    <associate|auto-113|<tuple|Temperature|?>>
    <associate|auto-114|<tuple|Boyle temperature|?>>
    <associate|auto-115|<tuple|Statistical mechanics|?>>
    <associate|auto-116|<tuple|2.2.6|?>>
    <associate|auto-117|<tuple|<tuple|Solid>|?>>
    <associate|auto-118|<tuple|Elastic deformation|?>>
    <associate|auto-119|<tuple|Deformation|?>>
    <associate|auto-12|<tuple|open|?>>
    <associate|auto-120|<tuple|Plastic deformation|?>>
    <associate|auto-121|<tuple|Deformation|?>>
    <associate|auto-122|<tuple|<tuple|Solid>|?>>
    <associate|auto-123|<tuple|2.3|?>>
    <associate|auto-124|<tuple|2.3.1|?>>
    <associate|auto-125|<tuple|<tuple|mass>|?>>
    <associate|auto-126|<tuple|<tuple|mass>|?>>
    <associate|auto-127|<tuple|2.3.2|?>>
    <associate|auto-128|<tuple|<tuple|amount>|?>>
    <associate|auto-129|<tuple|Amount|?>>
    <associate|auto-13|<tuple|System|?>>
    <associate|auto-130|<tuple|Mole|?>>
    <associate|auto-131|<tuple|Relative atomic mass|?>>
    <associate|auto-132|<tuple|Atomic mass, relative|?>>
    <associate|auto-133|<tuple|Atomic weight|?>>
    <associate|auto-134|<tuple|Relative atomic mass|?>>
    <associate|auto-135|<tuple|Molecular mass, relative|?>>
    <associate|auto-136|<tuple|Molecular weight|?>>
    <associate|auto-137|<tuple|<tuple|amount>|?>>
    <associate|auto-138|<tuple|2.3.3|?>>
    <associate|auto-139|<tuple|<tuple|volume>|?>>
    <associate|auto-14|<tuple|closed|?>>
    <associate|auto-140|<tuple|2.3.1|?>>
    <associate|auto-141|<tuple|Liter|?>>
    <associate|auto-142|<tuple|liter|?>>
    <associate|auto-143|<tuple|Milliliter|?>>
    <associate|auto-144|<tuple|<tuple|volume>|?>>
    <associate|auto-145|<tuple|2.3.4|?>>
    <associate|auto-146|<tuple|<tuple|density>|?>>
    <associate|auto-147|<tuple|Volume|?>>
    <associate|auto-148|<tuple|2.3.1|?>>
    <associate|auto-149|<tuple|<tuple|density>|?>>
    <associate|auto-15|<tuple|Diathermal boundary|?>>
    <associate|auto-150|<tuple|2.3.5|?>>
    <associate|auto-151|<tuple|<tuple|pressure|measurement of>|?>>
    <associate|auto-152|<tuple|Isotropic fluid|?>>
    <associate|auto-153|<tuple|Fluid|?>>
    <associate|auto-154|<tuple|Pascal (unit)|?>>
    <associate|auto-155|<tuple|pascal|?>>
    <associate|auto-156|<tuple|Bar|?>>
    <associate|auto-157|<tuple|bar|?>>
    <associate|auto-158|<tuple|Standard|?>>
    <associate|auto-159|<tuple|Pressure|?>>
    <associate|auto-16|<tuple|Boundary|?>>
    <associate|auto-160|<tuple|standard pressure|?>>
    <associate|auto-161|<tuple|<tuple|pressure|measurement of>|?>>
    <associate|auto-162|<tuple|2.3.6|?>>
    <associate|auto-163|<tuple|<tuple|Temperature>|?>>
    <associate|auto-164|<tuple|Temperature scale|?>>
    <associate|auto-165|<tuple|Thermometer|?>>
    <associate|auto-166|<tuple|Zeroth law of thermodynamics|?>>
    <associate|auto-167|<tuple|Maxwell, James Clerk|?>>
    <associate|auto-168|<tuple|2.3.6.1|?>>
    <associate|auto-169|<tuple|<tuple|temperature|equilibrium systems for
    fixed values>|?>>
    <associate|auto-17|<tuple|diathermal|?>>
    <associate|auto-170|<tuple|Ice point|?>>
    <associate|auto-171|<tuple|Steam point|?>>
    <associate|auto-172|<tuple|Melting point|?>>
    <associate|auto-173|<tuple|Triple|?>>
    <associate|auto-174|<tuple|Triple|?>>
    <associate|auto-175|<tuple|2.3.2|?>>
    <associate|auto-176|<tuple|<tuple|temperature|equilibrium systems for
    fixed values>|?>>
    <associate|auto-177|<tuple|2.3.6.2|?>>
    <associate|auto-178|<tuple|<tuple|temperature|scales>|?>>
    <associate|auto-179|<tuple|Ideal-gas temperature|?>>
    <associate|auto-18|<tuple|Adiabatic|?>>
    <associate|auto-180|<tuple|<tuple|Temperature|ideal-gas>|?>>
    <associate|auto-181|<tuple|Thermodynamic|?>>
    <associate|auto-182|<tuple|Temperature|?>>
    <associate|auto-183|<tuple|Kelvin (unit)|?>>
    <associate|auto-184|<tuple|kelvin|?>>
    <associate|auto-185|<tuple|<tuple|triple|point|H2O>|?>>
    <associate|auto-186|<tuple|SI|?>>
    <associate|auto-187|<tuple|Centrigrade scale|?>>
    <associate|auto-188|<tuple|Celsius|?>>
    <associate|auto-189|<tuple|Celsius|?>>
    <associate|auto-19|<tuple|Boundary|?>>
    <associate|auto-190|<tuple|<tuple|triple|point|H2O>|?>>
    <associate|auto-191|<tuple|Ice point|?>>
    <associate|auto-192|<tuple|Steam point|?>>
    <associate|auto-193|<tuple|ITS-90|?>>
    <associate|auto-194|<tuple|2.3.2|?>>
    <associate|auto-195|<tuple|<tuple|temperature|scales>|?>>
    <associate|auto-196|<tuple|2.3.6.3|?>>
    <associate|auto-197|<tuple|<tuple|primary thermometry>|?>>
    <associate|auto-198|<tuple|<tuple|thermometry|primary>|?>>
    <associate|auto-199|<tuple|Boltzmann constant|?>>
    <associate|auto-2|<tuple|2.1|?>>
    <associate|auto-20|<tuple|adiabatic|?>>
    <associate|auto-200|<tuple|Gas constant|?>>
    <associate|auto-201|<tuple|<tuple|gas|thermometry>|?>>
    <associate|auto-202|<tuple|<tuple|thermometry|gas>|?>>
    <associate|auto-203|<tuple|Thermometer|?>>
    <associate|auto-204|<tuple|2.3.3|?>>
    <associate|auto-205|<tuple|<tuple|gas|thermometry>|?>>
    <associate|auto-206|<tuple|<tuple|thermometry|gas>|?>>
    <associate|auto-207|<tuple|Acoustic gas thermometry|?>>
    <associate|auto-208|<tuple|Thermometry|?>>
    <associate|auto-209|<tuple|Dielectric constant gas thermometry|?>>
    <associate|auto-21|<tuple|Isolated system|?>>
    <associate|auto-210|<tuple|Thermometry|?>>
    <associate|auto-211|<tuple|Johnson noise thermometry|?>>
    <associate|auto-212|<tuple|Thermometry|?>>
    <associate|auto-213|<tuple|Doppler broadening thermometry|?>>
    <associate|auto-214|<tuple|Thermometry|?>>
    <associate|auto-215|<tuple|<tuple|primary thermometry>|?>>
    <associate|auto-216|<tuple|<tuple|thermometry|primary>|?>>
    <associate|auto-217|<tuple|2.3.6.4|?>>
    <associate|auto-218|<tuple|Thermometer|?>>
    <associate|auto-219|<tuple|Thermometer|?>>
    <associate|auto-22|<tuple|System|?>>
    <associate|auto-220|<tuple|Thermometer|?>>
    <associate|auto-221|<tuple|Thermometer|?>>
    <associate|auto-222|<tuple|Thermometer|?>>
    <associate|auto-223|<tuple|Thermometer|?>>
    <associate|auto-224|<tuple|Thermometer|?>>
    <associate|auto-225|<tuple|Thermometer|?>>
    <associate|auto-226|<tuple|Thermometer|?>>
    <associate|auto-227|<tuple|PLTS-2000|?>>
    <associate|auto-228|<tuple|ITS-90|?>>
    <associate|auto-229|<tuple|<tuple|Temperature>|?>>
    <associate|auto-23|<tuple|isolated|?>>
    <associate|auto-230|<tuple|2.4|?>>
    <associate|auto-231|<tuple|<tuple|state|system>|?>>
    <associate|auto-232|<tuple|<tuple|system|state of>|?>>
    <associate|auto-233|<tuple|<tuple|state|system>|?>>
    <associate|auto-234|<tuple|<tuple|system|state>|?>>
    <associate|auto-235|<tuple|state|?>>
    <associate|auto-236|<tuple|2.4.1|?>>
    <associate|auto-237|<tuple|<tuple|state function>|?>>
    <associate|auto-238|<tuple|State function|?>>
    <associate|auto-239|<tuple|state functions|?>>
    <associate|auto-24|<tuple|Reference frame|?>>
    <associate|auto-240|<tuple|Independent variables|?>>
    <associate|auto-241|<tuple|Variables|?>>
    <associate|auto-242|<tuple|independent variables|?>>
    <associate|auto-243|<tuple|Dependent variable|?>>
    <associate|auto-244|<tuple|Variables|?>>
    <associate|auto-245|<tuple|dependent variables|?>>
    <associate|auto-246|<tuple|2.4.2|?>>
    <associate|auto-247|<tuple|Equation of state|?>>
    <associate|auto-248|<tuple|2.4.1|?>>
    <associate|auto-249|<tuple|Variables|?>>
    <associate|auto-25|<tuple|Frame|?>>
    <associate|auto-250|<tuple|<tuple|state function>|?>>
    <associate|auto-251|<tuple|2.4.3|?>>
    <associate|auto-252|<tuple|Components, number of|?>>
    <associate|auto-253|<tuple|Equation of state|?>>
    <associate|auto-254|<tuple|2.4.4|?>>
    <associate|auto-255|<tuple|<tuple|equilibrium state>|?>>
    <associate|auto-256|<tuple|<tuple|state|equilibrium>|?>>
    <associate|auto-257|<tuple|Equilibrium state|?>>
    <associate|auto-258|<tuple|State|?>>
    <associate|auto-259|<tuple|equilibrium state|?>>
    <associate|auto-26|<tuple|Body|?>>
    <associate|auto-260|<tuple|Isolated system|?>>
    <associate|auto-261|<tuple|System|?>>
    <associate|auto-262|<tuple|Equilibrium|?>>
    <associate|auto-263|<tuple|Equilibrium|?>>
    <associate|auto-264|<tuple|Equilibrium|?>>
    <associate|auto-265|<tuple|Equilibrium|?>>
    <associate|auto-266|<tuple|External field|?>>
    <associate|auto-267|<tuple|Field|?>>
    <associate|auto-268|<tuple|Membrane, semipermeable|?>>
    <associate|auto-269|<tuple|<tuple|galvanic cell|equilibrium state>|?>>
    <associate|auto-27|<tuple|body|?>>
    <associate|auto-270|<tuple|Gravitational|?>>
    <associate|auto-271|<tuple|Field|?>>
    <associate|auto-272|<tuple|Metastable state|?>>
    <associate|auto-273|<tuple|State|?>>
    <associate|auto-274|<tuple|<tuple|equilibrium state>|?>>
    <associate|auto-275|<tuple|<tuple|state|equilibrium>|?>>
    <associate|auto-276|<tuple|2.4.5|?>>
    <associate|auto-277|<tuple|Steady state|?>>
    <associate|auto-278|<tuple|State|?>>
    <associate|auto-279|<tuple|steady state|?>>
    <associate|auto-28|<tuple|2.1.1|?>>
    <associate|auto-280|<tuple|Heat|?>>
    <associate|auto-281|<tuple|heat reservoir|?>>
    <associate|auto-282|<tuple|Thermal|?>>
    <associate|auto-283|<tuple|thermal reservoir|?>>
    <associate|auto-284|<tuple|2.4.1|?>>
    <associate|auto-285|<tuple|<tuple|state|system>|?>>
    <associate|auto-286|<tuple|<tuple|system|state of>|?>>
    <associate|auto-287|<tuple|2.5|?>>
    <associate|auto-288|<tuple|Process|?>>
    <associate|auto-289|<tuple|process|?>>
    <associate|auto-29|<tuple|2.1.1|?>>
    <associate|auto-290|<tuple|Path|?>>
    <associate|auto-291|<tuple|path|?>>
    <associate|auto-292|<tuple|Expansion|?>>
    <associate|auto-293|<tuple|Process|?>>
    <associate|auto-294|<tuple|expansion|?>>
    <associate|auto-295|<tuple|Compression|?>>
    <associate|auto-296|<tuple|Process|?>>
    <associate|auto-297|<tuple|compression|?>>
    <associate|auto-298|<tuple|Isothermal|?>>
    <associate|auto-299|<tuple|Process|?>>
    <associate|auto-3|<tuple|System|?>>
    <associate|auto-30|<tuple|Extensive property|?>>
    <associate|auto-300|<tuple|isothermal|?>>
    <associate|auto-301|<tuple|Isobaric Process|?>>
    <associate|auto-302|<tuple|Process|?>>
    <associate|auto-303|<tuple|isobaric|?>>
    <associate|auto-304|<tuple|Isopiestic|?>>
    <associate|auto-305|<tuple|Process|?>>
    <associate|auto-306|<tuple|isopiestic|?>>
    <associate|auto-307|<tuple|Isochoric process|?>>
    <associate|auto-308|<tuple|Process|?>>
    <associate|auto-309|<tuple|isochoric|?>>
    <associate|auto-31|<tuple|Property|?>>
    <associate|auto-310|<tuple|2.5.1|?>>
    <associate|auto-311|<tuple|Adiabatic|?>>
    <associate|auto-312|<tuple|Process|?>>
    <associate|auto-313|<tuple|adiabatic|?>>
    <associate|auto-314|<tuple|State function|?>>
    <associate|auto-315|<tuple|State function|?>>
    <associate|auto-316|<tuple|Exact differential|?>>
    <associate|auto-317|<tuple|exact differential|?>>
    <associate|auto-318|<tuple|Cyclic process|?>>
    <associate|auto-319|<tuple|Process|?>>
    <associate|auto-32|<tuple|extensive property|?>>
    <associate|auto-320|<tuple|cyclic process|?>>
    <associate|auto-321|<tuple|Path function|?>>
    <associate|auto-322|<tuple|path functions|?>>
    <associate|auto-323|<tuple|Inexact differential|?>>
    <associate|auto-324|<tuple|Differential|?>>
    <associate|auto-325|<tuple|inexact differentials|?>>
    <associate|auto-326|<tuple|2.6|?>>
    <associate|auto-327|<tuple|<tuple|energy|system>|?>>
    <associate|auto-328|<tuple|2.6.1|?>>
    <associate|auto-329|<tuple|Energy|?>>
    <associate|auto-33|<tuple|Intensive property|?>>
    <associate|auto-330|<tuple|Reference frame|?>>
    <associate|auto-331|<tuple|Frame|?>>
    <associate|auto-332|<tuple|Inertial reference frame|?>>
    <associate|auto-333|<tuple|Frame|?>>
    <associate|auto-334|<tuple|Lab frame|?>>
    <associate|auto-335|<tuple|Frame|?>>
    <associate|auto-336|<tuple|2.6.2|?>>
    <associate|auto-337|<tuple|<tuple|internal energy>|?>>
    <associate|auto-338|<tuple|<tuple|energy|internal>|?>>
    <associate|auto-339|<tuple|Internal energy|?>>
    <associate|auto-34|<tuple|Property|?>>
    <associate|auto-340|<tuple|Energy|?>>
    <associate|auto-341|<tuple|internal energy|?>>
    <associate|auto-342|<tuple|Local frame|?>>
    <associate|auto-343|<tuple|Frame|?>>
    <associate|auto-344|<tuple|Gravitational|?>>
    <associate|auto-345|<tuple|Field|?>>
    <associate|auto-346|<tuple|Center-of-mass frame|?>>
    <associate|auto-347|<tuple|Frame|?>>
    <associate|auto-348|<tuple|Einstein energy relation|?>>
    <associate|auto-349|<tuple|<tuple|internal energy>|?>>
    <associate|auto-35|<tuple|intensive property|?>>
    <associate|auto-350|<tuple|<tuple|energy|internal>|?>>
    <associate|auto-351|<tuple|<tuple|energy|system>|?>>
    <associate|auto-36|<tuple|Density|?>>
    <associate|auto-37|<tuple|Concentration|?>>
    <associate|auto-38|<tuple|Specific|?>>
    <associate|auto-39|<tuple|Quantity|?>>
    <associate|auto-4|<tuple|system|?>>
    <associate|auto-40|<tuple|specific quantity|?>>
    <associate|auto-41|<tuple|Specific|?>>
    <associate|auto-42|<tuple|Volume|?>>
    <associate|auto-43|<tuple|Molar|?>>
    <associate|auto-44|<tuple|Quantity|?>>
    <associate|auto-45|<tuple|molar quantity|?>>
    <associate|auto-46|<tuple|Volume|?>>
    <associate|auto-47|<tuple|2.2|?>>
    <associate|auto-48|<tuple|Phase|?>>
    <associate|auto-49|<tuple|phase|?>>
    <associate|auto-5|<tuple|Surroundings|?>>
    <associate|auto-50|<tuple|Homogeneous phase|?>>
    <associate|auto-51|<tuple|Phase|?>>
    <associate|auto-52|<tuple|Interface surface|?>>
    <associate|auto-53|<tuple|interface surface|?>>
    <associate|auto-54|<tuple|Isotropic phase|?>>
    <associate|auto-55|<tuple|Phase|?>>
    <associate|auto-56|<tuple|Anisotropic phase|?>>
    <associate|auto-57|<tuple|Phase|?>>
    <associate|auto-58|<tuple|2.2.1|?>>
    <associate|auto-59|<tuple|Physical state|?>>
    <associate|auto-6|<tuple|surroundings|?>>
    <associate|auto-60|<tuple|State|?>>
    <associate|auto-61|<tuple|<tuple|State|aggregation>|?>>
    <associate|auto-62|<tuple|Shear stress|?>>
    <associate|auto-63|<tuple|shear stress|?>>
    <associate|auto-64|<tuple|2.2.1|?>>
    <associate|auto-65|<tuple|solid|?>>
    <associate|auto-66|<tuple|solid|?>>
    <associate|auto-67|<tuple|Deformation|?>>
    <associate|auto-68|<tuple|Fluid|?>>
    <associate|auto-69|<tuple|fluid|?>>
    <associate|auto-7|<tuple|Boundary|?>>
    <associate|auto-70|<tuple|Viscoelastic solid|?>>
    <associate|auto-71|<tuple|Solid|?>>
    <associate|auto-72|<tuple|2.2.2|?>>
    <associate|auto-73|<tuple|Phase|?>>
    <associate|auto-74|<tuple|coexist|?>>
    <associate|auto-75|<tuple|Phase|?>>
    <associate|auto-76|<tuple|phase transition|?>>
    <associate|auto-77|<tuple|Phase|?>>
    <associate|auto-78|<tuple|2.2.3|?>>
    <associate|auto-79|<tuple|fluid|?>>
    <associate|auto-8|<tuple|boundary|?>>
    <associate|auto-80|<tuple|Supercritical fluid|?>>
    <associate|auto-81|<tuple|Fluid|?>>
    <associate|auto-82|<tuple|Plasma|?>>
    <associate|auto-83|<tuple|Liquid|?>>
    <associate|auto-84|<tuple|liquid|?>>
    <associate|auto-85|<tuple|Gas|?>>
    <associate|auto-86|<tuple|gas|?>>
    <associate|auto-87|<tuple|Coexistence curve|?>>
    <associate|auto-88|<tuple|<tuple|critical|point|pure substance>|?>>
    <associate|auto-89|<tuple|critical point|?>>
    <associate|auto-9|<tuple|Subsystem|?>>
    <associate|auto-90|<tuple|Supercritical fluid|?>>
    <associate|auto-91|<tuple|Fluid|?>>
    <associate|auto-92|<tuple|supercritical fluid|?>>
    <associate|auto-93|<tuple|2.2.2|?>>
    <associate|auto-94|<tuple|Vapor|?>>
    <associate|auto-95|<tuple|vapor|?>>
    <associate|auto-96|<tuple|Continuity of states|?>>
    <associate|auto-97|<tuple|Barotropic effect|?>>
    <associate|auto-98|<tuple|fluid|?>>
    <associate|auto-99|<tuple|2.2.4|?>>
    <associate|c2|<tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Chapter
    2>|?>>
    <associate|c2 eq density|<tuple|2.3.4|?>>
    <associate|c2 eq int dq=q, int dw=w|<tuple|2.5.3|?>>
    <associate|c2 eq molar-mass|<tuple|2.3.2|?>>
    <associate|c2 eq molar-volume|<tuple|2.1.2|?>>
    <associate|c2 eq virial-coeff-b|<tuple|2.2.6|?>>
    <associate|c2 eq virial-coeff-c|<tuple|2.2.7|?>>
    <associate|c2 eq virial-eos-simple|<tuple|2.2.8|?>>
    <associate|c2 eq virial-p|<tuple|2.2.3|?>>
    <associate|c2 eq virial-vm|<tuple|2.2.2|?>>
    <associate|c2 eq virial-vm-subp|<tuple|2.2.5|?>>
    <associate|c2 fig density|<tuple|2.3.1|?>>
    <associate|c2 fig phases|<tuple|2.2.2|?>>
    <associate|c2 fig shear|<tuple|2.2.1|?>>
    <associate|c2 fig-co2|<tuple|2.2.3|?>>
    <associate|c2 fig-gas-thermometer|<tuple|2.3.3|?>>
    <associate|c2 fig-paths|<tuple|2.5.1|?>>
    <associate|c2 fig-steady-state|<tuple|2.4.1|?>>
    <associate|c2 fig-tp-cell|<tuple|2.3.2|?>>
    <associate|c2 molar-quantity-subscript|<tuple|molar quantity|?>>
    <associate|c2 note-gas-thermometry|<tuple|Gas constant|?>>
    <associate|c2 note-kibble-balance|<tuple|<tuple|mass>|?>>
    <associate|c2 sec eots|<tuple|2.6|?>>
    <associate|c2 sec eots-frames|<tuple|2.6.1|?>>
    <associate|c2 sec eots-internal|<tuple|2.6.2|?>>
    <associate|c2 sec pap|<tuple|2.5|?>>
    <associate|c2 sec prop|<tuple|2.3|?>>
    <associate|c2 sec prop-amount|<tuple|2.3.2|?>>
    <associate|c2 sec prop-density|<tuple|2.3.4|?>>
    <associate|c2 sec prop-mass|<tuple|2.3.1|?>>
    <associate|c2 sec prop-pressure|<tuple|2.3.5|?>>
    <associate|c2 sec prop-temperature|<tuple|2.3.6|?>>
    <associate|c2 sec prop-temperature-equilibrium|<tuple|2.3.6.1|?>>
    <associate|c2 sec prop-temperature-scales|<tuple|2.3.6.2|?>>
    <associate|c2 sec prop-temperature-thermometers|<tuple|2.3.6.4|?>>
    <associate|c2 sec prop-temperature-thermometry|<tuple|2.3.6.3|?>>
    <associate|c2 sec prop-volume|<tuple|2.3.3|?>>
    <associate|c2 sec sots|<tuple|2.4|?>>
    <associate|c2 sec sots-equilibrium|<tuple|2.4.4|?>>
    <associate|c2 sec sots-functions|<tuple|2.4.1|?>>
    <associate|c2 sec sots-functions-example|<tuple|2.4.2|?>>
    <associate|c2 sec sots-steady|<tuple|2.4.5|?>>
    <associate|c2 sec ssb|<tuple|2.1|?>>
    <associate|c2 sec ssb-extintprop|<tuple|2.1.1|?>>
    <associate|c2 sec states|<tuple|2.2|?>>
    <associate|c2 sec states-eos-fluids|<tuple|2.2.4|?>>
    <associate|c2 sec states-eos-gases|<tuple|2.2.5|?>>
    <associate|c2 sec states-fluids|<tuple|2.2.3|?>>
    <associate|c2 sec states-physical|<tuple|2.2.1|?>>
    <associate|c2 sec states-solids|<tuple|2.2.6|?>>
    <associate|c2 sec states-transitions|<tuple|2.2.2|?>>
    <associate|c2 sec stos-indvars|<tuple|2.4.3|?>>
    <associate|c2 tab measurement-methods|<tuple|2.3.1|?>>
    <associate|c2 tab symbols|<tuple|2.1.1|?>>
    <associate|c2 tab-its-90|<tuple|2.3.2|?>>
    <associate|c2 tab-state|<tuple|2.4.1|?>>
    <associate|exact differential|<tuple|exact differential|?>>
    <associate|fig:2-CO2|<tuple|2.2.3|?>>
    <associate|fig:2-density|<tuple|2.3.1|?>>
    <associate|fig:2-gas thermometer|<tuple|2.3.3|?>>
    <associate|fig:2-paths|<tuple|2.5.1|?>>
    <associate|fig:2-steady state|<tuple|2.4.1|?>>
    <associate|fig:2-tp cell|<tuple|2.3.2|?>>
    <associate|footnote-2.1.1|<tuple|2.1.1|?>>
    <associate|footnote-2.1.2|<tuple|2.1.2|?>>
    <associate|footnote-2.2.1|<tuple|2.2.1|?>>
    <associate|footnote-2.3.1|<tuple|2.3.1|?>>
    <associate|footnote-2.3.10|<tuple|2.3.10|?>>
    <associate|footnote-2.3.11|<tuple|2.3.11|?>>
    <associate|footnote-2.3.12|<tuple|2.3.12|?>>
    <associate|footnote-2.3.13|<tuple|2.3.13|?>>
    <associate|footnote-2.3.2|<tuple|2.3.2|?>>
    <associate|footnote-2.3.3|<tuple|2.3.3|?>>
    <associate|footnote-2.3.4|<tuple|2.3.4|?>>
    <associate|footnote-2.3.5|<tuple|2.3.5|?>>
    <associate|footnote-2.3.6|<tuple|2.3.6|?>>
    <associate|footnote-2.3.7|<tuple|2.3.7|?>>
    <associate|footnote-2.3.8|<tuple|2.3.8|?>>
    <associate|footnote-2.3.9|<tuple|2.3.9|?>>
    <associate|footnote-2.4.1|<tuple|2.4.1|?>>
    <associate|footnote-2.4.2|<tuple|2.4.2|?>>
    <associate|footnote-2.4.3|<tuple|2.4.3|?>>
    <associate|footnote-2.5.1|<tuple|2.5.1|?>>
    <associate|footnote-2.5.2|<tuple|2.5.2|?>>
    <associate|footnr-2.1.1|<tuple|2.1.1|?>>
    <associate|footnr-2.1.2|<tuple|Frame|?>>
    <associate|footnr-2.2.1|<tuple|2.2.1|?>>
    <associate|footnr-2.3.1|<tuple|2.3.1|?>>
    <associate|footnr-2.3.10|<tuple|2.3.10|?>>
    <associate|footnr-2.3.11|<tuple|2.3.11|?>>
    <associate|footnr-2.3.12|<tuple|2.3.12|?>>
    <associate|footnr-2.3.13|<tuple|2.3.13|?>>
    <associate|footnr-2.3.2|<tuple|2.3.2|?>>
    <associate|footnr-2.3.3|<tuple|2.3.3|?>>
    <associate|footnr-2.3.4|<tuple|b|?>>
    <associate|footnr-2.3.5|<tuple|2.3.5|?>>
    <associate|footnr-2.3.6|<tuple|2.3.6|?>>
    <associate|footnr-2.3.7|<tuple|2.3.7|?>>
    <associate|footnr-2.3.8|<tuple|2.3.8|?>>
    <associate|footnr-2.3.9|<tuple|2.3.9|?>>
    <associate|footnr-2.4.1|<tuple|2.4.1|?>>
    <associate|footnr-2.4.2|<tuple|2.4.2|?>>
    <associate|footnr-2.4.3|<tuple|2.4.3|?>>
    <associate|footnr-2.5.1|<tuple|2.5.1|?>>
    <associate|footnr-2.5.2|<tuple|2.5.2|?>>
    <associate|identical temperature scales|<tuple|<tuple|triple|point|H2O>|?>>
    <associate|int dq=q, intdw=w|<tuple|2.5.3|?>>
    <associate|kinds of internal equilibria|<tuple|System|?>>
    <associate|neg p|<tuple|2.3.5|?>>
    <associate|oint(dX)=0|<tuple|2.5.2|?>>
    <associate|pVm=RT(1+B/Vm...)|<tuple|2.2.2|?>>
    <associate|pVm=RT(1+B_p RT(1/Vm...)...)|<tuple|2.2.5|?>>
    <associate|pVm=RT(1+B_p p...)|<tuple|2.2.3|?>>
    <associate|steady state|<tuple|2.4.5|?>>
    <associate|t/oC=T/K-273.15|<tuple|2.3.6|?>>
    <associate|tbl:2-ITS-90|<tuple|2.3.2|?>>
    <associate|tbl:2-methods|<tuple|2.3.1|?>>
    <associate|tbl:2-state|<tuple|2.4.1|?>>
    <associate|very slow changes|<tuple|3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      guggen-85

      chao-15

      stock-19

      haddad-16

      greer-74

      stock-19

      mcglashan-90

      preston-90

      rusby-02

      fellmuth-16

      moldover-14

      underwood-17

      fischer-16

      fellmuth-16
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.2.1>|>
        Experimental procedure for producing shear stress in a phase
        (shaded). Blocks at the upper and lower surfaces of the phase are
        pushed in opposite directions, dragging the adjacent portions of the
        phase with them.
      </surround>|<pageref|auto-64>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.2.2>|>
        Pressure\Utemperature phase diagram of a pure substance (schematic).
        Point cp is the critical point, and point tp is the triple point.
        Each area is labeled with the physical state that is stable under the
        pressure-temperature conditions that fall within the area. A solid
        curve (coexistence curve) separating two areas is the locus of
        pressure-temperature conditions that allow the phases of these areas
        to coexist at equilibrium. Path ABCD illustrates
        <with|font-shape|<quote|italic>|continuity of states>.
      </surround>|<pageref|auto-93>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.2.3>|>
        \;

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Compression
                factor of <with|mode|<quote|math>|<with|mode|<quote|text>|CO><rsub|2>>
                as a function of pressure at three temperatures. At
                <with|mode|<quote|math>|700 <with|mode|<quote|text>|K>>, the
                Boyle temperature, the initial slope is zero.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Second
                virial coefficient of <with|mode|<quote|math>|<with|mode|<quote|text>|CO><rsub|2>>
                as a function of temperature.
              </surround>
            </with>
          </surround>
        </with>

        \;
      </surround>|<pageref|auto-111>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.3.1>|>
        Three methods for measuring liquid density by comparison with samples
        of known density. The liquid is indicated by gray shading.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Glass
                pycnometer vessel with capillary stopper. The filled
                pycnometer is brought to the desired temperature in a
                thermostat bath, dried, and weighed.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Magnetic
                float densimeter.<space|0spc><assign|footnote-nr|4><hidden-binding|<tuple>|2.3.4><assign|fnote-+2BTH32sy1b4NrHu5|<quote|2.3.4>><assign|fnlab-+2BTH32sy1b4NrHu5|<quote|2.3.4>><rsup|<with|font-shape|<quote|right>|<reference|footnote-2.3.4>>>
                Buoy <with|mode|<quote|math>|B>, containing a magnet, is
                pulled down and kept in position with solenoid
                <with|mode|<quote|math>|S> by means of position detector
                <with|mode|<quote|math>|D> and servo control system
                <with|mode|<quote|math>|C>. The solenoid current required
                depends on the liquid density.

                <assign|item-nr|3><hidden-binding|<tuple>|c><assign|last-item-nr|3><assign|last-item|c><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|c<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>Vibrating-tube
                densimeter. The ends of a liquid-filled metal
                <with|font-family|<quote|ss>|U>-tube are clamped to a
                stationary block. An oscillating magnetic field at the tip of
                the tube is used to make it vibrate in the direction
                perpendicular to the page. The measured resonance frequency
                is a function of the mass of the liquid in the tube.
              </surround>
            </with>
          </surround>
        </with>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%-CFF93718--D0DD86F0>|<link|hyperlink|<id|%-CFF93718--D0DD86F0>|<url|#footnr-2.3.4>>|2.3.4>.
        |<hidden-binding|<tuple|footnote-2.3.4>|2.3.4>|Ref.
        [<write|bib|greer-74><reference|bib-greer-74>]>>>
      </surround>|<pageref|auto-148>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.3.2>|>
        Cross-section of a water triple-point cell. The cell has cylindrical
        symmetry about a vertical axis. Pure water of the same isotopic
        composition as <with|mode|<quote|math>|<with|mode|<quote|text>|H><rsub|2><with|mode|<quote|text>|O>>
        in ocean water is distilled into the cell. The air is pumped out and
        the cell is sealed. A freezing mixture is placed in the inner well to
        cause a thin layer of ice to form next to the inner wall. The
        freezing mixture is removed, and some of the ice is allowed to melt
        to a film of very pure water between the ice and inner wall. The
        thermometer bulb is placed in the inner well as shown, together with
        ice water (not shown) for good thermal contact.
      </surround>|<pageref|auto-175>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.3.3>|>
        Simple version of a constant-volume gas thermometer. The leveling
        bulb is raised or lowered to place the left-hand meniscus at the
        level indicator. The gas pressure is then determined from
        <with|mode|<quote|math>|\<Delta\>*h> and the density of the mercury:
        <with|mode|<quote|math>|p=p<rsub|<with|mode|<quote|text>|atm>>+\<rho\>\<cdot\>g\<cdot\>\<Delta\>*h>.
      </surround>|<pageref|auto-204>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.4.1>|>
        Steady state in a metal rod (shaded) with heat conduction. The boxes
        at the ends represent heat reservoirs of constant temperature.
      </surround>|<pageref|auto-284>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.5.1>|>
        Paths of three processes of a closed ideal-gas system with
        <with|mode|<quote|math>|p> and <with|mode|<quote|math>|V> as the
        independent variables. (a) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Isothermal
        expansion. (b) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Isobaric
        expansion. (c) <no-break><specific|screen|<resize|<move|<with|color|<quote|#A0A0FF>|->|-0.3em|>|0em||0em|>>Isochoric
        pressure reduction.
      </surround>|<pageref|auto-310>>
    </associate>
    <\associate|gly>
      <tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Chapter
      2>>

      <tuple|normal|system|<pageref|auto-4>>

      <tuple|normal|surroundings|<pageref|auto-6>>

      <tuple|normal|boundary|<pageref|auto-8>>

      <tuple|normal|open|<pageref|auto-12>>

      <tuple|normal|closed|<pageref|auto-14>>

      <tuple|normal|diathermal|<pageref|auto-17>>

      <tuple|normal|adiabatic|<pageref|auto-20>>

      <tuple|normal|isolated|<pageref|auto-23>>

      <tuple|normal|body|<pageref|auto-27>>

      <tuple|normal|extensive property|<pageref|auto-32>>

      <tuple|normal|intensive property|<pageref|auto-35>>

      <tuple|normal|specific quantity|<pageref|auto-40>>

      <tuple|normal|molar quantity|<pageref|auto-45>>

      <tuple|normal|phase|<pageref|auto-49>>

      <tuple|normal|interface surface|<pageref|auto-53>>

      <tuple|normal|shear stress|<pageref|auto-63>>

      <tuple|normal|solid|<pageref|auto-66>>

      <tuple|normal|fluid|<pageref|auto-69>>

      <tuple|normal|coexist|<pageref|auto-74>>

      <tuple|normal|phase transition|<pageref|auto-76>>

      <tuple|normal|liquid|<pageref|auto-84>>

      <tuple|normal|gas|<pageref|auto-86>>

      <tuple|normal|critical point|<pageref|auto-89>>

      <tuple|normal|supercritical fluid|<pageref|auto-92>>

      <tuple|normal|vapor|<pageref|auto-95>>

      <tuple|normal|equation of state|<pageref|auto-101>>

      <tuple|normal|Boyle temperature|<pageref|auto-114>>

      <tuple|normal|liter|<pageref|auto-142>>

      <tuple|normal|pascal|<pageref|auto-155>>

      <tuple|normal|bar|unit of pressure|<pageref|auto-157>>

      <tuple|normal|standard pressure|<pageref|auto-160>>

      <tuple|normal|kelvin|<pageref|auto-184>>

      <tuple|normal|state|<pageref|auto-235>>

      <tuple|normal|state functions|<pageref|auto-239>>

      <tuple|normal|independent variables|<pageref|auto-242>>

      <tuple|normal|dependent variables|<pageref|auto-245>>

      <tuple|normal|equilibrium state|<pageref|auto-259>>

      <tuple|normal|steady state|<pageref|auto-279>>

      <tuple|normal|heat reservoir|<pageref|auto-281>>

      <tuple|normal|thermal reservoir|<pageref|auto-283>>

      <tuple|normal|process|<pageref|auto-289>>

      <tuple|normal|path|<pageref|auto-291>>

      <tuple|normal|expansion|<pageref|auto-294>>

      <tuple|normal|compression|<pageref|auto-297>>

      <tuple|normal|isothermal|<pageref|auto-300>>

      <tuple|normal|isobaric|<pageref|auto-303>>

      <tuple|normal|isopiestic|<pageref|auto-306>>

      <tuple|normal|isochoric|<pageref|auto-309>>

      <tuple|normal|adiabatic|<pageref|auto-313>>

      <tuple|normal|exact differential|<pageref|auto-317>>

      <tuple|normal|cyclic process|<pageref|auto-320>>

      <tuple|normal|path functions|<pageref|auto-322>>

      <tuple|normal|inexact differentials|<pageref|auto-325>>

      <tuple|normal|internal energy|<pageref|auto-341>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|System>|<pageref|auto-3>>

      <tuple|<tuple|Surroundings>|<pageref|auto-5>>

      <tuple|<tuple|Boundary>|<pageref|auto-7>>

      <tuple|<tuple|Subsystem>|<pageref|auto-9>>

      <tuple|<tuple|Supersystem>|<pageref|auto-10>>

      <tuple|<tuple|System|open>|<pageref|auto-11>>

      <tuple|<tuple|System|closed>|<pageref|auto-13>>

      <tuple|<tuple|Diathermal boundary>|<pageref|auto-15>>

      <tuple|<tuple|Boundary|diathermal>|<pageref|auto-16>>

      <tuple|<tuple|Adiabatic|boundary>|<pageref|auto-18>>

      <tuple|<tuple|Boundary|adiabatic>|<pageref|auto-19>>

      <tuple|<tuple|Isolated system>|<pageref|auto-21>>

      <tuple|<tuple|System|isolated>|<pageref|auto-22>>

      <tuple|<tuple|Reference frame>|<pageref|auto-24>>

      <tuple|<tuple|Frame|reference>|<pageref|auto-25>>

      <tuple|<tuple|Body>|<pageref|auto-26>>

      <tuple|<tuple|Extensive property>|<pageref|auto-30>>

      <tuple|<tuple|Property|extensive>|<pageref|auto-31>>

      <tuple|<tuple|Intensive property>|<pageref|auto-33>>

      <tuple|<tuple|Property|intensive>|<pageref|auto-34>>

      <tuple|<tuple|Density>|<pageref|auto-36>>

      <tuple|<tuple|Concentration>|<pageref|auto-37>>

      <tuple|<tuple|Specific|quantity>|<pageref|auto-38>>

      <tuple|<tuple|Quantity|specific>|<pageref|auto-39>>

      <tuple|<tuple|Specific|volume>|<pageref|auto-41>>

      <tuple|<tuple|Volume|specific>|<pageref|auto-42>>

      <tuple|<tuple|Molar|quantity>|<pageref|auto-43>>

      <tuple|<tuple|Quantity|molar>|<pageref|auto-44>>

      <tuple|<tuple|Volume|molar>|<pageref|auto-46>>

      <tuple|<tuple|Phase>|<pageref|auto-48>>

      <tuple|<tuple|Homogeneous phase>|<pageref|auto-50>>

      <tuple|<tuple|Phase|homogeneous>|<pageref|auto-51>>

      <tuple|<tuple|Interface surface>|<pageref|auto-52>>

      <tuple|<tuple|Isotropic phase>|<pageref|auto-54>>

      <tuple|<tuple|Phase|isotropic>|<pageref|auto-55>>

      <tuple|<tuple|Anisotropic phase>|<pageref|auto-56>>

      <tuple|<tuple|Phase|anisotropic>|<pageref|auto-57>>

      <tuple|<tuple|Physical state>|<pageref|auto-59>>

      <tuple|<tuple|State|physical>|<pageref|auto-60>>

      <tuple|<tuple|State|aggregation>|||<tuple|State|of
      aggregation>|<pageref|auto-61>>

      <tuple|<tuple|Shear stress>|<pageref|auto-62>>

      <tuple|<tuple|solid>|<pageref|auto-65>>

      <tuple|<tuple|Deformation>|<pageref|auto-67>>

      <tuple|<tuple|Fluid>|<pageref|auto-68>>

      <tuple|<tuple|Viscoelastic solid>|<pageref|auto-70>>

      <tuple|<tuple|Solid|viscoelastic>|<pageref|auto-71>>

      <tuple|<tuple|Phase|coexistence>|<pageref|auto-73>>

      <tuple|<tuple|Phase|transition>|<pageref|auto-75>>

      <tuple|<tuple|Phase|transition|equilibrium>|<pageref|auto-77>>

      <tuple|fluid||c2 fluids|<tuple|Fluids>|<pageref|auto-79>>

      <tuple|<tuple|Supercritical fluid>|<pageref|auto-80>>

      <tuple|<tuple|Fluid|supercritical>|<pageref|auto-81>>

      <tuple|<tuple|Plasma>|<pageref|auto-82>>

      <tuple|<tuple|Liquid>|<pageref|auto-83>>

      <tuple|<tuple|Gas>|<pageref|auto-85>>

      <tuple|<tuple|Coexistence curve|liquid\Ugas>|<pageref|auto-87>>

      <tuple|<tuple|critical|point|pure substance>|||<tuple|Critical|point|of
      a pure substance>|<pageref|auto-88>>

      <tuple|<tuple|Supercritical fluid>|<pageref|auto-90>>

      <tuple|<tuple|Fluid|supercritical>|<pageref|auto-91>>

      <tuple|<tuple|Vapor>|<pageref|auto-94>>

      <tuple|<tuple|Continuity of states>|<pageref|auto-96>>

      <tuple|<tuple|Barotropic effect>|<pageref|auto-97>>

      <tuple|fluid||c2 fluids|<tuple|Fluids>|<pageref|auto-98>>

      <tuple|<tuple|Equation of state|fluid>|||<tuple|of a
      fluid>|<pageref|auto-100>>

      <tuple|<tuple|Ideal gas|equation>|<pageref|auto-102>>

      <tuple|<tuple|Equation of state|ideal gas>|||<tuple|of an ideal
      gas>|<pageref|auto-103>>

      <tuple|<tuple|Redlich\UKwong equation>|<pageref|auto-104>>

      <tuple|<tuple|Equation of state|virial>|<pageref|auto-106>>

      <tuple|<tuple|virial|equation|pure gas>|||<tuple|Virial|equation|for a
      pure gas>|<pageref|auto-107>>

      <tuple|<tuple|Statistical mechanics|virial
      equations>|<pageref|auto-108>>

      <tuple|<tuple|Virial|coefficient>|<pageref|auto-109>>

      <tuple|<tuple|Statistical mechanics>|<pageref|auto-110>>

      <tuple|<tuple|Boyle temperature>|<pageref|auto-112>>

      <tuple|<tuple|Temperature|Boyle>|<pageref|auto-113>>

      <tuple|<tuple|Statistical mechanics|Boyle
      temperature>|<pageref|auto-115>>

      <tuple|<tuple|Solid>||c2 sec solids|<tuple|Solid>|<pageref|auto-117>>

      <tuple|<tuple|Elastic deformation>|<pageref|auto-118>>

      <tuple|<tuple|Deformation|elastic>|<pageref|auto-119>>

      <tuple|<tuple|Plastic deformation>|<pageref|auto-120>>

      <tuple|<tuple|Deformation|plastic>|<pageref|auto-121>>

      <tuple|<tuple|Solid>||c2 sec solids|<tuple|Solid>|<pageref|auto-122>>

      <tuple|<tuple|mass>||c2 sec prop-mass|<tuple|Mass, measurement
      of>|<pageref|auto-125>>

      <tuple|<tuple|mass>||c2 sec prop-mass|<tuple|Mass, measurement
      of>|<pageref|auto-126>>

      <tuple|<tuple|amount>||c2 sec prop-amount|<tuple|Amount|measurement
      of>|<pageref|auto-128>>

      <tuple|<tuple|Amount>|<pageref|auto-129>>

      <tuple|<tuple|Mole>|<pageref|auto-130>>

      <tuple|<tuple|Relative atomic mass>|<pageref|auto-131>>

      <tuple|<tuple|Atomic mass, relative>|<pageref|auto-132>>

      <tuple|<tuple|Atomic weight>|<pageref|auto-133>>

      <tuple|<tuple|Relative atomic mass>|<pageref|auto-134>>

      <tuple|<tuple|Molecular mass, relative>|<pageref|auto-135>>

      <tuple|<tuple|Molecular weight>|<pageref|auto-136>>

      <tuple|<tuple|amount>||c2 sec prop-amount|<tuple|Amount|measurement
      of>|<pageref|auto-137>>

      <tuple|<tuple|volume>||c2 sec prop-volume|<tuple|Volume|measurement
      of>|<pageref|auto-139>>

      <tuple|<tuple|Liter>|<pageref|auto-141>>

      <tuple|<tuple|Milliliter>|<pageref|auto-143>>

      <tuple|<tuple|volume>||c2 sec prop-volume|<tuple|Volume|measurement
      of>|<pageref|auto-144>>

      <tuple|<tuple|density>||c2 sec prop-density|<tuple|Density>|<pageref|auto-146>>

      <tuple|<tuple|Volume|molar>|<pageref|auto-147>>

      <tuple|<tuple|density>||c2 sec prop-density|<tuple|Density>|<pageref|auto-149>>

      <tuple|<tuple|pressure|measurement of>||c2 sec
      prop-pressure|<tuple|Pressure|measurement of>|<pageref|auto-151>>

      <tuple|<tuple|Isotropic fluid>|<pageref|auto-152>>

      <tuple|<tuple|Fluid|isotropic>|<pageref|auto-153>>

      <tuple|<tuple|Pascal (unit)>|<pageref|auto-154>>

      <tuple|<tuple|Bar>|<pageref|auto-156>>

      <tuple|<tuple|Standard|pressure>|<pageref|auto-158>>

      <tuple|<tuple|Pressure|standard>|<pageref|auto-159>>

      <tuple|<tuple|pressure|measurement of>||c2 sec
      prop-pressure|<tuple|Pressure|measurement of>|<pageref|auto-161>>

      <tuple|<tuple|Temperature>||c2 sec prop-temperature
      idx1|<tuple|Temperature|measurement of>|<pageref|auto-163>>

      <tuple|<tuple|Temperature scale>|<pageref|auto-164>>

      <tuple|<tuple|Thermometer|liquid-in-glass>|<pageref|auto-165>>

      <tuple|<tuple|Zeroth law of thermodynamics>|<pageref|auto-166>>

      <tuple|<tuple|Maxwell, James Clerk>|<pageref|auto-167>>

      <tuple|<tuple|temperature|equilibrium systems for fixed values>||c2
      sec-temperature-equilibrium|<tuple|Temperature|equilibrium systems for
      fixed values>|<pageref|auto-169>>

      <tuple|<tuple|Ice point|for fixed temperature>|<pageref|auto-170>>

      <tuple|<tuple|Steam point>|<pageref|auto-171>>

      <tuple|<tuple|Melting point|for fixed temperature>|<pageref|auto-172>>

      <tuple|<tuple|Triple|point|for fixed temperature>|<pageref|auto-173>>

      <tuple|<tuple|Triple|point|cell>|<pageref|auto-174>>

      <tuple|<tuple|temperature|equilibrium systems for fixed values>||c2
      sec-temperature-equilibrium|<tuple|Temperature|equilibrium systems for
      fixed values>|<pageref|auto-176>>

      <tuple|<tuple|temperature|scales>||c2 sec prop-temperature-scales
      idx1|<tuple|Temperature|scales>|<pageref|auto-178>>

      <tuple|<tuple|Ideal-gas temperature>|<pageref|auto-179>>

      <tuple|<tuple|Temperature|ideal-gas>|||<tuple|Temperature|ideal
      gas>|<pageref|auto-180>>

      <tuple|<tuple|Thermodynamic|temperature>|<pageref|auto-181>>

      <tuple|<tuple|Temperature|thermodynamic>|<pageref|auto-182>>

      <tuple|<tuple|Kelvin (unit)>|<pageref|auto-183>>

      <tuple|<tuple|triple|point|H2O>|||<tuple|Triple|point|of
      H<with|mode|<quote|math>|<rsub|2>>O>|<pageref|auto-185>>

      <tuple|<tuple|SI|2019 revision>|<pageref|auto-186>>

      <tuple|<tuple|Centrigrade scale>|<pageref|auto-187>>

      <tuple|<tuple|Celsius|scale>|<pageref|auto-188>>

      <tuple|<tuple|Celsius|temperature>|<pageref|auto-189>>

      <tuple|<tuple|triple|point|H2O>|||<tuple|Triple|point|of
      H<with|mode|<quote|math>|<rsub|2>>O>|<pageref|auto-190>>

      <tuple|<tuple|Ice point>|<pageref|auto-191>>

      <tuple|<tuple|Steam point>|<pageref|auto-192>>

      <tuple|international temperature scale of 1990||International
      Temperature Scale of 1990, <with|font-shape|<quote|italic>|see> ITS-90>

      <tuple|<tuple|temperature|international scale of 1990>||International
      scale of 1990, <with|font-shape|<quote|italic>|see> ITS-90>

      <tuple|<tuple|ITS-90>|<pageref|auto-193>>

      <tuple|provisional low temperature scale of 2000||Provisional low
      temperature scale of 2000, <with|font-shape|<quote|italic>|see>
      PLST-2000>

      <tuple|<tuple|temperature|scales>||c2 sec prop-temperature-scales
      idx1|<tuple|Temperature|scales>|<pageref|auto-195>>

      <tuple|<tuple|primary thermometry>||c2 sec prop-temperature-thermometry
      idx1|<tuple|Primary thermometry>|<pageref|auto-197>>

      <tuple|<tuple|thermometry|primary>||c2 sec prop-temperature-thermometry
      idx2|<tuple|Thermometry|primary>|<pageref|auto-198>>

      <tuple|<tuple|Boltzmann constant>|<pageref|auto-199>>

      <tuple|<tuple|Gas constant>|<pageref|auto-200>>

      <tuple|<tuple|gas|thermometry>||c2 sec
      prop-temperature-thermometry-gas|<tuple|Gas|thermometry>|<pageref|auto-201>>

      <tuple|<tuple|thermometry|gas>||c2 sec prop-temperature-thermometry-gas
      idx2|<tuple|Thermometry|gas>|<pageref|auto-202>>

      <tuple|<tuple|Thermometer|constant-volume gas>|<pageref|auto-203>>

      <tuple|<tuple|gas|thermometry>||c2 sec
      prop-temperature-thermometry-gas|<tuple|Gas|thermometry>|<pageref|auto-205>>

      <tuple|<tuple|thermometry|gas>||c2 sec prop-temperature-thermometry-gas
      idx2|<tuple|Thermometry|gas>|<pageref|auto-206>>

      <tuple|<tuple|Acoustic gas thermometry>|<pageref|auto-207>>

      <tuple|<tuple|Thermometry|acoustic gas>|<pageref|auto-208>>

      <tuple|<tuple|Dielectric constant gas thermometry>|<pageref|auto-209>>

      <tuple|<tuple|Thermometry|dielectric constant gas>|<pageref|auto-210>>

      <tuple|<tuple|Johnson noise thermometry>|<pageref|auto-211>>

      <tuple|<tuple|Thermometry|Johnson noise>|<pageref|auto-212>>

      <tuple|<tuple|Doppler broadening thermometry>|<pageref|auto-213>>

      <tuple|<tuple|Thermometry|Doppler broadening>|<pageref|auto-214>>

      <tuple|<tuple|primary thermometry>||c2 sec prop-temperature-thermometry
      idx1|<tuple|Primary thermometry>|<pageref|auto-215>>

      <tuple|<tuple|thermometry|primary>||c2 sec prop-temperature-thermometry
      idx2|<tuple|Thermometry|primary>|<pageref|auto-216>>

      <tuple|<tuple|Thermometer|liquid-in-glass>|<pageref|auto-218>>

      <tuple|<tuple|Thermometer|Beckmann>|<pageref|auto-219>>

      <tuple|<tuple|Thermometer|resistance>|<pageref|auto-220>>

      <tuple|<tuple|Thermometer|platinum resistance>|<pageref|auto-221>>

      <tuple|<tuple|Thermometer|thermistor>|<pageref|auto-222>>

      <tuple|<tuple|Thermometer|thermocouple>|<pageref|auto-223>>

      <tuple|<tuple|Thermometer|thermopile>|<pageref|auto-224>>

      <tuple|<tuple|Thermometer|quartz crystal>|<pageref|auto-225>>

      <tuple|<tuple|Thermometer|optical pyrometer>|<pageref|auto-226>>

      <tuple|<tuple|PLTS-2000>|<pageref|auto-227>>

      <tuple|<tuple|ITS-90>|<pageref|auto-228>>

      <tuple|<tuple|Temperature>||c2 sec prop-temperature
      idx1|<tuple|Temperature|measurement of>|<pageref|auto-229>>

      <tuple|<tuple|state|system>||c2 sec sots idx1|<tuple|State|of a
      system>|<pageref|auto-231>>

      <tuple|<tuple|system|state of>||c2 sec sots idx2|<tuple|System|state
      of>|<pageref|auto-232>>

      <tuple|<tuple|state|system>|||<tuple|State|of a
      system>|<pageref|auto-233>>

      <tuple|<tuple|system|state>|||<tuple|System|state
      of>|<pageref|auto-234>>

      <tuple|<tuple|state function>||c2 sec sots-functions|<tuple|State
      function>|<pageref|auto-237>>

      <tuple|<tuple|State function>|<pageref|auto-238>>

      <tuple|<tuple|Independent variables>|<pageref|auto-240>>

      <tuple|<tuple|Variables|independent>|<pageref|auto-241>>

      <tuple|<tuple|Dependent variable>|<pageref|auto-243>>

      <tuple|<tuple|Variables|dependent>|<pageref|auto-244>>

      <tuple|<tuple|Equation of state>|<pageref|auto-247>>

      <tuple|<tuple|Variables|number of independent>|<pageref|auto-249>>

      <tuple|<tuple|state function>||c2 sec sots-functions|<tuple|State
      function>|<pageref|auto-250>>

      <tuple|<tuple|Components, number of>|<pageref|auto-252>>

      <tuple|<tuple|Equation of state>|<pageref|auto-253>>

      <tuple|<tuple|equilibrium state>||c2 sec sots-equilibrium
      idx1|<tuple|Equilibrium state>|<pageref|auto-255>>

      <tuple|<tuple|state|equilibrium>||c2 sec sots-equilibrium
      idx2|<tuple|State|equilibrium>|<pageref|auto-256>>

      <tuple|<tuple|Equilibrium state>|<pageref|auto-257>>

      <tuple|<tuple|State|equilibrium>|<pageref|auto-258>>

      <tuple|<tuple|Isolated system>|<pageref|auto-260>>

      <tuple|<tuple|System|isolated>|<pageref|auto-261>>

      <tuple|<tuple|Equilibrium|thermal>|<pageref|auto-262>>

      <tuple|<tuple|Equilibrium|mechanical>|<pageref|auto-263>>

      <tuple|<tuple|Equilibrium|transfer>|<pageref|auto-264>>

      <tuple|<tuple|Equilibrium|reaction>|<pageref|auto-265>>

      <tuple|<tuple|External field>|<pageref|auto-266>>

      <tuple|<tuple|Field|external>|<pageref|auto-267>>

      <tuple|<tuple|Membrane, semipermeable>|<pageref|auto-268>>

      <tuple|<tuple|galvanic cell|equilibrium state>|||<tuple|Galvanic
      cell|in an equilibrium state>|<pageref|auto-269>>

      <tuple|<tuple|Gravitational|field>|<pageref|auto-270>>

      <tuple|<tuple|Field|gravitational>|<pageref|auto-271>>

      <tuple|<tuple|Metastable state>|<pageref|auto-272>>

      <tuple|<tuple|State|metastable>|<pageref|auto-273>>

      <tuple|<tuple|equilibrium state>||c2 sec sots-equilibrium
      idx1|<tuple|Equilibrium state>|<pageref|auto-274>>

      <tuple|<tuple|state|equilibrium>||c2 sec sots-equilibrium
      idx2|<tuple|State|equilibrium>|<pageref|auto-275>>

      <tuple|<tuple|Steady state>|<pageref|auto-277>>

      <tuple|<tuple|State|steady>|<pageref|auto-278>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-280>>

      <tuple|<tuple|Thermal|reservoir>|<pageref|auto-282>>

      <tuple|<tuple|state|system>||c2 sec sots idx1|<tuple|State|of a
      system>|<pageref|auto-285>>

      <tuple|<tuple|system|state of>||c2 sec sots idx2|<tuple|System|state
      of>|<pageref|auto-286>>

      <tuple|<tuple|Process>|<pageref|auto-288>>

      <tuple|<tuple|Path>|<pageref|auto-290>>

      <tuple|<tuple|Expansion>|<pageref|auto-292>>

      <tuple|<tuple|Process|expansion>|<pageref|auto-293>>

      <tuple|<tuple|Compression>|<pageref|auto-295>>

      <tuple|<tuple|Process|compression>|<pageref|auto-296>>

      <tuple|<tuple|Isothermal|process>|<pageref|auto-298>>

      <tuple|<tuple|Process|isothermal>|<pageref|auto-299>>

      <tuple|<tuple|Isobaric Process>|<pageref|auto-301>>

      <tuple|<tuple|Process|isobaric>|<pageref|auto-302>>

      <tuple|<tuple|Isopiestic|process>|<pageref|auto-304>>

      <tuple|<tuple|Process|isopiestic>|<pageref|auto-305>>

      <tuple|<tuple|Isochoric process>|<pageref|auto-307>>

      <tuple|<tuple|Process|isochoric>|<pageref|auto-308>>

      <tuple|<tuple|Adiabatic|process>|<pageref|auto-311>>

      <tuple|<tuple|Process|adiabatic>|<pageref|auto-312>>

      <tuple|<tuple|State function|change of>|<pageref|auto-314>>

      <tuple|<tuple|State function|change of>|<pageref|auto-315>>

      <tuple|<tuple|Exact differential>|<pageref|auto-316>>

      <tuple|<tuple|Cyclic process>|<pageref|auto-318>>

      <tuple|<tuple|Process|cyclic>|<pageref|auto-319>>

      <tuple|<tuple|Path function>|<pageref|auto-321>>

      <tuple|<tuple|Inexact differential>|<pageref|auto-323>>

      <tuple|<tuple|Differential|inexact>|<pageref|auto-324>>

      <tuple|<tuple|energy|system>||c2 sec eots idx1|<tuple|Energy|of the
      system>|<pageref|auto-327>>

      <tuple|<tuple|Energy>|<pageref|auto-329>>

      <tuple|<tuple|Reference frame>|<pageref|auto-330>>

      <tuple|<tuple|Frame|reference>|<pageref|auto-331>>

      <tuple|<tuple|Inertial reference frame>|<pageref|auto-332>>

      <tuple|<tuple|Frame|reference|inertial>|<pageref|auto-333>>

      <tuple|<tuple|Lab frame>|<pageref|auto-334>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-335>>

      <tuple|<tuple|internal energy>||c2 sec eots-internal
      idx1|<tuple|Internal energy>|<pageref|auto-337>>

      <tuple|<tuple|energy|internal>||c2 sec eots-internal
      idx2|<tuple|Energy|internal>|<pageref|auto-338>>

      <tuple|<tuple|Internal energy>|<pageref|auto-339>>

      <tuple|<tuple|Energy|internal>|<pageref|auto-340>>

      <tuple|<tuple|Local frame>|<pageref|auto-342>>

      <tuple|<tuple|Frame|local>|<pageref|auto-343>>

      <tuple|<tuple|Gravitational|field>|<pageref|auto-344>>

      <tuple|<tuple|Field|gravitational>|<pageref|auto-345>>

      <tuple|<tuple|Center-of-mass frame>|<pageref|auto-346>>

      <tuple|<tuple|Frame|center-of-mass>|<pageref|auto-347>>

      <tuple|<tuple|Einstein energy relation>|<pageref|auto-348>>

      <tuple|<tuple|internal energy>||c2 sec eots-internal
      idx1|<tuple|Internal energy>|<pageref|auto-349>>

      <tuple|<tuple|energy|internal>||c2 sec eots-internal
      idx2|<tuple|Energy|internal>|<pageref|auto-350>>

      <tuple|<tuple|energy|system>||c2 sec eots idx1|<tuple|Energy|of the
      system>|<pageref|auto-351>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.1.1>|>
        Symbols and SI units for some common properties
      </surround>|<pageref|auto-29>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.3.1>|>
        Representative measurement methods
      </surround>|<pageref|auto-140>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.3.2>|>
        Fixed temperatures of the International Temperature Scale of 1990
      </surround>|<pageref|auto-194>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2.4.1>|>
        Values of state functions of an aqueous sucrose solution (A = water,
        B = sucrose)
      </surround>|<pageref|auto-248>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Systems
      and Their Properties> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      2.1<space|2spc>The System, Surroundings, and Boundary
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      <with|par-left|<quote|1tab>|2.1.1<space|2spc>Extensive and intensive
      properties <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-28>>

      2.2<space|2spc>Phases and Physical States of Matter
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-47>

      <with|par-left|<quote|1tab>|2.2.1<space|2spc>Physical states of matter
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-58>>

      <with|par-left|<quote|1tab>|2.2.2<space|2spc>Phase coexistence and
      phase transitions <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-72>>

      <with|par-left|<quote|1tab>|2.2.3<space|2spc>Fluids
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-78>>

      <with|par-left|<quote|1tab>|2.2.4<space|2spc>The equation of state of a
      fluid <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-99>>

      <with|par-left|<quote|1tab>|2.2.5<space|2spc>Virial equations of state
      for pure gases <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-105>>

      <with|par-left|<quote|1tab>|2.2.6<space|2spc>Solids
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-116>>

      2.3<space|2spc>Some Basic Properties and Their Measurement
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-123>

      <with|par-left|<quote|1tab>|2.3.1<space|2spc>Mass
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-124>>

      <with|par-left|<quote|1tab>|2.3.2<space|2spc>Amount of substance
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-127>>

      <with|par-left|<quote|1tab>|2.3.3<space|2spc>Volume
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-138>>

      <with|par-left|<quote|1tab>|2.3.4<space|2spc>Density
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-145>>

      <with|par-left|<quote|1tab>|2.3.5<space|2spc>Pressure
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-150>>

      <with|par-left|<quote|1tab>|2.3.6<space|2spc>Temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-162>>

      <with|par-left|<quote|2tab>|2.3.6.1<space|2spc>Equilibrium systems for
      fixed temperatures <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-168>>

      <with|par-left|<quote|2tab>|2.3.6.2<space|2spc>Temperature scales
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-177>>

      <with|par-left|<quote|2tab>|2.3.6.3<space|2spc>Primary thermometry
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-196>>

      <with|par-left|<quote|2tab>|2.3.6.4<space|2spc>Practical thermometers
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-217>>

      2.4<space|2spc>The State of the System
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-230>

      <with|par-left|<quote|1tab>|2.4.1<space|2spc>State functions and
      independent variables <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-236>>

      <with|par-left|<quote|1tab>|2.4.2<space|2spc>An example: state
      functions of a mixture <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-246>>

      <with|par-left|<quote|1tab>|2.4.3<space|2spc>More about independent
      variables <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-251>>

      <with|par-left|<quote|1tab>|2.4.4<space|2spc>Equilibrium states
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-254>>

      <with|par-left|<quote|1tab>|2.4.5<space|2spc>Steady states
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-276>>

      2.5<space|2spc>Processes and Paths <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-287>

      2.6<space|2spc>The Energy of the System
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-326>

      <with|par-left|<quote|1tab>|2.6.1<space|2spc>Energy and reference
      frames <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-328>>

      <with|par-left|<quote|1tab>|2.6.2<space|2spc>Internal energy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-336>>
    </associate>
  </collection>
</auxiliary>