<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <section*|Front Matter>

  The first edition of this book was previously published by Pearson
  Education, Inc. It was copyright \<copyright\>2001 by Prentice-Hall Inc.

  The second edition, version 10 was copyright \<copyright\>2020 by Howard
  DeVoe. The second edition was licensed under a Creative Commons Attribution
  4.0 International License (CC BY 4.0):

  <\padded-center>
    \ <hlink|<verbatim|https://creativecommons.org/licenses/by/4.0/>|https://creativecommons.org/licenses/by/4.0/>
  </padded-center>

  This retypeset book is copyright \<copyright\>2022 by Steven Baltakatei
  Sandoval. This book is a derivative of the second edition, version 10 by
  Howard DeVoe, used under a Creative Commons Attribution 4.0 International
  License. This book is licensed under Creative Commons
  Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0):

  <\padded-center>
    \ <hlink|<verbatim|https://creativecommons.org/licenses/by/4.0/>|https://creativecommons.org/licenses/by/4.0/>
  </padded-center>

  The book is available as source code in a
  <hlink|<verbatim|git>|https://git-scm.com/> repository available at:

  <\padded-center>
    \ <hlink|<verbatim|https://gitlab.com/baltakatei/bk-2021-07-1>|https://gitlab.com/baltakatei/bk-2021-07-1>
  </padded-center>

  This book was typeset using <strong|<TeXmacs>> version
  <verbatim|<TeXmacs-version>>.

  Most of the figures were produced with <strong|PSTricks>. Fonts used
  include <strong|<TeX> Gyre Termes>, Adobe Times, MathTime, and Computer
  Modern Typwriter.

  This book was rendered on <verbatim|<date|%Y-%m-%dT%H:%M:%S%z>>.
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|auto-1|<tuple|?|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      Front Matter <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>