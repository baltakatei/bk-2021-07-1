<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <no-indent>BIOGRAPHICAL SKETCH

  <paragraph|<person|Benoit Paul �mile Clapeyron> (1799\U1864)>

  <\padded-center>
    <image|BIO/clapeyron.png|92pt|115pt||>
  </padded-center>

  <label|bio:clapeyron><index|Clapeyron, �mile><no-indent>Clapeyron was a
  French civil and railroad engineer who made important contributions to
  thermodynamic theory. He was born in Paris, the son of a merchant.

  He graduated from the �cole Polytechnique in 1818, four years after Sadi
  Carnot's graduation from the same school, and then trained as an engineer
  at the �cole de Mines. At this time, the Russian czar asked the French
  government for engineers to help create a program of civil and military
  engineering. Clapeyron and his classmate Gabriel Lam� were offered this
  assignment. They lived in Russia for ten years, teaching pure and applied
  mathematics in Saint Petersburg and jointly publishing engineering and
  mathematical papers. In 1831 they returned to France; their liberal
  political views and the oppression of foreigners by the new czar, Nicholas
  I, made it impossible for them to remain in Russia.

  Back in France, Clapeyron became involved in the construction of the first
  French passenger railroads and in the design of steam locomotives and metal
  bridges. He was married with a daughter.

  In a paper published in 1834 in the journal of the �cole Polytechnique,
  Clapeyron brought attention to the work of Sadi Carnot (page
  <pageref|bio:carnot>), who had died two years before:<footnote|Ref.
  <cite|clapeyron-1834>.>

  <quotation|Among studies which have appeared on the theory of heat I will
  mention finally a work by S. Carnot, published in 1824, with the title
  <em|Reflections on the Motive Power of Fire>. The idea which serves as a
  basis of his researches seems to me to be both fertile and beyond question;
  his demonstrations are founded on <em|the absurdity of the possibility of
  creating motive power <text|[i.e., work]> or heat out of nothing>. ...This
  new method of demonstration seems to me worthy of the attention of
  theoreticians; it seems to me to be free of all objection ...>

  Clapeyron's paper used indicator diagrams and calculus for a rigorous proof
  of Carnot's conclusion that the efficiency of a reversible heat engine
  depends only on the temperatures of the hot and cold heat reservoirs.
  However, it retained the erroneous caloric theory of heat. It was not until
  the appearance of English and German translations of this paper that
  Clapeyron's analysis enabled Kelvin to define a thermodynamic temperature
  scale and Clausius to introduce entropy and write the mathematical
  statement of the second law.

  Clapeyron's 1834 paper also derived an expression for the temperature
  dependence of the vapor pressure of a liquid equivalent to what is now
  called the Clapeyron equation (Eq. <reference|dp/dT=del(trs)Hm/T*del(trs)Vm>).
  The paper used a reversible cycle with vaporization at one temperature
  followed by condensation at an infinitesimally-lower temperature and
  pressure, and equated the efficiency of this cycle to that of a gas between
  the same two temperatures. Although the thermodynamic temperature <math|T>
  does not appear as such, it is represented by a temperature-dependent
  function whose relation to the Celsius scale had to be determined
  experimentally.<footnote|Ref. <cite|wisniak-2000>.>

  Beginning in 1844, Clapeyron taught the course on steam engines at the
  �cole Nationale des Ponts et Chauss�es near Paris, the oldest French
  engineering school. In this course, surprisingly, he seldom mentioned his
  theory of heat engines based on Carnot's work.<footnote|Ref.
  <cite|kerker-60>.> He eventually embraced the equivalence of heat and work
  established by Joule's experiments.<footnote|Ref. <cite|hirsch-1895>.>

  At the time of Clapeyron's death, the railroad entrepreneur �mile P�reire
  wrote:<footnote|Ref. <cite|hirsch-1895>.>

  <quotation|We were together since 1832. I've never done important business
  without consulting him, I've never found a judgment more reliable and more
  honest. His modesty was so great and his character so good that I never
  knew him to have an enemy.>
</body>

<\initial>
  <\collection>
    <associate|font-base-size|9>
    <associate|page-medium|paper>
    <associate|par-columns|2>
    <associate|par-columns-sep|1fn>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|Clapeyron, �mile|1>>
    <associate|bio:clapeyron|<tuple|1|1>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|1>>
    <associate|footnote-3|<tuple|3|1>>
    <associate|footnote-4|<tuple|4|1>>
    <associate|footnote-5|<tuple|5|1>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|1>>
    <associate|footnr-3|<tuple|3|1>>
    <associate|footnr-4|<tuple|4|1>>
    <associate|footnr-5|<tuple|5|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      clapeyron-1834

      wisniak-2000

      kerker-60

      hirsch-1895

      hirsch-1895
    </associate>
    <\associate|idx>
      <tuple|<tuple|Clapeyron, �mile>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|Benoit
      Paul �mile Clapeyron> (1799\U1864) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>