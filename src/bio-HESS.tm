<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;
  </hide-preamble>

  <no-indent>BIOGRAPHICAL SKETCH

  <paragraph|<person|Germain Henri Hess> (1802\U1850)>

  <\padded-center>
    <image|BIO/hess.png|104pt|115pt||>
  </padded-center>

  <label|bio:hess><index|Hess, Germain><no-indent>Hess was a Russian chemist
  and physician whose calorimetric measurements led him to formulate the law
  of constant heat summation, now known as Hess's law. His given name had
  several versions: \PGermain Henri\Q in French, as shown above; \PHermann
  Heinrich\Q in German; and \PGerman Iwanowitsch\Q in Russian.

  He was born in Geneva, Switzerland, the son of an artist. The family moved
  to Russia when he was three years old; his father had found work there as a
  tutor on an estate. Hess studied medicine at the University of Tartu in
  Estonia (then part of the Russian empire) and received his doctor of
  medicine degree in 1825. In addition to his medical studies, Hess took
  courses in chemistry and geology and wrote a doctoral dissertation on the
  composition of mineral waters in Russia.<footnote|Refs. <cite|kohler-2011>
  and <cite|leicester-51>.>

  Hess was more interested in chemistry than medicine. He briefly studied
  with the famous Swedish chemist J�ns Jakob Berzelius in Stockholm, and they
  became life-long friends. Hess then practiced medicine in Irkutsk, Siberia,
  while continuing his interests in mineral chemistry and chemical analysis.

  In 1829, after being being elected an adjunct member of the St. Petersburg
  Academy of Sciences, he gave up his medical practice and began teaching
  chemistry at various institutions of higher learning in St. Petersburg. He
  wrote a two-volume textbook, <em|Fundamentals of Pure Chemistry>, in 1831,
  followed by a one-volume abridgment in 1834 that became the standard
  Russian chemistry textbook and went through seven editions. He became a
  full member of the St. Petersburg Academy Academy in 1834.

  Hess published the results of his thermochemical research between 1839 and
  1842. His 1840 paper<footnote|Ref. <cite|hess-1840>.> describes his
  measurements of the heat evolved when pure sulfuric acid,
  H<rsub|<math|2>>SO<rsub|<math|4>>, is mixed with various amounts of water,
  and another series of measurements of the heat evolved when the acid in
  H<rsub|<math|2>>SO<rsub|<math|4>>-water mixtures is neutralized with
  aqueous ammonia. The following table from this paper is of historical
  interest, although it is a bit difficult to decipher:

  <\quotation>
    <\padded-center>
      <tabular|<tformat|<cwith|1|1|2|2|cell-row-span|1>|<cwith|1|1|2|2|cell-col-span|2>|<cwith|6|6|4|4|cell-bborder|1ln>|<cwith|7|7|4|4|cell-tborder|1ln>|<cwith|3|6|1|1|cell-halign|l>|<cwith|3|6|1|1|cell-valign|c>|<cwith|3|7|2|-1|cell-halign|C.>|<cwith|7|7|3|3|cell-halign|r>|<table|<row|<cell|Acid>|<cell|Heat
      evolved by>|<cell|>|<cell|Sum>>|<row|<cell|>|<cell|ammonia>|<cell|water>|<cell|>>|<row|<cell|<math|<wide|<tx|<sout|H>>|\<dot\>><space|0.17em><wide|<tx|S>|\<dddot\>>>>|<cell|595.8>|<cell|>|<cell|595.8>>|<row|<cell|<math|<wide|<tx|<sout|H>>|\<dot\>><rsup|2><space|0.17em><wide|<tx|S>|\<dddot\>>>>|<cell|518.9>|<cell|77.8>|<cell|596.7>>|<row|<cell|<math|<wide|<tx|<sout|H>>|\<dot\>><rsup|3><space|0.17em><wide|<tx|S>|\<dddot\>>>>|<cell|480.5>|<cell|116.7>|<cell|597.2>>|<row|<cell|<math|<wide|<tx|<sout|H>>|\<dot\>><rsup|6><space|0.17em><wide|<tx|S>|\<dddot\>>>>|<cell|446.2>|<cell|155.6>|<cell|601.8>>|<row|<cell|>|<cell|>|<cell|<math|Average>>|<cell|597.9>>>>>
    </padded-center>
  </quotation>

  The first column gives the relative amounts of acid and water in the
  notation of Berzelius: <math|<wide|<tx|<sout|H>>|\<dot\>>> is
  H<rsub|<math|2>>O, <math|<wide|<tx|S>|\<dddot\>>> is SO<rsub|<math|3>>, and
  <math|<wide|<tx|<sout|H>>|\<dot\>><space|0.17em><wide|<tx|S>|\<dddot\>>> is
  H<rsub|<math|2>>SO<rsub|<math|4>>. Thus
  <math|<wide|<tx|<sout|H>>|\<dot\>><rsup|3><space|0.17em><wide|<tx|S>|\<dddot\>>>,
  for example, is <math|<text|H<rsub|2>SO<rsub|4>>+2*<text|H<rsub|2>O>>. The
  second and third columns show the heat evolved (<math|-q>) in units of
  calories per gram of the SO<rsub|<math|3>> moiety of the
  H<rsub|<math|2>>SO<rsub|<math|4>>. The near-equality of the sums in the
  last column for the overall reaction <math|<text|H<rsub|2>SO<rsub|4>>
  <around*|(|<text|l>|)>+2*<text|NH<rsub|3>>
  <around*|(|<text|aq>|)><arrow><around*|(|<text|NH<rsub|4>>|)><rsub|2>*<text|SO<rsub|4>>
  <around*|(|<text|aq>|)>> demonstrates Hess's law of constant heat
  summation, which he stated in the words:<footnote|Ref. <cite|hess-1840>;
  translation in Ref. <cite|davis-51>.>

  <quotation|The amount of heat evolved during the formation of a given
  compound is constant, independent of whether the compound is formed
  directly or indirectly in a series of steps.>

  Hess confirmed this law with similar experiments using other acids such as
  HCl(aq) and other bases such as NaOH(aq) and CaO(s).

  After 1848 Hess's health began to deteriorate. He was only 48 when he died.
  His role as the real founder of thermochemistry was largely forgotten until
  the influential German physical chemist Wilhelm Ostwald drew attention to
  it about forty years after Hess's death.
</body>

<\initial>
  <\collection>
    <associate|font-base-size|9>
    <associate|page-medium|paper>
    <associate|par-columns|2>
    <associate|par-columns-sep|1fn>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|Hess, Germain|1>>
    <associate|bio:cat|<tuple|1|1>>
    <associate|bio:hess|<tuple|1|?>>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnote-2|<tuple|2|1>>
    <associate|footnote-3|<tuple|3|1>>
    <associate|footnr-1|<tuple|1|1>>
    <associate|footnr-2|<tuple|2|1>>
    <associate|footnr-3|<tuple|3|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      kohler-2011

      leicester-51

      hess-1840

      hess-1840

      davis-51
    </associate>
    <\associate|idx>
      <tuple|<tuple|Willard, F. D. C.>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <with|par-left|<quote|4tab>|<with|font-shape|<quote|small-caps>|F. D.
      C. Willard> (1968\U1982) <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.15fn>>
    </associate>
  </collection>
</auxiliary>