<TeXmacs|2.1.1>

<style|source>

<\body>
  <\active*>
    <\src-title>
      <src-package|devoe-text-style|0.0.3>

      <src-purpose|Style package for Thermodynamics and Chemistry TeXmacs
      source files.>
    </src-title>
  </active*>

  <use-package|number-long-article|termes-font>

  <\active*>
    <\src-comment>
      Style parameters.
    </src-comment>
  </active*>

  <assign|info-flag|detailed>

  <assign|page-height|auto>

  <assign|page-width|auto>

  <assign|page-type|letter>

  <\active*>
    <\src-comment>
      Macro definitions.
    </src-comment>
  </active*>

  <assign|A|<macro|<rsub|<text|A>>>>

  <assign|allni|<macro|<math|<around*|{|n<rsub|i>|}>>>>

  <assign|aph|<macro|<rsup|<text|\<#3B1\>>>>>

  <assign|aphp|<macro|<rsup|<math|\<alpha\><rprime|'>>>>>

  <assign|arrow|<macro|\<rightarrow\>>>

  <assign|arrows|<macro|\<rightleftharpoons\>>>

  <assign|As|<macro|<math|A<rsub|<text|s>>>>>

  \;

  <assign|B|<macro|<rsub|<text|B>>>>

  <assign|bd|<macro|<rsub|<text|b>>>>

  <assign|bio-insert|<\macro|body>
    <page-break*><hrule>

    <arg|body>

    <hrule>
  </macro>>

  <assign|bktable3|<macro|body|<tformat|<twith|table-min-rows|2>|<twith|table-min-cols|2>|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|-1|-1|1|-1|cell-bborder|2ln>|<cwith|1|-1|1|-1|cell-bsep|0.25fn>|<cwith|1|-1|1|-1|cell-tsep|0.25fn>|<cwith|1|-1|1|-1|cell-lsep|0.50fn>|<cwith|1|-1|1|-1|cell-rsep|0.50fn>|<arg|body>>>>

  <assign|bk-equal-def|<macro|<above|<text|<space|0.5em>>=<text|<space|0.5em>>|<text|def>>>>

  <assign|bpd|<macro|numer|denom|const|<math|<around*|[|\<partial\>*<arg|numer>/\<partial\>*<arg|denom>|]><rsub|<arg|const>>>>>

  <assign|bPd|<macro|numer|denom|constant|<around*|[|<dfrac|\<partial\>*<arg|numer>|\<partial\>*<arg|denom>>|]><rsub|<arg|constant>>>>

  <assign|bph|<macro|<rsup|<text|\<#3B2\>>>>>

  <assign|br|<macro|<math| <text|bar>>>>

  \;

  <assign|C|<macro|<rsub|<text|C>>>>

  <assign|cbB|<macro|<rsub|c,<text|B>>>>

  <assign|chem|<macro|formula|charge|<text|<math|<arg|formula>>><rsup|><math|<rsup|<arg|charge>>>>>

  <assign|chemspec|<macro|formula|state|<math|<text|<arg|formula>>
  <around*|(|<text|<arg|state>>|)>>>>

  <assign|cm|<macro|<rsub|<text|cm>>>>

  <assign|Cpm|<macro|C<rsub|p,<text|m>>>>

  <assign|CVm|<macro|C<rsub|V,<text|m>>>>

  \;

  <assign|dbar|<macro|var|<math|<text|�>*<arg|var>>>>

  <assign|defn|<macro|<bk-equal-def> >>

  <assign|degC|<macro|<rsup|\<circ\>><text|C>>>

  <assign|Del|<macro|\<Delta\>*>>

  <assign|Delsub|<macro|body|\<Delta\><rsub|<text|<arg|body>>>*>>

  <assign|description-aligned|<\macro|body>
    <compound|<if|<and|<value|<merge|prefix-|description-aligned>>|<unequal|<value|last-item-nr>|0>>|list*|list>|<macro|name|<aligned-item|<item-strong|<arg|name><item-spc>>>>|<macro|name|<with|mode|math|<with|font-series|bold|math-font-series|bold|<rigid|\<ast\>>>>>|<arg|body>>
  </macro>>

  <assign|dif|<\macro>
    <text|d>*
  </macro>>

  <assign|difp|<macro|<text|d>*p>>

  <assign|dil|<macro|<around*|(|<text|dil>|)>>>

  <assign|df|<macro|<dvar|f>>>

  <assign|dotprod|<macro|\<bullet\>>>

  <assign|dq|<macro|<dbar|q>>>

  <assign|dQ|<macro|<dbar|Q>>>

  <assign|dt|<macro|<dvar|t>>>

  <assign|dvar|<macro|var|<math|<text|d>*<arg|var>>>>

  <assign|dw|<macro|<dbar|w>>>

  <assign|dx|<macro|<dvar|x>>>

  \;

  <assign|E|<macro|<rsup|<text|<with|font-family|ss|E>>>>>

  <assign|Eeq|<macro|<math|<inactive*|E<rsub|<text|cell>,<text|eq>>>>>>

  <assign|Ej|<macro|E<rsub|<text|j>>>>

  <assign|el|<macro|<math|<inactive*|<rsub|<text|el>>>>>>

  <assign|eq|<macro|<rsub|<text|eq>>>>

  <assign|equation-cov2|<\macro|body|cov>
    <surround|<next-equation>||<equation-lab-cov2|<arg|body>|<the-equation>|<arg|cov>>>
  </macro>>

  <assign|equation-lab-cov2|<\macro|body|lab|cov>
    <\surround|<set-binding|<arg|lab>>|<space|5mm><tabular|<tformat|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|(<arg|lab>)>>|<row|<cell|<arg|cov>>>>>>>
      <\equation*>
        <arg|body>
      </equation*>
    </surround>
  </macro>>

  <assign|expt|<macro|<inactive|<around|(|<text|expt>|)>>>>

  \;

  <assign|f|<macro|<rsub|<text|f>>>>

  <assign|fA|<macro|<rsub|<text|f>,<text|A>>>>

  <assign|fB|<macro|<rsub|<text|f>,<text|B>>>>

  <assign|fug|<macro|f>>

  \;

  <assign|g|<macro|\<gamma\>>>

  <assign|G|<macro|\<#1D6E4\>>>

  <assign|gas|<macro|<math| <around*|(|<text|g>|)>>>>

  <assign|gph|<macro|<rsup|\<gamma\>>>>

  \;

  <assign|ir|<macro|<dilate|.6|.6|<tabular|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lsep|0ln>|<cwith|1|-1|1|1|cell-rsep|0ln>|<cwith|1|-1|1|1|cell-bsep|0ln>|<cwith|1|-1|1|1|cell-tsep|0ln>|<table|<row|<cell|irrev>>|<row|<cell|rev>>>>>>>>

  \;

  <assign|jn|<macro|<text| \| >>>

  \;

  <assign|K|<macro|<math|<inactive*| <text|K>>>>>

  <assign|kHB|<macro|<math|k<rsub|<text|H>,<text|B>>>>>

  <assign|kHi|<macro|k<rsub|<text|H>,i>>>

  <assign|kT|<macro|<math|\<kappa\><rsub|T>>>>

  \;

  <assign|lab|<macro|<rsub|<text|lab>>>>

  <assign|liquid|<macro|<around*|(|<text|l>|)>>>

  <assign|ljn|<macro| \<#00a6\> >>

  <assign|lljn|<macro| \<#00a6\>\<#00a6\> >>

  \;

  <assign|m|<macro|<rsub|<text|m>>>>

  <assign|mA|<macro|<rsub|<text|m>,<text|A>>>>

  <assign|mB|<macro|<rsub|<text|m>,<text|B>>>>

  <assign|mbB|<macro|<rsub|m,<text|B>>>>

  <assign|mix|<macro|<around*|(|<text|mix>|)>>>

  <assign|mol|<macro|<text|mol>>>

  <assign|mue|<macro|\<mu\><rsub|<text|e>>>>

  \;

  <assign|newterm|<macro|term|<strong|<arg|term>><glossary|<arg|term>>>>

  <assign|newterm-explain|<macro|term|explanation|<strong|<arg|term>><glossary-explain|<arg|term>|<arg|explanation>>>>

  \;

  <assign|onehalf|<macro|<tfrac|1|2>*>>

  \;

  <assign|P|<macro|<math|<slanted|\<Pi\>>>>>

  <assign|pd|<macro|num|den|const|<around*|(|\<partial\>*<arg|num>/\<partial\>*<arg|den>|)><rsub|<arg|const>>>>

  <assign|Pd|<macro|num|den|const|<math|<around*|(|<dfrac|\<partial\>*<arg|num>|\<partial\>*<arg|den>>|)><rsub|<arg|const>>>>>

  <assign|per|<macro|<rsup|-1>>>

  <assign|pha|<macro|<text|\<#3B1\>>>>

  <assign|phb|<macro|<text|\<#3B2\>>>>

  \;

  <assign|ra|<macro|<math|\<rightarrow\>>>>

  <assign|rf|<macro|<rsup|<text|ref>>>>

  <assign|Rsix|<macro|8.31447 <text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>>

  <assign|rsupout|<macro|body|<rsup|><rsup|<arg|body>>>>

  <assign|rxn|<macro|<around*|(|<text|rxn>|)>>>

  \;

  <assign|sln|<macro|<inactive|<around|(|<text|sln>|)>>>>

  <assign|sol|<macro|<around*|(|<text|sol>|)>>>

  <assign|solid|<macro|<around*|(|<text|s>|)>>>

  <assign|solmB|<macro|<around*|(|<text|sol>,m<rsub|<text|B>>|)>>>

  <assign|sout|<macro|body|<strike-through|<arg|body>>>>

  <assign|st|<macro|<rsup|\<circ\>>>>

  <assign|subs|<macro|body|<rsub|<text|<arg|body>>>>>

  <assign|sups|<macro|body|<rsup|<text|<arg|body>>>>>

  <assign|sur|<macro|<rsup|<text|sur>>>>

  <assign|sys|<macro|<rsub|<text|sys>>>>

  \;

  <assign|timesten|<macro|var|\<times\>10<rsup|<arg|var>>>>

  <assign|tx|<macro|body|<text|<arg|body>>>>

  \;

  <assign|upOmega|<macro|\<Omega\>>>

  \;

  <assign|V|<macro|<text|V>>>

  <assign|varPi|<macro|\<#1D6F1\>>>

  <assign|varPhi|<macro|\<#1D6F7\>>>

  <assign|vpageref|<macro|name|<reference|<arg|name>> on page
  <pageref|<arg|name>>>>

  \;

  <assign|xbB|<macro|<rsub|x,<text|B>>>>

  <assign|xbC|<macro|<rsub|<text|x>,<text|C>>>>

  \;
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>