<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|generic|style-bk>>

<\body>
  <new-page*><section|Problems>

  <\problem>
    The state of a galvanic cell without liquid junction, when its
    temperature and pressure are uniform, can be fully described by values of
    the variables <math|T>, <math|p>, and <math|\<xi\>>. Find an expression
    for <math|<dif>G> during a reversible advancement of the cell reaction,
    and use it to derive the relation <math|\<Delta\><rsub|<text|r>>*G=-z*F<Eeq>>
    (Eq. <reference|del(r)G(cell)=-zFE>). (Hint: Eq.
    <reference|dw(el)=E(cell)dQ(cell)>.)\ 
  </problem>

  <\problem>
    Before 1982 the <subindex|Standard|pressure><subindex|Pressure|standard>standard
    pressure was usually taken as <math|1 <text|atm>> For the cell shown in
    Fig. <reference|fig:14-galvanic cell>, what correction is needed, for a
    value of <math|<Eeq><st>> obtained at <math|25 <degC>> and using the
    older convention, to change the value to one corresponding to a standard
    pressure of <math|1<br>>? Equation <reference|del(r)Go=-zFEo> can be used
    for this calculation.
  </problem>

  <\problem>
    <label|prb:14-1/2H2+AgCl-\<gtr\>>Careful measurements<footnote|Ref.
    <cite|bates-54>.> of the equilibrium cell potential of the cell

    <\equation*>
      <text|Pt><jn><text|H<rsub|2>><around|(|<text|g>|)><jn><text|HCl>
      <around|(|<text|aq>|)><jn><text|AgCl>
      <around|(|<text|s>|)><jn><text|Ag>
    </equation*>

    yielded, at <math|298.15<K>> and using a standard pressure of
    <math|1<br>>, the values <math|<Eeq><st>=0.22217 <V>> and
    <math|<dif><Eeq><st>/<dif>T=-6.462<timesten|-4>
    <text|V>\<cdot\><text|K<per>>>. (The requested calculated values are
    close to, but not exactly the same as, the values listed in Appendix
    <reference|app:props>, which are based on the same data combined with
    data of other workers.)

    <\enumerate-alpha>
      <item><label|1/2H2+AgCl-\<gtr\> a> Evaluate
      <math|\<Delta\><rsub|<text|r>>*G<st>>,
      <math|\<Delta\><rsub|<text|r>>*S<st>>, and
      <math|\<Delta\><rsub|<text|r>>*H<st>> at <math|298.15<K>> for the
      reaction

      <\equation*>
        <with|math-display|false|<frac|1|2>*<text|H<rsub|2>><around|(|<text|g>|)>+<text|AgCl>
        <around|(|<text|s>|)><arrow><text|H><rsup|+>
        <around|(|<text|aq>|)>+<text|Cl><rsup|->
        <around|(|<text|aq>|)>+<text|Ag> <around|(|<text|s>|)>>
      </equation*>

      <item><label|1/2H2+AgCl-\<gtr\> b> Problem <reference|prb:12-Cl ion>
      showed how the standard molar enthalpy of formation of the aqueous
      chloride ion may be evaluated based on the convention
      <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|H><rsup|<math|+>>,<space|0.17em><text|aq>|)>=0>.
      If this value is combined with the value of
      <math|\<Delta\><rsub|<text|r>>*H<st>> obtained in part
      <reference|1/2H2+AgCl-\<gtr\> a> of the present problem, the standard
      molar enthalpy of formation of crystalline silver chloride can be
      evaluated. Carry out this calculation for <math|T=298.15<K>> using the
      value <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|Cl><rsup|<math|->>,<space|0.17em><text|aq>|)>=-167.08
      <text|kJ>\<cdot\><text|mol><per>> (Appendix <reference|app:props>).

      <item><label|1/2H2+AgCl-\<gtr\> c> By a similar procedure, evaluate the
      standard molar entropy, the standard molar entropy of formation, and
      the standard molar Gibbs energy of formation of crystalline silver
      chloride at <math|298.15<K>>. You need the following standard molar
      entropies evaluated from spectroscopic and calorimetric data:

      \;

      <htab|5mm><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|l>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|l>|<cwith|1|-1|3|3|cell-halign|l>|<cwith|1|-1|3|3|cell-rborder|0ln>|<cwith|1|-1|1|-1|cell-valign|c>|<table|<row|<cell|<math|S<m><st><around|(|<text|H<rsub|<math|2>>>,<space|0.17em><text|g>|)>=130.68
      <text|J>*\<cdot\><text|K><per>\<cdot\><text|mol><per>>>|<cell|<space|2em>>|<cell|<math|S<m><st><around|(|<text|Cl<rsub|<math|2>>>,<space|0.17em><text|g>|)>=223.08
      <text|J>\<cdot\><text|K><per>\<cdot\><text|mol><per>>>>|<row|<cell|<math|S<m><st><around|(|<text|Cl><rsup|<math|->>,<space|0.17em><text|aq>|)>=56.60
      <text|J>\<cdot\><text|K><per>\<cdot\><text|mol><per>>>|<cell|>|<cell|<math|S<m><st><around|(|<text|Ag>,<space|0.17em><text|s>|)>=42.55
      <text|J>*\<cdot\><text|K><per>\<cdot\><text|mol><per>>>>>>><htab|5mm>
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:14-AgCl formation>The standard cell potential of the cell

    <\equation*>
      <text|Ag><jn><text|AgCl> <around|(|<text|s>|)><jn><text|HCl>
      <around|(|<text|aq>|)><jn><text|Cl<rsub|2>>
      <around|(|<text|g>|)><jn><text|Pt>
    </equation*>

    has been determined over a range of temperature.<footnote|Ref.
    <cite|faita-67>.> At <math|T=298.15<K>>, the standard cell potential was
    found to be <math|<Eeq><st>=1.13579 <V>>, and its temperature derivative
    was found to be <math|<dif><Eeq><st>/<dif>T=-5.9863<timesten|-4>
    <text|V>*\<cdot\><text|K><per>>.

    <\enumerate-alpha>
      <item>Write the cell reaction for this cell.

      <item>Use the data to evaluate the standard molar enthalpy of formation
      and the standard molar Gibbs energy of formation of crystalline silver
      chloride at <math|298.15<K>>. (Note that this calculation provides
      values of quantities also calculated in Prob.
      14.<reference|prb:14-1/2H2+AgCl-\<gtr\>> using independent data.)
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:14-Ks(AgCl)>Use data in Sec. <reference|14-st molar rxn
    quantities> to evaluate the solubility product of silver chloride at
    <math|298.15<K>>.
  </problem>

  <\problem>
    The equilibrium cell potential of the galvanic cell

    <\equation*>
      <text|Pt><jn><text|H<rsub|<math|2>>>
      <around|(|<text|g>,<space|0.17em><math|<fug|=>=1<br>>|)><jn><text|HCl>
      <around|(|<text|aq>,<space|0.17em><math|0.500
      <text|mol>\<cdot\><text|<space|0.17em>kg><per>>|)><jn><text|Cl<rsub|<math|2>>>*<around|(|<text|g>,<space|0.17em><math|<fug|=>=1<br>>|)><jn><text|Pt>
    </equation*>

    is found to be <math|<Eeq>=1.410 <V>> at <math|298.15<K>>. The standard
    cell potential is <math|<Eeq><st>=1.360 <V>>.

    <\enumerate-alpha>
      <item>Write the cell reaction and calculate its thermodynamic
      equilibrium constant at <math|298.15<K>>.

      <item>Use the cell measurement to calculate the mean ionic activity
      coefficient of aqueous HCl at <math|298.15<K>> and a molality of
      <math|0.500 <text|mol>\<cdot\><text|kg><per>>.
    </enumerate-alpha>
  </problem>

  <\problem>
    Consider the following galvanic cell, which combines a hydrogen electrode
    and a calomel electrode:

    <\equation*>
      <text|Pt><jn><text|H<rsub|2>> <around|(|<text|g>|)><jn><text|HCl>
      *<around|(|<text|aq>|)><jn><text|Hg<rsub|2>Cl<rsub|2>>
      <around|(|<text|s>|)><jn><text|Hg> <around|(|<text|l>|)><jn><text|Pt>
    </equation*>

    <\enumerate-alpha>
      <item>Write the cell reaction.

      <item>At <math|298.15<K>>, the standard cell potential of this cell is
      <math|<Eeq><st>=0.2680 <V>>. Using the value of
      <math|\<Delta\><rsub|<text|f>>*G<st>> for the aqueous chloride ion in
      Appendix <reference|app:props>, calculate the standard molar Gibbs
      energy of formation of crystalline mercury(I) chloride (calomel) at
      <math|298.15<K>>.

      <item>Calculate the solubility product of mercury(I) chloride at
      <math|298.15<K>>. The dissolution equilibrium is
      <math|<text|Hg<rsub|2>Cl<rsub|2>> <around|(|<text|s>|)><arrows|H*g<rsub|2>><text|Hg<rsub|2><rsup|><rsup|<math|2+>>><around|(|<text|aq>|)>+2*<space|0.17em><text|Cl><rsup|->
      <around|(|<text|aq>|)>>. Take values for the standard molar Gibbs
      energies of formation of the aqueous ions from Appendix
      <reference|app:props>.
    </enumerate-alpha>
  </problem>

  <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|9|9|1|-1|cell-bborder|2ln>|<cwith|2|-1|1|-1|cell-halign|C.>|<table|<row|<cell|<math|m<B>><space|0.17em>/<space|0.17em>(mol<space|0.17em>kg<per>)>|<cell|<math|<Eeq>><space|0.17em>/<space|0.17em>V>>|<row|<cell|0.0004042>|<cell|0.47381>>|<row|<cell|0.0008444>|<cell|0.43636>>|<row|<cell|0.0008680>|<cell|0.43499>>|<row|<cell|0.0013554>|<cell|0.41243>>|<row|<cell|0.001464>|<cell|0.40864>>|<row|<cell|0.001850>|<cell|0.39667>>|<row|<cell|0.002396>|<cell|0.38383>>|<row|<cell|0.003719>|<cell|0.36173>>>>>>
    <label|tbl:14-Ag-AgBr>Equilibrium cell potential as a function of HBr
    molality <math|m<B>>.
  </big-table>

  <\problem>
    <label|prb:14-AgAgBr>Table <vpageref|tbl:14-Ag-AgBr> lists equilibrium
    cell potentials obtained with the following cell at
    <math|298.15<K>>:<footnote|Ref. <cite|keston-35>.>

    <\equation*>
      <text|P*t><jn><text|H<rsub|2>><around|(|<text|g>,<space|0.17em><math|1.01<br>>|)><jn><text|HBr>
      *<around|(|<text|aq>,<space|0.17em><math|m<B>>|)><jn><text|AgBr>
      <around|(|<text|s>|)><jn><text|Ag>
    </equation*>

    Use these data to evaluate the standard electrode potential of the
    silver-silver bromide electrode at this temperature to the nearest
    millivolt. (Since the electrolyte solutions are quite dilute, you may
    ignore the term <math|B*a*<sqrt|m<B>>> in Eq. <reference|E'=E-...>.)
  </problem>

  <\problem>
    The cell diagram of a mercury cell can be written

    <\equation*>
      <text|Zn> <around|(|<text|s>|)><jn><text|ZnO>
      <around|(|<text|s>|)><jn><text|NaOH>
      *<around|(|<text|aq>|)><jn><text|HgO>
      <around|(|<text|s>|)><jn><text|Hg> <around|(|<text|l>|)>
    </equation*>

    <\enumerate-alpha>
      <item>Write the electrode reactions and cell reaction with electron
      number <math|z=2>.

      <item>Use data in Appendix <reference|app:props> to calculate the
      standard molar reaction quantities <math|\<Delta\><rsub|<text|r>>*H<st>>,
      <math|\<Delta\><rsub|<text|r>>*G<st>>, and
      <math|\<Delta\><rsub|<text|r>>*S<st>> for the cell reaction at
      <math|298.15<K>>.

      <item>Calculate the standard cell potential of the mercury cell at
      <math|298.15<K>> to the nearest <math|0.01 <V>>.

      <item>Evaluate the ratio of heat to advancement,
      <math|<dq>/<dif>\<xi\>>, at a constant temperature of <math|298.15<K>>
      and a constant pressure of <math|1<br>>, for the cell reaction taking
      place in two different ways: reversibly in the cell, and spontaneously
      in a reaction vessel that is not part of an electrical circuit.

      <item>Evaluate <math|<dif><Eeq><st>/<dif>T>, the temperature
      coefficient of the standard cell potential.
    </enumerate-alpha>
  </problem>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|14>
    <associate|page-first|369>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|6>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|1/2H2+AgCl-\<gtr\> a|<tuple|a|?>>
    <associate|1/2H2+AgCl-\<gtr\> b|<tuple|b|?>>
    <associate|1/2H2+AgCl-\<gtr\> c|<tuple|c|?>>
    <associate|auto-1|<tuple|7|?>>
    <associate|auto-2|<tuple|Standard|?>>
    <associate|auto-3|<tuple|Pressure|?>>
    <associate|auto-4|<tuple|7.1|?>>
    <associate|footnote-7.1|<tuple|7.1|?>>
    <associate|footnote-7.2|<tuple|7.2|?>>
    <associate|footnote-7.3|<tuple|7.3|?>>
    <associate|footnr-7.1|<tuple|7.1|?>>
    <associate|footnr-7.2|<tuple|7.2|?>>
    <associate|footnr-7.3|<tuple|7.3|?>>
    <associate|prb:14-1/2H2+AgCl-\<gtr\>|<tuple|7.3|?>>
    <associate|prb:14-AgAgBr|<tuple|7.8|?>>
    <associate|prb:14-AgCl formation|<tuple|7.4|?>>
    <associate|prb:14-Ks(AgCl)|<tuple|7.5|?>>
    <associate|tbl:14-Ag-AgBr|<tuple|7.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      bates-54

      faita-67

      keston-35
    </associate>
    <\associate|idx>
      <tuple|<tuple|Standard|pressure>|<pageref|auto-2>>

      <tuple|<tuple|Pressure|standard>|<pageref|auto-3>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|7.1>|>
        Equilibrium cell potential as a function of HBr molality
        <with|mode|<quote|math>|m<rsub|<with|mode|<quote|text>|B>>>.
      </surround>|<pageref|auto-4>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|7<space|2spc>Problems>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>