<TeXmacs|2.1.1>

<project|book.tm>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;
  </hide-preamble>

  <new-page*><section|Problems>

  <\problem>
    <label|prb:12-CaCO3>Consider the heterogeneous equilibrium
    <math|<text|CaCO<rsub|3>><around|(|<text|s>|)><arrows><text|CaO><around|(|<text|s>|)>+<text|CO<rsub|2>><around|(|<text|g>|)>>.
    Table <vpageref|tbl:12-CO2 pressure> lists pressures measured over a
    range of temperatures for this system.

    <\enumerate-alpha>
      <item>What is the approximate relation between <math|p> and <math|K>?

      <item><label|CaCO3 eval>Plot these data in the form <math|ln K> versus
      <math|1/T>, or fit <math|ln K> to a linear function of <math|1/T>.
      Then, evaluate the temperature at which the partial pressure of the
      CO<rsub|<math|2>> is <math|1 <text|bar>>, and the standard molar
      reaction enthalpy at this temperature.
    </enumerate-alpha>
  </problem>

  <float|float|thb|<\big-table|<tabular*|<tformat|<cwith|2|-1|1|-1|cell-halign|C.>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-lborder|0ln>|<cwith|1|1|5|5|cell-rborder|0ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|5|5|1|-1|cell-bborder|1ln>|<cwith|2|-1|1|1|cell-lborder|0ln>|<cwith|2|-1|5|5|cell-rborder|0ln>|<table|<row|<cell|<math|t/<degC>>>|<cell|<math|p/<text|Torr>>>|<cell|<space|2em>>|<cell|<math|t/<degC>>>|<cell|<math|p/<text|Torr>>>>|<row|<cell|842.3>|<cell|343.0>|<cell|>|<cell|904.3>|<cell|879.0>>|<row|<cell|852.9>|<cell|398.6>|<cell|>|<cell|906.5>|<cell|875.0>>|<row|<cell|854.5>|<cell|404.1>|<cell|>|<cell|937.0>|<cell|1350>>|<row|<cell|868.9>|<cell|510.9>|<cell|>|<cell|937.0>|<cell|1340>>>>>>
    <label|tbl:12-CO2 pressure>Pressure of an equilibrium system containing
    CaCO<rsub|<math|3>>(s), CaO(s), and CO<rsub|<math|2>>(g).<note-ref|+CSll5WOWbWXt1g>

    \;

    <note-inline||+CSll5WOWbWXt1g>Ref. <cite|symth-23>.
  </big-table>>

  <\problem>
    For a homogeneous reaction in which the reactants and products are
    solutes in a solution, write a rigorous relation between the standard
    molar reaction enthalpy and the temperature dependence of the
    thermodynamic equilibrium constant, with solute standard states based on
    concentration.
  </problem>

  <\problem>
    <label|prb:12-Del(r)S(m)o>Derive an expression for the
    <subsubindex|Entropy|reaction|standard molar>standard molar reaction
    entropy of a reaction that can be used to calculate its value from the
    thermodynamic equilibrium constant and its temperature derivative. Assume
    that no solute standard states are based on concentration.
  </problem>

  <\problem>
    <label|prb:12-Kf Kb>Use the data in Table <vpageref|tbl:12-Kb Kf> to
    evaluate the molal freezing-point depression constant and the molal
    boiling-point elevation constant for H<rsub|<math|2>>O at a pressure of
    <math|1<br>>.
  </problem>

  <\big-table|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|M>>|<cell|<math|t<rsub|<text|f>>>>|<cell|<math|t<bd>>>|<cell|<math|\<Delta\><rsub|<text|fus>>*H>>|<cell|<math|\<Delta\><rsub|<text|vap>>*H>>>|<row|<cell|<math|18.0153
  <text|g>\<cdot\><text|mol><rsup|-1>>>|<cell|<math|0.00
  <degC>>>|<cell|<math|99.61 <degC>>>|<cell|<math|6.010
  <text|kJ>\<cdot\><text|mol><rsup|-1>>>|<cell|<math|40.668
  <text|kJ>\<cdot\><text|mol><rsup|-1>>>>>>>>
    <label|tbl:12-Kb Kf>Properties of H<rsub|<math|2>>O at <math|1<br>>
  </big-table>

  <\problem>
    An aqueous solution of the protein bovine serum albumin, containing
    <math|2.00<timesten|-2> <text|g>> of protein per cubic centimeter, has an
    osmotic pressure of <math|8.1<timesten|-3><br>> at <math|0 <degC>>.
    Estimate the molar mass of this protein.
  </problem>

  <\problem>
    <label|prb:12-BuBenz>Figure <vpageref|fig:12-butylbenzene> shows a curve
    fitted to experimental points for the aqueous solubility of
    <math|n>-butylbenzene. The curve has the equation <math|ln
    x<B>=a*<around|(|t/<degC>-b|)><rsup|2>+c>, where the constants have the
    values <math|a=3.34<timesten|-4>>, <math|b=12.13>, and <math|c=-13.25>.
    Assume that the saturated solution behaves as an ideal-dilute solution,
    use a solute standard state based on mole fraction, and calculate
    <math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>> and
    <math|\<Delta\><rsub|<text|sol>,<text|B>>*S<st>> at <math|5.00 <degC>>,
    <math|12.13 <degC>> (the temperature of minimum solubility), and
    <math|25.00 <degC>>.
  </problem>

  <\problem>
    Consider a hypothetical system in which two aqueous solutions are
    separated by a semipermeable membrane. Solution <math|<pha>> is prepared
    by dissolving <math|1.00<timesten|-5> <mol>> KCl in <math|10.0 <text|g>>
    water. Solution <math|<phb>> is prepared from <math|1.00<timesten|-5>
    <mol>> KCl and <math|1.00<timesten|-6> <mol>> of the potassium salt of a
    polyelectrolyte dissolved in <math|10.0 <text|g>> water. All of solution
    <math|<phb>> is used to fill a dialysis bag, which is then sealed and
    placed in solution <math|<pha>>.

    Each polyelectrolyte ion has a charge of <math|-10>. The membrane of the
    dialysis bag is permeable to the water molecules and to the
    K<rsup|<math|+>> and Cl<rsup|<math|->> ions, but not to the
    polyelectrolyte. The system comes to equilibrium at <math|25.00 <degC>>.
    Assume that the volume of the dialysis bag remains constant. Also make
    the drastic approximation that both solutions behave as ideal-dilute
    solutions.

    <\enumerate-alpha>
      <item>Find the equilibrium molality of each solute species in the two
      solution phases.

      <item>Describe the amounts and directions of any macroscopic transfers
      of ions across the membrane that are required to establish the
      equilibrium state.

      <item>Estimate the Donnan potential, <math|\<phi\><aph>-\<phi\><bph>>.

      <item>Estimate the pressure difference across the membrane at
      equilibrium. (The density of liquid H<rsub|<math|2>>O at <math|25.00
      <degC>> is <math|0.997 <text|g>\<cdot\><text|cm><rsup|-3>>.)
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:12-droplet>The derivation of Prob. <vpageref|prb:9-droplet>
    shows that the pressure in a liquid droplet of radius <math|r> is greater
    than the pressure of the surrounding equilibrated gas phase by a quantity
    <math|2<g>/r>, where <math|<g>> is the surface tension.

    <\enumerate-alpha>
      <item>Consider a droplet of water of radius <math|1.00<timesten|-6>
      <text|m>> at <math|25 <degC>> suspended in air of the same temperature.
      The surface tension of water at this temperature is <math|0.07199
      <text|J>\<cdot\><text|m><rsup|-2>>. Find the pressure in the droplet if
      the pressure of the surrounding air is <math|1.00<br>>.

      <item>Calculate the difference between the fugacity of
      H<rsub|<math|2>>O in the air of pressure <math|1.00<br>> equilibrated
      with this water droplet, and the fugacity in air equilibrated at the
      same temperature and pressure with a pool of liquid water having a flat
      surface. Liquid water at <math|25 <degC>> and <math|1<br>> has a vapor
      pressure of <math|0.032<br>> and a molar volume of
      <math|1.807<timesten|-5> <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>.
    </enumerate-alpha>
  </problem>

  <\problem>
    For a solution process in which species B is transferred from a gas phase
    to a liquid solution, find the relation between
    <math|\<Delta\><rsub|<text|sol>>*G<st>> (solute standard state based on
    mole fraction) and the Henry's law constant <math|<kHB>>.
  </problem>

  <\problem>
    <label|prb:12-CO2>Crovetto<footnote|Ref. <cite|crovetto-91>.> reviewed
    the published data for the solubility of gaseous CO<rsub|<math|2>> in
    water, and fitted the Henry's law constant <math|<kHB>> to a function of
    temperature. Her recommended values of <math|<kHB>> at five temperatures
    are <math|1233<br>> at <math|15.00 <degC>>, <math|1433<br>> at
    <math|20.00 <degC>>, <math|1648<br>> at <math|25.00 <degC>>,
    <math|1874<br>> at <math|30.00 <degC>>, and <math|2111<br>> at <math|35
    <degC>>.

    <\enumerate-alpha>
      <item>The partial pressure of CO<rsub|<math|2>> in the atmosphere is
      typically about <math|3<timesten|-4><br>>. Assume a fugacity of
      <math|3.0<timesten|-4><br>>, and calculate the aqueous solubility at
      <math|25.00 <degC>> expressed both as a mole fraction and as a
      molality.

      <item>Find the standard molar enthalpy of solution at <math|25.00
      <degC>>.

      <item>Dissolved carbon dioxide exists mostly in the form of
      CO<rsub|<math|2>> molecules, but a small fraction exists as
      H<rsub|<math|2>>CO<rsub|<math|3>> molecules, and there is also some
      ionization:

      <no-indent><math|<text|CO<rsub|2>><around|(|<text|aq>|)>+<text|H<rsub|2>O><around|(|<text|l>|)><arrow><chem|<text|H>|+><around|(|<text|aq>|)>+<chem|<text|HCO<rsub|3>>|-><around|(|<text|aq>|)>>

      <no-indent>(The equilibrium constant of this reaction is often called
      the first ionization constant of carbonic acid.) Combine the
      <math|<kHB>> data with data in Appendix <reference|app:props> to
      evaluate <math|K> and <math|\<Delta\><rsub|<text|r>>*H<st>> for the
      ionization reaction at <math|25.00 <degC>>. Use solute standard states
      based on molality, which are also the solute standard states used for
      the values in Appendix <reference|app:props>.
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:12-salting out>The solubility of gaseous O<rsub|<math|2>> at a
    partial pressure of <math|1.01<br>> and a temperature of <math|310.2<K>>,
    expressed as a concentration, is <math|1.07<timesten|-3>
    <text|mol>\<cdot\><text|dm><rsup|-3>> in pure water and
    <math|4.68<timesten|-4> <text|mol>\<cdot\><text|dm><rsup|-3>> in a
    <math|3.0 <text|M>> aqueous solution of KCl.<footnote|Ref.
    <cite|CRC-92>.> This solubility decrease is the <index|Salting-out effect
    on gas solubility><em|salting-out effect>. Calculate the activity
    coefficient <math|<g><cbB>> of O<rsub|<math|2>> in the KCl solution.
  </problem>

  <\problem>
    <label|prb:12-p effect>At <math|298.15<K>>, the partial molar volume of
    CO<rsub|<math|2>>(aq) is <math|33 <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>.
    Use Eq. <reference|KmB(p2) approx> to estimate the percent change in the
    value of the Henry's law constant <math|<kHB>> for aqueous
    CO<rsub|<math|2>> at <math|298.15<K>> when the total pressure is changed
    from <math|1.00<br>> to <math|10.00<br>>.
  </problem>

  <\problem>
    <label|prb:12-gas sol>Rettich et al<footnote|Ref. <cite|rettich-00>.>
    made high-precision measurements of the solubility of gaseous oxygen
    (<math|<chem|<text|O<rsub|2>>|>>) in water. Each measurement was made by
    equilibrating water and oxygen in a closed vessel for a period of up to
    two days, at a temperature controlled within <math|\<pm\>0.003<K>>. The
    oxygen was extracted from samples of known volume of the equilibrated
    liquid and gas phases, and the amount of <math|<chem|<text|O<rsub|2>>|>>
    in each sample was determined from <math|p>-<math|V>-<math|T>
    measurements taking gas nonideality into account. It was then possible to
    evaluate the mole fraction <math|x<B>> of <math|<chem|<text|O<rsub|2>>|>>
    in the liquid phase and the ratio <math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>>
    for the <math|<chem|<text|O<rsub|2>>|>> in the gas
    phase.<float|float|thb|<\big-table|<tabular*|<tformat|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|6|6|1|-1|cell-bborder|1ln>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|3|3|cell-rborder|0ln>|<table|<row|<cell|<math|T=298.152<K>>>|<cell|<space|2em>>|<cell|Second
    virial coefficients:>>|<row|<cell|<math|x<B>=2.02142<timesten|-5>>>|<cell|>|<cell|<math|B<rsub|<text|A>\<nocomma\><text|A>>=-1152<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<cell|<math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>=35.9957
    <text|mol>\<cdot\><text|m><rsup|-3>>>|<cell|>|<cell|<math|B<rsub|<text|B>\<nocomma\><text|B>>=-16.2<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<cell|<math|p<A><rsup|\<ast\>>=3167.13
    <text|Pa>>>|<cell|>|<cell|<math|B<rsub|<text|A>\<nocomma\><text|B>>=-27.0<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<cell|<math|V<A><rsup|\<ast\>>=18.069<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>|<cell|>|<cell|>>|<row|<cell|<math|V<B><rsup|\<infty\>>=31.10<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>|<cell|>|<cell|>>>>>>
      <label|tbl:12-gas sol>Data for Problem <reference|prb:12-gas sol> (A =
      H<rsub|<math|2>>O, B = O<rsub|<math|2>>)
    </big-table>>

    Table <reference|tbl:12-gas sol> gives values of physical quantities at
    <math|T=298.152<K>> needed for this problem. The values of <math|x<B>>
    and <math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>> were
    obtained by Rettich et al from samples of liquid and gas phases
    equilibrated at temperature <math|T>, as explained above.
    <math|p<A><rsup|\<ast\>>> is the saturation vapor pressure of pure liquid
    water at this temperature.

    Your calculations will be similar to those used by Rettich et al to
    obtain values of the Henry's law constant of oxygen to six significant
    figures. Your own calculations should also be carried out to six
    significant figures. For the gas constant, use the value <math|R=<Rsix>>.

    The method you will use to evaluate the Henry's law constant
    <math|<kHB>=<fug><B>/x<B>> at the experimental temperature and pressure
    is as follows. The value of <math|x<B>> is known, and you need to find
    the fugacity <math|<fug><B>> of the <chem|<text|O<rsub|2>>|> in the gas
    phase. <math|<fug><B>> can be calculated from <math|\<phi\><B>> and
    <math|p<B>>. These in turn can be calculated from the pressure <math|p>,
    the mole fraction <math|y<B>> of <chem|<text|O<rsub|2>>|> in the gas
    phase, and known values of second virial coefficients. You will calculate
    <math|p> and <math|y<B>> by an iterative procedure. Assume the gas has
    the virial equation of state <math|<around|(|V<rsup|<text|g>>/n<rsup|<text|g>>|)>=<around|(|R*T/p|)>+B>
    (Eq. <reference|V=nRT/p+nB>) and use relevant relations in Sec.
    <reference|9-real gas mixtures>.

    <\enumerate-alpha>
      <item>For the equilibrated liquid-gas system, calculate initial
      approximate values of <math|p> and <math|y<B>> by assuming that
      <math|p<A>> is equal to <math|p<A><rsup|\<ast\>>> and <math|p<B>> is
      equal to <math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>*R*T>.

      <item>Use your approximate values of <math|p> and <math|y<B>> from part
      (a) to calculate <math|\<phi\><A>>, the fugacity coefficient of A in
      the gas mixture.

      <item>Evaluate the fugacity <math|<fug><A>> of the
      <math|<text|H<rsub|2>O>> in the gas phase. Assume <math|p>,
      <math|y<B>>, and <math|\<phi\><A>> have the values you calculated in
      parts (a) and (b). Hint: start with the value of the saturation vapor
      pressure of pure water.

      <item>Use your most recently calculated values of <math|p>,
      <math|\<phi\><A>>, and <math|<fug><A>> to calculate an improved value
      of <math|y<B>>.

      <item>Use your current values of <math|p> and <math|y<B>> to evaluate
      the compression factor <math|Z> of the gas mixture, taking nonideality
      into account.

      <item>Derive a general expression for <math|p> as a function of
      <math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>>, <math|T>,
      <math|y<B>>, and <math|Z>. Use this expression to calculate an improved
      value of <math|p>.

      <item>Finally, use the improved values of <math|p> and <math|y<B>> to
      evaluate the Henry's law constant <math|<kHB>> at the experimental
      <math|T> and <math|p>.
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:12-water-CH4>The method described in Prob.
    <reference|prb:12-gas sol> has been used to obtain high-precision values
    of the Henry's law constant, <math|<kHB>>, for gaseous methane dissolved
    in water.<footnote|Ref. <cite|rettich-81>.> Table
    <reference|tbl:12-water-CH4><float|float|thb|<\big-table|<tabular*|<tformat|<cwith|2|-1|1|-1|cell-halign|C.>|<cwith|7|7|1|-1|cell-bborder|1ln>|<cwith|2|-1|1|1|cell-lborder|0ln>|<cwith|2|-1|5|5|cell-rborder|0ln>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-lborder|0ln>|<cwith|1|1|5|5|cell-rborder|0ln>|<table|<row|<cell|<math|1/<around|(|T/<text|K>|)>>>|<cell|<math|<text|ln>
    <around|(|<kHB>/p<st>|)>>>|<cell|<space|2em>>|<cell|<math|1/<around|(|T/<text|K>|)>>>|<cell|<math|ln
    <around|(|<kHB>/p<st>|)>>>>|<row|<cell|0.00363029>|<cell|10.0569>|<cell|>|<cell|0.00329870>|<cell|10.6738>>|<row|<cell|0.00359531>|<cell|10.1361>|<cell|>|<cell|0.00319326>|<cell|10.8141>>|<row|<cell|0.00352175>|<cell|10.2895>|<cell|>|<cell|0.00314307>|<cell|10.8673>>|<row|<cell|0.00347041>|<cell|10.3883>|<cell|>|<cell|0.00309444>|<cell|10.9142>>|<row|<cell|0.00341111>|<cell|10.4951>|<cell|>|<cell|0.00304739>|<cell|10.9564>>|<row|<cell|0.00335390>|<cell|10.5906>|<cell|>|<cell|>|<cell|>>>>>>
      <label|tbl:12-water-CH4>Data for Prob. <reference|prb:12-water-CH4>
    </big-table>> lists values of <math|ln <around|(|<kHB>/p<st>|)>> at
    eleven temperatures in the range <math|275<K>>\U<math|328<K>> and at
    pressures close to <math|1<br>>. Use these data to evaluate
    <math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>> and
    <math|\<Delta\><rsub|<text|sol>,<text|B>>*C<st><rsub|p>> at
    <math|T=298.15<K>>. This can be done by a graphical method. Better
    precision will be obtained by making a least-squares fit of the data to
    the three-term polynomial

    <\equation*>
      ln <around|(|<kHB>/p<st>|)>=a+b*<around|(|1/T|)>+c*<around|(|1/T|)><rsup|2>
    </equation*>

    and using the values of the coefficients <math|a>, <math|b>, and <math|c>
    for the evaluations.
  </problem>

  <\problem>
    Liquid water and liquid benzene have very small mutual solubilities.
    Equilibria in the binary water\Ubenzene system were investigated by
    Tucker, Lane, and Christian<footnote|Ref. <cite|tucker-81>.> as follows.
    A known amount of distilled water was admitted to an evacuated,
    thermostatted vessel. Part of the water vaporized to form a vapor phase.
    Small, precisely measured volumes of liquid benzene were then added
    incrementally from the sample loop of a liquid-chromatography valve. The
    benzene distributed itself between the liquid and gaseous phases in the
    vessel. After each addition, the pressure was read with a precision
    pressure gauge. From the known amounts of water and benzene and the total
    pressure, the liquid composition and the partial pressure of the benzene
    were calculated. The fugacity of the benzene in the vapor phase was
    calculated from its partial pressure and the second virial coefficient.

    At a fixed temperature, for mole fractions <math|x<B>> of benzene in the
    liquid phase up to about <math|3<timesten|-4>> (less than the solubility
    of benzene in water), the fugacity of the benzene in the equilibrated gas
    phase was found to have the following dependence on <math|x<B>>:

    <\equation*>
      <frac|<fug><B>|x<B>>=<kHB>-A*x<B>
    </equation*>

    Here <math|<kHB>> is the Henry's law constant and <math|A> is a constant
    related to deviations from Henry's law. At <math|30 <degC>>, the measured
    values were <math|<kHB>=385.5<br>> and <math|A=2.24<timesten|4><br>>.

    <\enumerate-alpha>
      <item>Treat benzene (B) as the solute and find its activity coefficient
      on a mole fraction basis, <math|<g><xbB>>, at <math|30 <degC>> in the
      solution of composition <math|x<B>=3.00<timesten|-4>>.

      <item>The fugacity of benzene vapor in equilibrium with pure liquid
      benzene at <math|30 <degC>> is <math|<fug><B><rsup|\<ast\>>=0.1576<br>>.
      Estimate the mole fraction solubility of liquid benzene in water at
      this temperature.

      <item>The calculation of <math|<g><xbB>> in part (a) treated the
      benzene as a single solute species with deviations from
      infinite-dilution behavior. Tucker et al suggested a dimerization model
      to explain the observed negative deviations from Henry's law.
      (Classical thermodynamics, of course, cannot prove such a molecular
      interpretation of observed macroscopic behavior.) The model assumes
      that there are two solute species, a monomer (M) and a dimer (D), in
      reaction equilibrium: <math|2*<text|M><arrows><text|D>>. Let
      <math|n<B>> be the total amount of C<rsub|<math|6>>H<rsub|<math|6>>
      present in solution, and define the mole fractions

      <\equation*>
        x<B><defn><frac|n<B>|n<A>+n<B>>\<approx\><frac|n<B>|n<A>>
      </equation*>

      <\equation*>
        x<rsub|<text|M>><defn><frac|n<rsub|<text|M>>|n<A>+n<rsub|<text|M>>+n<rsub|<text|D>>>\<approx\><frac|n<rsub|<text|M>>|n<A>>*<space|2em>x<rsub|<text|D>><defn><frac|n<rsub|<text|D>>|n<A>+n<rsub|<text|M>>+n<rsub|<text|D>>>\<approx\><frac|n<rsub|<text|D>>|n<A>>
      </equation*>

      where the approximations are for dilute solution. In the model, the
      individual monomer and dimer particles behave as solutes in an
      ideal-dilute solution, with activity coefficients of unity. The monomer
      is in transfer equilibrium with the gas phase:
      <math|x<rsub|<text|M>>=<fug><B>/<kHB>>. The equilibrium constant
      expression (using a mole fraction basis for the solute standard states
      and setting pressure factors equal to 1) is
      <math|K=x<rsub|<text|D>>/x<rsub|<text|M>><rsup|2>>. From the relation
      <math|n<B>=n<rsub|<text|M>>+2*n<rsub|<text|D>>>, and because the
      solution is very dilute, the expression becomes

      <\equation*>
        K=<frac|x<B>-x<rsub|<text|M>>|2*x<rsub|<text|M>><rsup|2>>
      </equation*>

      Make individual calculations of <math|K> from the values of
      <math|<fug><B>> measured at <math|x<B>=1.00<timesten|-4>>,
      <math|x<B>=2.00<timesten|-4>>, and <math|x<B>=3.00<timesten|-4>>.
      Extrapolate the calculated values of <math|K> to <math|x<B|=>0> in
      order to eliminate nonideal effects such as higher aggregates. Finally,
      find the fraction of the benzene molecules present in the dimer form at
      <math|x<B>=3.00<timesten|-4>> if this model is correct.
    </enumerate-alpha>
  </problem>

  <\problem>
    Use data in Appendix <reference|app:props> to evaluate the thermodynamic
    equilibrium constant at <math|298.15<K>> for the limestone reaction

    <\equation*>
      <text|CaCO<rsub|3>> <around|(|<text|cr>,<text|calcite>|)>+<text|CO<rsub|2>>
      <around|(|<text|g>|)>+<text|H<rsub|2>O
      ><around|(|<text|l>|)><arrow><chem|<text|Ca>|2+>
      <around|(|<text|aq>|)>+2*<chem|<text|HCO<rsub|3>>|->
      <around|(|<text|aq>|)>
    </equation*>
  </problem>

  <\problem>
    <label|prb:12-formic acid> For the dissociation equilibrium of formic
    acid, <math|<text|HCO<rsub|2>H><around|(|<text|aq>|)><arrows><chem|<text|H>|+>
    <around|(|<text|aq>|)>+<chem|<text|HCO<rsub|2>>|->
    <around|(|<text|aq>|)>>, the acid dissociation constant at
    <math|298.15<K>> has the value <math|K<rsub|<text|a>>=1.77<timesten|-4>>.

    <\enumerate-alpha>
      <item>Use Eq. <reference|Ka> to find the degree of dissociation and the
      hydrogen ion molality in a 0.01000 molal formic acid solution. You can
      safely set <math|<G><rsub|<text|r>>> and <math|<g><rsub|m,<text|HA>>>
      equal to <math|1>, and use the Debye\UH�ckel limiting law (Eq.
      <reference|DH limiting law>) to calculate <math|<g><rsub|\<pm\>>>. You
      can do this calculation by iteration: Start with an initial estimate of
      the ionic strength (in this case 0), calculate <math|<g><rsub|\<pm\>>>
      and <math|\<alpha\>>, and repeat these steps until the value of
      <math|\<alpha\>> no longer changes.

      <item>Estimate the degree of dissociation of formic acid in a solution
      that is 0.01000 molal in both formic acid and sodium nitrate, again
      using the Debye--H�ckel limiting law for <math|<g><rsub|\<pm\>>>.
      Compare with the value in part (a).
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:12-Cl ion>Use the following experimental information to
    evaluate the standard molar enthalpy of formation and the standard molar
    entropy of the aqueous chloride ion at <math|298.15<K>>, based on the
    conventions <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|H<rsup|<math|+>>,aq>|)>=0>
    and <math|S<m><st><around|(|<text|H<rsup|<math|+>>,aq>|)>=0> (Secs.
    <reference|11-st molar enthalpy of formation> and <reference|11-eval of
    K>). (Your calculated values will be close to, but not exactly the same
    as, those listed in Appendix <reference|app:props>, which are based on
    the same data combined with data of other workers.)

    <\itemize>
      <item>For the reaction <math|<frac|1|2>*<text|H<rsub|2>>
      <around|(|<text|g>|)>+<frac|1|2>*<text|Cl<rsub|2>>
      <around|(|<text|g>|)><arrow><text|HCl> <around|(|<text|g>|)>>, the
      standard molar enthalpy of reaction at <math|298.15<K>> measured in a
      flow calorimeter<footnote|Ref. <cite|rossini-32>.> is
      <math|\<Delta\><rsub|<text|r>>*H<st>=-92.312
      <text|kJ>\<cdot\><text|mol><rsup|-1>>.

      <item>The standard molar entropy of gaseous HCl at <math|298.15<K>>
      calculated from spectroscopic data is <math|S<m><st>=186.902
      <text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>.

      <item>From five calorimetric runs,<footnote|Ref. <cite|gunn-63>.> the
      average experimental value of the standard molar enthalpy of solution
      of gaseous HCl at <math|298.15<K>> is
      <math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=-74.84
      <text|kJ>\<cdot\><text|mol><rsup|-1>>.

      <item>From vapor pressure measurements of concentrated aqueous HCl
      solutions,<footnote|Ref. <cite|randall-28>.> the value of the ratio
      <math|<fug><B>/a<mbB>> for gaseous HCl in equilibrium with aqueous HCl
      at <math|298.15<K>> is <math|5.032<timesten|-7><br>>.
    </itemize>
  </problem>

  <\problem>
    <label|prb:12-Ks(AgCl)>The solubility of crystalline AgCl in ultrapure
    water has been determined from the electrical conductivity of the
    saturated solution.<footnote|Ref. <cite|gledhill-52>.> The average of
    five measurements at <math|298.15<K>> is <math|s<B>=1.337<timesten|-5>
    <text|mol>\<cdot\><text|dm><rsup|-3>>. The density of water at this
    temperature is <math|\<rho\><A><rsup|\<ast\>>=0.9970
    <text|kg>\<cdot\><text|dm><rsup|-3>>.

    <\enumerate-alpha>
      <item>From these data and the Debye--H�ckel limiting law, calculate the
      solubility product <math|K<rsub|<text|s>>> of AgCl at <math|298.15<K>>.

      <item>Evaluate the standard molar Gibbs energy of formation of aqueous
      Ag<rsup|<math|+>> ion at <math|298.15<K>>, using the results of part
      (a) and the values <math|\<Delta\><rsub|<text|f>>*G<st><around|(|<text|Cl<rsup|<math|->>,aq>|)>=-131.22
      <text|kJ>\<cdot\><text|mol><rsup|-1>> and
      <math|\<Delta\><rsub|<text|f>>*G<st><around|(|<text|AgCl,s>|)>=-109.77
      <text|kJ>\<cdot\><text|mol><rsup|-1>> from Appendix
      <reference|app:props>.
    </enumerate-alpha>
  </problem>

  <\problem>
    <label|prb:12-AgCl pptn>The following reaction was carried out in an
    adiabatic solution calorimeter by Wagman and Kilday:<footnote|Ref.
    <cite|wagman-73>.>

    <\equation*>
      <text|AgNO<rsub|3>> <around*|(|<text|s>|)>+<text|KCl>
      <around*|(|<text|aq>,m<rsub|<text|B>>=0.101
      <text|mol>\<cdot\><text|kg><rsup|-1>|)><arrow><text|AgCl>
      <around*|(|<text|s>|)>+<text|KNO<rsub|3>> <around*|(|<text|aq>|)>
    </equation*>

    The reaction can be assumed to go to completion, and the amount of KCl
    was in slight excess, so the amount of AgCl formed was equal to the
    initial amount of AgNO<rsub|<math|3>>. After correction for the
    enthalpies of diluting the solutes in the initial and final solutions to
    infinite dilution, the standard molar reaction enthalpy at
    <math|298.15<K>> was found to be <math|\<Delta\><rsub|<text|r>>*H<st>=-43.042
    <text|kJ>\<cdot\><text|mol><rsup|-1>>. The same workers used solution
    calorimetry to obtain the molar enthalpy of solution at infinite dilution
    of crystalline AgNO<rsub|<math|3>> at <math|298.15<K>>:
    <math|\<Delta\><rsub|<text|sol>,<text|B>>*H<rsup|\<infty\>>=22.727
    <text|kJ>\<cdot\><text|mol><rsup|-1>>.

    <\enumerate-alpha>
      <item><label|AgCl delH(pptn)>Show that the difference of these two
      values is the standard molar reaction enthalpy for the precipitation
      reaction

      <\equation*>
        <chem|<text|Ag>|+> <around*|(|<text|aq>|)>+<chem|<text|Cl>|->
        <around*|(|<text|aq>|)><arrow><text|AgCl> <around*|(|<text|s>|)>
      </equation*>

      and evaluate this quantity.

      <item>Evaluate the standard molar enthalpy of formation of aqueous
      Ag<rsup|<math|+>> ion at <math|298.15<K>>, using the results of part
      (a) and the values <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|Cl<rsup|<math|->>>,<text|aq>|)>=-167.08
      <text|kJ>\<cdot\><text|mol><rsup|-1>> and
      <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|AgCl>,<text|s>|)>=-127.01
      <text|kJ>\<cdot\><text|mol><rsup|-1>> from Appendix
      <reference|app:props>. (These values come from calculations similar to
      those in Probs. <reference|prb:12-Cl ion> and <reference|prb:14-AgCl
      formation>.) The calculated value will be close to, but not exactly the
      same as, the value listed in Appendix <reference|app:props>, which is
      based on the same data combined with data of other workers.
    </enumerate-alpha>

    \;
  </problem>

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|12>
    <associate|page-first|323>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
    <associate|section-nr|10>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|AgCl delH(pptn)|<tuple|a|?>>
    <associate|CaCO3 eval|<tuple|b|?>>
    <associate|auto-1|<tuple|11|?>>
    <associate|auto-2|<tuple|11.1|?>>
    <associate|auto-3|<tuple|Entropy|?>>
    <associate|auto-4|<tuple|11.2|?>>
    <associate|auto-5|<tuple|Salting-out effect on gas solubility|?>>
    <associate|auto-6|<tuple|11.3|?>>
    <associate|auto-7|<tuple|11.4|?>>
    <associate|footnote-11.1|<tuple|11.1|?>>
    <associate|footnote-11.10|<tuple|11.10|?>>
    <associate|footnote-11.11|<tuple|11.11|?>>
    <associate|footnote-11.2|<tuple|11.2|?>>
    <associate|footnote-11.3|<tuple|11.3|?>>
    <associate|footnote-11.4|<tuple|11.4|?>>
    <associate|footnote-11.5|<tuple|11.5|?>>
    <associate|footnote-11.6|<tuple|11.6|?>>
    <associate|footnote-11.7|<tuple|11.7|?>>
    <associate|footnote-11.8|<tuple|11.8|?>>
    <associate|footnote-11.9|<tuple|11.9|?>>
    <associate|footnr-11.1|<tuple|11.1|?>>
    <associate|footnr-11.10|<tuple|11.10|?>>
    <associate|footnr-11.11|<tuple|11.11|?>>
    <associate|footnr-11.2|<tuple|11.2|?>>
    <associate|footnr-11.3|<tuple|11.3|?>>
    <associate|footnr-11.4|<tuple|11.4|?>>
    <associate|footnr-11.5|<tuple|11.5|?>>
    <associate|footnr-11.6|<tuple|11.6|?>>
    <associate|footnr-11.7|<tuple|11.7|?>>
    <associate|footnr-11.8|<tuple|11.8|?>>
    <associate|footnr-11.9|<tuple|11.9|?>>
    <associate|prb:12-AgCl pptn|<tuple|11.20|?>>
    <associate|prb:12-BuBenz|<tuple|11.6|?>>
    <associate|prb:12-CO2|<tuple|11.10|?>>
    <associate|prb:12-CaCO3|<tuple|11.1|?>>
    <associate|prb:12-Cl ion|<tuple|11.18|?>>
    <associate|prb:12-Del(r)S(m)o|<tuple|11.3|?>>
    <associate|prb:12-Kf Kb|<tuple|11.4|?>>
    <associate|prb:12-Ks(AgCl)|<tuple|11.19|?>>
    <associate|prb:12-droplet|<tuple|11.8|?>>
    <associate|prb:12-formic acid|<tuple|11.17|?>>
    <associate|prb:12-gas sol|<tuple|11.13|?>>
    <associate|prb:12-p effect|<tuple|11.12|?>>
    <associate|prb:12-salting out|<tuple|11.11|?>>
    <associate|prb:12-water-CH4|<tuple|11.14|?>>
    <associate|tbl:12-CO2 pressure|<tuple|11.1|?>>
    <associate|tbl:12-Kb Kf|<tuple|11.2|?>>
    <associate|tbl:12-gas sol|<tuple|11.3|?>>
    <associate|tbl:12-water-CH4|<tuple|11.4|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      symth-23

      crovetto-91

      CRC-92

      rettich-00

      rettich-81

      tucker-81

      rossini-32

      gunn-63

      randall-28

      gledhill-52

      wagman-73
    </associate>
    <\associate|idx>
      <tuple|<tuple|Entropy|reaction|standard molar>|<pageref|auto-3>>

      <tuple|<tuple|Salting-out effect on gas solubility>|<pageref|auto-5>>
    </associate>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.1>|>
        Pressure of an equilibrium system containing
        CaCO<rsub|<with|mode|<quote|math>|3>>(s), CaO(s), and
        CO<rsub|<with|mode|<quote|math>|2>>(g).<space|0spc><assign|footnote-nr|1><hidden-binding|<tuple>|11.1><assign|fnote-+CSll5WOWbWXt1g|<quote|11.1>><assign|fnlab-+CSll5WOWbWXt1g|<quote|11.1>><rsup|<with|font-shape|<quote|right>|<reference|footnote-11.1>>>

        \;

        <surround|||<with|font-size|<quote|0.771>|<surround|<locus|<id|%728B1FC8-8EAF3338>|<link|hyperlink|<id|%728B1FC8-8EAF3338>|<url|#footnr-11.1>>|11.1>.
        |<hidden-binding|<tuple|footnote-11.1>|11.1>|>>>Ref.
        [<write|bib|symth-23><reference|bib-symth-23>].
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.2>|>
        Properties of H<rsub|<with|mode|<quote|math>|2>>O at
        <with|mode|<quote|math>|1<with|mode|<quote|math>|
        <with|mode|<quote|text>|bar>>>
      </surround>|<pageref|auto-4>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.3>|>
        Data for Problem <reference|prb:12-gas sol> (A =
        H<rsub|<with|mode|<quote|math>|2>>O, B =
        O<rsub|<with|mode|<quote|math>|2>>)
      </surround>|<pageref|auto-6>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|11.4>|>
        Data for Prob. <reference|prb:12-water-CH4>
      </surround>|<pageref|auto-7>>
    </associate>
    <\associate|toc>
      11<space|2spc>Problems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>
    </associate>
  </collection>
</auxiliary>