#+TITLE: DeVoe's Thermodynamics and Chemistry, TeXmacs typeset License
#+AUTHOR: Steven Baltakatei Sandoval
#+EMAIL: baltakatei@gmail.com
#+LANGUAGE: en
#+OPTIONS: html-postamble:nil toc:nil

This file was created by [[https://baltakatei.com][Steven Baltakatei Sandoval]] on
2021-04-04T19:08:18Z under a [[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]] license and last updated on
2021-04-24T20:13Z.

* Summary
- The main text, "Thermodynamics and Chemistry" typeset in TeXmacs by
  Steven Baltakatei Sandoval, is licensed under [[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]].
- The original text, "Thermodynamics and Chemistry" by Howard DeVoe,
  is licensed under [[https://creativecommons.org/licenses/by/4.0/][CC BY 4.0]].
- Some files were created by other authors (e.g. figure images; See
  [[File-specific Licenses][File-specific Licenses]]). Such files may have their license specified
  by:
  - Information embedded within the file. (e.g. [[https://jeffmcneill.com/image-metadata/][image metadata]])
  - A ~.license~ file. (e.g. ~photo.jpg~ may be accompanied by a
    ~photo.jpg.license.txt~ file)

* Main text license
"DeVoe's Thermodynamics and Chemistry, TeXmacs typeset", is a
derivative of "[[http://www2.chem.umd.edu/thermobook/][Thermodynamics and Chemistry]]" by Howard DeVoe, used
under [[https://creativecommons.org/licenses/by/4.0/][CC BY 4.0]]. "DeVoe's Thermodynamics and Chemistry, TeXmacs typeset"
is licensed under [[https://creativecommons.org/licenses/by-sa/4.0/][CC BY-SA 4.0]] by [[http://baltakatei.com][Steven Baltakatei Sandoval]].

A copy of the CC BY-SA 4.0 license is included [[./legal/cc-by-sa-4.0..legalcode.txt][here]].

A copy of the CC BY 4.0 license is included [[./legal/cc-by-4.0..legalcode.txt][here]].

* File-specific Licenses
Some files in this repository, when rendered using appropriate
software, may indicate their own licenses (i.e. a JPEG image with
license details forming part of the visible image itself when rendered
using a web browser). For such files, this embedded license is
sufficient to define the license.

If no embedded license is present within a file, the license may be
specified using a ~.license~ file saved within the same directory as
the the file. For example, a file named ~example.png~ may have its
license defined using a text file named ~example.png.license.txt~; the
text file should contain human-readable license information; the extra
~.txt~ extension specifies the text file format (e.g. ~.md~, ~.org~).

If an embedded license is present within a file but a different
license is intended, the ~.license~ file takes precedence. This rule
is useful in cases where the integrity of the original file cannot be
violated for the purposes of identifying the file via cryptographic
digests (i.e. [[https://en.wikipedia.org/wiki/SHA-2][SHA256]]).
