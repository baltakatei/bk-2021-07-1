#+TITLE: DeVoe's Thermodynamics and Chemistry, TeXmacs typeset
#+AUTHOR: Steven Baltakatei Sandoval
#+EMAIL: baltakatei@gmail.com
#+DATE: 2022-03-20
#+LANGUAGE: en
#+OPTIONS: html-postamble:nil toc:nil

** Summary
This project is to retypeset the textbook "[[http://www2.chem.umd.edu/thermobook/][Thermodynamics and
Chemistry]]" by Professor Howard DeVoe using [[https://texmacs.org][TeXmacs]].

** Deliverables
The current draft may be found [[https://reboil.com/res/2022/txt/20220320T1802Z..devoe_thermo_book_retypeset_draft.pdf][here]].

See [[Build instructions]] to create your own PDF copy.

** Background
The textbook "[[http://www2.chem.umd.edu/thermobook/][Thermodynamics and Chemistry]]" is licensed Creative
Commons Attribution 4.0 International (CC BY 4.0). The version
contained within this project is typeset in [[https://texmacs.org][TeXmacs]] and is a
derivative work licensed Creative Commons Attribution-ShareAlike 4.0
International (CC BY-SA 4.0).

A solution set will be found in [[https://reboil.com/gitweb/BK-2021-07-2.git][this]] git repository.

** Authorship
Original textbook by [[http://www2.chem.umd.edu/thermobook/][Howard DeVoe]], Associate Professor Emeritus,
University of Maryland.

Retypeset version by [[http://baltakatei.com][Steven Baltakatei Sandoval]] ([[https://twitter.com/baltakatei][Twitter]], [[https://gitlab.com/baltakatei/bk-2021-07-1][GitLab]]).

** Status
- Chapter 1: Done.
- Chapter 2: Done.
- Chapter 3: Done.
- Chapter 4: Done.
- Chapter 5: Done.
- Chapter 6: Done.
- Chapter 7: Done.
- Chapter 8: Done.
- Chapter 9: Done.
- Chapter 10: Done.
- Chapter 11: Done.
- Chapter 12: Done.
- Chapter 13: Done.
- Chapter 14: Done.
- Appendix A: Done.
- Appendix B: Done.
- Appendix C: Done.
- Appendix D: Done.
- Appendix E: Done.
- Appendix F: Done.
- Appendix G: Done.
- Appendix H: Done.
- Appendix I: Done.
- Bibliography: Done.
- Index: Done.

*** Update Notifications
- [[https://reboil.com/gitweb/BK-2021-07-1.git/rss][RSS feed]]
- [[https://reboil.com/gitweb/BK-2021-07-1.git/atom][Atom feed]]

** License
See the [[./LICENSE.org][LICENSE]] file.
** Directory Structure
- ~img/~: Contains some image files referenced by ~src/~
  files. (e.g. ~.eps~, ~.svg~)
- ~legal/~: Contains files relevant legal files (e.g. license texts)
- ~ref/~: Contains reference material used in developing deliverables.
- ~src/~: Contains project source code.
- ~src-legacy/~: Contains very rough TeXmacs import of original ~.eps~
  files.
- ~temp/~: For containing untracked temporary files.
** Build instructions
*** 1. Download and install
Install:
- [[https://texmacs.org][TeXmacs]] - WYSIWYG scientific typesetting program (version 2.1.1 as
  of 2022-01-22).
- [[https://git-scm.com/][git]] - Version control software

*** 2. Clone this repository
Git can copy project files using this command:
#+BEGIN_EXAMPLE
$ git clone https://gitlab.com/baltakatei/bk-2021-07-1 BK-2021-07-1..devoe_thermo 
#+END_EXAMPLE

*** 3. Open master source file
Open ~BK-2021-07-1..devoe_thermo/src/book.tm~ in TeXmacs.

*** 4. Export PDF
Select File -> Export -> Pdf… .

*** 5. (optional) Update master source file
If changes to source files were made, the master file ~src/book.tm~
should be updated. This may be performed by clicking:

Tools -> Update -> Inclusions

then

Document -> Update -> All
