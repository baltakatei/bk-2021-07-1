<TeXmacs|1.99.20>

<style|<tuple|generic|style-bk>>

<\body>
  <\hide-preamble>
    <assign|R|<macro|\<bbb-R\>>>
  </hide-preamble>

  <label|Chap7><problem>Derive the following relations from the definitions
  of <math|\<alpha\>>, <math|<kT>>, and <math|\<rho\>>:

  <\equation*>
    \<alpha\>=-<frac|1|\<rho\>><Pd|\<rho\>|T|<space|-0.17em>p><space|2em><kT>=<frac|1|\<rho\>><Pd|\<rho\>|p|T>
  </equation*>

  <\soln>
    \ The definitions of <math|\<alpha\>> and <math|<kT>> refer to a fixed
    mass of a uniform phase. Let <math|X=1/V=\<rho\>/m>, where <math|m> is
    constant: <vs><math|<D>\<alpha\><defn><frac|1|V><Pd|V|T|<space|-0.17em>p>=X<bPd|<around|(|1/X|)>|T|<space|-0.17em>p>=X*<around*|[|-<frac|1|X<rsup|2>><Pd|X|T|<space|-0.17em>p>|]>>
    <vs> <math|<D><phantom|\<alpha\>>=-<frac|1|X><Pd|X|T|<space|-0.17em>p>=-<frac|m|\<rho\>><bPd|<around|(|\<rho\>/m|)>|T|p>=-<frac|1|\<rho\>><Pd|\<rho\>|T|<space|-0.17em>p>>
    <vs><math|<D><kT><defn>-<frac|1|V><Pd|V|p|T>=-X<bPd|<around|(|1/X|)>|p|T>=-X*<around*|[|-<frac|1|X<rsup|2>><Pd|X|p|T>|]>>
    <vs> <math|<D><phantom|<kT>>=<frac|1|X><Pd|X|p|T>=<frac|m|\<rho\>><bPd|<around|(|\<rho\>/m|)>|p|T>=<frac|1|\<rho\>><Pd|\<rho\>|p|T>>
  </soln>

  <problem>Use equations in this chapter to derive the following expressions
  for an ideal gas: <I|Cubic expansion coefficient!ideal gas@of an ideal
  gas\|p><I|Isothermal!compressibility!ideal gas@of an ideal gas\|p>

  <\equation*>
    \<alpha\>=1/T<space|2em><kT>=1/p
  </equation*>

  <\soln>
    \ In the following, <math|n> is constant:
    <vs><math|<D>\<alpha\><defn><frac|1|V><Pd|V|T|<space|-0.17em>p>=<frac|1|V><bPd|<around|(|n*R*T/p|)>|T|p>=<frac|1|V><around*|(|<frac|n*R|p>|)>>
    <vs> <math|<D><phantom|\<alpha\>>=<frac|1|T>>
    <vs><math|<D><kT><defn>-<frac|1|V><Pd|V|p|T>=-<frac|1|V><bPd|(n*R*T/p|p|T>=-<frac|1|V>*<around*|(|-<frac|n*R*T|p<rsup|2>>|)>=-<frac|1|V>*<around*|(|-<frac|V|p>|)>>
    <vs> <math|<D><phantom|<kT>>=<frac|1|p>>
  </soln>

  <problem>For a gas with the simple equation of state

  <\equation*>
    V<m>=<frac|R*T|p>+B
  </equation*>

  (Eq. <reference|Vm=RT/p+B>), where <math|B> is the second virial
  coefficient (a function of <math|T>), find expressions for
  <math|\<alpha\>>, <math|<kT>>, and <math|<pd|U<m>|V|T>> in terms of
  <math|<dif>B/<dif>T> and other state functions. <soln|
  <math|<D>\<alpha\>=<frac|1|V<m>><Pd|V<m>|T|<space|-0.17em>p>=<frac|1|V<m>>*<around*|(|<frac|R|p>+<frac|<dif>B|<dif>T>|)>>
  <vs><math|<D><kT>=-<frac|1|V<m>><Pd|V<m>|p|T>=-<frac|1|V<m>>*<around*|(|-<frac|R*T|p<rsup|2>>|)>=<frac|R*T|V<m>p<rsup|2>>>
  <vs><math|<D><Pd|U<m>|V|T>=<frac|\<alpha\>*T|<kT>>-p=<frac|T|V<m>>*<around*|(|<frac|R|p>+<frac|<dif>B|<dif>T>|)><around*|(|<frac|V<m>p<rsup|2>|R*T>|)>-p>
  <vs><math|<D><phantom|<Pd|U<m>|V|T>>=<frac|p<rsup|2>|R><around*|(|<frac|<dif>B|<dif>T>|)>>>

  <problem>Show that when the virial equation
  <math|p*V<m>=R*T*<around|(|1+B<rsub|p>*p+C<rsub|p>*p<rsup|2>+\<cdots\>|)>>
  (Eq. <reference|pVm=RT(1+B_p p...)>) adequately represents the equation of
  state of a real gas, the Joule\UThomson coefficient is given by

  <\equation*>
    \<mu\><subs|J*T>=<frac|R*T<rsup|2>*<around|[|<dif>B<rsub|p>/<dif>T+<around|(|<dif>C<rsub|p>/<dif>T|)>*p+\<cdots\>|]>|<Cpm>>
  </equation*>

  Note that the limiting value at low pressure,
  <math|R*T<rsup|2>*<around|(|<dif>B<rsub|p>/<dif>T|)>/<Cpm>>, is not
  necessarily equal to zero even though the equation of state approaches that
  of an ideal gas in this limit. <soln| <math|<D>\<mu\><subs|J*T>=<frac|<D><around|(|\<alpha\>*T-1|)>*V<m>|<D><Cpm>>>
  <vs><math|<D>\<alpha\>*V<m>=<Pd|V<m>|T|p>=<frac|R|p>*<around|(|1+B<rsub|p>*p+C<rsub|p>*p<rsup|2>+\<cdots\>|)>+<frac|R*T|p>*<around*|[|<around*|(|<frac|<dif>B<rsub|p>|<dif>T>|)>*p+<around*|(|<frac|<dif>C<rsub|p>|<dif>T>|)>*p<rsup|2>+\<cdots\>|]>>
  <vs>Combine these expressions with the expression for <math|V<m>> to obtain
  the final expression for <math|\<mu\><subs|J*T>>.>

  <problem>The quantity <math|<pd|T|V|U>> is called the
  <I|Joule!coefficient\|p><em|Joule coefficient>. <I|Joule, James
  Prescott\|p>James Joule attempted to evaluate this quantity by measuring
  the temperature change accompanying the expansion of air into a vacuum\Vthe
  <I|Joule!experiment\|p>\PJoule experiment.\Q Write an expression for the
  total differential of <math|U> with <math|T> and <math|V> as independent
  variables, and by a procedure similar to that used in Sec. <reference|7-JK
  coeff> show that the Joule coefficient is equal to

  <\equation*>
    <frac|p-\<alpha\>*T/<kT>|C<rsub|V>>
  </equation*>

  <\soln>
    \ <math|<D><dif>U=<Pd|U|T|V><dif>T+<Pd|U|V|T><dif>V> <vs>Divide by
    <math|<dif>V> and impose a condition of constant <math|U>:
    <vs><math|<D>0=<Pd|U|T|V><Pd|T|V|U>+<Pd|U|V|T>> <vs>Solve for
    <math|<pd|T|V|U>> and make appropriate substitutions:
    <vs><math|<D><Pd|T|V|U>=-<frac|<pd|U|V|T>|<pd|U|T|V>>=-<frac|\<alpha\>*T/<kT>-p|C<rsub|V>>>
  </soln>

  <problem><label|prb:7-aniline> <math|p>\U<math|V>\U<math|T> data for
  several organic liquids were measured by Gibson and Loeffler.<footnote|Ref.
  <cite|gibson-39>.> The following formulas describe the results for aniline.

  <\itemize>
    <item>Molar volume as a function of temperature at <math|p=1<br>>
    (<math|298>--<math|358<K>>):

    <\equation*>
      V<m>=a+b*T+c*T<rsup|2>+d*T<rsup|3>
    </equation*>

    where the parameters have the values

    <alignat*|2|<tformat|<table|<row|<cell|a>|<cell|=69.287<units|cm<rsup|<math|3>>
    <space|0.17em>mol<per>>>|<cell|<space|2em>c>|<cell|=-1.0443<timesten|-4><units|cm<rsup|<math|3>>
    <space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>>>|<row|<cell|b>|<cell|=0.08852<units|cm<rsup|<math|3>>
    <space|0.17em>K<per><space|0.17em>mol<per>>>|<cell|<space|2em>d>|<cell|=1.940<timesten|-7><units|cm<rsup|<math|3>>
    <space|0.17em>K<rsup|<math|-3>>*<space|0.17em>mol<per>>>>>>>

    <item>Molar volume as a function of pressure at <math|T=298.15<K>>
    (<math|1>--<math|1000<br>>):

    <\equation*>
      V<m>=e-f*ln <around|(|g+p/<tx|b*a*r>|)>
    </equation*>

    where the parameter values are

    <\equation*>
      e=156.812<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>><space|2em>f=8.5834<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>><space|2em>g=2006.6
    </equation*>
  </itemize>

  \;

  <\problemparts>
    \ <problempartAns>Use these formulas to evaluate <math|\<alpha\>>,
    <math|<kT>>, <math|<pd|p|T|V>>, and <math|<pd|U|V|T>> (the internal
    pressure) for aniline at <math|T=298.15<K>> and <math|p=1.000<br>>.

    <answer|<math|\<alpha\>=8.519<timesten|-4><units|K<per>>><next-line><math|\<kappa\><rsub|t>=4.671<timesten|-5><units|b*a*r<per>>><next-line><math|<pd|p|T|V>=18.24<units|b*a*r*<space|0.17em>K<per>>><next-line><math|<pd|U|V|T>=5437<br>>>

    <soln| At <math|298.15<K>> and <math|1.000<br>>, the molar volume
    calculated from either formula is <math|V<m>=91.538<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>.
    <vs><math|<D>\<alpha\>=<frac|1|V<m>><Pd|V|T|p>=<frac|b+2*c*T+3*d*T<rsup|2>|V<m>>=8.519<timesten|-4><units|K<per>>>
    <vs><math|<D><kT>=-<frac|1|V<m>><Pd|V|p|T>=<frac|1|V<m>><around*|(|<frac|f|g+p/<tx|b*a*r>>|)>=4.671<timesten|-5><units|b*a*r<per>>>
    <vs><math|<D><Pd|p|T|V>=<frac|\<alpha\>|<kT>>=18.24<units|b*a*r*<space|0.17em>K<per>>>
    <vs><math|<D><Pd|U|V|T>=<frac|\<alpha\>*T|<kT>>-p=5437<br>>>
    <problempartAns>Estimate the pressure increase if the temperature of a
    fixed amount of aniline is increased by <math|0.10<K>> at constant
    volume.

    <answer|<math|<Del>p\<approx\>1.8<br>>>

    <soln| <math|<D><Del>p\<approx\><Pd|p|T|V><Del>T=<around|(|18.24<units|b*a*r*<space|0.17em>K<per>>|)><around|(|0.10<K>|)>=1.8<br>>>
  </problemparts>

  <problem>

  <\problemparts>
    \ <problempart>From the total differential of <math|H> with <math|T> and
    <math|p> as independent variables, derive the relation
    <math|<pd|<Cpm>|p|T>=-T<pd|<rsup|2>V<m>|T<rsup|2>|p>>. <soln|
    <math|<D><dif>H=<Pd|H|T|<space|-0.17em>p><dif>T+<Pd|H|p|T><difp>=C<rsub|p><dif>T+<around|(|1-\<alpha\>*T|)>*V<difp>>
    <vs>The reciprocity relation from this total differential is
    <vs><math|<D><Pd|C<rsub|p>|p|T>=<bPd|<around|(|1-\<alpha\>*T|)>*V|T|p>>
    <vs>or <vs><math|<D><Pd|C<rsub|p>|p|T>=<Pd|V|T|<space|-0.17em>p>-<bPd|<around|(|\<alpha\>*T*V|)>|T|p>>
    <vs>Using the relation <math|\<alpha\>*V=<pd|V|T|p>>, this becomes
    <vs><math|<D><Pd|C<rsub|p>|p|T>=\<alpha\>*V-\<alpha\>*V-T<bPd|<around|(|\<alpha\>*V|)>|T|<space|-0.17em>p>=-T<bPd|<around|(|\<alpha\>*V|)>|T|p>=-T<Pd|<rsup|2>V|T<rsup|2>|<space|-0.17em>p>>
    <vs>Divide by <math|n> to obtain the final relation.>
    <problempartAns>Evaluate <math|<pd|<Cpm>|p|T>> for liquid aniline at
    <math|300.0<K>> and <math|1<br>> using data in Prob.
    7.<reference|prb:7-aniline>.

    <answer|<math|<pd|<Cpm>|p|T>><next-line><math|=-4.210<timesten|-8><units|J*<space|0.17em>K<per><space|0.17em>P*a<per><space|0.17em>m*o*l<per>>>>

    <soln| <math|<D><Pd|<Cpm>|p|T>=-T<Pd|<rsup|2>V<m>|T<rsup|2>|<space|-0.17em>p>=-T*<around|(|2*<space|.8pt>c+6*<space|1pt>d*T|)>>
    <vs> <math|<D><phantom|<Pd|<Cpm>|p|T>>=-4.210<timesten|-2><units|c*m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>>
    <vs> <math|<D><phantom|<Pd|<Cpm>|p|T>>=-<around|(|4.210<timesten|-2><units|c*m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>|)>*<around|(|10<rsup|-2><units|m>/<tx|c*m>|)><rsup|3>>
    <vs> <math|<D><phantom|<Pd|<Cpm>|p|T>>=-4.210<timesten|-8><units|J*<space|0.17em>K<per><space|0.17em>P*a<per><space|0.17em>m*o*l<per>>>>
  </problemparts>

  <problem>

  <\problemparts>
    \ <problempart>From the total differential of <math|V> with <math|T> and
    <math|p> as independent variables, derive the relation
    <math|<pd|\<alpha\>|p|T>=-<pd|<kT>|T|p>>. <soln|
    <math|<dif>V=\<alpha\>*V<dif>T-<kT>V<difp>> (Eq.
    <reference|dV=alpha*VdT-kappaT*Vdp>)<next-line>Reciprocity relation:
    <vs><math|<D><bPd|<around|(|\<alpha\>*V|)>|p|T>=-<bPd|<around|(|<kT>V|)>|T|p>>
    <vs>From the chain rule: <vs><math|<D>\<alpha\><Pd|V|p|T>+V<Pd|\<alpha\>|p|T>=-<kT><Pd|V|T|<space|-0.17em>p>-V<Pd|<kT>|T|<space|-0.17em>p>>
    <vs>or <vs><math|<D>\<alpha\>*<around|(|-<kT>V|)>+V<Pd|\<alpha\>|<space|-0.17em>p|T>=-<kT><around|(|\<alpha\>*V|)>-V<Pd|<kT>|T|<space|-0.17em>p>>
    <vs>The first term on the left equals the first term on the right, so
    these terms cancel, giving <vs><math|<D><Pd|\<alpha\>|p|T>=-<Pd|<kT>|T|<space|-0.17em>p>>>
    <problempartAns>Use this relation to estimate the value of
    <math|\<alpha\>> for benzene at <math|25<units|<degC>>> and
    <math|500<br>>, given that the value of <math|\<alpha\>> is
    <math|1.2<timesten|-3><units|K<per>>> at <math|25<units|<degC>>> and
    <math|1<br>>. (Use information from Fig. <reference|fig:7-kappaT vs
    T><vpageref|fig:7-kappaT vs T>.)

    <answer|<math|8<timesten|-4><units|K<per>>>>

    <soln| From the curve for benzene in Fig. <reference|fig:7-kappaT vs T>,
    the value of <math|<pd|<kT>|T|p>> at <math|25<units|<degC>>> is
    approximately <math|7.3<timesten|-7><units|b*a*r<per><space|0.17em>K<per>>>.
    <vs><math|<D>\<alpha\><around|(|500<br>|)>\<approx\>\<alpha\><around|(|1<br>|)>+<Pd|\<alpha\>|p|T><around|(|500-1|)><tx|b*a*r>>
    <vs> <math|<D><phantom|\<alpha\><around|(|500<br>|)>>=1.2<timesten|-3><units|K<per>>+<around|(|-7.3<timesten|-7><units|b*a*r<per><space|0.17em>K<per>>|)><around|(|499<br>|)>>
    <vs> <math|<D><phantom|\<alpha\><around|(|500<br>|)>>=8<timesten|-4><units|K<per>>>>
  </problemparts>

  <problem>Certain equations of state supposed to be applicable to nonpolar
  liquids and gases are of the form <math|p=T*f<around|(|V<m>|)>-a/V<m><rsup|2>>,
  where <math|f<around|(|V<m>|)>> is a function of the molar volume only and
  <math|a> is a constant. <problemparts| <problempart>Show that the van der
  Waals equation of state <math|<around|(|p+a/V<m><rsup|2>|)>*<around|(|V<m>-b|)>=R*T>
  (where <math|a> and <math|b> are constants) is of this form. <soln| The van
  der Waals equation can be rearranged to
  <vs><math|<D>p=<frac|R*T|V<m>-b>-<frac|a|V<m><rsup|2>>> <vs>The function
  <math|f<around|(|V<m>|)>> is <math|R/<around|(|V<m>-b|)>>.>
  <problempart>Show that any fluid with an equation of state of this form has
  an internal pressure equal to <math|a/V<m><rsup|2>>. <soln| From Eq.
  <reference|dU/dV=Tdp/dT-p>, the internal pressure is equal to
  <math|T<pd|p|T|V>-p>. <vs><math|<D>T<Pd|p|T|V,n>-p=T*f<around|(|V<m>|)>-<around*|[|T*f<around|(|V<m>|)>-<frac|a|V<m><rsup|2>>|]>=<frac|a|V<m><rsup|2>>>>>

  <problem><label|prb:7-delH, delS formulas> Suppose that the molar heat
  capacity at constant pressure of a substance has a temperature dependence
  given by <math|<Cpm>=a+b*T+c*T<rsup|2>>, where <math|a>, <math|b>, and
  <math|c> are constants. Consider the heating of an amount <math|n> of the
  substance from <math|T<rsub|1>> to <math|T<rsub|2>> at constant pressure.
  Find expressions for <math|<Del>H> and <math|<Del>S> for this process in
  terms of <math|a>, <math|b>, <math|c>, <math|n>, <math|T<rsub|1>>, and
  <math|T<rsub|2>>. <soln| <math|<D><Del>H=n*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><Cpm><dif>T=n*<around*|[|a*<around|(|T<rsub|2>-T<rsub|1>|)>+<around*|(|<frac|b|2>|)>*<around|(|T<rsub|2><rsup|2>-T<rsub|1><rsup|2>|)>+<around*|(|<frac|c|3>|)>*<around|(|T<rsub|2><rsup|3>-T<rsub|1><rsup|3>|)>|]>>
  <vs><math|<D><Del>S=n*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|<Cpm>|T><dif>T=n*<around*|[|a*ln
  <around*|(|<frac|T<rsub|2>|T<rsub|1>>|)>+b*<around|(|T<rsub|2>-T<rsub|1>|)>+<around*|(|<frac|c|2>|)>*<around|(|T<rsub|2><rsup|2>-T<rsub|1><rsup|2>|)>|]>>>

  <problemAns><label|prb:7-aluminum> At <math|p=1<units|a*t*m>>, the molar
  heat capacity at constant pressure of aluminum is given by

  <\equation*>
    <Cpm>=a+b*T
  </equation*>

  where the constants have the values

  <\equation*>
    a=20.67<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>><space|2em>b=0.01238<units|J*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>
  </equation*>

  Calculate the quantity of electrical work needed to heat <math|2.000<mol>>
  of aluminum from <math|300.00<K>> to <math|400.00<K>> at
  <math|1<units|a*t*m>> in an adiabatic enclosure.

  <\answer>
    <math|5.001<timesten|3><units|J>>
  </answer>

  <\soln>
    \ Since the process is isobaric, <math|<Del>H> is equal to the heat that
    would be needed to cause the same change of state without electrical
    work: <vs><math|<D><Del>H=n*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em><Cpm><dif>T=n*<around*|[|a*<around|(|T<rsub|2>-T<rsub|1>|)>+<frac|b|2>*<around|(|T<rsub|2><rsup|2>-T<rsub|1><rsup|2>|)>|]>=5.001<timesten|3><units|J>>
    <vs>In the adiabatic enclosure, <math|<Del>H> has the same value, and
    electrical work substitutes for heat:
    <math|w<rprime|'>=5.001<timesten|3><units|J>>.
  </soln>

  <problemAns><label|prb:7-CO2 DelH, DelS> The temperature dependence of the
  standard molar heat capacity of gaseous carbon dioxide in the temperature
  range <math|298<K>>\U<math|2000<K>> is given by

  <\equation*>
    <Cpm><st>=a+b*T+<frac|c|T<rsup|2>>
  </equation*>

  where the constants have the values

  <\equation*>
    a=44.2<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>><space|1em>b=8.8<timesten|-3><units|J*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>><space|1em>c=-8.6<timesten|5><units|J*<space|0.17em>K*<space|0.17em>m*o*l<per>>
  </equation*>

  Calculate the enthalpy and entropy changes when one mole of
  CO<rsub|<math|2>> is heated at <math|1<br>> from <math|300.00<K>> to
  <math|800.00<K>>. You can assume that at this pressure <math|<Cpm>> is
  practically equal to <math|<Cpm><st>>.

  <\answer>
    <math|<Del>H=2.27<timesten|4><units|J>>,
    <math|<Del>S=43.6<units|J*<space|0.17em>K<per>>>
  </answer>

  <\soln>
    \ <math|<D><Del>H=n*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><Cpm><dif>T=n*<around*|[|a*<around|(|T<rsub|2>-T<rsub|1>|)>+<around*|(|<frac|b|2>|)>*<around|(|T<rsub|2><rsup|2>-T<rsub|1><rsup|2>|)>-c*<around*|(|<frac|1|T<rsub|2>>-<frac|1|T<rsub|1>>|)>|]>>
    <vs> <math|<D><phantom|<Del>H>=2.27<timesten|4><units|J>>
    <vs><math|<D><Del>S=n*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|<Cpm>|T><dif>T=n*<around*|[|a*ln
    <around*|(|<frac|T<rsub|2>|T<rsub|1>>|)>+b*<around|(|T<rsub|2>-T<rsub|1>|)>-<around*|(|<frac|c|2>|)>*<around*|(|<frac|1|T<rsub|2><rsup|2>>-<frac|1|T<rsub|1><rsup|2>>|)>|]>>
    <vs> <math|<D><phantom|<Del>S>=43.6<units|J*<space|0.17em>K<per>>>
  </soln>

  <problem>This problem concerns gaseous carbon dioxide. At <math|400<K>>,
  the relation between <math|p> and <math|V<m>> at pressures up to at least
  <math|100<br>> is given to good accuracy by a virial equation of state
  truncated at the second virial coefficient, <math|B>. In the temperature
  range <math|300<K>>\U<math|800<K>> the dependence of <math|B> on
  temperature is given by

  <\equation*>
    B=a<rprime|'>+b<rprime|'>*T+c<rprime|'>*T<rsup|2>+d<rprime|'>*T<rsup|3>
  </equation*>

  where the constants have the values

  <\equation*>
    <\eqsplit>
      <tformat|<table|<row|<cell|a<rprime|'>>|<cell|=-521<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>|<row|<cell|b*'>|<cell|=2.08<units|c*m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>>>|<row|<cell|c*'>|<cell|=-2.89<timesten|-3><units|c*m<rsup|<math|3>>*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>>>|<row|<cell|d*'>|<cell|=1.397<timesten|-6><units|c*m<rsup|<math|3>>*<space|0.17em>K<rsup|<math|-3>>*<space|0.17em>mol<per>>>>>>
    </eqsplit>
  </equation*>

  <\problemparts>
    \ <problempartAns>From information in Prob. 7.<reference|prb:7-CO2 DelH,
    DelS>, calculate the standard molar heat capacity at constant pressure,
    <math|<Cpm><st>>, at <math|T=400.0<K>>.

    <answer|<math|<Cpm><st>=42.3<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>

    <soln| <math|<Cpm><st>=a+b*T+c/T<rsup|2>>
    <vs><math|<phantom|<Cpm><st>>=44.2<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>+<around|(|8.8<timesten|-3><units|J*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>|)><around|(|400.0<K>|)>+<dfrac|-8.6<timesten|5><units|J*<space|0.17em>K*<space|0.17em>m*o*l<per>>|<around|(|400.0<K>|)><rsup|2>>>
    <vs><math|<phantom|<Cpm><st>>=<around|(|44.2+3.52-5.4|)><units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>=42.3<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>
    <problempartAns>Estimate the value of <math|<Cpm>> under the conditions
    <math|T=400.0<K>> and <math|p=100.0<br>>.

    <answer|<math|<Cpm>\<approx\>52.0<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>

    <\soln>
      \ <math|B=a<rprime|'>+b<rprime|'>*T+c<rprime|'>*T<rsup|2>+d<rprime|'>*T<rsup|3>>
      <vs><math|<dfrac|<dif>B|<dif>T>=b<rprime|'>+2*c<rprime|'>*T+3*d<rprime|'>*T<rsup|2>>
      <vs><math|<dfrac|<dif><rsup|2>B|<dif>T<rsup|2>>=2*c<rprime|'>+6*d<rprime|'>*T>
      <vs><math|<phantom|<dfrac|<dif><rsup|2>B|<dif>T<rsup|2>>>=2*<around|(|-2.89<timesten|-3><units|c*m<rsup|<math|3>>*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>|)><around*|(|<dfrac|10<rsup|-2><units|m>|1<units|c*m>>|)><rsup|3>>
      <vs><math|<phantom|<dfrac|<dif><rsup|2>B|<dif>T<rsup|2>>=>+6<around|(|1.397<timesten|-6><units|c*m<rsup|<math|3>>*<space|0.17em>K<rsup|<math|-3>>*<space|0.17em>mol<per>>|)><around*|(|<dfrac|10<rsup|-2><units|m>|1<units|c*m>>|)><rsup|3><around|(|400.0<K>|)>>
      <vs><math|<phantom|<dfrac|<dif><rsup|2>B|<dif>T<rsup|2>>>=<around|(|-5.78<timesten|-9>+3.35<timesten|-9>|)><units|m<rsup|<math|3>>*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>>
      <vs><math|<phantom|<dfrac|<dif><rsup|2>B|<dif>T<rsup|2>>>=-2.43<timesten|-9><units|m<rsup|<math|3>>*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>>
      <vs><math|<Cpm>-<Cpm><st>\<approx\>-p*T<dfrac|<dif><rsup|2>B|<dif>T<rsup|2>>>
      <vs><math|<phantom|<Cpm>-<Cpm><st>>=-<around|(|100.0<br>|)><around*|(|<dfrac|10<rsup|5><units|P*a>|1<br>>|)><around|(|400.0<K>|)>*<around|(|-2.43<timesten|-9><units|m<rsup|<math|3>>*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>|)>>
      <vs><math|<phantom|<Cpm>-<Cpm><st>>=9.7<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
      <vs><math|<Cpm>\<approx\><around|(|42.3+9.7|)><units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>=52.0<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    </soln>
  </problemparts>

  <problem>A chemist, needing to determine the specific heat capacity of a
  certain liquid but not having an electrically heated calorimeter at her
  disposal, used the following simple procedure known as
  <I|Calorimetry!drop\|p><em|drop calorimetry>. She placed
  <math|500.0<units|g>> of the liquid in a thermally insulated container
  equipped with a lid and a thermometer. After recording the initial
  temperature of the liquid, <math|24.80<units|<degC>>>, she removed a
  <math|60.17>-g block of aluminum metal from a boiling water bath at
  <math|100.00<units|<degC>>> and quickly immersed it in the liquid in the
  container. After the contents of the container had become thermally
  equilibrated, she recorded a final temperature of
  <math|27.92<units|<degC>>>. She calculated the specific heat capacity
  <math|C<rsub|p>/m> of the liquid from these data, making use of the molar
  mass of aluminum (<math|M=26.9815<units|g*<space|0.17em>m*o*l<per>>>) and
  the formula for the molar heat capacity of aluminum given in Prob.
  7.<reference|prb:7-aluminum>.

  <\problemparts>
    \ <problempartAns>From these data, find the specific heat capacity of the
    liquid under the assumption that its value does not vary with
    temperature. Hint: Treat the temperature equilibration process as
    adiabatic and isobaric (<math|<Del>H=0>), and equate <math|<Del>H> to the
    sum of the enthalpy changes in the two phases.

    <answer|<math|2.56<units|J*<space|0.17em>K<per><space|0.17em>g<per>>>>

    <soln| Enthalpy change of the aluminum block:
    <vs><math|<D><Del>H<around|(|<tx|A*l>|)>=n*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em><Cpm><around|(|<tx|A*l>|)><dif>T=<frac|m|M>*<around*|[|a*<around|(|T<rsub|2>-T<rsub|1>|)>+<frac|b|2>*<around|(|T<rsub|2><rsup|2>-T<rsub|1><rsup|2>|)>|]>>
    <vs> <math|<D><phantom|<Del>H>=<frac|60.17<units|g>|26.9815<units|g*<space|0.17em>m*o*l<per>>><around*|{|<around*|(|20.67<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>)><around|(|301.07<K>-373.15<K>|)><vphantom|<frac|1|2>>|\<nobracket\>>|\<nobracket\>>>
    <vs> <math|<D><phantom|<Del>H=><around*|\<nobracket\>|+<frac|1|2><around|(|0.01238<units|J*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>|)>*<around*|[|<around|(|301.07<K>|)><rsup|2>-<around|(|373.15<K>|)><rsup|2>|]>|}>>
    <vs> <math|<D><phantom|<Del>H>=-3.993<timesten|3><units|J>> <vs>Assume
    that <math|C<rsub|p>> for the liquid is equal to
    <math|<Del>H<liquid>/<Del>T>, and equate <math|<Del>H<liquid>> to
    <math|-<Del>H<around|(|<tx|A*l>|)>>: <vs><math|<D>C<rsub|p><liquid>=<frac|<Del>H<liquid>|<Del>T>=<frac|3.993<timesten|3><units|J>|301.07<K>-297.95<K>>=1.280<timesten|3><units|J*<space|0.17em>K<per>>>
    <vs>Specific heat capacity: <vs><math|<D><frac|C<rsub|p><liquid>|m>=<frac|1.280<timesten|3><units|J*<space|0.17em>K<per>>|500.0<units|g>>=2.56<units|J*<space|0.17em>K<per><space|0.17em>g<per>>>>
    <problempart>Show that the value obtained in part (a) is actually an
    average value of <math|C<rsub|p>/m> over the temperature range between
    the initial and final temperatures of the liquid given by

    <\equation*>
      <frac|<D><big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><around|(|C<rsub|p>/m|)><dif>T|T<rsub|2>-T<rsub|1>>
    </equation*>

    <soln| The value in part (a) was calculated from
    <math|<Del>H<liquid>/<around|(|T<rsub|2>-T<rsub|1>|)>*m>. Make the
    substitution <vs><math|<D><Del>H<liquid>=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>>C<rsub|p><dif>T>
    <vs>and rearrange.>
  </problemparts>

  <problem>Suppose a gas has the virial equation of state
  <math|p*V<m>=R*T*<around|(|1+B<rsub|p>*p+C<rsub|p>*p<rsup|2>|)>>, where
  <math|B<rsub|p>> and <math|C<rsub|p>> depend only on <math|T>, and higher
  powers of <math|p> can be ignored.

  <\problemparts>
    \ <problempart>Derive an expression for the fugacity coefficient,
    <math|\<phi\>>, of this gas as a function of <math|p>. <soln| <math|<D>ln
    \<phi\><around|(|p<rprime|'>|)>=<big|int><rsub|0><rsup|p<rprime|'>><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)><difp>=<big|int><rsub|0><rsup|p<rprime|'>><around*|(|<frac|1+B<rsub|p>*p+C<rsub|p>*p<rsup|2>|p>-<frac|1|p>|)><difp>>
    <vs> <math|<D><phantom|ln \<phi\><around|(|p<rprime|'>|)>>=<big|int><rsub|0><rsup|p<rprime|'>><around|(|B<rsub|p>+C<rsub|p>*p|)><difp>>
    <vs> <math|<D><phantom|ln \<phi\><around|(|p<rprime|'>|)>>=B<rsub|p>*p<rprime|'>+<frac|1|2>*C<rsub|p><around|(|p<rprime|'>|)><rsup|2>>
    <vs> <math|ln \<phi\>=B<rsub|p>*p+<frac|1|2>*C<rsub|p>*p<rsup|2>>>
    <problempartAns>For CO<rsub|<math|2>>(g) at <math|0.00<units|<degC>>>,
    the virial coefficients have the values
    <math|B<rsub|p>=-6.67<timesten|-3><units|b*a*r<per>>> and
    <math|C<rsub|p>=-3.4<timesten|-5><units|b*a*r<rsup|<math|-2>>>>. Evaluate
    the fugacity <math|<fug>> at <math|0.00<units|<degC>>> and
    <math|p=20.0<br>>.

    <answer|<math|<fug>=17.4<br>>>

    <soln| <math|<D>ln \<phi\>=B<rsub|p>*p+<frac|1|2>*C<rsub|p>*p<rsup|2>>
    <vs> <math|<D><phantom|ln \<phi\>>=<around|(|-6.67<timesten|-3><units|b*a*r<per>>|)><around|(|20.0<br>|)>+<frac|1|2>*<around|(|-3.4<timesten|-5><units|b*a*r<rsup|<math|-2>>>|)><around|(|20.0<br>|)><rsup|2>>
    <vs> <math|<D><phantom|ln \<phi\>>=-0.140>
    <vs><math|<D><fug>=\<phi\>*p=<around|(|0.869|)><around|(|20.0<br>|)>=17.4<br>>>
  </problemparts>

  <\big-table>
    <minipagetable|12cm|}<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|5|5|cell-col-span|1>|<cwith|1|1|5|5|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|7|7|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|p/10<rsup|5><Pa>>>|<cell|<ccol|<math|V<m>/10<rsup|-3><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<cell|<space|2em>>|<cell|<ccol|<math|p/10<rsup|5><Pa>>>>|<cell|<math|V<m>/10<rsup|-3><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|1>|<cell|55.896>|<cell|>|<cell|100>|<cell|0.47575>>|<row|<cell|10>|<cell|5.5231>|<cell|>|<cell|120>|<cell|0.37976>>|<row|<cell|20>|<cell|2.7237>|<cell|>|<cell|140>|<cell|0.31020>>|<row|<cell|40>|<cell|1.3224>|<cell|>|<cell|160>|<cell|0.25699>>|<row|<cell|60>|<cell|0.85374>|<cell|>|<cell|180>|<cell|0.21447>>|<row|<cell|80>|<cell|0.61817>|<cell|>|<cell|200>|<cell|0.17918>>>>>>
  </big-table|Molar volume of H<rsub|<math|2>>O(g) at
  <math|400.00<units|<degC>>><space|.15em><footnote|based on data in Ref.
  <cite|grigull-90>>>

  <problem><label|prb:7-H2O fug> Table <reference|tbl:7-H2O(g)><vpageref|tbl:7-H2O(g)>
  lists values of the molar volume of gaseous H<rsub|<math|2>>O at
  <math|400.00<units|<degC>>> and 12 pressures.

  <\problemparts>
    \ <problempartAns>Evaluate the fugacity coefficient and fugacity of
    H<rsub|<math|2>>O(g) at <math|400.00<units|<degC>>> and <math|200<br>>.

    <answer|<math|\<phi\>=0.739>, <math|<fug>=148<br>>>

    <\soln>
      \ Calculate <math|<around|(|V<m>/R*T-1/p|)>> at each pressure; see
      Table <reference|data for H2O fug><vpageref|data for H2O fug>.

      <\big-table>
        <minipagetable|12cm|<label|data for H2O
        fug><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|5|5|cell-col-span|1>|<cwith|1|1|5|5|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|7|7|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|p/10<rsup|5><Pa>>>|<cell|<ccol|<math|<around*|(|<frac|<D>V<m>|<D>R*T>-<frac|<D>1|<D>p>|)>/10<rsup|-8><units|P*a<per>>>>>|<cell|<space|2em>>|<cell|<ccol|<math|p/10<rsup|5><Pa>>>>|<cell|<math|<around*|(|<frac|<D>V<m>|<D>R*T>-<frac|<D>1|<D>p>|)>/10<rsup|-8><units|P*a<per>>>>>|<row|<cell|1>|<cell|-1.302>|<cell|>|<cell|100>|<cell|-1.4997>>|<row|<cell|10>|<cell|-1.318>|<cell|>|<cell|120>|<cell|-1.5481>>|<row|<cell|20>|<cell|-1.335>|<cell|>|<cell|140>|<cell|-1.6005>>|<row|<cell|40>|<cell|-1.374>|<cell|>|<cell|160>|<cell|-1.6583>>|<row|<cell|60>|<cell|-1.413>|<cell|>|<cell|180>|<cell|-1.7236>>|<row|<cell|80>|<cell|-1.455>|<cell|>|<cell|200>|<cell|-1.7986>>>>>>
      </big-table|>

      The values are plotted in Fig. <reference|plot for H2O
      fug><vpageref|plot for H2O fug>.

      <\big-figure>
        <boxedfigure|<image|./07-SUP/H2O-gas.eps||||><capt|<label|plot for
        H2O fug>>>
      </big-figure|>

      Extrapolate these values to <math|p=0>:
      <vs><math|<D>lim<rsub|p<ra>0><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)>=-1.300<timesten|-8><units|P*a<per>>>
      <vs>Use numerical integration to find the area under the curve:
      <vs><math|<D>ln \<phi\>=<big|int><rsub|0><rsup|200<br>><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)><difp>=-0.3031>
      <vs><math|<D>\<phi\>=0.739<space|2em><fug>=\<phi\>*p=<around|(|0.739|)><around|(|200<br>|)>=148<br>>
    </soln>

    \ <problempartAns>Show that the second virial coefficient <math|B> in the
    virial equation of state, <math|p*V<m>=R*T*<around|(|1+B/V<m>+C/V<m><rsup|2>+\<cdots\>|)>>,
    is given by

    <\equation*>
      B=R*T*lim<rsub|p<ra>0><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)>
    </equation*>

    where the limit is taken at constant <math|T>. Then evaluate <math|B> for
    H<rsub|<math|2>>O(g) at <math|400.00<units|<degC>>>.

    <answer|<math|B=-7.28<timesten|-5><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>>

    <soln| <math|<D>p*V<m>=R*T*<around|(|1+B/V<m>+C/V<m><rsup|2>+\<cdots\>|)>>
    <vs><math|<D><frac|V<m>|R*T>-<frac|1|p>=<around*|(|<frac|1|p>|)>*<around|(|1+B/V<m>+C/V<m><rsup|2>+\<cdots\>|)>-<frac|1|p>=<around*|(|<frac|1|p>|)>*<around|(|B/V<m>+C/V<m><rsup|2>+\<cdots\>|)>>
    <vs> <math|<D><phantom|<frac|V<m>|R*T>-<frac|1|p>>=<frac|B/V<m>+C/V<m><rsup|2>+\<cdots\>|<around|(|R*T/V<m>|)>*<around|(|1+B/V<m>+C/V<m><rsup|2>+\<cdots\>|)>>>
    <vs> <math|<D><phantom|<frac|V<m>|R*T>-<frac|1|p>>=<frac|1|R*T><around*|(|<frac|B+C/V<m>+\<cdots\>|1+B/V<m>+C/V<m><rsup|2>+\<cdots\>)>|)>>
    <vs>As <math|p> approaches 0, <math|1/V<m>> also approaches zero and the
    preceding expression approaches <math|B/R*T>. Thus, <math|B> is the
    product of <math|R*T> and the limiting value of the
    expression.<next-line>From Prob. 7.<reference|prb:7-H2O fug>(a), the
    value of <math|B> for H<rsub|<math|2>>O(g) at <math|400.00<units|<degC>>>
    is <vs><math|<D>B=<around|(|<R>|)><around|(|673.15<K>|)>*<around|(|-1.300<timesten|-8><units|P*a<per>>|)>>
    <vs> <math|<D><phantom|B>=-7.28<timesten|-5><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>>
  </problemparts>

  \;
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>