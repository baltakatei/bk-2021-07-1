<TeXmacs|1.99.20>

<style|<tuple|book|std-latex>>

<\body>
  <label|Chap4><problem>Explain why an electric refrigerator, which transfers
  energy by means of heat from the cold food storage compartment to the
  warmer air in the room, is not an impossible \PClausius device.\Q <soln| In
  addition to heat transfer, there is consumption of electrical energy from
  the surroundings. The refrigerator does not fit the description of the
  device declared by the Clausius statement of the second law to be
  impossible; such a device would produce no other effect but the transfer of
  energy by means of heat from a cooler to a warmer body.>

  <problem>A system consisting of a fixed amount of an ideal gas is
  maintained in thermal equilibrium with a heat reservoir at temperature
  <math|T>. The system is subjected to the following isothermal cycle:

  <\enumerate>
    <item>The gas, initially in an equilibrium state with volume
    <math|V<rsub|0>>, is allowed to expand into a vacuum and reach a new
    equilibrium state of volume <math|V<rprime|'>>.

    <item>The gas is reversibly compressed from <math|V<rprime|'>> to
    <math|V<rsub|0>>.
  </enumerate>

  For this cycle, find expressions or values for <math|w>,
  <math|<big|oint><dq>/T>, and <math|<big|oint><dif>S>. <soln| Step 1:
  <math|w=0> because it is a free expansion; <math|<Del>U=0> because it is an
  ideal gas and isothermal; therefore <math|q=<Del>U-w=0>.<next-line>Step 2:
  <math|w=-n*R*T*ln <around|(|V<rsub|0>/V<rprime|'>|)>> (Eq.
  <reference|w=-nRT ln(V2/V1)>); <math|<Del>U=0> because it is an ideal gas
  and isothermal; therefore <math|q=<Del>U-w=n*R*T*ln
  <around|(|V<rsub|0>/V<rprime|'>|)>>.<next-line>Overall: <math|w=-n*R*T*ln
  <around|(|V<rsub|0>/V<rprime|'>|)>>; <math|<big|oint><dq>/T=q/T=n*R*ln
  <around|(|V<rsub|0>/V<rprime|'>|)>>; <math|<big|oint><dif>S=0> because
  <math|S> is a state function. Note that <math|<big|oint><dif>S> is greater
  than <math|<big|oint><dq>/T> because of the irreversible expansion step, in
  agreement with the mathematical statement of the second law.>

  <problem>In an irreversible isothermal process of a closed system:
  <problemparts| <problempart>Is it possible for <math|<Del>S> to be
  negative? <soln| Yes. Provided <math|<Del>S> is less negative than
  <math|q/T>, there is no violation of the second law.> <problempart>Is it
  possible for <math|<Del>S> to be less than <math|q/T>? <soln| According to
  the second law, no.>>

  <problemAns>Suppose you have two blocks of copper, each of heat capacity
  <math|C<rsub|V>=200.0<units|J*<space|0.17em>K<per>>>. Initially one block
  has a uniform temperature of <math|300.00<K>> and the other
  <math|310.00<K>>. Calculate the entropy change that occurs when you place
  the two blocks in thermal contact with one another and surround them with
  perfect thermal insulation. Is the sign of <math|<Del>S> consistent with
  the second law? (Assume the process occurs at constant volume.)

  <\answer>
    <math|<Del>S=0.054<units|J*<space|0.17em>K<per>>>
  </answer>

  <\soln>
    \ Since the blocks have equal heat capacities, a given quantity of heat
    transfer from the warmer to the cooler block causes temperature changes
    that are equal in magnitude and of opposite signs. The final equilibrium
    temperature is <math|305.00<K>>, the average of the initial values.
    <vs>When the temperature of one of the blocks changes reversibly from
    <math|T<rsub|1>> to <math|T<rsub|2>>, the entropy change is
    <vs><math|<D><Del>S=<big|int><space|-0.17em><frac|<dq>|T>=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em><frac|C<rsub|V><dif>T|T>=C<rsub|V>*ln
    <frac|T<rsub|2>|T<rsub|1>>> <vs>Cooler block:
    <math|<D><Del>S=200.0<units|J*<space|0.17em>K<per>>ln
    <frac|305.00<K>|300.00<K>>=3.306<units|J*<space|0.17em>K<per>>>
    <vs>Warmer block: <math|<D><Del>S=200.0<units|J*<space|0.17em>K<per>>ln
    <frac|305.00<K>|310.00<K>>=-3.252<units|J*<space|0.17em>K<per>>>
    <vs>Total entropy change: <math|<Del>S=3.306<units|J*<space|0.17em>K<per>>-3.252<units|J*<space|0.17em>K<per>>=0.054<units|J*<space|0.17em>K<per>>>
    <vs>The sign of <math|<Del>S> is positive as predicted by the second law
    for an irreversible process in an isolated system.
  </soln>

  <problemAns>Refer to the apparatus shown in Figs. <reference|fig:3-porous
  plug><vpageref|fig:3-porous plug> and <reference|fig:3-evacuated
  vessel><vpageref|fig:3-evacuated vessel> and described in Probs.
  3.<reference|prb:3-porous plug> and 3.<reference|prb:3-evacuated vessel>.
  For both systems, evaluate <math|<Del>S> for the process that results from
  opening the stopcock. Also evaluate <math|<big|int><space|-0.17em><dq>/T<subs|e*x*t>>
  for both processes (for the apparatus in Fig. <reference|fig:3-evacuated
  vessel>, assume the vessels have adiabatic walls). Are your results
  consistent with the mathematical statement of the second law?

  <\answer>
    <math|<Del>S=549<units|J*<space|0.17em>K<per>>> for both processes;
    <math|<big|int><space|-0.17em><dq>/T<subs|e*x*t>=333<units|J*<space|0.17em>K<per>>>
    and <math|0>.
  </answer>

  <\soln>
    \ The initial states of the ideal gas are the same in both processes, and
    the final states are also the same. Therefore the value of <math|<Del>S>
    is the same for both processes. Calculate <math|<Del>S> for a
    <em|reversible> isothermal expansion of the ideal gas from the initial to
    the final volume: <vs><math|<D><Del>S=<frac|q|T>=<frac|-w|T>=n*R*ln
    <around*|(|<frac|V<rsub|2>|V<rsub|1>>|)>=<frac|p<rsub|1>*V<rsub|1>|T>*ln
    <around*|(|<frac|V<rsub|2>|V<rsub|1>>|)>> <vs>
    <math|<D><phantom|<Del>S>=<frac|<around|(|3.00<timesten|5><Pa>|)><around|(|0.500<units|m<rsup|<math|3>>>|)>|300.<K>>*ln
    <around*|(|<frac|1.50<units|m<rsup|<math|3>>>|0.500<units|m<rsup|<math|3>>>>|)>>
    <vs> <math|<D><phantom|<Del>S>=549<units|J*<space|0.17em>K<per>>> <vs>Use
    the values of <math|q> found in Probs. 3.<reference|prb:3-porous plug>
    and 3.<reference|prb:3-evacuated vessel> to evaluate
    <math|<big|int><space|-0.17em><dq>/T<subs|e*x*t>>:<next-line>For the
    expansion through the porous plug, <math|<big|int><space|-0.17em><dq>/T<subs|e*x*t>=q/T=1.00<timesten|5><units|J>/300.<K>=333<units|J*<space|0.17em>K<per>>>.<next-line>For
    the expansion into the evacuated vessel,
    <math|<big|int><space|-0.17em><dq>/T<subs|e*x*t>=q/T=0>.<next-line>In
    both processes <math|<Del>S> is greater than
    <math|<big|int><space|-0.17em><dq>/T<subs|e*x*t>>, consistent with the
    second law.
  </soln>

  <\big-figure>
    <boxedfigure|<image|./04-SUP/paddle_problem.eps||||><capt|<label|fig:4-paddle_problem>>>
  </big-figure|>

  <problem>Figure <reference|fig:4-paddle_problem><vpageref|fig:4-paddle<rsub|p>roblem>
  shows the walls of a rigid thermally-insulated box (cross hatching). The
  <em|system> is the contents of this box. In the box is a paddle wheel
  immersed in a container of water, connected by a cord and pulley to a
  weight of mass <math|m>. The weight rests on a stop located a distance
  <math|h> above the bottom of the box. Assume the heat capacity of the
  system, <math|C<rsub|V>>, is independent of temperature. Initially the
  system is in an equilibrium state at temperature <math|T<rsub|1>>. When the
  stop is removed, the weight irreversibly sinks to the bottom of the box,
  causing the paddle wheel to rotate in the water. Eventually the system
  reaches a final equilibrium state with thermal equilibrium. Describe a
  <em|reversible> process with the same entropy change as this irreversible
  process, and derive a formula for <math|<Del>S> in terms of <math|m>,
  <math|h>, <math|C<rsub|V>>, and <math|T<rsub|1>>. <soln| When the stop is
  removed, the system is an isolated system of constant internal energy. To
  reversibly change the system between the same initial and final states as
  the irreversible process, reversibly lower the weight with gravitational
  work <math|w=-m*g*h>, then let reversible heat
  <math|q=C<rsub|V>*<around|(|T<rsub|2>-T<rsub|1>|)>> enter the system. Since
  <math|<Del>U> is zero, the sum of <math|q> and <math|w> must be zero:
  <vs><math|<D>C<rsub|V>*<around|(|T<rsub|2>-T<rsub|1>|)>-m*g*h=0*<space|2em>T<rsub|2>=T<rsub|1>+<frac|m*g*h|C<rsub|V>>>
  <vs>The entropy change of the reversible process is
  <vs><math|<D><Del>S=<big|int><space|-0.17em><frac|<dq>|T<bd>>=C<rsub|V>*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|<dif>T|T>=C<rsub|V>*ln
  <frac|T<rsub|2>|T<rsub|1>>=C<rsub|V>*ln
  <around*|(|1+<frac|m*g*h|C<rsub|V>*T<rsub|1>>|)>> <vs>This is also the
  entropy change of the irreversible process.>
</body>

<\initial>
  <\collection>
    <associate|global-title|04-problems.tm>
    <associate|info-flag|detailed>
    <associate|preamble|true>
  </collection>
</initial>