<TeXmacs|1.99.20>

<style|<tuple|generic|std-latex>>

<\body>
  <\hide-preamble>
    <assign|C|<macro|\<bbb-C\>>>

    <assign|R|<macro|\<bbb-R\>>>
  </hide-preamble>

  <label|Chap11><problemAns>Use values of <math|<Delsub|f>H<st>> and
  <math|<Delsub|f>G<st>> in Appendix <reference|app:props> to evaluate the
  standard molar reaction enthalpy and the thermodynamic equilibrium constant
  at <math|298.15<K>> for the oxidation of nitrogen to form aqueous nitric
  acid:

  <\equation*>
    <chem><frac|1|2>*N<rsub|2><around|(|g|)>+<frac|5|4>*O<rsub|2><around|(|g|)>+<frac|1|2>*H<rsub|2>*O<around|(|l|)><arrow>H<rsup|+>*<around|(|a*q|)>+N*O<rsub|3><rsup|-><around|(|a*q|)>
  </equation*>

  <\answer>
    <math|<Delsub|r>H<st>=-63.94<units|k*J*<space|0.17em>m*o*l<per>>><next-line><math|K=4.41<timesten|-2>>
  </answer>

  <\soln>
    \ The formation values of the elements and H<rsup|<math|+>> ion are zero.
    <vs> <math|<Delsub|r>H<st>=-<frac|1|2><Delsub|f>H<st><around|(|<tx|H<rsub|<math|2>>*O>|)>+<Delsub|f>H<st><around|(|<tx|N*O<math|<rsub|3><rsup|->>>|)>>
    <vs> <math|<phantom|<Delsub|r>H<st>>=-<frac|1|2>*<around|(|-285.830<units|k*J*<space|0.17em>m*o*l<per>>|)>+<around|(|-206.85<units|k*J*<space|0.17em>m*o*l<per>>|)>=-63.94<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs> <math|<Delsub|r>G<st>=-<frac|1|2><Delsub|f>G<st><around|(|<tx|H<rsub|<math|2>>*O>|)>+<Delsub|f>G<st><around|(|<tx|N*O<math|<rsub|3><rsup|->>>|)>>
    <vs> <math|<phantom|<Delsub|r>G<st>>=-<frac|1|2>*<around|(|-237.16<units|k*J*<space|0.17em>m*o*l<per>>|)>+<around|(|-110.84<units|k*J*<space|0.17em>m*o*l<per>>|)>=7.74<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs> <math|<D>K=exp <space|0.17em><around|(|-<Delsub|r>G<st>/R*T|)>=exp
    <around*|[|<frac|-7.74<timesten|3><units|J*<space|0.17em>m*o*l<per>>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>=4.41<timesten|-2>>
  </soln>

  <problem>In 1982, the International Union of Pure and Applied Chemistry
  recommended that the value of the <I|Standard!pressure\|p><I|Pressure!standard\|p>standard
  pressure <math|p<st>> be changed from <math|1<units|a*t*m>> to
  <math|1<br>>. This change affects the values of some standard molar
  quantities of a substance calculated from experimental data.

  <\problemparts>
    \ <problempart>Find the changes in <math|H<m><st>>, <math|S<m><st>>, and
    <math|G<m><st>> for a gaseous substance when the standard pressure is
    changed isothermally from <math|1.01325<br>> (<math|1<units|a*t*m>>) to
    exactly <math|1<br>>. (Such a small pressure change has an entirely
    negligible effect on these quantities for a substance in a condensed
    phase.) <soln| The standard state of a gaseous substance is the pure gas
    acting ideally. From expressions in Table <reference|tbl:7-isothermal p
    change>, which apply to isothermal changes of pressure of an ideal gas,
    we have <vs> <math|<D><Del>H<m>=<frac|<Del>H|n>=0> <vs>
    <math|<D><Del>S<m>=<frac|<Del>S|n>=-R*ln
    <frac|p<rsub|2>|p<rsub|1>>=-<around|(|<R>|)>*ln <frac|1<br>|1.01325<br>>>
    <vs> <math|<phantom|<Del>S<m>>=0.109<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs> <math|<D><Del>G<m>=<frac|<Del>G|n>=R*T*ln
    <frac|p<rsub|2>|p<rsub|1>>=<around|(|<R>|)>*T*ln
    <frac|1<br>|1.01325<br>>> <vs> <math|<phantom|<Del>G<m>>=-<around|(|0.109<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>|)>*T><next-line>>
    <problempartAns>What are the values of the corrections that need to be
    made to the standard molar enthalpy of formation, the standard molar
    entropy of formation, and the standard molar Gibbs energy of formation of
    N<rsub|<math|2>>O<rsub|<math|4>>(g) at <math|298.15<K>> when the standard
    pressure is changed from <math|1.01325<br>> to <math|1<br>>?

    <answer|<math|<Delsub|f>H<st>>: no change<next-line><math|<Delsub|f>S<st>>:
    subtract <math|0.219<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>><next-line><math|<Delsub|f>G<st>>:
    add <math|65<units|J*<space|0.17em>m*o*l<per>>>>

    <soln| The formation reaction is <vs><math|<tx|N<rsub|<math|2>><around|(|g|)>>+2<tx|O<rsub|<math|2>><around|(|g|)>><ra><tx|N<rsub|<math|2>>*O<rsub|<math|4>><around|(|g|)>><space|2em><big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>=-2>
    <vs>for which the standard molar enthalpy of formation is given by <vs>
    <math|<Delsub|f>H<st><around|(|<tx|N<rsub|<math|2>>*O<rsub|<math|4>>>|)>=-H<m><st><around|(|<tx|N<rsub|<math|2>>>|)>-2*H<m><st><around|(|<tx|O<rsub|<math|2>>>|)>+H<m><st><around|(|<tx|N<rsub|<math|2>>*O<rsub|<math|4>>>|)>>
    <vs>with analogous formulas for <math|<Delsub|f>S<st>> and
    <math|<Delsub|f>G<st>>. From the formulas in part (a), no correction is
    needed for <math|<Delsub|f>H<st>>. <vs>The correction for
    <math|<Delsub|f>S<st>> is <math|<around|(|-2|)><around|(|0.109<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>|)>=-0.219<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs>The correction for <math|<Delsub|f>G<st>> is
    <math|<around|(|-2|)>*<around|(|-0.109<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>|)><around|(|298.15<K>|)>=65<units|J*<space|0.17em>m*o*l<per>>>>
  </problemparts>

  <problemAns>From data for mercury listed in Appendix <reference|app:props>,
  calculate the saturation vapor pressure of liquid mercury at both
  <math|298.15<K>> and <math|273.15<K>>. You may need to make some reasonable
  approximations.

  <\answer>
    <math|p<around|(|298.15<K>|)>=2.6<timesten|-6><br>><next-line><math|p<around|(|273.15<K>|)>=2.7<timesten|-7><br>>
  </answer>

  <\soln>
    \ The reference state of mercury at <math|298.15<K>> is the liquid; thus,
    the formation reaction of gaseous mercury is the vaporization process:
    <math|<tx|H*g<around|(|l|)>><ra><tx|H*g<around|(|g|)>>>. Values from
    Appendix <reference|app:props> for Hg(g) at <math|298.15<K>>: <vs>
    <math|<Delsub|f>H<st>=61.38<units|k*J*<space|0.17em>m*o*l<per>><space|2em><Delsub|f>G<st>=31.84<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs>Since the vapor pressure is low, assume that the mercury vapor is an
    ideal gas. Assume that the molar enthalpy and Gibbs energy of the liquid
    at low pressure is the same as at the standard pressure. Find the vapor
    pressure at <math|298.15<K>> from the equilibrium constant of the
    formation reaction at this temperature: <vs>
    <math|<D>K=<frac|p|p<st>>=exp <around*|(|<frac|-<Delsub|r>G<st>|R*T>|)>=exp
    <around*|[|<frac|-31.84<timesten|3><units|J*<space|0.17em>m*o*l<per>>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>=2.6<timesten|-6>>
    <vs> <math|p=2.6<timesten|-6>p<st>=2.6<timesten|-6><br>> <vs>Find the
    vapor pressure at <math|273.15<K>> from the Clausius--Clapeyron equation:
    <vs> <math|<D>ln <frac|p<rsub|2>|p<rsub|1>>=-<frac|<Delsub|v*a*p>H|R>*<around*|(|<frac|1|T<rsub|2>>-<frac|1|T<rsub|1>>|)>=-<frac|61.38<timesten|3><units|J*<space|0.17em>m*o*l<per>>|<R>>*<around*|(|<frac|1|273.15<K>>-<frac|1|298.15<K>>|)>>
    <vs> <math|<phantom|ln <frac|p<rsub|2>|p<rsub|1>>>=-2.2662> <vs>
    <math|p<rsub|2>/p<rsub|1>=0.1037*<space|2em>p<rsub|2>=<around|(|0.1037|)><around|(|2.6<timesten|-6><br>|)>=2.7<timesten|-7><br>>
  </soln>

  <problem>Given the following experimental values at <math|T=298.15<K>>,
  <math|p=1<br>>:

  <wide-tabular|<tformat|<table|<row|<cell|>|<cell|<tx|H<rsup|<math|+>><around|(|aq|)>>+<tx|OH<rsup|<math|->><around|(|aq|)>>
  <arrow><tx|H<rsub|<math|2>>*O<around|(|l|)>>>|<cell|>|<cell|<Delsub|r>H<st>=-55.82<units|kJ
  <space|0.17em>mol<per>>>>|<row|<cell|>|<cell|<tx|Na<around|(|s|)>>+<tx|H<rsub|<math|2>>*O<around|(|l|)>>
  <arrow><tx|Na<rsup|<math|+>>(aq>)+<tx|OH<rsup|<math|->><around|(|aq|)>>+<with|math-display|false|<tformat|<table|<row|<cell|<frac|1|2><tx|H<rsub|<math|2>><around|(|g|)>>>|<cell|<space|2em>>|<cell|<Delsub|r>H<st>=-184.52<units|kJ
  <space|0.17em>mol<per>>>|<cell|>>|<row|<cell|>|<cell|<tx|NaOH<around|(|s|)>>
  <arrow><tx|NaOH<around|(|aq|)>>>|<cell|>|<cell|<Delsub|sol>H<rsup|\<infty\>>=-44.75<units|kJ
  <space|0.17em>mol<per>>>>|<row|<cell|>|<cell|<tx|NaOH in 5
  H<rsub|<math|2>>*O> <arrow><tx|NaOH in <math|\<infty\>>
  H<rsub|<math|2>>*O>>|<cell|>|<cell|<Del>H<m><dil>=-4.93<units|kJ
  <space|0.17em>mol<per>>>>|<row|<cell|>|<cell|<tx|NaOH<around|(|s|)>>>|<cell|>|<cell|<Delsub|f>H<st>=-425.61<units|kJ
  <space|0.17em>mol<per>>>>>>>>|<cell|>|<cell|>>>>>

  Using only these values, calculate:

  <\problemparts>
    \ <problempartAns><math|<Delsub|f>H<st>> for Na<rsup|<math|+>>(aq),
    NaOH(aq), and OH<rsup|<math|->>(aq);

    <answer|<math|-240.34<units|k*J*<space|0.17em>m*o*l<per>>>,
    <math|-470.36<units|k*J*<space|0.17em>m*o*l<per>>>,
    <math|-230.02<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| Add the first two reaction equations:
    <vs><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|l>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|l>|<cwith|1|-1|2|2|cell-rborder|0ln>|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|2|2|1|-1|cell-valign|top>|<cwith|2|2|1|-1|cell-vmode|exact>|<cwith|2|2|1|-1|cell-height|<plus|1fn|.5ex>>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|.5ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|1.5ex>>|<table|<row|<cell|<math|<tx|H<rsup|<math|+>><around|(|aq|)>>+<tx|O*H<rsup|<math|->><around|(|aq|)>><arrow><tx|H<rsub|<math|2>>*O<around|(|l|)>>>>|<cell|<math|<Delsub|r>H<st>=-55.82<units|k*J*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<math|<tx|N*a<around|(|s|)>>+<tx|H<rsub|<math|2>>*O<around|(|l|)>><arrow><tx|N*a<rsup|<math|+>>(aq>)+<tx|O*H<rsup|<math|->><around|(|aq|)>>+<frac|1|2><tx|H<rsub|<math|2>><around|(|g|)>>>>|<cell|<math|<Delsub|r>H<st>=-184.52<units|k*J*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<em|sum:>>|<cell|>>|<row|<cell|<math|<tx|N*a<around|(|s|)>>+<tx|H<rsup|<math|+>><around|(|aq|)>><arrow><tx|N*a<rsup|<math|+>>(aq>)+<frac|1|2><tx|H<rsub|<math|2>><around|(|g|)>>>>|<cell|<math|<Delsub|r>H<st>=-240.34<units|k*J*<space|0.17em>m*o*l<per>>>>>>>>
    From the relation <math|<Delsub|r>H<st>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i><Delsub|f>H<st><around|(|i|)>>
    and because <math|<Delsub|f>H<st>> is zero for each species in the
    reaction except Na<rsup|<math|+>>(aq):
    <vs><math|<Delsub|f>H<st><around|(|<tx|N*a<rsup|<math|+>>,aq>|)>=-240.34<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs>Add the reaction equations for the dissolution of NaOH and formation
    of NaOH(s): <vs><tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|l>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|l>|<cwith|1|-1|2|2|cell-rborder|0ln>|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|2|2|1|-1|cell-valign|top>|<cwith|2|2|1|-1|cell-vmode|exact>|<cwith|2|2|1|-1|cell-height|<plus|1fn|.5ex>>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|.5ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|1.5ex>>|<table|<row|<cell|<math|<tx|N*a*O*H<around|(|s|)>><arrow><tx|N*a<rsup|<math|+>><around|(|aq|)>>+<tx|O*H<rsup|<math|->><around|(|aq|)>>>>|<cell|<math|<Delsub|r>H<st>=-44.75<units|k*J*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<math|<tx|N*a<around|(|s|)>>+<frac|1|2><tx|O<rsub|<math|2>><around|(|g|)>>+<frac|1|2><tx|H<rsub|<math|2>><around|(|g|)>><arrow><tx|N*a*O*H<around|(|s|)>>>>|<cell|<math|<Delsub|r>H<st>=-425.61<units|k*J*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<em|sum:>>|<cell|>>|<row|<cell|<math|<tx|N*a<around|(|s|)>>+<frac|1|2><tx|O<rsub|<math|2>><around|(|g|)>>+<frac|1|2><tx|H<rsub|<math|2>><around|(|g|)>><arrow><tx|N*a<rsup|<math|+>><around|(|aq|)>>+<tx|O*H<rsup|<math|->><around|(|aq|)>>>>|<cell|<math|<Delsub|r>H<st>=-470.36<units|k*J*<space|0.17em>m*o*l<per>>>>>>>>
    The resulting reaction is the formation of aqueous NaOH:
    <vs><math|<Delsub|f>H<st><around|(|<tx|N*a*O*H,a*q>|)>=-470.36<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs>Apply the relation <math|<Delsub|r>H<st>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i><Delsub|f>H<st><around|(|i|)>>
    to this reaction, setting the standard molar enthalpies of formation of
    the elements equal to zero: <vs> <math|-470.36<units|k*J*<space|0.17em>m*o*l<per>>=<Delsub|f>H<st><around|(|<tx|N*a<rsup|<math|+>>,aq>|)>+<Delsub|f>H<st><around|(|<tx|O*H<rsup|<math|->>,aq>|)>>
    <vs> <math|<phantom|-470.36<units|k*J*<space|0.17em>m*o*l<per>>>=-240.34<units|k*J*<space|0.17em>m*o*l<per>>+<Delsub|f>H<st><around|(|<tx|O*H<rsup|<math|->>,aq>|)>>
    <vs> <math|<Delsub|f>H<st><around|(|<tx|O*H<rsup|<math|->>,aq>|)>=-230.02<units|k*J*<space|0.17em>m*o*l<per>>>>
    <problempartAns><math|<Delsub|f>H> for NaOH in 5 H<rsub|<math|2>>O;

    <answer|<math|-465.43<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| A solute at infinite dilution has the same partial molar enthalpy
    as in the solute standard state. Thus, the molar enthalpy change of the
    process in which NaOH in 5 H<rsub|<math|2>>O is formed from the elements
    in their reference states is the sum of the standard molar enthalpy of
    formation of NaOH(aq) and the molar enthalpy change of transferring NaOH
    from infinite dilution to NaOH in 5 H<rsub|<math|2>>O:
    <vs><math|<Delsub|f>H<around|(|<tx|N*a*O*H*i*n*5*H<rsub|<math|2>>*O>|)>=<around|(|-470.36+4.93|)><units|k*J*<space|0.17em>m*o*l<per>>=-465.43<units|k*J*<space|0.17em>m*o*l<per>>>>
    <problempartAns><math|<Del>H<m><sol>> for the dissolution of
    <math|1<mol>> NaOH(s) in <math|5<mol>> H<rsub|<math|2>>O<@>.

    <answer|<math|-39.82<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| The process can either be treated as the conversion of NaOH(s)
    into its elements, followed by the formation of the NaOH in the final
    solution: <vs> <math|<Delsub|r>H=-<Delsub|f>H<st><around|(|<tx|N*a*O*H,s>|)>+<Delsub|f>H<around|(|<tx|N*a*O*H*i*n*5*H<rsub|<math|2>>*O>|)>><no-page-break><vs>
    <math|<phantom|<Delsub|r>H>=<around|(|425.61-465.43|)><units|k*J*<space|0.17em>m*o*l<per>>=-39.82<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs>or as the solution process to form the infinitely dilute solution,
    followed by a change to the final solution (the reverse of dilution):
    <vs> <math|<Delsub|r>H=<Delsub|s*o*l>H<rsup|\<infty\>>-<Del>H<m><dil>>
    <vs> <math|<phantom|<Delsub|r>H>=<around|(|-44.75+4.93|)><units|k*J*<space|0.17em>m*o*l<per>>=-39.82<units|k*J*<space|0.17em>m*o*l<per>>>>
  </problemparts>

  <new-page>

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|7|7|1|-1|cell-bborder|1ln>|<table|<row|<cell|Substance>|<cell|<ccol|<math|<Delsub|f>H>/kJ<space|0.17em>mol<per>>>|<cell|<ccol|<math|M>/g<space|0.17em>mol<per><space|-0.17em><space|-0.17em><space|-0.17em><space|-0.17em><space|-0.17em>>>>|<row|<cell|H<rsub|<math|2>>O(l)>|<cell|-285.830>|<cell|18.0153>>|<row|<cell|<math|<chem>N*a<rsub|2>*S<rsub|2>*O<rsub|3><space|-0.17em>\<cdot\><space|-0.17em>5*H<rsub|2>*O<around|(|s|)>>>|<cell|-2607.93>|<cell|248.1828>>|<row|<cell|Na<rsub|<math|2>>S<rsub|<math|2>>O<rsub|<math|3>>
    in <math|50><space|0.17em>H<rsub|<math|2>>O>|<cell|-1135.914>|<cell|>>|<row|<cell|Na<rsub|<math|2>>S<rsub|<math|2>>O<rsub|<math|3>>
    in <math|100><space|0.17em>H<rsub|<math|2>>O>|<cell|-1133.822>|<cell|>>|<row|<cell|Na<rsub|<math|2>>S<rsub|<math|2>>O<rsub|<math|3>>
    in <math|200><space|0.17em>H<rsub|<math|2>>O>|<cell|-1132.236>|<cell|>>|<row|<cell|Na<rsub|<math|2>>S<rsub|<math|2>>O<rsub|<math|3>>
    in <math|300><space|0.17em>H<rsub|<math|2>>O>|<cell|-1131.780>|<cell|>>>>>
  </big-table|<label|tbl:11-Na2SO3 solns>Data for Problem
  11.<reference|prb:11-Na2SO3 solns><space|.15em><footnote|Ref.
  <cite|wagman-82>, pages 2-307 and 2-308.>>

  <problemAns><label|prb:11-Na2SO3 solns> Table <reference|tbl:11-Na2SO3
  solns><vpageref|tbl:11-Na2SO3 solns> lists data for water, crystalline
  sodium thiosulfate pentahydrate, and several sodium thiosulfate solutions.
  Find <math|<Del>H> to the nearest <math|0.01<units|k*J>> for the
  dissolution of <math|5.00<units|g>> of crystalline
  <math|<chem>N*a<rsub|2>*S<rsub|2>*O<rsub|3><space|-0.17em>\<cdot\><space|-0.17em>5*H<rsub|2>*O>
  in <math|50.0<units|g>> of water at <math|298.15<K>> and <math|1<br>>.

  <\answer>
    <math|<Del>H=0.92<units|k*J>>
  </answer>

  <\soln>
    \ The amounts in the solution are <vs>
    <math|<D>n<around|(|<tx|N*a<rsub|<math|2>>*S<rsub|<math|2>>*O<rsub|<math|3>>>|)>=<frac|5.00<units|g>|248.1828<units|g*<space|0.17em>m*o*l<per>>>=0.0201<units|m*o*l>>
    <vs> <math|<D>n<around|(|<tx|H<rsub|<math|2>>*O>|)>=<frac|50.0<units|g>|18.0153<units|g*<space|0.17em>m*o*l<per>>>+5\<times\>0.0201<units|m*o*l>=2.87<mol>>
    <vs>This is Na<rsub|<math|2>>S<rsub|<math|2>>O<rsub|<math|3>> in
    143<space|0.17em>H<rsub|<math|2>>O<@>. From a plot of <math|<Delsub|f>H>
    versus amount of H<rsub|<math|2>>O, estimate <vs>
    <math|<Delsub|f>H<around|(|<tx|N*a<rsub|<math|2>>*S<rsub|<math|2>>*O<rsub|<math|3>>
    in 143*<space|0.17em>H<rsub|<math|2>>*O>|)>=-1132.9<units|k*J*<space|0.17em>m*o*l<per>>>.
    <vs>Write reaction equations for three reactions whose sum is the
    dissolution process: <vs> <math|<tx|N*a<rsub|<math|2>>*S<rsub|<math|2>>*O<rsub|<math|3>>><space|-0.17em>\<cdot\><space|-0.17em><tx|5*H<rsub|<math|2>>*O<around|(|s|)>><arrow><tx|2*<space|0.17em>N*a<around|(|s|)>>+<tx|2*<space|0.17em>S<around|(|s|)>>+<frac|8|2><tx|O<rsub|<math|2>><around|(|g|)>>+<tx|5*<space|0.17em>H<rsub|<math|2>><around|(|g|)>>><next-line><math|<tx|2*<space|0.17em>N*a<around|(|s|)>>+<tx|2*<space|0.17em>S<around|(|s|)>>+<frac|3|2><tx|O<rsub|<math|2>><around|(|g|)>>+<tx|143*<space|0.17em>H<rsub|<math|2>>*O<around|(|l|)>><arrow><tx|N*a<rsub|<math|2>>*S<rsub|<math|2>>*O<rsub|<math|3>>
    in 143*<space|0.17em>H<rsub|<math|2>>*O>><next-line><math|<tx|5*<space|0.17em>H<rsub|<math|2>><around|(|g|)>>+<frac|5|2><tx|O<rsub|<math|2>><around|(|g|)>><arrow><tx|5*<space|0.17em>H<rsub|<math|2>>*O<around|(|l|)>>><next-line><em|sum:><next-line><math|<tx|N*a<rsub|<math|2>>*S<rsub|<math|2>>*O<rsub|<math|3>>><space|-0.17em>\<cdot\><space|-0.17em><tx|5*H<rsub|<math|2>>*O<around|(|s|)>>+<tx|138*<space|0.17em>H<rsub|<math|2>>*O<around|(|l|)>><arrow><tx|N*a<rsub|<math|2>>*S<rsub|<math|2>>*O<rsub|<math|3>>
    in 143*<space|0.17em>H<rsub|<math|2>>*O>> <vs>Calculate the enthalpy
    change per amount of solute dissolved from the sum for the three
    reactions: <vs><math|<around|(|<Del>H/n<B>|)>/<tx|k*J*<space|0.17em>m*o*l<per>>=2607.93-1132.9-5\<times\>285.830=45.9>
    <vs>Calculate the enthalpy change for <math|n<B>=0.0201<units|m*o*l>>:
    <vs><math|<Del>H=<around|(|0.0201<units|m*o*l>|)><around|(|45.9<units|k*J*<space|0.17em>m*o*l<per>>|)>=0.92<units|k*J>>
  </soln>

  <\big-table>
    <paragraphfootnotes>

    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|12|12|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|m<rprime|''><B>/<tx|m*o*l*<space|0.17em>k*g<per>>>>|<cell|<math|<Del>H<m><around|(|<tx|d*i*l,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>>|)>/<tx|k*J*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<math|0.295>>|<cell|<math|-2.883>>>|<row|<cell|<math|0.225>>|<cell|<math|-2.999>>>|<row|<cell|<math|0.199>>|<cell|<math|-3.041>>>|<row|<cell|<math|0.147>>|<cell|<math|-3.143>>>|<row|<cell|<math|0.113>>|<cell|<math|-3.217>>>|<row|<cell|<math|0.0716>>|<cell|<math|-3.325>>>|<row|<cell|<math|0.0544>>|<cell|<math|-3.381>>>|<row|<cell|<math|0.0497>>|<cell|<math|-3.412>>>|<row|<cell|<math|0.0368>>|<cell|<math|-3.466>>>|<row|<cell|<math|0.0179>>|<cell|<math|-3.574>>>|<row|<cell|<math|0.0128>>|<cell|<math|-3.621>>>>>>
  </big-table|<label|tbl:11-HCl prob>Data for Problem
  11.<reference|prb:11-HCl diln>. Molar integral enthalpies of dilution of
  aqueous HCl (<math|m<rprime|'><B>=3.337<units|m*o*l*<space|0.17em>k*g<per>>>)
  at <math|25<units|<degC>>>.<footnote|Ref. <cite|sturtevant-40a>.> >

  <problemAns><label|prb:11-HCl diln> Use the experimental data in Table
  <reference|tbl:11-HCl prob><vpageref|tbl:11-HCl prob> to evaluate
  <math|L<A>> and <math|L<B>> at <math|25<units|<degC>>> for an aqueous HCl
  solution of molality <math|m<B>=0.0900<units|m*o*l*<space|0.17em>k*g<per>>>.

  <\answer>
    <math|L<A>=-0.405<units|J*<space|0.17em>m*o*l<per>>><next-line><math|L<B>=0.810<units|k*J*<space|0.17em>m*o*l<per>>>
  </answer>

  <\soln>
    \ Plot <math|<Del>H<m><around|(|<tx|d*i*l,<space|0.17em><math|m<B><rprime|'>><ra><math|m<B><rprime|''>>>|)>>
    versus <math|<sqrt|m<rprime|''><B>>>; see Fig. <reference|fig:11-HCl
    diln><vpageref|fig:11-HCl diln>.

    <\big-figure>
      <boxedfigure|<image|./11-SUP/HCl-dil.eps||||> <capt|Plot for Problem
      11.<reference|prb:11-HCl diln>. The dashed line has the theoretical
      slope <math|C<rsub|<varPhi><rsub|L>>=1.988<timesten|3><units|J*<space|0.17em>k*g<rsup|<math|1/2>>*<space|0.17em>mol<rsup|<math|-3/2>>>>.<label|fig:11-HCl
      diln>>>
    </big-figure|>

    Extrapolation to <math|<sqrt|m<B>>=0> using the theoretical value of the
    limiting slope <math|C<rsub|<varPhi><rsub|L>>> gives
    <math|<Del>H<m><around|(|<tx|d*i*l,<space|0.17em><math|m<B><rprime|'>><ra><math|0>>|)>\<approx\>-3.836<units|k*J*<space|0.17em>m*o*l<per>>>,
    resulting in the scale of <math|<varPhi><rsub|L>> shown of the right side
    of the figure. <vs>At molality <math|m<B>=0.0900<units|m*o*l*<space|0.17em>k*g<per>>>
    (<math|<sqrt|m<B>>=0.300<units|m*o*l<rsup|<math|1/2>>*<space|0.17em>kg<rsup|<math|-1/2>>>>),
    values read from the plot are <math|<varPhi><rsub|L>\<approx\>0.560<units|k*g*<space|0.17em>m*o*l<per>>>
    and <math|<dif><varPhi><rsub|L>/<dif><sqrt|m<B>>\<approx\>1.670<units|k*J*<space|0.17em>k*g<rsup|<math|1/2>>*<space|0.17em>mol<rsup|<math|-3/2>>>>.
    <vs>From Eq. <reference|L(B)=....>: <vs><math|<D>L<B>=<varPhi><rsub|L>+<frac|<sqrt|m<B>>|2>*<space|0.17em><space|0.17em><frac|<dif><varPhi><rsub|L>|<dif><sqrt|m<B>>>>
    <vs><math|<D><phantom|L<B>>=0.560<units|k*g*<space|0.17em>m*o*l<per>>+<around*|(|<frac|0.300<units|m*o*l<rsup|<math|1/2>>*<space|0.17em>kg<rsup|<math|-1/2>>>|2>|)><around|(|1.670<units|k*J*<space|0.17em>k*g<rsup|<math|1/2>>*<space|0.17em>mol<rsup|<math|-3/2>>>|)>>
    <vs><math|<D><phantom|L<B>>=0.810<units|k*g*<space|0.17em>m*o*l<per>>>
    <vs>From Eq. <reference|L_A=M_A mB(Phi_L-L_B)>:
    <vs><math|L<A>=M<A>m<B><around|(|<varPhi><rsub|L>-L<B>|)>>
    <vs><math|<D><phantom|L<A>>=<around|(|0.018015<units|k*g*<space|0.17em>m*o*l<per>>|)><around|(|0.0900<units|m*o*l*<space|0.17em>k*g<per>>|)><around|[|<around|(|0.560-0.810|)><units|k*J*<space|0.17em>m*o*l<per>>|]>>
    <vs><math|<D><phantom|L<A>>=-0.405<units|J*<space|0.17em>m*o*l<per>>>
  </soln>

  <new-page>

  <problem><label|prb:11-hexane combustion> This 16-part problem illustrates
  the use of experimental data from <I|Bomb
  calorimetry\|p><I|Calorimetry!bomb\|p>bomb calorimetry and other sources,
  combined with thermodynamic relations derived in this and earlier chapters,
  to evaluate the standard molar combustion enthalpy of a liquid hydrocarbon.
  The substance under investigation is <em|n>-hexane, and the combustion
  reaction in the bomb vessel is

  <\equation*>
    <chem>C<rsub|6>*H<rsub|14><around|(|l|)>+<frac|19|2>*O<rsub|2><around|(|g|)><arrow>6*<space|0.17em>C*O<rsub|2><around|(|g|)>+7*<space|0.17em>H<rsub|2>*O<around|(|l|)>
  </equation*>

  Assume that the sample is placed in a glass ampoule that shatters at
  ignition. Data needed for this problem are collected in Table
  <reference|tbl:11-hexane combustion><vpageref|tbl:11-hexane combustion>.

  <\setlength|<tabcolsep>|.2em>
    <\big-table>
      <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-valign|top>|<cwith|1|1|1|-1|cell-vmode|exact>|<cwith|1|1|1|-1|cell-height|<plus|1fn|-1.8ex>>|<cwith|2|2|1|1|cell-col-span|2>|<cwith|6|6|1|1|cell-col-span|2>|<cwith|10|10|1|1|cell-col-span|2>|<cwith|15|15|1|1|cell-col-span|2>|<cwith|25|25|1|1|cell-col-span|2>|<cwith|28|28|1|1|cell-col-span|2>|<cwith|31|31|1|1|cell-col-span|2>|<cwith|34|34|1|-1|cell-valign|top>|<cwith|34|34|1|-1|cell-vmode|exact>|<cwith|34|34|1|-1|cell-height|<plus|1fn|-1.5ex>>|<cwith|34|34|1|-1|cell-bborder|1ln>|<table|<row|<cell|>|<cell|>>|<row|<cell|Properties
      of the bomb vessel:>|<cell|>>|<row|<cell|<space|2ex>internal
      volume<dotfill>>|<cell|<math|350.0<units|c*m<rsup|<math|3>>>>>>|<row|<cell|<space|2ex>mass
      of <em|n>-hexane placed in bomb<dotfill>>|<cell|<math|0.6741<units|g>>>>|<row|<cell|<space|2ex>mass
      of water placed in bomb<dotfill>>|<cell|<math|1.0016<units|g>>>>|<row|<cell|<addlinespace>Properties
      of liquid <em|n>-hexane:>|<cell|>>|<row|<cell|<space|2ex>molar
      mass<dotfill>>|<cell|<math|M=86.177<units|g*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<space|2ex>density<dotfill>>|<cell|<math|\<rho\>=0.6548<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>>>|<row|<cell|<space|2ex>cubic
      expansion coefficient<dotfill>>|<cell|<math|\<alpha\>=1.378<timesten|-3><units|K<per>>>>>|<row|<cell|<addlinespace>Properties
      of liquid H<rsub|<math|2>>O:>|<cell|>>|<row|<cell|<space|2ex>molar
      mass<dotfill>>|<cell|<math|M=18.0153<units|g*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<space|2ex>density<dotfill>>|<cell|<math|\<rho\>=0.9970<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>>>|<row|<cell|<space|2ex>cubic
      expansion coefficient<dotfill>>|<cell|<math|\<alpha\>=2.59<timesten|-4><units|K<per>>>>>|<row|<cell|<space|2ex>standard
      molar energy of vaporization ...>|<cell|<math|<Delsub|v*a*p>U<st>=41.53<units|k*J*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<addlinespace>Second
      virial coefficients, <math|298.15<K>>:>|<cell|>>|<row|<cell|<space|2ex><math|B<subs|A*A>><dotfill>>|<cell|<math|-1158<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|<space|2ex><math|B<subs|B*B>><dotfill>>|<cell|<math|-16<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|<space|2ex><math|<dif>B<subs|B*B>/<dif>T><dotfill>>|<cell|<math|0.21<units|c*m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>>>>|<row|<cell|<space|2ex><math|B<subs|C*C>><dotfill>>|<cell|<math|-127<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|<space|2ex><math|<dif>B<subs|C*C>/<dif>T><dotfill>>|<cell|<math|0.97<units|c*m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>>>>|<row|<cell|<space|2ex><math|B<subs|A*B>><dotfill>>|<cell|<math|-40<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|<space|2ex><math|B<subs|A*C>><dotfill>>|<cell|<math|-214<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|<space|2ex><math|B<subs|B*C>><dotfill>>|<cell|<math|-43.7<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|<space|2ex><math|<dif>B<subs|B*C>/<dif>T><dotfill>>|<cell|<math|0.4<units|c*m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>>>>|<row|<cell|<addlinespace>Henry's
      law constants at <math|1<br>> (solvent =
      H<rsub|<math|2>>O):>|<cell|>>|<row|<cell|<space|2ex>O<rsub|<math|2>><dotfill>>|<cell|<math|k<mbB>=796<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<space|2ex>CO<rsub|<math|2>><dotfill>>|<cell|<math|k<rsub|m,<tx|C>>=29.7<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<addlinespace>Partial
      molar volumes of solutes in water:>|<cell|>>|<row|<cell|<space|2ex>O<rsub|<math|2>><dotfill>>|<cell|<math|V<B><rsup|\<infty\>>=31<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|<space|2ex>CO<rsub|<math|2>><dotfill>>|<cell|<math|V<subs|C><rsup|\<infty\>>=33<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|<addlinespace>Standard
      molar energies of solution (solvent =
      H<rsub|<math|2>>O):>|<cell|>>|<row|<cell|<space|2ex>O<rsub|<math|2>><dotfill>>|<cell|<math|<Delsub|s*o*l>U<st>=-9.7<units|k*J*<space|0.17em>m*o*l<per>>>>>|<row|<cell|<space|2ex>CO<rsub|<math|2>><dotfill>>|<cell|<math|<Delsub|s*o*l>U<st>=-17.3<units|k*J*<space|0.17em>m*o*l<per>>>>>|<row|<cell|>|<cell|>>|<row|<cell|>|<cell|>>>>>
    </big-table|<label|tbl:11-hexane combustion>Data for Problem
    11.<reference|prb:11-hexane combustion>. The values of intensive
    properties are for a temperature of <math|298.15<K>> and a pressure of
    <math|30<br>> unless otherwise stated. Subscripts: A = H<rsub|<math|2>>O,
    B = O<rsub|<math|2>>, C = CO<rsub|<math|2>>.>
  </setlength>

  States 1 and 2 referred to in this problem are the initial and final states
  of the isothermal bomb process. The temperature is the reference
  temperature of <math|298.15<K>>.

  <\problemparts>
    <problempartAns><label|amounts> Parts
    <reference|amounts>--<reference|volumes> consist of simple calculations
    of some quantities needed in later parts of the problem. Begin by using
    the masses of C<rsub|<math|6>>H<rsub|<math|14>> and H<rsub|<math|2>>O
    placed in the bomb vessel, and their molar masses, to calculate the
    amounts (moles) of C<rsub|<math|6>>H<rsub|<math|14>> and
    H<rsub|<math|2>>O present initially in the bomb vessel. Then use the
    stoichiometry of the combustion reaction to find the amount of
    O<rsub|<math|2>> consumed and the amounts of H<rsub|<math|2>>O and
    CO<rsub|<math|2>> present in state 2. (There is not enough information at
    this stage to allow you to find the amount of O<rsub|<math|2>> present,
    just the change.) Also find the final mass of H<rsub|<math|2>>O<@>.
    Assume that oxygen is present in excess and the combustion reaction goes
    to completion.

    <answer|State 1:<next-line><math|n<subs|C<rsub|<math|6>>*H<rsub|<math|14>>>=7.822<timesten|-3><mol>><next-line><math|n<subs|H<rsub|<math|2>>*O>=0.05560<mol>><next-line>amount
    of O<rsub|<math|2>> consumed: <math|0.07431<mol>><next-line>State
    2:<next-line><math|n<subs|H<rsub|<math|2>>*O>=0.11035<mol>><next-line><math|n<subs|C*O<rsub|<math|2>>>=0.04693<mol>><next-line><tx|mass
    of H<rsub|<math|2>>O>=<math|1.9880<units|g>>>

    <soln| Initial amounts: <vs><math|<D>n<subs|C<rsub|<math|6>>*H<rsub|<math|14>>>=<frac|0.6741<units|g>|86.177<units|g*<space|0.17em>m*o*l<per>>>=7.822<timesten|-3><mol>>
    <vs><math|<D>n<subs|H<rsub|<math|2>>*O>=<frac|1.0016<units|g>|18.0153<units|g*<space|0.17em>m*o*l<per>>>=0.05560<mol>>
    <vs>Change in amount of oxygen: <vs><math|<Del>n<subs|O<rsub|<math|2>>>=-<around|(|19/2|)><around|(|7.822<timesten|-3><mol>|)>=-0.07431<mol>>
    <vs>Final amounts (state 2): <vs><math|n<subs|H<rsub|<math|2>>*O>=0.05560<mol>+<around|(|7|)><around|(|7.822<timesten|-3><mol>|)>=0.11035<mol>>
    <vs><math|n<subs|C*O<rsub|<math|2>>>=<around|(|6|)><around|(|7.822<timesten|-3><mol>|)>=0.04693<mol>>
    <vs>Final mass of H<rsub|<math|2>>O: <vs>
    <math|<around|(|0.11035<mol>|)><around|(|18.0153<units|g*<space|0.17em>m*o*l<per>>|)>=1.9880<units|g>>>

    <problempartAns><label|molar volumes> From the molar masses and the
    densities of liquid C<rsub|<math|6>>H<rsub|<math|14>> and
    H<rsub|<math|2>>O, calculate their molar volumes.

    <answer|<math|V<m><tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>=131.61<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>><next-line><math|V<m><tx|<around|(|H<rsub|<math|2>>*O|)>>=18.070<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>

    <soln| <math|<D>V<m><tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>=<frac|86.177<units|g*<space|0.17em>m*o*l<per>>|0.6548<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>=131.61<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    <vs><math|<D>V<m><tx|<around|(|H<rsub|<math|2>>*O|)>>=<frac|18.0153<units|g*<space|0.17em>m*o*l<per>>|0.9970<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>=18.070<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>

    <problempartAns><label|volumes>From the amounts present initially in the
    bomb vessel and the internal volume, find the volumes of liquid
    C<rsub|<math|6>>H<rsub|<math|14>>, liquid H<rsub|<math|2>>O, and gas in
    state 1 and the volumes of liquid H<rsub|<math|2>>O and gas in state 2.
    For this calculation, you can neglect the small change in the volume of
    liquid H<rsub|<math|2>>O due to its vaporization.

    <answer|State 1: <math|V<tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>=1.029<units|c*m<rsup|<math|3>>>><next-line><math|V<tx|<around|(|H<rsub|<math|2>>*O|)>>=1.005<units|c*m<rsup|<math|3>>>><next-line><math|V<sups|g>=348.0<units|c*m<rsup|<math|3>>>><next-line>State
    2:<next-line><math|V<tx|<around|(|H<rsub|<math|2>>*O|)>>=1.994<units|c*m<rsup|<math|3>>>><next-line><math|V<sups|g>=348.0<units|c*m<rsup|<math|3>>>>>

    <soln| Initial volumes: <vs><math|V<tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>=<around|(|7.822<timesten|-3><mol>|)><around|(|131.61<units|c*m<rsup|<math|3>>*mol<per>>|)>=1.029<units|c*m<rsup|<math|3>>>><next-line><math|V<tx|<around|(|H<rsub|<math|2>>*O|)>>=<around|(|0.05560<mol>|)><around|(|18.070<units|c*m<rsup|<math|3>>*mol<per>>|)>=1.005<units|c*m<rsup|<math|3>>>><next-line><math|V<sups|g>=<around|(|350.0-1.029-1.005|)><units|c*m<rsup|<math|3>>>=348.0<units|c*m<rsup|<math|3>>>>
    <vs>Final volumes: <vs><math|V<tx|<around|(|H<rsub|<math|2>>*O|)>>=<around|(|0.11035<mol>|)><around|(|18.070<units|c*m<rsup|<math|3>>*mol<per>>|)>=1.994<units|c*m<rsup|<math|3>>>><next-line><math|V<sups|g>=<around|(|350.0-1.994|)><units|c*m<rsup|<math|3>>>=348.0<units|c*m<rsup|<math|3>>>>>

    <problempartAns><label|O2 amounts>When the bomb vessel is charged with
    oxygen and before the inlet valve is closed, the pressure at
    <math|298.15<K>> measured on an external gauge is found to be
    <math|p<rsub|1>=30.00<br>>. To a good approximation, the gas phase of
    state 1 has the equation of state of pure O<rsub|<math|2>> (since the
    vapor pressure of water is only <math|0.1<units|%>> of <math|30.00<br>>).
    Assume that this equation of state is given by
    <math|V<m>=R*T/p+B<subs|B*B>> (Eq. <reference|Vm=RT/p+B>), where
    <math|B<subs|B*B>> is the second virial coefficient of O<rsub|<math|2>>
    listed in Table <reference|tbl:11-hexane combustion>. Solve for the
    amount of O<rsub|<math|2>> in the gas phase of state 1. The gas phase of
    state 2 is a mixture of O<rsub|<math|2>> and CO<rsub|<math|2>>, again
    with a negligible partial pressure of H<rsub|<math|2>>O<@>. Assume that
    only small fractions of the total amounts of O<rsub|<math|2>> and
    CO<rsub|<math|2>> dissolve in the liquid water, and find the amount of
    O<rsub|<math|2>> in the gas phase of state 2 and the mole fractions of
    O<rsub|<math|2>> and CO<rsub|<math|2>> in this phase.

    <answer|State 1:<next-line><math|n<subs|O<rsub|<math|2>>>=0.429<mol>><next-line>State
    2:<next-line><math|n<subs|O<rsub|<math|2>>>=0.355<mol>><next-line><math|y<subs|O<rsub|<math|2>>>=0.883><next-line><math|y<subs|C*O<rsub|<math|2>>>=0.117>>

    <soln| In gas phase of state 1: <vs> <math|<D>n<subs|O<rsub|<math|2>>>=<frac|V<sups|g>|<D><frac|R*T|p>+B<subs|B*B>>=<frac|348.0<timesten|-6><units|m<rsup|<math|3>>>|<D><frac|<around|(|<R>|)><around|(|298.15<K>|)>|30.00<timesten|5><Pa>>-16<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>><next-line><math|<phantom|n<subs|O<rsub|<math|2>>>>=0.429<mol>>
    <vs>In gas phase of state 2: <vs><math|n<subs|O<rsub|<math|2>>>=<around|(|0.429-0.07431|)><mol>=0.355<mol>>
    <vs><math|n<sups|g>=<around|(|0.355+0.04693|)><mol>=0.402<mol>>
    <vs><math|y<subs|O<rsub|<math|2>>>=<around|(|0.355<mol>|)>/<around|(|0.402<mol>|)>=0.883>
    <vs><math|y<subs|C*O<rsub|<math|2>>>=1-0.883=0.117>>

    <problempartAns><label|final p>You now have the information needed to
    find the pressure in state 2, which cannot be measured directly. For the
    mixture of O<rsub|<math|2>> and CO<rsub|<math|2>> in the gas phase of
    state 2, use Eq. <reference|B=yA^2 B(AA)+...><vpageref|B=yA<rsup|2>
    B(AA)+...> to calculate the second virial coefficient. Then solve the
    <I|Equation of state!gas at low pressure@of a gas at low
    pressure\|p>equation of state of Eq. <reference|V=nRT/p+nB><vpageref|V=nRT/p+nB>
    for the pressure. Also calculate the partial pressures of the
    O<rsub|<math|2>> and CO<rsub|<math|2>> in the gas mixture.

    <answer|State 2:<next-line><math|p<rsub|2>=27.9<br>><next-line><math|p<subs|O<rsub|<math|2>>>=24.6<br>><next-line><math|p<subs|C*O<rsub|<math|2>>>=3.26<br>>>

    <soln| In gas phase of state 2: <vs> <math|B=y<B><rsup|2>B<subs|B*B>+2*y<B>y<C>B<subs|B*C>+y<C><rsup|2>B<subs|C*C>>
    <vs> <math|B/<tx|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>=<around|(|0.883|)><rsup|2>*<around|(|-16|)>+2<around|(|0.883|)><around|(|0.117|)>*<around|(|-43.7|)>+<around|(|0.117|)><rsup|2>*<around|(|-127|)>><next-line><math|<phantom|B/<tx|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>=-23.2<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    <vs> <math|<D>p=<frac|n<sups|g>R*T|V<sups|g>-n<sups|g>B>=<frac|<around|(|0.402<mol>|)><around|(|<R>|)><around|(|298.15<K>|)>|348.0<timesten|-6><units|m<rsup|<math|3>>>-<around|(|0.402<mol>|)>*<around|(|-23.2<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>>><next-line><math|<phantom|p>=2.79<timesten|6><Pa>=27.9<br>>
    <vs> <math|p<subs|O<rsub|<math|2>>>=y<subs|O<rsub|<math|2>>>p=<around|(|0.883|)><around|(|27.9<br>|)>=24.6<br>>
    <vs> <math|p<subs|C*O<rsub|<math|2>>>=y<subs|C*O<rsub|<math|2>>>p=<around|(|0.117|)><around|(|27.9<br>|)>=3.26<br>>>

    <problempartAns><label|H2O fug>Although the amounts of H<rsub|<math|2>>O
    in the gas phases of states 1 and 2 are small, you need to know their
    values in order to take the energy of vaporization into account. In this
    part, you calculate the fugacities of the H<rsub|<math|2>>O in the
    initial and final gas phases, in part <reference|fug coeffs> you use gas
    equations of state to evaluate the fugacity coefficients of the
    H<rsub|<math|2>>O (as well as of the O<rsub|<math|2>> and
    CO<rsub|<math|2>>), and then in part <reference|H2O amts> you find the
    amounts of H<rsub|<math|2>>O in the initial and final gas phases.

    The pressure at which the pure liquid and gas phases of H<rsub|<math|2>>O
    are in equilibrium at <math|298.15<K>> (the saturation vapor pressure of
    water) is <math|0.03169<br>>. Use Eq.
    <reference|ln(phi)=Bp/RT><vpageref|ln(phi)=Bp/RT> to estimate the
    fugacity of H<rsub|<math|2>>O(g) in equilibrium with pure liquid water at
    this temperature and pressure. The effect of pressure on fugacity in a
    one-component liquid--gas system is discussed in Sec.
    <reference|12-effect of p on fug>; use Eq. <reference|f_i(p2)
    approx><vpageref|f<rsub|i>(p2) approx> to find the fugacity of
    H<rsub|<math|2>>O in gas phases equilibrated with liquid water at the
    pressures of states 1 and 2 of the isothermal bomb process. (The mole
    fraction of O<rsub|<math|2>> dissolved in the liquid water is so small
    that you can ignore its effect on the chemical potential of the water.)

    <answer|<math|<fug><subs|H<rsub|<math|2>>*O><around|(|0.03169<br>|)>=0.03164<br>><next-line>State
    1: <math|<fug><subs|H<rsub|<math|2>>*O>=0.03234<br>><next-line>State 2:
    <math|<fug><subs|H<rsub|<math|2>>*O>=0.03229<br>>>

    <soln| H<rsub|<math|2>>O(g) in equilibrium with H<rsub|<math|2>>O(l) at
    <math|298.15<K>> and <math|0.03169<br>>: <vs> <math|<D>ln
    \<phi\>=<frac|B<subs|A*A>p|R*T>=<frac|<around|(|-1158<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)><around|(|0.03169<timesten|5><Pa>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>=-1.48<timesten|-3>>
    <vs> <math|\<phi\>=0.9985> <vs> <math|<fug>=\<phi\>*p=<around|(|0.9985|)><around|(|0.03169<br>|)>=0.03164<br>>
    <vs>Equation <reference|f_i(p2) approx>: <vs>
    <math|<D><fug><rsub|i><around|(|p<rsub|2>|)>=<fug><rsub|i><around|(|p<rsub|1>|)>*exp
    <around*|[|<frac|V<rsub|i><liquid><around|(|p<rsub|2>-p<rsub|1>|)>|R*T>|]>>
    <vs>H<rsub|<math|2>>O(g) in equilibrium with H<rsub|<math|2>>O(l) at
    <math|298.15<K>> and <math|30.00<br>> (state 1): <vs>
    <math|<D><fug>=<around|(|0.03164<br>|)>*exp
    <around*|[|<frac|<around|(|18.070<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>*<around|(|30.00-0.03169|)><timesten|5><Pa>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>>
    <vs> <math|<phantom|<fug>>=0.03234<br>> <vs>H<rsub|<math|2>>O(g) in
    equilibrium with H<rsub|<math|2>>O(l) at <math|298.15<K>> and
    <math|27.9<br>> (state 2): <vs> <math|<D><fug>=<around|(|0.03164<br>|)>*exp
    <around*|[|<frac|<around|(|18.070<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>*<around|(|27.9-0.03169|)><timesten|5><Pa>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>>
    <vs> <math|<phantom|<fug>>=0.03229<br>>>

    <problempartAns><label|fug coeffs>Calculate the fugacity coefficients of
    H<rsub|<math|2>>O and O<rsub|<math|2>> in the gas phase of state 1 and of
    H<rsub|<math|2>>O, O<rsub|<math|2>>, and CO<rsub|<math|2>> in the gas
    phase of state 2.

    For state 1, in which the gas phase is practically-pure O<rsub|<math|2>>,
    you can use Eq. <reference|ln(phi)=Bp/RT><vpageref|ln(phi)=Bp/RT> to
    calculate <math|\<phi\><subs|O<rsub|<math|2>>>>. The other calculations
    require Eq. <reference|ln(phi_i)=Bi'p/RT><vpageref|ln(phi<rsub|i>)=Bi'p/RT>,
    with the value of <math|B<rsub|i><rprime|'>> found from the formulas of
    Eq. <reference|Bi'=2 sum yj Bij-B> or Eqs. <reference|B(A)'=> and
    <reference|B(B)'=> (<math|y<A>> is so small that you can set it equal to
    zero in these formulas).

    Use the fugacity coefficient and partial pressure of O<rsub|<math|2>> to
    evaluate its fugacity in states 1 and 2; likewise, find the fugacity of
    CO<rsub|<math|2>> in state 2. [You calculated the fugacity of the
    H<rsub|<math|2>>O in part <reference|H2O fug>.]

    <answer|State 1:<next-line><math|\<phi\><subs|H<rsub|<math|2>>*O>=0.925><next-line><math|\<phi\><subs|O<rsub|<math|2>>>=0.981><next-line><math|<fug><subs|O<rsub|<math|2>>>=29.4<br>><next-line>State
    2:<next-line><math|\<phi\><subs|H<rsub|<math|2>>*O>=0.896><next-line><math|\<phi\><subs|O<rsub|<math|2>>>=0.983><next-line><math|\<phi\><subs|C*O<rsub|<math|2>>>=0.910><next-line><math|<fug><subs|O<rsub|<math|2>>>=24.2<br>><next-line><math|<fug><subs|C*O<rsub|<math|2>>>=2.97<br>>>

    <soln| Gas phase of state 1: <vs> <math|B<A><rprime|'>=2*B<subs|A*B>-B<subs|B*B>=2*<around|(|-40<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>|)>-<around|(|-16<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>|)>><next-line><math|<phantom|B<A><rprime|'>>=-64<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>>
    <vs> <math|<D>ln \<phi\><A>=<frac|B<A><rprime|'>p|R*T>=<frac|<around|(|-64<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>|)><around|(|30.00<timesten|5><Pa>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>=-0.077>
    <vs> <math|\<phi\><A>=0.925> <vs> <math|<D>ln
    \<phi\><B>=<frac|B<subs|B*B>p|R*T>=<frac|<around|(|-16<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>|)><around|(|30.00<timesten|5><Pa>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>=-0.019>
    <vs> <math|\<phi\><B>=0.981> <vs> <math|<fug><B>=\<phi\><B>p<B>=<around|(|0.981|)><around|(|30.00<br>|)>=29.4<br>>
    <vs>Gas phase of state 2: <vs> <math|B<A><rprime|'>=2*y<B>B<subs|A*B>+2*y<C>B<subs|A*C>-2*y<B>y<C>B<subs|B*C>-y<B><rsup|2>B<subs|B*B>-y<C><rsup|2>B<subs|C*C>><no-page-break><vs>
    <math|<phantom|B<A><rprime|'>>=[2<around|(|0.883|)>*<around|(|-40|)>+2<around|(|0.117|)>*<around|(|-214|)>-2<around|(|0.883|)><around|(|0.117|)>*<around|(|-43.7|)>><no-page-break><vs>
    <math|<phantom|B<A><rprime|'>=>-<around|(|0.883|)><rsup|2>*<around|(|-16|)>-<around|(|0.117|)><rsup|2>*<around|(|-127|)>]<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>>
    <vs> <math|<phantom|B<A><rprime|'>>=-97.5<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>>
    <vs> <math|<D>ln \<phi\><A>=<frac|B<A><rprime|'>p|R*T>=<frac|<around|(|-97.5<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>|)><around|(|27.9<timesten|5><Pa>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>>
    <vs> <math|<D><phantom|ln \<phi\><A>>=-0.110> <vs>
    <math|\<phi\><A>=0.896> <vs> <math|B<B><rprime|'>=B<subs|B*B>-<around|(|B<subs|B*B>-2*B<subs|B*C>+B<subs|C*C>|)>*y<C><rsup|2>>
    <vs> <math|<phantom|B<B><rprime|'>>=<around|[|-16-<around|(|-16+2\<times\>43.7-127|)><around|(|0.117|)><rsup|2>|]><timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>>
    <vs> <math|<phantom|B<B><rprime|'>>=-15<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>>
    <vs> <math|<D>ln \<phi\><B>=<frac|B<B><rprime|'>p|R*T>=<frac|<around|(|-15<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>|)><around|(|27.9<timesten|5><Pa>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>>
    <vs> <math|<D><phantom|ln \<phi\><B>>=-0.017> <vs>
    <math|\<phi\><B>=0.983> <vs> <math|B<C><rprime|'>=B<subs|C*C>-<around|(|B<subs|B*B>-2*B<subs|B*C>+B<subs|C*C>|)>*y<B><rsup|2>>
    <vs> <math|<phantom|B<C><rprime|'>>=<around|[|-127-<around|(|-16+2\<times\>43.7-127|)><around|(|0.883|)><rsup|2>|]><timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>>
    <vs> <math|<phantom|B<C><rprime|'>>=-84<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>>
    <vs> <math|<D>ln \<phi\><C>=<frac|B<C><rprime|'>p|R*T>=<frac|<around|(|-84<timesten|-6><units|m<rsup|<math|3>><space|0.17em><mol><per>>|)><around|(|27.9<timesten|5><Pa>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>>
    <vs> <math|<D><phantom|ln \<phi\><C>>=-0.094> <vs>
    <math|\<phi\><C>=0.910> <vs><math|<fug><B>=\<phi\><B>p<B>=<around|(|0.983|)><around|(|24.6<br>|)>=24.2<br>>
    <vs><math|<fug><C>=\<phi\><C>p<C>=<around|(|0.910|)><around|(|3.26<br>|)>=2.97<br>>>

    <problempartAns><label|H2O amts>From the values of the fugacity and
    fugacity coefficient of a constituent of a gas mixture, you can calculate
    the partial pressure with Eq. <reference|f_i=phi_i*p_i><vpageref|f<rsub|i>=phi<rsub|i>*p<rsub|i>>,
    then the mole fraction with <math|y<rsub|i>=p<rsub|i>/p>, and finally the
    amount with <math|n<rsub|i>=y<rsub|i>*n>. Use this method to find the
    amounts of H<rsub|<math|2>>O in the gas phases of states 1 and 2, and
    also calculate the amounts of H<rsub|<math|2>>O in the liquid phases of
    both states.

    <answer|State 1:<next-line><math|n<subs|H<rsub|<math|2>>*O><sups|g>=5.00<timesten|-4><mol>><next-line><math|n<subs|H<rsub|<math|2>>*O><sups|l>=0.05510<mol>><next-line>State
    2: <math|n<subs|H<rsub|<math|2>>*O><sups|g>=5.19<timesten|-4><mol>><next-line><math|n<subs|H<rsub|<math|2>>*O><sups|l>=0.10983<mol>>>

    <soln| H<rsub|<math|2>>O in gas phase of state 1:
    <vs><math|<D>p<A>=<frac|<fug><A>|\<phi\><A>>=<frac|0.03234<br>|0.925>=0.0350<br>>
    <vs><math|<D>y<A>=<frac|p<A>|p>=<frac|0.0350<br>|30.00<br>>=0.00117>
    <vs><math|n<A>=y<A>n<sups|g>=<around|(|0.00117|)><around|(|0.429<mol>|)>=5.00<timesten|-4><mol>>
    <vs>H<rsub|<math|2>>O in gas phase of state 2:
    <vs><math|<D>p<A>=<frac|<fug><A>|\<phi\><A>>=<frac|0.03229<br>|0.896>=0.03604<br>>
    <vs><math|<D>y<A>=<frac|p<A>|p>=<frac|0.03604<br>|27.9<br>>=0.00129>
    <vs><math|n<A>=y<A>n<sups|g>=<around|(|0.00129|)><around|(|0.402<mol>|)>=5.19<timesten|-4><mol>>
    <vs>H<rsub|<math|2>>O in liquid phase of state 1:
    <vs><math|n<A>=0.05560<mol>-5.00<timesten|-4><mol>=0.05510<mol>>
    <vs>H<rsub|<math|2>>O in liquid phase of state 2:
    <vs><math|n<A>=0.11035<mol>-5.19<timesten|-4><mol>=0.10983<mol>>>

    <problempartAns><label|g sol>Next, consider the O<rsub|<math|2>>
    dissolved in the water of state 1 and the O<rsub|<math|2>> and
    CO<rsub|<math|2>> dissolved in the water of state 2. Treat the solutions
    of these gases as ideal dilute with the molality of solute <math|i> given
    by <math|m<rsub|i>=<fug><rsub|i>/k<rsub|m,i>> (Eq.
    <reference|KmB=lim(fB/mB>). The values of the Henry's law constants of
    these gases listed in Table <reference|tbl:11-hexane combustion> are for
    the standard pressure of <math|1<br>>. Use Eq. <reference|KmB(p2)
    approx><vpageref|KmB(p2) approx> to find the appropriate values of
    <math|k<rsub|m,i>> at the pressures of states 1 and 2, and use these
    values to calculate the amounts of the dissolved gases in both states.

    <answer|State 1:<next-line><math|k<rsub|m,<tx|O<rsub|<math|2>>>>=825<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>><next-line><math|n<subs|O<rsub|<math|2>>>=3.57<timesten|-5><mol>><next-line>State
    2:<next-line><math|k<rsub|m,<tx|O<rsub|<math|2>>>>=823<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>><next-line><math|k<rsub|m,<tx|C*O<rsub|<math|2>>>>=30.8<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>><next-line><math|n<subs|O<rsub|<math|2>>>=5.85<timesten|-5><mol>><next-line><math|n<subs|C*O<rsub|<math|2>>>=1.92<timesten|-4><mol>>>

    <soln| Equation <reference|KmB(p2) approx>:
    <vs><math|<D>k<mbB><around|(|p<rsub|2>|)>=k<mbB><around|(|p<rsub|1>|)>*exp
    <around*|[|<frac|V<B><rsup|\<infty\>><around|(|p<rsub|2>-p<rsub|1>|)>|R*T>|]>>
    <vs>Dissolved O<rsub|<math|2>> in state 1:
    <vs><math|<D>k<mbB>=<around|(|796<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>|)>*exp
    <around*|[|<frac|<around|(|31<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>*<around|(|30.00-1|)><timesten|5><Pa>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>>
    <vs> <math|<phantom|k<mbB>>=825<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>
    <vs><math|<D>m<B>=<frac|<fug><B>|k<mbB>>=<frac|29.4<br>|825<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>=0.0356<units|m*o*l*<space|0.17em>k*g<per>>>
    <vs><math|n<B>=<around|(|0.0356<units|m*o*l*<space|0.17em>k*g<per>>|)><around|(|1.0016<timesten|-3><units|k*g>|)>=3.57<timesten|-5><mol>>
    <vs>Dissolved O<rsub|<math|2>> and CO<rsub|<math|2>> in state 2:
    <vs><math|<D>k<mbB>=<around|(|796<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>|)>*exp
    <around*|[|<frac|<around|(|31<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>*<around|(|27.9-1|)><timesten|5><Pa>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>>
    <vs> <math|<phantom|k<mbB>>=823<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>
    <vs><math|<D>m<B>=<frac|<fug><B>|k<mbB>>=<frac|24.2<br>|823<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>=0.0294<units|m*o*l*<space|0.17em>k*g<per>>>
    <vs><math|n<B>=<around|(|0.0294<units|m*o*l*<space|0.17em>k*g<per>>|)><around|(|1.9880<timesten|-3><units|k*g>|)>=5.85<timesten|-5><mol>>
    <vs><math|<D>k<rsub|m,<tx|C>>=<around|(|29.7<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>|)>*exp
    <around*|[|<frac|<around|(|33<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>*<around|(|27.9-1|)><timesten|5><Pa>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>>
    <vs> <math|<phantom|k<rsub|m,<tx|C>>>=30.8<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>
    <vs><math|<D>m<C>=<frac|<fug><C>|k<rsub|m,<tx|C>>>=<frac|2.97<br>|30.8<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>=0.096<units|m*o*l*<space|0.17em>k*g<per>>>
    <vs><math|n<C>=<around|(|0.096<units|m*o*l*<space|0.17em>k*g<per>>|)><around|(|1.9880<timesten|-3><units|k*g>|)>=1.92<timesten|-4><mol>>>

    <problempartAns><label|H2O vap>At this point in the calculations, you
    know the values of all properties needed to describe the initial and
    final states of the isothermal bomb process. You are now able to evaluate
    the various <I|Washburn corrections\|p>Washburn corrections. These
    corrections are the internal energy changes, at the reference temperature
    of <math|298.15<K>>, of processes that connect the standard states of
    substances with either state 1 or state 2 of the isothermal bomb process.

    First, consider the gaseous H<rsub|<math|2>>O<@>. The Washburn
    corrections should be based on a pure-liquid standard state for the
    H<rsub|<math|2>>O<@>. Section <reference|7-st molar fncs of a gas> shows
    that the molar internal energy of a pure gas under ideal-gas conditions
    (low pressure) is the same as the molar internal energy of the gas in its
    standard state at the same temperature. Thus, the molar internal energy
    change when a substance in its pure-liquid standard state changes
    isothermally to an ideal gas is equal to the standard molar internal
    energy of vaporization, <math|<Delsub|v*a*p>U<st>>. Using the value of
    <math|<Delsub|v*a*p>U<st>> for H<rsub|<math|2>>O given in Table
    <reference|tbl:11-hexane combustion>, calculate <math|<Del>U> for the
    vaporization of liquid H<rsub|<math|2>>O at pressure <math|p<st>> to
    ideal gas in the amount present in the gas phase of state 1. Also
    calculate <math|<Del>U> for the condensation of ideal gaseous
    H<rsub|<math|2>>O in the amount present in the gas phase of state 2 to
    liquid at pressure <math|p<st>>.

    <answer|H<rsub|<math|2>>O vaporization:
    <math|<Del>U=+20.8<units|J>><next-line>H<rsub|<math|2>>O condensation:
    <math|<Del>U=-21.6<units|J>>>

    <soln| Vaporization: <vs><math|<Del>U=<around|(|5.00<timesten|-4><mol>|)><around|(|41.53<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)>=20.8<units|J>>
    <vs>Condensation: <vs><math|<Del>U=-<around|(|5.19<timesten|-4><mol>|)><around|(|41.53<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)>=-21.6<units|J>>>

    <problempartAns><label|gas sol delU>Next, consider the dissolved
    O<rsub|<math|2>> and CO<rsub|<math|2>>, for which gas standard states are
    used. Assume that the solutions are sufficiently dilute to have
    infinite-dilution behavior; then the partial molar internal energy of
    either solute in the solution at the standard pressure <math|p<st>=1<br>>
    is equal to the standard partial molar internal energy based on a solute
    standard state (Sec. <reference|9-mixt st states>). Values of
    <math|<Delsub|s*o*l>U<st>> are listed in Table <reference|tbl:11-hexane
    combustion>. Find <math|<Del>U> for the dissolution of O<rsub|<math|2>>
    from its gas standard state to ideal-dilute solution at pressure
    <math|p<st>> in the amount present in the aqueous phase of state 1. Find
    <math|<Del>U> for the desolution (transfer from solution to gas phase) of
    O<rsub|<math|2>> and of CO<rsub|<math|2>> from ideal-dilute solution at
    pressure <math|p<st>>, in the amounts present in the aqueous phase of
    state 2, to their gas standard states.

    <answer|O<rsub|<math|2>> dissolution:
    <math|<Del>U=-0.35<units|J>><next-line>O<rsub|<math|2>> desolution:
    <math|<Del>U=0.57<units|J>><next-line>CO<rsub|<math|2>> desolution:
    <math|<Del>U=3.32<units|J>>>

    <soln| O<rsub|<math|2>> dissolution: <vs>
    <math|<Del>U=<around|(|3.57<timesten|-5><mol>|)>*<around|(|-9.7<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)>=-0.35<units|J>>
    <vs>O<rsub|<math|2>> desolution: <vs>
    <math|<Del>U=<around|(|5.85<timesten|-5><mol>|)><around|(|9.7<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)>=0.57<units|J>>
    <vs>CO<rsub|<math|2>> desolution: <vs>
    <math|<Del>U=<around|(|1.92<timesten|-4><mol>|)><around|(|17.3<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)>=3.32<units|J>>>

    <problempartAns><label|l delU>Calculate the internal energy changes when
    the liquid phases of state 1 (<em|n>-hexane and aqueous solution) are
    compressed from <math|p<st>> to <math|p<rsub|1>> and the aqueous solution
    of state 2 is decompressed from <math|p<rsub|2>> to <math|p<st>>. Use an
    approximate expression from Table <reference|tbl:7-isothermal p change>,
    and treat the cubic expansion coefficient of the aqueous solutions as
    being the same as that of pure water.

    <answer|C<rsub|<math|6>>H<rsub|<math|14>>(l) compression:
    <math|<Del>U=-1.226<units|J>><next-line>solution compression:
    <math|<Del>U=-0.225<units|J>><next-line>solution decompression:
    <math|<Del>U=0.414<units|J>>>

    <soln| From Table <reference|tbl:7-isothermal p change>:
    <math|<Del>U\<approx\>-\<alpha\>*T*V<Del>p>
    <vs>C<rsub|<math|6>>H<rsub|<math|14>>(l) compression: <vs>
    <math|<Del>U=-<around|(|1.378<timesten|-3><units|K<per>>|)><around|(|298.15<K>|)><around|(|1.029<timesten|-6><units|m<rsup|<math|3>>>|)>>
    <vs> <math|<phantom|<Del>U=>\<times\><around|(|30.00<timesten|5><Pa>-1<timesten|5><Pa>|)>=-1.226<units|J>>
    <vs>Solution compression: <vs> <math|<Del>U=-<around|(|2.59<timesten|-4><units|K<per>>|)><around|(|298.15<K>|)><around|(|1.005<timesten|-6><units|m<rsup|<math|3>>>|)>>
    <vs> <math|<phantom|<Del>U=>\<times\><around|(|30.00<timesten|5><Pa>-1<timesten|5><Pa>|)>=-0.225<units|J>>
    <vs>Solution decompression: <vs> <math|<Del>U=-<around|(|2.59<timesten|-4><units|K<per>>|)><around|(|298.15<K>|)><around|(|1.994<timesten|-6><units|m<rsup|<math|3>>>|)>>
    <vs> <math|<phantom|<Del>U=>\<times\><around|(|1<timesten|5><Pa>-27.9<timesten|5><Pa>|)>=0.414<units|J>>>

    <problempartAns><label|gas delU>The final Washburn corrections are
    internal energy changes of the gas phases of states 1 and 2.
    H<rsub|<math|2>>O has such low mole fractions in these phases that you
    can ignore H<rsub|<math|2>>O in these calculations; that is, treat the
    gas phase of state 1 as pure O<rsub|<math|2>> and the gas phase of state
    2 as a binary mixture of O<rsub|<math|2>> and CO<rsub|<math|2>>.

    One of the internal energy changes is for the compression of gaseous
    O<rsub|<math|2>>, starting at a pressure low enough for ideal-gas
    behavior (<math|U<m>=U<m><st>>) and ending at pressure <math|p<rsub|1>>
    to form the gas phase present in state 1. Use the approximate expression
    for <math|U<m>-U<m><st><gas>> in Table <reference|tbl:7-gas standard
    molar> to calculate <math|<Del>U=U<around|(|p<rsub|1>|)>-n*U<m><st><gas>>;
    a value of <math|<dif>B/<dif>T> for pure O<rsub|<math|2>> is listed in
    Table <reference|tbl:11-hexane combustion>.

    The other internal energy change is for a process in which the gas phase
    of state 2 at pressure <math|p<rsub|2>> is expanded until the pressure is
    low enough for the gas to behave ideally, and the mixture is then
    separated into ideal-gas phases of pure O<rsub|<math|2>> and
    CO<rsub|<math|2>>. The molar internal energies of the separated
    low-pressure O<rsub|<math|2>> and CO<rsub|<math|2>> gases are the same as
    the standard molar internal energies of these gases. The internal energy
    of unmixing ideal gases is zero (Eq. <reference|del(mix)Um(id)=0>). The
    dependence of the internal energy of the gas mixture is given, to a good
    approximation, by <math|U=<big|sum><rsub|i>U<rsub|i><st><gas>-n*p*T<dif>B/<dif>T>,
    where <math|B> is the second virial coefficient of the gas mixture; this
    expression is the analogy for a gas mixture of the approximate expression
    for <math|U<m>-U<m><st><gas>> in Table <reference|tbl:7-gas standard
    molar>. Calculate the value of <math|<dif>B/<dif>T> for the mixture of
    O<rsub|<math|2>> and CO<rsub|<math|2>> in state 2 (you need Eq.
    <reference|B=yA^2 B(AA)+...><vpageref|B=yA<rsup|2> B(AA)+...> and the
    values of <math|<dif>B<rsub|i*j>/<dif>T> in Table
    <reference|tbl:11-hexane combustion>) and evaluate
    <math|<Del>U=<big|sum><rsub|i>n<rsub|i>*U<rsub|i><st><gas>-U<around|(|p<rsub|2>|)>>
    for the gas expansion.

    <answer|O<rsub|<math|2>> compression:
    <math|<Del>U=-81<units|J>><next-line>gas mixture:
    <math|<dif>B/<dif>T=0.26<timesten|-6><units|m<rsup|<math|3>>*K<per>mol<per>>><next-line>gas
    mixture expansion: <math|<Del>U=87<units|J>>>

    <soln| O<rsub|<math|2>> compression: <vs>
    <math|<Del>U=U<around|(|p<rsub|1>|)>-n*U<m><st><gas>=n*U<m><around|(|p<rsub|1>|)>-n*U<m><st><gas>=-n*p<rsub|1>*T<dif>B<subs|B*B>/<dif>T>
    <vs> <math|<phantom|<Del>U>=-<around|(|0.429<mol>|)><around|(|30.00<timesten|5><Pa>|)><around|(|298.15<K>|)><around|(|0.21<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>|)>>
    <vs> <math|<phantom|<Del>U>=-81<units|J>> <vs>Gas mixture: <vs>
    <math|<dif>B/<dif>T=y<B><rsup|2><dif>B<subs|B*B>/<dif>T+2*y<B>y<C><dif>B<subs|B*C>/<dif>T+y<C><rsup|2><dif>B<subs|C*C>/<dif>T>
    <vs> <math|<phantom|<dif>B/<dif>T>=<around|(|0.883|)><rsup|2><around|(|0.21<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>|)>>
    <vs> <math|<phantom|<dif>B/<dif>T=>+2<around|(|0.883|)><around|(|0.117|)><around|(|0.4<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>|)>>
    <vs> <math|<phantom|<dif>B/<dif>T=>+<around|(|0.117|)><rsup|2><around|(|0.97<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>|)>>
    <vs> <math|<phantom|<dif>B/<dif>T>=0.26<timesten|-6><units|m<rsup|<math|3>>*K<per>mol<per>>>
    <vs>Gas mixture expansion: <vs> <math|<Del>U=<big|sum><rsub|i>n<rsub|i>*U<rsub|i><st><gas>-U<around|(|p<rsub|2>|)>=n<sups|g>p<rsub|2>*T<dif>B/<dif>T>
    <vs> <math|<phantom|<Del>U>=<around|(|0.402<mol>|)><around|(|27.9<timesten|5><Pa>|)><around|(|298.15<K>|)><around|(|0.26<timesten|-6><units|m<rsup|<math|3>>*K<per>mol<per>>|)>>
    <vs> <math|<phantom|<Del>U>=87<units|J>>>

    <problempartAns><label|del U>Add the internal energy changes you
    calculated in parts <reference|H2O vap>--<reference|gas delU> to find the
    total internal energy change of the Washburn corrections. Note that most
    of the corrections occur in pairs of opposite sign and almost completely
    cancel one another. Which contributions are the greatest in magnitude?

    <answer|<math|<Del>U=8<units|J>>>

    <\soln>
      \ The Washburn corrections are collected in Table
      <reference|tbl:11prob-Washburn><vpageref|tbl:11prob-Washburn>. The
      largest contributions are those for H<rsub|<math|2>>O vaporization and
      condensation, for compression of the O<rsub|<math|2>>, and for
      expansion of the product gas mixture.

      <\big-table>
        <minipagetable|5.1cm|<label|tbl:11prob-Washburn><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|2|2|cell-col-span|2>|<cwith|1|1|2|2|cell-halign|l>|<cwith|1|1|2|2|cell-rborder|0ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|11|11|1|-1|cell-valign|top>|<cwith|11|11|1|-1|cell-vmode|exact>|<cwith|11|11|1|-1|cell-height|<plus|1fn|.5ex>>|<cwith|12|12|1|-1|cell-bborder|1ln>|<table|<row|<cell|Contribution>|<cell|<math|<Del>U/<tx|J>>>|<cell|>>|<row|<cell|H<rsub|<math|2>>O
        vaporization>|<cell|20>|<cell|8>>|<row|<cell|H<rsub|<math|2>>O
        condensation>|<cell|<math|-21>>|<cell|6>>|<row|<cell|O<rsub|<math|2>>
        dissolution>|<cell|<math|-0>>|<cell|35>>|<row|<cell|O<rsub|<math|2>>
        desolution>|<cell|0>|<cell|57>>|<row|<cell|CO<rsub|<math|2>>
        desolution>|<cell|3>|<cell|32>>|<row|<cell|C<rsub|<math|6>>H<rsub|<math|14>>(l)
        compression>|<cell|<math|-1>>|<cell|226>>|<row|<cell|solution
        compression>|<cell|<math|-0>>|<cell|225>>|<row|<cell|solution
        decompression>|<cell|0>|<cell|414>>|<row|<cell|O<rsub|<math|2>>
        compression>|<cell|<math|-81>>|<cell|>>|<row|<cell|gas mixture
        expansion>|<cell|87>|<cell|>>|<row|<cell|<em|Sum>>|<cell|8>|<cell|>>>>>>
      </big-table|Washburn corrections>
    </soln>

    <problempartAns>The internal energy change of the isothermal bomb process
    in the bomb vessel, corrected to the reference temperature of
    <math|298.15<K>>, is found to be <math|<Del>U<around|(|<tx|I*B*P>,T<subs|r*e*f>|)>=-32.504<units|k*J>>.
    Assume there are no side reactions or auxiliary reactions. From Eqs.
    <reference|DelUo=DelU(IBP)+...> and <reference|DelUom=DelUo/xi>,
    calculate the standard molar internal energy of combustion of
    <em|n>-hexane at <math|298.15<K>>.

    <answer|<math|<Delsub|c>U<st>=-4154.4<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| <math|<D><Delsub|c>U<st><around|(|T<subs|r*e*f>|)>=<frac|<Del>U<around|(|<tx|I*B*P>,T<subs|r*e*f>|)>+<tx|<around|(|W*a*s*h*b*u*r*n*c*o*r*r*e*c*t*i*o*n*s|)>>|n<subs|C<rsub|<math|6>>*H<rsub|<math|14>>>>>
    <vs> <math|<D><phantom|<Delsub|c>U<st><around|(|T<subs|r*e*f>|)>>=<frac|-32.504<units|k*J>+8<timesten|-3><units|k*J>|7.822<timesten|-3><mol>>=-4154.4<units|k*J*<space|0.17em>m*o*l<per>>>>

    <problempartAns><label|del(c)Hmo(hexane)>From Eq.
    <reference|DelHom=DelUom+sum nu_i(g)RT>, calculate the standard molar
    enthalpy of combustion of <em|n>-hexane at <math|298.15<K>>.

    <answer|<math|<Delsub|c>H<st>=-4163.1<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| <math|<D><Delsub|c>H<st>=<Delsub|c>U<st><around|(|T<subs|r*e*f>|)>+<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i><sups|g>R*T<subs|r*e*f>>
    <vs> <math|<phantom|<Delsub|c>H<st>>=-4154.4<units|k*J*<space|0.17em>m*o*l<per>>+<around*|(|-<frac|19|2>+6|)><around|(|<R>|)><around|(|298.15<K>|)>*<around|(|1<units|k*J>/10<rsup|3><units|J>|)>>
    <vs> <math|<phantom|<Delsub|c>H<st>>=-4163.1<units|k*J*<space|0.17em>m*o*l<per>>>>
  </problemparts|>

  <problemAns>By combining the results of Prob. 11.<reference|prb:11-hexane
  combustion><reference|del(c)Hmo(hexane)> with the values of standard molar
  enthalpies of formation from Appendix <reference|app:props>, calculate the
  standard molar enthalpy of formation of liquid <em|n>-hexane at
  <math|298.15<K>>.

  <\answer>
    <math|<Delsub|f>H<st>=-198.8<units|k*J*<space|0.17em>m*o*l<per>>>
  </answer>

  <\soln>
    \ Apply the relation <math|<Delsub|r>H<st>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i><Delsub|f>H<st><around|(|i|)>>
    to the combustion reaction <math|<chem>C<rsub|6>*H<rsub|14><around|(|l|)>+<frac|19|2>*O<rsub|2><around|(|g|)><ra>6*<space|0.17em>C*O<rsub|2><around|(|g|)>+7*<space|0.17em>H<rsub|2>*O<around|(|l|)>>:
    <vs><math|<Delsub|c>H<st><tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>=-<Delsub|f>H<st><tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>+6<space|0.17em><Delsub|f>H<st><tx|<around|(|C*O<rsub|<math|2>>|)>>+7<space|0.17em><Delsub|f>H<st><tx|<around|(|H<rsub|<math|2>>*O,<space|0.17em>l|)>>>
    <vs><math|<Delsub|f>H<st><tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>=-<Delsub|c>H<st><tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>+6<space|0.17em><Delsub|f>H<st><tx|<around|(|C*O<rsub|<math|2>>|)>>+7<space|0.17em><Delsub|f>H<st><tx|<around|(|H<rsub|<math|2>>*O,<space|0.17em>l|)>>>
    <vs><math|<phantom|<Delsub|f>H<st><tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>>=-<around|(|-4163.1<units|k*J*<space|0.17em>m*o*l<per>>|)>+6*<around|(|-393.51<units|k*J*<space|0.17em>m*o*l<per>>|)>+7*<around|(|-285.830<units|k*J*<space|0.17em>m*o*l<per>>|)>>
    <vs><math|<phantom|<Delsub|f>H<st><tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>>=-198.8<units|k*J*<space|0.17em>m*o*l<per>>>
  </soln>

  <problemAns><label|prb:11-flame> Consider the combustion of methane:

  <\equation*>
    <chem>C*H<rsub|4><around|(|g|)>+2*<space|0.17em>O<rsub|2><around|(|g|)><arrow>C*O<rsub|2><around|(|g|)>+2*<space|0.17em>H<rsub|2>*O<around|(|g|)>
  </equation*>

  Suppose the reaction occurs in a flowing gas mixture of methane and air.
  Assume that the pressure is constant at <math|1<br>>, the reactant mixture
  is at a temperature of <math|298.15<K>> and has stoichiometric proportions
  of methane and oxygen, and the reaction goes to completion with no
  dissociation. For the quantity of gaseous product mixture containing
  <math|1<mol>> CO<rsub|<math|2>>, <math|2<mol>> H<rsub|<math|2>>O, and the
  nitrogen and other substances remaining from the air, you may use the
  approximate formula <math|C<rsub|p><around|(|<tx|P>|)>=a+b*T>, where the
  coefficients have the values <math|a=297.0<units|J*<space|0.17em>K<per>>>
  and <math|b=8.520<timesten|-2><units|J*<space|0.17em>K<rsup|<math|-2>>>>.
  Solve Eq. <reference|ad. flame> for <math|T<rsub|2>> to estimate the flame
  temperature to the nearest kelvin.

  <\answer>
    <math|T<rsub|2>=2272<K>>
  </answer>

  <\soln>
    \ Equation <reference|ad. flame>: <math|\<xi\><Delsub|r>H<st><around|(|T<rsub|1>|)>+<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>>C<rsub|p><around|(|<tx|P>|)><dif>T=0>
    <vs>Calculate <math|<Delsub|c>H<st><around|(|T<rsub|1>|)>> from values in
    Appendix <reference|app:props> for <math|T<rsub|1>=298.15<K>>:
    <vs><math|<Delsub|c>H<st>=-<Delsub|f>H<st><tx|<around|(|C*H<rsub|<math|4>>|)>>+<Delsub|f>H<st><tx|<around|(|C*O<rsub|<math|2>>|)>>+2<space|0.17em><Delsub|f>H<st><tx|<around|(|H<rsub|<math|2>>*O,<space|0.17em>g|)>>=-802.29<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs><math|<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>>C<rsub|p><around|(|<tx|P>|)><dif>T=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><around|(|a+b*T|)><dif>T=a*<around|(|T<rsub|2>-T<rsub|1>|)>+<frac|b|2>*<around|(|T<rsub|2><rsup|2>-T<rsub|1><rsup|2>|)>>
    <vs><math|<phantom|<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>>C<rsub|p><around|(|<tx|P>|)><dif>T>=a*T<rsub|2><rsup|2>+<frac|b|2>*T<rsub|2><rsup|2>-a*T<rsub|1>-<frac|b|2>*T<rsub|1><rsup|2>>
    <vs>Write Eq. <reference|ad. flame> in the form
    <math|A*x<rsup|2>+B*x+C=0>, where <vs><math|x=T<rsub|2>>
    <vs><math|A=b/2=4.260<timesten|-2><units|J*<space|0.17em>K<rsup|<math|-2>>>>
    <vs><math|B=a=297.0<units|J*<space|0.17em>K<per>>>
    <vs><math|C=\<xi\><Delsub|c>H<st><around|(|T<rsub|1>|)>-a*T<rsub|1>-<frac|b|2>*T<rsub|1><rsup|2>>
    <vs><math|<phantom|C>=<around|(|1<mol>|)>*<around|(|-802.29<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)>-<around|(|297.0<units|J*<space|0.17em>K<per>>|)><around|(|298.15<K>|)>>
    <vs><math|<phantom|C=>-<around|(|4.260<timesten|-2><units|J*<space|0.17em>K<rsup|<math|-2>>>|)><around|(|298.15<K>|)><rsup|2>>
    <vs><math|<phantom|C>=-8.9463<timesten|5><units|J>> <vs>Solve with the
    quadratic formula: <vs><math|<D>T<rsub|2>=x=<frac|-B\<pm\><sqrt|B<rsup|2>-4*A*C>|2*A>=2272<K>>
    or <math|-9244<K>> <vs>Only the positive value is physically possible.
  </soln>

  <problemAns>The standard molar Gibbs energy of formation of crystalline
  mercury(II) oxide at <math|600.00<K>> has the value
  <math|<Delsub|f>G<st>=-26.386<units|k*J*<space|0.17em>m*o*l<per>>>.
  Estimate the partial pressure of O<rsub|<math|2>> in equilibrium with HgO
  at this temperature: <math|<tx|2*<space|0.17em>H*g*O<around|(|s|)>><arrows><tx|2*<space|0.17em>H*g<around|(|l|)>>+<tx|O<rsub|<math|2>><around|(|g|)>>>.

  <\answer>
    <math|p<tx|<around|(|O<rsub|<math|2>>|)>>=2.55<timesten|-5><br>>
  </answer>

  <\soln>
    \ <math|<Delsub|r>G<st>=-2<space|0.17em><Delsub|f>G<st><tx|<around|(|H*g*O|)>>=52.772<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs><math|<D>ln K=<frac|-<Delsub|r>G<st>|R*T>=<frac|-52.772<timesten|3><units|J*<space|0.17em>m*o*l<per>>|<around|(|<R>|)><around|(|600.00<K>|)>>=-10.578>
    <vs><math|K=2.55<timesten|-5>> <vs><math|<D>K=<frac|a<subs|H*g<around|(|l|)>><rsup|2><fug><subs|O<rsub|<math|2>>>/p<st>|a<subs|H*g*O<around|(|s|)>><rsup|2>>\<approx\>p<subs|O<rsub|<math|2>>>/p<st>>
    <vs><math|p<subs|O<rsub|<math|2>>>=K*p<st>=2.55<timesten|-5><br>>
  </soln>

  <problem>The combustion of hydrogen is a reaction that is known to \Pgo to
  completion.\Q

  <\problemparts>
    \ <problempartAns> Use data in Appendix <reference|app:props> to evaluate
    the thermodynamic equilibrium constant at <math|298.15<K>> for the
    reaction

    <\equation*>
      <chem>H<rsub|2><around|(|g|)>+<frac|1|2>*O<rsub|2><around|(|g|)><arrow>H<rsub|2>*O<around|(|l|)>
    </equation*>

    <answer|<math|K=3.5<timesten|41>>>

    <soln| <math|<Delsub|r>G<st>=<Delsub|f>G<st><tx|<around|(|H<rsub|<math|2>>*O,<space|0.17em>l|)>>=-237.16<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs><math|<D>K=exp <space|0.17em><around|(|-<Delsub|r>G<st>/R*T|)>=exp
    <frac|237.16<timesten|3><units|J*<space|0.17em>m*o*l<per>>|<around|(|<R>|)><around|(|298.15<K>|)>>=3.5<timesten|41>>>
    <problempartAns> Assume that the reaction is at equilibrium at
    <math|298.15<K>> in a system in which the partial pressure of
    O<rsub|<math|2>> is <math|1.0<br>>. Assume ideal-gas behavior and find
    the equilibrium partial pressure of H<rsub|<math|2>> and the number of
    H<rsub|<math|2>> <em|molecules> in <math|1.0<units|m<rsup|<math|3>>>> of
    the gas phase.

    <answer|<math|p<subs|H<rsub|<math|2>>>=2.8<timesten|-42><br>><next-line><math|N<subs|H<rsub|<math|2>>>=6.9<timesten|-17>>>

    <soln| <math|<D>K=<frac|a<subs|H<rsub|<math|2>>*O<around|(|l|)>>|<around|(|<fug><subs|H<rsub|<math|2>>>/p<st>|)>*<around|(|<fug><subs|O<rsub|<math|2>>>/p<st>|)><rsup|1/2>>>
    <vs>For ideal gas mixture, <math|p<subs|O<rsub|<math|2>>>=1.0<br>>:
    <vs><math|<D>K=<frac|1|<around|(|p<subs|H<rsub|<math|2>>>/p<st>|)><around|(|1.0|)><rsup|1/2>>>
    <vs><math|<D>p<subs|H<rsub|<math|2>>>=<frac|p<st>|K>=<frac|1<br>|3.5<timesten|41>>=2.8<timesten|-42><br>>
    <vs><math|<D>N<subs|H<rsub|<math|2>>>=<frac|N<subs|A>p<subs|H<rsub|<math|2>>>V|R*T>>
    <vs><math|<D><phantom|N<subs|H<rsub|<math|2>>>>=<frac|<around|(|6.022<timesten|23><units|m*o*l<per>>|)><around|(|2.8<timesten|-42><br>|)>*<around|(|10<rsup|5><Pa>/1<br>|)><around|(|1.0<units|m<rsup|<math|3>>>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>=6.9<timesten|-17>>>
    <problempartAns> In the preceding part, you calculated a very small value
    (a fraction) for the number of H<rsub|<math|2>> molecules in
    <math|1.0<units|m<rsup|<math|3>>>>. Statistically, this fraction can be
    interpreted as the fraction of a given length of time during which one
    molecule is present in the system. Take the age of the universe as
    <math|1.0<timesten|10>> years and find the total length of time in
    seconds, during the age of the universe, that a H<rsub|<math|2>> molecule
    is present in the equilibrium system. (This hypothetical value is a
    dramatic demonstration of the statement that the limiting reactant is
    essentially entirely exhausted during a reaction with a large value of
    <math|K>.)

    <answer|<math|t=22<units|s>>>

    <soln| <math|t=<around|(|6.9<timesten|-17>|)><around|(|1.0<timesten|10><units|y*e*a*r*s>|)><around|(|365<units|d*<space|0.17em>y*e*a*r<per>>|)><around|(|24<units|h*<space|0.17em>d<per>>|)><around|(|60<units|m*i*n*<space|0.17em>h<per>>|)><around|(|60<units|s*<space|0.17em>m*i*n<per>>|)>>
    <vs><math|<phantom|t>=22<units|s>>>
  </problemparts>

  <problem>Let G represent carbon in the form of <em|graphite> and D
  represent the <em|diamond> crystal form. At <math|298.15<K>>, the
  thermodynamic equilibrium constant for G<math|\<rightleftharpoons\>>D,
  based on a standard pressure <math|p<st>=1<br>>, has the value
  <math|K=0.31>. The molar volumes of the two crystal forms at this
  temperature are <math|V<m><around|(|<tx|G>|)>=5.3<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>
  and <math|V<m><around|(|<tx|D>|)>=3.4<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>.

  <\problemparts>
    \ <problempart>Write an expression for the reaction quotient
    <math|Q<subs|r*x*n>> as a function of pressure. Use the approximate
    expression of the pressure factor given in Table
    <reference|tbl:9-Gamma_i>. <soln| <math|Q<subs|r*x*n>=a<around|(|<tx|D>|)>/a<around|(|<tx|G>|)>=<G><around|(|<tx|D>|)>/<G><around|(|<tx|G>|)>>
    <vs><math|<phantom|Q<subs|r*x*n>>\<approx\><dfrac|exp
    <around*|[|V<m><around|(|<tx|D>|)>*<around|(|p-p<st>|)>/R*T|]>|exp
    <around*|[|V<m><around|(|<tx|G>|)>*<around|(|p-p<st>|)>/R*T|]>>=exp
    <dfrac|<around|[|V<m><around|(|<tx|D>|)>-V<m><around|(|<tx|G>|)>|]>*<around|(|p-p<st>|)>|R*T>>>
    <problempartAns>Use the value of <math|K> to estimate the pressure at
    which the D and G crystal forms are in equilibrium with one another at
    <math|298.15<K>>. (This is the lowest pressure at which graphite could in
    principle be converted to diamond at this temperature.)

    <answer|<math|p\<approx\>1.5<timesten|4><br>>>

    <soln| Find the pressure at which <math|Q<subs|r*x*n>> is equal to
    <math|K>: <vs><math|ln Q<subs|r*x*n>\<approx\><dfrac|<around|[|V<m><around|(|<tx|D>|)>-V<m><around|(|<tx|G>|)>|]>*<around|(|p-p<st>|)>|R*T>>
    <vs><math|<phantom|ln Q<subs|r*x*n>>=<dfrac|<around|(|3.4-5.3|)><timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>><around|(|p-p<st>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>=-7.7<timesten|-10><units|m<rsup|<math|3>>*<space|0.17em>J<per>><around|(|p-p<st>|)>>
    <vs>At equilibrium, <math|ln Q<subs|r*x*n>=ln K=ln
    <around|(|0.31|)>=-1.17> <vs><math|p\<approx\><dfrac|-1.17|-7.7<timesten|-10><units|m<rsup|<math|3>>*<space|0.17em>J<per>>>=1.5<timesten|9><Pa>=1.5<timesten|4><br>>>
  </problemparts>

  <problem><label|prb:11-G-xi>Consider the dissociation reaction
  <math|<chem>N<rsub|2>*O<rsub|4><around|(|g|)><arrow>2*<space|0.17em>N*O<rsub|2><around|(|g|)>>
  taking place at a constant temperature of <math|298.15<K>> and a constant
  pressure of <math|0.0500<br>>. Initially (at <math|\<xi\>=0>) the system
  contains <math|1.000<mol>> of N<rsub|<math|2>>O<rsub|<math|4>> and no
  NO<rsub|<math|2>>. Other needed data are found in Appendix
  <reference|app:props>. Assume ideal-gas behavior.

  <\problemparts>
    \ <problempart><label|prb:11-G-xi table>For values of the advancement
    <math|\<xi\>> ranging from 0 to <math|1<mol>>, at an interval of
    <math|0.1<mol>> or less, calculate <math|<around|[|<space|0.17em>G<around|(|\<xi\>|)>-G<around|(|0|)><space|0.17em>|]>>
    to the nearest <math|0.01<units|k*J>>. A computer spreadsheet would be a
    convenient way to make the calculations.

    <\soln>
      \ To simplify the nomenclature, write the reaction as
      <math|<chem>A<ra>2*<space|0.17em>B>. Use Eq. <reference|G(xi)-G(0)=
      (IG)><vpageref|G(xi)-G(0)= (IG)>, with <math|p=0.0500*p<st>>,
      <math|y<subs|A,<space|0.17em>0>=1>, <math|n<subs|B,<space|0.17em>0>=0>,
      <math|\<nu\><A>=-1>, and <math|\<nu\><B>=2>:
      <vs><math|G<around|(|\<xi\>|)>-G<around|(|0|)>=\<xi\><Delsub|r>G<st>+n<A>R*T*ln
      y<A>+n<B>R*T*ln y<B>+R*T*\<xi\>*ln <around|(|0.0500|)>> <vs>where
      <vs><math|<Delsub|r>G<st>=-<Delsub|f>G<st><tx|<around|(|N<rsub|<math|2>>*O<rsub|<math|4>>|)>>+2<space|0.17em><Delsub|f>G<st><tx|<around|(|N*O<rsub|<math|2>>|)>>=-<around|(|97.72<units|k*J<mol><per>>|)>+2<around|(|51.22<units|k*J<mol><per>>|)>>
      <vs><math|<phantom|<Delsub|r>G<st>>=4.72<units|k*J<mol><per>>>
      <vs><math|<D>y<A>=<frac|n<A>|n<A>+n<B>>*<space|2em>y<B>=1-y<A>>
      <vs><math|n<A>=1.000<mol>-\<xi\>*<space|2em>n<B>=2*\<xi\>>
      <vs><math|R*T=<around|(|<R>|)><around|(|298.15<K>|)>=2.4790<units|k*J<mol><per>>>
      <vs>See Table <reference|tbl:11-G-xi> for calculated values.

      <\big-table>
        <minipagetable|11cm|<label|tbl:11-G-xi><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|6|6|cell-col-span|1>|<cwith|1|1|6|6|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|13|13|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|\<xi\>/<tx|m*o*l>>>|<cell|<ccol|<math|n<A>/<tx|m*o*l>>>>|<cell|<ccol|<math|n<B>/<tx|m*o*l>>>>|<cell|<ccol|<math|y<A>>>>|<cell|<ccol|<math|y<B>>>>|<cell|<math|<around|[|G<around|(|\<xi\>|)>-G<around|(|0|)>|]>/<tx|k*J*<space|0.17em>m*o*l<per>>>>>|<row|<cell|0>|<cell|1.000>|<cell|0>|<cell|1>|<cell|0>|<cell|0>>|<row|<cell|0.1>|<cell|0.900>|<cell|0.200>|<cell|0.818>|<cell|0.182>|<cell|-1.56>>|<row|<cell|0.2>|<cell|0.800>|<cell|0.400>|<cell|0.667>|<cell|0.333>|<cell|-2.43>>|<row|<cell|0.3>|<cell|0.700>|<cell|0.600>|<cell|0.538>|<cell|0.462>|<cell|-3.04>>|<row|<cell|0.4>|<cell|0.600>|<cell|0.800>|<cell|0.429>|<cell|0.571>|<cell|-3.45>>|<row|<cell|0.5>|<cell|0.500>|<cell|1.000>|<cell|0.333>|<cell|0.667>|<cell|-3.72>>|<row|<cell|0.6>|<cell|0.400>|<cell|1.200>|<cell|0.250>|<cell|0.750>|<cell|-3.85>>|<row|<cell|0.65>|<cell|0.350>|<cell|1.300>|<cell|0.212>|<cell|0.788>|<cell|-3.87>>|<row|<cell|0.7>|<cell|0.300>|<cell|1.400>|<cell|0.176>|<cell|0.824>|<cell|-3.86>>|<row|<cell|0.8>|<cell|0.200>|<cell|1.600>|<cell|0.111>|<cell|0.889>|<cell|-3.72>>|<row|<cell|0.9>|<cell|0.100>|<cell|1.800>|<cell|0.053>|<cell|0.947>|<cell|-3.41>>|<row|<cell|1>|<cell|0>|<cell|2.000>|<cell|0>|<cell|1>|<cell|-2.71>>>>>>
      </big-table|Problem 11.<reference|prb:11-G-xi><reference|prb:11-G-xi
      table>>
    </soln>

    \ <problempart><label|prb:11-G-xi graph>Plot your values of
    <math|G<around|(|\<xi\>|)>-G<around|(|0|)>> as a function of
    <math|\<xi\>>, and draw a smooth curve through the points.

    <\soln>
      \ See Fig. <reference|fig:11-G-xi prob>.

      <\big-figure>
        <boxedfigure|<image|./11-SUP/GXI-PROB.eps||||> <capt|Problem
        11.<reference|prb:11-G-xi><reference|prb:11-G-xi graph>. The open
        circle at <math|\<xi\>=0.65<mol>> indicates the estimated position of
        <math|\<xi\><eq>>.<label|fig:11-G-xi prob>>>
      </big-figure|>
    </soln>

    \ <problempartAns>On your curve, indicate the estimated position of
    <math|\<xi\><eq>>. Calculate the activities of
    N<rsub|<math|2>>O<rsub|<math|4>> and NO<rsub|<math|2>> for this value of
    <math|\<xi\>>, use them to estimate the thermodynamic equilibrium
    constant <math|K>, and compare your result with the value of <math|K>
    calculated from Eq. <reference|K=exp(-del(r)Gmo/RT)>.

    <answer|<math|K=0.15>>

    <soln| The curve minimum is at <math|\<xi\><eq>=0.65<mol>>. The
    activities here are <math|a<A>=y<A>p/p<st>=0.0106> and
    <math|a<B>=y<B>p/p<st>=0.0394>. The thermodynamic equilibrium constant
    has the value <vs><math|<D>K=<frac|a<B><rsup|2>|a<A>>=<frac|<around|(|0.0394|)><rsup|2>|0.0106>=0.146>
    <vs>From Eq. <reference|K=exp(-del(r)Gmo/RT)>: <vs><math|<D>K=exp
    <around*|(|-<frac|<Delsub|r>G<st>|R*T>|)>=exp
    <around*|[|-<frac|4.72<timesten|3><units|J*<space|0.17em>m*o*l<per>>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>=0.15>>
  </problemparts>
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>