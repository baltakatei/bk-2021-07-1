<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|Definitions of the SI Base Units><label|app:SI>

  <pagestyle|nosectioninheader>

  <subindex|SI|base units><index|Base units>

  This appendix gives two definitions for each of the seven SI base units.
  The <em|previous definitions> are from the 2007 IUPAC Green
  Book.<footnote|Ref. <cite|greenbook-3>, Sec. 3.3.> <index|IUPAC Green
  Book>The <em|revised definitions> are from the SI revision effective
  beginning 20 May 2019.<footnote|Ref. <cite|stock-19>> Values of the
  defining constants referred to in the revised definitions are listed in
  Appendix <reference|app:const>.

  \;

  <item>The <index|Second><newterm|second>, symbol s, is the SI unit of time.

  <\itemize>
    <item><with|font-series|bold|Previous definition:> The second is the
    duration of 9<space|0.17em>192<space|0.17em>631<space|0.17em>770 periods
    of the radiation corresponding to the transition between the two
    hyperfine levels of the ground state of the cesium-133 atom.

    <item><with|font-series|bold|Revised definition:> No change from the
    previous definition. The number 9<space|0.17em>192<space|0.17em>631<space|0.17em>770
    is the numerical value of the defining constant
    <math|<Del>\<nu\><subs|C*s>> expressed in units of s<per>.
  </itemize>

  \;

  <item>The <index|Meter><newterm|meter>,<footnote|An alternative spelling is
  <index|Metre><em|metre>.> symbol <math|m>, is the SI unit of length.

  <\itemize>
    <item><with|font-series|bold|Previous definition:> The meter is the
    length of path traveled by light in vacuum during a time interval of
    1/(299<space|0.17em>792<space|0.17em>458) of a second.

    <item><with|font-series|bold|Revised definition:> No change from the
    previous definition. The number 299<space|0.17em>792<space|0.17em>458 is
    the numerical value of the defining constant <math|c> expressed in units
    of m<space|0.17em>s<per>.
  </itemize>

  \;

  <item>The <index|Kilogram><newterm|kilogram>, symbol kg, is the SI unit of
  <index|Mass>mass.

  <\itemize>
    <item><with|font-series|bold|Previous definition:> The kilogram is equal
    to the mass of the international prototype of the kilogram in S�vres,
    France.

    <item><with|font-series|bold|Revised definition:> The kilogram is defined
    using the defining constant <math|h> and the definitions of second and
    meter.
  </itemize>

  \;

  <item>The <I|Kelvin (unit)\|reg><newterm|kelvin>, symbol K, is the SI unit
  of <subindex|Thermodynamic|temperature><subindex|Temperature|thermodynamic>thermodynamic
  temperature.

  <\itemize>
    <item><with|font-series|bold|Previous definition:> The kelvin is the
    fraction 1/<math|273.16> of the thermodynamic temperature of the triple
    point of water.

    <item><with|font-series|bold|Revised definition:> The kelvin is equal to
    the change of thermodynamic temperature <math|T> that results in a change
    of the translational energy <math|<around|(|3/2|)>*k*T> of an ideal gas
    molecule by <math|<around|(|3/2|)>*1.380*<space|0.17em>649<timesten|-23><units|J>>.
    The number 1.380 649<timesten|-23> is the numerical value of the defining
    constant <math|k> expressed in units of J<space|0.17em>K<per>.
  </itemize>

  \;

  <item>The <index|Mole><newterm|mole>, symbol mol, is the SI unit of
  <index|Amount>amount of substance.

  <\itemize>
    <item><with|font-series|bold|Previous definition:> The mole is the amount
    of substance of a system which contains as many elementary entities as
    there are atoms in <math|0.012> kilogram of carbon 12.

    <item><with|font-series|bold|Revised definition:> One mole contains
    exactly 6.022 140 76<timesten|23> elementary entities. This number is the
    numerical value of the defining constant <math|N<subs|A>> expressed in
    the unit mol<per>.
  </itemize>

  \;

  <item>The <index|Ampere><newterm|ampere>, symbol A, is the SI unit of
  electric current. <subindex|Electric|current><index|Current, electric>

  <\itemize>
    <item><with|font-series|bold|Previous definition:> The ampere is that
    constant current which, if maintained in two straight parallel conductors
    of infinite length, of negligible circular cross-section, and placed 1
    meter apart in vacuum, would produce between these conductors a force
    equal to <math|2<timesten|-7>> newton per meter of length.

    <item><with|font-series|bold|Revised definition:> The ampere is defined
    as the electric current in which<next-line>1/(1.602 176
    634<timesten|-19>) elementary charges travel across a given point in one
    second. The number 1.602 176 634<timesten|-19> is the numerical value of
    the defining constant <math|e> expressed in coulombs.
  </itemize>

  \;

  <item>The <index|Candela><newterm|candela>, symbol cd, is the SI unit of
  luminous intensity.

  <\itemize>
    <item><with|font-series|bold|Previous definition:> The candela is the
    luminous intensity, in a given direction, of a source that emits
    monochromatic radiation of frequency 540<timesten|12><units|s<per>> and
    that has a radiant intensity in that direction of
    (1/683)<units|m<rsup|<math|2>> kg s<rsup|<math|-3>>> per steradian.

    <item><with|font-series|bold|Revised definition:> No change from the
    previous definition. The meter, kilogram, and second in this definition
    are defined in terms of the defining constants <math|c>, <math|h>, and
    <math|<Del>\<nu\><subs|C*s>>.
  </itemize>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|app:SI|<tuple|1|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|Kilogram|?>>
    <associate|auto-11|<tuple|kilogram|?>>
    <associate|auto-12|<tuple|Mass|?>>
    <associate|auto-13|<tuple|kelvin|?>>
    <associate|auto-14|<tuple|Thermodynamic|?>>
    <associate|auto-15|<tuple|Temperature|?>>
    <associate|auto-16|<tuple|Mole|?>>
    <associate|auto-17|<tuple|mole|?>>
    <associate|auto-18|<tuple|Amount|?>>
    <associate|auto-19|<tuple|Ampere|?>>
    <associate|auto-2|<tuple|SI|?>>
    <associate|auto-20|<tuple|ampere|?>>
    <associate|auto-21|<tuple|Electric|?>>
    <associate|auto-22|<tuple|Current, electric|?>>
    <associate|auto-23|<tuple|Candela|?>>
    <associate|auto-24|<tuple|candela|?>>
    <associate|auto-3|<tuple|Base units|?>>
    <associate|auto-4|<tuple|IUPAC Green Book|?>>
    <associate|auto-5|<tuple|Second|?>>
    <associate|auto-6|<tuple|second|?>>
    <associate|auto-7|<tuple|Meter|?>>
    <associate|auto-8|<tuple|meter|?>>
    <associate|auto-9|<tuple|Metre|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|Metre|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      greenbook-3

      stock-19
    </associate>
    <\associate|gly>
      <tuple|normal|second|<pageref|auto-6>>

      <tuple|normal|meter|<pageref|auto-8>>

      <tuple|normal|kilogram|<pageref|auto-11>>

      <tuple|normal|kelvin|<pageref|auto-13>>

      <tuple|normal|mole|<pageref|auto-17>>

      <tuple|normal|ampere|<pageref|auto-20>>

      <tuple|normal|candela|<pageref|auto-24>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|SI|base units>|<pageref|auto-2>>

      <tuple|<tuple|Base units>|<pageref|auto-3>>

      <tuple|<tuple|IUPAC Green Book>|<pageref|auto-4>>

      <tuple|<tuple|Second>|<pageref|auto-5>>

      <tuple|<tuple|Meter>|<pageref|auto-7>>

      <tuple|<tuple|Metre>|<pageref|auto-9>>

      <tuple|<tuple|Kilogram>|<pageref|auto-10>>

      <tuple|<tuple|Mass>|<pageref|auto-12>>

      <tuple|<tuple|Thermodynamic|temperature>|<pageref|auto-14>>

      <tuple|<tuple|Temperature|thermodynamic>|<pageref|auto-15>>

      <tuple|<tuple|Mole>|<pageref|auto-16>>

      <tuple|<tuple|Amount>|<pageref|auto-18>>

      <tuple|<tuple|Ampere>|<pageref|auto-19>>

      <tuple|<tuple|Electric|current>|<pageref|auto-21>>

      <tuple|<tuple|Current, electric>|<pageref|auto-22>>

      <tuple|<tuple|Candela>|<pageref|auto-23>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Definitions
      of the SI Base Units> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>