<TeXmacs|1.99.21>

<style|<tuple|book|std-latex>>

<\body>
  <label|Chap13>

  <problem>Consider a single-phase system that is a gaseous mixture of
  N<rsub|<math|2>>, H<rsub|<math|2>>, and NH<rsub|<math|3>>. For each of the
  following cases, find the number of degrees of freedom and give an example
  of the independent intensive variables that could be used to specify the
  equilibrium state, apart from the total amount of gas.

  <\problemparts>
    \ <problempartAns>There is no reaction.

    <answer|<math|F=4>>

    <soln| There are three components: N<rsub|<math|2>>, H<rsub|<math|2>>,
    and NH<rsub|<math|3>>. The number of degrees of freedom is
    <vs><math|F=2+C-P=2+3-1=4> <vs>The equilibrium state could be specified
    by <math|T>, <math|p>, and the mole fractions of two of the substances;
    or by <math|T> and the partial pressures of each of the substances.>

    \;

    <problempartAns>The reaction <math|<chem>N<rsub|2><around|(|g|)>+3*H<rsub|2><around|(|g|)><arrow>2*N*H<rsub|3><around|(|g|)>>
    is at equilibrium.

    <answer|<math|F=3>>

    <soln| There are three species and one independent relation for reaction
    equilibrium, <math|-\<mu\><subs|N<rsub|<math|2>>>-3*\<mu\><subs|H<rsub|<math|2>>>+2*\<mu\><subs|N*H<rsub|<math|3>>>=0>.
    <vs><math|F=2+s-r-P=2+3-1-1=3> <vs>One possibility would be to specify
    the equilibrium state by values of <math|T> and the partial pressures of
    two of the gases; the partial pressure of the third gas would be
    determined by the thermodynamic equilibrium constant and the fugacity
    coefficients, and <math|p> would be the sum of the three partial
    pressures.>\ 

    \;

    <problempartAns>The reaction is at equilibrium and the system is prepared
    from NH<rsub|<math|3>> only.

    <answer|<math|F=2>>

    <soln| There are three species and two independent relations among
    intensive variables: <math|-\<mu\><subs|N<rsub|<math|2>>>-3*\<mu\><subs|H<rsub|<math|2>>>+2*\<mu\><subs|N*H<rsub|<math|3>>>=0>
    and <math|y<subs|H<rsub|<math|2>>>=3*y<subs|N<rsub|<math|2>>>>.
    <vs><math|F=2+s-r-P=2+3-2-1=2> <vs>The equilibrium state could be
    specified by <math|T> and <math|p>. The partial pressures of the three
    gases would be those that satisfy the relations
    <math|-\<mu\><subs|N<rsub|<math|2>>>-3*\<mu\><subs|H<rsub|<math|2>>>+2*\<mu\><subs|N*H<rsub|<math|3>>>=0>,
    <math|y<subs|H<rsub|<math|2>>>=3*y<subs|N<rsub|<math|2>>>>, and
    <math|p<subs|N<rsub|<math|2>>>+p<subs|H<rsub|<math|2>>>+p<subs|N*H<rsub|<math|3>>>=p>.>
  </problemparts>

  \;

  <problem>How many components has a mixture of water and deuterium oxide in
  which the equilibrium <math|<chem>H<rsub|2>*O+D<rsub|2>*O<arrows>2*H*D*O>
  exists?

  <soln| There are three species and one relation among intensive variables,
  <math|-\<mu\><subs|H<rsub|<math|2>>*O>-\<mu\><subs|D<rsub|<math|2>>*O>+2*\<mu\><subs|H*D*O>=0>.
  <vs><math|C=s-r=3-1=2>>

  \;

  <problem>Consider a system containing only NH<rsub|<math|4>>Cl(s),
  NH<rsub|<math|3>>(g), and HCl(g). Assume that the equilibrium
  <math|<chem>N*H<rsub|4>*C*l<around|(|s|)><arrows>N*H<rsub|3><around|(|g|)>+H*C*l<around|(|g|)>>
  exists.

  <\problemparts>
    <problempart>Suppose you prepare the system by placing solid
    NH<rsub|<math|4>>Cl in an evacuated flask and heating to <math|400<K>>.
    Use the phase rule to decide whether you can vary the pressure while both
    phases remain in equilibrium at <math|400<K>>.

    <soln| There are three species and two relations among intensive
    variables: <math|-\<mu\><subs|N*H<rsub|<math|4>>*Cl>+\<mu\><subs|N*H<rsub|<math|3>>>+\<mu\><subs|H*C*l>=0>
    and <math|y<subs|N*H<rsub|<math|3>>>=y<subs|H*C*l>> (or
    <math|p<subs|N*H<rsub|<math|3>>>=p<subs|H*C*l>>).
    <vs><math|F=2+s-r-P=2+3-2-2=1> <vs>The two phases cannot remain in
    equilibrium while <math|p> is varied and <math|T> is fixed.>\ 

    \;

    <problempart>According to the phase rule, if the system is not prepared
    as described in part (a) could you vary the pressure while both phases
    remain in equilibrium at <math|400<K>>? Explain.

    <soln| Yes, because <math|y<subs|N*H<rsub|<math|3>>>> no longer has to
    equal <math|y<subs|H*C*l>> and <math|F> is increased to 2. <math|T> and
    <math|p> can be varied independently, and the pressure can be varied
    while the temperature is fixed at an arbitrary value.>

    \;

    <problempart>Rationalize your conclusions for these two cases on the
    basis of the thermodynamic equilibrium constant. Assume that the gas
    phase is an ideal gas mixture and use the approximate expression
    <math|K=p<subs|N*H<rsub|<math|3>>>p<subs|H*C*l>/<around|(|p<st>|)><rsup|2>>.

    <soln| At a fixed temperature, <math|K> has a fixed value. If the system
    is prepared from NH<rsub|<math|4>>Cl(s) only, then the following
    relations hold <vs><math|p=p<subs|N*H<rsub|<math|3>>>+p<subs|H*C*l><space|2em>p<subs|N*H<rsub|<math|3>>>=p<subs|H*C*l>=p/2*<space|2em>K=<around|(|p/2|)><rsup|2>/<around|(|p<st>|)><rsup|2>*<space|2em>p=2*<sqrt|K>*p<st>>
    <vs>and therefore <math|p> can have only one value at each temperature.
    If, however, <math|p<subs|N*H<rsub|<math|3>>>> and <math|p<subs|H*C*l>>
    can be varied independently, then the relation
    <math|K=p<subs|N*H<rsub|<math|3>>>p<subs|H*C*l>/<around|(|p<st>|)><rsup|2>>
    can be satisfied for a fixed value of <math|K> and a varying value of
    <math|p=p<subs|N*H<rsub|<math|3>>>+p<subs|H*C*l>>.>
  </problemparts>

  \;

  <problem>Consider the lime-kiln process
  <math|<chem>C*a*C*O<rsub|3><around|(|s|)><ra>C*a*O<around|(|s|)>+C*O<rsub|2><around|(|g|)>>.
  Find the number of intensive variables that can be varied independently in
  the equilibrium system under the following conditions:

  <\problemparts>
    <problempart>The system is prepared by placing calcium carbonate, calcium
    oxide, and carbon dioxide in a container.

    <soln| There are three species, three phases, and one relation among
    intensive variables: <vs><math|-\<mu\><subs|C*a*C*O<rsub|<math|3>>>+\<mu\><subs|C*a*O>+\<mu\><subs|C*O<rsub|<math|2>>>=0>
    <vs><math|F=2+s-r-P=2+3-1-3=1> <vs>Only one intensive variable, such as
    <math|T>, can be varied independently while the three phases remain in
    equilibrium.>

    \;

    <problempart>The system is prepared from calcium carbonate only.\ 

    <soln| Only one intensive variable can be varied, as before; the initial
    condition leads to no relation among intensive variables.>

    \;

    <problempart>The temperature is fixed at <math|1000<K>>.

    <soln| If <math|T> is fixed, no other intensive variable can be varied.>
  </problemparts>

  \;

  <problem>What are the values of <math|C> and <math|F> in systems consisting
  of solid AgCl in equilibrium with an aqueous phase containing
  H<rsub|<math|2>>O, Ag<rsup|<math|+>>(aq), Cl<rsup|<math|->>(aq),
  Na<rsup|<math|+>>(aq), and NO<math|<rsub|3><rsup|->>(aq) prepared in the
  following ways? Give examples of intensive variables that could be varied
  independently.

  <\problemparts>
    \ <problempart>The system is prepared by equilibrating excess solid AgCl
    with an aqueous solution of NaNO<rsub|<math|3>>.

    <soln| Both phases were prepared from either the first or all three of
    the substances AgCl, H<rsub|<math|2>>O, and NaNO<rsub|<math|3>>; thus,
    there are three components: <vs><math|C=3*<space|2em>F=2+C-P=2+3-2=3>.
    <vs><math|T>, <math|p>, and <math|m<subs|N*a<rsup|<math|+>>>> could be
    varied independently.>

    \;

    <problempart>The system is prepared by mixing aqueous solutions of
    AgNO<rsub|<math|3>> and NaCl in arbitrary proportions; some solid AgCl
    forms by precipitation.

    <soln| There are six species and two independent relations among
    intensive variables: <vs><math|-\<mu\><subs|A*g*C*l>+\<mu\><subs|A*g<rsup|<math|+>>>+\<mu\><subs|C*l<rsup|<math|->>>=0>
    <vs><math|m<subs|A*g<rsup|<math|+>>>+m<subs|N*a<rsup|<math|+>>>=m<subs|C*l<rsup|<math|->>>+m<subs|N*O<math|<rsub|3><rsup|->>>>
    <vs>From the phase rule we find <vs><math|C=s-r=4*<space|2em>F=2+C-P=2+4-2=4>
    <vs><math|T>, <math|p>, and the molalities of any two of the aqueous ions
    could be varied independently.>
  </problemparts>

  \;

  <problem>How many degrees of freedom has a system consisting of solid NaCl
  in equilibrium with an aqueous phase containing H<rsub|<math|2>>O,
  Na<rsup|<math|+>>(aq), Cl<rsup|<math|->>(aq), H<rsup|<math|+>>(aq), and
  OH<rsup|<math|->>(aq)? Would it be possible to independently vary <math|T>,
  <math|p>, and <math|m<subs|O*H<rsup|<math|->>>>? If so, explain how you
  could do this.

  <soln| There are six species, two phases, and three independent relations
  among intensive variables: <vs><math|-\<mu\><subs|N*a*C*l>+\<mu\><subs|N*a<rsup|<math|+>>>+\<mu\><subs|C*l<rsup|<math|->>>=0>
  <vs><math|-\<mu\><subs|H<rsub|<math|2>>*O>+\<mu\><subs|H<rsup|<math|+>>>+\<mu\><subs|O*H<rsup|<math|->>>=0>
  <vs><math|m<subs|N*a<rsup|<math|+>>>+m<subs|H<rsup|<math|+>>>=m<subs|C*l<rsup|<math|->>>+m<subs|O*H<rsup|<math|->>>>
  <vs>The system has three degrees of freedom: <vs><math|F=2+s-r-P=2+6-3-2=3>
  <vs><math|m<subs|O*H<rsup|<math|->>>> may be varied at any given <math|T>
  and <math|p> by dissolving HCl or NaOH in the aqueous phase.>

  \;

  <problem>Consult the phase diagram shown in Fig.
  <reference|fig:13-H2O-NaCl><vpageref|fig:13-H2O-NaCl>. Suppose the system
  contains <math|36.0<units|g>> (<math|2.00<mol>>) H<rsub|<math|2>>O and
  <math|58.4<units|g>> (<math|1.00<mol>>) NaCl at <math|25 <degC>> and
  <math|1<br>>.

  <\problemparts>
    <problempart>Describe the phases present in the equilibrium system and
    their masses.

    <soln| The mass percent NaCl in the system as a whole is
    <vs><math|<D><frac|58.4<units|g>|36.0<units|g>+58.4<units|g>>\<times\>100=61.9%>
    <vs>The system point at 61.9% NaCl by mass and <math|25 <degC>> lies in
    the two-phase area labeled sln+NaCl(s). The left end of the tie line
    drawn through the system point is at 26% NaCl by mass. The lever rule
    gives the relation <vs><math|<D><frac|m<sups|s*l*n>|m<sups|s>>=<frac|100-61.9|61.9-26>=1.06>
    <vs>where <math|m> is mass. Solve this equation simultaneously with
    <math|m<sups|s>+m<sups|s*l*n>=94.4<units|g>>:
    <vs><math|m<sups|s*l*n>=48.6<units|g><space|2em>m<sups|s>=45.8<units|g>>
    <vs>The system contains approximately <math|49<units|g>> solution of
    composition 26% NaCl by mass, and <math|46<units|g>> solid NaCl.>

    \;

    <problempart>Describe the changes that occur at constant pressure if the
    system is placed in thermal contact with a heat reservoir at <math|-30
    <degC>>.

    <soln| As the temperature decreases, some NaCl precipitates from the
    solution. The temperature drop halts at <math|0 <degC>> while the system
    is converted completely to solid <math|<chem>N*a*C*l\<cdot\>2*H<rsub|2>*O>;
    then the solid cools to <math|-30 <degC>>.>

    \;

    <problempart>Describe the changes that occur if the temperature is raised
    from <math|25 <degC>> to <math|120 <degC>> at constant pressure.

    <soln| As the temperature increases, some of the solid NaCl dissolves in
    the solution. The temperature rise halts at <math|109 <degC>> until all
    the water vaporizes. At <math|120 <degC>> the system contains
    <math|1.00<mol>> of solid NaCl and <math|2.00<mol>> of gaseous
    H<rsub|<math|2>>O<@>.>

    \;

    <problempart>Describe the system after <math|200<units|g>>
    H<rsub|<math|2>>O is added at <math|25 <degC>>.

    <soln| The solid NaCl dissolves to give a single phase, an unsaturated
    solution of composition 19.8% NaCl by mass.>
  </problemparts>

  <\big-table>
    <tabular*|<tformat|<table|<row|<cell|<math|<chem>N*a<rsub|2>*S*O<rsub|4>\<cdot\>10*H<rsub|2>*O>>|<cell|>|<cell|<space|2em>>|<cell|<math|<chem>N*a<rsub|2>*S*O<rsub|4>>>|<cell|>>|<row|<cell|<ccol|<math|t/<degC>>>>|<cell|<ccol|<math|x<B>>>>|<cell|>|<cell|<ccol|<math|t/<degC>>>>|<cell|<ccol|<math|x<B>>>>>|<row|<cell|10>|<cell|0.011>|<cell|>|<cell|40>|<cell|0.058>>|<row|<cell|15>|<cell|0.016>|<cell|>|<cell|50>|<cell|0.056>>|<row|<cell|20>|<cell|0.024>|<cell|>|<cell|>|<cell|>>|<row|<cell|25>|<cell|0.034>|<cell|>|<cell|>|<cell|>>|<row|<cell|30>|<cell|0.048>|<cell|>|<cell|>|<cell|>>>>>
  </big-table|<label|tbl:13-H2O+Na2SO4>Aqueous solubilities of sodium sulfate
  decahydrate and anhydrous sodium sulfate<footnote|Ref. <cite|findlay-45>,
  p. 179--180.>>

  <problem>Use the following information to draw a temperature\Ucomposition
  phase diagram for the binary system of H<rsub|<math|2>>O (A) and
  Na<rsub|<math|2>>SO<rsub|<math|4>> (B) at <math|p=1<br>>, confining
  <math|t> to the range <math|-20> to <math|50 <degC>> and <math|z<B>> to the
  range <math|0>\U<math|0.2>. The solid decahydrate,
  <math|<chem>N*a<rsub|2>*S*O<rsub|4>\<cdot\>10*H<rsub|2>*O>, is stable below
  <math|32.4 <degC>>. The anhydrous salt,
  <math|<chem>N*a<rsub|2>*S*O<rsub|4>>, is stable above this temperature.
  There is a peritectic point for these two solids and the solution at
  <math|x<B>=0.059> and <math|t=32.4 <degC>>. There is a eutectic point for
  ice, <math|<chem>N*a<rsub|2>*S*O<rsub|4>\<cdot\>10*H<rsub|2>*O>, and the
  solution at <math|x<B>=0.006> and <math|t=-1.3 <degC>>. Table
  <reference|tbl:13-H2O+Na2SO4><vpageref|tbl:13-H2O+Na2SO4> gives the
  temperature dependence of the solubilities of the ionic solids.

  <\soln>
    \ See Fig. <reference|fig:13-H2O-Na2SO4>.

    <\big-figure>
      <boxedfigure|<image|./13-SUP/Na2SO4.eps||||> <capt|Phase diagram for
      the binary system of H<rsub|<math|2>>O (A) and
      Na<rsub|<math|2>>SO<rsub|<math|4>> (B) at <math|p=1<br>>. Open circles
      are data points given in problem.<label|fig:13-H2O-Na2SO4>>>
    </big-figure|>
  </soln>

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|13|13|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|x<A>>>|<cell|<math|t/<degC>>>|<cell|<space|2em>>|<cell|<math|x<A>>>|<cell|<math|t/<degC>>>|<cell|<space|2em>>|<cell|<math|x<A>>>|<cell|<math|t/<degC>>>>|<row|<cell|0.000>|<cell|0.0>|<cell|>|<cell|0.119>|<cell|35.0>|<cell|>|<cell|0.286>|<cell|56.0>>|<row|<cell|0.020>|<cell|<math|-10.0>>|<cell|>|<cell|0.143>|<cell|37.0>|<cell|>|<cell|0.289>|<cell|55.0>>|<row|<cell|0.032>|<cell|<math|-20.5>>|<cell|>|<cell|0.157>|<cell|36.0>|<cell|>|<cell|0.293>|<cell|60.0>>|<row|<cell|0.037>|<cell|<math|-27.5>>|<cell|>|<cell|0.173>|<cell|33.0>|<cell|>|<cell|0.301>|<cell|69.0>>|<row|<cell|0.045>|<cell|<math|-40.0>>|<cell|>|<cell|0.183>|<cell|30.0>|<cell|>|<cell|0.318>|<cell|72.5>>|<row|<cell|0.052>|<cell|<math|-55.0>>|<cell|>|<cell|0.195>|<cell|27.4>|<cell|>|<cell|0.333>|<cell|73.5>>|<row|<cell|0.053>|<cell|<math|-41.0>>|<cell|>|<cell|0.213>|<cell|32.0>|<cell|>|<cell|0.343>|<cell|72.5>>|<row|<cell|0.056>|<cell|<math|-27.0>>|<cell|>|<cell|0.222>|<cell|32.5>|<cell|>|<cell|0.358>|<cell|70.0>>|<row|<cell|0.076>|<cell|0.0>|<cell|>|<cell|0.232>|<cell|30.0>|<cell|>|<cell|0.369>|<cell|66.0>>|<row|<cell|0.083>|<cell|10.0>|<cell|>|<cell|0.238>|<cell|35.0>|<cell|>|<cell|0.369>|<cell|80.0>>|<row|<cell|0.093>|<cell|20.0>|<cell|>|<cell|0.259>|<cell|50.0>|<cell|>|<cell|0.373>|<cell|100.0>>|<row|<cell|0.106>|<cell|30.0>|<cell|>|<cell|0.277>|<cell|55.0>|<cell|>|<cell|>|<cell|>>>>>
  </big-table|<label|tbl:13-FeCl3-H2O>Data for Problem
  13.<reference|prb:13-FeCl3 solns>. Temperatures of saturated solutions of
  aqueous iron(III) chloride at <math|p=1<br>> (A =
  <math|<chem>F*e*C*l<rsub|3>>, B = <math|<chem>H<rsub|2>*O>)<footnote|Data
  from Ref. <cite|findlay-45>, page 193.> >

  \;

  <problem><label|prb:13-FeCl3 solns> Iron(III) chloride forms various solid
  hydrates, all of which melt congruently. Table
  <reference|tbl:13-FeCl3-H2O><vpageref|tbl:13-FeCl3-H2O> lists the
  temperatures <math|t> of aqueous solutions of various compositions that are
  saturated with respect to a solid phase.

  \;

  <\problemparts>
    \ <problempart>Use these data to construct a <math|t>--<math|z<B>> phase
    diagram for the binary system of <math|<chem>F*e*C*l<rsub|3>> (A) and
    <math|<chem>H<rsub|2>*O> (B). Identify the formula and melting point of
    each hydrate. Hint: derive a formula for the mole ratio <math|n<B>/n<A>>
    as a function of <math|x<A>> in a binary mixture.

    <\soln>
      \ See Fig. <reference|fig:13-FeCl3-H2O>.

      <\big-figure>
        <boxedfigure|<image|./13-SUP/FeCl3-H2O.eps||||> <capt|Phase diagram
        for the binary system of <math|<chem>F*e*C*l<rsub|3>> (A) and
        <math|<chem>H<rsub|2>*O> (B). Solid circles: data
        points.<label|fig:13-FeCl3-H2O>>>
      </big-figure|>

      <vs><math|<D><frac|n<B>|n<A>>=<frac|x<B>|x<A>>=<frac|1-x<A>|x<A>>>
      <vs>The stoichiometry of each hydrate is given by the mole ratio of the
      liquid mixture with the same composition: <vs><math|x<A>=0.143>,
      <math|n<B>/n<A>=5.99>: <math|<chem>A*B<rsub|6>> or
      <math|<chem>F*e*C*l<rsub|3>\<cdot\>6*H<rsub|2>*O>
      <vs><math|x<A>=0.222>, <math|n<B>/n<A>=3.50>:
      <math|<chem>A<rsub|2>*B<rsub|7>> or
      <math|<chem><around|(|F*e*C*l<rsub|3>|)><rsub|2>\<cdot\>7*H<rsub|2>*O>
      <vs><math|x<A>=0.286>, <math|n<B>/n<A>=2.50>:
      <math|<chem>A<rsub|2>*B<rsub|5>> or
      <math|<chem><around|(|F*e*C*l<rsub|3>|)><rsub|2>\<cdot\>5*H<rsub|2>*O>
      <vs><math|x<A>=0.333>, <math|n<B>/n<A>=2.00>: <math|<chem>A*B<rsub|2>>
      or <math|<chem>F*e*C*l<rsub|3>\<cdot\>2*H<rsub|2>*O>
    </soln>

    \;

    <problempart>For the following conditions, determine the phase or phases
    present at equilibrium and the composition of each.

    <\enumerate>
      <item><math|t=-70.0<degC>> and <math|z<A>=0.100>

      <item><math|t=50.0<degC>> and <math|z<A>=0.275>
    </enumerate>

    <soln| See points 1 and 2 on the phase diagram. <vs>At
    <math|t=-70.0<degC>> and <math|z<A>=0.100>, the phases are solid
    <math|<chem>H<rsub|2>*O> (ice) and solid hydrate
    <math|<chem>A*B<rsub|6>>. <vs>At <math|t=50.0<degC>> and
    <math|z<A>=0.275>, the phases are solution of composition
    <math|x<B>=0.259> and solid hydrate <math|<chem>A<rsub|2>*B<rsub|5>>.>
  </problemparts>

  \;

  <\big-figure>
    <boxedfigure|<image|./13-SUP/phenol.eps||||>
    <capt|Temperature--composition phase diagram for the binary system of
    water (A) and phenol (B) at <math|1<br>>.<footnote|Ref.
    <cite|findlay-45>, p. 95.> Only liquid phases are
    present.<label|fig:13-H2O-phenol>>>
  </big-figure|>

  \;

  <problem><label|prb:13-phenol> Figure <reference|fig:13-H2O-phenol><vpageref|fig:13-H2O-phenol>
  is a temperature\Ucomposition phase diagram for the binary system of water
  (A) and phenol (B) at <math|1<br>>. These liquids are partially miscible
  below <math|67 <degC>>. Phenol is more dense than water, so the layer with
  the higher mole fraction of phenol is the bottom layer. Suppose you place
  <math|4.0<mol>> of H<rsub|<math|2>>O and <math|1.0<mol>> of phenol in a
  beaker at <math|30 <degC>> and gently stir to allow the layers to
  equilibrate.

  \;

  <\problemparts>
    \ <problempartAns>What are the compositions of the equilibrated top and
    bottom layers?

    <answer|<math|x<B><tx|<around|(|t*o*p|)>>=0.02>,
    <math|x<B><tx|<around|(|b*o*t*t*o*m|)>>=0.31>>

    <soln| The system point is at <math|z<B>=<around|(|1.0<mol>|)>/<around|(|5.0<mol>|)>=0.20>
    and <math|t=30 <degC>>, in the two-phase area. The ends of the tie line
    through this point give the compositions
    <math|x<B><tx|<around|(|t*o*p|)>>=0.02> and
    <math|x<B><tx|<around|(|b*o*t*t*o*m|)>>=0.31>.>

    \;

    <problempartAns>Find the amount of each component in the bottom layer.

    <answer|<math|n<A>=2.1<mol>>, <math|n<B>=1.0<mol>>>

    <soln| The lever rule gives the relation
    <vs><math|<D><frac|n<tx|<around|(|b*o*t*t*o*m|)>>|n<tx|<around|(|t*o*p|)>>>=<frac|0.20-0.02|0.31-0.20>=1.6>
    <vs>Solve simultaneously with <math|n<tx|<around|(|b*o*t*t*o*m|)>>+n<tx|<around|(|t*o*p|)>>=5.0<mol>>
    to get <math|n<tx|<around|(|b*o*t*t*o*m|)>>=3.1<mol>>. Then, in the
    bottom layer we have <vs><math|n<B>=x<B>n=<around|(|0.31|)><around|(|3.1<mol>|)>=1.0<mol><space|2em>n<A>=3.1<mol>-1.0<mol>=2.1<mol>>>\ 

    \;

    <problempart>As you gradually stir more phenol into the beaker,
    maintaining the temperature at <math|30 <degC>>, what changes occur in
    the volumes and compositions of the two layers? Assuming that one layer
    eventually disappears, what additional amount of phenol is needed to
    cause this to happen?

    <soln| The volume of the bottom layer increases and that of the top layer
    decreases. There is no change in the compositions of the two phases. When
    the overall system composition reaches <math|z<B>=0.31>, the top layer
    disappears; the amount of phenol in the system at this point is
    calculated from <vs><math|<D>z<B>=<frac|n<B>|n<A>+n<B>>*<space|2em>n<B>=<frac|z<B>n<A>|1-z<B>>=<frac|<around|(|0.31|)><around|(|4.0<mol>|)>|1-0.31>=1.8<mol>>
    <vs>Since <math|1.0<mol>> phenol was present initially, the top layer
    disappears when an additional <math|0.8<mol>> phenol has been added.>
  </problemparts>

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|3|3|cell-col-span|1>|<cwith|1|1|3|3|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|4|4|1|-1|cell-bborder|1ln>|<table|<row|<cell|<ccol|<math|t/<degC>>>>|<cell|<ccol|<math|p<A><rsup|\<ast\>>/<tx|b*a*r>>>>|<cell|<math|p<B><rsup|\<ast\>>/<tx|b*a*r>>>>|<row|<cell|-10.0>|<cell|3.360>|<cell|0.678>>|<row|<cell|-20.0>|<cell|2.380>|<cell|0.441>>|<row|<cell|-30.0>|<cell|1.633>|<cell|0.275>>>>>
  </big-table|<label|tbl:13-prop-but>Saturation vapor pressures of propane
  (A) and <em|n>-butane (B)>

  <problem>The standard boiling point of propane is <math|-41.8 <degC>> and
  that of <em|n>-butane is <math|-0.2 <degC>>. Table
  <reference|tbl:13-prop-but><vpageref|tbl:13-prop-but> lists vapor pressure
  data for the pure liquids. Assume that the liquid mixtures obey Raoult's
  law.\ 

  \;

  <\problemparts>
    <problempart>Calculate the compositions, <math|x<A>>, of the liquid
    mixtures with boiling points of <math|-10.0 <degC>>, <math|-20.0 <degC>>,
    and <math|-30.0 <degC>> at a pressure of <math|1<br>>.

    <soln| Solve Eq. <reference|p=pB*+(pA*-pB*)xA(l)> for <math|x<A>>:
    <vs><math|<D>x<A>=<frac|p-p<B><rsup|\<ast\>>|p<A><rsup|\<ast\>>-p<B><rsup|\<ast\>>>>
    <vs>Set <math|p> equal to <math|1<br>> and take values of
    <math|p<A><rsup|\<ast\>>> and <math|p<B><rsup|\<ast\>>> from the table.
    <vs>at <math|-10 <degC>>: <math|x<A>=0.120> <vs>at <math|-20 <degC>>:
    <math|x<A>=0.288> <vs>at <math|-30 <degC>>: <math|x<A>=0.534>>

    \;

    <problempart>Calculate the compositions, <math|y<A>>, of the equilibrium
    vapor at these three temperatures.

    <soln| Use Eq. <reference|xA(g)=>: <math|y<A>=x<A>p<A><rsup|\<ast\>>/p>.
    <vs>at <math|-10 <degC>>: <math|y<A>=0.403> <vs>at <math|-20 <degC>>:
    <math|y<A>=0.686> <vs>at <math|-30 <degC>>: <math|y<A>=0.872>>\ 

    \;

    <problempart>Plot the temperature--composition phase diagram at
    <math|p=1<br>> using these data, and label the areas appropriately.

    <\soln>
      \ See Fig. <reference|fig:13-propane-butane>.

      <\big-figure>
        <boxedfigure|<image|./13-SUP/Pr-But.eps||||> <capt|Phase diagram for
        the binary system of propane (A) and <em|n>-butane at
        <math|1<br>>.<label|fig:13-propane-butane>>>
      </big-figure|>
    </soln>

    \ 

    <problempart>Suppose a system containing <math|10.0<mol>> propane and
    <math|10.0<mol>> <em|n>-butane is brought to a pressure of <math|1<br>>
    and a temperature of <math|-25 <degC>>. From your phase diagram, estimate
    the compositions and amounts of both phases.

    <soln| From the ends of the tie line (dashed line) in Fig.
    <reference|fig:13-propane-butane>: <vs><math|x<A>=0.39>, <math|y<A>=0.79>
    <vs>Use the lever rule: <vs><math|<D><frac|n<sups|g>|n<sups|l>>=<frac|z<A>-x<A>|y<A>-z<A>>=<frac|0.50-0.39|0.79-0.50>=0.38>
    <vs>Solve this equation simultaneously with
    <math|n<sups|l>+n<sups|g>=20.0<mol>>: <vs><math|n<sups|l>=14.5<mol>>,
    <math|n<sups|g>=5.5<mol>>>
  </problemparts>

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|7|7|cell-col-span|1>|<cwith|1|1|7|7|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|8|8|1|-1|cell-bborder|1ln>|<table|<row|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|y<A>>>>|<cell|<ccol|<math|p/<tx|k*P*a>>>>|<cell|<space|2em>>|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|y<A>>>>|<cell|<math|p/<tx|k*P*a>>>>|<row|<cell|0>|<cell|0>|<cell|29.89>|<cell|>|<cell|0.5504>|<cell|0.3692>|<cell|35.32>>|<row|<cell|0.0472>|<cell|0.1467>|<cell|33.66>|<cell|>|<cell|0.6198>|<cell|0.3951>|<cell|34.58>>|<row|<cell|0.0980>|<cell|0.2066>|<cell|35.21>|<cell|>|<cell|0.7096>|<cell|0.4378>|<cell|33.02>>|<row|<cell|0.2047>|<cell|0.2663>|<cell|36.27>|<cell|>|<cell|0.8073>|<cell|0.5107>|<cell|30.28>>|<row|<cell|0.2960>|<cell|0.2953>|<cell|36.45>|<cell|>|<cell|0.9120>|<cell|0.6658>|<cell|25.24>>|<row|<cell|0.3862>|<cell|0.3211>|<cell|36.29>|<cell|>|<cell|0.9655>|<cell|0.8252>|<cell|21.30>>|<row|<cell|0.4753>|<cell|0.3463>|<cell|35.93>|<cell|>|<cell|1.0000>|<cell|1.0000>|<cell|18.14>>>>>
  </big-table|<label|tbl:13-isoPrOH-benz>Liquid and gas compositions in the
  two-phase system of 2-propanol (A) and benzene at <math|45
  <degC>><footnote|Ref. <cite|brown-56>.>>

  \;

  <problem>Use the data in Table <reference|tbl:13-isoPrOH-benz><vpageref|tbl:13-isoPrOH-benz>
  to draw a pressure\Ucomposition phase diagram for the 2-propanol\Ubenzene
  system at <math|45 <degC>>. Label the axes and each area.

  <\soln>
    \ See Fig. <reference|fig:13-isoPrOH-benz>.

    <\big-figure>
      <boxedfigure|<image|./13-SUP/PrOH-ben.eps||||> <capt|Phase diagram for
      the binary system of 2-propanol (A) and benzene at <math|45
      <degC>>.<label|fig:13-isoPrOH-benz>>>
    </big-figure|>

    The system exhibits a minimum-boiling azeotrope.
  </soln>

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|7|7|cell-col-span|1>|<cwith|1|1|7|7|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|8|8|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|x<A>>>|<cell|<ccol|<math|y<A>>>>|<cell|<ccol|<math|p/<tx|k*P*a>>>>|<cell|<space|2em>>|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|y<A>>>>|<cell|<math|p/<tx|k*P*a>>>>|<row|<cell|0>|<cell|0>|<cell|39.08>|<cell|>|<cell|0.634>|<cell|0.727>|<cell|36.29>>|<row|<cell|0.083>|<cell|0.046>|<cell|37.34>|<cell|>|<cell|0.703>|<cell|0.806>|<cell|38.09>>|<row|<cell|0.200>|<cell|0.143>|<cell|34.92>|<cell|>|<cell|0.815>|<cell|0.896>|<cell|40.97>>|<row|<cell|0.337>|<cell|0.317>|<cell|33.22>|<cell|>|<cell|0.877>|<cell|0.936>|<cell|42.62>>|<row|<cell|0.413>|<cell|0.437>|<cell|33.12>|<cell|>|<cell|0.941>|<cell|0.972>|<cell|44.32>>|<row|<cell|0.486>|<cell|0.534>|<cell|33.70>|<cell|>|<cell|1.000>|<cell|1.000>|<cell|45.93>>|<row|<cell|0.577>|<cell|0.662>|<cell|35.09>|<cell|>|<cell|>|<cell|>|<cell|>>>>>
  </big-table|<label|tbl:13-acetone-CHCl3>Liquid and gas compositions in the
  two-phase system of acetone (A) and chloroform at <math|35.2
  <degC>><footnote|Ref. <cite|ICT-3>, p. 286.>>

  \;

  <problem>Use the data in Table <reference|tbl:13-acetone-CHCl3><vpageref|tbl:13-acetone-CHCl3>
  to draw a pressure\Ucomposition phase diagram for the acetone\Uchloroform
  system at <math|35.2 <degC>>. Label the axes and each area.

  <\soln>
    \ See Fig. <reference|fig:13-acetone-CHCl3>.

    <\big-figure>
      <boxedfigure|<image|./13-SUP/acet-chl.eps||||> <capt|Phase diagram for
      the binary system of acetone (A) and chloroform at <math|35.2
      <degC>>.<label|fig:13-acetone-CHCl3>>>
    </big-figure|>

    The system exhibits a maximum-boiling azeotrope.
  </soln>

  \;
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|footnote-1|<tuple|1|?>>
  </collection>
</references>