<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|Physical Constants><label|app:const>

  <pagestyle|nosectioninheader>

  <index|Physical constants, values of><index|Constants, values of>The
  following table lists values of fundamental physical constants used to
  define SI base units or needed in thermodynamic calculations. The 2019 SI
  revision treats the first six constants (<math|<Del>\<nu\><subs|C*s>>
  through <math|N<subs|A>>) as <em|defining constants> or <em|fundamental
  constants> whose values are exact by definition.

  <\comment>
    \ 2010 CODATA values are the most recent as of Jan2015
  </comment>

  <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-valign|top>|<cwith|2|2|1|-1|cell-vmode|exact>|<cwith|2|2|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|5|5|1|-1|cell-valign|top>|<cwith|5|5|1|-1|cell-vmode|exact>|<cwith|5|5|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|6|6|1|-1|cell-valign|top>|<cwith|6|6|1|-1|cell-vmode|exact>|<cwith|6|6|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|7|7|1|-1|cell-valign|top>|<cwith|7|7|1|-1|cell-vmode|exact>|<cwith|7|7|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|8|8|1|-1|cell-valign|top>|<cwith|8|8|1|-1|cell-vmode|exact>|<cwith|8|8|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|9|9|1|-1|cell-valign|top>|<cwith|9|9|1|-1|cell-vmode|exact>|<cwith|9|9|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|10|10|1|-1|cell-valign|top>|<cwith|10|10|1|-1|cell-vmode|exact>|<cwith|10|10|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|11|11|1|-1|cell-valign|top>|<cwith|11|11|1|-1|cell-vmode|exact>|<cwith|11|11|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|11|11|1|-1|cell-bborder|1ln>|<table|<row|<cell|Constant>|<cell|Symbol>|<cell|Value
  in SI units>>|<row|<cell|cesium-133 hyperfine transition
  frequency>|<cell|<math|<Del>\<nu\><subs|C*s>>>|<cell|9.192 631
  770<timesten|9><units|s<per>> <tablestrut>>>|<row|<cell|speed of light in
  vacuum>|<cell|<math|c>>|<cell|2.997 924
  58<math|<timesten|8><units|m*<space|0.17em>s<per>>>>>|<row|<cell|Planck
  constant>|<cell|<math|h>>|<cell|6.626 070
  15<timesten|-34><units|J<space|0.17em>s>>>|<row|<cell|elementary
  charge>|<cell|<math|e>>|<cell|1.602 176
  634<timesten|-19><units|C>>>|<row|<cell|Boltzmann
  constant>|<cell|<math|k>>|<cell|1.380 649<timesten|-23><units|J<space|0.17em>K<per>>>>|<row|<cell|<index|Avogadro
  constant>Avogadro constant>|<cell|<math|N<subs|A>>>|<cell|6.022 140
  76<timesten|23><units|mol<per>>>>|<row|<cell|<index|Gas constant>gas
  constant<space|.15em><footnote|or molar gas constant; <math|R> is equal to
  <math|N<subs|A>k>>>|<cell|<math|R>>|<cell|8.314 462
  ...<units|J<space|0.17em>K<per><space|0.17em>mol<per>>>>|<row|<cell|<index|Faraday
  constant>Faraday constant<space|.15em><footnote|<math|F> is equal to
  <math|N<subs|A>e>>>|<cell|<math|F>>|<cell|9.648 533
  ...<timesten|4><units|C<space|0.17em>mol<per>>>>|<row|<cell|electric
  constant<space|.15em><footnote|or permittivity of vacuum;
  <math|\<epsilon\><rsub|0>> is equal to <math|10<rsup|-7>/<around|(|4*\<pi\>*c<rsup|2>|)>>>>|<cell|<math|\<epsilon\><rsub|0>>>|<cell|8.854
  187 ...<timesten|-12><units|C<rsup|<math|2>><space|0.17em>J<per><space|0.17em>m<per>>>>|<row|<cell|standard
  acceleration of free fall<space|.15em><footnote|or standard acceleration of
  gravity>>|<cell|<math|g<subs|n>>>|<cell|9.806
  65<units|m<space|0.17em>s<rsup|<math|-2>>>>>|<row|<cell|>|<cell|>|<cell|>>>>>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|app:const|<tuple|1|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|Physical constants, values of|?>>
    <associate|auto-3|<tuple|Constants, values of|?>>
    <associate|auto-4|<tuple|Avogadro constant|?>>
    <associate|auto-5|<tuple|Gas constant|?>>
    <associate|auto-6|<tuple|Faraday constant|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|Physical constants, values of>|<pageref|auto-2>>

      <tuple|<tuple|Constants, values of>|<pageref|auto-3>>

      <tuple|<tuple|Avogadro constant>|<pageref|auto-4>>

      <tuple|<tuple|Gas constant>|<pageref|auto-5>>

      <tuple|<tuple|Faraday constant>|<pageref|auto-6>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Physical
      Constants> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>