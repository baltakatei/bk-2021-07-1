<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|Thermodynamic Potentials>

  <paragraphfootnotes><label|Chap. 5>

  This chapter begins with a discussion of mathematical properties of the
  total differential of a dependent variable. Three extensive state functions
  with dimensions of energy are introduced: enthalpy, Helmholtz energy, and
  Gibbs energy. These functions, together with internal energy, are called
  <subindex|Thermodynamic|potential><subindex|Potential|thermodynamic><newterm|thermodynamic
  potentials>.<footnote|The term <em|thermodynamic potential> should not be
  confused with the <em|chemical potential>, <math|\<mu\>>, to be introduced
  on page <pageref|mu=chem pot>.> Some formal mathematical manipulations of
  the four thermodynamic potentials are described that lead to expressions
  for heat capacities, surface work, and criteria for spontaneity in closed
  systems.

  <section|Total Differential of a Dependent Variable>

  Recall from Sec. <reference|2-state fncs \ ind variables> that the state of
  the system at each instant is defined by a certain minimum number of state
  functions, the independent variables. State functions not treated as
  independent variables are dependent variables. Infinitesimal changes in any
  of the independent variables will, in general, cause an infinitesimal
  change in each dependent variable.

  A dependent variable is a function of the independent variables. The
  <index|Total differential><subindex|Differential|total><newterm|total
  differential> of a dependent variable is an expression for the
  infinitesimal change of the variable in terms of the infinitesimal changes
  of the independent variables. As explained in Sec. <reference|app:total
  diff> of Appendix <reference|app:state>, the expression can be written as a
  sum of terms, one for each independent variable. Each term is the product
  of a partial derivative with respect to one of the independent variables
  and the infinitesimal change of that independent variable. For example, if
  the system has two independent variables, and we take these to be <math|T>
  and <math|V>, the expression for the total differential of the pressure is

  <\equation>
    <label|dp(T,V)><difp>=<Pd|p|T|V><dif>T+<Pd|p|V|T><dif>V
  </equation>

  Thus, in the case of a fixed amount of an ideal gas with pressure given by
  <math|p=n*R*T/V>, the total differential of the pressure can be written

  <\equation>
    <difp>=<frac|n*R|V><dif>T-<frac|n*R*T|V<rsup|2>><dif>V
  </equation>

  <section|Total Differential of the Internal Energy>

  <I|Total differential!internal energy@of the internal
  energy\|(><I|Differential!total!internal energy@of the internal energy\|(>

  For a closed system undergoing processes in which the only kind of work is
  expansion work, the first law becomes <math|<dif>U=<dq>+<dw>=<dq>-p<bd><dif>V>.
  Since it will often be useful to make a distinction between expansion work
  and other kinds of work, this book will sometimes write the first law in
  the form <index|First law of thermodynamics>

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>U=<dq>-p<bd><dif>V+<dw><rprime|'>><cond|<around|(|c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|dU=dq-pdV+dw'>>>>>
  </gather>

  where <math|<dw><rprime|'>> is <index|Nonexpansion
  work><subindex|Work|nonexpansion><newterm|nonexpansion work>\Vthat is, any
  thermodynamic work that is not expansion work.

  Consider a closed system of one chemical component (e.g., a pure substance)
  in a single homogeneous phase. The only kind of work is expansion work,
  with <math|V> as the work variable. This kind of system has <em|two>
  independent variables (Sec. <reference|2-more about ind variables>). During
  a <em|reversible> process in this system, the heat is <math|<dq>=T<dif>S>,
  the work is <math|<dw>=-p<dif>V>, and an infinitesimal internal energy
  change is given by

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>U=T<dif>S-p<dif>V><cond|(c*l*o*s*e*d*s*y*s*t*e*m,<math|C=1>,><nextcond|<math|P=1>,<math|<dw><rprime|'>=0>)><eq-number><label|dU=TdS-pdV>>>>>
  </gather>

  In the conditions of validity shown next to this equation, <math|C=1> means
  there is one component (<math|C> is the number of components) and
  <math|P=1> means there is one phase (<math|P> is the number of phases).

  The appearance of the intensive variables <math|T> and <math|p> in Eq.
  <reference|dU=TdS-pdV> implies, of course, that the temperature and
  pressure are uniform throughout the system during the process. If they were
  not uniform, the phase would not be homogeneous and there would be more
  than two independent variables. The temperature and pressure are strictly
  uniform only if the process is reversible; it is not necessary to include
  \Preversible\Q as one of the conditions of validity.

  A real process approaches a reversible process in the limit of infinite
  slowness. For all practical purposes, therefore, we may apply Eq.
  <reference|dU=TdS-pdV> to a process obeying the conditions of validity and
  taking place so slowly that the temperature and pressure remain essentially
  uniform\Vthat is, for a process in which the system stays very close to
  thermal and mechanical equilibrium.

  Because the system under consideration has two independent variables, Eq.
  <reference|dU=TdS-pdV> is an expression for the total differential of
  <math|U> with <math|S> and <math|V> as the independent variables. In
  general, an expression for the differential <math|<dif>X> of a state
  function <math|X> is a total differential if

  <\enumerate>
    <item>it is a valid expression for <math|<dif>X>, consistent with the
    physical nature of the system and any conditions and constraints;

    <item>it is a sum with the same number of terms as the number of
    independent variables;

    <item>each term of the sum is a function of state functions multiplied by
    the differential of one of the independent variables.
  </enumerate>

  Note that the work coordinate of any kind of <index|Dissipative
  work><subindex|Work|dissipative>dissipative work\Vwork without a reversible
  limit\Vcannot appear in the expression for a total differential, because it
  is not a state function (Sec. <reference|3-generalities>).

  As explained in Appendix <reference|app:state>, we may identify the
  coefficient of each term in an expression for the total differential of a
  state function as a partial derivative of the function. We identify the
  coefficients on the right side of Eq. <reference|dU=TdS-pdV> as follows:

  <\equation>
    T=<Pd|U|S|V><space|2em>-p=<Pd|U|V|S>
  </equation>

  Now let us consider some of the ways a system might have more than two
  independent variables. Suppose the system has one phase and one substance,
  with expansion work only, and is <em|open> so that the amount <math|n> of
  the substance can vary. Such a system has three independent variables. Let
  us write the formal expression for the total differential of <math|U> with
  <math|S>, <math|V>, and <math|n> as the three independent variables:

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>U=<Pd|U|S|V,n><dif>S+<Pd|U|V|S,n><dif>V+<Pd|U|n|S,V><dif>n><cond|(p*u*r*e*s*u*b*s*t*a*n*c*e,><nextcond|<math|P=1>,<math|<dw><rprime|'>=0>)><eq-number><label|dU=(dU/dS)dS+(dU/dV)dV+(dU/dn)dn>>>>>
  </gather>

  We have seen above that if the system is <em|closed>, the partial
  derivatives are <math|<pd|U|S|V>=T> and <math|<pd|U|V|S>=-p>. Since both of
  these partial derivatives are for a closed system in which <math|n> is
  constant, they are the same as the first two partial derivatives on the
  right side of Eq. <reference|dU=(dU/dS)dS+(dU/dV)dV+(dU/dn)dn>.

  The quantity given by the third partial derivative, <math|<pd|U|n|S,V>>, is
  represented by the symbol <math|\<mu\>> (mu). This quantity is an intensive
  state function called the <index|Chemical
  potential><subindex|Potential|chemical><newterm|chemical
  potential>.<label|mu=chem pot>

  With these substitutions, Eq. <reference|dU=(dU/dS)dS+(dU/dV)dV+(dU/dn)dn>
  becomes

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>U=T<dif>S-p<dif>V+\<mu\><dif>n><cond|(p*u*r*e*s*u*b*s*t*a*n*c*e,><nextcond|<math|P=1>,<math|<dw><rprime|'>=0>)><eq-number><label|dU=TdS-pdV+(mu)dn>>>>>
  </gather>

  and this is a valid expression for the total differential of <math|U> under
  the given conditions.

  If a system contains a mixture of <math|s> different substances in a single
  phase, and the system is open so that the amount of each substance can vary
  independently, there are <math|2+s> independent variables and the total
  differential of <math|U> can be written

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>U=T<dif>S-p<dif>V+<big|sum><rsub|i=1><rsup|s>\<mu\><rsub|i><dif>n<rsub|i>><cond|(o*p*e*n*s*y*s*t*e*m,><nextcond|<math|P=1>,<math|<dw><rprime|'>=0>)><eq-number><label|dU=TdS-pdV+sum_i(mu_i)dn_i>>>>>
  </gather>

  The coefficient <math|\<mu\><rsub|i>> is the chemical potential of
  substance <math|i>. We identify it as the partial derivative
  <math|<pd|U|n<rsub|i>|S,V,n<rsub|j\<ne\>i>>>.

  <\quote-env>
    \ The term <math|-p<dif>V> on the right side of Eq.
    <reference|dU=TdS-pdV+sum_i(mu_i)dn_i> is the reversible work. However,
    the term <math|T<dif>S> does not equal the reversible heat as it would if
    the system were closed. This is because the entropy change <math|<dif>S>
    is partly due to the entropy of the matter transferred across the
    boundary. It follows that the remaining term,
    <math|<big|sum><rsub|i>\<mu\><rsub|i><dif>n<rsub|i>> (sometimes called
    the <index|Chemical work><subindex|Work|chemical>``chemical work''),
    should not be interpreted as the energy brought into the system by the
    transfer of matter.<footnote|Ref. <cite|knuiman-12>.>
  </quote-env>

  Suppose that in addition to expansion work, other kinds of reversible work
  are possible. Each work coordinate adds an additional independent variable.
  Thus, for a closed system of one component in one phase, with reversible
  nonexpansion work given by <math|<dw><rprime|'>=Y<dif>X>, the total
  differential of <math|U> becomes

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>U=T<dif>S-p<dif>V+Y<dif>X><cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>)><eq-number><label|dU=TdS-pdV+YdX>>>>>
  </gather>

  <I|Total differential!internal energy@of the internal
  energy\|)><I|Differential!total!internal energy@of the internal energy\|)>

  <section|Enthalpy, Helmholtz Energy, and Gibbs Energy>

  For the moment we shall confine our attention to closed systems with one
  component in one phase. The total differential of the internal energy in
  such a system is given by Eq. <reference|dU=TdS-pdV>:
  <math|<dif>U=T<dif>S-p<dif>V>. The independent variables in this equation,
  <math|S> and <math|V>, are called the <index|Natural
  variables><subindex|Variables|natural><em|natural variables> of <math|U>.

  In the laboratory, entropy and volume may not be the most convenient
  variables to measure and control. Entropy is especially inconvenient, as
  its value cannot be measured directly. The way to change the independent
  variables is to make <index|Legendre transform>Legendre transforms, as
  explained in Sec. <reference|app:Legendre> in Appendix
  <reference|app:state>.

  A Legendre transform of a dependent variable is made by subtracting one or
  more products of <subindex|Conjugate|variables><subindex|Variables|conjugate><em|conjugate
  variables>. In the total differential <math|<dif>U=T<dif>S-p<dif>V>,
  <math|T> and <math|S> are conjugates (that is, they comprise a
  <subindex|Conjugate|pair><em|conjugate pair>), and <math|-p> and <math|V>
  are conjugates. Thus the products that can be subtracted from <math|U> are
  either <math|T*S> or <math|-p*V>, or both. Three <index|Legendre
  transform>Legendre transforms of the internal energy are possible, defined
  as follows:

  <alignat|3|<tformat|<table|<row|<cell|>|<cell|<index|Enthalpy><tx|<newterm|Enthalpy>>>|<cell|<space|2em>>|<cell|H>|<cell|<defn>>|<cell|U+pV<eq-number><label|H=U+pV>>>|<row|<cell|>|<cell|<index|Helmholtz
  energy><subindex|Energy|Helmholtz><tx|<newterm|Helmholtz
  energy>>>|<cell|<space|2em>>|<cell|A>|<cell|<defn>>|<cell|U-TS<eq-number><label|A=U-TS>>>|<row|<cell|>|<cell|<index|Gibbs
  energy><subindex|Energy|Gibbs><tx|<newterm|Gibbs
  energy>>>|<cell|<space|2em>>|<cell|G>|<cell|<defn>>|<cell|U-TS+pV=H-TS<eq-number><label|G=H-TS>>>>>>

  These definitions are used whether or not the system has only two
  independent variables.

  <input|./bio/gibbs>

  The enthalpy, Helmholtz energy, and Gibbs energy are important functions
  used extensively in thermodynamics. They are state functions (because the
  quantities used to define them are state functions) and are extensive
  (because <math|U>, <math|S>, and <math|V> are extensive). If temperature or
  pressure are not uniform in the system, we can apply the definitions to
  constituent phases, or to subsystems small enough to be essentially
  uniform, and sum over the phases or subsystems.

  <\quote-env>
    Alternative names for the Helmholtz energy are Helmholtz function,
    Helmholtz free energy, and work function. Alternative names for the Gibbs
    energy are Gibbs function and Gibbs free energy. Both the Helmholtz
    energy and Gibbs energy have been called simply free energy, and the
    symbol <math|F> has been used for both. The nomenclature in this book
    follows the recommendations of the <index|IUPAC Green Book>IUPAC Green
    Book (Ref. <cite|greenbook-3>).
  </quote-env>

  Expressions for infinitesimal changes of <math|H>, <math|A>, and <math|G>
  are obtained by applying the rules of differentiation to their defining
  equations:

  <\equation>
    <label|dH=dU+pdV+Vdp><dif>H=<dif>U+p<dif>V+V<difp>
  </equation>

  <\equation>
    <label|dA=dU-TdS-SdT><dif>A=<dif>U-T<dif>S-S<dif>T
  </equation>

  <\equation>
    <label|dG=dU-TdS-SdT+pdV+Vdp><dif>G=<dif>U-T<dif>S-S<dif>T+p<dif>V+V<difp>
  </equation>

  These expressions for <math|<dif>H>, <math|<dif>A>, and <math|<dif>G> are
  general expressions for any system or phase with uniform <math|T> and
  <math|p>. They are <em|not> total differentials of <math|H>, <math|A>, and
  <math|G>, as the variables in the differentials in each expression are not
  independent.

  A useful property of the enthalpy in a closed system can be found by
  replacing <math|<dif>U> in Eq. <reference|dH=dU+pdV+Vdp> by the first law
  expression <math|<dq>-p<dif>V+<dw><rprime|'>>, to obtain
  <math|<dif>H=<dq>+V<difp>+<dw><rprime|'>>. Thus, in a process at constant
  pressure (<math|<difp>=0>) with expansion work only
  (<math|<dw><rprime|'>=0>), we have

  <\gather>
    <tformat|<table|<row|<cell|<dif>H=<dq><cond|(c*l*o*s*e*d*s*y*s*t*e*m,c*o*n*s*t*a*n*t<math|p>,><nextcond|<math|<dw><rprime|'>=0>)><eq-number><label|dH=dq
    (dp=0)>>>>>
  </gather>

  The enthalpy change under these conditions is equal to the heat. The
  integrated form of this relation is <math|<big|int><space|-0.17em><dif>H=<big|int><space|-0.17em><dq>>,
  or

  <\gather>
    <tformat|<table|<row|<cell|<s|<Del>H=q><cond|(c*l*o*s*e*d*s*y*s*t*e*m,c*o*n*s*t*a*n*t<math|p>,><nextcond|<math|w<rprime|'>=0>)><eq-number><label|delH=q
    (dp=0)>>>>>
  </gather>

  Equation <reference|dH=dq (dp=0)> is analogous to the following relation
  involving the internal energy, obtained from the first law:

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>U=<dq>><cond|(c*l*o*s*e*d*s*y*s*t*e*m,c*o*n*s*t*a*n*t<math|V>,><nextcond|<math|<dw><rprime|'>=0>)><eq-number><label|dU=dq
    (dV=0)>>>>>
  </gather>

  That is, in a process at constant volume with expansion work only, the
  internal energy change is equal to the heat.

  <section|Closed Systems>

  In order to find expressions for the total differentials of <math|H>,
  <math|A>, and <math|G> in a closed system with one component in one phase,
  we must replace <math|<dif>U> in Eqs. <reference|dH=dU+pdV+Vdp>\U<reference|dG=dU-TdS-SdT+pdV+Vdp>
  with

  <\equation>
    <label|dU=TdS-pdV(again)><dif>U=T<dif>S-p<dif>V
  </equation>

  to obtain

  <\align>
    <tformat|<table|<row|<cell|<dif>H>|<cell|=T<dif>S+V<difp><eq-number><label|dH=TdS+Vdp>>>|<row|<cell|<dif>A>|<cell|=-S<dif>T-p<dif>V<eq-number><label|dA=-SdT-pdV>>>|<row|<cell|<dif>G>|<cell|=-S<dif>T+V<difp><eq-number><label|dG=-SdT+Vdp>>>>>
  </align>

  Equations <reference|dU=TdS-pdV(again)>\U<reference|dG=-SdT+Vdp> are
  sometimes called the <subindex|Gibbs|equations><newterm|Gibbs equations>.
  They are expressions for the total differentials of the thermodynamic
  potentials <math|U>, <math|H>, <math|A>, and <math|G> in closed systems of
  one component in one phase with expansion work only. Each equation shows
  how the dependent variable on the left side varies as a function of changes
  in two independent variables (the <index|Natural
  variables><subindex|Variables|natural>natural variables of the dependent
  variable) on the right side.

  By identifying the coefficients on the right side of Eqs.
  <reference|dU=TdS-pdV(again)>\U<reference|dG=-SdT+Vdp>, we obtain the
  following relations (which again are valid for a closed system of one
  component in one phase with expansion work only):

  <alignat|2|<tformat|<table|<row|<cell|>|<cell|<tx|from Eq.
  <reference|dU=TdS-pdV(again)>:>>|<cell|<space|2em><Pd|U|S|V>>|<cell|=T<eq-number><label|dU/dS=T>>>|<row|<cell|\<ast\>>|<cell|>|<cell|<Pd|U|V|S>>|<cell|=-p<eq-number><label|dU/dV=-p>>>|<row|<cell|>|<cell|<tx|from
  Eq. <reference|dH=TdS+Vdp>:>>|<cell|<Pd|H|S|<space|-0.17em>p>>|<cell|=T<eq-number><label|dH/dS=T>>>|<row|<cell|\<ast\>>|<cell|>|<cell|<Pd|H|p|S>>|<cell|=V<eq-number><label|dH/dp=V>>>|<row|<cell|>|<cell|<tx|from
  Eq. <reference|dA=-SdT-pdV>:>>|<cell|<Pd|A|T|V>>|<cell|=-S<eq-number><label|dA/dT=-S>>>|<row|<cell|\<ast\>>|<cell|>|<cell|<Pd|A|V|T>>|<cell|=-p<eq-number><label|dA/dV=-p>>>|<row|<cell|>|<cell|<tx|from
  Eq. <reference|dG=-SdT+Vdp>:>>|<cell|<Pd|G|T|<space|-0.17em>p>>|<cell|=-S<eq-number><label|dG/dT=-S>>>|<row|<cell|\<ast\>>|<cell|>|<cell|<Pd|G|p|T>>|<cell|=V<eq-number><label|dG/dp=V>>>>>>

  This book now uses for the first time an extremely useful mathematical tool
  called the <index|Reciprocity relation><newterm|reciprocity relation> of a
  total differential (Sec. <reference|app:total diff>). Suppose the
  independent variables are <math|x> and <math|y> and the total differential
  of a dependent state function <math|f> is given by

  <\equation>
    <df>=a<dx>+b<dif>y
  </equation>

  where <math|a> and <math|b> are functions of <math|x> and <math|y>. Then
  the reciprocity relation is

  <\equation>
    <label|reciprocity reln><Pd|a|y|x>=<Pd|b|x|y>
  </equation>

  The reciprocity relations obtained from the Gibbs equations (Eqs.
  <reference|dU=TdS-pdV(again)>\U<reference|dG=-SdT+Vdp>) are called
  <index|Maxwell relations><newterm|Maxwell relations> (again valid for a
  closed system with <math|C=1>, <math|P=1>, and <math|<dw><rprime|'>=0>):

  <alignat|2|<tformat|<table|<row|<cell|>|<cell|<tx|from Eq.
  <reference|dU=TdS-pdV(again)>:>>|<cell|<space|2em><Pd|T|V|S>>|<cell|=-<Pd|p|S|V><eq-number><label|dT/dV=-dp/dS>>>|<row|<cell|>|<cell|<tx|from
  Eq. <reference|dH=TdS+Vdp>:>>|<cell|<Pd|T|p|S>>|<cell|=<Pd|V|S|<space|-0.17em>p><eq-number><label|dT/dp=dV/dS>>>|<row|<cell|>|<cell|<tx|from
  Eq. <reference|dA=-SdT-pdV>:>>|<cell|<Pd|S|V|T>>|<cell|=<Pd|p|T|V><eq-number><label|dS/dV=dp/dT>>>|<row|<cell|>|<cell|<tx|from
  Eq. <reference|dG=-SdT+Vdp>:>>|<cell|-<Pd|S|p|T>>|<cell|=<Pd|V|T|<space|-0.17em>p><eq-number><label|-dS/dp=dV/dT>>>>>>

  <section|Open Systems><label|5-open systems>

  An open system of one substance in one phase, with expansion work only, has
  three independent variables. The total differential of <math|U> is given by
  Eq. <reference|dU=TdS-pdV+(mu)dn>:

  <\equation>
    <label|dU=TdS-pdV+(mu)dn(again)><dif>U=T<dif>S-p<dif>V+\<mu\><dif>n
  </equation>

  In this open system the <index|Natural variables><subindex|Variables|natural>natural
  variables of <math|U> are <math|S>, <math|V>, and <math|n>. Substituting
  this expression for <math|<dif>U> into the expressions for <math|<dif>H>,
  <math|<dif>A>, and <math|<dif>G> given by Eqs.
  <reference|dH=dU+pdV+Vdp>\U<reference|dG=dU-TdS-SdT+pdV+Vdp>, we obtain the
  following total differentials:

  <\equation>
    <label|dH=TdS+Vdp+mu*dn><dif>H=T<dif>S+V<difp>+\<mu\><dif>n
  </equation>

  <\equation>
    <label|dA=-SdT-pdV+mu*dn><dif>A=-S<dif>T-p<dif>V+\<mu\><dif>n
  </equation>

  <\equation>
    <label|dG=-SdT+Vdp+mu*dn><dif>G=-S<dif>T+V<difp>+\<mu\><dif>n
  </equation>

  Note that these are the same as the four <subindex|Gibbs|equations>Gibbs
  equations (Eqs. <reference|dU=TdS-pdV(again)>\U<reference|dG=-SdT+Vdp>)
  with the addition of a term <math|\<mu\><dif>n> to allow for a change in
  the amount of substance.

  Identification of the coefficient of the last term on the right side of
  each of these equations shows that the chemical potential can be equated to
  four different partial derivatives:

  <\equation>
    <label|mu=.=.=.=.>\<mu\>=<Pd|U|n|S,V>=<Pd|H|n|S,p>=<Pd|A|n|T,V>=<Pd|G|n|T,p>
  </equation>

  All four of these partial derivatives must have the same value for a given
  state of the system; the value, of course, depends on what that state is.

  The last partial derivative on the right side of Eq.
  <reference|mu=.=.=.=.>, <math|<pd|G|n|T,p>>, is especially interesting
  because it is the rate at which the Gibbs energy increases with the amount
  of substance added to a system whose intensive properties remain constant.
  Thus, <math|\<mu\>> is revealed to be equal to <math|G<m>>, the
  <subindex|Gibbs energy|molar>molar Gibbs energy of the substance.<label|mu
  = Gm>

  Suppose the system contains several substances or species in a single phase
  (a mixture) whose amounts can be varied independently. We again assume the
  only work is expansion work. Then, making use of Eq.
  <reference|dU=TdS-pdV+sum_i(mu_i)dn_i>, we find the total differentials of
  the thermodynamic potentials are given by

  <\equation>
    <label|dU=TdS-pdV+sum(mu_i)dn_i><dif>U=T<dif>S-p<dif>V+<big|sum><rsub|i>\<mu\><rsub|i><dif>n<rsub|i>
  </equation>

  <\equation>
    <label|dH=TdS+Vdp+sum(mu_i)dn_i><dif>H=T<dif>S+V<difp>+<big|sum><rsub|i>\<mu\><rsub|i><dif>n<rsub|i>
  </equation>

  <\equation>
    <label|dA=-SdT-pdV+sum(mu_i)dn_i><dif>A=-S<dif>T-p<dif>V+<big|sum><rsub|i>\<mu\><rsub|i><dif>n<rsub|i>
  </equation>

  <\equation>
    <label|dG=-SdT+Vdp+sum(mu_i)dn_i><dif>G=-S<dif>T+V<difp>+<big|sum><rsub|i>\<mu\><rsub|i><dif>n<rsub|i>
  </equation>

  The independent variables on the right side of each of these equations are
  the <index|Natural variables><subindex|Variables|natural>natural variables
  of the corresponding thermodynamic potential. Section
  <reference|app:Legendre> shows that all of the information contained in an
  algebraic expression for a state function is preserved in a <index|Legendre
  transform>Legendre transform of the function. What this means for the
  thermodynamic potentials is that an expression for any one of them, as a
  function of its natural variables, can be converted to an expression for
  each of the other thermodynamic potentials as a function of its natural
  variables.

  <index|Gibbs, Josiah Willard>Willard Gibbs, after whom the Gibbs energy is
  named, called Eqs. <reference|dU=TdS-pdV+sum(mu_i)dn_i>\U<reference|dG=-SdT+Vdp+sum(mu_i)dn_i>
  the <index|Fundamental equations, Gibbs><subindex|Gibbs|fundamental
  equations><em|fundamental equations> of thermodynamics, because from any
  single one of them not only the other thermodynamic potentials but also all
  thermal, mechanical, and chemical properties of the system can be
  deduced.<footnote|Ref. <cite|gibbs-76>, p. 86.> Problem
  5.<reference|prb:5-fncs from A> illustrates this useful application of the
  total differential of a thermodynamic potential.

  In Eqs. <reference|dU=TdS-pdV+sum(mu_i)dn_i>\U<reference|dG=-SdT+Vdp+sum(mu_i)dn_i>,
  the coefficient <math|\<mu\><rsub|i>> is the chemical potential of species
  <math|i>. The equations show that <math|\<mu\><rsub|i>> can be equated to
  four different partial derivatives, similar to the equalities shown in Eq.
  <reference|mu=.=.=.=.> for a pure substance:

  <\equation>
    <label|mu_i=.=.=.=.>\<mu\><rsub|i>=<Pd|U|n<rsub|i>|S,V,n<rsub|j\<ne\>i>>=<Pd|H|n<rsub|i>|S,p,n<rsub|j\<ne\>i>>=<Pd|A|n<rsub|i>|T,V,n<rsub|j\<ne\>i>>=<Pd|G|n<rsub|i>|T,p,n<rsub|j\<ne\>i>>
  </equation>

  The partial derivative <math|<pd|G|n<rsub|i>|T,P,n<rsub|j\<ne\>i>>> is
  called the <subindex|Partial molar|Gibbs energy><em|partial molar Gibbs
  energy> of species <math|i>, another name for the <index|Chemical
  potential>chemical potential as will be discussed in Sec. <reference|9-chem
  pot of species in a mixt>.

  <section|Expressions for Heat Capacity><label|5-heat capacity>

  <I|Heat capacity\|(>

  As explained in Sec. <reference|3-heat capacity>, the heat capacity of a
  closed system is defined as the ratio of an infinitesimal quantity of heat
  transferred across the boundary under specified conditions and the
  resulting infinitesimal temperature change:
  <math|<tx|h*e*a*t*c*a*p*a*c*i*t*y><defn><dq>/<dif>T>. The heat capacities
  of isochoric (constant volume) and isobaric (constant pressure) processes
  are of particular interest.

  The <I|Heat capacity!constant volume@at constant volume\|reg><newterm|heat
  capacity at constant volume>, <math|C<rsub|V>>, is the ratio
  <math|<dq>/<dif>T> for a process in a closed constant-volume system with no
  nonexpansion work\Vthat is, no work at all. The first law shows that under
  these conditions the internal energy change equals the heat:
  <math|<dif>U=<dq>> (Eq. <reference|dU=dq (dV=0)>). We can replace
  <math|<dq>> by <math|<dif>U> and write <math|C<rsub|V>> as a partial
  derivative:

  <\gather>
    <tformat|<table|<row|<cell|C<rsub|V>=<Pd|U|T|V><cond|<around|(|c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|CV=dU/dT>>>>>
  </gather>

  <\quote-env>
    If the closed system has more than two independent variables, additional
    conditions<label|ht cap conditions>are needed to define <math|C<rsub|V>>
    unambiguously. For instance, if the system is a gas mixture in which
    reaction can occur, we might specify that the system remains in reaction
    equilibrium as <math|T> changes at constant <math|V>.

    Equation <reference|CV=dU/dT> does not require the condition
    <math|<dw><rprime|'>=0>, because all quantities appearing in the equation
    are <em|state> functions whose relations to one another are fixed by the
    nature of the system and not by the path. Thus, if heat transfer into the
    system at constant <math|V> causes <math|U> to increase at a certain rate
    with respect to <math|T>, and this rate is defined as <math|C<rsub|V>>,
    the performance of electrical work on the system at constant <math|V>
    will cause the same rate of increase of <math|U> with respect to <math|T>
    and can equally well be used to evaluate <math|C<rsub|V>>.
  </quote-env>

  Note that <math|C<rsub|V>> is a state function whose value depends on the
  state of the system\Vthat is, on <math|T>, <math|V>, and any additional
  independent variables. <math|C<rsub|V>> is an <em|extensive> property: the
  combination of two identical phases has twice the value of <math|C<rsub|V>>
  that one of the phases has by itself.

  For a phase containing a pure substance, the <I|Heat capacity!constant
  volume@at constant volume!molar\|reg><newterm|molar heat capacity at
  constant volume> is defined by <math|<CVm><defn>C<rsub|V>/n>. <math|<CVm>>
  is an <em|intensive> property.

  If the system is an ideal gas, its internal energy depends only on
  <math|T>, regardless of whether <math|V> is constant, and Eq.
  <reference|CV=dU/dT> can be simplified to

  <\gather>
    <tformat|<table|<row|<cell|C<rsub|V>=<frac|<dif>U|<dif>T><cond|<around|(|c*l*o*s*e*d*s*y*s*t*e*m,i*d*e*a*l*g*a*s|)>><eq-number><label|CV=dU/dT
    (id gas)>>>>>
  </gather>

  Thus the internal energy change of an ideal gas is given by
  <math|<dif>U=C<rsub|V><dif>T>, as mentioned earlier in Sec.
  <reference|3-rev adiab expan>.

  The <I|Heat capacity!constant pressure@at constant
  pressure\|reg><newterm|heat capacity at constant pressure>,
  <math|C<rsub|p>>, is the ratio <math|<dq>/<dif>T> for a process in a closed
  system with a constant, uniform pressure and with expansion work only.
  Under these conditions, the heat <math|<dq>> is equal to the enthalpy
  change <math|<dif>H> (Eq. <reference|dH=dq (dp=0)>), and we obtain a
  relation analogous to Eq. <reference|CV=dU/dT>:

  <\gather>
    <tformat|<table|<row|<cell|C<rsub|p>=<Pd|H|T|<space|-0.17em>p><cond|<around|(|c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|Cp=dH/dT>>>>>
  </gather>

  <math|C<rsub|p>> is an extensive state function. For a phase containing a
  pure substance, the <I|Heat capacity!constant pressure@at constant
  pressure!molar\|reg><newterm|molar heat capacity at constant pressure> is
  <math|<Cpm>=C<rsub|p>/n>, an intensive property.

  Since the enthalpy of a fixed amount of an ideal gas depends only on
  <math|T> (Prob. 5.<reference|prb:5-H(T)>), we can write a relation
  analogous to Eq. <reference|CV=dU/dT (id gas)>:

  <\gather>
    <tformat|<table|<row|<cell|C<rsub|p>=<frac|<dif>H|<dif>T><cond|<around|(|c*l*o*s*e*d*s*y*s*t*e*m,i*d*e*a*l*g*a*s|)>><eq-number><label|Cp=dH/dT
    (id gas)>>>>>
  </gather>

  <I|Heat capacity\|)>

  <section|Surface Work><label|5-surface work>

  <I|Work!surface\|(><I|Surface work\|(>

  Sometimes we need more than the usual two independent variables to describe
  an equilibrium state of a closed system of one substance in one phase. This
  is the case when, in addition to expansion work, another kind of work is
  possible. The total differential of <math|U> is then given by
  <math|<dif>U=T<dif>S-p<dif>V+Y<dif>X> (Eq. <reference|dU=TdS-pdV+YdX>),
  where <math|Y<dif>X> represents the nonexpansion work
  <math|<dw><rprime|'>>.

  A good example of this situation is surface work in a system in which
  surface area is relevant to the description of the state.

  A liquid\Ugas interface behaves somewhat like a stretched membrane. The
  upper and lower surfaces of the liquid film in the device depicted in Fig.
  <reference|fig:5-surface tension device><vpageref|fig:5-surface tension
  device>

  <\big-figure>
    <boxedfigure|<image|./05-SUP/surf-ten.eps||||> <capt|Device to measure
    the surface tension of a liquid film. The film is stretched between a
    bent wire and a sliding rod.<label|fig:5-surface tension device>>>
  </big-figure|>

  exert a force <math|F> on the sliding rod, tending to pull it in the
  direction that reduces the surface area. We can measure the force by
  determining the opposing force <math|F<subs|e*x*t>> needed to prevent the
  rod from moving. This force is found to be proportional to the length of
  the rod and independent of the rod position <math|x>. The force also
  depends on the temperature and pressure.

  The <index|Surface tension><newterm|surface tension> or interfacial
  tension, <math|<g>>, is the force exerted by an interfacial surface per
  unit length. The film shown in Fig. <reference|fig:5-surface tension
  device> has two surfaces, so we have <math|<g>=F/2*l> where <math|l> is the
  rod length.

  To increase the surface area of the film by a practically-reversible
  process, we slowly pull the rod to the right in the <math|+x> direction.
  The <em|system> is the liquid. The <math|x> component of the force exerted
  by the system on the surroundings at the moving boundary,
  <math|F<rsub|x><sups|s*y*s>>, is equal to <math|-F> (<math|F> is positive
  and <math|F<rsub|x><sups|s*y*s>> is negative). The displacement of the rod
  results in surface work given by Eq. <reference|dw=-Fx(sys)dx w=>:
  <math|<dw><rprime|'>=-F<rsub|x><sups|s*y*s><dx>=2<g>l<dx>>. The increase in
  surface area, <math|<dif>A<subs|s>>, is <math|2*l<dx>>, so the surface work
  is <math|<dw><rprime|'>=<g><dif>A<subs|s>> where <math|<g>> is the work
  coefficient and <math|A<subs|s>> is the work coordinate. Equation
  <reference|dU=TdS-pdV+YdX> becomes

  <\equation>
    <label|dU=TdS-pdV+gamma*dAs><dif>U=T<dif>S-p<dif>V+<g><dif>A<subs|s>
  </equation>

  Substitution into Eq. <reference|dG=dU-TdS-SdT+pdV+Vdp> gives

  <\equation>
    <label|dG=-SdT+Vdp+gamma*dAs><dif>G=-S<dif>T+V<difp>+<g><dif>A<subs|s>
  </equation>

  which is the total differential of <math|G> with <math|T>, <math|p>, and
  <math|A<subs|s>> as the independent variables. Identifying the coefficient
  of the last term on the right side as a partial derivative, we find the
  following expression for the surface tension:

  <\equation>
    <g>=<Pd|G|A<subs|s>|T,p>
  </equation>

  That is, the surface tension is not only a force per unit length, but also
  a Gibbs energy per unit area.

  From Eq. <reference|dG=-SdT+Vdp+gamma*dAs>, we obtain the reciprocity
  relation

  <\equation>
    <label|d(gamma)/dT=-dS/dA(s)><Pd|<g>|T|p,A<subs|s>>=-<Pd|S|A<subs|s>|T,p>
  </equation>

  It is valid to replace the partial derivative on the left side by
  <math|<pd|<g>|T|p>> because <math|<g>> is independent of <math|A<subs|s>>.
  Thus, the variation of surface tension with temperature tells us how the
  entropy of the liquid varies with surface area.

  <I|Work!surface\|)><I|Surface work\|)>

  <section|Criteria for Spontaneity><label|5-combining>

  In this section we combine the first and second laws in order to derive
  some general relations for changes during a reversible or irreversible
  process of a closed system. The temperature and pressure will be assumed to
  be practically uniform during the process, even if the process is
  irreversible. For example, the volume might be changing at a finite rate
  but very slowly, or there might be a spontaneous homogeneous reaction in a
  mixture of uniform temperature and pressure.

  The second law states that <math|<dif>S> is equal to <math|<dq>/T> if the
  process is reversible, and is greater than <math|<dq>/T> if the process is
  irreversible:

  <\gather>
    <tformat|<table|<row|<cell|<dif>S\<ge\><dq>/T<cond|<around|(|<ir>,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number>>>>>
  </gather>

  or

  <\gather>
    <tformat|<table|<row|<cell|<s|<dq>\<le\>T<dif>S><cond|<around|(|<ir>,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|dq\<less\>=TdS>>>>>
  </gather>

  The <em|inequalities> in these relations refer to an irreversible process
  and the <em|equalities> to a reversible process, as indicated by the
  notation <ir>.

  When we substitute <math|<dq>> from Eq. <reference|dq\<less\>=TdS> into the
  first law in the form <math|<dif>U=<dq>-p<dif>V+<dw><rprime|'>>, where
  <math|<dw><rprime|'>> is <index|Nonexpansion
  work><subindex|Work|nonexpansion>nonexpansion work, we obtain the relation

  <\gather>
    <tformat|<table|<row|<cell|<dif>U\<le\>T<dif>S-p<dif>V+<dw><rprime|'><cond|<around|(|<ir>,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|dU\<less\>=TdS-pdV+dw'>>>>>
  </gather>

  We substitute this relation for <math|<dif>U> into the differentials of
  enthalpy, Helmholtz energy, and Gibbs energy given by Eqs.
  <reference|dH=dU+pdV+Vdp>\U<reference|dG=dU-TdS-SdT+pdV+Vdp> to obtain
  three more relations:

  <\gather>
    <tformat|<table|<row|<cell|<dif>H\<le\>T<dif>S+V<difp>+<dw><rprime|'><cond|<around|(|<ir>,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|dH\<less\>=TdS+Vdp+dw'>>>>>
  </gather>

  <\gather>
    <tformat|<table|<row|<cell|<dif>A\<le\>-S<dif>T-p<dif>V+<dw><rprime|'><cond|<around|(|<ir>,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|dA\<less\>=-SdT-pdV+dw'>>>>>
  </gather>

  <\gather>
    <tformat|<table|<row|<cell|<dif>G\<le\>-S<dif>T+V<difp>+<dw><rprime|'><cond|<around|(|<ir>,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|dG\<less\>=-SdT+Vdp+dw'>>>>>
  </gather>

  The last two of these relations provide valuable criteria for spontaneity
  under common laboratory conditions. Equation
  <reference|dA\<less\>=-SdT-pdV+dw'> shows that during a spontaneous
  irreversible change at constant temperature and volume, <math|<dif>A> is
  less than <math|<dw><rprime|'>>. If the only work is expansion work (i.e.,
  <math|<dw><rprime|'>> is zero),<label|spon at const T \ V>the Helmholtz
  energy decreases during a spontaneous process at constant <math|T> and
  <math|V> and has its minimum value when the system reaches an equilibrium
  state.

  Equation <reference|dG\<less\>=-SdT+Vdp+dw'> is especially useful. From it,
  we can conclude the following:

  <\itemize>
    <item>Reversible nonexpansion work at constant <math|T> and <math|p> is
    equal to the Gibbs energy change. For example, if the system is a
    galvanic cell operated in the reversible limit (Sec.
    <reference|3-galvanic cell>) at constant <math|T> and <math|p>, the
    electrical work is given by <math|<dw><subs|e*l,r*e*v>=<dif>G>. There is
    an application of this relation in Sec. <reference|14-el rxn eqm
    derivation>.

    <item>During a spontaneous process at constant <math|T> and <math|p> in a
    closed system with expansion work only, the Gibbs energy continuously
    decreases until the system reaches an equilibrium state.
  </itemize>

  <\quote-env>
    <label|rectification>Ben-Amotz and Honig<footnote|Refs.
    <cite|ben-amotz-03> and <cite|honig-06>.> developed a ``rectification''
    procedure that simplifies the mathematical manipulation of inequalities.
    Following this procedure, we can write

    <\equation>
      <dif>S=<dq>/T+<dBar>\<theta\>
    </equation>

    where <math|<dBar>\<theta\>> is an <subindex|Excess|function><em|excess
    entropy function> that is positive for an irreversible change and zero
    for a reversible change (<math|<dBar>\<theta\>\<geq\>0>). Solving for
    <math|<dq>> gives the expression <math|<dq>=T<dif>S-T<dBar>\<theta\>>
    that, when substituted in the first law expression
    <math|<dif>U=<dq>-p<dif>V+<dw><rprime|'>>, produces

    <\equation>
      <dif>U=T<dif>S-p<dif>V+<dw><rprime|'>-T<dBar>\<theta\>
    </equation>

    The equality of this equation is equivalent to the combined equality and
    inequality of Eq. <reference|dU\<less\>=TdS-pdV+dw'>. Then by
    substitution of this expression for <math|<dif>U> into Eqs.
    <reference|dH=dU+pdV+Vdp>--<reference|dG=dU-TdS-SdT+pdV+Vdp>, we obtain
    equalities equivalent to Eqs. <reference|dH\<less\>=TdS+Vdp+dw'>--<reference|dG\<less\>=-SdT+Vdp+dw'>,
    for example

    <\equation>
      <label|dG= (rectification)><dif>G=-S<dif>T+V<difp>+<dw><rprime|'>-T<dBar>\<theta\>
    </equation>

    Equation <reference|dG= (rectification)> tells us that during a process
    at constant <math|T> and <math|p>, with expansion work only
    (<math|<dw><rprime|'>=0>), <math|<dif>G> has the same sign as
    <math|-T<dBar>\<theta\>>: negative for an irreversible change and zero
    for a reversible change.
  </quote-env>

  <new-page><phantomsection><addcontentsline|toc|section|Problems>
  <paragraphfootnotes><problems| <input|05-problems><page-break>>
  <plainfootnotes>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|-dS/dp=dV/dT|<tuple|1.4.18|?>>
    <associate|5-combining|<tuple|1.8|?>>
    <associate|5-heat capacity|<tuple|1.6|?>>
    <associate|5-open systems|<tuple|1.5|?>>
    <associate|5-surface work|<tuple|1.7|?>>
    <associate|A=U-TS|<tuple|1.3.2|?>>
    <associate|CV=dU/dT|<tuple|1.6.1|?>>
    <associate|CV=dU/dT (id gas)|<tuple|1.6.2|?>>
    <associate|Chap. 5|<tuple|1|?>>
    <associate|Cp=dH/dT|<tuple|1.6.3|?>>
    <associate|Cp=dH/dT (id gas)|<tuple|1.6.4|?>>
    <associate|G=H-TS|<tuple|1.3.3|?>>
    <associate|H=U+pV|<tuple|1.3.1|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|First law of thermodynamics|?>>
    <associate|auto-11|<tuple|Nonexpansion work|?>>
    <associate|auto-12|<tuple|Work|?>>
    <associate|auto-13|<tuple|nonexpansion work|?>>
    <associate|auto-14|<tuple|Dissipative work|?>>
    <associate|auto-15|<tuple|Work|?>>
    <associate|auto-16|<tuple|Chemical potential|?>>
    <associate|auto-17|<tuple|Potential|?>>
    <associate|auto-18|<tuple|chemical potential|?>>
    <associate|auto-19|<tuple|Chemical work|?>>
    <associate|auto-2|<tuple|Thermodynamic|?>>
    <associate|auto-20|<tuple|Work|?>>
    <associate|auto-21|<tuple|1.3|?>>
    <associate|auto-22|<tuple|Natural variables|?>>
    <associate|auto-23|<tuple|Variables|?>>
    <associate|auto-24|<tuple|Legendre transform|?>>
    <associate|auto-25|<tuple|Conjugate|?>>
    <associate|auto-26|<tuple|Variables|?>>
    <associate|auto-27|<tuple|Conjugate|?>>
    <associate|auto-28|<tuple|Legendre transform|?>>
    <associate|auto-29|<tuple|Enthalpy|?>>
    <associate|auto-3|<tuple|Potential|?>>
    <associate|auto-30|<tuple|Enthalpy|?>>
    <associate|auto-31|<tuple|Helmholtz energy|?>>
    <associate|auto-32|<tuple|Energy|?>>
    <associate|auto-33|<tuple|Helmholtz energy|?>>
    <associate|auto-34|<tuple|Gibbs energy|?>>
    <associate|auto-35|<tuple|Energy|?>>
    <associate|auto-36|<tuple|Gibbs energy|?>>
    <associate|auto-37|<tuple|IUPAC Green Book|?>>
    <associate|auto-38|<tuple|1.4|?>>
    <associate|auto-39|<tuple|Gibbs|?>>
    <associate|auto-4|<tuple|thermodynamic potentials|?>>
    <associate|auto-40|<tuple|Gibbs equations|?>>
    <associate|auto-41|<tuple|Natural variables|?>>
    <associate|auto-42|<tuple|Variables|?>>
    <associate|auto-43|<tuple|Reciprocity relation|?>>
    <associate|auto-44|<tuple|reciprocity relation|?>>
    <associate|auto-45|<tuple|Maxwell relations|?>>
    <associate|auto-46|<tuple|Maxwell relations|?>>
    <associate|auto-47|<tuple|1.5|?>>
    <associate|auto-48|<tuple|Natural variables|?>>
    <associate|auto-49|<tuple|Variables|?>>
    <associate|auto-5|<tuple|1.1|?>>
    <associate|auto-50|<tuple|Gibbs|?>>
    <associate|auto-51|<tuple|Gibbs energy|?>>
    <associate|auto-52|<tuple|Natural variables|?>>
    <associate|auto-53|<tuple|Variables|?>>
    <associate|auto-54|<tuple|Legendre transform|?>>
    <associate|auto-55|<tuple|Gibbs, Josiah Willard|?>>
    <associate|auto-56|<tuple|Fundamental equations, Gibbs|?>>
    <associate|auto-57|<tuple|Gibbs|?>>
    <associate|auto-58|<tuple|Partial molar|?>>
    <associate|auto-59|<tuple|Chemical potential|?>>
    <associate|auto-6|<tuple|Total differential|?>>
    <associate|auto-60|<tuple|1.6|?>>
    <associate|auto-61|<tuple|heat capacity at constant volume|?>>
    <associate|auto-62|<tuple|molar heat capacity at constant volume|?>>
    <associate|auto-63|<tuple|heat capacity at constant pressure|?>>
    <associate|auto-64|<tuple|molar heat capacity at constant pressure|?>>
    <associate|auto-65|<tuple|1.7|?>>
    <associate|auto-66|<tuple|1.7.1|?>>
    <associate|auto-67|<tuple|Surface tension|?>>
    <associate|auto-68|<tuple|surface tension|?>>
    <associate|auto-69|<tuple|1.8|?>>
    <associate|auto-7|<tuple|Differential|?>>
    <associate|auto-70|<tuple|Nonexpansion work|?>>
    <associate|auto-71|<tuple|Work|?>>
    <associate|auto-72|<tuple|Excess|?>>
    <associate|auto-8|<tuple|total differential|?>>
    <associate|auto-9|<tuple|1.2|?>>
    <associate|d(gamma)/dT=-dS/dA(s)|<tuple|1.7.4|?>>
    <associate|dA/dT=-S|<tuple|1.4.9|?>>
    <associate|dA/dV=-p|<tuple|1.4.10|?>>
    <associate|dA\<less\>=-SdT-pdV+dw'|<tuple|1.8.5|?>>
    <associate|dA=-SdT-pdV|<tuple|1.4.3|?>>
    <associate|dA=-SdT-pdV+mu*dn|<tuple|1.5.3|?>>
    <associate|dA=-SdT-pdV+sum(mu_i)dn_i|<tuple|1.5.8|?>>
    <associate|dA=dU-TdS-SdT|<tuple|1.3.5|?>>
    <associate|dG/dT=-S|<tuple|1.4.11|?>>
    <associate|dG/dp=V|<tuple|1.4.12|?>>
    <associate|dG\<less\>=-SdT+Vdp+dw'|<tuple|1.8.6|?>>
    <associate|dG= (rectification)|<tuple|1.8.9|?>>
    <associate|dG=-SdT+Vdp|<tuple|1.4.4|?>>
    <associate|dG=-SdT+Vdp+gamma*dAs|<tuple|1.7.2|?>>
    <associate|dG=-SdT+Vdp+mu*dn|<tuple|1.5.4|?>>
    <associate|dG=-SdT+Vdp+sum(mu_i)dn_i|<tuple|1.5.9|?>>
    <associate|dG=dU-TdS-SdT+pdV+Vdp|<tuple|1.3.6|?>>
    <associate|dH/dS=T|<tuple|1.4.7|?>>
    <associate|dH/dp=V|<tuple|1.4.8|?>>
    <associate|dH\<less\>=TdS+Vdp+dw'|<tuple|1.8.4|?>>
    <associate|dH=TdS+Vdp|<tuple|1.4.2|?>>
    <associate|dH=TdS+Vdp+mu*dn|<tuple|1.5.2|?>>
    <associate|dH=TdS+Vdp+sum(mu_i)dn_i|<tuple|1.5.7|?>>
    <associate|dH=dU+pdV+Vdp|<tuple|1.3.4|?>>
    <associate|dH=dq (dp=0)|<tuple|1.3.7|?>>
    <associate|dS/dV=dp/dT|<tuple|1.4.17|?>>
    <associate|dT/dV=-dp/dS|<tuple|1.4.15|?>>
    <associate|dT/dp=dV/dS|<tuple|1.4.16|?>>
    <associate|dU/dS=T|<tuple|1.4.5|?>>
    <associate|dU/dV=-p|<tuple|1.4.6|?>>
    <associate|dU\<less\>=TdS-pdV+dw'|<tuple|1.8.3|?>>
    <associate|dU=(dU/dS)dS+(dU/dV)dV+(dU/dn)dn|<tuple|1.2.4|?>>
    <associate|dU=TdS-pdV|<tuple|1.2.2|?>>
    <associate|dU=TdS-pdV(again)|<tuple|1.4.1|?>>
    <associate|dU=TdS-pdV+(mu)dn|<tuple|1.2.5|?>>
    <associate|dU=TdS-pdV+(mu)dn(again)|<tuple|1.5.1|?>>
    <associate|dU=TdS-pdV+YdX|<tuple|1.2.7|?>>
    <associate|dU=TdS-pdV+gamma*dAs|<tuple|1.7.1|?>>
    <associate|dU=TdS-pdV+sum(mu_i)dn_i|<tuple|1.5.6|?>>
    <associate|dU=TdS-pdV+sum_i(mu_i)dn_i|<tuple|1.2.6|?>>
    <associate|dU=dq (dV=0)|<tuple|1.3.9|?>>
    <associate|dU=dq-pdV+dw'|<tuple|1.2.1|?>>
    <associate|delH=q (dp=0)|<tuple|1.3.8|?>>
    <associate|dp(T,V)|<tuple|1.1.1|?>>
    <associate|dq\<less\>=TdS|<tuple|1.8.2|?>>
    <associate|fig:5-surface tension device|<tuple|1.7.1|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-1.2.1|<tuple|1.2.1|?>>
    <associate|footnote-1.5.1|<tuple|1.5.1|?>>
    <associate|footnote-1.8.1|<tuple|1.8.1|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-1.2.1|<tuple|1.2.1|?>>
    <associate|footnr-1.5.1|<tuple|1.5.1|?>>
    <associate|footnr-1.8.1|<tuple|1.8.1|?>>
    <associate|ht cap conditions|<tuple|1.6.1|?>>
    <associate|mu = Gm|<tuple|Gibbs energy|?>>
    <associate|mu=.=.=.=.|<tuple|1.5.5|?>>
    <associate|mu=chem pot|<tuple|chemical potential|?>>
    <associate|mu_i=.=.=.=.|<tuple|1.5.10|?>>
    <associate|reciprocity reln|<tuple|1.4.14|?>>
    <associate|rectification|<tuple|<with|mode|<quote|math>|\<bullet\>>|?>>
    <associate|spon at const T \ V|<tuple|1.8.6|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      knuiman-12

      greenbook-3

      gibbs-76

      ben-amotz-03

      honig-06
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1.7.1>||>|<pageref|auto-66>>
    </associate>
    <\associate|gly>
      <tuple|normal|thermodynamic potentials|<pageref|auto-4>>

      <tuple|normal|total differential|<pageref|auto-8>>

      <tuple|normal|nonexpansion work|<pageref|auto-13>>

      <tuple|normal|chemical potential|<pageref|auto-18>>

      <tuple|normal|Enthalpy|<pageref|auto-30>>

      <tuple|normal|Helmholtz energy|<pageref|auto-33>>

      <tuple|normal|Gibbs energy|<pageref|auto-36>>

      <tuple|normal|Gibbs equations|<pageref|auto-40>>

      <tuple|normal|reciprocity relation|<pageref|auto-44>>

      <tuple|normal|Maxwell relations|<pageref|auto-46>>

      <tuple|normal|heat capacity at constant volume|<pageref|auto-61>>

      <tuple|normal|molar heat capacity at constant volume|<pageref|auto-62>>

      <tuple|normal|heat capacity at constant pressure|<pageref|auto-63>>

      <tuple|normal|molar heat capacity at constant
      pressure|<pageref|auto-64>>

      <tuple|normal|surface tension|<pageref|auto-68>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Thermodynamic|potential>|<pageref|auto-2>>

      <tuple|<tuple|Potential|thermodynamic>|<pageref|auto-3>>

      <tuple|<tuple|Total differential>|<pageref|auto-6>>

      <tuple|<tuple|Differential|total>|<pageref|auto-7>>

      <tuple|<tuple|First law of thermodynamics>|<pageref|auto-10>>

      <tuple|<tuple|Nonexpansion work>|<pageref|auto-11>>

      <tuple|<tuple|Work|nonexpansion>|<pageref|auto-12>>

      <tuple|<tuple|Dissipative work>|<pageref|auto-14>>

      <tuple|<tuple|Work|dissipative>|<pageref|auto-15>>

      <tuple|<tuple|Chemical potential>|<pageref|auto-16>>

      <tuple|<tuple|Potential|chemical>|<pageref|auto-17>>

      <tuple|<tuple|Chemical work>|<pageref|auto-19>>

      <tuple|<tuple|Work|chemical>|<pageref|auto-20>>

      <tuple|<tuple|Natural variables>|<pageref|auto-22>>

      <tuple|<tuple|Variables|natural>|<pageref|auto-23>>

      <tuple|<tuple|Legendre transform>|<pageref|auto-24>>

      <tuple|<tuple|Conjugate|variables>|<pageref|auto-25>>

      <tuple|<tuple|Variables|conjugate>|<pageref|auto-26>>

      <tuple|<tuple|Conjugate|pair>|<pageref|auto-27>>

      <tuple|<tuple|Legendre transform>|<pageref|auto-28>>

      <tuple|<tuple|Enthalpy>|<pageref|auto-29>>

      <tuple|<tuple|Helmholtz energy>|<pageref|auto-31>>

      <tuple|<tuple|Energy|Helmholtz>|<pageref|auto-32>>

      <tuple|<tuple|Gibbs energy>|<pageref|auto-34>>

      <tuple|<tuple|Energy|Gibbs>|<pageref|auto-35>>

      <tuple|<tuple|IUPAC Green Book>|<pageref|auto-37>>

      <tuple|<tuple|Gibbs|equations>|<pageref|auto-39>>

      <tuple|<tuple|Natural variables>|<pageref|auto-41>>

      <tuple|<tuple|Variables|natural>|<pageref|auto-42>>

      <tuple|<tuple|Reciprocity relation>|<pageref|auto-43>>

      <tuple|<tuple|Maxwell relations>|<pageref|auto-45>>

      <tuple|<tuple|Natural variables>|<pageref|auto-48>>

      <tuple|<tuple|Variables|natural>|<pageref|auto-49>>

      <tuple|<tuple|Gibbs|equations>|<pageref|auto-50>>

      <tuple|<tuple|Gibbs energy|molar>|<pageref|auto-51>>

      <tuple|<tuple|Natural variables>|<pageref|auto-52>>

      <tuple|<tuple|Variables|natural>|<pageref|auto-53>>

      <tuple|<tuple|Legendre transform>|<pageref|auto-54>>

      <tuple|<tuple|Gibbs, Josiah Willard>|<pageref|auto-55>>

      <tuple|<tuple|Fundamental equations, Gibbs>|<pageref|auto-56>>

      <tuple|<tuple|Gibbs|fundamental equations>|<pageref|auto-57>>

      <tuple|<tuple|Partial molar|Gibbs energy>|<pageref|auto-58>>

      <tuple|<tuple|Chemical potential>|<pageref|auto-59>>

      <tuple|<tuple|Surface tension>|<pageref|auto-67>>

      <tuple|<tuple|Nonexpansion work>|<pageref|auto-70>>

      <tuple|<tuple|Work|nonexpansion>|<pageref|auto-71>>

      <tuple|<tuple|Excess|function>|<pageref|auto-72>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Thermodynamic
      Potentials> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Total Differential of a Dependent Variable
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>

      1.2<space|2spc>Total Differential of the Internal Energy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>

      1.3<space|2spc>Enthalpy, Helmholtz Energy, and Gibbs Energy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21>

      1.4<space|2spc>Closed Systems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-38>

      1.5<space|2spc>Open Systems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-47>

      1.6<space|2spc>Expressions for Heat Capacity
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-60>

      1.7<space|2spc>Surface Work <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-65>

      1.8<space|2spc>Criteria for Spontaneity
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-69>
    </associate>
  </collection>
</auxiliary>