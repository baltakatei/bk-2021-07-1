<TeXmacs|1.99.21>

<style|<tuple|generic|std-latex>>

<\body>
  <\hide-preamble>
    <assign|R|<macro|\<bbb-R\>>>
  </hide-preamble>

  <label|Chap12><problem><label|prb:12-CaCO3> Consider the heterogeneous
  equilibrium <math|<chem>C*a*C*O<rsub|3><around|(|s|)><arrows>C*a*O<around|(|s|)>+C*O<rsub|2><around|(|g|)>>.
  Table <reference|tbl:12-CO2 pressure><vpageref|tbl:12-CO2 pressure> lists
  pressures measured over a range of temperatures for this system.

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|5|5|cell-col-span|1>|<cwith|1|1|5|5|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|5|5|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|t/<degC>>>|<cell|<ccol|<math|p/<text|Torr>>>>|<cell|<space|2em>>|<cell|<ccol|<math|t/<degC>>>>|<cell|<math|p/<text|Torr>>>>|<row|<cell|842.3>|<cell|343.0>|<cell|>|<cell|904.3>|<cell|879.0>>|<row|<cell|852.9>|<cell|398.6>|<cell|>|<cell|906.5>|<cell|875.0>>|<row|<cell|854.5>|<cell|404.1>|<cell|>|<cell|937.0>|<cell|1350>>|<row|<cell|868.9>|<cell|510.9>|<cell|>|<cell|937.0>|<cell|1340>>>>>
  </big-table|<label|tbl:12-CO2 pressure>Pressure of an equilibrium system
  containing CaCO<rsub|<math|3>>(s), CaO(s), and
  CO<rsub|<math|2>>(g)<space|.15em><footnote|Ref. <cite|symth-23>.>>

  <\problemparts>
    \ <problempart>What is the approximate relation between <math|p> and
    <math|K>? <soln| <math|<D>K=<frac|a<subs|C*a*O>a<subs|C*O<rsub|<math|2>>>|a<subs|C*a*C*O<rsub|<math|3>>>>=<frac|<G><subs|C*a*O><fug><subs|C*O<rsub|<math|2>>>/p<st>|<G><subs|C*a*C*O<rsub|<math|3>>>>>
    <vs>Approximate the pressure coefficients of the solids by unity and the
    CO<rsub|<math|2>> fugacity by <math|<fug><subs|C*O<rsub|<math|2>>>\<approx\>p<subs|C*O<rsub|<math|2>>>=p>:
    <vs><math|K\<approx\>p/p<st>>> <problempartAns><label|CaCO3 eval> Plot
    these data in the form <math|ln K> versus <math|1/T>, or fit <math|ln K>
    to a linear function of <math|1/T>. Then, evaluate the temperature at
    which the partial pressure of the CO<rsub|<math|2>> is <math|1 <br>>, and
    the standard molar reaction enthalpy at this temperature.

    <answer|<math|T=1168<K>><next-line><math|\<Delta\><rsub|<text|r>>*H<st>=1.64<timesten|5>
    <text|J>\<cdot\><text|mol><rsup|-1>>>

    <\soln>
      \ From the values of <math|p/<text|Torr>>, calculate <math|p/p<st>>
      using <vs><math|<D>p<st>=<around|(|1<br>|)><frac|10<rsup|5>
      <text|Pa>\<cdot\><text|bar><rsup|-1>|<around|(|101,325/760|)>
      <text|Pa>\<cdot\><text|Torr><rsup|-1>>=750.06 <text|Torr>> <vs>The
      values of <math|1/T> and <math|ln <around|(|p/p<st>|)>\<approx\>ln K>
      are listed in Table <reference|tbl:12-CaCO3> and plotted in Fig.
      <reference|fig:12-CaCO3>.

      <\big-table>
        <minipagetable|10cm|<label|tbl:12-CaCO3><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|5|5|cell-col-span|1>|<cwith|1|1|5|5|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|5|5|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|<around|(|1/T|)>/10<rsup|-4>
        <text|K<rsup|-1>>>>|<cell|<ccol|<math|ln
        <around|(|p/p<st>|)>>>>|<cell|<space|2em>>|<cell|<ccol|<math|<around|(|1/T|)>/10<rsup|-4>
        <text|K<rsup|-1>>>>>|<cell|<math|ln
        <around|(|p/p<st>|)>>>>|<row|<cell|8.965>|<cell|-0.7824>|<cell|>|<cell|8.493>|<cell|0.1586>>|<row|<cell|8.881>|<cell|-0.6322>|<cell|>|<cell|8.477>|<cell|0.1894>>|<row|<cell|8.868>|<cell|-0.6185>|<cell|>|<cell|8.263>|<cell|0.5877>>|<row|<cell|8.756>|<cell|-0.3840>|<cell|>|<cell|8.263>|<cell|0.5803>>>>>>
      </big-table|Data for Problem 12.<reference|prb:12-CaCO3><reference|CaCO3
      eval>>

      <\big-figure>
        <boxedfigure|<image|./12-SUP/CaCO3.eps||||> <capt|Graph for Problem
        12.<reference|prb:12-CaCO3><reference|CaCO3 eval>. The solid line is
        the least-squares fit to the points: <math|ln
        <around|(|p/p<st>|)>=a+b*<around|(|1/T|)>/10<rsup|-4>
        <text|K<rsup|-1>>> with <math|a=16.839>,
        <math|b=-1.967>.<label|fig:12-CaCO3>>>
      </big-figure|>

      From either the plot or the equation for the least-squares line,
      <math|p<subs|C*O<rsub|<math|2>>>> equals <math|1<br>> and <math|ln
      <around|(|p/p<st>|)>> is zero when <math|1/T> equals
      <math|8.561<timesten|-4> <text|K<rsup|-1>>> and <math|T> is
      <math|1168<K>>. Use Eq. <reference|del(r)Hmo=-R*dln(K)/d(1/T)> to
      calculate <math|\<Delta\><rsub|<text|r>>*H<st>> from the slope of the
      least-squares line: <vs><math|<D>\<Delta\><rsub|<text|r>>*H<st>=-R*<frac|<dif>ln
      <around|(|p/p<st>|)>|<dif><around|(|1/T|)>>=-<around|(|<R>|)>*<around|(|-1.97<timesten|4>
      <text|K>|)>> <vs><math|<phantom|\<Delta\><rsub|<text|r>>*H<st>>=1.64<timesten|5>
      <text|J>\<cdot\><text|mol><rsup|-1>>
    </soln>
  </problemparts>

  <problem>For a homogeneous reaction in which the reactants and products are
  solutes in a solution, write a rigorous relation between the standard molar
  reaction enthalpy and the temperature dependence of the thermodynamic
  equilibrium constant, with solute standard states based on concentration.
  <soln| Solve Eq. <reference|d(lnK)/dT=del(r)Hmo/RT2-...> for
  <math|\<Delta\><rsub|<text|r>>*H<st>>: <vs><math|<D>\<Delta\><rsub|<text|r>>*H<st>=R*T<rsup|2>*<frac|<dif>ln
  K|<dif>T>+R*T<rsup|2>*\<alpha\><A><rsup|\<ast\>><big|sum><rsub|i\<neq\>A>\<nu\><rsub|i>>
  <vs><math|\<alpha\><A><rsup|\<ast\>>> is the cubic expansion coefficient of
  the pure solvent.>

  <problem><label|prb:12-Del(r)S(m)o> Derive an expression for the
  <I|Entropy!reaction!standard molar\|p>standard molar reaction entropy of a
  reaction that can be used to calculate its value from the thermodynamic
  equilibrium constant and its temperature derivative. Assume that no solute
  standard states are based on concentration. <soln| Combine the relations
  <math|\<Delta\><rsub|<text|r>>*G<st>=-R*T*ln K> (Eq.
  <reference|del(r)Gmo=-RT*ln(K)>) and <math|\<Delta\><rsub|<text|r>>*G<st>=\<Delta\><rsub|<text|r>>*H<st>-T\<Delta\><rsub|<text|r>>*S<st>>
  (Eq. <reference|del(r)Gmo=del(r)Hmo-Tdel(r)Smo>):
  <vs><math|\<Delta\><rsub|<text|r>>*S<st>=R*ln
  K+<around|(|1/T|)>\<Delta\><rsub|<text|r>>*H<st>> <vs>From Eq.
  <reference|del(r)Hmo=(RT^2)dln(K)/dT>, substitute
  <vs><math|<D>\<Delta\><rsub|<text|r>>*H<st>=R*T<rsup|2>*<frac|<dif>ln
  K|<dif>T>> <vs>to obtain the relation <vs><math|<D>\<Delta\><rsub|<text|r>>*S<st>=R*ln
  K+R*T*<frac|<dif>ln K|<dif>T>>>

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|M>>|<cell|<math|t<subs|f>>>|<cell|<math|t<bd>>>|<cell|<math|\<Delta\><rsub|<text|fus>>*H>>|<cell|<math|\<Delta\><rsub|<text|vap>>*H>>>|<row|<cell|<math|18.0153
    <text|g>\<cdot\><text|mol><rsup|-1>>>|<cell|<math|0.00
    <degC>>>|<cell|<math|99.61 <degC>>>|<cell|<math|6.010
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>|<cell|<math|40.668
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>>>>>
  </big-table|<label|tbl:12-Kb Kf>Properties of H<rsub|<math|2>>O at
  <math|1<br>>>

  <problemAns><label|prb:12-Kf Kb> Use the data in Table <reference|tbl:12-Kb
  Kf><vpageref|tbl:12-Kb Kf> to evaluate the molal freezing-point depression
  constant and the molal boiling-point elevation constant for
  H<rsub|<math|2>>O at a pressure of <math|1<br>>.

  <\answer>
    <math|K<subs|f>=1.860 <text|K>\<cdot\><text|kg>\<cdot\><text|mol><rsup|-1>><next-line><math|K<bd>=0.5118
    <text|K>\<cdot\><text|kg>\<cdot\><text|mol><rsup|-1>>
  </answer>

  <\soln>
    \ <math|<D>K<subs|f>=<frac|M<A>R<around|(|T<f><rsup|\<ast\>>|)><rsup|2>|\<Delta\><rsub|<text|fus>,<text|A>>*H>=<frac|<around|(|18.0153<timesten|-3>
    <text|kg>\<cdot\><text|mol><rsup|-1>|)><around|(|<R>|)><around|(|273.15<K>|)><rsup|2>|6.010<timesten|3>
    <text|J>\<cdot\><text|mol><rsup|-1>>> <vs><math|<phantom|K<subs|f>>=1.860
    <text|K>\<cdot\><text|kg>\<cdot\><text|mol><rsup|-1>>
    <vs><math|<D>K<bd>=<frac|M<A>R<around|(|T<bd><rsup|\<ast\>>|)><rsup|2>|\<Delta\><rsub|<text|vap>,<text|A>>*H>=<frac|<around|(|18.0153<timesten|-3>
    <text|kg>\<cdot\><text|mol><rsup|-1>|)><around|(|<R>|)><around|(|372.76<K>|)><rsup|2>|40.668<timesten|3>
    <text|J>\<cdot\><text|mol><rsup|-1>>> <vs><math|<phantom|K<bd>>=0.5118
    <text|K>\<cdot\><text|kg>\<cdot\><text|mol><rsup|-1>>
  </soln>

  <problemAns>An aqueous solution of the protein bovine serum albumin,
  containing\ 

  2.00<timesten|-2> <text|g> of protein per cubic centimeter, has an osmotic
  pressure of <math|8.1<timesten|-3><br>> at <math|0 <degC>>. Estimate the
  molar mass of this protein.

  <\answer>
    <math|M<B>\<approx\>5.6<timesten|4> <text|g>\<cdot\><text|mol><rsup|-1>>
  </answer>

  <\soln>
    \ From van't Hoff's equation for osmotic pressure:
    <vs><math|<D>c<B>\<approx\><frac|<varPi>|\<nu\>*R*T>=<frac|<around|(|8.1<timesten|-3><br>|)><around|(|10<rsup|5>
    <text|Pa>\<cdot\><text|bar><rsup|-1>|)>|<around|(|1|)><around|(|<R>|)><around|(|273<K>|)>>=0.36
    <text|mol>\<cdot\><text|m><rsup|-3>> <vs><math|n<B>\<approx\><around|(|0.36
    <text|mol>\<cdot\><text|m><rsup|-3>|)><around|(|1
    <text|cm><rsup|3>|)><around|(|10<rsup|-2>
    <text|m>\<cdot\><text|cm><rsup|-1>|)><rsup|3>=3.6<timesten|-7><mol>>
    <vs><math|<D>M<B>\<approx\><frac|2.00<timesten|-2>
    <text|g>|3.6<timesten|-7><mol>>=5.6<timesten|4>
    <text|g>\<cdot\><text|mol><rsup|-1>>
  </soln>

  <problemAns><label|prb:12-BuBenz> Figure
  <reference|fig:12-butylbenzene><vpageref|fig:12-butylbenzene> shows a curve
  fitted to experimental points for the aqueous solubility of
  <math|n>-butylbenzene. The curve has the equation <math|ln
  x<B>=a*<around|(|t/<degC>-b|)><rsup|2>+c>, where the constants have the
  values <math|a=3.34<timesten|-4>>, <math|b=12.13>, and <math|c=-13.25>.
  Assume that the saturated solution behaves as an ideal-dilute solution, use
  a solute standard state based on mole fraction, and calculate
  <math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>> and
  <math|\<Delta\><rsub|<text|sol>,<text|B>>*S<st>> at <math|5.00 <degC>>,
  <math|12.13 <degC>> (the temperature of minimum solubility), and
  <math|25.00 <degC>>.

  <\answer>
    <math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>/<text|kJm*o*l<per>>=-3.06,0,6.35><next-line><math|\<Delta\><rsub|<text|sol>,<text|B>>*S<st>/<text|JK<per>m*o*l<per>>><next-line><math|<phantom|m*m>=-121.0,-110.2,-88.4>
  </answer>

  <\soln>
    \ Rewrite the equation for the curve using thermodynamic temperature:
    <vs><math|ln x<B>=A*<around|(|T-B|)><rsup|2>+C> <vs>where
    <math|A=3.34<timesten|-4> <text|K><rsup|-2><space|1em>B=<around|(|12.13+273.15|)><K>=285.28<K><space|1em>C=c=-13.25>
    <vs>From Sec. <reference|12-sol of l>, with <math|K=x<B>>:
    <vs><math|<D>\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=R*T<rsup|2>*<frac|<dif>ln
    x<B>|<dif>T>=<around|[|R*T<rsup|2>|]>*<around|[|2*A*<around|(|T-B|)>|]>>
    <vs><math|\<Delta\><rsub|<text|sol>,<text|B>>*G<st>=-R*T*ln
    x<B>=-R*T*<around|[|A*<around|(|T-B|)><rsup|2>+C|]>>
    <vs><math|<D>\<Delta\><rsub|<text|sol>,<text|B>>*S<st>=<frac|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>-\<Delta\><rsub|<text|sol>,<text|B>>*G<st>|T>>
    <vs>The formula of Prob. 12.<reference|prb:12-Del(r)S(m)o> can also be
    used, with <math|K> replaced with <math|x<B>>:
    <vs><math|<D>\<Delta\><rsub|<text|sol>,<text|B>>*S<st>=R*ln
    x<B>+R*T*<frac|<dif>ln x<B>|<dif>T>> <vs>The values calculated at the
    three temperatures are listed in Table
    <reference|tbl:12prob-BuBenz><vpageref|tbl:12prob-BuBenz>.

    <\big-table>
      <minipagetable|10cm|<label|tbl:12prob-BuBenz><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|5|5|cell-col-span|1>|<cwith|1|1|5|5|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|4|4|1|-1|cell-bborder|1ln>|<table|<row|<cell|<ccol|<math|t/<degC>>>>|<cell|<ccol|<math|T/<text|K>>>>|<cell|<ccol|<math|<D><frac|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>|<text|kJm*o*l<per>>>>>>|<cell|<ccol|<math|<D><frac|\<Delta\><rsub|<text|sol>,<text|B>>*G<st>|<text|kJm*o*l<per>>>>>>|<cell|<math|<D><frac|\<Delta\><rsub|<text|sol>,<text|B>>*S<st>|<text|JK<per>m*o*l<per>>>>>>|<row|<cell|5.00>|<cell|278.15>|<cell|-3.06>|<cell|30.60>|<cell|-121.0>>|<row|<cell|12.13>|<cell|285.28>|<cell|0>|<cell|31.43>|<cell|-110.2>>|<row|<cell|25.00>|<cell|298.15>|<cell|6.35>|<cell|32.71>|<cell|-88.4>>>>>>
    </big-table|Problem 12.<reference|prb:12-BuBenz>>
  </soln>

  <problem>Consider a hypothetical system in which two aqueous solutions are
  separated by a semipermeable membrane. Solution <math|<pha>> is prepared by
  dissolving <math|1.00<timesten|-5><mol>> KCl in\ 

  10.0 <text|g> water. Solution <math|<phb>> is prepared from
  <math|1.00<timesten|-5><mol>> KCl and <math|1.00<timesten|-6><mol>> of the
  potassium salt of a polyelectrolyte dissolved in\ 

  10.0 <text|g> water. All of solution <math|<phb>> is used to fill a
  dialysis bag, which is then sealed and placed in solution <math|<pha>>.

  Each polyelectrolyte ion has a charge of <math|-10>. The membrane of the
  dialysis bag is permeable to the water molecules and to the
  K<rsup|<math|+>> and Cl<rsup|<math|->> ions, but not to the
  polyelectrolyte. The system comes to equilibrium at <math|25.00 <degC>>.
  Assume that the volume of the dialysis bag remains constant. Also make the
  drastic approximation that both solutions behave as ideal-dilute solutions.

  <\problemparts>
    \ <problempartAns>Find the equilibrium molality of each solute species in
    the two solution phases.

    <answer|<math|m<rsub|+><aph>=m<rsub|-><aph>=1.20<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>><next-line><math|m<rsub|+><bph>=1.80<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>><next-line><math|m<rsub|-><bph>=0.80<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>><next-line><math|m<subs|P>=2.00<timesten|-6>
    <text|mol>\<cdot\><text|kg><rsup|-1>>>

    <soln| Polyelectrolyte molality: <math|<D>m<subs|P>=<frac|1.00<timesten|-6><mol>|10.0<timesten|-3>
    <text|kg>>=1.00<timesten|-4> <text|mol>\<cdot\><text|kg><rsup|-1>>
    <vs>Calculate the initial molalities of K<rsup|<math|+>> and
    Cl<rsup|<math|->>: <vs><math|<D>m<rsub|+><aph>=m<rsub|-><aph>=m<rsub|-><bph>=<frac|1.00<timesten|-5><mol>|10.0<timesten|-3>
    <text|kg>>=1.00<timesten|-3> <text|mol>\<cdot\><text|kg><rsup|-1>>
    <vs><math|m<rsub|+><bph>=1.00<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>+<around|(|10|)><around|(|1.00<timesten|-4>
    <text|mol>\<cdot\><text|kg><rsup|-1>|)>=2.00<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>> <vs>Simultaneously solve the
    following equations for <math|m<rsub|-><aph>> and <math|m<rsub|-><bph>>
    in the equilibrium system: <vs><math|m<rsub|-><aph>+m<rsub|-><bph>=2.00<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>>
    <vs><math|<around|(|m<rsub|-><aph>|)><rsup|2>=<around|(|m<rsub|-><bph>+z*m<subs|P>|)>*m<rsub|-><bph>=<around|(|m<rsub|-><bph>+1.00<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>|)>*m<rsub|-><bph>> <vs>The resulting
    equilibrium molalities are <vs><math|m<rsub|-><aph>=1.20<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1><space|2em>m<rsub|-><bph>=0.80<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>> <vs>Find the equilibrium values of
    <math|m<rsub|+><aph>> and <math|m<rsub|+><bph>> from the requirement of
    electroneutrality in each phase: <vs><math|m<rsub|+><aph>=m<rsub|-><aph>=1.20<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>>
    <vs><math|m<rsub|+><bph>=m<rsub|-><bph>+z*m<subs|P>=1.80<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>>> <problempart>Describe the amounts
    and directions of any macroscopic transfers of ions across the membrane
    that are required to establish the equilibrium state. <soln| The change
    in the amount of KCl in phase <math|<pha>> is
    <vs><math|<around|(|1.20<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>|)><around|(|10.0<timesten|-3>
    <text|kg>|)>-1.00<timesten|-5><mol>=2.0<timesten|-6><mol>> <vs>Thus,
    <math|2.0<timesten|-6><mol>> KCl has transferred from phase <math|<phb>>
    to phase <math|<pha>>.> <problempart>Estimate the Donnan potential,
    <math|\<phi\><aph>-\<phi\><bph>>. <soln| Apply Eq.
    <reference|phi(a)-phi(b)(cation)>: <vs><math|<D>\<phi\><aph>-\<phi\><bph>\<approx\><frac|R*T|F>*ln
    <frac|m<rsub|+><bph>|m<rsub|+><aph>>=<frac|<around|(|<R>|)><around|(|298.15<K>|)>|<around|(|96,485<units|C*m*o*l<per>>|)>>*ln
    <frac|1.80<timesten|-3> <text|mol>\<cdot\><text|kg><rsup|-1>|1.20<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>>>
    <vs><math|<phantom|\<phi\><aph>-\<phi\><bph>>=0.0104<V>>>
    <problempart>Estimate the pressure difference across the membrane at
    equilibrium. (The density of liquid H<rsub|<math|2>>O at <math|25.00
    <degC>> is <math|0.997 <text|g>\<cdot\><text|cm><rsup|-3>>.) <soln| From
    Eq. <reference|p(beta)-p(alpha)(Donnan)=>:
    <math|p<bph>-p<aph>\<approx\>\<rho\><A><rsup|\<ast\>>R*T*<around|[|<around|(|m<rsub|+><bph>+m<rsub|-><bph>+m<subs|P>|)>-<around|(|m<rsub|+><aph>+m<rsub|-><aph>|)>|]>>
    <vs><math|<phantom|p<bph>-p<aph>>=<around|(|0.997
    <text|g>\<cdot\><text|cm><rsup|-3>|)><around|(|10<rsup|-3>
    <text|kg>\<cdot\><text|g><rsup|-1>|)><around|(|10<rsup|6>
    <text|cm><rsup|3>\<cdot\><text|m><rsup|-3>|)>>
    <vs><math|<phantom|p<bph>-p<aph>=>\<times\><around|(|<R>|)><around|(|298.15<K>|)>>
    <vs><math|<phantom|p<bph>-p<aph>=>\<times\><around|[|<around|(|1.80+0.80+0.100-1.20-1.20|)>*10<rsup|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>|]>>
    <vs><math|<phantom|p<bph>-p<aph>>=7.4<timesten|2><Pa>>>
  </problemparts>

  <problem><label|prb:12-droplet>The derivation of Prob.
  9.<reference|prb:9-droplet><vpageref|prb:9-droplet> shows that the pressure
  in a liquid droplet of radius <math|r> is greater than the pressure of the
  surrounding equilibrated gas phase by a quantity <math|2<g>/r>, where
  <math|<g>> is the surface tension.

  <\problemparts>
    \ <problempartAns>Consider a droplet of water of radius
    <math|1.00<timesten|-6> <text|m>> at <math|25 <degC>> suspended in air of
    the same temperature. The surface tension of water at this temperature is
    <math|0.07199 <text|J>\<cdot\><text|m><rsup|-2>>. Find the pressure in
    the droplet if the pressure of the surrounding air is <math|1.00<br>>.

    <answer|<math|p<sups|l>=2.44<br>>>

    <soln| <math|<D>p<sups|l>=p<rsup|<text|g>>+<frac|2<g>|r>=1.00<br>+<frac|2<around|(|0.07199
    <text|J>\<cdot\><text|m><rsup|-2>|)>|1.00<timesten|-6>
    <text|m>>=2.44<timesten|5><Pa>=2.44<br>>> <problempartAns>Calculate the
    difference between the fugacity of H<rsub|<math|2>>O in the air of
    pressure <math|1.00<br>> equilibrated with this water droplet, and the
    fugacity in air equilibrated at the same temperature and pressure with a
    pool of liquid water having a flat surface. Liquid water at <math|25
    <degC>> and <math|1<br>> has a vapor pressure of <math|0.032<br>> and a
    molar volume of <math|1.807<timesten|-5>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>.

    <answer|<math|<fug><around|(|2.44<br>|)>-<fug><around|(|1.00<br>|)>><next-line><math|<phantom|m*m>=3.4<timesten|-5><br>>>

    <soln| From Eq. <reference|f_i(p2) approx>:
    <vs><math|<D><fug><around|(|p<rsub|2>|)>=<fug><around|(|p<rsub|1>|)>*exp
    <around*|[|<frac|V<m><liquid><around|(|p<rsub|2>-p<rsub|1>|)>|R*T>|]>>
    <vs><math|<D><phantom|<fug><around|(|p<rsub|2>|)>>=<fug><around|(|p<rsub|1>|)>*exp
    <around*|[|<frac|<around|(|1.807<timesten|-5>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>|)><around|(|1.44<timesten|5><Pa>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>>
    <vs><math|<phantom|<fug><around|(|p<rsub|2>|)>>=1.00105<fug><around|(|p<rsub|1>|)>>
    <vs><math|<fug><around|(|p<rsub|2>|)>-<fug><around|(|p<rsub|1>|)>=<around|(|1.00105-1|)><fug><around|(|p<rsub|1>|)>=<around|(|0.00105|)><around|(|0.032<br>|)>=3.4<timesten|-5><br>>>
  </problemparts>

  <problem>For a solution process in which species B is transferred from a
  gas phase to a liquid solution, find the relation between
  <math|\<Delta\><rsub|<text|sol>>*G<st>> (solute standard state based on
  mole fraction) and the Henry's law constant <math|<kHB>>. <soln|
  <math|\<Delta\><rsub|<text|sol>>*G<st>=-R*T*ln K> <vs>From Eq.
  <reference|K(x,B)=Gamma(x,B)po/K>: <math|K=<dfrac|<G><xbB>p<st>|<kHB>>>
  <vs>Under standard state conditions, <math|p=p<st>> and <math|<G><xbB>=1>
  <vs>Therefore <math|\<Delta\><rsub|<text|sol>>*G<st>=R*T*ln
  <around|[|<kHB><around|(|p<st>|)>/p<st>|]>>>

  <problem><label|prb:12-CO2>Crovetto<footnote|Ref. <cite|crovetto-91>.>
  reviewed the published data for the solubility of gaseous CO<rsub|<math|2>>
  in water, and fitted the Henry's law constant <math|<kHB>> to a function of
  temperature. Her recommended values of <math|<kHB>> at five temperatures
  are <math|1233<br>> at <math|15.00 <degC>>, <math|1433<br>> at <math|20.00
  <degC>>, <math|1648<br>> at <math|25.00 <degC>>, <math|1874<br>> at
  <math|30.00 <degC>>, and <math|2111<br>> at <math|35 <degC>>.

  <\problemparts>
    \ <problempartAns>The partial pressure of CO<rsub|<math|2>> in the
    atmosphere is typically about <math|3<timesten|-4><br>>. Assume a
    fugacity of <math|3.0<timesten|-4><br>>, and calculate the aqueous
    solubility at <math|25.00 <degC>> expressed both as a mole fraction and
    as a molality.

    <answer|<math|x<B>=1.8<timesten|-7>><next-line><math|m<B>=1.0<timesten|-5>
    <text|mol>\<cdot\><text|kg><rsup|-1>>>

    <soln| From Table <reference|tbl:9-act coeff-fugacity>:
    <math|x<B>=<fug><B>/<g><xbB><kHB>>. Assume that <math|<g><xbB>> is 1:
    <vs><math|<D>x<B>=<frac|<fug><B>|<kHB>>=<frac|3.0<timesten|-4><br>|1648<br>>=1.8<timesten|-7>>
    <vs>From Eq. <reference|nB/nA (dilute)>, at high dilution:
    <vs><math|<D>m<B>=<frac|x<B>|M<A>>=<frac|1.8<timesten|-7>|18.0153<timesten|-3>
    <text|kg>\<cdot\><text|mol><rsup|-1>>=1.0<timesten|-5>
    <text|mol>\<cdot\><text|kg><rsup|-1>>> <problempartAns>Find the standard
    molar enthalpy of solution at <math|25.00 <degC>>.

    <answer|<math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=-1.99<timesten|4>
    <text|J>\<cdot\><text|mol><rsup|-1>>>

    <\soln>
      \ From Eq. <reference|dln(k/po)/d(1/T)=>:
      <vs><math|<D>\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=R*<frac|<dif>ln
      <around|(|<kHB>/p<st>|)>|<dif><around|(|1/T|)>>> <vs>The values of
      <math|1/T> and <math|ln <kHB>/p<st>> are listed in Table
      <reference|tbl:12prob-CO2><vpageref|tbl:12prob-CO2>.

      <\big-table>
        <minipagetable|10cm|<label|tbl:12prob-CO2><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|4|4|cell-col-span|1>|<cwith|1|1|4|4|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|6|6|1|-1|cell-bborder|1ln>|<table|<row|<cell|<ccol|<math|t/<degC>>>>|<cell|<ccol|<math|T/<text|K>>>>|<cell|<ccol|<math|<around|(|1/T|)>/10<rsup|-3>
        <text|K<rsup|-1>>>>>|<cell|<math|ln
        <around|(|<kHB>/p<st>|)>>>>|<row|<cell|15.00>|<cell|288.15>|<cell|3.4704>|<cell|7.117>>|<row|<cell|20.00>|<cell|293.15>|<cell|3.4112>|<cell|7.268>>|<row|<cell|25.00>|<cell|298.15>|<cell|3.3540>|<cell|7.407>>|<row|<cell|30.00>|<cell|303.15>|<cell|3.2987>|<cell|7.536>>|<row|<cell|35.00>|<cell|308.15>|<cell|3.2452>|<cell|7.655>>>>>>
      </big-table|Data for Problem 12.<reference|prb:12-CO2>>

      The points are plotted in Fig. <reference|fig:12prob-CO2><vpageref|fig:12prob-CO2>.

      <\big-figure>
        <boxedfigure|<image|./12-SUP/CO2.eps||||> <capt|Plot for Problem
        12.<reference|prb:12-CO2>(b).<label|fig:12prob-CO2>>>
      </big-figure|>

      The tangent to the curve at the point for <math|298.15<K>> (dashed
      line) has the slope <math|<dif>ln <around|(|<kHB>/p<st>|)>/<dif><around|(|1/T|)>=-2.39<timesten|3><K>>.
      The same value may be obtained from the slope of a line between the two
      end points. <vs><math|<D>\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=<around|(|<R>|)>*<around|(|-2.39<timesten|3><K>|)>=-1.99<timesten|4>
      <text|J>\<cdot\><text|mol><rsup|-1>>
    </soln>

    \ <problempartAns>Dissolved carbon dioxide exists mostly in the form of
    CO<rsub|<math|2>> molecules, but a small fraction exists as
    H<rsub|<math|2>>CO<rsub|<math|3>> molecules, and there is also some
    ionization: <vs><math|<chem>C*O<rsub|2><around|(|a*q|)>+H<rsub|2>*O<around|(|l|)><arrow>H<rsup|+>*<around|(|a*q|)>+H*C*O<rsub|3><rsup|-><around|(|a*q|)>>
    <vs>(The equilibrium constant of this reaction is often called the first
    ionization constant of carbonic acid.) Combine the <math|<kHB>> data with
    data in Appendix <reference|app:props> to evaluate <math|K> and
    <math|\<Delta\><rsub|<text|r>>*H<st>> for the ionization reaction at
    <math|25.00 <degC>>. Use solute standard states based on molality, which
    are also the solute standard states used for the values in Appendix
    <reference|app:props>.

    <answer|<math|K=4.4<timesten|-7>><next-line><math|\<Delta\><rsub|<text|r>>*H<st>=9.3
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>

    <soln| The ionization reaction is the difference of the reaction
    <math|<chem>C*O<rsub|2><around|(|g|)>+H<rsub|2>*O<around|(|l|)><ra>H<rsup|+>*<around|(|a*q|)>+H*C*O<rsub|3><rsup|-><around|(|a*q|)>>
    and the solution process <math|<chem>C*O<rsub|2><around|(|g|)><ra>C*O<rsub|2><around|(|a*q|)>>.
    Calculate the standard molar reaction Gibbs energy and standard molar
    reaction enthalpy of the first reaction:
    <vs><math|\<Delta\><rsub|<text|r>>*G<st>/<text|kJm*o*l<per>>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>\<Delta\><rsub|<text|f>>*G<st><around|(|i|)>/<text|kJm*o*l<per>>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*G<st>/<text|kJm*o*l<per>>>=-<around|(|-394.41|)>-<around|(|-237.16|)>+<around|(|0|)>+<around|(|-586.90|)>=44.67>
    <vs><math|\<Delta\><rsub|<text|r>>*H<st>/<text|kJm*o*l<per>>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>\<Delta\><rsub|<text|f>>*H<st><around|(|i|)>/<text|kJm*o*l<per>>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*H<st>/<text|kJm*o*l<per>>>=-<around|(|-393.51|)>-<around|(|-285.830|)>+<around|(|0|)>+<around|(|-689.93|)>=-10.59>
    <vs>From Eq. <reference|K(x,B)=Gamma(x,B)po/K>, the equilibrium constant
    for the solution process, with standard state based on mole fraction, is
    related to <math|<kHB>> by <math|K=<G><xbB>p<st>/<kHB>>. Approximate
    <math|<G><xbB>> by 1 and take <math|<kHB>> at <math|25.00 <degC>>:
    <math|K=p<st>/<kHB>=1/1648=6.07<timesten|-4>>. Since the solute standard
    states used in Appendix <reference|app:props> are based on molality,
    convert the value using Eq. <reference|K(x basis)=.=.>:
    <vs><math|<D>K<text|<around|(|<math|m>
    basis|)>>=<frac|K<text|<around|(|<math|x>
    basis|)>>|M<A>m<st>>=<frac|6.07<timesten|-4>|<around|(|18.0153<timesten|-3>
    <text|kg>\<cdot\><text|mol><rsup|-1>|)><around|(|1
    <text|mol>\<cdot\><text|kg><rsup|-1>|)>>=3.37<timesten|-2>> <vs>Calculate
    the standard molar reaction Gibbs energy of the solution process:
    <vs><math|<D>\<Delta\><rsub|<text|sol>,<text|B>>*G<st>=-R*T*ln
    K<text|<around|(|<math|m> basis|)>>=-<around|(|<R>|)><around|(|298.15<K>|)>*ln
    <around|(|3.37<timesten|-2>|)>> <vs><math|<phantom|\<Delta\><rsub|<text|sol>,<text|B>>*G<st>>=8.41<timesten|3>
    <text|J>\<cdot\><text|mol><rsup|-1>> <vs>For the overall ionization
    reaction: <vs><math|\<Delta\><rsub|<text|r>>*G<st>/<text|kJm*o*l<per>>=44.67-<around|(|8.41|)>=36.26>
    <vs><math|<D>K=exp <around*|(|-<frac|\<Delta\><rsub|<text|r>>*G<st>|R*T>|)>=exp
    <around*|[|-<frac|36.26<timesten|3> <text|J>\<cdot\><text|mol><rsup|-1>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>=4.4<timesten|-7>>
    <vs><math|\<Delta\><rsub|<text|r>>*H<st>/<text|kJm*o*l<per>>=<around|(|-10.59|)>-<around|(|-19.9|)>=9.3>>
  </problemparts>

  <problem><label|prb:12-salting out>The solubility of gaseous
  O<rsub|<math|2>> at a partial pressure of <math|1.01<br>> and a temperature
  of <math|310.2<K>>, expressed as a concentration, is
  <math|1.07<timesten|-3> <text|mol>\<cdot\><text|dm><rsup|-3>> in pure water
  and <math|4.68<timesten|-4> <text|mol>\<cdot\><text|dm><rsup|-3>> in a
  <math|3.0 <text|m>> aqueous solution of KCl.<footnote|Ref. <cite|CRC-92>.>
  This solubility decrease is the <I|Salting-out effect on gas
  solubility\|p><em|salting-out effect>. Calculate the activity coefficient
  <math|<g><cbB>> of O<rsub|<math|2>> in the KCl solution. <soln| The
  equation for solubility expressed as a concentration analogous to Eq.
  <reference|xB=()fB/gB> is <vs><math|<D>c<B>=<frac|K*c<st><fug><B>/p<st>|<G><cbB><g><cbB>>>
  <vs>Assume that <math|<g><cbB>> is equal to 1 when no KCl is present, and
  that <math|<G><cbB>> is not affected by the presence of KCl:
  <vs><math|<D><g><cbB><text|<around|(|3.0MKCl|)>>=<g><cbB><text|<around|(|0MKCl|)>><frac|c<B><text|<around|(|0MKCl|)>>|c<B><text|<around|(|3.0MKCl|)>>>=<around|(|1|)><frac|1.07<timesten|-3>
  <text|mol>\<cdot\><text|dm><rsup|-3>|4.68<timesten|-4>
  <text|mol>\<cdot\><text|dm><rsup|-3>>=2.29>>

  <problem><label|prb:12-p effect>At <math|298.15<K>>, the partial molar
  volume of CO<rsub|<math|2>>(aq) is <math|33
  <text|cm><rsup|3>\<cdot\><text|mol><rsup|-1>>. Use Eq. <reference|KmB(p2)
  approx> to estimate the percent change in the value of the Henry's law
  constant <math|<kHB>> for aqueous CO<rsub|<math|2>> at <math|298.15<K>>
  when the total pressure is changed from <math|1.00<br>> to
  <math|10.00<br>>. <soln| From Eq. <reference|KmB(p2) approx>:
  <vs><math|<D><kHB><around|(|p<rsub|2>|)>\<approx\><kHB><around|(|p<rsub|1>|)>*exp
  <around*|[|<frac|V<B><rsup|\<infty\>><around|(|p<rsub|2>-p<rsub|1>|)>|R*T>|]>>
  <vs><math|<D><kHB><around|(|10.00<br>|)>\<approx\><kHB><around|(|1.00<br>|)>*exp
  <around*|[|<frac|<around|(|33<timesten|-6>
  <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>|)>*<around|(|10.00-1.00|)><timesten|5><Pa>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>>
  <vs><math|<phantom|<kHB><around|(|10.00<br>|)>>\<approx\>1.012<kHB><around|(|1.00<br>|)>>
  <vs><math|<D><text|percentchange>\<approx\>1.2%>>

  \;

  <problem><label|prb:12-gas sol>Rettich et al<footnote|Ref.
  <cite|rettich-00>.> made high-precision measurements of the solubility of
  gaseous oxygen (<math|<chem>O<rsub|2>>) in water. Each measurement was made
  by equilibrating water and oxygen in a closed vessel for a period of up to
  two days, at a temperature controlled within <math|\<pm\>0.003<K>>. The
  oxygen was extracted from samples of known volume of the equilibrated
  liquid and gas phases, and the amount of <math|<chem>O<rsub|2>> in each
  sample was determined from <math|p>-<math|V>-<math|T> measurements taking
  gas nonideality into account. It was then possible to evaluate the mole
  fraction <math|x<B>> of <math|<chem>O<rsub|2>> in the liquid phase and the
  ratio <math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>> for the
  <math|<chem>O<rsub|2>> in the gas phase.

  <\big-table>
    <minipagetable|9.9cm|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-valign|top>|<cwith|1|1|1|-1|cell-vmode|exact>|<cwith|1|1|1|-1|cell-height|<plus|1fn|1ex>>|<cwith|2|2|1|-1|cell-valign|top>|<cwith|2|2|1|-1|cell-vmode|exact>|<cwith|2|2|1|-1|cell-height|<plus|1fn|1ex>>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|1ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|1ex>>|<cwith|5|5|1|-1|cell-valign|top>|<cwith|5|5|1|-1|cell-vmode|exact>|<cwith|5|5|1|-1|cell-height|<plus|1fn|1ex>>|<cwith|6|6|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|T=298.152<K>>>|<cell|<space|2em>>|<cell|Second
    virial coefficients:>>|<row|<cell|<math|x<B>=2.02142<timesten|-5>>>|<cell|>|<cell|<math|B<subs|A*A>=-1152<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<cell|<math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>=35.9957
    <text|mol>\<cdot\><text|m><rsup|-3>>>|<cell|>|<cell|<math|B<subs|B*B>=-16.2<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<cell|<math|p<A><rsup|\<ast\>>=3167.13<Pa>>>|<cell|>|<cell|<math|B<subs|A*B>=-27.0<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>>|<row|<cell|<math|V<A><rsup|\<ast\>>=18.069<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>|<cell|>|<cell|>>|<row|<cell|<math|V<B><rsup|\<infty\>>=31.10<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>>>|<cell|>|<cell|>>>>>>
  </big-table|<label|tbl:12-gas sol>Data for Problem 12.<reference|prb:12-gas
  sol> (A = H<rsub|<math|2>>O, B = O<rsub|<math|2>>)>

  Table <reference|tbl:12-gas sol> gives values of physical quantities at
  <math|T=298.152<K>> needed for this problem. The values of <math|x<B>> and
  <math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>> were obtained by
  Rettich et al from samples of liquid and gas phases equilibrated at
  temperature <math|T>, as explained above. <math|p<A><rsup|\<ast\>>> is the
  saturation vapor pressure of pure liquid water at this temperature.

  Your calculations will be similar to those used by Rettich et al to obtain
  values of the Henry's law constant of oxygen to six significant figures.
  Your own calculations should also be carried out to six significant
  figures. For the gas constant, use the value <math|R=<Rsix>>.

  The method you will use to evaluate the Henry's law constant
  <math|<kHB>=<fug><B>/x<B>> at the experimental temperature and pressure is
  as follows. The value of <math|x<B>> is known, and you need to find the
  fugacity <math|<fug><B>> of the <math|<chem>O<rsub|2>> in the gas phase.
  <math|<fug><B>> can be calculated from <math|\<phi\><B>> and <math|p<B>>.
  These in turn can be calculated from the pressure <math|p>, the mole
  fraction <math|y<B>> of <math|<chem>O<rsub|2>> in the gas phase, and known
  values of second virial coefficients. You will calculate <math|p> and
  <math|y<B>> by an iterative procedure. Assume the gas has the virial
  equation of state <math|<around|(|V<rsup|<text|g>>/n<rsup|<text|g>>|)>=<around|(|R*T/p|)>+B>
  (Eq. <reference|V=nRT/p+nB>) and use relevant relations in Sec.
  <reference|9-real gas mixtures>.

  <\problemparts>
    \ <problempartAns>For the equilibrated liquid-gas system, calculate
    initial approximate values of <math|p> and <math|y<B>> by assuming that
    <math|p<A>> is equal to <math|p<A><rsup|\<ast\>>> and <math|p<B>> is
    equal to <math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>*R*T>.

    <answer|<math|p=92399.6<Pa>>, <math|y<B>=0.965724>>

    <soln| <math|p<B>=<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>*R*T=<around|(|35.9957
    <text|mol>\<cdot\><text|m><rsup|-3>|)><around|(|<Rsix>|)><around|(|298.152<K>|)>>
    <vs><math|<phantom|p<B>>=89232.5<Pa>>
    <vs><math|p=p<A>+p<B>=3167.13<Pa>+89232.5<Pa>=92399.6<Pa>>
    <vs><math|y<B>=<dfrac|p<B>|p>=0.965724>>\ 

    \;

    <problempartAns>Use your approximate values of <math|p> and <math|y<B>>
    from part (a) to calculate <math|\<phi\><A>>, the fugacity coefficient of
    A in the gas mixture.

    <answer|<math|\<phi\><A>=0.995801>>

    <soln| From Eq. <reference|B(A)'=>: <math|B<A><rprime|'>=B<subs|A*A>+<around|(|-B<subs|A*A>+2*B<subs|A*B>-B<subs|B*B>|)>*y<B><rsup|2>=-112.9<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>> <vs>From Eq.
    <reference|ln(phi_i)=Bi'p/RT>: <math|\<phi\><A>=exp
    <around|(|B<A><rprime|'>p/R*T|)>=0.995801>>\ 

    \;

    <problempartAns>Evaluate the fugacity <math|<fug><A>> of the
    <math|<chem>H<rsub|2>*O> in the gas phase. Assume <math|p>, <math|y<B>>,
    and <math|\<phi\><A>> have the values you calculated in parts (a) and
    (b). Hint: start with the value of the saturation vapor pressure of pure
    water.

    <answer|<math|<fug><A>=3164.47<Pa>>>

    <soln| From Eq. <reference|B(A)'=>, with <math|y<B>> set equal to 1:
    <math|B<A><rprime|'>=B<subs|A*A>=-1152<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>> <vs>From Eq.
    <reference|ln(phi_i)=Bi'p/RT>, with <math|p> set equal to
    <math|p<A><rsup|\<ast\>>>: <math|\<phi\><A><rsup|\<ast\>><around|(|p<A><rsup|\<ast\>>|)>=exp
    <around|(|B<A><rprime|'>p<A><rsup|\<ast\>>/R*T|)>=0.998529>
    <vs><math|<D><fug><A><rsup|\<ast\>><around|(|p<A><rsup|\<ast\>>|)>=\<phi\><A><rsup|\<ast\>><around|(|p<A><rsup|\<ast\>>|)>*p<A><rsup|\<ast\>>=<around|(|0.998529|)><around|(|3167.13<Pa>|)>=3162.47<Pa>>
    <vs>From Eq. <reference|f_i(p2) approx>:
    <vs><math|<D><fug><A><rsup|\<ast\>><around|(|p|)>=<fug><A><rsup|\<ast\>><around|(|p<A><rsup|\<ast\>>|)>*exp
    <around*|[|<frac|V<A><rsup|\<ast\>><around|(|p-p<A><rsup|\<ast\>>|)>|R*T>|]>>
    <vs><math|<D><phantom|<fug><A><rsup|\<ast\>><around|(|p|)>>=<around|(|3162.47<Pa>|)>*exp
    <around*|[|<frac|<around|(|18.069<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>|)>*<around|(|92399.6<Pa>-3167.13<Pa>|)>|<around|(|<Rsix>|)><around|(|298.152<K>|)>>|]>>
    <vs><math|<phantom|<fug><A><rsup|\<ast\>><around|(|p|)>>=3164.53<Pa>>
    <vs>From Raoult's law for fugacity: <vs><math|<fug><A><around|(|p|)>=<around|(|1-x<B>|)><fug><A><rsup|\<ast\>><around|(|p|)>=<around|(|0.999980|)><around|(|3164.53<Pa>|)>=3164.47<Pa>>>\ 

    \;

    <problempartAns>Use your most recently calculated values of <math|p>,
    <math|\<phi\><A>>, and <math|<fug><A>> to calculate an improved value of
    <math|y<B>>.

    <answer|<math|y<B>=0.965608>>

    <soln| <math|p<A>=<fug><A>/\<phi\><A>=<around|(|3164.47<Pa>|)>/<around|(|0.995801|)>=3177.81<Pa>>
    <vs><math|y<B>=1-y<A>=1-p<A>/p=1-<around|(|3177.81<Pa>|)>/<around|(|92399.6<Pa>|)>=0.965608>>\ 

    \;

    <problempartAns>Use your current values of <math|p> and <math|y<B>> to
    evaluate the compression factor <math|Z> of the gas mixture, taking
    nonideality into account.

    <answer|<math|Z=0.999319>>

    <soln| <math|y<A>=1-y<B>=1-0.965608=0.034392> <vs>From Eq.
    <reference|B=yA^2 B(AA)+...>: <vs><math|B=y<A><rsup|2>B<subs|A*A>+2*y<A>y<B>B<subs|A*B>+y<B><rsup|2>B<subs|B*B>>
    <vs><math|<phantom|B>=<around|(|0.034392|)><rsup|2>*B<subs|A*A>+2<around|(|0.034392|)><around|(|0.965608|)>*B<subs|A*B>+<around|(|0.965608|)><rsup|2>*B<subs|B*B>>
    <vs><math|<phantom|B>=-18.2608<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>> <vs>From Eq.
    <reference|Z=(1+Bp/RT)>: <vs><math|<D>Z=1+<frac|B*p|R*T>=1+<frac|<around|(|-18.2608<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>|)><around|(|92399.6<Pa>|)>|<around|(|<Rsix>|)><around|(|298.152<K>|)>>=0.999319>>

    \;

    <problempartAns>Derive a general expression for <math|p> as a function of
    <math|<around|(|n<B><rsup|<text|g>>/V<rsup|<text|g>>|)>>, <math|T>,
    <math|y<B>>, and <math|Z>. Use this expression to calculate an improved
    value of <math|p>.

    <answer|<math|p=92347.7<Pa>>>

    <soln| <math|<D>Z<defn><frac|p*V<rsup|<text|g>>|n<rsup|<text|g>>R*T>*<space|2em>n<rsup|<text|g>>=n<B><rsup|<text|g>>/y<B>>
    <vs><math|<D>p=<frac|n<rsup|<text|g>>R*T*Z|V<rsup|<text|g>>>=<around*|(|<frac|n<B><rsup|<text|g>>|V<rsup|<text|g>>>|)><frac|R*T*Z|y<B>>>
    <vs><math|<D><phantom|p>=<around|(|35.9957
    <text|mol>\<cdot\><text|m><rsup|-3>|)><frac|<around|(|<Rsix>|)><around|(|298.152<K>|)><around|(|0.999319|)>|0.965608>>
    <vs><math|<phantom|p>=92347.7<Pa>>>

    \;

    <problempartAns>Finally, use the improved values of <math|p> and
    <math|y<B>> to evaluate the Henry's law constant <math|<kHB>> at the
    experimental <math|T> and <math|p>.

    <answer|<math|<kHB>=4.40890<timesten|9><Pa>>>

    <soln| From Eq. <reference|B(B)'=>: <vs><math|B<B><rprime|'>=B<subs|B*B>+<around|(|-B<subs|A*A>+2*B<subs|A*B>-B<subs|B*B>|)>*<around|(|1-y<B>|)><rsup|2>=-14.88<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>> <vs>From Eq.
    <reference|ln(phi_i)=Bi'p/RT>: <vs><math|<D>\<phi\><B>=exp
    <around|(|B<B><rprime|'>p/R*T|)>=exp <around*|[|<frac|<around|(|-14.88<timesten|-6>
    <text|m><rsup|3>\<cdot\><text|mol><rsup|-1>|)><around|(|92347.7<Pa>|)>|<around|(|<Rsix>|)><around|(|298.152<K>|)>>|]>=0.999446>
    <vs><math|<D><kHB>=<frac|<fug><B>|x<B>>=<frac|\<phi\><B>y<B>p|x<B>>=<frac|<around|(|0.999446|)><around|(|0.965608|)><around|(|92347.7<Pa>|)>|2.02142<timesten|-5>>>
    <vs><math|<phantom|<kHB>>=4.40890<timesten|9><Pa>>>
  </problemparts>

  <problem><label|prb:12-water-CH4>The method described in Prob.
  12.<reference|prb:12-gas sol> has been used to obtain high-precision values
  of the Henry's law constant, <math|<kHB>>, for gaseous methane dissolved in
  water.<footnote|Ref. <cite|rettich-81>.> Table <reference|tbl:12-water-CH4>

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|5|5|cell-col-span|1>|<cwith|1|1|5|5|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|7|7|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|1/<around|(|T/<text|K>|)>>>|<cell|<ccol|<math|ln
    <around|(|<kHB>/p<st>|)>>>>|<cell|<space|2em>>|<cell|<ccol|<math|1/<around|(|T/<text|K>|)>>>>|<cell|<math|ln
    <around|(|<kHB>/p<st>|)>>>>|<row|<cell|0.00363029>|<cell|10.0569>|<cell|>|<cell|0.00329870>|<cell|10.6738>>|<row|<cell|0.00359531>|<cell|10.1361>|<cell|>|<cell|0.00319326>|<cell|10.8141>>|<row|<cell|0.00352175>|<cell|10.2895>|<cell|>|<cell|0.00314307>|<cell|10.8673>>|<row|<cell|0.00347041>|<cell|10.3883>|<cell|>|<cell|0.00309444>|<cell|10.9142>>|<row|<cell|0.00341111>|<cell|10.4951>|<cell|>|<cell|0.00304739>|<cell|10.9564>>|<row|<cell|0.00335390>|<cell|10.5906>|<cell|>|<cell|>|<cell|>>>>>
  </big-table|<label|tbl:12-water-CH4>Data for Prob.
  12.<reference|prb:12-water-CH4>>

  lists values of <math|ln <around|(|<kHB>/p<st>|)>> at eleven temperatures
  in the range <math|275<K>>\U<math|328<K>> and at pressures close to
  <math|1<br>>. Use these data to evaluate
  <math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>> and
  <math|\<Delta\><rsub|<text|sol>,<text|B>>*C<st><rsub|p>> at
  <math|T=298.15<K>>. This can be done by a graphical method. Better
  precision will be obtained by making a least-squares fit of the data to the
  three-term polynomial

  <\equation*>
    ln <around|(|<kHB>/p<st>|)>=a+b*<around|(|1/T|)>+c*<around|(|1/T|)><rsup|2>
  </equation*>

  and using the values of the coefficients <math|a>, <math|b>, and <math|c>
  for the evaluations.

  <\soln>
    \ For the <em|graphical method>, make a plot of <math|ln <kHB>> versus
    <math|1/T> as in Fig. <reference|fig:12-water-CH4>.

    <\big-figure>
      <boxedfigure|<image|./12-SUP/water-CH4.eps||||> <capt|Plot for Prob.
      12.<reference|prb:12-water-CH4>. Open circles: data points from Table
      12.<reference|tbl:12-water-CH4>. Dashed line: tangent to curve at
      <math|1/T=1/298.15<K>>.<label|fig:12-water-CH4>>>
    </big-figure|>

    The tangent to the curve at <math|1/T=1/298.15<K>> (dashed line) has a
    slope of <math|-1.58<timesten|3><K>>. <vs>From Eq.
    <reference|dln(k/po)/d(1/T)=>: <vs><math|<D>\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=R*<frac|<dif>ln
    <around|(|<kHB>/p<st>|)>|<dif><around|(|1/T|)>>=<around|(|<R>|)>*<around|(|-1.58<timesten|3><K>|)>=-13.1
    <text|kJ>\<cdot\><text|mol><rsup|-1>> <vs>From Eq.
    <reference|dDel(r)H^o/dT=>: <vs><math|\<Delta\><rsub|<text|sol>,<text|B>>*C<rsub|p><st>=<dif>\<Delta\><rsub|<text|sol>,<text|B>>*H<st>/<dif>T>
    <vs>From the slope between the first two points on the graph:
    <vs><math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=<around|(|-897<K>|)>*R>
    at <math|T\<approx\>325.6<K>> <vs>From the slope between the last two
    points on the graph: <vs><math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=<around|(|-2264<K>|)>*R>
    at <math|T\<approx\>276.8<K>> <vs><math|<D>\<Delta\><rsub|<text|sol>,<text|B>>*C<rsub|p><st>\<approx\><frac|<around|(|-897<K>+2264<K>|)><around|(|<R>|)>|325.6<K>-276.8<K>>=233<units|J*K<per>m*o*l<per>>>
    <vs>Using a <em|least-squares fit> to <math|ln
    <around|(|<kHB>/p<st>|)>=a+b*<around|(|1/T|)>+c*<around|(|1/T|)><rsup|2>>:
    <vs><math|a=1.6755*<space|2em>b=6896.86<K><space|2em>c=-1.263836<timesten|6><units|K<rsup|<math|2>>>>
    <vs><math|<D>\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=R*<frac|<dif>ln
    <around|(|<kHB>/p<st>|)>|<dif><around|(|1/T|)>>=R*<around|[|b+2*c*<around|(|1/T|)>|]>=-13.145
    <text|kJ>\<cdot\><text|mol><rsup|-1>>
    <vs><math|\<Delta\><rsub|<text|sol>,<text|B>>*C<rsub|p><st>=<dif>\<Delta\><rsub|<text|sol>,<text|B>>*H<st>/<dif>T=-2*R*c*<around|(|1/T|)><rsup|2>=236.42<units|J*K<per>m*o*l<per>>>
  </soln>

  <problem>Liquid water and liquid benzene have very small mutual
  solubilities. Equilibria in the binary water\Ubenzene system were
  investigated by Tucker, Lane, and Christian<footnote|Ref.
  <cite|tucker-81>.> as follows. A known amount of distilled water was
  admitted to an evacuated, thermostatted vessel. Part of the water vaporized
  to form a vapor phase. Small, precisely measured volumes of liquid benzene
  were then added incrementally from the sample loop of a
  liquid-chromatography valve. The benzene distributed itself between the
  liquid and gaseous phases in the vessel. After each addition, the pressure
  was read with a precision pressure gauge. From the known amounts of water
  and benzene and the total pressure, the liquid composition and the partial
  pressure of the benzene were calculated. The fugacity of the benzene in the
  vapor phase was calculated from its partial pressure and the second virial
  coefficient.

  At a fixed temperature, for mole fractions <math|x<B>> of benzene in the
  liquid phase up to about <math|3<timesten|-4>> (less than the solubility of
  benzene in water), the fugacity of the benzene in the equilibrated gas
  phase was found to have the following dependence on <math|x<B>>:

  <\equation*>
    <frac|<fug><B>|x<B>>=<kHB>-A*x<B>
  </equation*>

  Here <math|<kHB>> is the Henry's law constant and <math|A> is a constant
  related to deviations from Henry's law. At <math|30 <degC>>, the measured
  values were <math|<kHB>=385.5<br>> and <math|A=2.24<timesten|4><br>>.

  <\problemparts>
    \ <problempartAns>Treat benzene (B) as the solute and find its activity
    coefficient on a mole fraction basis, <math|<g><xbB>>, at <math|30
    <degC>> in the solution of composition <math|x<B>=3.00<timesten|-4>>.

    <answer|<math|<g><xbB>=0.9826>>

    <soln| From Table <reference|tbl:9-activities>, with the pressure factor
    of unity: <vs><math|<D>a<xbB>=<g><xbB>x<B>=<frac|<fug><B>|<kHB>>>
    <vs><math|<D><g><xbB>=<frac|<fug><B>|<kHB>x<B>>=1-<frac|A*x<B>|<kHB>>=1-<frac|<around|(|2.24<timesten|4><br>|)><around|(|3.00<timesten|-4>|)>|385.5<br>>=0.9826>>\ 

    \;

    <problempartAns>The fugacity of benzene vapor in equilibrium with pure
    liquid benzene at <math|30 <degC>> is
    <math|<fug><B><rsup|\<ast\>>=0.1576<br>>. Estimate the mole fraction
    solubility of liquid benzene in water at this temperature.

    <answer|<math|x<B>=4.19<timesten|-4>>>

    <soln| Assume that the fugacity of benzene vapor in equilibrium with a
    saturated aqueous solution of benzene is <math|0.1576<br>>, and solve the
    equation <math|<fug><B>/x<B>=<kHB>-A*x<B>> for <math|x<B>>:
    <vs><math|A*x<B><rsup|2>-<kHB>x<B>+<fug><B>=0>
    <vs><math|<D>x<B>=<frac|<kHB>\<pm\><sqrt|<kHB><rsup|2>-4*A<fug><B>>|2*A>=1.68<timesten|-2><space|1em><text|or><space|1em>4.19<timesten|-4>>
    <vs>As <math|x<B>> is increased in the unsaturated solution the
    saturation condition is reached at the lower of the two values:
    <math|x<B>=4.19<timesten|-4>>.>\ 

    \;

    <problempart>The calculation of <math|<g><xbB>> in part (a) treated the
    benzene as a single solute species with deviations from infinite-dilution
    behavior. Tucker et al suggested a dimerization model to explain the
    observed negative deviations from Henry's law. (Classical thermodynamics,
    of course, cannot prove such a molecular interpretation of observed
    macroscopic behavior.) The model assumes that there are two solute
    species, a monomer (M) and a dimer (D), in reaction equilibrium:
    <math|<chem>2*M<arrows>D>. Let <math|n<B>> be the total amount of
    C<rsub|<math|6>>H<rsub|<math|6>> present in solution, and define the mole
    fractions

    <\equation*>
      x<B><defn><frac|n<B>|n<A>+n<B>>\<approx\><frac|n<B>|n<A>>
    </equation*>

    <\equation*>
      x<rsub|<text|M>><defn><frac|n<rsub|<text|M>>|n<A>+n<rsub|<text|M>>+n<rsub|<text|D>>>\<approx\><frac|n<rsub|<text|M>>|n<A>>*<space|2em>x<rsub|<text|D>><defn><frac|n<rsub|<text|D>>|n<A>+n<rsub|<text|M>>+n<rsub|<text|D>>>\<approx\><frac|n<rsub|<text|D>>|n<A>>
    </equation*>

    where the approximations are for dilute solution. In the model, the
    individual monomer and dimer particles behave as solutes in an
    ideal-dilute solution, with activity coefficients of unity. The monomer
    is in transfer equilibrium with the gas phase:
    <math|x<rsub|<text|M>>=<fug><B>/<kHB>>. The equilibrium constant
    expression (using a mole fraction basis for the solute standard states
    and setting pressure factors equal to 1) is
    <math|K=x<rsub|<text|D>>/x<rsub|<text|M>><rsup|2>>. From the relation
    <math|n<B>=n<rsub|<text|M>>+2*n<rsub|<text|D>>>, and because the solution
    is very dilute, the expression becomes

    <\equation*>
      K=<frac|x<B>-x<rsub|<text|M>>|2*x<rsub|<text|M>><rsup|2>>
    </equation*>

    Make individual calculations of <math|K> from the values of
    <math|<fug><B>> measured at <math|x<B>=1.00<timesten|-4>>,
    <math|x<B>=2.00<timesten|-4>>, and <math|x<B>=3.00<timesten|-4>>.
    Extrapolate the calculated values of <math|K> to <math|x<B|=>0> in order
    to eliminate nonideal effects such as higher aggregates. Finally, find
    the fraction of the benzene molecules present in the dimer form at
    <math|x<B>=3.00<timesten|-4>> if this model is correct. <soln| Use the
    formulas <vs><math|<D>x<rsub|<text|M>>=<frac|<fug><B>|<kHB>>=x<B>-<frac|A*x<B><rsup|2>|<kHB>><space|1em><text|and><space|1em>K=<frac|x<B>-x<rsub|<text|M>>|2*x<rsub|<text|M>><rsup|2>>>
    <vs>The results are <math|K=29.4> at <math|x<B>=1.00<timesten|-4>>,
    <math|K=29.7> at <math|x<B>=2.00<timesten|-4>>, and <math|K=30.1> at
    <math|x<B>=3.00<timesten|-4>>. The extrapolated value is
    <math|K\<approx\>29.1>. At <math|x<B>=3.00<timesten|-4>>, the fraction of
    benzene molecules in the dimer form is
    <vs><math|<D><frac|2*n<rsub|<text|D>>|n<B>>=1-<frac|n<rsub|<text|M>>|n<B>>\<approx\>1-<frac|x<rsub|<text|M>>|x<B>>=1-<frac|2.95<timesten|-4>|3.00<timesten|-4>>=0.017>>
  </problemparts>

  <problemAns>Use data in Appendix <reference|app:props> to evaluate the
  thermodynamic equilibrium constant at <math|298.15<K>> for the limestone
  reaction

  <\equation*>
    <chem>C*a*C*O<rsub|3><around|(|c*r,c*a*l*c*i*t*e|)>+C*O<rsub|2><around|(|g|)>+H<rsub|2>*O<around|(|l|)><arrow>C*a<rsup|2+>*<around|(|a*q|)>+2*H*C*O<rsub|3><rsup|-><around|(|a*q|)>
  </equation*>

  <\answer>
    <math|K=1.2<timesten|-6>>
  </answer>

  <\soln>
    \ <math|\<Delta\><rsub|<text|r>>*G<st>/<text|kJm*o*l<per>>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>\<Delta\><rsub|<text|f>>*G<st><around|(|i|)>/<text|kJm*o*l<per>>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*G<st>/<text|kJm*o*l<per>>>=-<around|(|-1128.8|)>-<around|(|-394.41|)>-<around|(|-237.16|)>+<around|(|-552.8|)>+2*<around|(|-586.90|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*G<st>/<text|kJm*o*l<per>>>=33.8>
    <vs><math|<D>K=exp <around*|(|-<frac|\<Delta\><rsub|<text|r>>*G<st>|R*T>|)>=exp
    <around*|[|-<frac|33.8<timesten|3> <text|J>\<cdot\><text|mol><rsup|-1>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>=1.2<timesten|-6>>
  </soln>

  <problem><label|prb:12-formic acid> For the dissociation equilibrium of
  formic acid, <math|<chem>H*C*O<rsub|2> H*<around|(|a*q|)><arrows>H<rsup|+>*<around|(|a*q|)>+H*C*O<rsub|2><rsup|-><around|(|a*q|)>>,
  the acid dissociation constant at <math|298.15<K>> has the value
  <math|K<rsub|<text|a>>=1.77<timesten|-4>>.

  <\problemparts>
    \ <problempartAns>Use Eq. <reference|Ka> to find the degree of
    dissociation and the hydrogen ion molality in a 0.01000 molal formic acid
    solution. You can safely set <math|<G><rsub|<text|r>>> and
    <math|<g><subs|<math|m>,HA>> equal to <math|1>, and use the Debye--H�ckel
    limiting law (Eq. <reference|DH limiting law>) to calculate
    <math|<g><rsub|\<pm\>>>. You can do this calculation by iteration: Start
    with an initial estimate of the ionic strength (in this case 0),
    calculate <math|<g><rsub|\<pm\>>> and <math|\<alpha\>>, and repeat these
    steps until the value of <math|\<alpha\>> no longer changes.

    <answer|<math|\<alpha\>=0.129><next-line><math|m<rsub|+>=1.29<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>>>

    <soln| Equation <reference|Ka>: <vs><math|<D>K<rsub|<text|a>>=<G><rsub|<text|r>><frac|<g><rsub|\<pm\>><rsup|2>\<alpha\><rsup|2>*m<B>/m<st>|<g><subs|<math|m>,HA><around|(|1-\<alpha\>|)>>\<approx\><frac|<g><rsub|\<pm\>><rsup|2>\<alpha\><rsup|2>*m<B>/m<st>|1-\<alpha\>>>
    <vs>Solve for <math|\<alpha\>>: <vs><math|<D>\<alpha\>=<frac|-K<rsub|<text|a>>+<around*|[|K<rsub|<text|a>><rsup|2>+4<g><rsub|\<pm\>><rsup|2><around|(|m<B>/m<st>|)>*K<rsub|<text|a>>|]><rsup|1/2>|2<g><rsub|\<pm\>><rsup|2><around|(|m<B>/m<st>|)>>>
    <vs>Calculate <math|<g><rsub|\<pm\>>> from Eq. <reference|DH limiting
    law>: <vs><math|ln <g><rsub|\<pm\>>=-A*<around*|\||z<rsub|+>*z<rsub|->|\|><sqrt|I<rsub|m>>=-1.1744*<around|(|\<alpha\>*m<B>/m<st>|)><rsup|1/2>>
    <vs>First estimate (<math|I<rsub|m>=0,<g><rsub|\<pm\>>=1>):
    <vs><math|<D>\<alpha\>=<frac|-1.77<timesten|-4>+<around*|[|<around|(|1.77<timesten|-4>|)><rsup|2>+4<around|(|0.01000|)><around|(|1.77<timesten|-4>|)>|]><rsup|1/2>|2<around|(|0.01000|)>>=0.124>
    <vs><math|ln <g><rsub|\<pm\>>=-<around|(|1.1744|)><around|(|0.00124|)><rsup|1/2>=-0.0414<space|2em><g><rsub|\<pm\>>=0.9594>
    <vs>Second estimate: <vs><math|<D>\<alpha\>=<frac|-1.77<timesten|-4>+<around*|[|<around|(|1.77<timesten|-4>|)><rsup|2>+4<around|(|0.9594|)><rsup|2><around|(|0.01000|)><around|(|1.77<timesten|-4>|)>|]><rsup|1/2>|2<around|(|0.9594|)><rsup|2><around|(|0.01000|)>>>
    <vs><math|<phantom|\<alpha\>>=0.129> <vs><math|ln
    <g><rsub|\<pm\>>=-<around|(|1.1744|)><around|(|0.00129|)><rsup|1/2>=-0.0422<space|2em><g><rsub|\<pm\>>=0.9586>
    <vs>Third estimate: <vs><math|<D>\<alpha\>=<frac|-1.77<timesten|-4>+<around*|[|<around|(|1.77<timesten|-4>|)><rsup|2>+4<around|(|0.9586|)><rsup|2><around|(|0.01000|)><around|(|1.77<timesten|-4>|)>|]><rsup|1/2>|2<around|(|0.9586|)><rsup|2><around|(|0.01000|)>>>
    <vs><math|<phantom|\<alpha\>>=0.129> <vs>The calculated degree of
    dissociation has become constant at <math|\<alpha\>=0.129>; the hydrogen
    ion molality is <math|m<rsub|+>=\<alpha\>*m<B>=1.29<timesten|-3>
    <text|mol>\<cdot\><text|kg><rsup|-1>>.>\ 

    \;

    <problempartAns>Estimate the degree of dissociation of formic acid in a
    solution that is 0.01000 molal in both formic acid and sodium nitrate,
    again using the Debye--H�ckel limiting law for <math|<g><rsub|\<pm\>>>.
    Compare with the value in part (a).

    <answer|<math|\<alpha\>=0.140>>

    <soln| Use same formula for <math|\<alpha\>> as in part (a); calculate
    <math|<g><rsub|\<pm\>>> from <vs><math|ln
    <g><rsub|\<pm\>>=-A*<around*|\||z<rsub|+>*z<rsub|->|\|><sqrt|I<rsub|m>>=-1.1744*<around|(|\<alpha\>*m<B>/m<st>+0.01000|)><rsup|1/2>>
    <vs>First estimate: <vs><math|ln <g><rsub|\<pm\>>=-1.1744<around|(|0.01000|)><rsup|1/2>=-0.1174<space|2em><g><rsub|\<pm\>>=0.8892>
    <vs><math|<D>\<alpha\>=<frac|-1.77<timesten|-4>+<around*|[|<around|(|1.77<timesten|-4>|)><rsup|2>+4<around|(|0.8892|)><rsup|2><around|(|0.01000|)><around|(|1.77<timesten|-4>|)>|]><rsup|1/2>|2<around|(|0.8892|)><rsup|2><around|(|0.01000|)>>>
    <vs><math|<phantom|\<alpha\>>=0.139> <vs><math|ln
    <g><rsub|\<pm\>>=-<around|(|1.1744|)>*<around|(|0.00139+0.01000|)><rsup|1/2>=-0.1253<space|2em><g><rsub|\<pm\>>=0.8822>
    <vs>Second estimate: <vs><math|<D>\<alpha\>=<frac|-1.77<timesten|-4>+<around*|[|<around|(|1.77<timesten|-4>|)><rsup|2>+4<around|(|0.8822|)><rsup|2><around|(|0.01000|)><around|(|1.77<timesten|-4>|)>|]><rsup|1/2>|2<around|(|0.8822|)><rsup|2><around|(|0.01000|)>>>
    <vs><math|<phantom|\<alpha\>>=0.140> <vs><math|ln
    <g><rsub|\<pm\>>=-<around|(|1.1744|)>*<around|(|0.00140+0.01000|)><rsup|1/2>=-0.1254<space|2em><g><rsub|\<pm\>>=0.8822>
    <vs>This value of <math|<g><rsub|\<pm\>>> will give the same value of the
    degree of dissociation as before: <math|\<alpha\>=0.140>. The increased
    ionic strength causes the degree of dissociation to increase.>
  </problemparts>

  <problemAns><label|prb:12-Cl ion>Use the following experimental information
  to evaluate the standard molar enthalpy of formation and the standard molar
  entropy of the aqueous chloride ion at <math|298.15<K>>, based on the
  conventions <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|H<rsup|<math|+>>,aq>|)>=0>
  and <math|S<m><st><around|(|<text|H<rsup|<math|+>>,aq>|)>=0> (Secs.
  <reference|11-st molar enthalpy of formation> and <reference|11-eval of
  K>). (Your calculated values will be close to, but not exactly the same as,
  those listed in Appendix <reference|app:props>, which are based on the same
  data combined with data of other workers.)

  <\itemize>
    <item>For the reaction <math|<chem><frac|1|2>*H<rsub|2><around|(|g|)>+<frac|1|2>*C*l<rsub|2><around|(|g|)><arrow>H*C*l<around|(|g|)>>,
    the standard molar enthalpy of reaction at <math|298.15<K>> measured in a
    flow calorimeter<footnote|Ref. <cite|rossini-32>.> is
    <math|\<Delta\><rsub|<text|r>>*H<st>=-92.312
    <text|kJ>\<cdot\><text|mol><rsup|-1>>.

    <item>The standard molar entropy of gaseous HCl at <math|298.15<K>>
    calculated from spectroscopic data is
    <math|S<m><st>=186.902<units|J*K<per>m*o*l<per>>>.

    <item>From five calorimetric runs,<footnote|Ref. <cite|gunn-63>.> the
    average experimental value of the standard molar enthalpy of solution of
    gaseous HCl at <math|298.15<K>> is <math|\<Delta\><rsub|<text|sol>,<text|B>>*H<st>=-74.84
    <text|kJ>\<cdot\><text|mol><rsup|-1>>.

    <item>From vapor pressure measurements of concentrated aqueous HCl
    solutions,<footnote|Ref. <cite|randall-28>.> the value of the ratio
    <math|<fug><B>/a<mbB>> for gaseous HCl in equilibrium with aqueous HCl at
    <math|298.15<K>> is <math|5.032<timesten|-7><br>>.
  </itemize>

  <\answer>
    <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|Cl<rsup|<math|->>,aq>|)>=-167.15
    <text|kJ>\<cdot\><text|mol><rsup|-1>><next-line><math|S<m><st><around|(|<text|Cl<rsup|<math|->>,aq>|)>=56.46<units|J*K<per>m*o*l<per>>>
  </answer>

  <\soln>
    \ The sum of the reactions <math|<chem><frac|1|2>*H<rsub|2><around|(|g|)>+<frac|1|2>*C*l<rsub|2><around|(|g|)><arrow>H*C*l<around|(|g|)>>
    and <math|<chem>H*C*l<around|(|g|)><arrow>H<rsup|+>*<around|(|a*q|)>+C*l<rsup|->*<around|(|a*q|)>>
    is the net reaction <vs><math|<chem><frac|1|2>*H<rsub|2><around|(|g|)>+<frac|1|2>*C*l<rsub|2><around|(|g|)><arrow>H<rsup|+>*<around|(|a*q|)>+C*l<rsup|->*<around|(|a*q|)>>
    <vs>with standard molar reaction enthalpy equal to the sum of the
    corresponding standard molar enthalpy changes:
    <vs><math|\<Delta\><rsub|<text|r>>*H<st>=<around|(|-92.312-74.84|)>
    <text|kJ>\<cdot\><text|mol><rsup|-1>=-167.15
    <text|kJ>\<cdot\><text|mol><rsup|-1>> <vs>Since
    <math|\<Delta\><rsub|<text|r>>*H<st>> for the net reaction is equal to
    the sum of the standard molar enthalpies of formation of
    H<rsup|<math|+>>(aq) and Cl<rsup|<math|->>(aq), and
    <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|H<rsup|<math|+>>,aq>|)>>
    is zero, we have <vs><math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|Cl<rsup|<math|->>,aq>|)>=-167.15
    <text|kJ>\<cdot\><text|mol><rsup|-1>> <vs>For the dissolution reaction
    <vs><math|<chem>H*C*l<around|(|g|)><arrow>H<rsup|+>*<around|(|a*q|)>+C*l<rsup|->*<around|(|a*q|)>>
    <vs>the equilibrium constant is given by
    <math|K=a<mbB>/<around|(|<fug><B>/p<st>|)>>, and the standard molar
    reaction Gibbs energy is <vs><math|<D>\<Delta\><rsub|<text|r>>*G<st>=-R*T*ln
    K=-<around|(|<R>|)><around|(|298.15<K>|)>*ln
    <around*|(|<frac|1|5.032<timesten|-7>>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*G<st>>=3.595<timesten|4>
    <text|J>\<cdot\><text|mol><rsup|-1>> <vs>The standard molar reaction
    entropy is then <vs><math|<D>\<Delta\><rsub|<text|r>>*S<st>=<frac|\<Delta\><rsub|<text|r>>*H<st>-\<Delta\><rsub|<text|r>>*G<st>|T>=<frac|<around|(|-74.84<timesten|3>+3.595<timesten|4>|)>
    <text|J>\<cdot\><text|mol><rsup|-1>|298.15<K>>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*S<st>>=-130.44<units|J*K<per>m*o*l<per>>>
    <vs>Use Eq. <reference|del(r)Smo=sum(nu_i)Smio><vpageref|del(r)Smo=sum(nu<rsub|i>)Smio>:
    <vs><math|\<Delta\><rsub|<text|r>>*S<st>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*S<rsub|i><st>=-S<m><st><text|<around|(|HCl,g|)>>+S<m><st><around|(|<text|H<rsup|<math|+>>,aq>|)>+S<m><st><around|(|<text|Cl<rsup|<math|->>,aq>|)>>
    <vs>Since <math|S<m><st><around|(|<text|H<rsup|<math|+>>,aq>|)>> is zero,
    we have <vs><math|S<m><st><around|(|<text|Cl<rsup|<math|->>,aq>|)>=\<Delta\><rsub|<text|r>>*S<st>+S<m><st><text|<around|(|HCl,g|)>>=<around|(|-130.44+186.902|)><units|J*K<per>m*o*l<per>>>
    <vs><math|<phantom|S<m><st><around|(|<text|Cl<rsup|<math|->>,aq>|)>>=56.46<units|J*K<per>m*o*l<per>>>
  </soln>

  <problem><label|prb:12-Ks(AgCl)>The solubility of crystalline AgCl in
  ultrapure water has been determined from the electrical conductivity of the
  saturated solution.<footnote|Ref. <cite|gledhill-52>.> The average of five
  measurements at <math|298.15<K>> is <math|s<B>=1.337<timesten|-5>
  <text|mol>\<cdot\><text|dm><rsup|-3>>. The density of water at this
  temperature is <math|\<rho\><A><rsup|\<ast\>>=0.9970<units|k*g*d*m<rsup|<math|-3>>>>.

  <\problemparts>
    \ <problempartAns>From these data and the Debye--H�ckel limiting law,
    calculate the solubility product <math|K<subs|s>> of AgCl at
    <math|298.15<K>>.

    <answer|<math|K<subs|s>=1.783<timesten|-10>>>

    <soln| From Eq. <reference|nB/nA (dilute)> for a dilute solution:
    <vs><math|<D>m<B>=<frac|c<B>|\<rho\><A><rsup|\<ast\>>>=<frac|1.337<timesten|-5>
    <text|mol>\<cdot\><text|dm><rsup|-3>|0.9970<units|k*g*d*m<rsup|<math|-3>>>>=1.341<timesten|-5>
    <text|mol>\<cdot\><text|kg><rsup|-1>> <vs>Find the mean ionic activity
    coefficient from the Debye--H�ckel limiting law, Eq. <reference|DH
    limiting law>: <vs><math|ln <g><rsub|\<pm\>>=-A*<sqrt|I<rsub|m>>=-<around|(|1.1744<units|k*g<rsup|<math|1/2>>*mol<rsup|<math|-1/2>>>|)><around|(|1.341<timesten|-5>
    <text|mol>\<cdot\><text|kg><rsup|-1>|)><rsup|1/2>><no-page-break><vs><math|<phantom|ln
    <g><rsub|\<pm\>>>=-4.301<timesten|-3>> <vs><math|<g><rsub|\<pm\>>=0.996>
    <vs>Use Eq. <reference|Ks, no common ion> with
    <math|<G><rsub|<text|r>>=1>: <vs><math|K<subs|s>=<g><rsub|\<pm\>><rsup|2><around|(|m<B>/m<st>|)><rsup|2>=<around|(|0.996|)><rsup|2><around|(|1.341<timesten|-5>|)><rsup|2>=1.783<timesten|-10>>>\ 

    \;

    <problempart>Evaluate the standard molar Gibbs energy of formation of
    aqueous Ag<rsup|<math|+>> ion at <math|298.15<K>>, using the results of
    part (a) and the values <math|\<Delta\><rsub|<text|f>>*G<st><around|(|<text|Cl<rsup|<math|->>,aq>|)>=-131.22
    <text|kJ>\<cdot\><text|mol><rsup|-1>> and
    <math|\<Delta\><rsub|<text|f>>*G<st><around|(|<text|AgCl,s>|)>=-109.77
    <text|kJ>\<cdot\><text|mol><rsup|-1>> from Appendix
    <reference|app:props>.

    <soln| Calculate <math|\<Delta\><rsub|<text|sol>>*G<st>> for the
    dissolution reaction <math|<chem>A*g*C*l<around|(|s|)><arrows>A*g<rsup|+>*<around|(|a*q|)>+C*l<rsup|->*<around|(|a*q|)>>:
    <vs><math|\<Delta\><rsub|<text|sol>>*G<st>=-R*T*ln
    K<subs|s>=-<around|(|<R>|)><around|(|298.15<K>|)>*ln
    <around|(|1.783<timesten|-10>|)>> <vs><math|<phantom|\<Delta\><rsub|<text|sol>>*G<st>>=5.5647<timesten|4>
    <text|J>\<cdot\><text|mol><rsup|-1>> <vs>Apply the general relation
    <math|\<Delta\><rsub|<text|r>>*G<st>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>\<Delta\><rsub|<text|f>>*G<st><around|(|i|)>>
    to the dissolution reaction: <vs><math|\<Delta\><rsub|<text|sol>>*G<st>=-\<Delta\><rsub|<text|f>>*G<st><around|(|<text|AgCl,s>|)>+\<Delta\><rsub|<text|f>>*G<st><around|(|<text|Ag<rsup|<math|+>>,aq>|)>+\<Delta\><rsub|<text|f>>*G<st><around|(|<text|Cl<rsup|<math|->>,aq>|)>>
    <vs>Rearrange to <vs><math|\<Delta\><rsub|<text|f>>*G<st><around|(|<text|Ag<rsup|<math|+>>,aq>|)>=\<Delta\><rsub|<text|sol>>*G<st>+\<Delta\><rsub|<text|f>>*G<st><around|(|<text|AgCl,s>|)>-\<Delta\><rsub|<text|f>>*G<st><around|(|<text|Cl<rsup|<math|->>,aq>|)>>
    <vs><math|\<Delta\><rsub|<text|f>>*G<st><around|(|<text|Ag<rsup|<math|+>>,aq>|)>/<text|kJm*o*l<per>>=55.647+<around|(|-109.77|)>-<around|(|-131.22|)>=77.10>>
  </problemparts>

  <problem><label|prb:12-AgCl pptn>The following reaction was carried out in
  an adiabatic solution calorimeter by Wagman and Kilday:<footnote|Ref.
  <cite|wagman-73>.>

  <\equation*>
    <text|AgNO<rsub|<math|3>><around|(|s|)>>+<text|KCl<around|(|aq,<math|m<B>=0.101
    <text|mol>\<cdot\><text|kg><rsup|-1>>|)>><arrow><text|AgCl<around|(|s|)>>+<text|KNO<rsub|<math|3>><around|(|aq|)>>
  </equation*>

  The reaction can be assumed to go to completion, and the amount of KCl was
  in slight excess, so the amount of AgCl formed was equal to the initial
  amount of AgNO<rsub|<math|3>>. After correction for the enthalpies of
  diluting the solutes in the initial and final solutions to infinite
  dilution, the standard molar reaction enthalpy at <math|298.15<K>> was
  found to be <math|\<Delta\><rsub|<text|r>>*H<st>=-43.042
  <text|kJ>\<cdot\><text|mol><rsup|-1>>. The same workers used solution
  calorimetry to obtain the molar enthalpy of solution at infinite dilution
  of crystalline AgNO<rsub|<math|3>> at <math|298.15<K>>:
  <math|\<Delta\><rsub|<text|sol>,<text|B>>*H<rsup|\<infty\>>=22.727
  <text|kJ>\<cdot\><text|mol><rsup|-1>>.

  <\problemparts>
    \ <problempartAns><label|AgCl delH(pptn)>Show that the difference of
    these two values is the standard molar reaction enthalpy for the
    precipitation reaction

    <\equation*>
      <chem>A*g<rsup|+>*<around|(|a*q|)>+C*l<rsup|->*<around|(|a*q|)><arrow>A*g*C*l<around|(|s|)>
    </equation*>

    and evaluate this quantity.

    <answer|<math|\<Delta\><rsub|<text|r>>*H<st>=-65.769
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>

    <soln| The reaction <math|<text|AgNO<rsub|<math|3>><around|(|s|)>>+<text|KCl<around|(|aq|)>><arrow><text|AgCl<around|(|s|)>>+<text|KNO<rsub|<math|3>><around|(|aq|)>>>
    is equivalent to <math|<text|AgNO<rsub|<math|3>><around|(|s|)>>+<text|Cl<rsup|<math|->><around|(|aq|)>><arrow><text|AgCl<around|(|s|)>>+<text|NO<math|<rsub|3><rsup|->><around|(|aq|)>>>.
    Subtracting the equation for the dissolution of AgNO<rsub|<math|3>>,
    <math|<text|AgNO<rsub|<math|3>><around|(|s|)>><arrow><text|Ag<rsup|<math|+>><around|(|aq|)>>+<text|Cl<rsup|<math|->><around|(|aq|)>>>,
    gives the equation for precipitation,
    <math|<text|Ag<rsup|<math|+>><around|(|aq|)>>+<text|Cl<rsup|<math|->><around|(|aq|)>><arrow><text|AgCl<around|(|s|)>>>.
    Thus, the standard molar reaction enthalpy for the precipitation reaction
    is the difference <vs><math|\<Delta\><rsub|<text|r>>*H<st>=<around|[|<around|(|-43.042|)>-<around|(|22.727|)>|]>
    <text|kJ>\<cdot\><text|mol><rsup|-1>=-65.769
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>\ 

    <problempartAns>Evaluate the standard molar enthalpy of formation of
    aqueous Ag<rsup|<math|+>> ion at <math|298.15<K>>, using the results of
    part (a) and the values <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|Cl<rsup|<math|->>,aq>|)>=-167.08
    <text|kJ>\<cdot\><text|mol><rsup|-1>> and
    <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<text|AgCl,s>|)>=-127.01
    <text|kJ>\<cdot\><text|mol><rsup|-1>> from Appendix
    <reference|app:props>. (These values come from calculations similar to
    those in Probs. 12.<reference|prb:12-Cl ion> and
    14.<reference|prb:14-AgCl formation>.) The calculated value will be close
    to, but not exactly the same as, the value listed in Appendix
    <reference|app:props>, which is based on the same data combined with data
    of other workers.

    <answer|<math|\<Delta\><rsub|<text|f>>*H<st><text|<around|(|Ag<rsup|<math|+>>,aq|)>>=105.84
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>

    <soln| Apply the relation <math|\<Delta\><rsub|<text|r>>*H<st>=<big|sum><rsub|i>\<Delta\><rsub|<text|f>>*H<st><around|(|i|)>>
    to the precipitation reaction: <vs><math|\<Delta\><rsub|<text|r>>*H<st>=-\<Delta\><rsub|<text|f>>*H<st><text|<around|(|Ag<rsup|<math|+>>,aq|)>>-\<Delta\><rsub|<text|f>>*H<st><text|<around|(|Cl<rsup|<math|->>,aq|)>>+\<Delta\><rsub|<text|f>>*H<st><text|<around|(|AgCl,s|)>>>
    <vs><math|\<Delta\><rsub|<text|f>>*H<st><text|<around|(|Ag<rsup|<math|+>>,aq|)>>=-\<Delta\><rsub|<text|r>>*H<st>-\<Delta\><rsub|<text|f>>*H<st><text|<around|(|Cl<rsup|<math|->>,aq|)>>+\<Delta\><rsub|<text|f>>*H<st><text|<around|(|AgCl,s|)>>>
    <vs><math|<phantom|\<Delta\><rsub|<text|f>>*H<st><text|<around|(|Ag<rsup|<math|+>>,aq|)>>>=<around|[|-<around|(|-65.769|)>-<around|(|-167.08|)>+<around|(|-127.01|)>|]>
    <text|kJ>\<cdot\><text|mol><rsup|-1>>
    <vs><math|<phantom|\<Delta\><rsub|<text|f>>*H<st><text|<around|(|Ag<rsup|<math|+>>,aq|)>>>=105.84
    <text|kJ>\<cdot\><text|mol><rsup|-1>>>
  </problemparts>

  \ 

  <\initial>
    <\collection>
      <associate|preamble|true>
    </collection>
  </initial>

  <\references>
    <\collection>
      <associate|footnote-1|<tuple|1|?>>
    </collection>
  </references>
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
  </collection>
</references>