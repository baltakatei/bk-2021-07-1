<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <label|Chap2><problem>Let <math|X> represent the quantity <math|V<rsup|2>>
  with dimensions <math|<around|(|<tx|l*e*n*g*t*h>|)><rsup|6>>. Give a reason
  that <math|X> is or is not an extensive property. Give a reason that
  <math|X> is or is not an intensive property. <soln| <math|X> is not an
  extensive property because it is not additive:
  <math|<around|(|V<aph>|)><rsup|2>+<around|(|V<bph>|)><rsup|2>\<ne\><around|(|V<aph>+V<bph>|)><rsup|2>>
  (e.g., <math|1<rsup|2>+1<rsup|2>\<ne\>2<rsup|2>>).<next-line><math|X> is
  not an intensive property because it is dependent on volume.>

  <problem>Calculate the <em|relative uncertainty> (the uncertainty divided
  by the value) for each of the measurement methods listed in Table
  <reference|tbl:2-methods><vpageref|tbl:2-methods>, using the typical values
  shown. For each of the five physical quantities listed, which measurement
  method has the smallest relative uncertainty? <soln|
  Mass:<next-line><space|1em><wide*|analytical balance|\<bar\>>,
  <math|0.1<timesten|-3><units|g>/100<units|g>=1<timesten|-6>><next-line><space|1em>micro
  balance, <math|0.1<timesten|-6><units|g>/20<timesten|-3><units|g>=5<timesten|-6>><next-line>Volume:<next-line><space|1em>pipet,
  <math|0.02<units|m*l>/10<units|m*L>=2<timesten|-3>><next-line><space|1em><wide*|volumetric
  flask|\<bar\>>, <math|0.3<timesten|-3><units|L>/1<units|L>=3<timesten|-4>><next-line>Density:<next-line><space|1em>pycnometer,
  <math|2<timesten|-3><units|g*<space|0.17em>m*L<per>>/1<units|g*<space|0.17em>m*L<per>>=2<timesten|-3>><next-line><space|1em><wide*|magnetic
  float densimeter|\<bar\>>, <math|0.1<timesten|-3><units|g*<space|0.17em>m*L<per>>/1<units|g*<space|0.17em>m*L<per>>=1<timesten|-4>><next-line>Pressure:<next-line><space|1em><wide*|manometer
  or barometer|\<bar\>>, <math|0.001<units|T*o*r*r>/760<units|T*o*r*r>=1<timesten|-6>><next-line><space|1em>diaphragm
  gauge, <math|1<units|T*o*r*r>/100<units|T*o*r*r>=1<timesten|-2>><next-line>Temperature:<next-line><space|1em>gas
  thermometer, <math|0.001<K>/10<K>=1<timesten|-4>><next-line><space|1em>mercury
  thermometer, <math|0.01<K>/300<K>=3<timesten|-5>><next-line><space|1em><wide*|platinum
  resistance thermometer|\<bar\>>, <math|0.0001<K>/300<K>=3<timesten|-7>><next-line><space|1em>optical
  pyrometer, <math|0.03<K>/1300<K>=2<timesten|-5>><next-line><vspace*|.5ex>
  The measurement of temperature with a platinum resistance thermometer has
  the least relative uncertainty, and the measurement of pressure with a
  diaphragm gauge has the greatest. For each physical quantity, the
  measurement method with smallest relative uncertainty is underlined in the
  preceding list.>

  <problem><label|prb:2-He-virial> Table <reference|tbl:2-He-virial><vpageref|tbl:2-He-virial>

  <\big-table>
    <minipagetable|10cm|<label|tbl:2-He-virial><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|12|12|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|<around*|(|1/V<m>|)>/10<rsup|2><units|m*o*l*<space|0.17em>m<rsup|<math|-3>>>>>|<cell|<math|<around*|(|p<rsub|2>*V<m>/R|)>/K>>>|<row|<cell|1.0225>|<cell|2.7106>>|<row|<cell|1.3202>|<cell|2.6994>>|<row|<cell|1.5829>|<cell|2.6898>>|<row|<cell|1.9042>|<cell|2.6781>>|<row|<cell|2.4572>|<cell|2.6580>>|<row|<cell|2.8180>|<cell|2.6447>>|<row|<cell|3.4160>|<cell|2.6228>>|<row|<cell|3.6016>|<cell|2.6162>>|<row|<cell|4.1375>|<cell|2.5965>>|<row|<cell|4.6115>|<cell|2.5790>>|<row|<cell|5.1717>|<cell|2.5586>>>>>>
  </big-table|Helium at a fixed temperature>

  lists data obtained from a constant-volume gas thermometer containing
  samples of varying amounts of helium maintained at a certain fixed
  temperature <math|T<rsub|2>> in the gas bulb.<footnote|Ref.
  <cite|berry-79>.> The molar volume <math|V<m>> of each sample was evaluated
  from its pressure in the bulb at a reference temperature of
  <math|T<rsub|1>=7.1992<K>>, corrected for gas nonideality with the known
  value of the second virial coefficient at that temperature.

  Use these data and Eq. <reference|pVm=RT(1+B/Vm...)><vpageref|pVm=RT(1+B/Vm...)>
  to evaluate <math|T<rsub|2>> and the second virial coefficient of helium at
  temperature <math|T<rsub|2>>. (You can assume the third and higher virial
  coefficients are negligible.)

  <\soln>
    \ With the third and higher virial coefficients set equal to zero, Eq.
    <reference|pVm=RT(1+B/Vm...)> becomes

    <\equation*>
      p*V<m>=R*T*<around*|(|1+<frac|B|V<m>>|)>
    </equation*>

    According to this equation, a plot of <math|p<rsub|2>*V<m>/R> versus
    <math|1/V<m>> should be linear with an intercept at <math|1/V<m|=>0>
    equal to <math|T<rsub|2>> and a slope equal to <math|B*T<rsub|2>>. The
    plot is shown in Fig. <reference|fig:2-He-virial><vpageref|fig:2-He-virial>.

    <\big-figure>
      <boxedfigure|<image|./02-SUP/He-virial.eps||||><capt|<label|fig:2-He-virial>>>
    </big-figure|>

    A least-squares fit of the data to a first-order polynomial yields an
    intercept of <math|2.7478<K>> and a slope of
    <math|-3.659<timesten|-4><units|K*<space|0.17em>m<rsup|<math|3>>*<space|0.17em>mol<per>>>.
    The temperature and second virial coefficient therefore have the values
    <vs><math|T<rsub|2>=2.7478<K>> <vs><math|<D>B=<frac|-3.659<timesten|-4><units|K*<space|0.17em>m<rsup|<math|3>>*<space|0.17em>mol<per>>|2.7478<K>>=-1.332<timesten|-4><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>
  </soln>

  <problem>Discuss the proposition that, to a certain degree of
  approximation, a living organism is a steady-state system. <soln| The
  organism can be treated as being in a steady state if we assume that its
  mass is constant and if we neglect internal motion. Matter enters the
  organism in the form of food, water, and oxygen; waste matter and heat
  leave the system.>

  <problem>

  The value of <math|<Del>U> for the formation of one mole of crystalline
  potassium iodide from its elements at <math|25<units|<degC>>> and
  <math|1<br>> is <math|-327.9<units|k*J>>. Calculate <math|<Del>m> for this
  process. Comment on the feasibility of measuring this mass change.

  <\soln>
    <math|<Del>m=<Del>U/c<rsup|2>=-327.9<timesten|3><units|J>/<around|(|2.998<timesten|8><units|m*<space|0.17em>s<per>>|)><rsup|2>=-3.648<timesten|-12><units|k*g>>
    <vs>This mass change is much less than the uncertainty of a microbalance
    (Table <reference|tbl:2-methods>), which does not even have the capacity
    to weigh one mole of KI<emdash>so it is hopeless to try to measure this
    mass change.
  </soln>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|Chap2|<tuple|?|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|1|?>>
    <associate|fig:2-He-virial|<tuple|1|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|prb:2-He-virial|<tuple|3|?>>
    <associate|tbl:2-He-virial|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      berry-79
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1>||>|<pageref|auto-2>>
    </associate>
    <\associate|table>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1>||Helium at a fixed
      temperature>|<pageref|auto-1>>
    </associate>
  </collection>
</auxiliary>