<TeXmacs|1.99.20>

<style|<tuple|generic|std-latex>>

<\body>
  <label|Chap10><problem><label|prb:10-NaCl> The mean ionic activity
  coefficient of NaCl in a 0.100 molal aqueous solution at <math|298.15<K>>
  has been evaluated with measurements of equilibrium cell
  potentials,<footnote|Ref. <cite|robinson-59>, Table 9.3.> with the result
  <math|ln <g><rsub|\<pm\>>=-0.2505>. Use this value in Eq.
  <reference|ln(g+-)=...>, together with the values of osmotic coefficients
  in Table <reference|tbl:10-NaCl><vpageref|tbl:10-NaCl>, to evaluate
  <math|<g><rsub|\<pm\>>> at each of the molalities shown in the table; then
  plot <math|<g><rsub|\<pm\>>> as a function of <math|m<B>>.

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|7|7|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|m<B>/<tx|m*o*l*<space|0.17em>k*g<per>>>>|<cell|<math|\<phi\><rsub|m>>>|<cell|>|<cell|<math|m<B>/<tx|m*o*l*<space|0.17em>k*g<per>>>>|<cell|<math|\<phi\><rsub|m>>>>|<row|<cell|0.1>|<cell|0.9325>|<cell|>|<cell|2.0>|<cell|0.9866>>|<row|<cell|0.2>|<cell|0.9239>|<cell|>|<cell|3.0>|<cell|1.0485>>|<row|<cell|0.3>|<cell|0.9212>|<cell|>|<cell|4.0>|<cell|1.1177>>|<row|<cell|0.5>|<cell|0.9222>|<cell|>|<cell|5.0>|<cell|1.1916>>|<row|<cell|1.0>|<cell|0.9373>|<cell|>|<cell|6.0>|<cell|1.2688>>|<row|<cell|1.5>|<cell|0.9598>|<cell|>|<cell|>|<cell|>>>>>
  </big-table|<label|tbl:10-NaCl>Osmotic coefficients of aqueous NaCl at
  <math|298.15<K>><space|.15em><footnote|Ref. <cite|clarke-85>.>>

  <\soln>
    <\big-figure>
      <boxedfigure|<image|./10-SUP/NaCl.eps||||> <capt|Aqueous NaCl at
      <math|298.15<K>>.<label|fig:10prob-NaCl>>>
    </big-figure|>

    The function <math|<around|(|\<phi\><rsub|m>-1|)>/m<B>> is plotted versus
    <math|m<B>> in Fig. <reference|fig:10prob-NaCl>(a)
    <vpageref|fig:10prob-NaCl>. Values of the area under the curve from
    <math|m<B>=0.1<units|m*o*l*<space|0.17em>k*g<per>>> to higher molalities
    are listed in the second column of Table
    <reference|tbl:7soln-NaCl><vpageref|tbl:7soln-NaCl>. The last column of
    this table lists values of <math|ln <g><rsub|\<pm\>>> calculated from Eq.
    <reference|ln(g+-)=...> with <math|m<rprime|''><B>=0.1<units|m*o*l*<space|0.17em>k*g<per>>>
    and <math|ln <g><rsub|\<pm\>><around|(|m<rprime|''><B>|)>=-0.2505>; Fig.
    <reference|fig:10prob-NaCl>(b) shows <math|<g><rsub|\<pm\>>> as a
    function of <math|m<B>>.

    <\big-table>
      <minipagetable|6.4cm|<label|tbl:7soln-NaCl><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|12|12|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|m<rprime|'><B>/<tx|m*o*l*<space|0.17em>k*g<per>>>>|<cell|<ccol|<math|<big|int><rsub|m<rprime|''><B>><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>><dif>m<B>>>>|<cell|<ccol|<math|ln
      <g><rsub|\<pm\>>>>>>|<row|<cell|0.1>|<cell|0>|<cell|-0.2505>>|<row|<cell|0.2>|<cell|-0.0430>|<cell|-0.3021>>|<row|<cell|0.3>|<cell|-0.0751>|<cell|-0.3369>>|<row|<cell|0.5>|<cell|-0.1118>|<cell|-0.3726>>|<row|<cell|1.0>|<cell|-0.1609>|<cell|-0.4066>>|<row|<cell|1.5>|<cell|-0.1808>|<cell|-0.4040>>|<row|<cell|2.0>|<cell|-0.1887>|<cell|-0.3851>>|<row|<cell|3.0>|<cell|-0.1825>|<cell|-0.3170>>|<row|<cell|4.0>|<cell|-0.1592>|<cell|-0.2245>>|<row|<cell|5.0>|<cell|-0.1251>|<cell|-0.1165>>|<row|<cell|6.0>|<cell|-0.0836>|<cell|-0.0022>>>>>>
    </big-table|Calculations for Prob. 10.<reference|prb:10-NaCl>>
  </soln>

  <problemAns>Rard and Miller<footnote|Ref. <cite|rard-81>.> used published
  measurements of the freezing points of dilute aqueous solutions of
  Na<rsub|<math|2>>SO<rsub|<math|4>> to calculate the osmotic coefficients of
  these solutions at <math|298.15<K>>. Use their values listed in Table
  <reference|tbl:10-Na2SO4><vpageref|tbl:10-Na2SO4> to evaluate the mean
  ionic activity coefficient of Na<rsub|<math|2>>SO<rsub|<math|4>> at
  <math|298.15<K>> and a molality of <math|m<B>=0.15<units|m*o*l*<space|0.17em>k*g<per>>>.
  For the parameter <math|a> in the Debye\UH�ckel equation (Eq.
  <reference|ln(g+-) (DH)>), use the value
  <math|a=3.0<timesten|-10><units|m>>.

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|9|9|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|m<B>/<tx|m*o*l*<space|0.17em>k*g<per>>>>|<cell|<math|\<phi\><rsub|m>>>|<cell|>|<cell|<math|m<B>/<tx|m*o*l*<space|0.17em>k*g<per>>>>|<cell|<math|\<phi\><rsub|m>>>>|<row|<cell|0.0126>|<cell|0.8893>|<cell|>|<cell|0.0637>|<cell|0.8111>>|<row|<cell|0.0181>|<cell|0.8716>|<cell|>|<cell|0.0730>|<cell|0.8036>>|<row|<cell|0.0228>|<cell|0.8607>|<cell|>|<cell|0.0905>|<cell|0.7927>>|<row|<cell|0.0272>|<cell|0.8529>|<cell|>|<cell|0.0996>|<cell|0.7887>>|<row|<cell|0.0374>|<cell|0.8356>|<cell|>|<cell|0.1188>|<cell|0.7780>>|<row|<cell|0.0435>|<cell|0.8294>|<cell|>|<cell|0.1237>|<cell|0.7760>>|<row|<cell|0.0542>|<cell|0.8178>|<cell|>|<cell|0.1605>|<cell|0.7616>>|<row|<cell|0.0594>|<cell|0.8141>|<cell|>|<cell|>|<cell|>>>>>
  </big-table|<label|tbl:10-Na2SO4>Osmotic coefficients of aqueous
  Na<rsub|<math|2>>SO<rsub|<math|4>> at <math|298.15<K>>>

  <\answer>
    <math|<g|\<pm\>>=0.392>
  </answer>

  <\soln>
    <\big-figure>
      <boxedfigure|<image|./10-SUP/Na2SO4.eps||||> <capt|Aqueous
      Na<rsub|<math|2>>SO<rsub|<math|4>> at
      <math|298.15<K>>.<label|fig:10prob-Na2SO4>>>
    </big-figure|>

    Estimate <math|ln <g><rsub|\<pm\>>> at
    <math|m<B>=0.0126<units|m*o*l*<space|0.17em>k*g<per>>>, the lowest
    molality for which experimental data are available, using the
    Debye--H�ckel equation: <vs><math|<D>ln
    <g><rsub|\<pm\>>=-<frac|A<subs|D*H><around*|\||z<rsub|+>*z<rsub|->|\|><sqrt|I<rsub|m>>|1+B<subs|D*H>a<sqrt|I<rsub|m>>>>
    <vs><math|<D><phantom|ln <g><rsub|\<pm\>>>=-<frac|<around|(|1.1744<units|k*g<rsup|<math|1/2>>*<space|0.17em>mol<rsup|<math|-1/2>>>|)><around|(|2|)><sqrt|<around|(|3|)><around|(|0.0126<units|m*o*l*<space|0.17em>k*g<per>>|)>>|1+<around|(|3.285<timesten|9><units|m<per><space|0.17em>k*g<rsup|<math|1/2>>*<space|0.17em>mol<rsup|<math|-1/2>>>|)><around|(|3.0<timesten|-10><units|m>|)><sqrt|<around|(|3|)><around|(|0.0126<units|m*o*l*<space|0.17em>k*g<per>>|)>>>>
    <vs><math|<D><phantom|ln <g><rsub|\<pm\>>>=-0.3832> <vs>The experimental
    points are plotted in Fig. <reference|fig:10prob-Na2SO4><vpageref|fig:10prob-Na2SO4>
    (open circles). From the curve through the points, estimate
    <math|<around|(|\<phi\><rsub|m>-1|)>/m<B>=-1.58<units|k*g*<space|0.17em>m*o*l<per>>>
    at <math|m<B>=0.15<units|m*o*l*<space|0.17em>k*g<per>>> (open triangle).
    At this point: <math|\<phi\><rsub|m>=<around|(|0.15<units|m*o*l*<space|0.17em>k*g<per>>|)>*<around|(|-1.58<units|k*g*<space|0.17em>m*o*l<per>>|)>+1=0.763>.
    <vs>Let <math|m<B><rprime|''>=0.0126<units|m*o*l*<space|0.17em>k*g<per>>>
    and <math|m<B><rprime|'>=0.15<units|m*o*l*<space|0.17em>k*g<per>>>. By
    numerical integration: <vs><math|<D><big|int><rsub|m<B><rprime|''>><rsup|m<B><rprime|'>><space|-0.17em><space|-0.17em><around*|(|<frac|\<phi\><rsub|m>-1|m<B>>|)><dif>m<B>=-0.426>
    <vs>Evaluate <math|<g><rsub|\<pm\>>> at <math|m<B>=m<B><rprime|'>> from
    Eq. <reference|ln(g+-)=...>: <vs><math|<D>ln
    <g><rsub|\<pm\>><around|(|m<rprime|'><B>|)>=\<phi\><rsub|m><around|(|m<rprime|'><B>|)>-\<phi\><rsub|m><around|(|m<rprime|''><B>|)>+ln
    <g><rsub|\<pm\>><around|(|m<rprime|''><B>|)>+<big|int><rsub|m<rprime|''><B>><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>><dif>m<B>>
    <vs><math|<D><phantom|ln <g><rsub|\<pm\>><around|(|m<rprime|'><B>|)>>=0.763-0.8893-0.3832-0.426>
    <vs><math|<D><phantom|ln <g><rsub|\<pm\>><around|(|m<rprime|'><B>|)>>=-0.936>
    <vs><math|<g><rsub|\<pm\>>=0.392>
  </soln>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|Chap10|<tuple|?|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|1|?>>
    <associate|auto-3|<tuple|2|?>>
    <associate|auto-4|<tuple|3|?>>
    <associate|auto-5|<tuple|2|?>>
    <associate|fig:10prob-Na2SO4|<tuple|2|?>>
    <associate|fig:10prob-NaCl|<tuple|1|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
    <associate|prb:10-NaCl|<tuple|1|?>>
    <associate|tbl:10-Na2SO4|<tuple|3|?>>
    <associate|tbl:10-NaCl|<tuple|1|?>>
    <associate|tbl:7soln-NaCl|<tuple|2|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      robinson-59

      clarke-85

      rard-81
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1>||>|<pageref|auto-2>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|2>||>|<pageref|auto-5>>
    </associate>
    <\associate|table>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1>||Osmotic
      coefficients of aqueous NaCl at <with|mode|<quote|math>|298.15<error|compound
      K>><space|.15em><assign|footnote-nr|2><hidden-binding|<tuple>|2><\float|footnote|>
        <with|font-size|<quote|0.771>|<with|par-mode|<quote|justify>|par-left|<quote|0cm>|par-right|<quote|0cm>|font-shape|<quote|right>|dummy|<quote|<macro|<tex-footnote-sep>>>|dummy|<quote|<macro|<tex-footnote-tm-barlen>>>|<surround|<locus|<id|%-475953EA8--47672EBD8>|<link|hyperlink|<id|%-475953EA8--47672EBD8>|<url|#footnr-2>>|2>.
        |<hidden-binding|<tuple|footnote-2>|2><specific|texmacs|<htab|0fn|first>>|Ref.
        [<write|bib|clarke-85><reference|bib-clarke-85>].>>>
      </float><space|0spc><rsup|<with|font-shape|<quote|right>|<reference|footnote-2>>>>|<pageref|auto-1>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|2>||Calculations for
      Prob. 10.<reference|prb:10-NaCl>>|<pageref|auto-3>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|3>||Osmotic
      coefficients of aqueous Na<rsub|<with|mode|<quote|math>|2>>SO<rsub|<with|mode|<quote|math>|4>>
      at <with|mode|<quote|math>|298.15<error|compound K>>>|<pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>