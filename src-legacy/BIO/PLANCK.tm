<TeXmacs|2.1.1>

<style|<tuple|generic|std-latex>>

<\body>
  <label|bio:planck>

  <I|Planck, Max\|biopage>

  <bioimage| <image|./bio/planck.eps||||> ><no-indent>Max Planck, best known
  for his formulation of the quantum theory, had a passionate interest in
  thermodynamics in the early part of his career.

  He was born in Kiel, Germany, where his father was a distinguished law
  professor. His family had a long tradition of conservatism, idealism, and
  excellence in scholarship.

  As a youth, Planck had difficulty deciding between music and physics as a
  career, finally settling on physics. He acquired his interest in
  thermodynamics from studies with Hermann von Helmholtz and Gustav Kirchhoff
  and from the writings of Rudolf Clausius. His doctoral dissertation at the
  University of Munich (1879) was on the second law.

  In 1897, Planck turned his papers on thermodynamics into a concise
  introductory textbook, <em|Treatise on Thermodynamics>. It went through at
  least seven editions and has been translated into English.<footnote|Ref.
  <cite|planck-22>.>

  Concerning the second law he wrote:<footnote|Ref. <cite|planck-48>, pages
  29--30.>

  <\quotation>
    Another controversy arose with relation to the question of the analogy
    between the passage of heat from a higher to a lower temperature and the
    sinking of a weight from a greater to a smaller height. I had emphasized
    the need for a sharp distinction between these two processes...However,
    this theory of mine was contradicted by the view universally accepted in
    those days, and I just could not make my fellow physicists see it my
    way...

    A consequence of this point of view [held by others] was that the
    assumption of irreversibility for proving the Second Law of
    Thermodynamics was declared to be unessential; furthermore, the existence
    of an absolute zero of temperature was disputed, on the ground that for
    temperature, just as for height, only differences can be measured. It is
    one of the most painful experiences of my entire scientific life that I
    have but seldom<emdash>in fact, I might say, never<emdash>succeeded in
    gaining universal recognition for a new result, the truth of which I
    could demonstrate by a conclusive, albeit only theoretical proof. This is
    what happened this time, too. All my sound arguments fell on deaf ears.
  </quotation>

  Planck became an associate professor of physics at the University of Kiel.
  In 1889 he succeeded Kirchhoff as Professor at Berlin University. By the
  end of the following year, at the age of 42, he had worked out his quantum
  theory to explain the experimental facts of blackbody radiation, a
  formulation that started a revolution in physics. He was awarded the 1918
  Nobel Prize in Physics ``in recognition of the services he rendered to the
  advancement of Physics by his discovery of energy quanta.''

  Planck was reserved and only enjoyed socializing with persons of similar
  rank. He was a gifted pianist with perfect pitch, and enjoyed hiking and
  climbing well into his old age. He was known for his fairness, integrity,
  and moral force.

  He endured many personal tragedies in his later years. His first wife died
  after 22 years of a happy marriage. His elder son was killed in action
  during World War I. Both of his twin daughters died in childbirth.

  Planck openly opposed the Nazi persecution of Jews but remained in Germany
  during World War II out of a sense of duty. The war brought further
  tragedy: his house in Berlin was destroyed by bombs, and his second son was
  implicated in the failed 1944 attempt to assassinate Hitler and was
  executed by the Gestapo. Planck and his second wife escaped the bombings by
  hiding in the woods and sleeping in haystacks in the countryside. They were
  rescued by American troops in May, 1945.
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:planck|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      planck-22

      planck-48
    </associate>
  </collection>
</auxiliary>