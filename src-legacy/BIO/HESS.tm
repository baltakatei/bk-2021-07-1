<TeXmacs|2.1.1>

<style|<tuple|generic|std-latex>>

<\body>
  <label|bio:hess>

  <I|Hess, Germain\|biopage>

  <bioimage| <image|./bio/hess.eps||||> ><no-indent>Hess was a Russian
  chemist and physician whose calorimetric measurements led him to formulate
  the law of constant heat summation, now known as Hess's law. His given name
  had several versions: ``Germain Henri'' in French, as shown above;
  ``Hermann Heinrich'' in German; and ``German Iwanowitsch'' in Russian.

  He was born in Geneva, Switzerland, the son of an artist. The family moved
  to Russia when he was three years old; his father had found work there as a
  tutor on an estate. Hess studied medicine at the University of Tartu in
  Estonia (then part of the Russian empire) and received his doctor of
  medicine degree in 1825. In addition to his medical studies, Hess took
  courses in chemistry and geology and wrote a doctoral dissertation on the
  composition of mineral waters in Russia.<footnote|Refs. <cite|kohler-2011>
  and <cite|leicester-51>.>

  Hess was more interested in chemistry than medicine. He briefly studied
  with the famous Swedish chemist J�ns Jakob Berzelius in Stockholm, and they
  became life-long friends. Hess then practiced medicine in Irkutsk, Siberia,
  while continuing his interests in mineral chemistry and chemical analysis.

  In 1829, after being being elected an adjunct member of the St. Petersburg
  Academy of Sciences, he gave up his medical practice and began teaching
  chemistry at various institutions of higher learning in St. Petersburg. He
  wrote a two-volume textbook, <em|Fundamentals of Pure Chemistry>, in 1831,
  followed by a one-volume abridgment in 1834 that became the standard
  Russian chemistry textbook and went through seven editions. He became a
  full member of the St. Petersburg Academy Academy in 1834.

  Hess published the results of his thermochemical research between 1839 and
  1842. His 1840 paper<footnote|Ref. <cite|hess-1840>.> describes his
  measurements of the heat evolved when pure sulfuric acid,
  H<rsub|<math|2>>SO<rsub|<math|4>>, is mixed with various amounts of water,
  and another series of measurements of the heat evolved when the acid in
  H<rsub|<math|2>>SO<rsub|<math|4>>-water mixtures is neutralized with
  aqueous ammonia. The following table from this paper is of historical
  interest, although it is a bit difficult to decipher:

  <\quotation>
    <padded-center|<tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|l>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|r>|<cwith|1|-1|3|3|cell-halign|r>|<cwith|1|-1|4|4|cell-halign|r>|<cwith|1|-1|4|4|cell-rborder|0ln>|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|2|2|cell-col-span|2>|<cwith|1|1|2|2|cell-halign|c>|<cwith|1|1|2|2|cell-rborder|0ln>|<cwith|6|6|4|4|cell-bborder|1ln>|<cwith|7|7|3|3|cell-col-span|2>|<cwith|7|7|3|3|cell-halign|r>|<cwith|7|7|3|3|cell-rborder|0ln>|<table|<row|<cell|Acid>|<cell|Heat
    evolved by>|<cell|>|<cell|Sum <vspace|-.09cm>>>|<row|<cell|>|<cell|ammonia>|<cell|water>|<cell|>>|<row|<cell|<math|<wide|<tx|<sout|H>>|\<dot\>><space|0.17em><wide|<tx|S>|\<dddot\>>>>|<cell|<math|595.8>>|<cell|>|<cell|<math|595.8>>>|<row|<cell|<math|<wide|<tx|<sout|H>>|\<dot\>><rsup|2><space|0.17em><wide|<tx|S>|\<dddot\>>>>|<cell|<math|518.9>>|<cell|<math|77.8>>|<cell|<math|596.7>>>|<row|<cell|<math|<wide|<tx|<sout|H>>|\<dot\>><rsup|3><space|0.17em><wide|<tx|S>|\<dddot\>>>>|<cell|<math|480.5>>|<cell|<math|116.7>>|<cell|<math|597.2>>>|<row|<cell|<math|<wide|<tx|<sout|H>>|\<dot\>><rsup|6><space|0.17em><wide|<tx|S>|\<dddot\>>>>|<cell|<math|446.2>>|<cell|<math|155.6>>|<cell|<math|601.8>>>|<row|<cell|>|<cell|>|<cell|Average
    <math|597.9>>|<cell|>>>>>>
  </quotation>

  The first column gives the relative amounts of acid and water in the
  notation of Berzelius: <math|<wide|<tx|<sout|H>>|\<dot\>>> is
  H<rsub|<math|2>>O, <math|<wide|<tx|S>|\<dddot\>>> is SO<rsub|<math|3>>, and
  <math|<wide|<tx|<sout|H>>|\<dot\>><space|0.17em><wide|<tx|S>|\<dddot\>>> is
  H<rsub|<math|2>>SO<rsub|<math|4>>. Thus
  <math|<wide|<tx|<sout|H>>|\<dot\>><rsup|3><space|0.17em><wide|<tx|S>|\<dddot\>>>,
  for example, is <math|<chem|H<rsub|2>*S*O<rsub|4>>+2<chem|H<rsub|2>*O>>.
  The second and third columns show the heat evolved (<math|-q>) in units of
  calories per gram of the SO<rsub|<math|3>> moiety of the
  H<rsub|<math|2>>SO<rsub|<math|4>>. The near-equality of the sums in the
  last column for the overall reaction <math|<chem>H<rsub|2>*S*O<rsub|4><around|(|l|)>+2*<space|0.17em>N*H<rsub|3>*<around|(|a*q|)><arrow><around|(|N*H<rsub|4>|)><rsub|2>*S*O<rsub|4><around|(|a*q|)>>
  demonstrates Hess's law of constant heat summation, which he stated in the
  words:<footnote|Ref. <cite|hess-1840>; translation in Ref.
  <cite|davis-51>.>

  <quotation|The amount of heat evolved during the formation of a given
  compound is constant, independent of whether the compound is formed
  directly or indirectly in a series of steps.>

  Hess confirmed this law with similar experiments using other acids such as
  HCl(aq) and other bases such as NaOH(aq) and CaO(s).

  After 1848 Hess's health began to deteriorate. He was only 48 when he died.
  His role as the real founder of thermochemistry was largely forgotten until
  the influential German physical chemist Wilhelm Ostwald drew attention to
  it about forty years after Hess's death.
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:hess|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      kohler-2011

      leicester-51

      hess-1840

      hess-1840

      davis-51
    </associate>
  </collection>
</auxiliary>