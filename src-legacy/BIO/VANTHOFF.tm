<TeXmacs|2.1.1>

<style|<tuple|generic|std-latex>>

<\body>
  <label|bio:vanthoff>

  <I|van't Hoff, Jacobus\|biopage>

  <bioimage| <image|./bio/vanthoff.eps||||> ><no-indent>van't Hoff was a
  Dutch chemist who gained fame from epoch-making publications in several
  fields of theoretical chemistry. He was an introvert who valued nature,
  philosophy, poetry, and the power of imagination.<footnote|Ref.
  <cite|vanthoff-nobel>.>

  van't Hoff was born in Rotterdam, the son of a physician, and studied at
  Delft, Leyden, Bonn, and Paris before obtaining his doctor's degree at
  Utrecht in 1874. For 18 years he was Professor of Chemistry, Mineralogy,
  and Geology at the University of Amsterdam. In 1896, mainly to escape
  burdensome lecture duties, he accepted an appointment as Professor of
  Physical Chemistry at the University of Berlin, where he remained for the
  rest of his life.

  In the same year he received his doctor's degree, he published the first
  explanation of optical activity based on the stereoisomerism of an
  asymmetric carbon atom. Similar ideas were published independently two
  months later by the French chemist Joseph Le Bel.

  In 1884 he entered the field of physical chemistry with his book <em|�tudes
  de Dynamique Chimique>, a systematic study of theories of reaction kinetics
  and chemical equilibrium. The book introduced, in the form of Eq.
  <reference|d(lnK)/dT=del(r)Hmo/RT2>, his expression for the temperature
  dependence of an equilibrium constant.

  van't Hoff next used thermodynamic cycles to reason that solutions of equal
  osmotic pressure should have the same values of freezing-point depression
  and of vapor-pressure lowering. Then, from Raoult's extensive measurements
  of these values, he found that the osmotic pressure of a dilute solution is
  described by <math|<varPi>=i*c<B>R*T> (Eq. <reference|Pi=()cB> with
  <math|\<nu\>> replaced by <math|i>). The Swedish chemist Svante Arrhenius
  later interpreted <math|i> as the number of particles per solute formula
  unit, helping to validate his theory of ionic dissociation of electrolytes.

  In a celebrated 1887 summary of his theory of osmotic pressure, van't Hoff
  wrote:<footnote|Ref. <cite|vanthoff-1887>.>

  <quotation|...the relation found permits of an important extension of the
  law of Avogadro, which now finds application also to all [nonelectrolyte]
  solutions, if only osmotic pressure is considered instead of elastic
  pressure. At equal osmotic pressure and equal temperature, equal volumes of
  the most widely different solutions contain an equal number of molecules,
  and, indeed, the same number which, at the same pressure and temperature,
  is contained in an equal volume of a gas. ...the existence of the so-called
  normal molecular lowering of the freezing-point and diminution of the
  vapor-pressure were not discovered until Raoult employed the organic
  compounds. These substances, almost without exception, behave normally. It
  may, then, have appeared daring to give Avogadro's law for solutions such a
  prominent place, and I should not have done so had not Arrhenius pointed
  out to me, by letter, the probability that salts and analogous substances,
  when in solution, break down into ions.>

  In 1901 van't Hoff was awarded the first Nobel Prize in Chemistry ``in
  recognition of the extraordinary services he has rendered by the discovery
  of the laws of chemical dynamics and osmotic pressure in solutions.''

  van't Hoff was married with two daughters and two sons. He died of
  tuberculosis at age fifty-eight. In a memorial article, Frederick
  Donnan<footnote|Donnan was an Irish physical chemist after whom the Donnan
  membrane equilibrium and Donnan potential (Sec. <reference|12-Donnan eqm>)
  are named.> wrote:<footnote|Ref. <cite|donnan-1911>.>

  <quotation|The present writer is one of those whose privilege it is to have
  worked under van't Hoff. ...Every day endeared van't Hoff to the small band
  of workers in his laboratory. His joy in his work, the simple and
  unaffected friendliness of his nature, and the marvellous power of his mind
  affected us most deeply. All who worked with van't Hoff quickly learned to
  love and respect him, and we were no exception to the rule.>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:vanthoff|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      vanthoff-nobel

      vanthoff-1887

      donnan-1911
    </associate>
  </collection>
</auxiliary>