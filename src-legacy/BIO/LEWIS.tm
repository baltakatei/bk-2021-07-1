<TeXmacs|2.1.1>

<style|<tuple|generic|std-latex>>

<\body>
  <label|bio:lewis>

  <I|Lewis, Gilbert Newton\|biopage>

  <\bioimage>
    \ <minipage|t|5pt|>

    <minipage|t|5pt|>

    <image|./bio/lewis.eps||||> <minipage|t|5pt|
    <rotatebox|90|<miniscule><with|font-family|sf|<definecolor|darkgray|gray|.5><with|color|darkgray|EDGAR
    FAHS SMITH COLLECTION>>>>

    <minipage|t|5pt|<rotatebox|90|<miniscule><with|font-family|sf|<definecolor|darkgray|gray|.5><with|color|darkgray|UNIVERSITY
    OF PENNSYLVANIA LIBRARY>>>>
  </bioimage>

  <no-indent>Gilbert Lewis made major contributions to several fields of
  physical chemistry. He was born in Weymouth, Massachusetts. His father was
  a lawyer and banker.

  Lewis was reserved, even shy in front of a large audience. He was also
  ambitious, had great personal charm, excelled at both experimental and
  theoretical thermodynamics, and was a chain smoker of vile Manila cigars.

  Lewis considered himself to be a disciple of Willard Gibbs. After
  completing his Ph.D dissertation at Harvard University in 1899, he
  published several papers of thermodynamic theory that introduced for the
  first time the terms fugacity (1901) and activity (1907). The first of
  these papers was entitled ``A New Conception of Thermal Pressure and a
  Theory of Solutions'' and began:<footnote|Ref. <cite|lewis-00>.>

  <quotation|For an understanding of all kinds of physico-chemical
  equilibrium a further insight is necessary into the nature of the
  conditions which exist in the interior of any homogeneous phase. It will be
  the aim of the present paper to study this problem in the light of a new
  theory, which, although opposed to some ideas which are now accepted as
  correct, yet recommends itself by its simplicity and by its ability to
  explain several important phenomena which have hitherto received no
  satisfactory explanation.>

  His first faculty position (1905-1912) was at Boston Tech, now the
  Massachusetts Institute of Technology, where he continued work in one of
  his dissertation subjects: the measurement of standard electrode potentials
  in order to determine standard molar Gibbs energies of formation of
  substances and ions.

  In 1912 he became the chair of the chemistry department at the University
  of California at Berkeley, which he turned into a renowned center of
  chemical research and teaching. In 1916 he published his theory of the
  shared electron-pair chemical bond (Lewis structures), a concept he had
  been thinking about since at least 1902. In the course of measuring the
  thermodynamic properties of electrolyte solutions, he introduced the
  concept of ionic strength (1921).

  In 1923, at age 48, he consolidated his knowledge of thermodynamics in the
  great classic <em|Thermodynamics and the Free Energy of Chemical
  Substances><footnote|Ref. <cite|lewis-23>.> with Merle Randall as coauthor.
  After that his interests changed to other subjects. He was the first to
  prepare pure deuterium and D<rsub|<math|2>>O (1933), he formulated his
  generalized definitions of acids and bases (Lewis acids and bases, 1938),
  and at the time of his death he was doing research on photochemical
  processes.

  Lewis was nominated 35 times for the Nobel prize, but was never awarded it.
  According to a history of modern chemistry published in 2008,<footnote|Ref.
  <cite|coffey-08>, Chap. 7.> Wilhelm Palmaer, a Swedish electrochemist, used
  his position on the Nobel Committee for Chemistry to block the award to
  Lewis. Palmaer was a close friend of Walther Nernst, whom Lewis had
  criticized on the basis of occasional ``arithmetic and thermodynamic
  inaccuracy.''<footnote|Ref. <cite|lewis-23>, page 6.>

  His career was summarized by his last graduate student, Michael Kasha, as
  follows:<footnote|Ref. <cite|kasha-84>.>

  <quotation|Gilbert Lewis once defined physical chemistry as encompassing
  ``everything that is interesting.'' His own career touched virtually every
  aspect of science, and in each he left his mark. He is justly regarded as
  one of the key scientists in American history. It would be a great omission
  not to record the warmth and intellectual curiosity radiated by Lewis'
  personality. He epitomized the scientist of unlimited imagination, and the
  joy of working with him was to experience the life of the mind unhindered
  by pedestrian concerns.>

  \ 
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:lewis|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnote-5|<tuple|5|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
    <associate|footnr-5|<tuple|5|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      lewis-00

      lewis-23

      coffey-08

      lewis-23

      kasha-84
    </associate>
  </collection>
</auxiliary>