<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <\biofloat>
    <label|bio:carnot>

    <I|Carnot, Sadi\|biopage>

    <\bio|Sadi Carnot|1796--1832>
      <bioimage| <image|./bio/carnot.eps||||> ><no-indent>

      - significance to thermo - where born - parents - personality - events,
      anecdotes, quotes
    </bio>
  </biofloat>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:carnot|<tuple|?|?>>
  </collection>
</references>