<TeXmacs|2.1.1>

<style|<tuple|generic|std-latex>>

<\body>
  <label|bio:kelvin>

  <I|Kelvin, Baron of Largs\|biopage><I|Thomson, William\|biopage>

  <bioimage| <image|./bio/kelvin.eps||||> ><no-indent>William Thomson was
  born in Belfast, Ireland. His mother died when he was six. In 1832 the
  family moved to Glasgow, Scotland, where his father had been appointed as
  the chair of mathematics at the University.

  In 1845 Thomson was age 21. He had recently graduated from Cambridge
  University with high honors in mathematics, and was in Paris for
  discussions with French physicists and mathematicians. He had learned about
  Carnot's book from Clapeyron's 1834 paper but could not find a
  copy<emdash>no bookseller in Paris had even heard of it. Nevertheless, the
  information he had was sufficient for him to realize that Carnot's ideas
  would allow a thermodynamic temperature scale to be defined, one that does
  not depend on any particular gas.

  The following year, Thomson became the Chair of Natural Philosophy at
  Glasgow University, largely through the influence of his father. He
  remained there until his retirement in 1899. His concept of a thermodynamic
  temperature scale, his best-known contribution to thermodynamics, was
  published in 1848.<footnote|Ref. <cite|kelvin-1848>.>

  Thomson published other papers on the theory of heat, but his ideas
  eventually changed as a result of hearing a presentation by James Joule at
  a meeting in Oxford in 1847. Joule was describing his experiments with the
  conversion of work to thermal energy by a paddle wheel. In a letter written
  to his nephew, J. T. Bottomley, Thomson wrote:<footnote|Ref.
  <cite|bottomley-1882>.>

  <\quotation>
    I made Joule's acquaintance at the Oxford meeting, and it quickly ripened
    into a life-long friendship.

    I heard his paper read in the section, and felt strongly impelled at
    first to rise and say that it must be wrong because the true mechanical
    value of heat given, suppose in warm water, must, for small differences
    of temperature, be proportional to the square of its quantity. I knew
    from Carnot that this <em|must> be true (and it <em|is> true; only now I
    call it `motivity,' to avoid clashing with Joule's `mechanical value.')
    But as I listened on and on, I saw that (though Carnot had vitally
    important truth, not to be abandoned) Joule had certainly a great truth
    and a great discovery, and a most important measurement to bring forward.
    So instead of rising with my objection to the meeting I waited till it
    was over, and said my say to Joule himself, at the end of the meeting.
    This made my first introduction to him. After that I had a long talk over
    the whole matter at one of the <em|conversaziones> of the Association,
    and we became fast friends from thenceforward.
  </quotation>

  The physicist Charles Everitt described Thomson's personality as
  follows:<footnote|Ref. <cite|everitt-67>.>

  <quotation|Thomson was the kind of man who created all around him a sense
  of bustle and excitement. ...[He] was a man of violent enthusiasms. He
  would take up a subject, work at it furiously for a few weeks, throw out a
  string of novel ideas, and then inexplicably drop everything and pass on.>

  During his career, Thomson published more than 600 papers and received many
  honors. In addition to the theory of heat, his work included a dynamical
  theory of electricity and magnetism, and the invention of the mirror
  galvanometer used as the telegraph receiver for the first transatlantic
  submarine cables.

  Thomson was the chief technical consultant for the initial cable projects.
  As a result of their success and his involvement in the construction of a
  global cable network, he became extremely wealthy and was knighted in 1866.
  In 1892 he became Baron Kelvin of Largs. ``Kelvin'' is the name of the
  river that flows past Glasgow University, ``Largs'' is the town where he
  had his home, and <em|kelvin> is now the SI unit of thermodynamic
  temperature.
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:kelvin|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      kelvin-1848

      bottomley-1882

      everitt-67
    </associate>
  </collection>
</auxiliary>