<TeXmacs|2.1.1>

<style|<tuple|generic|std-latex>>

<\body>
  <label|bio:henry>

  <I|Henry, William\|biopage>

  <bioimage| <image|./bio/henry.eps||||> ><no-indent>William Henry was a
  British chemist, trained as a physician, who is best known for his
  formulation of what is now called Henry's law.

  Henry was born in Manchester, England. His father was an apothecary and
  industrial chemist who established a profitable business manufacturing such
  products as magnesium carbonate (used as an antacid) and carbonated water.
  At the age of ten, Henry was severely injured by a falling beam and was
  plagued by pain and ill health for the rest of his life.

  Henry began medical studies at the University of Edinburgh in 1795. He
  interrupted these studies to do chemical research, to assist his father in
  general medical practice, and to help run the family chemical business. He
  finally received his diploma of Doctor in Medicine in 1807. In 1809, in
  recognition of his research papers, he was elected a Fellow of the Royal
  Society.

  In 1801 the first edition of his influential chemistry textbook appeared,
  originally called <em|An Epitome of Chemistry> and in later editions
  <em|Elements of Experimental Chemistry>. The book went through eleven
  editions over a period of 28 years.

  Henry investigated the relation between the pressure of a gas and the
  volume of the gas, measured at that pressure, that was absorbed into a
  given volume of water. He used a simple apparatus in which the water and
  gas were confined over mercury in a graduated glass vessel, and the
  contents agitated to allow a portion of the gas to dissolve in the water.
  His findings were presented to the Royal Society of London in 1802 and
  published the following year:<footnote|Ref. <cite|henry-1803>.>

  <quotation|The results of a series of at least fifty experiments, on
  carbonic acid, sulphuretted hydrogen gas, nitrous oxide, oxygenous and
  azotic gases,<footnote|These gases are respectively
  <math|<chem|C*O<rsub|2>>>, <math|<chem|H<rsub|2>*S>>,
  <math|<chem|N<rsub|2>*O>>, <math|<chem|O<rsub|2>>>, and
  <math|<chem|N<rsub|2>>>.> with the above apparatus, establish the following
  general law: <em|that, under equal circumstances of temperature, water
  takes up, in all cases, the same volume of condensed gas as of gas under
  ordinary pressure.> But, as the spaces occupied by every gas are inversely
  as the compressing force, it follows, <em|that water takes up, of gas
  condensed by one, two, or more additional atmospheres, a quantity which,
  ordinarily compressed, would be equal to twice, thrice, &c. the volume
  absorbed under the common pressure of the atmosphere.>>

  Henry later confirmed a suggestion made by his close friend John Dalton,
  that the amount of a constituent of a gaseous mixture that is absorbed is
  proportional to its <em|partial> pressure.<footnote|Ref.
  <cite|henry-1803a>.>

  Henry carried out other important work, chiefly on gases, including the
  elemental compositions of hydrogen chloride, ammonia, and methane.

  Because of his poor health and unsuccessful surgery on his hands, Henry was
  unable to continue working in the lab after 1824. Twelve years later,
  suffering from pain and depression, he committed suicide.

  In a biography published the year after Henry's death, his son William
  Charles Henry wrote:<footnote|Quoted in Ref. <cite|thornber>.>

  <quotation|In the general intercourse of society, Dr. Henry was
  distinguished by a polished courtesy, by an intuitive propriety, and by a
  considerate forethought and respect for the feelings and opinions of
  others...His comprehensive range of thought and knowledge, his proneness to
  general speculation in contradistinction to detail, his ready command of
  the refinements of language and the liveliness of his feelings and
  imagination rendered him a most instructive and engaging companion.>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:henry|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      henry-1803

      henry-1803a

      thornber
    </associate>
  </collection>
</auxiliary>