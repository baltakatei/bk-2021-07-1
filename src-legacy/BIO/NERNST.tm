<TeXmacs|2.1.1>

<style|<tuple|generic|std-latex>>

<\body>
  <label|bio:nernst>

  <I|Nernst, Walther\|biopage>

  <bioimage| <image|./bio/nernst.eps||||> ><no-indent>Walther Nernst was a
  German physical chemist best known for his heat theorem, also known as the
  third law of thermodynamics.

  Nernst was born in Briesen, West Prussia (now Poland). His father was a
  district judge.

  From all accounts, Nernst was not an easy person to get along with. Gilbert
  Lewis, who spent a semester in Nernst's laboratory, was one of those who
  developed an enmity toward him; in later years, Lewis delighted in pointing
  out what he considered to be errors in Nernst's writings. The American
  physicist Robert A. Millikan, who studied with Nernst at Göttingen, wrote
  in a memorial article:<footnote|Ref. <cite|millikan-42>.>

  <quotation|He was a little fellow with a fish-like mouth and other
  well-marked idiosyncrasies. However, he was in the main popular in the
  laboratory, despite the fact that in the academic world he nearly always
  had a quarrel on with somebody. He lived on the second floor of the
  institute with his wife and three young children. As we students came to
  our work in the morning we would not infrequently meet him in his hunting
  suit going out for some early morning shooting. ...His greatest weakness
  lay in his intense prejudices and the personal, rather than the objective,
  character of some of his judgments.>

  At Leipzig University, in 1888, he published the Nernst equation, and in
  1890 the Nernst distribution law.

  In 1891 he moved to the University of Göttingen, where in 1895 he became
  director of the Göttingen Physicochemical Institute.

  In 1892 Nernst married Emma Lohmeyer, daughter of a Göttingen medical
  professor. They had two sons, both killed in World War I, and three
  daughters.

  Nernst wrote an influential textbook of physical chemistry, the second in
  the field, entitled <em|Theoretische Chemie vom Standpunkte der
  Avogadroschen Regel und der Thermodynamik>. It was first published in 1893
  and its last edition was in 1926.

  Nernst began work in 1893 on a novel electric incandescent lamp based on
  solid-state electrolytes. His sale of the patent in 1898 made him wealthy,
  but the lamp was not commercially successful.

  In 1905 Nernst was appointed director of the Berlin Physicochemical
  Institute; at the end of that year he reported the discovery of his heat
  theorem.

  Nernst was awarded the Nobel Prize in Chemistry for the year 1920 ``in
  recognition of his work in thermochemistry.'' In his Nobel Lecture,
  describing the heat theorem, he said:

  <\quotation>
    ...in all cases chemical affinity and evolution of heat become identical
    at low temperatures. Not, and this is the essential point, in the sense
    that they intersect at absolute zero, but rather in the sense that they
    invariably become practically identical some distance before absolute
    zero is reached; in other words the two curves become mutually tangential
    in the vicinity of absolute zero.

    If we frame this principle in quite general terms, i.e. if we apply it
    not only to chemical but to all processes, then we have the new heat
    theorem which gives rise to a series of very far-reaching consequences
    ...
  </quotation>

  Nernst would have nothing to do with the Nazis. When they passed the 1933
  law barring Jews from state employment, he refused to fire the Jewish
  scientists at the Berlin institute, and instead took the opportunity to
  retire. He caused a stir at a meeting by refusing to stand for the singing
  of the <em|Horst Wessel Lied>. Before his death he ordered that letters he
  had received be burned, perhaps to protect his correspondents from the Nazi
  authorities.<footnote|Ref. <cite|coffey-08>.>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:nernst|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      millikan-42

      coffey-08
    </associate>
  </collection>
</auxiliary>