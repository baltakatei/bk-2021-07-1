<TeXmacs|2.1.1>

<style|<tuple|generic|std-latex>>

<\body>
  <label|bio:debye>

  <I|Debye, Peter\|biopage>

  <bioimage| <image|./bio/debye.eps||||> ><no-indent>Peter Debye made major
  contributions to various areas of chemistry and physics.

  He was born in Maastricht, The Netherlands, where his father was foreman in
  a machine workshop.

  Henri Sack, a close associate for 40 years, recalled in 1968:<footnote|Ref.
  <cite|james-93>, page 232.>

  <quotation|He was not only endowed with a most powerful and penetrating
  intellect and an unmatched ability for presenting his ideas in a most lucid
  way, but he also knew the art of living a full life. He greatly enjoyed his
  scientific endeavors, he had a deep love for his family and home life, and
  he had an eye for the beauties of nature and a taste for the pleasure of
  the out-of-doors as manifested by his hobbies such as fishing, collecting
  cacti, and gardening, mostly in the company of Mrs. Debye.>

  Before World War II, Debye held appointments at several universities in The
  Netherlands, Switzerland, and Germany. He emigrated to America in 1940 and
  was at Cornell University in Ithaca, New York until his death. He became an
  American citizen in 1946.

  Debye was responsible for theoretical treatments of a variety of subjects,
  including molecular dipole moments (for which the <em|debye> is a non-SI
  unit), X-ray diffraction and scattering, and light scattering. His theories
  relevant to thermodynamics include the temperature dependence of the heat
  capacity of crystals at a low temperature (Debye crystal theory), adiabatic
  demagnetization, and the Debye--H�ckel theory of electrolyte solutions.

  In an interview in 1962, Debye said that he actually had not been
  interested in electrolytes at all. He had been at a colloquium at which a
  new theory of electrolytes had been described that was supposed to explain
  why the conductivity of a dilute solution of a strong electrolyte is
  proportional to the square root of the concentration. Debye, on hearing
  this description, objected that the theory neglected the effects of
  Brownian motion. The discussion became heated, and some of those present
  told Debye ``you will have to do something about it.'' What Debye did about
  it was to ask his assistant, Erich H�ckel, to study the literature and find
  out what they were missing. That, according to Debye in the interview, is
  how the Debye--H�ckel theory came about.<footnote|Ref. <cite|debye-62>.>

  In a reminiscence of Debye published in 1972, Erich H�ckel
  wrote:<footnote|Translation in Ref. <cite|vanginkel-06>, pages 73--74.>

  <\quotation>
    My personal relations with Debye were always completely care-free.
    Although I was 12 years younger than he and a complete freshman when I
    came to Z�rich, he always treated me as his equal.

    ...Debye conceived his work<emdash>in my opinion<emdash>as an artist who
    operates on the basis of joy in his work and its creations, and who was
    led often by intuition, which was then later on rationally founded in the
    most plain and clear way leaving out everything that was unessential.
    ...I never found in Debye any interest in philosophical questions.
    Debye's way of life seemed to me rather straightforward and
    uncomplicated. He liked a good dinner: when a problem could not be solved
    after a physics lecture, he used to say: ``one must enjoy a good evening
    dinner and then the inspiration comes by itself''...Debye received an
    immense number of awards. It did not seem to matter much to him. When I
    visited him in Berlin to congratulate him on the Nobel Prize, he
    interrupted: ``Fine that you are here.'' My congratulations were
    therefore not completed.
  </quotation>

  Debye was awarded the 1936 Nobel Prize in Chemistry ``for his contributions
  to our knowledge of molecular structure through his investigations on
  dipole moments and on the diffraction of X-rays and electrons in gases.''
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:debye|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      james-93

      debye-62

      vanginkel-06
    </associate>
  </collection>
</auxiliary>