<TeXmacs|2.1.1>

<style|<tuple|generic|std-latex>>

<\body>
  <label|bio:gibbs>

  <I|Gibbs, Willard\|biopage>

  <bioimage| <image|./bio/gibbs.eps||||> ><no-indent>Willard Gibbs's
  brilliant and rigorous formulation of the theoretical basis of classical
  thermodynamics was essential for further development of the subject.

  Gibbs was born in New Haven, Connecticut, and lived there all his life. His
  father was a professor in the Yale Divinity School. Gibbs was Professor of
  Mathematical Physics at Yale College from 1871 until his death.

  Gibbs never married. In demeanor he was serene, kindly, reserved, and
  self-effacing. A biographer wrote:<footnote|Ref. <cite|wheeler-52>, page
  83.>

  <quotation|Gibbs' attitude toward his discoveries is also illuminating as
  to his character. ...he made no effort to ``sell'' his discoveries (aside
  from the usual distribution of reprints of his papers) or to popularize the
  results. He was so confident of their rightness and ability to stand on
  their own feet that he was entirely content to let their value and
  importance be ``discovered'' by others. The fact that <em|he> had made a
  discovery was to him an irrelevant matter; the important thing was the
  truth established.>

  In 1873, when he was 34, the first two of Gibbs's remarkable papers on
  theoretical thermodynamics appeared in an obscure journal, <em|Transactions
  of the Connecticut Academy>.<footnote|Ref. <cite|gibbs-73a>.><footnote|Ref.
  <cite|gibbs-73b>.> These papers explored relations among state functions
  using two- and three-dimensional geometrical constructions.

  James Clerk Maxwell promoted Gibbs's ideas in England, and made a small
  plaster model of the three-dimensional <math|S>--<math|V>--<math|U> surface
  for H<rsub|<math|2>>O which he sent to Gibbs.

  The two papers of 1873 were followed by a monumental paper in the same
  journal<emdash>in two parts (1876 and 1878) and over 300 pages in
  length!<emdash>entitled simply ``On the Equilibrium of Heterogeneous
  Substances.''<footnote|Ref. <cite|gibbs-76>.> This third paper used an
  analytical rather than geometrical approach. From the first and second laws
  of thermodynamics, it derived the conditions needed for equilibrium in the
  general case of a multiphase, multicomponent system. It introduced the
  state functions now known as enthalpy, Helmholtz energy,<footnote|Hermann
  von Helmholtz, a German physiologist and physicist, introduced the term
  ``free energy'' for this quantity in 1882.> Gibbs energy, and chemical
  potential. Included in the paper was the exposition of the Gibbs phase
  rule.

  The only public comment Gibbs ever made on his thermodynamic papers was in
  a letter of 1881 accepting membership in the American Academy of Arts and
  Sciences:<footnote|Ref. <cite|wheeler-52>, page 89.>

  <quotation|The leading idea which I followed in my paper on the Equilibrium
  of Heterogeneous Substances was to develop the <em|r�les> of energy and
  entropy in the theory of thermo-dynamic equilibrium. By means of these
  quantities the general condition of equilibrium is easily expressed, and by
  applying this to various cases we are led at once to the special conditions
  which characterize them. We thus obtain the consequences resulting from the
  fundamental principles of thermo-dynamics (which are implied in the
  definitions of energy and entropy) by a process which seems more simple,
  and which lends itself more readily to the solution of problems, than the
  usual method, in which the several parts of a cyclic operation are
  explicitly and separately considered. Although my results were in a large
  measure such as had previously been demonstrated by other methods, yet, as
  I readily obtained those which were to me before unknown, I was confirmed
  in my belief in the suitableness of the method adopted.>

  Gibbs had a visit about 1898 from a young Gilbert Lewis. He told Lewis that
  he was rather lonely at Yale, where few others were actively interested in
  his work.<footnote|Ref. <cite|pitzer-84>.>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bio:gibbs|<tuple|?|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnote-5|<tuple|5|?>>
    <associate|footnote-6|<tuple|6|?>>
    <associate|footnote-7|<tuple|7|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
    <associate|footnr-5|<tuple|5|?>>
    <associate|footnr-6|<tuple|6|?>>
    <associate|footnr-7|<tuple|7|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      wheeler-52

      gibbs-73a

      gibbs-73b

      gibbs-76

      wheeler-52

      pitzer-84
    </associate>
  </collection>
</auxiliary>