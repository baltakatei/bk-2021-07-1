<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <newfont|<covertitle>|cmr15 scaled<magstep>5> <newfont|<coveredition>|cmr12
  scaled<magstep>3> <newfont|<coverauthor>|cmr12 scaled<magstep>5>

  \ \ \ \ \ \ <newcolumntype*|d|1|D..<1>>

  \ <assign|reftextfaceafter|<macro|on the next
  page>><assign|reftextfacebefore|<macro|>><assign|reftextafter|<macro|on the
  next page>><assign|reftextbefore|<macro|on the preceding
  page>><assign|reftextcurrent|<macro|>>

  \ <hypersetup|bookmarks,bookmarksnumbered=true,linktocpage=true,hyperindex=false,pdftitle=Thermodynamics
  and Chemistry,pdfauthor=Howard DeVoe,pdfdisplaydoctitle=true,colorlinks=true,linkcolor=black,citecolor=black,urlcolor=black>

  <assign|I|<macro|1|<index|>>><assign|longpage|<macro|>><assign|shortpage|<macro|>><assign|s|<macro|<smash|[>b]>><assign|vs|<macro|<next-line><vspace*|1.5ex>>><assign|cond|<macro|1|<next-line><vspace*|-2.5pt><with|font-size|0.84|<arg|1>>>><assign|nextcond|<macro|1|<next-line><vspace*|-5pt><with|font-size|0.84|<arg|1>>>><assign|minor|<macro|body|<\surround|
  |<setlength>>
    <arg|body>
  </surround>>><listparindent>par-first } } }

  <item*|><with|font-size|0.84|} <newenvironment|plainlist|<new-line>>>

  <with|font-size|0.84|}} >

  <with|font-size|0.84|<assign|listofbios|<macro|<@starttoc|lob>>>>

  <\with|font-size|0.84>
    <assign|bioname|<macro|1|<addcontentsline|lob|biogr|<arg|1>>>><assign|l@biogr|<macro|<@dottedtocline|1|0em|2.3em>>>

    <newfloat|biography|bio|biocapt> <setfloatlocations|biography|p>

    <assign|biofloat|<macro|body|<\surround| |<setlength>>
      <arg|body>
    </surround>>><fboxsep>8pt <boxedminipage|tex-text-width| } } >
  </with>

  <assign|bio|<macro|1|2|body|<\surround|<paragraphfootnotes>
  <with|par-mode|left|<clubpenalty>=0 <widowpenalty>=0
  \ <assign|the-footnote|<macro|<number|<footnote-nr>|alpha>>>
  <assign|footnoterule|<macro|<kern>-3pt <hrule>height 0.4pt width
  0.2tex-column-width<kern>2.6pt>> <with|par-columns|2|[<bioname|<arg|1>><with|font-size|0.84|<no-indent>BIOGRAPHICAL
  SKETCH<next-line>> <with|font-series|bold|<arg|1> (<arg|2>)>
  <vspace|-.2cm>] >>|>
    <arg|body>
  </surround>>>}

  <plainfootnotes>

  <assign|bioimage|<macro|body|<\surround| |>
    <arg|body>
  </surround>>>minipage[t]6.5cm <with|par-mode|center|<vspace|-.2cm>} >

  <vspace|.2cm>

  <assign|bioquote|<macro|body|<\surround| |<setlength>>
    <arg|body>
  </surround>>><itemindent>0ex } } } }}

  <item*|><with|font-size|0.71|} <assign|tablestrut|<macro|<rule|0in|2.6ex>>><assign|Strut|<macro|1|<rule|0cm|<arg|1>>>><assign|mptbl|<macro|body|<surround|<new-line>||<padded-center|<\surround|<let><with|font-size|1|=>|>
    <arg|body>
  </surround>>>>><assign|minipagetable|<macro|1|body|<surround|<new-line>|<new-line>|<padded-center|<surround||<new-line>|<minipage|f|<arg|1>|<surround|<assign|*|<macro|<footnoterule>>>||<mptbl|<\surround||<vspace|-1ex>>
    <arg|body>
  </surround>>>>>>>>><assign|boxedfigure|<macro|body|<\surround||<setlength>>
    <arg|body>
  </surround>>>>

  <with|font-size|0.71|<fboxsep>0pt <boxedminipage|tex-text-width|
  <vspace|10pt><with|par-mode|center|<no-indent>} <vspace|.3cm>>> >

  <assign|capt|<\macro|1>
    <minipage|f|12cm| <vspace|.3cm> <assign|footnoterule|<macro|<kern>-3pt
    <hrule>height 0.4pt width 0.1tex-column-width<kern>2.6pt>>
    <caption|<arg|1>>>
  </macro>><assign|mpfig|<macro|body|<\padded-center>
    <arg|body>
  </padded-center>>><assign|minipagefig|<macro|body|<padded-center|<surround||<new-line>|<minipage|f|4.7in|<surround|<assign|footnoterule|<macro|<kern>-3pt
  <hrule>height 0.4pt width 0.1tex-column-width<kern>2.6pt>>||<mpfig|<\surround||<vspace|-2ex>>
    <arg|body>
  </surround>>>>>>>><assign|Lower|<macro|1|<smash|<lower>1.5ex
  <arg|1>>>><new-counter|probnum> <new-counter|probpart>[probnum]
  <new-counter|oldchapnum> <new-counter|oldprobnum>

  <Newassociation|ans|Ans|ANS>

  <assign|theproblemnum|<macro|<the-chapter>.<number|<probnum-nr>|arabic>>><assign|theprobpart|<macro|(<number|<probpart-nr>|alpha>)>><assign|Ansparams|<macro|<the-chapter><theprobnum><theprobpart>>><assign|Ans|<macro|1|2|3|body|<\surround>
    <if|<equal|<arg|3>|()>|%|t>hen print with problem number only:

    <item*|<with|font-series|bold|<arg|1>.<arg|2>>>
    <if|<equal|<theoldchapnum>.<theoldprobnum>|<arg|1>.<arg|2>>|%|p>rint with
    part number only:

    <item*|<arg|3>>

    <item*|<with|font-series|bold|<arg|1>.<arg|2>><space|0.5em><arg|3>>
  <|surround|<setcounter>>
    <arg|body>
  </surround>>>oldchapnum<1> <assign|oldprobnum-nr|>}
  <assign|problems|<macro|body|<\surround>
    <markright|Problems> <vspace|.3cm>

    <padded-center|PROBLEMS>

    <\with|font-size|0.84>
      An underlined problem number or problem-part letter indicates that the
      numerical answer appears in Appendix <reference|app:ans>.

      <usecounter|probnum> <itemsep>1ex <leftmargin>2.2em <labelsep>0.5em
      <rightmargin>0in
    </with>
  <|surround|>
    <arg|body>
  </surround>>><assign|problemsNoAns|<macro|body|<\surround>
    <markright|Problems> <vspace|.3cm>

    <padded-center|PROBLEMS>

    <\with|font-size|0.84>
      <usecounter|probnum> <itemsep>1ex <leftmargin>2.2em <labelsep>0.5em
      <rightmargin>0in
    </with>
  <|surround|>
    <arg|body>
  </surround>>><assign|oneproblem|<macro|body|<\surround>
    <markright|Problem> <vspace|.3cm>

    <padded-center|PROBLEM>

    <\with|font-size|0.84>
      <usecounter|probnum> <itemsep>1ex <leftmargin>2.2em <labelsep>0.5em
      <rightmargin>0in
    </with>
  <|surround|>
    <arg|body>
  </surround>>><assign|problempart|<macro|<item>>><assign|problempart*|<macro|1|<item*|<arg|1>>>><assign|problemAns|<\macro>
    <next-counter|probnum>

    <item*|<wide*|<with|font-series|bold|<theproblemnum>>|\<bar\>>>
  </macro>><assign|problempartAns|<\macro>
    <next-counter|probpart>

    <item*|<wide*|<theprobpart>|\<bar\>>>
  </macro>><assign|problemparts|<macro|body|<\list|<theprobpart>|<usecounter|probpart>
  <itemsep>1ex <leftmargin>2.0em <labelsep>0.5em <rightmargin>0em>
    <arg|body>
  </list>>>

  <specialcomment|soln>

  <\padded-left-aligned>
    <\trivlist>
      <item><with|font-series|bold|Solution:><no-page-break><rule*|-1.5ex|0ex|1.5ex><next-line><vspace*|.5ex>
    </trivlist>
  </padded-left-aligned>

  <assign|jn|<macro|<space|.13em><space|.2em>>><assign|ljn|<macro|<space|.13em><space|.2em>>><assign|lljn|<macro|<space|.13em><space|.2em>>><assign|R|<macro|<math|8.3145<units|J*<space|0.17em>K<per><space|0.17em>mol<per>>>>><assign|Rsix|<macro|<math|8.31447<units|J*<space|0.17em>K<per><space|0.17em>mol<per>>>>><assign|reg|<macro|1|<hyperpage|<arg|1>>>><assign|biopage|<macro|1|<hyperpage|<arg|1>>b>><assign|n|<macro|1|<hyperpage|<arg|1>>n>><assign|p|<macro|1|<hyperpage|<arg|1>>p>><assign|ccol|<macro|1|<multicolumn|1|c|<arg|1>>>><assign|doidxbookmark|<\macro|1>
    <assign|@|<macro|Symbols>><assign|@|<macro|<arg|1>>>

    <\with|font-size|1.41|font-series|bold>
      <ifx><@tempa><@tempb>Analphabetics <phantomsection><pdfbookmark*|0|Analphabetics|Analphabetics-idx><else><arg|1><phantomsection><pdfbookmark*|1|<arg|1>|<arg|1>-idx><label|-idx><vspace|1ex>
    </with>

    \ 
  </macro>>
</body>

<initial|<\collection>
</collection>>