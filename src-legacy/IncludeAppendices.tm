<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <chapter|Definitions of the SI Base Units><label|app:SI>

  <pagestyle|nosectioninheader>

  <I|SI!base units\|reg><I|Base units\|reg>

  This appendix gives two definitions for each of the seven SI base units.
  The <em|previous definitions> are from the 2007 IUPAC Green
  Book.<footnote|Ref. <cite|greenbook-3>, Sec. 3.3.> <I|IUPAC Green
  Book\|reg>The <em|revised definitions> are from the SI revision effective
  beginning 20 May 2019.<footnote|Ref. <cite|stock-19>> Values of the
  defining constants referred to in the revised definitions are listed in
  Appendix <reference|app:const>.

  <\plainlist>
    <item>The <I|Second\|reg><newterm|second>, symbol s, is the SI unit of
    time.

    <\itemize>
      <item><with|font-series|bold|Previous definition:> The second is the
      duration of 9<space|0.17em>192<space|0.17em>631<space|0.17em>770
      periods of the radiation corresponding to the transition between the
      two hyperfine levels of the ground state of the cesium-133 atom.

      <item><with|font-series|bold|Revised definition:> No change from the
      previous definition. The number 9<space|0.17em>192<space|0.17em>631<space|0.17em>770
      is the numerical value of the defining constant
      <math|<Del>\<nu\><subs|C*s>> expressed in units of s<per>.
    </itemize>

    <item>The <I|Meter\|reg><newterm|meter>,<footnote|An alternative spelling
    is <I|Metre\|reg><em|metre>.> symbol <math|m>, is the SI unit of length.

    <\itemize>
      <item><with|font-series|bold|Previous definition:> The meter is the
      length of path traveled by light in vacuum during a time interval of
      1/(299<space|0.17em>792<space|0.17em>458) of a second.

      <item><with|font-series|bold|Revised definition:> No change from the
      previous definition. The number 299<space|0.17em>792<space|0.17em>458
      is the numerical value of the defining constant <math|c> expressed in
      units of m<space|0.17em>s<per>.
    </itemize>

    <item>The <I|Kilogram\|reg><newterm|kilogram>, symbol kg, is the SI unit
    of <I|Mass\|reg>mass.

    <\itemize>
      <item><with|font-series|bold|Previous definition:> The kilogram is
      equal to the mass of the international prototype of the kilogram in
      S�vres, France.

      <item><with|font-series|bold|Revised definition:> The kilogram is
      defined using the defining constant <math|h> and the definitions of
      second and meter.
    </itemize>

    <item>The <I|Kelvin (unit)\|reg><newterm|kelvin>, symbol K, is the SI
    unit of <I|Thermodynamic!temperature\|reg><I|Temperature!thermodynamic\|reg>thermodynamic
    temperature.

    <\itemize>
      <item><with|font-series|bold|Previous definition:> The kelvin is the
      fraction 1/<math|273.16> of the thermodynamic temperature of the triple
      point of water.

      <item><with|font-series|bold|Revised definition:> The kelvin is equal
      to the change of thermodynamic temperature <math|T> that results in a
      change of the translational energy <math|<around|(|3/2|)>*k*T> of an
      ideal gas molecule by <math|<around|(|3/2|)>*1.380*<space|0.17em>649<timesten|-23><units|J>>.
      The number 1.380 649<timesten|-23> is the numerical value of the
      defining constant <math|k> expressed in units of J<space|0.17em>K<per>.
    </itemize>

    <item>The <I|Mole\|reg><newterm|mole>, symbol mol, is the SI unit of
    <I|Amount\|reg>amount of substance.

    <\itemize>
      <item><with|font-series|bold|Previous definition:> The mole is the
      amount of substance of a system which contains as many elementary
      entities as there are atoms in <math|0.012> kilogram of carbon 12.

      <item><with|font-series|bold|Revised definition:> One mole contains
      exactly 6.022 140 76<timesten|23> elementary entities. This number is
      the numerical value of the defining constant <math|N<subs|A>> expressed
      in the unit mol<per>.
    </itemize>

    <item>The <I|Ampere\|reg><newterm|ampere>, symbol A, is the SI unit of
    electric current. <I|Electric!current\|reg><I|Current, electric\|reg>

    <\itemize>
      <item><with|font-series|bold|Previous definition:> The ampere is that
      constant current which, if maintained in two straight parallel
      conductors of infinite length, of negligible circular cross-section,
      and placed 1 meter apart in vacuum, would produce between these
      conductors a force equal to <math|2<timesten|-7>> newton per meter of
      length.

      <item><with|font-series|bold|Revised definition:> The ampere is defined
      as the electric current in which<next-line>1/(1.602 176
      634<timesten|-19>) elementary charges travel across a given point in
      one second. The number 1.602 176 634<timesten|-19> is the numerical
      value of the defining constant <math|e> expressed in coulombs.
    </itemize>

    <item>The <I|Candela\|reg><newterm|candela>, symbol cd, is the SI unit of
    luminous intensity.

    <\itemize>
      <item><with|font-series|bold|Previous definition:> The candela is the
      luminous intensity, in a given direction, of a source that emits
      monochromatic radiation of frequency 540<timesten|12><units|s<per>> and
      that has a radiant intensity in that direction of
      (1/683)<units|m<rsup|<math|2>> kg s<rsup|<math|-3>>> per steradian.

      <item><with|font-series|bold|Revised definition:> No change from the
      previous definition. The meter, kilogram, and second in this definition
      are defined in terms of the defining constants <math|c>, <math|h>, and
      <math|<Del>\<nu\><subs|C*s>>.
    </itemize>
  </plainlist>

  <chapter|Physical Constants><label|app:const>

  <pagestyle|nosectioninheader>

  <I|Physical constants, values of\|reg><I|Constants, values of\|reg>The
  following table lists values of fundamental physical constants used to
  define SI base units or needed in thermodynamic calculations. The 2019 SI
  revision treats the first six constants (<math|<Del>\<nu\><subs|C*s>>
  through <math|N<subs|A>>) as <em|defining constants> or <em|fundamental
  constants> whose values are exact by definition.

  <\comment>
    \ 2010 CODATA values are the most recent as of Jan2015
  </comment>

  <\minipagetable|13.1cm>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-valign|top>|<cwith|2|2|1|-1|cell-vmode|exact>|<cwith|2|2|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|5|5|1|-1|cell-valign|top>|<cwith|5|5|1|-1|cell-vmode|exact>|<cwith|5|5|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|6|6|1|-1|cell-valign|top>|<cwith|6|6|1|-1|cell-vmode|exact>|<cwith|6|6|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|7|7|1|-1|cell-valign|top>|<cwith|7|7|1|-1|cell-vmode|exact>|<cwith|7|7|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|8|8|1|-1|cell-valign|top>|<cwith|8|8|1|-1|cell-vmode|exact>|<cwith|8|8|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|9|9|1|-1|cell-valign|top>|<cwith|9|9|1|-1|cell-vmode|exact>|<cwith|9|9|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|10|10|1|-1|cell-valign|top>|<cwith|10|10|1|-1|cell-vmode|exact>|<cwith|10|10|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|11|11|1|-1|cell-valign|top>|<cwith|11|11|1|-1|cell-vmode|exact>|<cwith|11|11|1|-1|cell-height|<plus|1fn|1.5ex>>|<cwith|11|11|1|-1|cell-bborder|1ln>|<table|<row|<cell|Constant>|<cell|Symbol>|<cell|Value
    in SI units>>|<row|<cell|cesium-133 hyperfine transition
    frequency>|<cell|<math|<Del>\<nu\><subs|C*s>>>|<cell|9.192 631
    770<timesten|9><units|s<per>> <tablestrut>>>|<row|<cell|speed of light in
    vacuum>|<cell|<math|c>>|<cell|2.997 924
    58<math|<timesten|8><units|m*<space|0.17em>s<per>>>>>|<row|<cell|Planck
    constant>|<cell|<math|h>>|<cell|6.626 070
    15<timesten|-34><units|J<space|0.17em>s>>>|<row|<cell|elementary
    charge>|<cell|<math|e>>|<cell|1.602 176
    634<timesten|-19><units|C>>>|<row|<cell|Boltzmann
    constant>|<cell|<math|k>>|<cell|1.380
    649<timesten|-23><units|J<space|0.17em>K<per>>>>|<row|<cell|<I|Avogadro
    constant\|reg>Avogadro constant>|<cell|<math|N<subs|A>>>|<cell|6.022 140
    76<timesten|23><units|mol<per>>>>|<row|<cell|<I|Gas constant\|reg>gas
    constant<space|.15em><footnote|or molar gas constant; <math|R> is equal
    to <math|N<subs|A>k>>>|<cell|<math|R>>|<cell|8.314 462
    ...<units|J<space|0.17em>K<per><space|0.17em>mol<per>>>>|<row|<cell|<I|Faraday
    constant\|reg>Faraday constant<space|.15em><footnote|<math|F> is equal to
    <math|N<subs|A>e>>>|<cell|<math|F>>|<cell|9.648 533
    ...<timesten|4><units|C<space|0.17em>mol<per>>>>|<row|<cell|electric
    constant<space|.15em><footnote|or permittivity of vacuum;
    <math|\<epsilon\><rsub|0>> is equal to
    <math|10<rsup|-7>/<around|(|4*\<pi\>*c<rsup|2>|)>>>>|<cell|<math|\<epsilon\><rsub|0>>>|<cell|8.854
    187 ...<timesten|-12><units|C<rsup|<math|2>><space|0.17em>J<per><space|0.17em>m<per>>>>|<row|<cell|standard
    acceleration of free fall<space|.15em><footnote|or standard acceleration
    of gravity>>|<cell|<math|g<subs|n>>>|<cell|9.806
    65<units|m<space|0.17em>s<rsup|<math|-2>>>>>|<row|<cell|>|<cell|>|<cell|>>>>>
  </minipagetable>

  <chapter|Symbols for Physical Quantities><label|app:sym>

  <pagestyle|nosectioninheader>

  <I|Symbols for physical quantities\|(><I|Physical quantities, symbols
  for\|(>This appendix lists the symbols for most of the variable physical
  quantities used in this book. The symbols are those recommended in the
  IUPAC Green Book (Ref. <cite|greenbook-2>) except for quantities followed
  by an asterisk (<rsup|<math|\<ast\>>>).

  <\longtable|c|lll@>
    <tformat|<table|<row|<cell|<hline> <multicolumn|1|@l|Symbol>>|<cell|Physical
    quantity>|<cell|SI unit <tablestrut>>>|<row|<cell|<vspace*|0.5ex>
    <hline>>|<cell|>|<cell|>>|<row|<cell|<vspace*|-1.5ex> <endfirsthead>
    <multicolumn|3|@l|<with|font-size|0.84|(continued from previous
    page)>>>|<cell|>|<cell|>>|<row|<cell|<vspace*|0.5ex> <hline>
    <multicolumn|1|@l|Symbol>>|<cell|Physical quantity>|<cell|SI unit
    <tablestrut>>>|<row|<cell|<vspace*|0.5ex>
    <hline>>|<cell|>|<cell|>>|<row|<cell|<vspace*|-1.5ex>
    <endhead>>|<cell|>|<cell|>>|<row|<cell|<vspace*|-3.4mm> <hline> <endfoot>
    <multicolumn|2|@l|<em|Roman letters>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|A>>|<cell|Helmholtz
    energy>|<cell|J>>|<row|<cell|<math|A<subs|r>>>|<cell|relative atomic mass
    (atomic weight)>|<cell|(dimensionless)>>|<row|<cell|<math|<As>>>|<cell|surface
    area>|<cell|m<rsup|<math|2>>>>|<row|<cell|<math|a>>|<cell|activity>|<cell|(dimensionless)>>|<row|<cell|<math|B>>|<cell|second
    virial coefficient>|<cell|m<rsup|<math|3>><space|0.17em>mol<per>>>|<row|<cell|<math|C>>|<cell|number
    of components<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|C<rsub|p>>>|<cell|heat
    capacity at constant pressure>|<cell|J<space|0.17em>K<per>>>|<row|<cell|<math|C<rsub|V>>>|<cell|heat
    capacity at constant volume>|<cell|J<space|0.17em>K<per>>>|<row|<cell|<math|c>>|<cell|concentration>|<cell|mol<space|0.17em>m<rsup|<math|-3>>>>|<row|<cell|<math|E>>|<cell|energy>|<cell|J>>|<row|<cell|<no-page-break>>|<cell|electrode
    potential>|<cell|V>>|<row|<cell|<math|\<b-E\>>>|<cell|electric field
    strength>|<cell|V<space|0.17em>m<per>>>|<row|<cell|<math|E>>|<cell|cell
    potential>|<cell|V>>|<row|<cell|<math|<Ej>>>|<cell|liquid junction
    potential>|<cell|V>>|<row|<cell|<math|E<sys>>>|<cell|system energy in a
    lab frame>|<cell|J>>|<row|<cell|<math|F>>|<cell|force>|<cell|N>>|<row|<cell|<no-page-break>>|<cell|number
    of degrees of freedom<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|<fug>>>|<cell|fugacity>|<cell|Pa>>|<row|<cell|<math|g>>|<cell|acceleration
    of free fall>|<cell|m<space|0.17em>s<rsup|<math|-2>>>>|<row|<cell|<math|G>>|<cell|Gibbs
    energy>|<cell|J>>|<row|<cell|<math|h>>|<cell|height,
    elevation>|<cell|m>>|<row|<cell|<math|H>>|<cell|enthalpy>|<cell|J>>|<row|<cell|<math|\<b-H\>>>|<cell|magnetic
    field strength>|<cell|A<space|0.17em>m<per>>>|<row|<cell|<math|I>>|<cell|electric
    current>|<cell|A>>|<row|<cell|<math|I<rsub|m>>>|<cell|ionic strength,
    molality basis>|<cell|mol<space|0.17em>kg<per>>>|<row|<cell|<math|I<rsub|c>>>|<cell|ionic
    strength, concentration basis>|<cell|mol<space|0.17em>m<rsup|<math|-3>>>>|<row|<cell|<math|K>>|<cell|thermodynamic
    equilibrium constant>|<cell|(dimensionless)>>|<row|<cell|<math|K<subs|a>>>|<cell|acid
    dissociation constant>|<cell|(dimensionless)>>|<row|<cell|<math|K<rsub|p>>>|<cell|equilibrium
    constant, pressure basis>|<cell|Pa<rsup|<math|<big|sum>\<nu\>>>>>|<row|<cell|<math|K<subs|s>>>|<cell|solubility
    product>|<cell|(dimensionless)>>|<row|<cell|<math|<kHi>>>|<cell|Henry's
    law constant of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>mole
    fraction basis>|<cell|Pa>>|<row|<cell|<math|k<rsub|c,i>>>|<cell|Henry's
    law constant of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>concentration
    basis<rsup|<math|\<ast\>>>>|<cell|Pa<space|0.17em>m<rsup|<math|3>><space|0.17em>mol<per>>>|<row|<cell|<math|k<rsub|m,i>>>|<cell|Henry's
    law constant of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>molality
    basis<rsup|<math|\<ast\>>>>|<cell|Pa<space|0.17em>kg<space|0.17em>mol<per>>>|<row|<cell|<math|l>>|<cell|length,
    distance>|<cell|m>>|<row|<cell|<math|L>>|<cell|relative partial molar
    enthalpy<rsup|<math|\<ast\>>>>|<cell|J<space|0.17em>mol<per>>>|<row|<cell|<math|M>>|<cell|molar
    mass>|<cell|kg<space|0.17em>mol<per>>>|<row|<cell|<math|\<b-M\>>>|<cell|magnetization>|<cell|A<space|0.17em>m<per>>>|<row|<cell|<math|M<subs|r>>>|<cell|relative
    molecular mass (molecular weight)>|<cell|(dimensionless)>>|<row|<cell|<math|m>>|<cell|mass>|<cell|kg>>|<row|<cell|<math|m<rsub|i>>>|<cell|molality
    of species <math|i>>|<cell|mol<space|0.17em>kg<per>>>|<row|<cell|<math|N>>|<cell|number
    of entities (molecules, atoms, ions,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>formula
    units, etc.)>|<cell|(dimensionless)>>|<row|<cell|<math|n>>|<cell|amount
    of substance>|<cell|mol>>|<row|<cell|<math|P>>|<cell|number of
    phases<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|p>>|<cell|pressure>|<cell|Pa>>|<row|<cell|<no-page-break>>|<cell|partial
    pressure>|<cell|Pa>>|<row|<cell|<math|\<b-P\>>>|<cell|dielectric
    polarization>|<cell|C<space|0.17em>m<rsup|<math|-2>>>>|<row|<cell|<math|Q>>|<cell|electric
    charge>|<cell|C>>|<row|<cell|<math|Q<sys>>>|<cell|charge entering system
    at right conductor<rsup|<math|\<ast\>>>>|<cell|C>>|<row|<cell|<math|Q<subs|r*x*n>>>|<cell|reaction
    quotient<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|q>>|<cell|heat>|<cell|J>>|<row|<cell|<math|R<el>>>|<cell|electric
    resistance<rsup|<math|\<ast\>>>>|<cell|<math|<upOmega>>>>|<row|<cell|<math|S>>|<cell|entropy>|<cell|J<space|0.17em>K<per>>>|<row|<cell|<math|s>>|<cell|solubility>|<cell|mol<space|0.17em>m<rsup|<math|-3>>>>|<row|<cell|<no-page-break>>|<cell|number
    of species<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|T>>|<cell|thermodynamic
    temperature>|<cell|K>>|<row|<cell|<math|t>>|<cell|time>|<cell|s>>|<row|<cell|<no-page-break>>|<cell|Celsius
    temperature>|<cell|<math|<degC>>>>|<row|<cell|<math|U>>|<cell|internal
    energy>|<cell|J>>|<row|<cell|<math|V>>|<cell|volume>|<cell|m<rsup|<math|3>>>>|<row|<cell|<math|v>>|<cell|specific
    volume>|<cell|m<rsup|<math|3>> kg<per>>>|<row|<cell|>|<cell|velocity,
    speed>|<cell|m<space|0.17em>s<per>>>|<row|<cell|<math|w>>|<cell|work>|<cell|J>>|<row|<cell|>|<cell|mass
    fraction (weight fraction)>|<cell|(dimensionless)>>|<row|<cell|<math|w<el>>>|<cell|electrical
    work<rsup|<math|\<ast\>>>>|<cell|J>>|<row|<cell|<math|w<rprime|'>>>|<cell|nonexpansion
    work<rsup|<math|\<ast\>>>>|<cell|J>>|<row|<cell|<math|x>>|<cell|mole
    fraction in a phase>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|Cartesian
    space coordinate>|<cell|m>>|<row|<cell|<math|y>>|<cell|mole fraction in
    gas phase>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|Cartesian
    space coordinate>|<cell|m>>|<row|<cell|<math|Z>>|<cell|compression factor
    (compressibility factor)>|<cell|(dimensionless)>>|<row|<cell|<math|z>>|<cell|mole
    fraction in multiphase system<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|charge
    number of an ion>|<cell|(dimensionless)>>|<row|<cell|>|<cell|electron
    number of cell reaction>|<cell|(dimensionless)>>|<row|<cell|>|<cell|Cartesian
    space coordinate>|<cell|m>>|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|<multicolumn|2|@l|<em|Greek
    letters>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><multicolumn|1|@l|<em|alpha>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<alpha\>>>|<cell|degree
    of reaction, dissociation, etc.>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|cubic
    expansion coefficient>|<cell|K<per>>>|<row|<cell|<multicolumn|1|@l|<em|gamma>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|<g>>>|<cell|surface
    tension>|<cell|N<space|0.17em>m<per>,
    J<space|0.17em>m<rsup|<math|-2>>>>|<row|<cell|<math|<g><rsub|i>>>|<cell|activity
    coefficient of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>pure
    liquid or solid standard state<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|<g><rsub|m,i>>>|<cell|activity
    coefficient of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>molality
    basis>|<cell|(dimensionless)>>|<row|<cell|<math|<g><rsub|c,i>>>|<cell|activity
    coefficient of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>concentration
    basis>|<cell|(dimensionless)>>|<row|<cell|<math|<g><rsub|x,i>>>|<cell|activity
    coefficient of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>mole
    fraction basis>|<cell|(dimensionless)>>|<row|<cell|<math|<g><rsub|\<pm\>>>>|<cell|mean
    ionic activity coefficient>|<cell|(dimensionless)>>|<row|<cell|<math|<G>>>|<cell|pressure
    factor (activity of a reference state)<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<multicolumn|1|@l|<em|epsilon>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<epsilon\>>>|<cell|efficiency
    of a heat engine>|<cell|(dimensionless)>>|<row|<cell|>|<cell|energy
    equivalent of a calorimeter<rsup|<math|\<ast\>>>>|<cell|J<space|0.17em>K<per>>>|<row|<cell|<multicolumn|1|@l|<em|theta>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<vartheta\>>>|<cell|angle
    of rotation>|<cell|(dimensionless)>>|<row|<cell|<multicolumn|1|@l|<em|kappa>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<kappa\>>>|<cell|reciprocal
    radius of ionic atmosphere>|<cell|m<per>>>|<row|<cell|<math|\<kappa\><rsub|T>>>|<cell|isothermal
    compressibility>|<cell|Pa<per>>>|<row|<cell|<multicolumn|1|@l|<em|mu>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<mu\>>>|<cell|chemical
    potential>|<cell|J<space|0.17em>mol<per>>>|<row|<cell|<math|\<mu\><subs|J*T>>>|<cell|Joule--Thomson
    coefficient>|<cell|K<space|0.17em>Pa<per>>>|<row|<cell|<multicolumn|1|@l|<em|nu>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<nu\>>>|<cell|number
    of ions per formula unit>|<cell|(dimensionless)>>|<row|<cell|>|<cell|stoichiometric
    number>|<cell|(dimensionless)>>|<row|<cell|<math|\<nu\><rsub|+>>>|<cell|number
    of cations per formula unit>|<cell|(dimensionless)>>|<row|<cell|<math|\<nu\><rsub|->>>|<cell|number
    of anions per formula unit>|<cell|(dimensionless)>>|<row|<cell|<multicolumn|1|@l|<em|xi>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<xi\>>>|<cell|advancement
    (extent of reaction)>|<cell|mol>>|<row|<cell|<multicolumn|1|@l|<em|pi>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|<varPi>>>|<cell|osmotic
    pressure>|<cell|Pa>>|<row|<cell|<multicolumn|1|@l|<em|rho>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<rho\>>>|<cell|density>|<cell|kg<space|0.17em>m<rsup|<math|-3>>>>|<row|<cell|<multicolumn|1|@l|<em|tau>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<tau\>>>|<cell|torque<rsup|<math|\<ast\>>>>|<cell|J>>|<row|<cell|<multicolumn|1|@l|<em|phi>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<phi\>>>|<cell|fugacity
    coefficient>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|electric
    potential>|<cell|V>>|<row|<cell|<math|<Del>\<phi\>>>|<cell|electric
    potential difference>|<cell|V>>|<row|<cell|<math|\<phi\><rsub|m>>>|<cell|osmotic
    coefficient, molality basis>|<cell|(dimensionless)>>|<row|<cell|<math|<varPhi><rsub|L>>>|<cell|relative
    apparent molar enthalpy of solute<rsup|<math|\<ast\>>>>|<cell|J<space|0.17em>mol<per>>>|<row|<cell|<multicolumn|1|@l|<em|omega>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<omega\>>>|<cell|angular
    velocity>|<cell|s<per>>>|<row|<cell|>|<cell|>|<cell|>>>>
  </longtable>

  <I|Symbols for physical quantities\|)><I|Physical quantities, symbols
  for\|)>

  <chapter|Miscellaneous Abbreviations and Symbols><label|app:abbrev>

  <pagestyle|nosectioninheader>

  <section|Physical States>

  These abbreviations for <I|Physical state!symbols
  for\|reg><I|State!physical\|reg><I|State!aggregation@of
  aggregation\|reg>physical states (states of aggregation) may be appended in
  parentheses to chemical formulas or used as superscripts to symbols for
  physical quantities. All but \Pmixt\Q are listed in the IUPAC Green Book
  (Ref. <cite|greenbook-3>, p. 54).

  <\padded-center>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<table|<row|<cell|g>|<cell|gas
    or vapor>>|<row|<cell|l>|<cell|liquid>>|<row|<cell|f>|<cell|fluid (gas or
    liquid)>>|<row|<cell|s>|<cell|solid>>|<row|<cell|cd>|<cell|condensed
    phase (liquid or solid)>>|<row|<cell|cr>|<cell|crystalline>>|<row|<cell|mixt>|<cell|mixture>>|<row|<cell|sln>|<cell|solution>>|<row|<cell|aq>|<cell|aqueous
    solution>>|<row|<cell|aq<math|,\<infty\>>>|<cell|aqueous solution at
    infinite dilution>>>>>
  </padded-center>

  <new-page>

  <section|Subscripts for Chemical Processes><label|app:abbrev-processes>

  <I|Subscripts for chemical processes\|reg><I|Process!chemical!subscript
  for\|reg><I|Chemical process!subscript for\|reg>These abbreviations are
  used as subscripts to the <math|<Del>> symbol. They are listed in the IUPAC
  Green Book (Ref. <cite|greenbook-3>, p. 59\U60).

  The combination <math|<Delsub|p>>, where \Pp\Q is any one of the
  abbreviations below, can be interpreted as an operator:
  <math|<Delsub|p><defn>\<partial\>/\<partial\>*\<xi\><subs|p>> where
  <math|\<xi\><subs|p>> is the advancement of the given process at constant
  temperature and pressure. For example, <math|<Delsub|c>H=<pd|H|\<xi\><subs|c>|T,p>>
  is the molar differential enthalpy of combustion.

  <\padded-center>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<table|<row|<cell|vap>|<cell|vaporization,
    evaporation (l <math|<ra>> g)>>|<row|<cell|sub>|<cell|sublimation (s
    <math|<ra>> g)>>|<row|<cell|fus>|<cell|melting, fusion (s <math|<ra>>
    l)>>|<row|<cell|trs>|<cell|transition between two
    phases>>|<row|<cell|mix>|<cell|mixing of
    fluids>>|<row|<cell|sol>|<cell|solution of a solute in
    solvent>>|<row|<cell|dil>|<cell|dilution of a
    solution>>|<row|<cell|ads>|<cell|adsorption>>|<row|<cell|dpl>|<cell|displacement>>|<row|<cell|imm>|<cell|immersion>>|<row|<cell|r>|<cell|reaction
    in general>>|<row|<cell|at>|<cell|atomization>>|<row|<cell|c>|<cell|combustion
    reaction>>|<row|<cell|f>|<cell|formation reaction>>>>>
  </padded-center>

  <new-page>

  <section|Superscripts>

  <I|Superscripts\|reg>These abbreviations and symbols are used as
  superscripts to symbols for physical quantities. All but <math|<rprime|'>>,
  int, and ref are listed as recommended superscripts in the IUPAC Green Book
  (Ref. <cite|greenbook-3>, p. 60).

  <\padded-center>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<table|<row|<cell|<st>>|<cell|standard>>|<row|<cell|<rsup|<math|\<ast\>>>>|<cell|pure
    substance>>|<row|<cell|<math|<rprime|'>>>|<cell|Legendre transform of a
    thermodynamic potential>>|<row|<cell|<math|\<infty\>>>|<cell|infinite
    dilution>>|<row|<cell|id>|<cell|ideal>>|<row|<cell|int>|<cell|integral>>|<row|<cell|<with|font-family|ss|E>>|<cell|excess
    quantity>>|<row|<cell|ref>|<cell|reference state>>>>>
  </padded-center>

  <chapter|Calculus Review><label|app:calc>

  <pagestyle|nosectioninheader>

  <section|Derivatives>

  Let <math|f> be a function of the variable <math|x>, and let <math|<Del>f>
  be the change in <math|f> when <math|x> changes by <math|<Del>x>. Then the
  <I|Derivative\|reg><newterm|derivative> <math|<df>/<dx>> is the ratio
  <math|<Del>f/<Del>x> in the limit as <math|<Del>x> approaches zero. The
  derivative <math|<df>/<dx>> can also be described as the rate at which
  <math|f> changes with <math|x>, and as the slope of a curve of <math|f>
  plotted as a function of <math|x>.

  The following is a short list of formulas likely to be needed. In these
  formulas, <math|u> and <math|v> are arbitrary functions of <math|x>, and
  <math|a> is a constant. <I|Derivative!formulas\|reg>

  <\align*>
    <tformat|<table|<row|<cell|>|<cell|<frac|<dif><around|(|u<rsup|a>|)>|<dx>>=a*u<rsup|a-1>*<frac|<dif>u|<dx>>>>|<row|<cell|>|<cell|<frac|<dif><around|(|u*v|)>|<dx>>=u*<frac|<dif>v|<dx>>+v<frac|<dif>u|<dx>>>>|<row|<cell|>|<cell|<frac|<dif><around|(|u/v|)>|<dx>>=<around*|(|<frac|1|v<rsup|2>>|)>*<around*|(|v<frac|<dif>u|<dx>>-u*<frac|<dif>v|<dx>>|)>>>|<row|<cell|>|<cell|<frac|<dif>ln
    <around|(|a*x|)>|<dx>>=<frac|1|x>>>|<row|<cell|>|<cell|<frac|<dif><around|(|e<rsup|a*x>|)>|<dx>>=a*e<rsup|a*x>>>|<row|<cell|>|<cell|<frac|<df><around|(|u|)>|<dx>>=<frac|<df><around|(|u|)>|<dif>u>\<cdot\><frac|<dif>u|<dx>>>>>>
  </align*>

  <section|Partial Derivatives>

  If <math|f> is a function of the independent variables <math|x>, <math|y>,
  and <math|z>, the <I|Partial derivative\|reg><newterm|partial derivative>
  <math|<pd|f|x|y,z>> is the derivative <math|<df>/<dx>> with <math|y> and
  <math|z> held constant. It is important in thermodynamics to indicate the
  variables that are held constant, as <math|<pd|f|x|y,z>> is not necessarily
  equal to <math|<pd|f|x|a,b>> where <math|a> and <math|b> are variables
  different from <math|y> and <math|z>.

  The variables shown at the bottom of a partial derivative should tell you
  which variables are being used as the independent variables. For example,
  if the partial derivative is <math|<D><Pd|f|y|a,b>> then <math|f> is being
  treated as a function of <math|y>, <math|a>, and <math|b>.

  <new-page>

  <section|Integrals>

  Let <math|f> be a function of the variable <math|x>. Imagine the range of
  <math|x> between the limits <math|x<rprime|'>> and <math|x<rprime|''>> to
  be divided into many small increments of size
  <math|<Del>x<rsub|i>*<around|(|i=1,2,\<ldots\>|)>>. Let <math|f<rsub|i>> be
  the value of <math|f> when <math|x> is in the middle of the range of the
  <math|i>th increment. Then the <I|Integral\|reg><newterm|integral>

  <\equation*>
    <big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><space|-0.17em><space|-0.17em>f<dx>
  </equation*>

  is the sum <math|<big|sum><rsub|i>f<rsub|i><Del>x<rsub|i>> in the limit as
  each <math|<Del>x<rsub|i>> approaches zero and the number of terms in the
  sum approaches infinity. The integral is also the area under a curve of
  <math|f> plotted as a function of <math|x>, measured from
  <math|x=x<rprime|'>> to <math|x=x<rprime|''>>. The function <math|f> is the
  <I|Integrand\|reg><newterm|integrand>, which is integrated over the
  integration variable <math|x>.

  This book uses the following integrals: <I|Integral!formulas\|reg>

  <\align*>
    <tformat|<table|<row|<cell|>|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><space|-0.17em><space|-0.17em><dx>=x<rprime|''>-x<rprime|'>>>|<row|<cell|>|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><frac|<dx>|x>=ln
    <around*|\||<frac|x<rprime|''>|x<rprime|'>>|\|>>>|<row|<cell|>|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><space|-0.17em><space|-0.17em>x<rsup|a><dx>=<frac|1|a+1>*<around*|[|<around|(|x<rprime|''>|)><rsup|a+1>-<around|(|x<rprime|'>|)><rsup|a+1>|]><space|2em><tx|<around|(|<math|a>
    is a constant other than<math|-1>|)>>>>|<row|<cell|>|<cell|<big|int><rsub|x*'><rsup|x*'*'><space|-0.17em><space|-0.17em><frac|<dx>|a*x+b>=<frac|1|a>*ln
    <around*|\||<frac|a*x*'*'+b|a*x*'+b>|\|><space|2em><tx|<around|(|<math|a>
    is a constant|)>>>>>>
  </align*>

  Here are examples of the use of the expression for the third integral with
  <math|a> set equal to <math|1> and to <math|-2>:

  <\align*>
    <tformat|<table|<row|<cell|>|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><space|-0.17em><space|-0.17em>x<dx>=<frac|1|2>*<around*|[|<around|(|x<rprime|''>|)><rsup|2>-<around|(|x<rprime|'>|)><rsup|2>|]>>>|<row|<cell|>|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><space|-0.17em><frac|<dx>|x<rsup|2>>=-<around*|(|<frac|1|x<rprime|''>>-<frac|1|x<rprime|'>>|)>>>>>
  </align*>

  <section|Line Integrals><label|app-line integrals>

  A <I|Line integral\|reg><I|Integral!line\|reg><newterm|line integral> is an
  integral with an implicit single integration variable that constraints the
  integration to a path.

  The most frequently-seen line integral in this book,
  <math|<big|int><space|-0.17em>p<dif>V>, will serve as an example. The
  integral can be evaluated in three different ways:

  <\enumerate>
    <item>The integrand <math|p> can be expressed as a function of the
    integration variable <math|V>, so that there is only one variable. For
    example, if <math|p> equals <math|c/V> where <math|c> is a constant, the
    line integral is given by <math|<big|int><space|-0.17em>p<dif>V=c*<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>><around|(|1/V|)><dif>V=c*ln
    <around|(|V<rsub|2>/V<rsub|1>|)>>.

    <item>If <math|p> and <math|V> can be written as functions of another
    variable, such as time, that coordinates their values so that they follow
    the desired path, this new variable becomes the integration variable.

    <item>The desired path can be drawn as a curve on a plot of <math|p>
    versus <math|V>; then <math|<big|int><space|-0.17em>p<dif>V> is equal in
    value to the area under the curve.
  </enumerate>

  <chapter|Mathematical Properties of State Functions><label|app:state>

  <pagestyle|nosectioninheader>

  A state function is a property of a thermodynamic system whose value at any
  given instant depends only on the state of the system at that instant (Sec.
  <reference|2-state of the system>).

  <section|Differentials>

  The <I|Differential\|reg><newterm|differential> <math|<df>> of a state
  function <math|f> is an infinitesimal change of <math|f>. Since the value
  of a state function by definition depends only on the state of the system,
  integrating <math|<df>> between an initial state <math|1> and a final state
  <math|2> yields the change in <math|f>, and this change is independent of
  the path:

  <\equation>
    <big|int><rsub|f<rsub|1>><rsup|f<rsub|2>><space|-0.17em><df>=f<rsub|2>-f<rsub|1>=<Del>f
  </equation>

  A differential with this property is called an
  <I|Differential!exact\|reg><I|Exact differential\|reg><em|exact>
  differential. The differential of a state function is always exact.

  <section|Total Differential><label|app:total diff>

  A state function <math|f> treated as a dependent variable is a function of
  a certain number of independent variables that are also state functions.
  The <I|Differential!total\|reg><I|Total differential\|reg><newterm|total
  differential> of <math|f> is <math|<df>> expressed in terms of the
  differentials of the independent variables and has the form

  <\equation>
    <label|df=(df/dx)dx+.+.+...><df>=<Pd|f|x|><dx>+<Pd|f|y|><dif>y+<Pd|f|z|><dif>z+\<ldots\>
  </equation>

  There are as many terms in the expression on the right side as there are
  independent variables. Each partial derivative in the expression has all
  independent variables held constant except the variable shown in the
  denominator.

  Figure <reference|fig:F-total differential><vpageref|fig:F-total
  differential> interprets this expression for a function <math|f> of the two
  independent variables <math|x> and <math|y>. The shaded plane represents a
  small element of the surface <math|f=f*<around|(|x,y|)>>.

  <\big-figure>
    <boxedfigure|<image|./BACK-SUP/totaldif.eps||||><capt|<label|fig:F-total
    differential>>>
  </big-figure|>

  Consider a system with three independent variables. If we choose these
  independent variables to be <math|x>, <math|y>, and <math|z>, the total
  differential of the dependent state function <math|f> takes the form

  <\equation>
    <label|df=adx+bdy+cdz><df>=a<dx>+b<dif>y+c<dif>z
  </equation>

  where we can identify the coefficients as

  <\equation>
    <label|a=(df/dx)_y,z etc.>a=<Pd|f|x|y,z><space|2em>b=<Pd|f|y|x,z><space|2em>c=<Pd|f|z|x,y>
  </equation>

  These coefficients are themselves, in general, functions of the independent
  variables and may be differentiated to give mixed second partial
  derivatives; for example:

  <\equation>
    <Pd|a|y|x,z>=<frac|\<partial\><rsup|2>*<space|-.08cm>f|\<partial\>*y*\<partial\>*x><space|2em><Pd|b|x|y,z>=<frac|\<partial\><rsup|2>*<space|-.08cm>f|\<partial\>*x*\<partial\>*y>
  </equation>

  The second partial derivative <math|\<partial\><rsup|2>*<space|-.08cm>f/\<partial\>*y*\<partial\>*x>,
  for instance, is the partial derivative with respect to <math|y> of the
  partial derivative of <math|f> with respect to <math|x>. It is a theorem of
  calculus that if a function <math|f> is single valued and has continuous
  derivatives, the order of differentiation in a mixed derivative is
  immaterial. Therefore the mixed derivatives
  <math|\<partial\><rsup|2>*<space|-.08cm>f/\<partial\>*y*\<partial\>*x> and
  <math|\<partial\><rsup|2>*<space|-.08cm>f/\<partial\>*x*\<partial\>*y>,
  evaluated for the system in any given state, are equal:

  <\equation>
    <Pd|a|y|x,z>=<Pd|b|x|y,z>
  </equation>

  The general relation that applies to a function of any number of
  independent variables is

  <\equation>
    <Pd|X|y|>=<Pd|Y|x|>
  </equation>

  where <math|x> and <math|y> are <em|any> two of the independent variables,
  <math|X> is <math|\<partial\>*<space|-.04cm>f/\<partial\>*x>, <math|Y> is
  <math|\<partial\>*<space|-.04cm>f/\<partial\>*y>, and each partial
  derivative has all independent variables held constant except the variable
  shown in the denominator. This general relation is the <I|Euler reciprocity
  relation\|reg>Euler reciprocity relation, or <I|Reciprocity
  relation\|reg><newterm|reciprocity relation> for short. A necessary and
  sufficient condition for <math|<df>> to be an exact differential is that
  the reciprocity relation is satisfied for each pair of independent
  variables.

  <section|Integration of a Total Differential><label|app:int of tot diff>

  If the coefficients of the total differential of a dependent variable are
  known as functions of the independent variables, the expression for the
  total differential may be integrated to obtain an expression for the
  dependent variable as a function of the independent variables.

  For example, suppose the total differential of the state function
  <math|f*<around|(|x,y,z|)>> is given by Eq. <reference|df=adx+bdy+cdz> and
  the coefficients are known functions <math|a<around|(|x,y,z|)>>,
  <math|b*<around|(|x,y,z|)>>, and <math|c*<around|(|x,y,z|)>>. Because
  <math|f> is a state function, its change between
  <math|f*<around|(|0,0,0|)>> and <math|f*<around|(|x<rprime|'>,y<rprime|'>,z<rprime|'>|)>>
  is independent of the integration path taken between these two states. A
  convenient path would be one with the following three segments:

  <\enumerate>
    <item>integration from <math|<around|(|0,0,0|)>> to
    <math|<around|(|x<rprime|'>,0,0|)>>: <math|<big|int><rsub|0><rsup|x<rprime|'>><space|-0.17em>a<around|(|x,0,0|)><dx>>

    <item>integration from <math|<around|(|x<rprime|'>,0,0|)>> to
    <math|<around|(|x<rprime|'>,y<rprime|'>,0|)>>:
    <math|<big|int><rsub|0><rsup|y<rprime|'>><space|-0.17em>b*<around|(|x<rprime|'>,y,0|)><dif>y>

    <item>integration from <math|<around|(|x<rprime|'>,y<rprime|'>,0|)>> to
    <math|<around|(|x<rprime|'>,y<rprime|'>,z<rprime|'>|)>>:
    <math|<big|int><rsub|0><rsup|z<rprime|'>><space|-0.17em>c*<around|(|x<rprime|'>,y<rprime|'>,z|)><dif>z>
  </enumerate>

  The expression for <math|f*<around|(|x,y,z|)>> is then the sum of the three
  integrals and a constant of integration.

  Here is an example of this procedure applied to the total differential

  <\equation>
    <label|df=(2xy)dx+...><df>=<around|(|2*x*y|)><dx>+<around|(|x<rsup|2>+z|)><dif>y+<around|(|y-9*z<rsup|2>|)><dif>z
  </equation>

  An expression for the function <math|f> in this example is given by the sum

  <\equation>
    <label|f=>

    <\eqsplit>
      <tformat|<table|<row|<cell|f>|<cell|=<big|int><rsub|0><rsup|x<rprime|'>><space|-0.17em><around|(|2*x\<cdot\>0|)><dx>+<big|int><rsub|0><rsup|y<rprime|'>><space|-0.17em><around|[|<around|(|x<rprime|'>|)><rsup|2>+0|]><dif>y+<big|int><rsub|0><rsup|z<rprime|'>><space|-0.17em><around|(|y<rprime|'>-9*z<rsup|2>|)><dif>z+C>>|<row|<cell|>|<cell|=0+x<rsup|2>*y+<around|(|y*z-9*z<rsup|3>/3|)>+C>>|<row|<cell|>|<cell|=x<rsup|2>*y+y*z-3*z<rsup|3>+C>>>>
    </eqsplit>
  </equation>

  where primes are omitted on the second and third lines because the
  expressions are supposed to apply to any values of <math|x>, <math|y>, and
  <math|z>. <math|C> is an integration constant. You can verify that the
  third line of Eq. <reference|f=> gives the correct expression for <math|f>
  by taking partial derivatives with respect to <math|x>, <math|y>, and
  <math|z> and comparing with Eq. <reference|df=(2xy)dx+...>.

  In chemical thermodynamics, there is not likely to be occasion to perform
  this kind of integration. The fact that it can be done, however, shows that
  if we stick to one set of independent variables, the expression for the
  total differential of an independent variable contains the same information
  as the independent variable itself.

  A different kind of integration can be used to express a dependent
  extensive property in terms of independent extensive properties. An
  <em|extensive> property of a thermodynamic system is one that is additive,
  and an <em|intensive> property is one that is not additive and has the same
  value everywhere in a homogeneous region (Sec. <reference|2-extensive
  \ intensive>). Suppose we have a state function <math|f> that is an
  extensive property with the total differential

  <\equation>
    <df>=a<dx>+b<dif>y+c<dif>z+\<ldots\>
  </equation>

  where the independent variables <math|x,y,z,\<ldots\>> are extensive and
  the coefficients <math|a,b,c,\<ldots\>> are intensive. If the independent
  variables include those needed to describe an open system (for example, the
  amounts of the substances), then it is possible to integrate both sides of
  the equation from a lower limit of zero for each of the extensive functions
  while holding the intensive functions constant:

  <\equation>
    <big|int><rsub|0><rsup|f<rprime|'>><space|-0.17em><space|-0.17em><df>=a*<big|int><rsub|0><rsup|x<rprime|'>><space|-0.17em><space|-0.17em><dx>+b*<big|int><rsub|0><rsup|y<rprime|'>><space|-0.17em><space|-0.17em><dif>y+c*<big|int><rsub|0><rsup|z<rprime|'>><space|-0.17em><space|-0.17em><dif>z+\<ldots\>
  </equation>

  <\equation>
    f<rprime|'>=a*x<rprime|'>+b*y<rprime|'>+c*z<rprime|'>+\<ldots\>
  </equation>

  Note that a term of the form <math|c<dif>u> where <math|u> is
  <em|intensive> becomes <em|zero> when integrated with intensive functions
  held constant, because <math|<dif>u> is this case is zero.

  <section|Legendre Transforms><label|app:Legendre>

  <I|Legendre transform\|(>A <I|Legendre transform\|reg><newterm|Legendre
  transform> of a state function is a linear change of one or more of the
  independent variables made by subtracting products of conjugate variables.

  To understand how this works, consider a state function <math|f> whose
  total differential is given by

  <\equation>
    <label|df(x,y,x)=adx+bdy+cdz><df>=a<dx>+b<dif>y+c<dif>z
  </equation>

  In the expression on the right side, <math|x>, <math|y>, and <math|z> are
  being treated as the independent variables. The pairs <math|a> and
  <math|x>, <math|b> and <math|y>, and <math|c> and <math|z> are
  <em|conjugate pairs>. That is, <math|a> and <math|x> are conjugates,
  <math|b> and <math|y> are conjugates, and <math|c> and <math|z> are
  conjugates.

  For the first example of a Legendre transform, we define a new state
  function <math|f<rsub|1>> by subtracting the product of the conjugate
  variables <math|a> and <math|x>:

  <\equation>
    <label|f1=f-ax>f<rsub|1><defn>f-a*x
  </equation>

  The function <math|f<rsub|1>> is a Legendre transform of <math|f>. We take
  the differential of Eq. <reference|f1=f-ax>

  <\equation>
    <df><rsub|1>=<df>-a<dx>-x<dif>a
  </equation>

  and substitute for <math|<df>> from Eq. <reference|df(x,y,x)=adx+bdy+cdz>:

  <\equation>
    <label|df1=-xda+bdy+cdz>

    <\eqsplit>
      <tformat|<table|<row|<cell|<df><rsub|1>>|<cell|=<around|(|a<dx>+b<dif>y+c<dif>z|)>-a<dx>-x<dif>a>>|<row|<cell|>|<cell|=-x<dif>a+b<dif>y+c<dif>z>>>>
    </eqsplit>
  </equation>

  Equation <reference|df1=-xda+bdy+cdz> gives the total differential of
  <math|f<rsub|1>> with <math|a>, <math|y>, and <math|z> as the independent
  variables. The functions <math|x> and <math|a> have switched places as
  independent variables. What we did in order to let <math|a> replace
  <math|x> as an independent variable was to subtract from <math|f> the
  product of the conjugate variables <math|a> and <math|x>.

  Because the right side of Eq. <reference|df1=-xda+bdy+cdz> is an expression
  for the total differential of the state function <math|f<rsub|1>>, we can
  use the expression to identify the coefficients as partial derivatives of
  <math|f<rsub|1>> with respect to the new set of independent variables:

  <\equation>
    -x=<Pd|f<rsub|1>|a|y,z><space|2em>b=<Pd|f<rsub|1>|y|a,z><space|2em>c=<Pd|f<rsub|1>|z|a,y>
  </equation>

  We can also use Eq. <reference|df1=-xda+bdy+cdz> to write new reciprocity
  relations, such as

  <\equation>
    -<Pd|x|y|a,z>=<Pd|b|a|y,z>
  </equation>

  We can make other Legendre transforms of <math|f> by subtracting one or
  more products of conjugate variables. A second example of a Legendre
  transform is

  <\equation>
    <label|f2=f-by-cz>f<rsub|2><defn>f-b*y-c*z
  </equation>

  whose total differential is

  <\equation>
    <label|df2=>

    <\eqsplit>
      <tformat|<table|<row|<cell|<df><rsub|2>>|<cell|=<df>-b<dif>y-y<dif>b-c<dif>z-z<dif>c>>|<row|<cell|>|<cell|=a<dx>-y<dif>b-z<dif>c>>>>
    </eqsplit>
  </equation>

  Here <math|b> has replaced <math|y> and <math|c> has replaced <math|z> as
  independent variables. Again, we can identify the coefficients as partial
  derivatives and write new reciprocity relations.

  If we have an algebraic expression for a state function as a function of
  independent variables, then a Legendre transform preserves all the
  information contained in that expression. To illustrate this, we can use
  the state function <math|f> and its Legendre transform <math|f<rsub|2>>
  described above. Suppose we have an expression for
  <math|f*<around|(|x,y,z|)>>\Vthis is <math|f> expressed as a function of
  the independent variables <math|x>, <math|y>, and <math|z>. Then by taking
  partial derivatives of this expression, we can find according to Eq.
  <reference|a=(df/dx)_y,z etc.> expressions for the functions
  <math|a<around|(|x,y,z|)>>, <math|b*<around|(|x,y,z|)>>, and
  <math|c*<around|(|x,y,z|)>>.

  Now we perform the Legendre transform of Eq. <reference|f2=f-by-cz>:
  <math|f<rsub|2>=f-b*y-c*z> with total differential
  <math|<df><rsub|2>=a<dx>-y<dif>b-z<dif>c> (Eq. <reference|df2=>). The
  independent variables have been changed from <math|x>, <math|y>, and
  <math|z> to <math|x>, <math|b>, and <math|c>.

  We want to find an expression for <math|f<rsub|2>> as a function of these
  new variables, using the information available from the original function
  <math|f*<around|(|x,y,z|)>>. To do this, we eliminate <math|z> from the
  known functions <math|b*<around|(|x,y,z|)>> and <math|c*<around|(|x,y,z|)>>
  and solve for <math|y> as a function of <math|x>, <math|b>, and <math|c>.
  We also eliminate <math|y> from <math|b*<around|(|x,y,z|)>> and
  <math|c*<around|(|x,y,z|)>> and solve for <math|z> as a function of
  <math|x>, <math|b>, and <math|c>. This gives us expressions for
  <math|y*<around|(|x,b,c|)>> and <math|z<around|(|x,b,c|)>> which we
  substitute into the expression for <math|f*<around|(|x,y,z|)>>, turning it
  into the function <math|f*<around|(|x,b,c|)>>. Finally, we use the
  functions of the new variables to obtain an expression for
  <math|f<rsub|2>*<around|(|x,b,c|)>=f*<around|(|x,b,c|)>-b*y*<around|(|x,b,c|)>-c*z<around|(|x,b,c|)>>.

  The original expression for <math|f*<around|(|x,y,z|)>> and the new
  expression for <math|f<rsub|2>*<around|(|x,b,c|)>> contain the same
  information. We could take the expression for
  <math|f<rsub|2>*<around|(|x,b,c|)>> and, by following the same procedure
  with the Legendre transform <math|f=f<rsub|2>+b*y+c*z>, retrieve the
  expression for <math|f*<around|(|x,y,z|)>>. Thus no information is lost
  during a Legendre transform. <I|Legendre transform\|)>

  <chapter|Forces, Energy, and Work><label|app:forces>

  <pagestyle|thermobkpagestyle>

  The aim of this appendix is to describe a simple model that will help to
  clarify the meaning of energy and mechanical work in macroscopic systems.
  The appendix applies fundamental principles of classical mechanics to a
  collection of material particles representing a closed system and its
  surroundings. Although classical mechanics cannot duplicate all features of
  a chemical system\Vfor instance, quantum properties of atoms are
  ignored\Vthe behavior of the particles and their interactions will show us
  how to evaluate the thermodynamic work in a real system.

  <I|Force\|(>In broad outline the derivation is as follows. An <I|Reference
  frame!inertial\|reg><I|Frame!reference!inertial\|reg>inertial reference
  frame in which Newton's laws of motion are valid is used to describe the
  positions and velocities of the particles. The particles are assumed to
  exert central forces on one another, such that between any two particles
  the force is a function only of the interparticle distance and is directed
  along the line between the particles.

  We define the kinetic energy of the collection of particles as the sum for
  all particles of <math|<onehalf>m*v<rsup|2>> (where <math|m> is mass and
  <math|v> is velocity). We define the potential energy as the sum over
  pairwise particle\Uparticle interactions of potential functions that depend
  only on the interparticle distances. The total energy is the sum of the
  kinetic and potential energies. With these definitions and Newton's laws, a
  series of mathematical operations leads to the principle of the
  conservation of energy: the total energy remains constant over time.

  Continuing the derivation, we consider one group of particles to represent
  a closed thermodynamic system and the remaining particles to constitute the
  surroundings. The system particles may interact with an external force
  field, such as a gravitational field, created by some of the particles in
  the surroundings. The energy of the system is considered to be the sum of
  the kinetic energy of the system particles, the potential energy of
  pairwise particle\Uparticle interactions within the system, and the
  potential energy of the system particles in any external field or fields.
  The change in the system energy during a time interval is then found to be
  given by a certain sum of integrals which, in the transition to a
  macroscopic model, becomes the sum of heat and thermodynamic work in accord
  with the first law of thermodynamics.

  A similar derivation, using a slightly different notation, is given in Ref.
  <cite|devoe-07>.

  <section|Forces between Particles><label|A-forces between particles>

  A <em|material particle> is a body that has mass and is so small that it
  behaves as a point, without rotational energy or internal structure. We
  assume each particle has a constant mass, ignoring relativistic effects
  that are important only when the particle moves at a speed close to the
  speed of light.

  Consider a collection of an arbitrary number of material particles that
  have interactions only among themselves and with no other particles. Later
  we will consider some of the particles to constitute a thermodynamic
  <em|system> and the others to be the <em|surroundings>.

  Newton's laws of motion are obeyed only in an <I|Inertial reference
  frame\|reg><I|Frame!reference!inertial\|reg><em|inertial> reference frame.
  A reference frame that is fixed or moving at a constant velocity relative
  to local stars is practically an inertial reference frame. To a good
  approximation, a reference frame fixed relative to the earth's surface is
  also an inertial system (the necessary corrections are discussed in Sec.
  <reference|A-earth-fixed frame>). This reference frame will be called
  simply the <I|Lab frame\|reg><I|Frame!lab\|reg><em|lab frame>, and treated
  as an inertial frame in order that we may apply Newton's laws.

  It will be assumed that the Cartesian components of all vector quantities
  appearing in Sections <reference|A-forces between
  particles>\U<reference|A-macr work> are measured in an inertial lab frame.

  Classical mechanics is based on the idea that one material particle acts on
  another by means of a <em|force> that is independent of the reference
  frame. Let the vector <math|\<b-F\><rsub|i*j>> denote the force exerted on
  particle <math|i> by particle <math|j>.<footnote|This and the next two
  footnotes are included for readers who are not familiar with vector
  notation. The quantity <math|\<b-F\><rsub|i*j>> is printed in boldface to
  indicate it is a <em|vector> having both magnitude and direction.> The
  <em|net> force <math|\<b-F\><rsub|i>> acting on particle <math|i> is the
  vector sum of the individual forces exerted on it by the other
  particles:<footnote|The rule for adding vectors, as in the summation shown
  here, is that the sum is a vector whose component along each axis of a
  Cartesian coordinate system is the sum of the components along that axis of
  the vectors being added. For example, the vector
  <math|\<b-C\>=\<b-A\>+\<b-B\>> has components
  <math|C<rsub|x>=A<rsub|x>+B<rsub|x>>, <math|C<rsub|y>=A<rsub|y>+B<rsub|y>>,
  and <math|C<rsub|z>=A<rsub|z>+B<rsub|z>>.>

  <\equation>
    <label|Fi=sum(Fij)>\<b-F\><rsub|i>=<big|sum><rsub|j\<ne\>i>\<b-F\><rsub|i*j>
  </equation>

  (The term in which <math|j> equals <math|i> has to be omitted because a
  particle does not act on itself.) According to <I|Newton's second law of
  motion\|reg>Newton's second law of motion, the net force
  <math|\<b-F\><rsub|i>> acting on particle <math|i> is equal to the product
  of its mass <math|m<rsub|i>> and its acceleration:

  <\equation>
    <label|Fi=mdv(i)/dt>\<b-F\><rsub|i>=m<rsub|i>*<frac|<dif>\<b-v\><rsub|i>|<dt>>
  </equation>

  Here <math|\<b-v\><rsub|i>> is the particle's velocity in the <I|Lab
  frame\|reg><I|Frame!lab\|reg>lab frame and <math|t> is time.

  A nonzero net force causes particle <math|i> to accelerate and its velocity
  and position to change. The <I|Work\|(><em|work> done by the net force
  acting on the particle in a given time interval is defined by the
  integral<footnote|The dot between the vectors in the integrand indicates a
  scalar product or dot product, which is a <em|non>vector quantity. The
  general definition of the scalar product of two vectors, <math|\<b-A\>> and
  <math|\<b-B\>>, is <math|\<b-A\><dotprod>\<b-B\>=A*B*cos \<alpha\>> where
  <math|A> and <math|B> are the magnitudes of the two vectors and
  <math|\<alpha\>> is the angle between their positive directions.>

  <\equation>
    <label|Wi=int F(i)dr(i)>W<rsub|i>=<big|int><space|-0.17em>\<b-F\><rsub|i><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  where <math|\<b-r\><rsub|i>> is the position vector of the particle\Va
  vector from the origin of the <I|Lab frame\|reg><I|Frame!lab\|reg>lab frame
  to the position of the particle.

  <\minor>
    \ The integral on the right side of Eq. <reference|Wi=int F(i)dr(i)> is
    an example of a <em|line integral>. It indicates that the scalar product
    of the net force acting on the particle and the particle's displacement
    is to be integrated over time during the time interval. The integral can
    be written without vectors in the form
    <math|<big|int><space|-0.17em>F<rsub|i>*cos
    \<alpha\>*<around|(|<dif>s/<dt>|)><dt>> where <math|F<rsub|i>> is the
    magnitude of the net force, <math|<dif>s/<dt>> is the magnitude of the
    velocity of the particle along its path in three-dimensional space, and
    <math|\<alpha\>> is the angle between the force and velocity vectors. The
    three quantities <math|F<rsub|i>>, <math|cos \<alpha\>>, and
    <math|<dif>s/<dt>> are all functions of time, <math|t>, and the
    integration is carried out with time as the integration variable.
  </minor>

  By substituting the expression for <math|\<b-F\><rsub|i>> (Eq.
  <reference|Fi=mdv(i)/dt>) in Eq. <reference|Wi=int F(i)dr(i)>, we obtain

  <\equation>
    <label|intF(i)dr(i)=Del(KE)>

    <\eqsplit>
      <tformat|<table|<row|<cell|W<rsub|i>>|<cell|=m<rsub|i>*<big|int><space|-0.17em><frac|<dif>\<b-v\><rsub|i>|<dt>><dotprod><dif>\<b-r\><rsub|i>=m<rsub|i>*<big|int><space|-0.17em><frac|<dif>\<b-r\><rsub|i>|<dt>><dotprod><dif>\<b-v\><rsub|i>=m<rsub|i>*<big|int><space|-0.17em>\<b-v\><rsub|i><dotprod><dif>\<b-v\><rsub|i>=m<rsub|i>*<big|int><space|-0.17em>v<rsub|i><dif>v<rsub|i>>>|<row|<cell|>|<cell|=<Del><space|-0.17em><around*|(|<onehalf>m<rsub|i>*v<rsub|i><rsup|2>|)>>>>>
    </eqsplit>
  </equation>

  where <math|v<rsub|i>> is the magnitude of the velocity.

  The quantity <math|<frac|1|2>*m<rsub|i>*v<rsub|i><rsup|2>> is called the
  <I|Energy\|(><I|Energy!kinetic\|reg><I|Kinetic energy\|reg><em|kinetic
  energy> of particle <math|i>. This kinetic energy depends only on the
  magnitude of the velocity (i.e., on the speed) and not on the particle's
  position.

  The <em|total> work <math|W<subs|t*o*t>> done by all forces acting on all
  particles during the time interval is the sum of <math|W<rsub|i>> for all
  particles: <math|W<subs|t*o*t>=<big|sum><rsub|i><space|-0.17em>W<rsub|i>>.<footnote|The
  work <math|W<subs|t*o*t>> defined here is not the same as the thermodynamic
  work appearing in the first law of thermodynamics.> Equation
  <reference|intF(i)dr(i)=Del(KE)> then gives us

  <\equation>
    <label|W(tot)=Del(KE)>W<subs|t*o*t>=<big|sum><rsub|i><Del><space|-0.17em><around*|(|<onehalf>m<rsub|i>*v<rsub|i><rsup|2>|)>=<Del><space|-0.17em><around*|(|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>|)>
  </equation>

  Equation <reference|W(tot)=Del(KE)> shows that the total work during a time
  interval is equal to the change in the total kinetic energy in this
  interval. This result is called the \Pwork-energy principle\Q by
  physicists.<footnote|Ref. <cite|sears-70>, p. 95.>

  From Eqs. <reference|Fi=sum(Fij)> and <reference|Wi=int F(i)dr(i)> we
  obtain a second expression for <math|W<subs|t*o*t>>:

  <\equation>
    <label|W(tot)=sum(i)sum(j)int>W<subs|t*o*t>=<big|sum><rsub|i>W<rsub|i>=<big|sum><rsub|i><big|int><space|-0.17em><big|sum><rsub|j\<ne\>i>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>=<big|sum><rsub|i><big|sum><rsub|j\<ne\>i><big|int><space|-0.17em><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  The double sum in the right-most expression can be written as a sum over
  pairs of particles, the term for the pair <math|i> and <math|j> being

  <\equation>
    <label|F(ij)dr(i)+F(ji)dr(j)>

    <\eqsplit>
      <tformat|<table|<row|<cell|<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>+<big|int><space|-0.17em>\<b-F\><rsub|j*i><dotprod><dif>\<b-r\><rsub|j>>|<cell|=<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>-<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|j>>>|<row|<cell|>|<cell|=<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif><around|(|\<b-r\><rsub|i>-\<b-r\><rsub|j>|)>=<big|int><space|-0.17em><around|(|\<b-F\><rsub|i*j><dotprod>\<b-e\><rsub|i*j>|)><dif>r<rsub|i*j>>>>>
    </eqsplit>
  </equation>

  Here we have used the relations <math|\<b-F\><rsub|j*i>=-\<b-F\><rsub|i*j>>
  (from <I|Newton's third law of action and reaction\|reg>Newton's third law)
  and <math|<around|(|\<b-r\><rsub|i>-\<b-r\><rsub|j>|)>=\<b-e\><rsub|i*j>*r<rsub|i*j>>,
  where <math|\<b-e\><rsub|i*j>> is a unit vector pointing from <math|j> to
  <math|i> and <math|r<rsub|i*j>> is the distance between the particles.
  Equation <reference|W(tot)=sum(i)sum(j)int> becomes

  <\equation>
    <label|W(tot)=sum(i)sum(j\<gtr\>i)>W<subs|t*o*t>=<big|sum><rsub|i><big|sum><rsub|j\<ne\>i><big|int><space|-0.17em><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>=<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><big|int><space|-0.17em><around|(|\<b-F\><rsub|i*j><dotprod>\<b-e\><rsub|i*j>|)><dif>r<rsub|i*j>
  </equation>

  Next we look in detail at the force that particle <math|j> exerts on
  particle <math|i>. This force depends on the nature of the two particles
  and on the distance between them. For instance, <I|Newton's law of
  universal gravitation\|reg>Newton's law of universal gravitation gives the
  magnitude of a <I|Force!gravitational\|reg><I|Gravitational!force\|reg><em|gravitational>
  force as <math|G*m<rsub|i>*m<rsub|j>/r<rsub|i*j><rsup|2>>, where <math|G>
  is the gravitational constant. <I|Coulomb's law\|reg>Coulomb's law gives
  the magnitude of an <I|Force!electrical\|reg><I|Electrical!force\|reg><em|electrical>
  force between stationary charged particles as
  <math|Q<rsub|i>*Q<rsub|j>/<around|(|4*\<pi\>*\<epsilon\><rsub|0>*r<rsub|i*j><rsup|2>|)>>,
  where <math|Q<rsub|i>> and <math|Q<rsub|j>> are the charges and
  <math|\<epsilon\><rsub|0>> is the electric constant (or permittivity of
  vacuum). These two kinds of forces are central forces that obey <I|Newton's
  third law of action and reaction\|reg>Newton's third law of action and
  reaction, namely, that the forces exerted by two particles on one another
  are equal in magnitude and opposite in direction and are directed along the
  line joining the two particles. (In contrast, the <em|electromagnetic>
  force between charged particles in relative motion does <em|not> obey
  Newton's third law.)

  We will assume the force <math|\<b-F\><rsub|i*j>> exerted on particle
  <math|i> by particle <math|j> has a magnitude that depends only on the
  interparticle distance <math|r<rsub|i*j>> and is directed along the line
  between <math|i> and <math|j>, as is true of gravitational and
  electrostatic forces and on intermolecular forces in general. Then we can
  define a <I|Potential!function\|reg><em|potential function>,
  <math|<varPhi><rsub|i*j>>, for this force that will be a contribution to
  the potential energy. To see how <math|<varPhi><rsub|i*j>> is related to
  <math|\<b-F\><rsub|i*j>>, we look at Eq. <reference|F(ij)dr(i)+F(ji)dr(j)>.
  The left-most expression, <math|<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>+<big|int><space|-0.17em>\<b-F\><rsub|j*i><dotprod><dif>\<b-r\><rsub|j>>,
  is the change in the kinetic energies of particles <math|i> and <math|j>
  during a time interval (see Eq. <reference|intF(i)dr(i)=Del(KE)>). If these
  were the only particles, their total energy should be constant for
  conservation of energy; thus <math|<Del><varPhi><rsub|i*j>> should have the
  same magnitude and the opposite sign of the kinetic energy change:

  <\equation>
    <label|del phi><Del><varPhi><rsub|i*j>=-<big|int><space|-0.17em><around|(|\<b-F\><rsub|i*j><dotprod>\<b-e\><rsub|i*j>|)><dif>r<rsub|i*j>
  </equation>

  The value of <math|<varPhi><rsub|i*j>> at any interparticle distance
  <math|r<rsub|i*j>> is fully defined by Eq. <reference|del phi> and the
  choice of an arbitrary zero. The quantity
  <math|<around|(|\<b-F\><rsub|i*j><dotprod>\<b-e\><rsub|i*j>|)>> is simply
  the component of the force along the line between the particles, and is
  negative for an attractive force (one in which <math|\<b-F\><rsub|i*j>>
  points from <math|i> to <math|j>) and positive for a repulsive force. If
  the force is attractive, the value of <math|<varPhi><rsub|i*j>> increases
  with increasing <math|r<rsub|i*j>>; if the force is repulsive,
  <math|<varPhi><rsub|i*j>> decreases with increasing <math|r<rsub|i*j>>.
  Since <math|<varPhi><rsub|i*j>> is a function only of <math|r<rsub|i*j>>,
  it is independent of the choice of reference frame.

  Equations <reference|W(tot)=sum(i)sum(j\<gtr\>i)> and <reference|del phi>
  can be combined to give

  <\equation>
    <label|W(tot)=-Del(PE)>W<subs|t*o*t>=-<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><Del><varPhi><rsub|i*j>=-<Del><space|-0.17em><around*|(|<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>|)>
  </equation>

  By equating the expressions for <math|W<subs|t*o*t>> given by Eqs.
  <reference|W(tot)=Del(KE)> and <reference|W(tot)=-Del(PE)> and rearranging,
  we obtain

  <\equation>
    <label|E(tot)2=E(tot)1><Del><space|-0.17em><around*|(|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>|)>+<Del><space|-0.17em><around*|(|<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>|)>=0
  </equation>

  This equation shows that the quantity

  <\equation>
    <label|E(tot)=KE+PE>E<subs|t*o*t>=<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>
  </equation>

  is constant over time as the particles move in response to the forces
  acting on them. The first term on the right side of Eq.
  <reference|E(tot)=KE+PE> is the total kinetic energy of the particles. The
  second term is the pairwise sum of particle\Uparticle potential functions;
  this term is called the <I|Energy!potential\|reg><I|Potential!energy\|reg><em|potential
  energy> of the particles. Note that the kinetic energy depends only on
  particle speeds and the potential energy depends only on particle
  positions.

  The significance of Eq. <reference|E(tot)2=E(tot)1> is that the total
  energy <math|E<subs|t*o*t>> defined by Eq. <reference|E(tot)=KE+PE> is
  <em|conserved>. This will be true provided the <I|Inertial reference
  frame\|reg><I|Frame!reference!inertial\|reg>reference frame used for
  kinetic energy is inertial and the only forces acting on the particles are
  those responsible for the particle\Uparticle potential functions.

  <section|The System and Surroundings>

  Now we are ready to assign the particles to two groups: particles in the
  system and those in the surroundings. This section will use the following
  convention: indices <math|i> and <math|j> refer to particles in the
  <em|system>; indices <math|k> and <math|l> refer to particles in the
  <em|surroundings>. This division of particles is illustrated schematically
  in Fig. <reference|fig:A-forces>(a) <vpageref|fig:A-forces>.

  <\big-figure>
    <\boxedfigure>
      <image|./BACK-SUP/forces.eps||||>

      <\capt>
        Assignment of particles to groups, and some representative
        particle--particle potential functions (schematic). The closed dashed
        curve represents the system boundary.

        \ (a)<nbsp>The open circles represent particles in the system, and
        the filled circles are particles in the surroundings.

        \ (b)<nbsp>The filled triangles are particles in the surroundings
        that are the source of a conservative force field for particles in
        the system.<label|fig:A-forces>
      </capt>
    </boxedfigure>
  </big-figure|>

  With this change in notation, Eq. <reference|E(tot)=KE+PE> becomes

  <\equation>
    <label|E(tot)=sums>E<subs|t*o*t>=<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><big|sum><rsub|k><varPhi><rsub|i*k>+<big|sum><rsub|k><onehalf>m<rsub|k>*v<rsub|k><rsup|2>+<big|sum><rsub|k><big|sum><rsub|l\<gtr\>k><varPhi><rsub|k*l>
  </equation>

  A portion of the surroundings may create a time-independent conservative
  force field (an \Pexternal\Q field) for a particle in the system. In order
  for such a field to be present, its contribution to the force exerted on
  the particle and to the particle's potential energy must depend only on the
  particle's position in the <I|Lab frame\|reg><I|Frame!lab\|reg>lab frame.
  The usual gravitational and electrostatic fields are of this type.

  In order to clarify the properties of a conservative external field, the
  index <math|k<rprime|'>> will be used for those particles in the
  surroundings that are not the source of an external field, and
  <math|k<rprime|''>> for those that are, as indicated in Fig.
  <reference|fig:A-forces>(b). Then the force exerted on system particle
  <math|i> due to the field is <math|\<b-F\><rsub|i><sups|f*i*e*l*d>=<big|sum><rsub|k<rprime|''>>\<b-F\><rsub|i*k<rprime|''>>>.
  If this were the only force acting on particle <math|i>, the change in its
  kinetic energy during a time interval would be
  <math|<big|int><space|-0.17em>\<b-F\><rsub|i><sups|f*i*e*l*d><dotprod><dif>\<b-r\><rsub|i>>
  (Eq. <reference|intF(i)dr(i)=Del(KE)>). For conservation of energy, the
  potential energy change in the time interval should have the same magnitude
  and the opposite sign:

  <\equation>
    <label|del phi_i(field)><Del><varPhi><rsub|i><sups|f*i*e*l*d>=-<big|int><space|-0.17em>\<b-F\><rsub|i><sups|f*i*e*l*d><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  Only if the integral <math|<big|int><space|-0.17em>\<b-F\><rsub|i><sups|f*i*e*l*d><dotprod><dif>\<b-r\><rsub|i>>
  has the same value for all paths between the initial and final positions of
  the particle does a conservative force field exist; otherwise the concept
  of a potential energy <math|<varPhi><rsub|i><sups|f*i*e*l*d>> is not valid.

  Taking a gravitational field as an example of a conservative external
  field, we replace <math|\<b-F\><rsub|i><sups|f*i*e*l*d>> and
  <math|<varPhi><rsub|i><sups|f*i*e*l*d>> by
  <math|\<b-F\><rsub|i><sups|g*r*a*v>> and
  <math|<varPhi><rsub|i><sups|g*r*a*v>>: <math|<Del><varPhi><rsub|i><sups|g*r*a*v>=-<big|int><space|-0.17em>\<b-F\><rsub|i><sups|g*r*a*v><dotprod><dif>\<b-r\><rsub|i>>.
  The gravitational force on particle <math|i> is, from Newton's second law,
  the product of the particle mass and its acceleration
  <math|-g*\<b-e\><rsub|z>> in the gravitational field:
  <math|\<b-F\><rsub|i><sups|g*r*a*v>=-m<rsub|i>*g*\<b-e\><rsub|z>> where
  <math|g> is the acceleration of free fall and <math|\<b-e\><rsub|z>> is a
  unit vector in the vertical (upward) <math|z> direction. The change in the
  gravitational potential energy given by Eq. <reference|del phi_i(field)> is

  <\equation>
    <label|delPhi_i(grav)=m_i g del z_i><Del><varPhi><rsub|i><sups|g*r*a*v>=m<rsub|i>*g*<big|int><space|-0.17em>\<b-e\><rsub|z><dotprod><dif>\<b-r\><rsub|i>=m<rsub|i>*g<space|0.17em><Del>z<rsub|i>
  </equation>

  (The range of elevations of the system particles is assumed to be small
  compared with the earth's radius, so that each system particle experiences
  essentially the same constant value of <math|g>.) Thus we can define the
  gravitational potential energy of particle <math|i>, which is a function
  only of the particle's vertical position in the <I|Lab
  frame\|reg><I|Frame!lab\|reg>lab frame, by
  <math|<varPhi><rsub|i><sups|g*r*a*v>=m<rsub|i>*g*z<rsub|i>+C<rsub|i>> where
  <math|C<rsub|i>> is an arbitrary constant.

  Returning to Eq. <reference|E(tot)=sums> for the total energy, we can now
  write the third term on the right side in the form

  <\equation>
    <label|sum_i sum_k phi(ik)><big|sum><rsub|i><big|sum><rsub|k><varPhi><rsub|i*k>=<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>+<big|sum><rsub|i><varPhi><rsub|i><sups|f*i*e*l*d>
  </equation>

  To divide the expression for the total energy into meaningful parts, we
  substitute Eq. <reference|sum_i sum_k phi(ik)> in Eq.
  <reference|E(tot)=sums> and rearrange in the form

  <\equation>
    <label|E(tot)(grouped)>

    <\eqsplit>
      <tformat|<table|<row|<cell|E<subs|t*o*t>>|<cell|=<around*|[|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><varPhi><rsub|i><sups|f*i*e*l*d>|]>>>|<row|<cell|>|<cell|<space|1em>+<around*|[|<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>|]>+<around*|[|<big|sum><rsub|k><onehalf>m<rsub|k>*v<rsub|k><rsup|2>+<big|sum><rsub|k><big|sum><rsub|l\<gtr\>k><varPhi><rsub|k*l>|]>>>>>
    </eqsplit>
  </equation>

  The terms on the right side of this equation are shown grouped with
  brackets into three quantities. The first quantity depends only on the
  speeds and positions of the particles in the <em|system>, and thus
  represents the energy of the system:

  <\equation>
    <label|E(sys)=>E<sys>=<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><varPhi><rsub|i><sups|f*i*e*l*d>
  </equation>

  The three terms in this expression for <math|E<sys>> are, respectively, the
  kinetic energy of the system particles relative to the <I|Lab
  frame\|reg><I|Frame!lab\|reg>lab frame, the potential energy of interaction
  among the system particles, and the total potential energy of the system in
  the external field.

  The last bracketed quantity on the right side of Eq.
  <reference|E(tot)(grouped)> depends only on the speeds and positions of all
  the particles in the <em|surroundings>, so that this quantity is the energy
  of the surroundings, <math|E<subs|s*u*r*r>>. Thus, an abbreviated form of
  Eq. <reference|E(tot)(grouped)> is

  <\equation>
    <label|E(tot)=E(sys)+...>E<subs|t*o*t>=E<sys>+<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>+E<subs|s*u*r*r>
  </equation>

  The quantity <math|<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>>
  represents potential energy shared by both the system and surroundings on
  account of forces acting across the system boundary, other than
  gravitational forces or forces from other external fields. The forces
  responsible for the quantity <math|<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>>
  are generally significant only between particles in the immediate vicinity
  of the system boundary, and will presently turn out to be the crucial
  forces for evaluating thermodynamic work.

  <section|System Energy Change>

  This section derives an important relation between the change
  <math|<Del>E<sys>> of the energy of the system measured in a <I|Lab
  frame\|reg><I|Frame!lab\|reg>lab frame, and the forces exerted by the
  surroundings on the system particles. The indices <math|i> and <math|j>
  will refer to only the particles in the <em|system>.

  We write the net force on particle <math|i> in the form

  <\equation>
    <label|F_i=sum...>\<b-F\><rsub|i>=<big|sum><rsub|j\<ne\>i>\<b-F\><rsub|i*j>+\<b-F\><rsub|i><sups|f*i*e*l*d>+\<b-F\><sur><rsub|i>
  </equation>

  where <math|\<b-F\><rsub|i*j>> is the force exerted on particle <math|i> by
  particle <math|j>, both particles being in the system, and
  <math|\<b-F\><sur><rsub|i>=<big|sum><rsub|k<rprime|'>>\<b-F\><rsub|i*k<rprime|'>>>
  is the net force exerted on particle <math|i> by the particles in the
  surroundings that are not the source of an external field. During a given
  period of time, the work done by forces acting on only the system particles
  is

  <\equation>
    <label|sum intF(i)dr(i)><big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><rsub|i><dotprod><dif>\<b-r\><rsub|i>=<big|sum><rsub|i><big|sum><rsub|j\<ne\>i><big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>+<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><rsub|i><sups|f*i*e*l*d><dotprod><dif>\<b-r\><rsub|i>+<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  We can replace the first three sums in this equation with new expressions.
  Using Eq. <reference|intF(i)dr(i)=Del(KE)>, we have

  <\equation>
    <label|sum=del(KE)><big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><rsub|i><dotprod><dif>\<b-r\><rsub|i>=<Del><around*|(|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>|)>
  </equation>

  From Eqs. <reference|W(tot)=sum(i)sum(j\<gtr\>i)> and <reference|del phi>
  we obtain

  <\equation>
    <label|sum=del(PE)><big|sum><rsub|i><big|sum><rsub|j\<ne\>i><big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>=-<Del><around*|(|<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>|)>
  </equation>

  where the sums are over the system particles. From Eq. <reference|del
  phi_i(field)> we can write

  <\equation>
    <label|del phi(field)><big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><rsub|i><sups|f*i*e*l*d><dotprod><dif>\<b-r\><rsub|i>=-<Del><around*|(|<big|sum><rsub|i><varPhi><rsub|i><sups|f*i*e*l*d>|)>
  </equation>

  Combining Eqs. <reference|sum intF(i)dr(i)>\U<reference|del phi(field)> and
  rearranging, we obtain

  <\equation>
    <big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i>=<Del><around*|(|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><varPhi><rsub|i><sups|f*i*e*l*d>|)>
  </equation>

  Comparison of the expression on the right side of this equation with Eq.
  <reference|E(sys)=> shows that the expression is the same as the change of
  <math|E<sys>>:

  <\equation>
    <label|DelE(sys)=><Del>E<sys>=<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  Recall that the vector <math|\<b-F\><sur><rsub|i>> is the force exerted on
  particle <math|i>, in the system, by the particles in the surroundings
  other than those responsible for an external field. Thus <math|<Del>E<sys>>
  is equal to the total work done on the system by the surroundings, other
  than work done by an external field such as a gravitational field.

  <\minor>
    \ It might seem strange that work done by an external field is not
    included in <math|<Del>E<sys>>. The reason it is not included is that
    <math|<varPhi><rsub|i><sups|f*i*e*l*d>> was defined to be a potential
    energy belonging only to the system, and is thus irrelevant to energy
    transfer from or to the surroundings.

    As a simple example of how this works, consider a system consisting of a
    solid body in a gravitational field. If the only force exerted on the
    body is the downward gravitational force, then the body is in free fall
    but <math|<Del>E<sys>> in the <I|Lab frame\|reg><I|Frame!lab\|reg>lab
    frame is zero; the loss of gravitational potential energy as the body
    falls is equal to the gain of kinetic energy. On the other hand, work
    done on the system by an external force that <em|opposes> the
    gravitational force is included in <math|<Del>E<sys>>. For example, if
    the body is pulled upwards at a constant speed with a string, its
    potential energy increases while its kinetic energy remains constant, and
    <math|E<sys>> increases.
  </minor>

  <section|Macroscopic Work><label|A-macr work>

  In thermodynamics we are interested in the quantity of work done on
  <em|macroscopic> parts of the system during a process, rather than the work
  done on individual particles. Macroscopic work is the energy transferred
  across the system boundary due to concerted motion of many particles on
  which the surroundings exert a force. Macroscopic <em|mechanical> work
  occurs when there is displacement of a macroscopic portion of the system on
  which a short-range <em|contact force> <I|Contact!force\|reg><I|Force!contact\|reg>acts
  across the system boundary. This force could be, for instance, the pressure
  of an external fluid at a surface element of the boundary multiplied by the
  area of the surface element, or it could be the tension in a cord at the
  point where the cord passes through the boundary.

  The symbol <math|w<lab>> will refer to macroscopic work measured with
  displacements in the <I|Lab frame\|reg><I|Frame!lab\|reg>lab frame.

  At any given instant, only the system particles that are close to the
  boundary will have nonnegligible contact forces exerted on them. We can
  define an <em|interaction layer>, a thin shell-like layer within the system
  and next to the system boundary that contains all the system particles with
  appreciable contact forces. We imagine the interaction layer to be divided
  into volume elements, or segments, each of which either moves as a whole
  during the process or else is stationary. Let <math|\<b-R\><rsub|\<tau\>>>
  be a position vector from the origin of the lab frame to a point fixed in
  the boundary at segment <math|\<tau\>>, and let
  <math|\<b-r\><rsub|i*\<tau\>>> be a vector from this point to particle
  <math|i> (Fig. <reference|fig:A-boundary>).

  <\big-figure>
    <boxedfigure|<image|./BACK-SUP/boundary.eps||||> <capt|Position vectors
    within the system. Segment <math|\<tau\>> of the interaction layer lies
    within the heavy curve (representing the system boundary) and the dashed
    lines. Open circle: origin of lab frame; open square: point fixed in
    system boundary at segment <math|\<tau\>>; filled circle: particle
    <math|i>.<label|fig:A-boundary>>>
  </big-figure|>

  Then the position vector for particle <math|i> can be written
  <math|\<b-r\><rsub|i>=\<b-R\><rsub|\<tau\>>+\<b-r\><rsub|i*\<tau\>>>. Let
  <math|\<b-F\><sur><rsub|\<tau\>>> be the total contact force exerted by the
  surroundings on the system particles in segment <math|\<tau\>>:
  <math|\<b-F\><sur><rsub|\<tau\>>=<big|sum><rsub|i>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i>>,
  where <math|\<up-delta\><rsub|i*\<tau\>>> is equal to <math|1> when
  particle <math|i> is in segment <math|\<tau\>> and is zero otherwise.

  The change in the system energy during a process is, from Eq.
  <reference|DelE(sys)=>,

  <\equation>
    <label|Del E(sys)=2 terms>

    <\eqsplit>
      <tformat|<table|<row|<cell|<Del>E<sys>>|<cell|=<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i>=<big|sum><rsub|\<tau\>><big|sum><rsub|i><big|int><space|-0.17em>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i><dotprod><dif><around*|(|\<b-R\><rsub|\<tau\>>+\<b-r\><rsub|i*\<tau\>>|)>>>|<row|<cell|>|<cell|=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rsub|\<tau\>>+<big|sum><rsub|\<tau\>><big|sum><rsub|i><big|int><space|-0.17em>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i*\<tau\>>>>>>
    </eqsplit>
  </equation>

  We recognize the integral <math|<big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rsub|\<tau\>>>
  as the macroscopic work at surface element <math|\<tau\>>, because it is
  the integrated scalar product of the force exerted by the surroundings and
  the displacement. The total macroscopic work during the process is then
  given by

  <\equation>
    <label|w(lab)=sum int>w<lab>=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rsub|\<tau\>>
  </equation>

  <em|Heat>, <I|Heat\|reg><math|q<lab>>, can be defined as energy transfer to
  or from the system that is not accounted for by macroscopic work. This
  transfer occurs by means of chaotic motions and collisions of individual
  particles at the boundary. With this understanding, Eq. <reference|Del
  E(sys)=2 terms> becomes

  <\equation>
    <label|delE(sys)=q(lab)+w(lab)><Del>E<sys>=q<lab>+w<lab>
  </equation>

  with <math|w<lab>> given by the expression in Eq. <reference|w(lab)=sum
  int> and <math|q<lab>> given by

  <\equation>
    <label|q(lab)=sum int>q<lab>=<big|sum><rsub|\<tau\>><big|sum><rsub|i><big|int><space|-0.17em>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i*\<tau\>>
  </equation>

  <section|The Work Done on the System and Surroundings><label|A-work done>

  An additional comment can be made about the transfer of energy between the
  system and the surroundings. We may use Eq. <reference|w(lab)=sum int>,
  with appropriate redefinition of the quantities on the right side, to
  evaluate the work done on the <em|surroundings>. This work may be equal in
  magnitude and opposite in sign to the work <math|w<lab>> done on the
  system. A necessary condition for this equality is that the interacting
  parts of the system and surroundings have equal displacements; that is,
  that there be continuity of motion at the system boundary. We expect there
  to be continuity of motion when a fluid contacts a moving piston or paddle.

  Suppose, however, that the system is stationary and an interacting part of
  the surroundings moves. Then according to Eq. <reference|w(lab)=sum int>,
  <math|w<lab>> is zero, whereas the work done on or by that part of the
  surroundings is <em|not> zero. How can this be, considering that
  <math|E<subs|t*o*t>> remains constant? One possibility, discussed by
  <I|Bridgman, Percy\|reg>Bridgman,<footnote|Ref. <cite|bridgman-41>, p.
  47--56.> is <I|Friction!sliding\|reg>sliding friction at the boundary:
  energy lost by the surroundings in the form of work is gained by the system
  and surroundings in the form of <I|Energy!thermal\|reg><I|Thermal!energy\|reg>thermal
  energy. Since the effect on the system is the same as a flow of heat from
  the surroundings, the division of energy transfer into heat and work can be
  ambiguous when there is <I|Friction!sliding\|reg>sliding friction at the
  boundary.<footnote|The ambiguity can be removed by redefining the system
  boundary so that a thin stationary layer next to the sliding interface, on
  the side that was originally part of the system, is considered to be
  included in the surroundings instead of the system. The layer removed from
  the system by this change can be so thin that the values of the system's
  extensive properties are essentially unaffected. With this redefined
  boundary, the energy transfer across the boundary is entirely by means of
  heat.>

  Another way work can have different magnitudes for system and surroundings
  is a change in potential energy shared by the system and surroundings. This
  shared energy is associated with forces acting across the boundary, other
  than from a time-independent external field, and is represented in Eq.
  <reference|E(tot)=E(sys)+...> by the sum
  <math|<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>>.
  In the usual types of processes this sum is either practically constant, or
  else each term falls off so rapidly with distance that the sum is
  negligible. Since <math|E<subs|t*o*t>> is constant, during such processes
  the quantity <math|E<sys>+E<subs|s*u*r*r>> remains essentially constant.
  <I|Force\|)><I|Work\|)><I|Energy\|)>

  <section|The Local Frame and Internal Energy><label|A-local frame>

  As explained in Sec. <reference|2-internal energy>, a lab frame may not be
  an appropriate reference frame in which to measure changes in the system's
  energy. This is the case when the system as a whole moves or rotates in the
  lab frame, so that <math|E<sys>> depends in part on external coordinates
  that are not state functions. In this case it may be possible to define a
  <I|Local frame\|reg><I|Frame!local\|reg><em|local frame> moving with the
  system in which the energy of the system is a state function, the internal
  energy <math|U>.

  As before, <math|\<b-r\><rsub|i>> is the position vector of particle
  <math|i> in a <I|Lab frame\|reg><I|Frame!lab\|reg>lab frame. A prime
  notation will be used for quantities measured in the local frame. Thus the
  position of particle <math|i> relative to the local frame is given by
  vector <math|\<b-r\><rprime|'><rsub|i>>, which points from the origin of
  the local frame to particle <math|i> (see Fig.
  <reference|fig:A-localframe>).

  <\big-figure>
    <boxedfigure|<image|./BACK-SUP/localframe.eps||||> <capt|Vectors in the
    lab and local reference frames. Open circle: origin of lab frame; open
    triangle: origin of local frame; open square: point fixed in system
    boundary at segment <math|\<tau\>>; filled circle: particle <math|i>. The
    thin lines are the Cartesian axes of the reference
    frames.<label|fig:A-localframe>>>
  </big-figure|>

  The velocity of the particle in the local frame is
  <math|\<b-v\><rprime|'><rsub|i>=<dif>\<b-r\><rprime|'><rsub|i>/<dt>>.

  We continue to treat the earth-fixed lab frame as an inertial frame,
  although this is not strictly true (Sec. <reference|A-earth-fixed frame>).
  If the origin of the local frame moves at constant velocity in the lab
  frame, with Cartesian axes that do not rotate with respect to those of the
  lab frame, then the local frame is also inertial but <math|U> is not equal
  to <math|E<sys>> and the change <math|<Del>U> during a process is not
  necessarily equal to <math|<Del>E<sys>>.

  If the origin of the local frame moves with nonconstant velocity in the lab
  frame, or if the <I|Local frame!rotating\|reg><I|Frame!local!rotating\|reg>local
  frame rotates with respect to the <I|Lab frame\|reg><I|Frame!lab\|reg>lab
  frame, then the local frame has finite acceleration and is noninertial. In
  this case the motion of particle <math|i> in the local frame does not obey
  Newton's second law as it does in an inertial frame. We can, however,
  define an <I|Force!effective\|reg><em|effective> net force
  <math|\<b-F\><sups|e*f*f><rsub|i>> whose relation to the particle's
  acceleration in the local frame has the same form as Newton's second law:

  <\equation>
    <label|F_i(eff)=>\<b-F\><sups|e*f*f><rsub|i>=m<rsub|i>*<frac|<dif>\<b-v\><rprime|'><rsub|i>|<dt>>
  </equation>

  To an observer who is stationary in the local frame, the effective force
  will appear to make the particle's motion obey Newton's second law even
  though the frame is not inertial.

  The net force on particle <math|i> from interactions with other particles
  is given by Eq. <reference|F_i=sum...>:
  <math|\<b-F\><rsub|i>=<big|sum><rsub|j\<ne\>i>\<b-F\><rsub|i*j>+\<b-F\><rsub|i><sups|f*i*e*l*d>+\<b-F\><sur><rsub|i>>.
  The effective force can be written

  <\equation>
    <label|F_i(eff)=F_i+F_i(accel)>\<b-F\><sups|e*f*f><rsub|i>=\<b-F\><rsub|i>+\<b-F\><rsub|i><sups|a*c*c*e*l>
  </equation>

  where <math|\<b-F\><rsub|i><sups|a*c*c*e*l>> is the contribution due to
  acceleration. <math|\<b-F\><rsub|i><sups|a*c*c*e*l>> is not a true force in
  the sense of resulting from the interaction of particle <math|i> with other
  particles. Instead, it is an <I|Force!apparent\|reg>apparent or
  <I|Force!fictitious\|reg>fictitious force introduced to make it possible to
  write Eq. <reference|F_i(eff)=> which resembles Newton's second law. The
  motion of particle <math|i> in an inertial frame is given by
  <math|m<rsub|i><dif>\<b-v\><rsub|i>/<dt>=\<b-F\><rsub|i>>, whereas the
  motion in the local frame is given by <math|m<rsub|i><dif>\<b-v\><rprime|'><rsub|i>/<dt>=\<b-F\><rsub|i>+\<b-F\><rsub|i><sups|a*c*c*e*l>>.

  A simple example may make these statements clear. Consider a small
  unattached object suspended in the \Pweightless\Q environment of an
  orbiting space station. Assume the object is neither moving nor spinning
  relative to the station. Let the object be the system, and fix the local
  frame in the space station. The local frame rotates with respect to local
  stars as the station orbits around the earth; the local frame is therefore
  noninertial. The only true force exerted on the object is a gravitational
  force directed toward the earth. This force explains the object's
  acceleration relative to local stars. The fact that the object has no
  acceleration in the local frame can be explained by the presence of a
  fictitious centrifugal force having the same magnitude as the gravitational
  force but directed in the opposite direction, so that the <em|effective>
  force on the object as a whole is zero.

  The reasoning used to derive the equations in Secs. <reference|A-forces
  between particles>\U<reference|A-macr work> can be applied to an arbitrary
  <I|Local frame\|reg><I|Frame!local\|reg>local frame. To carry out the
  derivations we replace <math|\<b-F\><rsub|i>> by
  <math|\<b-F\><sups|e*f*f><rsub|i>>, <math|\<b-r\><rsub|i>> by
  <math|\<b-r\><rprime|'><rsub|i>>, and <math|\<b-v\><rsub|i>> by
  <math|\<b-v\><rprime|'><rsub|i>>, and use the local frame to measure the
  Cartesian components of all vectors. We need two new potential energy
  functions for the local frame, defined by the relations

  <\equation>
    <label|del phi'(field)_1=><Del><varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i><defn>-<big|int><space|-0.17em>\<b-F\><sups|f*i*e*l*d><rsub|i><dotprod><dif>\<b-r\>*'<rsub|i>
  </equation>

  <\equation>
    <label|del phi(accel)_i=><Del><varPhi><sups|a*c*c*e*l><rsub|i><defn>-<big|int><space|-0.17em>\<b-F\><sups|a*c*c*e*l><rsub|i><dotprod><dif>\<b-r\><rprime|'><rsub|i>
  </equation>

  Both <math|<varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i>> and
  <math|<varPhi><sups|a*c*c*e*l>> must be time-independent functions of the
  position of particle <math|i> in the local frame in order to be valid
  potential functions. (If the local frame is inertial,
  <math|\<b-F\><sups|a*c*c*e*l><rsub|i>> and
  <math|<varPhi><sups|a*c*c*e*l><rsub|i>> are zero.)

  The detailed line of reasoning in Secs. <reference|A-forces between
  particles>\U<reference|A-macr work> will not be repeated here, but the
  reader can verify the following results. The <em|total energy> of the
  system and surroundings measured in the <em|local> frame is given by
  <math|E<rprime|'><subs|t*o*t>=U+<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>+E<rprime|'><subs|s*u*r*r>>
  where the index <math|k<rprime|'>> is for particles in the surroundings
  that are not the source of an external field for the system. The energy of
  the <em|system> (the internal energy) is given by

  <\equation>
    <label|U=4 terms>U=<big|sum><rsub|i><onehalf>m<rsub|i><around|(|v<rprime|'><rsub|i>|)><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><varPhi><rsub|i><sups|<math|<rprime|'>><space|0.17em>field>+<big|sum><rsub|i><varPhi><rsub|i><sups|a*c*c*e*l>
  </equation>

  where the indices <math|i> and <math|j> are for system particles. The
  energy of the <em|surroundings> measured in the <I|Local
  frame\|reg><I|Frame!local\|reg>local frame is

  <\equation>
    E<rprime|'><subs|s*u*r*r>=<big|sum><rsub|k><onehalf>m<rsub|k><around|(|v<rprime|'><rsub|k>|)><rsup|2>+<big|sum><rsub|k><big|sum><rsub|l\<gtr\>k><varPhi><rsub|k*l>+<big|sum><rsub|k><varPhi><rsub|k><sups|a*c*c*e*l>
  </equation>

  where <math|k> and <math|l> are indices for particles in the surroundings.
  The value of <math|E<rprime|'><subs|t*o*t>> is found to be constant over
  time, meaning that energy is conserved in the local frame. The internal
  energy change during a process is the sum of the heat <math|q> measured in
  the local frame and the macroscopic work <math|w> in this frame:

  <\equation>
    <label|Del U=q+w><Del>U=q+w
  </equation>

  The expressions for <math|q> and <math|w>, analogous to Eqs.
  <reference|q(lab)=sum int> and <reference|w(lab)=sum int>, are found to be

  <\equation>
    <label|q=sum int>q=<big|sum><rsub|\<tau\>><big|sum><rsub|i><big|int><space|-0.17em>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i*\<tau\>>
  </equation>

  <\equation>
    <label|w=sum int>w=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rprime|'><rsub|\<tau\>>
  </equation>

  In these equations <math|\<b-R\><rprime|'><rsub|\<tau\>>> is a vector from
  the origin of the local frame to a point fixed in the system boundary at
  segment <math|\<tau\>>, and <math|\<b-r\><rsub|i*\<tau\>>> is a vector from
  this point to particle <math|i> (see Fig. <reference|fig:A-localframe>).

  We expect that an observer in the <I|Local
  frame\|reg><I|Frame!local\|reg>local frame will find the laws of
  thermodynamics are obeyed.<label|thermo-noninertial frame>For instance, the
  Clausius statement of the second law (Sec. <reference|4-Clausius
  statement>) is as valid in a manned orbiting space laboratory as it is in
  an earth-fixed laboratory: nothing the investigator can do will allow
  energy to be transferred by heat from a colder to a warmer body through a
  device operating in a cycle. Equation <reference|Del U=q+w> is a statement
  of the first law of thermodynamics (box on page <pageref|first law>) in the
  local frame. Accordingly, we may assume that the thermodynamic derivations
  and relations treated in the body of this book are valid in any local
  frame, whether or not it is inertial, when <math|U> and <math|w> are
  defined by Eqs. <reference|U=4 terms> and <reference|w=sum int>.

  In the body of the book, <math|w> is called the <em|thermodynamic work>, or
  simply the work. Note the following features brought out by the derivation
  of the expression for <math|w>:

  <\itemize>
    <item>The equation <math|w=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rprime|'><rsub|\<tau\>>>
    has been derived for a <em|closed> system.

    <item>The equation shows how we can evaluate the thermodynamic work
    <math|w> done on the system. For each moving surface element of the
    system boundary at segment <math|\<tau\>> of the interaction layer, we
    need to know the contact force <math|\<b-F\><sur><rsub|\<tau\>>> exerted
    by the surroundings and the displacement
    <math|<dif>\<b-R\><rprime|'><rsub|\<tau\>>> in the <I|Local
    frame\|reg><I|Frame!local\|reg>local frame.

    <item>We could equally well calculate <math|w> from the force exerted by
    the <em|system> on the surroundings. According to <I|Newton's third law
    of action and reaction\|reg>Newton's third law, the force
    <math|\<b-F\><sups|s*y*s><rsub|\<tau\>>> exerted by segment
    <math|\<tau\>> has the same magnitude as
    <math|\<b-F\><sur><rsub|\<tau\>>> and the opposite direction:
    <math|\<b-F\><sups|s*y*s><rsub|\<tau\>>=-\<b-F\><sur><rsub|\<tau\>>>.

    <item>

    During a process, a point fixed in the system boundary at segment
    <math|\<tau\>> is either stationary or traverses a path in
    three-dimensional space. At each intermediate stage of the process, let
    <math|s<rsub|\<tau\>>> be the length of the path that began in the
    initial state. We can write the infinitesimal quantity
    <math|\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rprime|'><rsub|\<tau\>>>
    in the form <math|F<sur><rsub|\<tau\>>cos
    \<alpha\><rsub|\<tau\>><dif>s<rsub|\<tau\>>>, where
    <math|F<sur><rsub|\<tau\>>> is the magnitude of the force,
    <math|<dif>s<rsub|\<tau\>>> is an infinitesimal change of the path
    length, and <math|\<alpha\><rsub|\<tau\>>> is the angle between the
    directions of the force and the displacement. We then obtain the
    following integrated and differential forms of the work:

    <\equation>
      <label|w=sum int F cos(alpha)ds dw=>w=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>F<sur><rsub|\<tau\>>cos
      \<alpha\><rsub|\<tau\>><dif>s<rsub|\<tau\>><space|2em><dw>=<big|sum><rsub|\<tau\>>F<sur><rsub|\<tau\>>cos
      \<alpha\><rsub|\<tau\>><dif>s<rsub|\<tau\>>
    </equation>

    <item>

    If only one portion of the boundary moves in the <I|Local
    frame\|reg><I|Frame!local\|reg>local frame, and this portion has linear
    motion parallel to the <math|x<rprime|'>> axis, we can replace
    <math|\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rprime|'><rsub|\<tau\>>>
    by <math|F<sur><rsub|x<rprime|'>><dx><rprime|'>>, where
    <math|F<sur><rsub|x<rprime|'>>> is the <math|x<rprime|'>> component of
    the force exerted by the surroundings on the moving boundary and
    <math|<dx><rprime|'>> is an infinitesimal displacement of the boundary.
    In this case we can write the following integrated and differential forms
    of the work:

    <\equation>
      <label|w=int F(sur)dx' dw=>w=<big|int><space|-0.17em>F<sur><rsub|x<rprime|'>><dx><rprime|'><space|2em><dw>=F<sur><rsub|x<rprime|'>><dx><rprime|'>
    </equation>

    <item>The work <math|w> does not include work done internally by one part
    of the system on another part.

    <item>In the calculation of work with Eqs. <reference|w=sum
    int>\U<reference|w=int F(sur)dx' dw=>, we do not include forces from an
    external field such as a gravitational field, or
    <I|Force!fictitious\|reg>fictitious forces
    <math|\<b-F\><sups|a*c*c*e*l><rsub|i>> if present.
  </itemize>

  <section|Nonrotating Local Frame><label|A-nonrotating frame>

  <I|Local frame!nonrotating\|reg><I|Frame!local!nonrotating\|reg>Consider
  the case of a nonrotating local frame whose origin moves in the lab frame
  but whose Cartesian axes <math|x<rprime|'>>, <math|y<rprime|'>>,
  <math|z<rprime|'>> remain parallel to the axes <math|x>, <math|y>, <math|z>
  of the lab frame. In this case the Cartesian components of
  <math|\<b-F\><sur><rsub|i>> for particle <math|i> are the same in both
  frames, and so also are the Cartesian components of the infinitesimal
  vector displacement <math|<dif>\<b-r\><rsub|i*\<tau\>>>. According to Eqs.
  <reference|q(lab)=sum int> and <reference|q=sum int>, then, for an
  arbitrary process the value of the heat <math|q> in the local frame is the
  same as the value of the heat <math|q<lab>> in the <I|Lab
  frame\|reg><I|Frame!lab\|reg>lab frame.

  From Eqs. <reference|delE(sys)=q(lab)+w(lab)> and <reference|Del U=q+w>
  with <math|q<lab>> set equal to <math|q>, we obtain the useful relation

  <\equation>
    <label|delU-delE(sys)=w-w(lab)><Del>U-<Del>E<sys>=w-w<lab>
  </equation>

  This equation is not valid if the local frame has rotational motion with
  respect to the lab frame.

  The vector <math|\<b-R\><rprime|'><rsub|\<tau\>>> has the same Cartesian
  components in the lab frame as in the nonrotating local frame, so we can
  write <math|\<b-R\><rsub|\<tau\>>-\<b-R\><rprime|'><rsub|\<tau\>>=\<b-R\><subs|l*o*c>>
  where <math|\<b-R\><subs|l*o*c>> is the position vector in the lab frame of
  the origin of the local frame (see Fig. <reference|fig:A-localframe>). From
  Eqs. <reference|w(lab)=sum int> and <reference|w=sum int>, setting
  <math|<around|(|\<b-R\><rsub|\<tau\>>-\<b-R\><rprime|'><rsub|\<tau\>>|)>>
  equal to <math|\<b-R\><subs|l*o*c>>, we obtain the relation

  <\equation>
    <label|w-w(lab)=int>w-w<lab>=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif><around|(|\<b-R\><rprime|'><rsub|\<tau\>>-\<b-R\><rsub|\<tau\>>|)>=-<big|int><space|-0.17em><around*|(|<big|sum><rsub|\<tau\>>\<b-F\><sur><rsub|\<tau\>>|)><dotprod><dif>\<b-R\><subs|l*o*c>
  </equation>

  The sum <math|<big|sum><rsub|\<tau\>>\<b-F\><sur><rsub|\<tau\>>> is the net
  contact force exerted on the system by the surroundings. For example,
  suppose the system is a fluid in a gravitational field. Let the system
  boundary be at the inner walls of the container, and let the local frame be
  fixed with respect to the container and have negligible acceleration in the
  lab frame. At each surface element of the boundary, the force exerted by
  the pressure of the fluid on the container wall is equal in magnitude and
  opposite in direction to the contact force exerted by the surroundings on
  the fluid. The horizontal components of the contact forces on opposite
  sides of the container cancel, but the vertical components do not cancel
  because of the hydrostatic pressure. The net contact force is
  <math|m*g*\<b-e\><rsub|z>>, where <math|m> is the system mass and
  <math|\<b-e\><rsub|z>> is a unit vector in the vertical <math|+z>
  direction. For this example, Eq. <reference|w-w(lab)=int> becomes

  <\equation>
    <label|w-w(lab)=-mg del z(loc)>w-w<lab>=-m*g<Del>z<subs|l*o*c>
  </equation>

  where <math|z<subs|l*o*c>> is the elevation in the lab frame of the origin
  of the local frame.

  <section|Center-of-mass Local Frame>

  <I|Center-of-mass frame\|(><I|Frame!center-of-mass\|(>If we use a
  <em|center-of-mass frame> (cm frame) for the local frame, the internal
  energy change during a process is related in a particularly simple way to
  the system energy change measured in a lab frame. A cm frame has its origin
  at the center of mass of the system and its Cartesian axes parallel to the
  Cartesian axes of a lab frame. This is a special case of the
  <I|Frame!local!nonrotating\|reg>nonrotating local frame discussed in Sec.
  <reference|A-nonrotating frame>. Since the center of mass may accelerate in
  the lab frame, a cm frame is not necessarily inertial.

  The indices <math|i> and <math|j> in this section refer only to the
  particles in the <em|system>.

  The <I|Center of mass\|reg><newterm|center of mass> of the system is a
  point whose position in the lab frame is defined by

  <\equation>
    <label|R=sum(m(i)r(i)/m>\<b-R\><cm><defn><frac|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rsub|i>|m>
  </equation>

  where <math|m> is the system mass: <math|m=<big|sum><rsub|i>m<rsub|i>>. The
  position vector of particle <math|i> in the <I|Lab
  frame\|reg><I|Frame!lab\|reg>lab frame is equal to the sum of the vector
  <math|\<b-R\><cm>> from the origin of the lab frame to the center of mass
  and the vector <math|\<b-r\><rprime|'><rsub|i>> from the center of mass to
  the particle (see Fig. <reference|fig:A-cmframe>):

  <\big-figure>
    <boxedfigure|<image|./BACK-SUP/cmframe.eps||||> <capt|Position vectors in
    a lab frame and a center-of-mass frame. Open circle: origin of lab frame;
    open triangle: center of mass; filled circle: particle <math|i>. The thin
    lines represent the Cartesian axes of the two
    frames.<label|fig:A-cmframe>>>
  </big-figure|>

  <\equation>
    <label|r(i)=R+r'(i)>\<b-r\><rsub|i>=\<b-R\><cm>+\<b-r\><rprime|'><rsub|i>
  </equation>

  We can use Eqs. <reference|R=sum(m(i)r(i)/m> and <reference|r(i)=R+r'(i)>
  to derive several relations that will be needed presently. Because the
  Cartesian axes of the lab frame and cm frame are parallel to one another
  (that is, the cm frame does not rotate), we can add vectors or form scalar
  products using the vector components measured in either frame. The time
  derivative of Eq. <reference|r(i)=R+r'(i)> is
  <math|<dif>\<b-r\><rsub|i>/<dt>=<dif>\<b-R\><cm>/<dt>+<dif>\<b-r\><rprime|'><rsub|i>/<dt>>,
  or

  <\equation>
    <label|v(i)=v+v'(i)>\<b-v\><rsub|i>=\<b-v\><cm>+\<b-v\><rprime|'><rsub|i>
  </equation>

  where the vector <math|\<b-v\><cm>> gives the velocity of the center of
  mass in the lab frame. Substitution from Eq. <reference|r(i)=R+r'(i)> into
  the sum <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rsub|i>> gives
  <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rsub|i>=m*\<b-R\><cm>+<big|sum><rsub|i>m<rsub|i>*\<b-r\><rprime|'><rsub|i>>,
  and a rearrangement of Eq. <reference|R=sum(m(i)r(i)/m> gives
  <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rsub|i>=m*\<b-R\><cm>>. Comparing
  these two equalities, we see the sum <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rprime|'><rsub|i>>
  must be zero. Therefore the first and second derivatives of
  <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rprime|'><rsub|i>> with respect to
  time must also be zero:

  <\equation>
    <label|sum m(i)v'(i)=0><big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>=0*<space|2em><big|sum><rsub|i>m<rsub|i>*<frac|<dif>\<b-v\><rprime|'><rsub|i>|<dt>>=0
  </equation>

  From Eqs. <reference|Fi=mdv(i)/dt>, <reference|F_i(eff)=>,
  <reference|F_i(eff)=F_i+F_i(accel)>, and <reference|v(i)=v+v'(i)> we obtain

  <\equation>
    <label|F_i(accel)=>\<b-F\><sups|a*c*c*e*l><rsub|i>=m<rsub|i>*<frac|<dif><around|(|\<b-v\><rprime|'><rsub|i>-\<b-v\><rsub|i>|)>|<dt>>=-m<rsub|i>*<frac|<dif>\<b-v\><cm>|<dt>>
  </equation>

  Equation <reference|F_i(accel)=> is valid only for a <I|Local
  frame!nonrotating\|reg><I|Frame!local!nonrotating\|reg>nonrotating cm
  frame.

  The difference between the energy changes of the system in the cm frame and
  the lab frame during a process is given, from Eqs. <reference|E(sys)=> and
  <reference|U=4 terms>, by

  <\equation>
    <label|delU-delE(sys)=>

    <\eqsplit>
      <tformat|<table|<row|<cell|<Del>U-<Del>E<sys>>|<cell|=<Del><around*|[|<big|sum><rsub|i><onehalf>m<rsub|i><around|(|v<rprime|'><rsub|i>|)><rsup|2>-<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>|]>>>|<row|<cell|>|<cell|<space|1em>+<Del><around*|(|<big|sum><rsub|i><varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i>-<big|sum><rsub|i><varPhi><sups|f*i*e*l*d><rsub|i>|)>+<Del><around*|(|<big|sum><rsub|i><varPhi><sups|a*c*c*e*l><rsub|i>|)>>>>>
    </eqsplit>
  </equation>

  We will find new expressions for the three terms on the right side of this
  equation.

  The first term is the difference between the total kinetic energy changes
  measured in the cm frame and lab frame. We can derive an important
  relation, well known in classical mechanics, for the kinetic energy in the
  lab frame:

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>>|<cell|=<big|sum><rsub|i><onehalf>m<rsub|i>*<around|(|\<b-v\><cm>+\<b-v\><rprime|'><rsub|i>|)><dotprod><around|(|\<b-v\><cm>+\<b-v\><rprime|'><rsub|i>|)>>>|<row|<cell|>|<cell|=<onehalf>m*v<rsup|2><cm>+<big|sum><rsub|i><onehalf>m<rsub|i><around*|(|v<rprime|'><rsub|i>|)><rsup|2>+\<b-v\><cm><dotprod><around*|(|<big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>|)>>>>>
    </eqsplit>
  </equation>

  The quantity <math|<onehalf>m*v<rsup|2><cm>> is the bulk kinetic energy of
  the system in the lab frame\Vthat is, the translational energy of a body
  having the same mass as the system and moving with the center of mass. The
  sum <math|<big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>> is zero
  (Eq. <reference|sum m(i)v'(i)=0>). Therefore the first term on the right
  side of Eq. <reference|delU-delE(sys)=> is

  <\equation>
    <Del><around*|[|<big|sum><rsub|i><onehalf>m<rsub|i><around|(|v<rprime|'><rsub|i>|)><rsup|2>-<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>|]>=-<Del><around*|(|<onehalf>m*v<rsup|2><cm>|)>
  </equation>

  Only by using a <I|Local frame!nonrotating\|reg><I|Frame!local!nonrotating\|reg>nonrotating
  local frame moving with the center of mass is it possible to derive such a
  simple relation among these kinetic energy quantities.

  The second term on the right side of Eq. <reference|delU-delE(sys)=>, with
  the help of Eqs. <reference|del phi_i(field)>, <reference|del
  phi'(field)_1=>, and <reference|r(i)=R+r'(i)> becomes

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<Del><around*|(|<big|sum><rsub|i><varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i>-<big|sum><rsub|i><varPhi><sups|f*i*e*l*d><rsub|i>|)>>|<cell|=-<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sups|f*i*e*l*d><rsub|i><dotprod><dif><around|(|\<b-r\>*'<rsub|i>-\<b-r\><rsub|i>|)>>>|<row|<cell|>|<cell|=<big|int><space|-0.17em><around*|(|<big|sum><rsub|i>\<b-F\><sups|f*i*e*l*d><rsub|i>|)><dotprod><dif>\<b-R\><cm>>>>>
    </eqsplit>
  </equation>

  Suppose the only external field is gravitational:
  <math|\<b-F\><sups|f*i*e*l*d><rsub|i>=\<b-F\><sups|g*r*a*v><rsub|i>=-m<rsub|i>*g*\<b-e\><rsub|z>>
  where <math|\<b-e\><rsub|z>> is a unit vector in the vertical (upward)
  <math|+z> direction. In this case we obtain

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<Del><around*|(|<big|sum><rsub|i><varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i>-<big|sum><rsub|i><varPhi><sups|f*i*e*l*d><rsub|i>|)>>|<cell|=-<big|int><space|-0.17em><around*|(|<big|sum><rsub|i>m<rsub|i>|)>*g*\<b-e\><rsub|z><dotprod><dif>\<b-R\><cm>>>|<row|<cell|>|<cell|=-m*g*<big|int><space|-0.17em>\<b-e\><rsub|z><dotprod><dif>\<b-R\><cm>=-m*g*<big|int><space|-0.17em><dif>z<cm>>>|<row|<cell|>|<cell|=-m*g<Del>z<cm>>>>>
    </eqsplit>
  </equation>

  where <math|z<cm>> is the elevation of the center of mass in the lab frame.
  The quantity <math|m*g<Del>z<cm>> is the change in the system's bulk
  gravitational potential energy in the lab frame\Vthe change in the
  potential energy of a body of mass <math|m> undergoing the same change in
  elevation as the system's center of mass.

  The third term on the right side of Eq. <reference|delU-delE(sys)=> can be
  shown to be zero when the local frame is a cm frame. The derivation uses
  Eqs. <reference|del phi(accel)_i=> and <reference|F_i(accel)=> and is as
  follows:

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<Del><around*|(|<big|sum><rsub|i><varPhi><sups|a*c*c*e*l><rsub|i>|)>>|<cell|=-<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sups|a*c*c*e*l><rsub|i><dotprod><dif>\<b-r\><rprime|'><rsub|i>=<big|sum><rsub|i><big|int><space|-0.17em>m<rsub|i>*<frac|<dif>\<b-v\><cm>|<dt>><dotprod><dif>\<b-r\><rprime|'><rsub|i>>>|<row|<cell|>|<cell|=<big|int><space|-0.17em><around*|(|<big|sum><rsub|i>m<rsub|i>*<frac|<dif>\<b-r\><rprime|'><rsub|i>|<dt>>|)><dotprod><dif>\<b-v\><cm>=<big|int><space|-0.17em><around*|(|<big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>|)><dotprod><dif>\<b-v\><cm>>>>>
    </eqsplit>
  </equation>

  The sum <math|<big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>> in the
  integrand of the last integral on the right side is zero (Eq.
  <reference|sum m(i)v'(i)=0>) so the integral is also zero.

  With these substitutions, Eq. <reference|delU-delE(sys)=> becomes
  <math|<Del>U-<Del>E<sys>=-<onehalf>m<Del><space|-0.17em><around*|(|v<rsup|2><cm>|)>-m*g<Del>z<cm>>.
  Since <math|<Del>U-<Del>E<sys>> is equal to <math|w-w<lab>> when the local
  frame is <I|Local frame!nonrotating\|reg><I|Frame!local!nonrotating\|reg>nonrotating
  (Eq. <reference|delU-delE(sys)=w-w(lab)>), we have

  <\equation>
    <label|w-w(lab) (cm)>w-w<lab>=-<onehalf>m<Del><space|-0.17em><around*|(|v<rsup|2><cm>|)>-m*g<Del>z<cm>
  </equation>

  <I|Center-of-mass frame\|)><I|Frame!center-of-mass\|)>

  <section|Rotating Local Frame><label|A-rotating frame>

  <I|Rotating local frame\|(><I|Frame!local!rotating\|(>A rotating local
  frame is the most convenient to use in treating the thermodynamics of a
  system with rotational motion in a lab frame. A good example of such a
  system is a solution in a sample cell of a spinning ultracentrifuge (Sec.
  <reference|9-centrifuge>).

  We will make several simplifying assumptions. The rotating local frame has
  the same origin and the same <math|z> axis as the lab frame, as shown in
  Fig. <reference|fig:A-rotating frame>.

  <\big-figure>
    <boxedfigure|<image|./BACK-SUP/rotation.eps||||> <capt|Relation between
    the Cartesian axes <math|x>, <math|y>, <math|z> of a lab frame and the
    axes <math|x<rprime|'>>, <math|y<rprime|'>>, <math|z> of a rotating local
    frame. The filled circle represents particle
    <math|i>.<label|fig:A-rotating frame>>>
  </big-figure|>

  The <math|z> axis is vertical and is the axis of rotation for the local
  frame. The local frame rotates with constant angular velocity
  <math|\<omega\>=<dif>\<vartheta\>/<dt>>, where <math|\<vartheta\>> is the
  angle between the <math|x> axis of the lab frame and the <math|x<rprime|'>>
  axis of the local frame. There is a gravitational force in the <math|-z>
  direction; this force is responsible for the only external field, whose
  potential energy change in the local frame during a process is
  <math|<Del><varPhi><rsub|i><sups|<math|<rprime|'>><space|0.17em>grav>=m<rsub|i>*g<space|0.17em><Del>z<rsub|i>>
  (Eq. <reference|delPhi_i(grav)=m_i g del z_i>).

  The contribution to the effective force acting on particle <math|i> due to
  acceleration when <math|\<omega\>> is constant can be shown to be given
  by<footnote|The derivation, using a different notation, can be found in
  Ref. <cite|marion-95>, Chap. 10.>

  <\equation>
    \<b-F\><sups|a*c*c*e*l><rsub|i>=\<b-F\><sups|c*e*n*t*r><rsub|i>+\<b-F\><sups|C*o*r><rsub|i>
  </equation>

  where <math|\<b-F\><sups|c*e*n*t*r><rsub|i>> is the so-called
  <I|Force!centrifugal\|reg><I|Centrifugal force\|reg><em|centrifugal force>
  and <math|\<b-F\><sups|C*o*r><rsub|i>> is called the
  <I|Force!Coriolis\|reg><I|Coriolis force\|reg><em|Coriolis force>.

  The centrifugal force acting on particle <math|i> is given by

  <\equation>
    \<b-F\><sups|c*e*n*t*r><rsub|i>=m<rsub|i>*\<omega\><rsup|2>*r<rsub|i>*\<b-e\><rsub|i>
  </equation>

  Here <math|r<rsub|i>> is the radial distance of the particle from the axis
  of rotation, and <math|\<b-e\><rsub|i>> is a unit vector pointing from the
  particle in the direction away from the axis of rotation (see Fig.
  <reference|fig:A-rotating frame>). The direction of <math|\<b-e\><rsub|i>>
  in the local frame changes as the particle moves in this frame.

  The Coriolis force acting on particle <math|i> arises only when the
  particle is moving relative to the rotating frame. This force has magnitude
  <math|2*m<rsub|i>*\<omega\>*v<rprime|'><rsub|i>> and is directed
  perpendicular to both <math|\<b-v\><rprime|'><rsub|i>> and the axis of
  rotation.

  In a rotating local frame, the work during a process is not the same as
  that measured in a lab frame. The heats <math|q> and <math|q<lab>> are not
  equal to one another as they are when the local frame is nonrotating, nor
  can general expressions using macroscopic quantities be written for
  <math|<Del>U-<Del>E<sys>> and <math|w-w<lab>>. <I|Rotating local
  frame\|)><I|Frame!local!rotating\|)>

  <section|Earth-Fixed Reference Frame><label|A-earth-fixed frame>

  <I|Reference frame!earth fixed@earth-fixed\|(><I|Frame!reference!earth
  fixed@earth-fixed\|(>In the preceding sections of Appendix
  <reference|app:forces>, we assumed that a lab frame whose coordinate axes
  are fixed relative to the earth's surface is an inertial frame. This is not
  exactly true, because the earth spins about its axis and circles the sun.
  Small correction terms, a centrifugal force
  <I|Force!centrifugal\|reg><I|Centrifugal force\|reg>and a Coriolis force,
  <I|Force!Coriolis\|reg><I|Coriolis force\|reg>are needed to obtain the
  effective net force acting on particle <math|i> that allows Newton's second
  law to be obeyed exactly in the lab frame.<footnote|Ref.
  <cite|goldstein-50>, Sec. 4--9.>

  The earth's movement around the sun makes only a minor contribution to
  these correction terms. The Coriolis force, which occurs only if the
  particle is moving in the lab frame, is usually so small that it can be
  neglected.

  This leaves as the only significant correction the centrifugal force on the
  particle from the earth's spin about its axis. This force is directed
  perpendicular to the earth's axis and has magnitude
  <math|m<rsub|i>*\<omega\><rsup|2>*r<rsub|i>>, where <math|\<omega\>> is the
  earth's angular velocity, <math|m<rsub|i>> is the particle's mass, and
  <math|r<rsub|i>> is the radial distance of the particle from the earth's
  axis. The correction can be treated as a small modification of the
  gravitational force <I|Force!gravitational\|reg><I|Gravitational!force\|reg>acting
  on the particle that is at most, at the equator, only about 0.3% of the
  actual gravitational force. Not only is the correction small, but it is
  completely taken into account in the lab frame when we calculate the
  effective gravitational force from <math|\<b-F\><sups|g*r*a*v><rsub|i>=-m<rsub|i>*g*\<b-e\><rsub|z>>,
  where <math|g> is the acceleration of free fall and <math|\<b-e\><rsub|z>>
  is a unit vector in the <math|+z> (upward) direction. The value of <math|g>
  is an experimental quantity that includes the effect of
  <math|\<b-F\><sups|c*e*n*t*r><rsub|i>>, and thus depends on latitude as
  well as elevation above the earth's surface. Since
  <math|\<b-F\><sups|g*r*a*v><rsub|i>> depends only on position, we can treat
  gravity as a conservative force field in the earth-fixed lab frame.
  <I|Reference frame!earth fixed@earth-fixed\|)><I|Frame!reference!earth
  fixed@earth-fixed\|)>

  <chapter|Standard Molar Thermodynamic Properties><label|app:props>

  <pagestyle|nosectioninheader>

  <I|Standard molar!properties, values of\|(>The values in this table are for
  a temperature of <math|298.15<units|K>> (<math|25.00<units|<degC>>>) and
  the standard pressure <math|p<st>=1<units|b*a*r>>. Solute standard states
  are based on molality. A crystalline solid is denoted by cr.

  Most of the values in this table come from a project of the
  <I|CODATA\|reg>Committee on Data for Science and Technology (CODATA) to
  establish a set of recommended, internally consistent values of
  thermodynamic properties. The values of <math|<Delsub|f>H<st>> and
  <math|S<m><st>> shown with uncertainties are values recommended by
  CODATA.<footnote|Ref. <cite|cox-89>; also available online at
  <slink|http://www.codata.info/resources/databases/key1.html>.>

  <\longtable|c|l r@c@l r@c@l r@c@l @>
    <\tformat>
      <\table|<row|<cell|<hline> <multicolumn|1|@l|Species>>|<cell|<multicolumn|3|c|<math|<D><frac|<Delsub|f>H<st>|<tx|k*J*<space|0.17em>m*o*l<per>>>>>>|<cell|<multicolumn|3|c|<math|<D><frac|S<m><st>|<tx|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>>>|<cell|<multicolumn|3|c|<math|<D><frac|<Delsub|f>G<st>|<tx|k*J*<space|0.17em>m*o*l<per>>>>>
      <rule*|-4mm|0in|10mm>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|0.5ex>
      <hline>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|-1.5ex>
      <endfirsthead> <multicolumn|10|@l|<with|font-size|0.84|(continued from
      previous page)>>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|0.5ex>
      <hline> <multicolumn|1|@l|Species>>|<cell|<multicolumn|3|c|<math|<D><frac|<Delsub|f>H<st>|<tx|k*J*<space|0.17em>m*o*l<per>>>>>>|<cell|<multicolumn|3|c|<math|<D><frac|S<m><st>|<tx|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>>>|<cell|<multicolumn|3|c|<math|<D><frac|<Delsub|f>G<st>|<tx|k*J*<space|0.17em>m*o*l<per>>>>>
      <rule*|-4mm|0in|10mm>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|0.5ex>
      <hline>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|-1.5ex>
      <endhead>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|-3.4mm>
      <hline> <endfoot>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|-3.8ex>
      <multicolumn|4|@l|<em|Inorganic substances>>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|Ag(cr)>|<cell|0>|<cell|>|<cell|>|<cell|42>|<cell|.>|<cell|<math|55\<pm\>0.20>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|AgCl(cr)>|<cell|<math|-127>>|<cell|.>|<cell|<math|01\<pm\>0.05>>|<cell|96>|<cell|.>|<cell|<math|25\<pm\>0.20>>|<cell|<math|-109>>|<cell|.>|<cell|<math|77>>>|<row|<cell|C(cr,<space|0.17em>graphite)>|<cell|0>|<cell|>|<cell|>|<cell|5>|<cell|.>|<cell|<math|74\<pm\>0.10>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|CO(g)>|<cell|<math|-110>>|<cell|.>|<cell|<math|53\<pm\>0.17>>|<cell|197>|<cell|.>|<cell|<math|660\<pm\>0.004>>|<cell|<math|-137>>|<cell|.>|<cell|17>>|<row|<cell|CO<rsub|<math|2>>(g)>|<cell|<math|-393>>|<cell|.>|<cell|<math|51\<pm\>0.13>>|<cell|213>|<cell|.>|<cell|<math|785\<pm\>0.010>>|<cell|<math|-394>>|<cell|.>|<cell|41>>|<row|<cell|Ca(cr)>|<cell|0>|<cell|>|<cell|>|<cell|41>|<cell|.>|<cell|<math|59\<pm\>0.40>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|CaCO<rsub|<math|3>>(cr,<space|0.17em>calcite)>|<cell|<math|-1206>>|<cell|.>|<cell|9>|<cell|92>|<cell|.>|<cell|9>|<cell|<math|-1128>>|<cell|.>|<cell|8>>|<row|<cell|CaO(cr)>|<cell|<math|-634>>|<cell|.>|<cell|<math|92\<pm\>0.90>>|<cell|38>|<cell|.>|<cell|<math|1\<pm\>0.4>>|<cell|<math|-603>>|<cell|.>|<cell|31>>|<row|<cell|Cl<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|223>|<cell|.>|<cell|<math|081\<pm\>0.010>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|F<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|202>|<cell|.>|<cell|<math|791\<pm\>0.005>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|H<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|130>|<cell|.>|<cell|<math|680\<pm\>0.003>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|HCl(g)>|<cell|<math|-92>>|<cell|.>|<cell|<math|31\<pm\>0.10>>|<cell|186>|<cell|.>|<cell|<math|902\<pm\>0.005>>|<cell|<math|-95>>|<cell|.>|<cell|30>>|<row|<cell|HF(g)>|<cell|<math|-273>>|<cell|.>|<cell|<math|30\<pm\>0.70>>|<cell|173>|<cell|.>|<cell|<math|779\<pm\>0.003>>|<cell|<math|-275>>|<cell|.>|<cell|40>>|<row|<cell|HI(g)>|<cell|26>|<cell|.>|<cell|<math|50\<pm\>0.10>>|<cell|206>|<cell|.>|<cell|<math|590\<pm\>0.004>>|<cell|1>|<cell|.>|<cell|70>>|<row|<cell|H<rsub|<math|2>>O(l)>|<cell|<math|-285>>|<cell|.>|<cell|<math|830\<pm\>0.040>>|<cell|69>|<cell|.>|<cell|<math|95\<pm\>0.03>>|<cell|<math|-237>>|<cell|.>|<cell|16>>|<row|<cell|H<rsub|<math|2>>O(g)>|<cell|<math|-241>>|<cell|.>|<cell|<math|826\<pm\>0.040>>|<cell|188>|<cell|.>|<cell|<math|835\<pm\>0.010>>|<cell|<math|-228>>|<cell|.>|<cell|58>>|<row|<cell|H<rsub|<math|2>>S(g)>|<cell|<math|-20>>|<cell|.>|<cell|<math|6\<pm\>0.5>>|<cell|205>|<cell|.>|<cell|<math|81\<pm\>0.05>>|<cell|<math|-33>>|<cell|.>|<cell|44>>|<row|<cell|Hg(l)>|<cell|0>|<cell|>|<cell|>|<cell|75>|<cell|.>|<cell|<math|90\<pm\>0.12>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|Hg(g)>|<cell|61>|<cell|.>|<cell|<math|38\<pm\>0.04>>|<cell|174>|<cell|.>|<cell|<math|971\<pm\>0.005>>|<cell|31>|<cell|.>|<cell|84>>|<row|<cell|HgO(cr,<space|0.17em>red)>|<cell|<math|-90>>|<cell|.>|<cell|<math|79\<pm\>0.12>>|<cell|70>|<cell|.>|<cell|<math|25\<pm\>0.30>>|<cell|<math|-58>>|<cell|.>|<cell|54>>|<row|<cell|Hg<rsub|<math|2>>Cl<rsub|<math|2>>(cr)>|<cell|<math|-265>>|<cell|.>|<cell|<math|37\<pm\>0.40>>|<cell|191>|<cell|.>|<cell|<math|6\<pm\>0.8>>|<cell|<math|-210>>|<cell|.>|<cell|72>>|<row|<cell|I<rsub|<math|2>>(cr)>|<cell|0>|<cell|>|<cell|>|<cell|116>|<cell|.>|<cell|<math|14\<pm\>0.30>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|K(cr)>|<cell|0>|<cell|>|<cell|>|<cell|64>|<cell|.>|<cell|<math|68\<pm\>0.20>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|KI(cr)>|<cell|<math|-327>>|<cell|.>|<cell|90>|<cell|106>|<cell|.>|<cell|37>|<cell|<math|-323>>|<cell|.>|<cell|03>>|<row|<cell|KOH(cr)>|<cell|<math|-424>>|<cell|.>|<cell|72>|<cell|78>|<cell|.>|<cell|90>|<cell|<math|-378>>|<cell|.>|<cell|93>>|<row|<cell|N<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|191>|<cell|.>|<cell|<math|609\<pm\>0.004>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|NH<rsub|<math|3>>(g)>|<cell|<math|-45>>|<cell|.>|<cell|<math|94\<pm\>0.35>>|<cell|192>|<cell|.>|<cell|<math|77\<pm\>0.05>>|<cell|<math|-16>>|<cell|.>|<cell|41>>|<row|<cell|NO<rsub|<math|2>>(g)>|<cell|33>|<cell|.>|<cell|10>|<cell|240>|<cell|.>|<cell|04>|<cell|51>|<cell|.>|<cell|22>>|<row|<cell|N<rsub|<math|2>>O<rsub|<math|4>>(g)>|<cell|9>|<cell|.>|<cell|08>|<cell|304>|<cell|.>|<cell|38>|<cell|97>|<cell|.>|<cell|72>>|<row|<cell|Na(cr)>|<cell|0>|<cell|>|<cell|>|<cell|51>|<cell|.>|<cell|<math|30\<pm\>0.20>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|NaCl(cr)>|<cell|<math|-411>>|<cell|.>|<cell|12>|<cell|72>|<cell|.>|<cell|11>|<cell|<math|-384>>|<cell|.>|<cell|02>>|<row|<cell|O<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|205>|<cell|.>|<cell|<math|152\<pm\>0.005>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|O<rsub|<math|3>>(g)>|<cell|142>|<cell|.>|<cell|67>|<cell|238>|<cell|.>|<cell|92>|<cell|163>|<cell|.>|<cell|14>>|<row|<cell|P(cr,<space|0.17em>white)>|<cell|0>|<cell|>|<cell|>|<cell|41>|<cell|.>|<cell|<math|09\<pm\>0.25>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|S(cr,<space|0.17em>rhombic)>|<cell|0>|<cell|>|<cell|>|<cell|32>|<cell|.>|<cell|<math|054\<pm\>0.050>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|SO<rsub|<math|2>>(g)>|<cell|<math|-296>>|<cell|.>|<cell|<math|81\<pm\>0.20>>|<cell|248>|<cell|.>|<cell|<math|223\<pm\>0.050>>|<cell|<math|-300>>|<cell|.>|<cell|09>>|<row|<cell|Si(cr)>|<cell|0>|<cell|>|<cell|>|<cell|18>|<cell|.>|<cell|<math|81\<pm\>0.08>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|SiF<rsub|<math|4>>(g)>|<cell|<math|-1615>>|<cell|.>|<cell|<math|0\<pm\>0.8>>|<cell|282>|<cell|.>|<cell|<math|76\<pm\>0.50>>|<cell|<math|-1572>>|<cell|.>|<cell|8>>|<row|<cell|SiO<rsub|<math|2>>(cr,<space|0.17em><math|\<alpha\>>-quartz)>|<cell|<math|-910>>|<cell|.>|<cell|<math|7\<pm\>1.0>>|<cell|41>|<cell|.>|<cell|<math|46\<pm\>0.20>>|<cell|<math|-856>>|<cell|.>|<cell|3>>|<row|<cell|<vspace*|1mm>
      Zn(cr)>|<cell|0>|<cell|>|<cell|>|<cell|41>|<cell|.>|<cell|<math|63\<pm\>0.15>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|<vspace*|1mm>
      ZnO(cr)>|<cell|<math|-350>>|<cell|.>|<cell|<math|46\<pm\>0.27>>|<cell|43>|<cell|.>|<cell|<math|65\<pm\>0.40>>|<cell|<math|-320>>|<cell|.>|<cell|48>>>
        <\row>
          <\cell>
            <vspace*|1mm>

            <hline><multicolumn|4|@l|<em|Organic compounds>> <tablestrut>
          </cell>
        </row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>
      <|table|<row|<cell|CH<rsub|<math|4>>(g)>|<cell|<math|-74>>|<cell|.>|<cell|87>|<cell|186>|<cell|.>|<cell|25>|<cell|<math|-50>>|<cell|.>|<cell|77>>|<row|<cell|CH<rsub|<math|3>>OH(l)>|<cell|<math|-238>>|<cell|.>|<cell|9>|<cell|127>|<cell|.>|<cell|2>|<cell|<math|-166>>|<cell|.>|<cell|6>>|<row|<cell|CH<rsub|<math|3>>CH<rsub|<math|2>>OH(l)>|<cell|<math|-277>>|<cell|.>|<cell|0>|<cell|159>|<cell|.>|<cell|9>|<cell|<math|-173>>|<cell|.>|<cell|8>>|<row|<cell|C<rsub|<math|2>>H<rsub|<math|2>>(g)>|<cell|226>|<cell|.>|<cell|73>|<cell|200>|<cell|.>|<cell|93>|<cell|209>|<cell|.>|<cell|21>>|<row|<cell|C<rsub|<math|2>>H<rsub|<math|4>>(g)>|<cell|52>|<cell|.>|<cell|47>|<cell|219>|<cell|.>|<cell|32>|<cell|68>|<cell|.>|<cell|43>>|<row|<cell|C<rsub|<math|2>>H<rsub|<math|6>>(g)>|<cell|<math|-83>>|<cell|.>|<cell|85>|<cell|229>|<cell|.>|<cell|6>|<cell|<math|-32>>|<cell|.>|<cell|00>>|<row|<cell|C<rsub|<math|3>>H<rsub|<math|8>>(g)>|<cell|<math|-104>>|<cell|.>|<cell|7>|<cell|270>|<cell|.>|<cell|31>|<cell|<math|-24>>|<cell|.>|<cell|3>>|<row|<cell|C<rsub|<math|6>>H<rsub|<math|6>>(l,<space|0.17em>benzene)>|<cell|49>|<cell|.>|<cell|04>|<cell|173>|<cell|.>|<cell|26>|<cell|124>|<cell|.>|<cell|54>>>
        <\row>
          <\cell>
            <vspace*|1mm>

            <hline><multicolumn|4|@l|<em|Ionic solutes>> <tablestrut>
          </cell>
        </row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>
      </table|<row|<cell|Ag<rsup|<math|+>>(aq)>|<cell|105>|<cell|.>|<cell|<math|79\<pm\>0.08>>|<cell|73>|<cell|.>|<cell|<math|45\<pm\>0.40>>|<cell|77>|<cell|.>|<cell|10>>|<row|<cell|CO<math|<rsub|3><rsup|2->>(aq)>|<cell|<math|-675>>|<cell|.>|<cell|<math|23\<pm\>0.25>>|<cell|<math|-50>>|<cell|.>|<cell|<math|0\<pm\>1.0>>|<cell|<math|-527>>|<cell|.>|<cell|90>>|<row|<cell|Ca<rsup|<math|2+>>(aq)>|<cell|<math|-543>>|<cell|.>|<cell|<math|0\<pm\>1.0>>|<cell|<math|-56>>|<cell|.>|<cell|<math|2\<pm\>1.0>>|<cell|<math|-552>>|<cell|.>|<cell|8>>|<row|<cell|Cl<rsup|<math|->>(aq)>|<cell|<math|-167>>|<cell|.>|<cell|<math|08\<pm\>0.10>>|<cell|56>|<cell|.>|<cell|<math|60\<pm\>0.20>>|<cell|<math|-131>>|<cell|.>|<cell|22>>|<row|<cell|F<rsup|<math|->>(aq)>|<cell|<math|-335>>|<cell|.>|<cell|<math|35\<pm\>0.65>>|<cell|<math|-13>>|<cell|.>|<cell|<math|8\<pm\>0.8>>|<cell|<math|-281>>|<cell|.>|<cell|52>>|<row|<cell|H<rsup|<math|+>>(aq)>|<cell|0>|<cell|>|<cell|>|<cell|0>|<cell|>|<cell|>|<cell|0>|<cell|>|<cell|>>|<row|<cell|HCO<math|<rsub|3><rsup|->>(aq)>|<cell|<math|-689>>|<cell|.>|<cell|<math|93\<pm\>2.0>>|<cell|98>|<cell|.>|<cell|<math|4\<pm\>0.5>>|<cell|<math|-586>>|<cell|.>|<cell|90>>|<row|<cell|HS<rsup|<math|->>(aq)>|<cell|<math|-16>>|<cell|.>|<cell|<math|3\<pm\>1.5>>|<cell|67>|<cell|>|<cell|<math|\<pm\><space|0.17em>5>>|<cell|12>|<cell|.>|<cell|2>>|<row|<cell|HSO<math|<rsub|4><rsup|->>(aq)>|<cell|<math|-886>>|<cell|.>|<cell|<math|9\<pm\>1.0>>|<cell|131>|<cell|.>|<cell|<math|7\<pm\>3.0>>|<cell|<math|-755>>|<cell|.>|<cell|4>>|<row|<cell|Hg<math|<rsub|2><rsup|2+>>(aq)>|<cell|166>|<cell|.>|<cell|<math|87\<pm\>0.50>>|<cell|65>|<cell|.>|<cell|<math|74\<pm\>0.80>>|<cell|153>|<cell|.>|<cell|57>>|<row|<cell|I<rsup|<math|->>(aq)>|<cell|<math|-56>>|<cell|.>|<cell|<math|78\<pm\>0.05>>|<cell|106>|<cell|.>|<cell|<math|45\<pm\>0.30>>|<cell|<math|-51>>|<cell|.>|<cell|72>>|<row|<cell|K<rsup|<math|+>>(aq)>|<cell|<math|-252>>|<cell|.>|<cell|<math|14\<pm\>0.08>>|<cell|101>|<cell|.>|<cell|<math|20\<pm\>0.20>>|<cell|<math|-282>>|<cell|.>|<cell|52>>|<row|<cell|NH<math|<rsub|4><rsup|+>>(aq)>|<cell|<math|-133>>|<cell|.>|<cell|<math|26\<pm\>0.25>>|<cell|111>|<cell|.>|<cell|<math|17\<pm\>0.40>>|<cell|<math|-79>>|<cell|.>|<cell|40>>|<row|<cell|NO<math|<rsub|3><rsup|->>(aq)>|<cell|<math|-206>>|<cell|.>|<cell|<math|85\<pm\>0.40>>|<cell|146>|<cell|.>|<cell|<math|70\<pm\>0.40>>|<cell|<math|-110>>|<cell|.>|<cell|84>>|<row|<cell|Na<rsup|<math|+>>(aq)>|<cell|<math|-240>>|<cell|.>|<cell|<math|34\<pm\>0.06>>|<cell|58>|<cell|.>|<cell|<math|45\<pm\>0.15>>|<cell|<math|-261>>|<cell|.>|<cell|90>>|<row|<cell|OH<rsup|<math|->>(aq)>|<cell|<math|-230>>|<cell|.>|<cell|<math|015\<pm\>0.040>>|<cell|<math|-10>>|<cell|.>|<cell|<math|90\<pm\>0.20>>|<cell|<math|-157>>|<cell|.>|<cell|24>>|<row|<cell|S<rsup|<math|2->>(aq)>|<cell|33>|<cell|.>|<cell|1>|<cell|<math|-14>>|<cell|.>|<cell|6>|<cell|86>|<cell|.>|<cell|0>>|<row|<cell|SO<math|<rsub|4><rsup|2->>(aq)>|<cell|<math|-909>>|<cell|.>|<cell|<math|34\<pm\>0.40>>|<cell|18>|<cell|.>|<cell|<math|50\<pm\>0.40>>|<cell|<math|-744>>|<cell|.>|<cell|00>>|<row|<cell|<new-line>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>>
    </tformat>
  </longtable>

  <I|Standard molar!properties, values of\|)>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|A-earth-fixed frame|<tuple|10|?>>
    <associate|A-forces between particles|<tuple|1|?>>
    <associate|A-local frame|<tuple|6|?>>
    <associate|A-macr work|<tuple|4|?>>
    <associate|A-nonrotating frame|<tuple|7|?>>
    <associate|A-rotating frame|<tuple|9|?>>
    <associate|A-work done|<tuple|5|?>>
    <associate|Del E(sys)=2 terms|<tuple|47|?>>
    <associate|Del U=q+w|<tuple|57|?>>
    <associate|DelE(sys)=|<tuple|46|?>>
    <associate|E(sys)=|<tuple|38|?>>
    <associate|E(tot)(grouped)|<tuple|37|?>>
    <associate|E(tot)2=E(tot)1|<tuple|31|?>>
    <associate|E(tot)=E(sys)+...|<tuple|39|?>>
    <associate|E(tot)=KE+PE|<tuple|32|?>>
    <associate|E(tot)=sums|<tuple|33|?>>
    <associate|F(ij)dr(i)+F(ji)dr(j)|<tuple|27|?>>
    <associate|F_i(accel)=|<tuple|69|?>>
    <associate|F_i(eff)=|<tuple|51|?>>
    <associate|F_i(eff)=F_i+F_i(accel)|<tuple|52|?>>
    <associate|F_i=sum...|<tuple|40|?>>
    <associate|Fi=mdv(i)/dt|<tuple|22|?>>
    <associate|Fi=sum(Fij)|<tuple|21|?>>
    <associate|R=sum(m(i)r(i)/m|<tuple|65|?>>
    <associate|U=4 terms|<tuple|55|?>>
    <associate|W(tot)=-Del(PE)|<tuple|30|?>>
    <associate|W(tot)=Del(KE)|<tuple|25|?>>
    <associate|W(tot)=sum(i)sum(j)int|<tuple|26|?>>
    <associate|W(tot)=sum(i)sum(j\<gtr\>i)|<tuple|28|?>>
    <associate|Wi=int F(i)dr(i)|<tuple|23|?>>
    <associate|a=(df/dx)_y,z etc.|<tuple|4|?>>
    <associate|app-line integrals|<tuple|4|?>>
    <associate|app:Legendre|<tuple|4|?>>
    <associate|app:SI|<tuple|1|?>>
    <associate|app:abbrev|<tuple|4|?>>
    <associate|app:abbrev-processes|<tuple|2|?>>
    <associate|app:calc|<tuple|5|?>>
    <associate|app:const|<tuple|2|?>>
    <associate|app:forces|<tuple|7|?>>
    <associate|app:int of tot diff|<tuple|3|?>>
    <associate|app:props|<tuple|8|?>>
    <associate|app:state|<tuple|6|?>>
    <associate|app:sym|<tuple|3|?>>
    <associate|app:total diff|<tuple|2|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|2|?>>
    <associate|auto-11|<tuple|3|?>>
    <associate|auto-12|<tuple|4|?>>
    <associate|auto-13|<tuple|6|?>>
    <associate|auto-14|<tuple|1|?>>
    <associate|auto-15|<tuple|2|?>>
    <associate|auto-16|<tuple|1|?>>
    <associate|auto-17|<tuple|3|?>>
    <associate|auto-18|<tuple|4|?>>
    <associate|auto-19|<tuple|7|?>>
    <associate|auto-2|<tuple|2|?>>
    <associate|auto-20|<tuple|1|?>>
    <associate|auto-21|<tuple|2|?>>
    <associate|auto-22|<tuple|2|?>>
    <associate|auto-23|<tuple|3|?>>
    <associate|auto-24|<tuple|4|?>>
    <associate|auto-25|<tuple|3|?>>
    <associate|auto-26|<tuple|5|?>>
    <associate|auto-27|<tuple|6|?>>
    <associate|auto-28|<tuple|4|?>>
    <associate|auto-29|<tuple|7|?>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-30|<tuple|8|?>>
    <associate|auto-31|<tuple|5|?>>
    <associate|auto-32|<tuple|9|?>>
    <associate|auto-33|<tuple|6|?>>
    <associate|auto-34|<tuple|10|?>>
    <associate|auto-35|<tuple|8|?>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|1|?>>
    <associate|auto-6|<tuple|2|?>>
    <associate|auto-7|<tuple|3|?>>
    <associate|auto-8|<tuple|5|?>>
    <associate|auto-9|<tuple|1|?>>
    <associate|del phi|<tuple|29|?>>
    <associate|del phi'(field)_1=|<tuple|53|?>>
    <associate|del phi(accel)_i=|<tuple|54|?>>
    <associate|del phi(field)|<tuple|44|?>>
    <associate|del phi_i(field)|<tuple|34|?>>
    <associate|delE(sys)=q(lab)+w(lab)|<tuple|49|?>>
    <associate|delPhi_i(grav)=m_i g del z_i|<tuple|35|?>>
    <associate|delU-delE(sys)=|<tuple|70|?>>
    <associate|delU-delE(sys)=w-w(lab)|<tuple|62|?>>
    <associate|df(x,y,x)=adx+bdy+cdz|<tuple|13|?>>
    <associate|df1=-xda+bdy+cdz|<tuple|16|?>>
    <associate|df2=|<tuple|20|?>>
    <associate|df=(2xy)dx+...|<tuple|8|?>>
    <associate|df=(df/dx)dx+.+.+...|<tuple|2|?>>
    <associate|df=adx+bdy+cdz|<tuple|3|?>>
    <associate|f1=f-ax|<tuple|14|?>>
    <associate|f2=f-by-cz|<tuple|19|?>>
    <associate|f=|<tuple|9|?>>
    <associate|fig:A-boundary|<tuple|3|?>>
    <associate|fig:A-cmframe|<tuple|5|?>>
    <associate|fig:A-forces|<tuple|2|?>>
    <associate|fig:A-localframe|<tuple|4|?>>
    <associate|fig:A-rotating frame|<tuple|6|?>>
    <associate|fig:F-total differential|<tuple|1|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-10|<tuple|10|?>>
    <associate|footnote-11|<tuple|11|?>>
    <associate|footnote-12|<tuple|12|?>>
    <associate|footnote-13|<tuple|13|?>>
    <associate|footnote-14|<tuple|14|?>>
    <associate|footnote-15|<tuple|15|?>>
    <associate|footnote-16|<tuple|16|?>>
    <associate|footnote-17|<tuple|17|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnote-5|<tuple|5|?>>
    <associate|footnote-6|<tuple|6|?>>
    <associate|footnote-7|<tuple|7|?>>
    <associate|footnote-8|<tuple|8|?>>
    <associate|footnote-9|<tuple|9|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-10|<tuple|10|?>>
    <associate|footnr-11|<tuple|11|?>>
    <associate|footnr-12|<tuple|12|?>>
    <associate|footnr-13|<tuple|13|?>>
    <associate|footnr-14|<tuple|14|?>>
    <associate|footnr-15|<tuple|15|?>>
    <associate|footnr-16|<tuple|16|?>>
    <associate|footnr-17|<tuple|17|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
    <associate|footnr-5|<tuple|5|?>>
    <associate|footnr-6|<tuple|6|?>>
    <associate|footnr-7|<tuple|7|?>>
    <associate|footnr-8|<tuple|8|?>>
    <associate|footnr-9|<tuple|9|?>>
    <associate|intF(i)dr(i)=Del(KE)|<tuple|24|?>>
    <associate|q(lab)=sum int|<tuple|50|?>>
    <associate|q=sum int|<tuple|58|?>>
    <associate|r(i)=R+r'(i)|<tuple|66|?>>
    <associate|sum intF(i)dr(i)|<tuple|41|?>>
    <associate|sum m(i)v'(i)=0|<tuple|68|?>>
    <associate|sum=del(KE)|<tuple|42|?>>
    <associate|sum=del(PE)|<tuple|43|?>>
    <associate|sum_i sum_k phi(ik)|<tuple|36|?>>
    <associate|thermo-noninertial frame|<tuple|59|?>>
    <associate|v(i)=v+v'(i)|<tuple|67|?>>
    <associate|w(lab)=sum int|<tuple|48|?>>
    <associate|w-w(lab) (cm)|<tuple|76|?>>
    <associate|w-w(lab)=-mg del z(loc)|<tuple|64|?>>
    <associate|w-w(lab)=int|<tuple|63|?>>
    <associate|w=int F(sur)dx' dw=|<tuple|61|?>>
    <associate|w=sum int|<tuple|59|?>>
    <associate|w=sum int F cos(alpha)ds dw=|<tuple|60|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      greenbook-3

      stock-19

      greenbook-2

      greenbook-3

      greenbook-3

      greenbook-3

      devoe-07

      sears-70

      bridgman-41

      marion-95

      goldstein-50

      cox-89
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1>||>|<pageref|auto-16>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|2>||>|<pageref|auto-22>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|3>||>|<pageref|auto-25>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|4>||>|<pageref|auto-28>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|5>||>|<pageref|auto-31>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|6>||>|<pageref|auto-33>>
    </associate>
    <\associate|toc>
      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|1<space|2spc>Definitions
      of the SI Base Units> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|1fn>

      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|2<space|2spc>Physical
      Constants> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|1fn>

      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|3<space|2spc>Symbols
      for Physical Quantities> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|1fn>

      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|4<space|2spc>Miscellaneous
      Abbreviations and Symbols> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4><vspace|1fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Physical
      States> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Subscripts
      for Chemical Processes> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Superscripts>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7><vspace|0.5fn>

      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|5<space|2spc>Calculus
      Review> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8><vspace|1fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Derivatives>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Partial
      Derivatives> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Integrals>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Line
      Integrals> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12><vspace|0.5fn>

      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|6<space|2spc>Mathematical
      Properties of State Functions> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13><vspace|1fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Differentials>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Total
      Differential> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Integration
      of a Total Differential> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Legendre
      Transforms> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-18><vspace|0.5fn>

      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|7<space|2spc>Forces,
      Energy, and Work> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-19><vspace|1fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Forces
      between Particles> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-20><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>The
      System and Surroundings> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>System
      Energy Change> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-23><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|4<space|2spc>Macroscopic
      Work> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-24><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|5<space|2spc>The
      Work Done on the System and Surroundings>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-26><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|6<space|2spc>The
      Local Frame and Internal Energy> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-27><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|7<space|2spc>Nonrotating
      Local Frame> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-29><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|8<space|2spc>Center-of-mass
      Local Frame> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-30><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|9<space|2spc>Rotating
      Local Frame> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-32><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|10<space|2spc>Earth-Fixed
      Reference Frame> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-34><vspace|0.5fn>

      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|8<space|2spc>Standard
      Molar Thermodynamic Properties> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-35><vspace|1fn>
    </associate>
  </collection>
</auxiliary>