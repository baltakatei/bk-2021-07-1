<TeXmacs|1.99.20>

<style|<tuple|generic|std-latex>>

<\body>
  <\hide-preamble>
    <assign|R|<macro|\<bbb-R\>>>
  </hide-preamble>

  <label|Chap9><problem><label|prb:9-mole fractions> For a binary solution,
  find expressions for the mole fractions <math|x<B>> and <math|x<A>> as
  functions of the solute molality <math|m<B>>. <soln| Equate the expressions
  for <math|n<B>/n<A>> in Eqs. <reference|nB/nA=xB/(1-xB)> and
  <reference|nB/nA=M(A)mB> and solve for <math|x<B>>: <vs>
  <math|<D><frac|x<B>|1-x<B>>=M<A>m<B><space|2em>x<B>=<frac|M<A>m<B>|1+M<A>m<B>>>
  <vs>Find <math|x<A>>: <vs> <math|<D>x<A>=1-x<B>=<frac|1|1+M<A>m<B>>>>

  <problem>Consider a binary mixture of two liquids, A and B. The molar
  volume of mixing, <math|<Del>V<mix>/n>, is given by Eq. <reference|binary
  mixt Del(mix)V>.

  <\problemparts>
    \ <problempart>Find a formula for calculating the value of
    <math|<Del>V<mix>/n> of a binary mixture from values of <math|x<A>>,
    <math|x<B>>, <math|M<A>>, <math|M<B>>, <math|\<rho\>>,
    <math|\<rho\><A><rsup|\<ast\>>>, and <math|\<rho\><B><rsup|\<ast\>>>.

    <\soln>
      \ <math|<Del>V<mix>/n=V/n-x<A>V<A><rsup|\<ast\>>-x<B>V<B><rsup|\<ast\>>>
      <vs>Make the following substitutions:
      <vs><math|V=m/\<rho\>=<around|(|n<A>M<A>+n<B>M<B>|)>/\<rho\>>
      <space|2em><math|V<A><rsup|\<ast\>>=M<A>/\<rho\><A><rsup|\<ast\>>>
      <space|2em><math|V<B><rsup|\<ast\>>=M<B>/\<rho\><B><rsup|\<ast\>>>
      <vs>The result is

      <\equation*>
        <Del>V<mix>/n=<frac|n<A>M<A>+n<B>M<B>|n*\<rho\>>-<frac|x<A>M<A>|\<rho\><A><rsup|\<ast\>>>-<frac|x<B>M<B>|\<rho\><B><rsup|\<ast\>>>=<frac|x<A>M<A>+x<B>M<B>|\<rho\>>-<frac|x<A>M<A>|\<rho\><A><rsup|\<ast\>>>-<frac|x<B>M<B>|\<rho\><B><rsup|\<ast\>>>
      </equation*>
    </soln>

    \;

    <\big-table>
      <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|5|5|cell-col-span|1>|<cwith|1|1|5|5|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|12|12|1|-1|cell-bborder|1ln>|<table|<row|<cell|<ccol|<math|x<B>>>>|<cell|<ccol|<math|<around|[|<Del>V<mix>/n|]>/<tx|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<cell|<space|2em>>|<cell|<ccol|<math|x<B>>>>|<cell|<math|<around|[|<Del>V<mix>/n|]>/<tx|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>|<row|<cell|0>|<cell|0>|<cell|>|<cell|0.555>|<cell|0.005>>|<row|<cell|0.049>|<cell|-0.027>|<cell|>|<cell|0.597>|<cell|0.011>>|<row|<cell|0.097>|<cell|-0.050>|<cell|>|<cell|0.702>|<cell|0.029>>|<row|<cell|0.146>|<cell|-0.063>|<cell|>|<cell|0.716>|<cell|0.035>>|<row|<cell|0.199>|<cell|-0.077>|<cell|>|<cell|0.751>|<cell|0.048>>|<row|<cell|0.235>|<cell|-0.073>|<cell|>|<cell|0.803>|<cell|0.056>>|<row|<cell|0.284>|<cell|-0.074>|<cell|>|<cell|0.846>|<cell|0.058>>|<row|<cell|0.343>|<cell|-0.065>|<cell|>|<cell|0.897>|<cell|0.057>>|<row|<cell|0.388>|<cell|-0.053>|<cell|>|<cell|0.944>|<cell|0.049>>|<row|<cell|0.448>|<cell|-0.032>|<cell|>|<cell|1>|<cell|0>>|<row|<cell|0.491>|<cell|-0.016>|<cell|>|<cell|>|<cell|>>>>>
    </big-table|<label|tbl:9-octene-hexOH V^E>Molar volumes of mixing of
    binary mixtures of 1-hexanol (A) and 1-octene (B) at
    <math|25<units|<degC>>>.<space|.15em><footnote|Ref. <cite|Tresz-02>.>>

    <problempartAns>The molar volumes of mixing for liquid binary mixtures of
    1-hexanol (A) and 1-octene (B) at <math|25<units|<degC>>> have been
    calculated from their measured densities. The data are in Table
    <reference|tbl:9-octene-hexOH V^E>. The molar volumes of the pure
    constituents are <math|V<A><rsup|\<ast\>>=125.31<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    and <math|V<B><rsup|\<ast\>>=157.85<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>.
    Use the method of intercepts to estimate the partial molar volumes of
    both constituents in an equimolar mixture (<math|x<A>=x<B>=0.5>), and the
    partial molar volume <math|V<B><rsup|\<infty\>>> of B at infinite
    dilution.

    <answer|<math|V<A><around|(|x<B>=0.5|)>\<approx\>125.13<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>><next-line><math|V<B><around|(|x<B>=0.5|)>\<approx\>158.01<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>><next-line><math|V<B><rsup|\<infty\>>\<approx\>157.15<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>

    <\soln>
      \ The experimental points are shown as filled circles in Fig.
      <reference|fig:9-octene-hexOH V^E><vpageref|fig:9-octene-hexOH
      V<rsup|E>>.

      <\big-figure>
        <boxedfigure|<image|./09-SUP/HexOH-octene.eps||||> <capt|Molar volume
        of mixing as a function of <math|x<B>>.<label|fig:9-octene-hexOH
        V^E>>>
      </big-figure|>

      <vs>The dashed line is an estimated tangent to a curve through the
      experimental points at the point shown by the open circle at
      <math|x<B>=0.5,<Del>V<mix>/n=-0.013<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>.
      The slope of the line is <math|0.34<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>.
      Represent the line by <math|y=a*x<B>+b> and find
      <math|a=0.34<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>> and
      <math|b=-0.18<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>. The
      intercepts are <math|y=-0.18<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
      at <math|x<B>=0> and <math|y=0.16<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
      at <math|x<B|=>1>. The partial molar volumes are therefore
      <vs><math|V<A><around|(|x<B>=0.5|)>\<approx\><around|(|125.31-0.18|)><units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>=125.13<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
      <vs><math|V<B><around|(|x<B>=0.5|)>\<approx\><around|(|157.85+0.16|)><units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>=158.01<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
      <vs>The dotted line in Fig. <reference|fig:9-octene-hexOH V^E> is an
      estimated tangent to a curve through the experimental points at
      <math|x<B>=0>. The slope of this line is
      <math|-0.70<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>; this is
      also the value of the intercept at <math|x<B|=>1>. Therefore
      <vs><math|V<B><rsup|\<infty\>>\<approx\><around|(|157.85-0.70|)><units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>=157.15<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    </soln>
  </problemparts>

  <problem><label|prb:9-droplet> Extend the derivation of Prob.
  8.<reference|prb:8-droplet>, concerning a liquid droplet of radius <math|r>
  suspended in a gas, to the case in which the liquid and gas are both
  mixtures. Show that the equilibrium conditions are
  <math|T<sups|g>=T<sups|l>>, <math|\<mu\><rsub|i><sups|g>=\<mu\><rsub|i><sups|l>>
  (for each species <math|i> that can equilibrate between the two phases),
  and <math|p<sups|l>=p<sups|g>+2<g>/r>, where <math|<g>> is the surface
  tension. (As in Prob. 8.<reference|prb:8-droplet>, the last relation is the
  <I|Laplace equation\|p>Laplace equation.) <soln| The derivation is like
  that of Prob. 8.<reference|prb:8-droplet>, with the total differential of
  <math|U> given by <vs><math|<D><dif>U=T<sups|l><dif>S<sups|l>-p<sups|l><dif>V<sups|l>+<big|sum><rsub|i>\<mu\><rsub|i><sups|l><dif>n<rsub|i><sups|l>+T<sups|g><dif>S<sups|g>-p<sups|g><dif>V<sups|g>+<big|sum><rsub|i>\<mu\><rsub|i><sups|g><dif>n<rsub|i><sups|g>+<g><dif><As>>
  <vs>and the conditions for an isolated system given by
  <vs><math|<D><dif>U=0<space|2em><dif>V<sups|g>=-<dif>V<sups|l><space|2em><dif>n<rsub|i><sups|g>=-<dif>n<rsub|i><sups|l><tx|f*o*r*e*a*c*h*s*p*e*c*i*e*s<math|i>>>
  <vs>The result is the total differential
  <vs><math|<D><dif>S=<frac|<around|(|T<sups|l>-T<sups|g>|)>|T<sups|l>><dif>S<sups|g>+<frac|<around|(|p<sups|l>-p<sups|g>-2<g>/r|)>|T<sups|l>><dif>V<sups|l>-<frac|1|T<sups|l>>*<big|sum><rsub|i><around|(|\<mu\><rsub|i><sups|l>-\<mu\><rsub|i><sups|g>|)><dif>n<rsub|i><sups|l>>
  <vs>The equilibrium conditions are the ones that make the coefficient of
  each term zero.>

  <longpage>

  <problemAns>Consider a gaseous mixture of <math|4.0000<timesten|-2><mol>>
  of N<rsub|<math|2>> (A) and <math|4.0000<timesten|-2><mol>> of
  CO<rsub|<math|2>> (B) in a volume of <math|1.0000<timesten|-3><units|m<rsup|<math|3>>>>
  at a temperature of <math|298.15<K>>. The second virial coefficients at
  this temperature have the values<footnote|Refs. <cite|angus-76>,
  <cite|dymond-80>, and <cite|edwards-42>.>

  <\align*>
    <tformat|<table|<row|<cell|B<subs|A*A>>|<cell|=-4.8<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>>|<row|<cell|B<subs|B*B>>|<cell|=-124.5<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>>|<row|<cell|B<subs|A*B>>|<cell|=-47.5<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>>>>
  </align*>

  Compare the pressure of the real gas mixture with that predicted by the
  ideal gas equation. See Eqs. <reference|pV/n=RT[1+B/(V/n)+...)> and
  <reference|B=yA^2 B(AA)+...>.

  <\answer>
    real gas: <math|p=1.9743<br>><next-line>ideal gas: <math|p=1.9832<br>>
  </answer>

  <\soln>
    \ The composition is given by <math|y<rsub|A>=0.50000>,
    <math|y<rsub|B>=0.50000>. <vs>Real gas:
    <vs><math|<D>V/n=<frac|1.0000<timesten|-3><units|m<rsup|<math|3>>>|8.0000<timesten|-2><units|m*o*l>>=1.2500<timesten|-2><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    <vs><math|<D>B=y<A><rsup|2>B<subs|A*A>+2*y<A>y<B>B<subs|A*B>+y<B><rsup|2>B<subs|B*B>=-56.1<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    <vs><math|<D>p=R*T*<around*|(|<frac|1|V/n>+<frac|B|<around|(|V/n|)><rsup|2>>|)>=1.9743<timesten|5><Pa>=1.9743<br>>
    <vs>Ideal gas: <math|p=n*R*T/V=1.9832<Pa>=1.9832<br>> <vs>The real gas
    mixture has a slightly lower pressure than the ideal gas at the same
    temperature and volume, due to attractive forces between the molecules.
  </soln>

  <problem><label|prb:9-air> At <math|25<units|<degC>>> and <math|1<br>>, the
  Henry's law constants of nitrogen and oxygen dissolved in water are
  <math|k<subs|H,N<rsub|<math|2>>>=8.64<timesten|4><br>> and
  <math|k<subs|H,O<rsub|<math|2>>>=4.41<timesten|4><br>>.<footnote|Ref.
  <cite|wilhelm-77>.> The vapor pressure of water at this temperature and
  pressure is <math|p<subs|H<rsub|<math|2>>*O>=0.032<br>>. Assume that dry
  air contains only N<rsub|<math|2>> and O<rsub|<math|2>> at mole fractions
  <math|y<subs|N<rsub|<math|2>>>=0.788> and
  <math|y<subs|O<rsub|<math|2>>>=0.212>. Consider liquid\Ugas systems formed
  by equilibrating liquid water and air at <math|25<units|<degC>>> and
  <math|1.000<br>>, and assume that the gas phase behaves as an ideal gas
  mixture.

  Hint: The sum of the partial pressures of N<rsub|<math|2>> and
  O<rsub|<math|2>> must be <math|<around|(|1.000-0.032|)><br>=0.968<br>>. If
  the volume of one of the phases is much larger than that of the other, then
  almost all of the N<rsub|<math|2>> and O<rsub|<math|2>> will be in the
  predominant phase and the ratio of their amounts in this phase must be
  practically the same as in dry air.

  Determine the mole fractions of N<rsub|<math|2>> and O<rsub|<math|2>> in
  both phases in the following limiting cases:

  <\problemparts>
    \ <problempartAns>A large volume of air is equilibrated with just enough
    water to leave a small drop of liquid.

    <answer|<math|x<subs|N<rsub|<math|2>>>=8.83<timesten|-6>><next-line><math|x<subs|O<rsub|<math|2>>>=4.65<timesten|-6>><next-line><math|y<subs|N<rsub|<math|2>>>=0.763><next-line><math|y<subs|O<rsub|<math|2>>>=0.205>>

    <soln| Assume that the amounts of N<rsub|<math|2>> and O<rsub|<math|2>>
    in the gas phase are in the same ratio as in dry air: <vs>
    <math|<D><frac|n<subs|N<rsub|<math|2>>>|n<subs|O<rsub|<math|2>>>>=<frac|0.788|0.212>=3.72>
    <vs> <math|<D><frac|p<subs|N<rsub|<math|2>>>|p<subs|O<rsub|<math|2>>>>=<frac|y<subs|N<rsub|<math|2>>>p|y<subs|O<rsub|<math|2>>>p>=<frac|<around|(|n<subs|N<rsub|<math|2>>>/n|)>*p|<around|(|n<subs|O<rsub|<math|2>>>/n|)>*p>=3.72>
    <vs>Solve simultaneously with <math|p<subs|N<rsub|<math|2>>>+p<subs|O<rsub|<math|2>>>=0.968<br>>:
    <vs> <math|p<subs|N<rsub|<math|2>>>=0.763<br>>,
    <math|p<subs|O<rsub|<math|2>>>=0.205<br>> <vs>Calculate compositions in
    gas and liquid phases: <vs> <math|y<subs|N<rsub|<math|2>>>=p<subs|N<rsub|<math|2>>>/p=0.763>,
    <math|y<subs|O<rsub|<math|2>>>=p<subs|O<rsub|<math|2>>>/p=0.205> <vs>
    <math|x<subs|N<rsub|<math|2>>>=p<subs|N<rsub|<math|2>>>/k<subs|H,N<rsub|<math|2>>>=0.763<br>/8.64<timesten|4><br>=8.83<timesten|-6>>
    <vs> <math|x<subs|O<rsub|<math|2>>>=p<subs|O<rsub|<math|2>>>/k<subs|H,O<rsub|<math|2>>>=0.205<br>/4.41<timesten|4><br>=4.65<timesten|-6>>>
    <problempartAns>A large volume of water is equilibrated with just enough
    air to leave a small bubble of gas.

    <answer|<math|x<subs|N<rsub|<math|2>>>=9.85<timesten|-6>><next-line><math|x<subs|O<rsub|<math|2>>>=2.65<timesten|-6>><next-line><math|y<subs|N<rsub|<math|2>>>=0.851><next-line><math|y<subs|O<rsub|<math|2>>>=0.117>>

    <soln| Assume that the amounts of N<rsub|<math|2>> and O<rsub|<math|2>>
    in the liquid phase are in the same ratio as in dry air: <vs>
    <math|<D><frac|x<subs|N<rsub|<math|2>>>|x<subs|O<rsub|<math|2>>>>=<frac|n<subs|N<rsub|<math|2>>>|n<subs|O<rsub|<math|2>>>>=<frac|0.788|0.212>=3.72>
    <vs> <math|<D><frac|p<subs|N<rsub|<math|2>>>|p<subs|O<rsub|<math|2>>>>=<frac|k<subs|H,N<rsub|<math|2>>><space|0.17em>x<subs|N<rsub|<math|2>>>|k<subs|H,O<rsub|<math|2>>><space|0.17em>x<subs|O<rsub|<math|2>>>>=<around*|(|<frac|8.64<timesten|4><br>|4.41<timesten|4><br>>|)><around|(|3.72|)>=7.29>
    <vs>Solve simultaneously with <math|p<subs|N<rsub|<math|2>>>+p<subs|O<rsub|<math|2>>>=0.968<br>>:
    <vs> <math|p<subs|N<rsub|<math|2>>>=0.851<br>>,
    <math|p<subs|O<rsub|<math|2>>>=0.117<br>> <vs>Calculate compositions in
    gas and liquid phases: <vs> <math|y<subs|N<rsub|<math|2>>>=p<subs|N<rsub|<math|2>>>/p=0.851>,
    <math|y<subs|O<rsub|<math|2>>>=p<subs|O<rsub|<math|2>>>/p=0.117> <vs>
    <math|x<subs|N<rsub|<math|2>>>=p<subs|N<rsub|<math|2>>>/k<subs|H,N<rsub|<math|2>>>=0.851<br>/8.64<timesten|4><br>=9.85<timesten|-6>>
    <vs> <math|x<subs|O<rsub|<math|2>>>=p<subs|O<rsub|<math|2>>>/k<subs|H,O<rsub|<math|2>>>=0.117<br>/4.41<timesten|4><br>=2.65<timesten|-6>>>
  </problemparts>

  <problem>Derive the expression for <math|<g><mbB>> given in Table
  <reference|tbl:9-act coeff-fugacity>, starting with Eq. <reference|act
  coeff m,B>. <soln| Equate the expression for <math|\<mu\><mbB>> from Eq.
  <reference|act coeff m,B> and the expression for <math|\<mu\><B>> in the
  equilibrated gas phase: <vs><math|<D>\<mu\><mbB><rf>+R*T*ln
  <around*|(|<g><mbB><frac|m<B>|m<st>>|)>=\<mu\><B><rf><gas>+R*T*ln
  <space|0.17em><around|(|<fug><B>/p|)>> <vs>Solve for <math|<g><mbB>>:
  <vs><math|<D><g><mbB>=exp <around*|[|<frac|\<mu\><B><rf><gas>-\<mu\><mbB><rf>|R*T>|]>\<times\><frac|m<st><fug><B>|m<B>p>>
  <vs>On the right side of this expression, only <math|<fug><B>> and
  <math|m<B>> depend on the solution composition:
  <vs><math|<D><g><mbB>=C<mbB><frac|<fug><B>|m<B>>> <vs>where <math|C<mbB>>
  is independent of <math|m<B>>. <vs>Solve for <math|C<mbB>>:
  <vs><math|<D>C<mbB>=<frac|<g><mbB>m<B>|<fug><B>>> <vs>In the limit as
  <math|m<B>> approaches zero at constant <math|T> and <math|p>,
  <math|<g><mbB>> approaches <math|1> and <math|<fug><B>/m<B>> approaches
  <math|k<mbB>>. Evaluate <math|C<mbB>> in this limit:
  <vs><math|<D>C<mbB>=lim<rsub|m<B><ra>0><g><mbB><frac|m<B>|<fug><B>>=<frac|1|k<mbB>>>
  <vs>With this value for <math|C<mbB>>, the activity coefficient is given by
  <vs><math|<D><g><mbB>=<frac|<fug><B>|k<mbB>m<B>>>>

  <problem><label|prb:9-nonideal gas mixt> Consider a nonideal binary gas
  mixture with the simple <I|Equation of state!gas at low pressure@of a gas
  at low pressure\|p>equation of state <math|V=n*R*T/p+n*B> (Eq.
  <reference|V=nRT/p+nB>).

  <\problemparts>
    \ <problempart>The <I|Lewis and Randall rule\|p><em|rule of Lewis and
    Randall> states that the value of the mixed second virial coefficient
    <math|B<subs|A*B>> is the average of <math|B<subs|A*A>> and
    <math|B<subs|B*B>>. Show that when this rule holds, the fugacity
    coefficient of A in a binary gas mixture of any composition is given by
    <math|ln \<phi\><A>=B<subs|A*A>p/R*T>. By comparing this expression with
    Eq. <reference|ln(phi)=Bp/RT> for a pure gas, express the fugacity of A
    in the mixture as a function of the fugacity of pure A at the same
    temperature and pressure as the mixture. <soln| If <math|B<subs|A*B>> is
    equal to <math|<around|(|B<subs|A*A>+B<subs|B*B>|)>/2>, Eq.
    <reference|B(A)'=> gives <math|B<A><rprime|'>=B<subs|A*A>>, and Eq.
    <reference|ln(phi_i)=Bi'p/RT> becomes <vs><math|<D>ln
    \<phi\><A>=<frac|B<subs|A*A>p|R*T>> <vs>Comparing the expression with Eq.
    <reference|ln(phi)=Bp/RT> for a pure gas, <math|ln
    \<phi\>\<approx\>B*p/R*T>, we see that the fugacity coefficient of A in
    the mixture is equal to the fugacity coefficient of pure A at the same
    <math|T> and <math|p>: <vs><math|\<phi\><A>=<fug><A>/p<A>=<fug><A><rsup|\<ast\>>/p>
    <vs>Therefore, according to the rule of Lewis and Randall the fugacity of
    A is given by <vs><math|<fug><A>=<around|(|p<A>/p|)><fug><A><rsup|\<ast\>>=y<A><fug><A><rsup|\<ast\>>>>
    <problempartAns>The rule of Lewis and Randall is not accurately obeyed
    when constituents A and B are chemically dissimilar. For example, at
    <math|298.15<K>>, the second virial coefficients of H<rsub|<math|2>>O (A)
    and N<rsub|<math|2>> (B) are <math|B<subs|A*A>=-1158<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    and <math|B<subs|B*B>=-5<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>,
    respectively, whereas the mixed second virial coefficient is
    <math|B<subs|A*B>=-40<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>.
    <vs>When liquid water is equilibrated with nitrogen at <math|298.15<K>>
    and <math|1<br>>, the partial pressure of H<rsub|<math|2>>O in the gas
    phase is <math|p<A>=0.03185<br>>. Use the given values of
    <math|B<subs|A*A>>, <math|B<subs|B*B>>, and <math|B<subs|A*B>> to
    calculate the fugacity of the gaseous H<rsub|<math|2>>O in this binary
    mixture. Compare this fugacity with the fugacity calculated with the
    value of <math|B<subs|A*B>> predicted by the rule of Lewis and Randall.

    <answer|<math|<fug><A>=0.03167<br>>, <math|<fug><A>=0.03040<br>>>

    <soln| The gas-phase composition is <math|y<A>=p<A>/p=0.03185>,
    <math|y<B>=1-y<A>=0.96815>. Using the values of <math|B<subs|A*A>>,
    <math|B<subs|B*B>>, and <math|B<subs|A*B>> given in the problem, and Eq.
    <reference|B(A)'=>: <vs><math|<D>B<rprime|'><A>=-143<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    <vs>Then from Eq. <reference|ln(phi_i)=Bi'p/RT>: <vs> <math|<D>ln
    \<phi\><A>=<frac|B<rprime|'><A>p|R*T>=<frac|<around|(|-143<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>*<around|(|10<rsup|-2><space|0.17em><tx|m>/<tx|c*m>|)><rsup|3><around|(|1<timesten|5><Pa>|)>|<around|(|<R>|)><around|(|298.15<K>|)>>=-0.00576>
    <vs> <math|\<phi\><A>=0.99425>, <math|<fug><A>=\<phi\><A>p<A>=<around|(|0.99425|)><around|(|0.03185<br>|)>=0.03167<br>>
    <vs>Using <math|B<subs|A*B>=<around|(|B<subs|A*A>+B<subs|B*B>|)>/2=-581.5<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>:
    <vs><math|B<rprime|'><A>=B<subs|A*A>=-1158<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>,
    <math|<fug><A>=0.03040<br>>>
  </problemparts>

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|6|6|1|-1|cell-bborder|1ln>|<table|<row|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|<g><A>>>>|<cell|<space|2em>>|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|<g><A>>>>>|<row|<cell|0>|<cell|2.0<space|.15em><footnote|extrapolated>>|<cell|>|<cell|0.7631>|<cell|1.183>>|<row|<cell|0.1334>|<cell|1.915>|<cell|>|<cell|0.8474>|<cell|1.101>>|<row|<cell|0.2381>|<cell|1.809>|<cell|>|<cell|0.9174>|<cell|1.046>>|<row|<cell|0.4131>|<cell|1.594>|<cell|>|<cell|0.9782>|<cell|1.005>>|<row|<cell|0.5805>|<cell|1.370>|<cell|>|<cell|>|<cell|<ccol|>>>>>>
  </big-table|Activity coefficient of benzene (A) in mixtures of benzene and
  1-octanol at <math|20<units|<degC>>>. The reference state is the pure
  liquid.>

  <problem><label|prb:9-benz-oct> Benzene and 1-octanol are two liquids that
  mix in all proportions. Benzene has a measurable vapor pressure, whereas
  1-octanol is practically nonvolatile. The data in Table
  <reference|tbl:9-benz-oct><vpageref|tbl:9-benz-oct> were obtained by
  Platford<footnote|Ref. <cite|platford-76>.> using the isopiestic vapor
  pressure method.

  <\problemparts>
    \ <problempartAns>Use numerical integration to evaluate the integral on
    the right side of Eq. <reference|ln(ac_B)=int...> at each of the values
    of <math|x<A>> listed in the table, and thus find <math|<g><B>> at these
    compositions.

    <answer|In the mixture of composition <math|x<A>=0.9782>, the activity
    coefficient is <math|<g><B>\<approx\>11.5>.>

    <\soln>
      <\big-table>
        <minipagetable|10cm|<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|11|11|1|-1|cell-bborder|1ln>|<table|<row|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|ln
        <g><B>=-<big|int><rsub|x<A>=0><rsup|x<A>><frac|x<A>|x<B>><dif>ln
        <g><A>>>>|<cell|<ccol|<math|<g><B>>>>|<cell|<ccol|<math|<g><A>x<A>>>>|<cell|<ccol|<math|<g><B>x<B>>>>>|<row|<cell|0>|<cell|0>|<cell|1>|<cell|0>|<cell|1>>|<row|<cell|0.1334>|<cell|0.0033>|<cell|1.003>|<cell|0.255>|<cell|0.869>>|<row|<cell|0.2381>|<cell|0.0166>|<cell|1.017>|<cell|0.431>|<cell|0.775>>|<row|<cell|0.4131>|<cell|0.0809>|<cell|1.084>|<cell|0.658>|<cell|0.636>>|<row|<cell|0.5805>|<cell|0.2389>|<cell|1.270>|<cell|0.796>|<cell|0.533>>|<row|<cell|0.7631>|<cell|0.5767>|<cell|1.780>|<cell|0.903>|<cell|0.422>>|<row|<cell|0.8474>|<cell|0.8921>|<cell|2.440>|<cell|0.933>|<cell|0.372>>|<row|<cell|0.9174>|<cell|1.3188>|<cell|3.739>|<cell|0.960>|<cell|0.309>>|<row|<cell|0.9782>|<cell|2.438>|<cell|11.45>|<cell|0.983>|<cell|0.250>>|<row|<cell|1>|<cell|>|<cell|>|<cell|1>|<cell|0>>>>>>
      </big-table|<label|tbl:9prob-benz-oct>Calculations for Problem
      9.<reference|prb:9-benz-oct>>

      The values in column 2 of Table <reference|tbl:9prob-benz-oct><vpageref|tbl:9prob-benz-oct>
      were calculated by the trapezoidal rule. The resulting values of
      <math|<g><B>> are listed in column 3 of this table.
    </soln>

    \ <problempart>Draw two curves on the same graph showing the effective
    mole fractions <math|<g><A>x<A>> and <math|<g><B>x<B>> as functions of
    <math|x<A>>. Are the deviations from ideal-mixture behavior positive or
    negative?

    <\soln>
      \ The values listed in the last two columns of Table
      <reference|tbl:9prob-benz-oct> are plotted in Fig.
      <reference|fig:9-benz-oct><vpageref|fig:9-benz-oct>. The deviations
      from ideal-mixture behavior are <em|positive>.

      <\big-figure>
        <boxedfigure|<image|./09-SUP/benz-oct.eps||||> <capt|Results for
        Problem 9.<reference|prb:9-benz-oct>(b).<label|fig:9-benz-oct>>>
      </big-figure|>
    </soln>
  </problemparts>

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|7|7|cell-col-span|1>|<cwith|1|1|7|7|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|9|9|1|-1|cell-bborder|1ln>|<table|<row|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|y<A>>>>|<cell|<ccol|<math|p/<tx|k*P*a>>>>|<cell|>|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|y<A>>>>|<cell|<math|p/<tx|k*P*a>>>>|<row|<cell|0>|<cell|0>|<cell|29.894>|<cell|<space|2em>>|<cell|0.4201>|<cell|0.5590>|<cell|60.015>>|<row|<cell|0.0207>|<cell|0.2794>|<cell|40.962>|<cell|>|<cell|0.5420>|<cell|0.5783>|<cell|60.416>>|<row|<cell|0.0314>|<cell|0.3391>|<cell|44.231>|<cell|>|<cell|0.6164>|<cell|0.5908>|<cell|60.416>>|<row|<cell|0.0431>|<cell|0.3794>|<cell|46.832>|<cell|>|<cell|0.7259>|<cell|0.6216>|<cell|59.868>>|<row|<cell|0.0613>|<cell|0.4306>|<cell|50.488>|<cell|>|<cell|0.8171>|<cell|0.6681>|<cell|58.321>>|<row|<cell|0.0854>|<cell|0.4642>|<cell|53.224>|<cell|>|<cell|0.9033>|<cell|0.7525>|<cell|54.692>>|<row|<cell|0.1811>|<cell|0.5171>|<cell|57.454>|<cell|>|<cell|0.9497>|<cell|0.8368>|<cell|51.009>>|<row|<cell|0.3217>|<cell|0.5450>|<cell|59.402>|<cell|>|<cell|1>|<cell|1>|<cell|44.608>>>>>
  </big-table|<label|tbl:9-benz-MeOH>Liquid and gas compositions in the
  two-phase system of methanol (A) and benzene (B) at
  <math|45<units|<degC>>><space|.15em><footnote|Ref. <cite|Toghiani-94>.>>

  <problem><label|prb:9-MeOH-benz> Table <reference|tbl:9-benz-MeOH><vpageref|tbl:9-benz-MeOH>
  lists measured values of gas-phase composition and total pressure for the
  binary two-phase methanol\Ubenzene system at constant temperature and
  varied liquid-phase composition. <math|x<A>> is the mole fraction of
  methanol in the liquid mixture, and <math|y<A>> is the mole fraction of
  methanol in the equilibrated gas phase.

  <\problemparts>
    \ <problempart>For each of the 16 different liquid-phase compositions,
    tabulate the partial pressures of A and B in the equilibrated gas phase.

    <\soln>
      <\big-table>
        <minipagetable|12.2cm|<label|tbl:9prob-MeOH-benz><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|9|9|1|-1|cell-bborder|1ln>|<table|<row|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|p<A>/<tx|k*P*a>>>>|<cell|<ccol|<math|p<B>/<tx|k*P*a>>>>|<cell|<ccol|<math|<g><B>>>>|<cell|>|<cell|<ccol|<math|x<A>>>>|<cell|<ccol|<math|p<A>/<tx|k*P*a>>>>|<cell|<ccol|<math|p<B>/<tx|k*P*a>>>>|<cell|<ccol|<math|<g><B>>>>>|<row|<cell|0>|<cell|0>|<cell|29.894>|<cell|1>|<cell|<space|2em><space|1em>>|<cell|0.4201>|<cell|33.55>|<cell|26.47>|<cell|1.527>>|<row|<cell|0.0207>|<cell|11.44>|<cell|29.52>|<cell|1.008>|<cell|>|<cell|0.5420>|<cell|34.94>|<cell|25.48>|<cell|1.861>>|<row|<cell|0.0314>|<cell|15.00>|<cell|29.23>|<cell|1.009>|<cell|>|<cell|0.6164>|<cell|35.69>|<cell|24.73>|<cell|2.157>>|<row|<cell|0.0431>|<cell|17.77>|<cell|29.06>|<cell|1.016>|<cell|>|<cell|0.7259>|<cell|37.21>|<cell|22.66>|<cell|2.765>>|<row|<cell|0.0613>|<cell|21.74>|<cell|28.75>|<cell|1.025>|<cell|>|<cell|0.8171>|<cell|38.96>|<cell|19.36>|<cell|3.541>>|<row|<cell|0.0854>|<cell|24.71>|<cell|28.51>|<cell|1.043>|<cell|>|<cell|0.9033>|<cell|41.15>|<cell|13.54>|<cell|4.68>>|<row|<cell|0.1811>|<cell|29.71>|<cell|27.74>|<cell|1.133>|<cell|>|<cell|0.9497>|<cell|42.68>|<cell|8.33>|<cell|5.54>>|<row|<cell|0.3217>|<cell|32.37>|<cell|27.03>|<cell|1.333>|<cell|>|<cell|1>|<cell|44.608>|<cell|0>|<cell|>>>>>>
      </big-table|Calculations for Problem 9.<reference|prb:9-MeOH-benz>>

      Calculate the partial pressures with the relations
      <vs><math|p<A>=y<A>p> <vs><math|p<B>=p-p<A>> <vs>The values are
      tabulated in columns 2 and 3 of Table
      <reference|tbl:9prob-MeOH-benz><vpageref|tbl:9prob-MeOH-benz>.
    </soln>

    \ <problempart>Plot <math|p<A>> and <math|p<B>> versus <math|x<A>> on the
    same graph. Notice that the behavior of the mixture is far from that of
    an ideal mixture. Are the deviations from Raoult's law positive or
    negative?

    <\soln>
      \ See Fig. <reference|fig:9-MeOH-benz><vpageref|fig:9-MeOH-benz>. The
      deviations from Raoult's law are positive.

      <\big-figure>
        <boxedfigure|<image|./09-SUP/MeOH-ben.eps||||> <capt|Results for
        Problem 9.<reference|prb:9-MeOH-benz>.<label|fig:9-MeOH-benz>>>
      </big-figure|>
    </soln>

    \ <problempart>Tabulate and plot the activity coefficient <math|<g><B>>
    of the benzene as a function of <math|x<A>> using a pure-liquid reference
    state. Assume that the fugacity <math|<fug><B>> is equal to <math|p<B>>,
    and ignore the effects of variable pressure.

    <\soln>
      \ Calculate the activity coefficient with the relation
      <vs><math|<D><g><B>=<frac|p<B>|x<B>p<B><rsup|\<ast\>>>> <vs>where the
      vapor pressure of pure benzene is <math|p<B><rsup|\<ast\>>=29.894<units|k*P*a>>.
      The calculated values are in the last column of Table
      <reference|tbl:9prob-MeOH-benz> and are plotted in Fig.
      <reference|fig:9-benzene ac><vpageref|fig:9-benzene ac>. Note how
      <math|<g><B>> approaches <math|1> as <math|x<A>> approaches zero.

      <\big-figure>
        <boxedfigure|<image|./09-SUP/benz-ac.eps||||> <capt|Activity
        coefficient of benzene in liquid methanol--benzene
        mixtures.<label|fig:9-benzene ac>>>
      </big-figure|>
    </soln>

    \ <problempartAns>Estimate the Henry's law constant <math|k<subs|H,A>> of
    methanol in the benzene environment at <math|45<units|<degC>>> by the
    graphical method suggested in Fig. <reference|fig:9-fugacity vs xB>(b).
    Again assume that <math|<fug><A>> and <math|p<A>> are equal, and ignore
    the effects of variable pressure.

    <answer|<math|k<subs|H,A>\<approx\>680<units|k*P*a>>>

    <\soln>
      \ See Fig. <reference|fig:9-K(MeOH)><vpageref|fig:9-K(MeOH)>. Based on
      the first three points, the extrapolation of <math|p<A>/x<A>> to
      <math|x<A>=0> gives a value of <math|k<subs|H,A>\<approx\>680<units|k*P*a>>.

      <\big-figure>
        <boxedfigure|<image|./09-SUP/K-MeOH.eps||||> <capt|Estimate of the
        Henry's law constant of methanol in benzene.<label|fig:9-K(MeOH)>>>
      </big-figure|>
    </soln>
  </problemparts>

  <problem><label|prb:9-dil osmotic coeff> Consider a dilute binary
  nonelectrolyte solution in which the dependence of the chemical potential
  of solute B on composition is given by

  <\equation*>
    \<mu\><B>=\<mu\><mbB><rf>+R*T*ln <frac|m<B>|m<st>>+k<rsub|m>*m<B>
  </equation*>

  where <math|\<mu\><mbB><rf>> and <math|k<rsub|m>> are constants at a given
  <math|T> and <math|p>. (The derivation of this equation is sketched in Sec.
  <reference|9-nonideal dil solns>.) Use the Gibbs\UDuhem equation in the
  form <math|<dif>\<mu\><A>=-<around|(|n<B>/n<A>|)><dif>\<mu\><B>> to obtain
  an expression for <math|\<mu\><A>-\<mu\><A><rsup|\<ast\>>> as a function of
  <math|m<B>> in this solution.

  <\soln>
    \ Make the substitutions <math|n<B>/n<A>=M<A>m<B>> (Eq.
    <reference|nB/nA=M(A)mB>) and <math|<dif>\<mu\><B>=<around|(|R*T/m<B>+k<rsub|m>|)><dif>m<B>>,
    and integrate from pure solvent to solution of molality <math|m<B>>: <vs>
    <math|<D><big|int><rsub|\<mu\><A><rsup|\<ast\>>><rsup|\<mu\><rprime|'><A>><dif>\<mu\><A>=-M<A><big|int><rsub|0><rsup|m<rprime|'><B>>m<B><around*|(|<frac|R*T|m<B>>+k<rsub|m>|)><dif>m<B>>
    <vs> <math|\<mu\><rprime|'><A>-\<mu\><A><rsup|\<ast\>>=-M<A>R*T*m<rprime|'><B>-<frac|1|2>*M<A>k<rsub|m><around|(|m<rprime|'><B>|)><rsup|2>>
    <vs> <math|\<mu\><A>-\<mu\><A><rsup|\<ast\>>=-M<A>R*T*m<B>-<frac|1|2>*M<A>k<rsub|m>*m<B><rsup|2>>
  </soln>

  <problemAns>By means of the isopiestic vapor pressure technique, the
  osmotic coefficients of aqueous solutions of urea at
  <math|25<units|<degC>>> have been measured at molalities up to the
  saturation limit of about <math|20<units|m*o*l*<space|0.17em>k*g<per>>>.<footnote|Ref.
  <cite|scatchard-38a>.> The experimental values are closely approximated by
  the function

  <\equation*>
    \<phi\><rsub|m>=1.00-<frac|0.050*<space|0.17em>m<B>/m<st>|1.00+0.179*<space|0.17em>m<B>/m<st>>
  </equation*>

  where <math|m<st>> is <math|1<units|m*o*l*<space|0.17em>k*g<per>>>.
  Calculate values of the solvent and solute activity coefficients
  <math|<g><A>> and <math|<g><mbB>> at various molalities in the range
  0\U<math|20<units|m*o*l*<space|0.17em>k*g<per>>>, and plot them versus
  <math|m<B>/m<st>>. Use enough points to be able to see the shapes of the
  curves. What are the limiting slopes of these curves as <math|m<B>>
  approaches zero?

  <\answer>
    Values for <math|m<B>/m<st>=20>: <math|<g><A>=1.026>,
    <math|<g><mbB>=0.526>; the limiting slopes are
    <math|<dif><g><A>/<dif><around|(|m<B>/m<st>|)>=0>,
    <math|<dif><g><mbB>/<dif><around|(|m<B>/m<st>|)>=-0.09>
  </answer>

  <\soln>
    \ Substitute the expression for <math|\<mu\><A>> given by Eq.
    <reference|act coeff, solvent> into Eq.
    <reference|phi(m)=(muA*-muA)/RTM(A)mB> and solve for <math|ln <g><A>>:
    <vs> <math|<D>ln <g><A>=-ln x<A>-\<phi\><rsub|m>*M<A>m<B>> <vs>Values of
    <math|x<A>> can be found from <math|m<B>> with the formula derived in
    Prob. 9.<reference|prb:9-mole fractions>: <vs>
    <math|<D>x<A>=<frac|1|1+M<A>m<B>>> <vs><math|<g><mbB>> is found from Eq.
    <reference|ln gamma(mB)=>: <vs> <math|<D>ln
    <g><mbB><around|(|m<rprime|'><B>|)>=\<phi\><rsub|m>-1+<big|int><rsub|0><rsup|m<rprime|'><B>><frac|\<phi\><rsub|m>-1|m<B>><dif>m<B>>
    <vs> <math|<D><phantom|ln <g><mbB><around|(|m<rprime|'><B>|)>>=-<frac|0.050*m<rprime|'><B>/m<st>|1.00+0.179*m<rprime|'><B>/m<st>>-<big|int><rsub|0><rsup|m<rprime|'><B>/m<st>><frac|0.050|1.00+0.179*m<B>/m<st>><dif><around|(|m<B>/m<st>|)>>
    <vs> <math|<D><phantom|ln <g><mbB><around|(|m<rprime|'><B>|)>>=-<frac|0.050*m<rprime|'><B>/m<st>|1.00+0.179*m<rprime|'><B>/m<st>>-0.50*ln
    <around|(|1.00+0.179*m<rprime|'><B>/m<st>|)>> <vs>See Fig.
    <reference|fig:9prob-urea><vpageref|fig:9prob-urea> for the curves
    plotted from values calculated with these formulas. The limiting slopes
    are <math|<dif><g><A>/<dif><around|(|m<B>/m<st>|)>=0> and
    <math|<dif><g><mbB>/<dif><around|(|m<B>/m<st>|)>=-0.09>.

    <\big-figure>
      <boxedfigure|<image|./09-SUP/urea.eps||||> <capt|Activity coefficients
      in aqueous urea solutions at <math|25<units|<degC>>>.<label|fig:9prob-urea>>>
    </big-figure|>
  </soln>

  <problem>Use Eq. <reference|d(mu_i)/dp=V_i> to derive an expression for the
  rate at which the logarithm of the activity coefficient of component
  <math|i> of a liquid mixture changes with pressure at constant temperature
  and composition: <math|<pd|ln <g><rsub|i>|p|T,<allni>>=>? <soln|
  <math|\<mu\><rsub|i>=\<mu\><rsub|i><rf>+R*T*ln
  <around|(|<g><rsub|i>x<rsub|i>|)>*<space|2em>R*T*ln
  <g><rsub|i>=\<mu\><rsub|i>-\<mu\><rsub|i><rf>-R*T*ln x<rsub|i>>
  <vs><math|<D><Pd|R*T*ln <g><rsub|i>|p|T,<allni>>=<Pd|\<mu\><rsub|i>|p|T,<allni>>-<Pd|\<mu\><rsub|i><rf>|p|T,<allni>>=V<rsub|i>-V<rsub|i><rsup|\<ast\>>>
  <vs><math|<D><Pd|ln <g><rsub|i>|p|T,<allni>>=<frac|V<rsub|i>-V<rsub|i><rsup|\<ast\>>|R*T>>>

  <problemAns>Assume that at sea level the atmosphere has a pressure of
  <math|1.00<br>> and a composition given by
  <math|y<subs|N<rsub|<math|2>>>=0.788> and
  <math|y<subs|O<rsub|<math|2>>>=0.212>. Find the partial pressures and mole
  fractions of N<rsub|<math|2>> and O<rsub|<math|2>>, and the total pressure,
  at an altitude of <math|10.0<units|k*m>>, making the (drastic)
  approximation that the atmosphere is an ideal gas mixture in an equilibrium
  state at <math|0<units|<degC>>>. For <math|g> use the value of the standard
  acceleration of free fall listed in Appendix <reference|app:const>.

  <\answer>
    <math|p<subs|N<rsub|<math|2>>>=0.235<br>><next-line><math|y<subs|N<rsub|<math|2>>>=0.815><next-line><math|p<subs|O<rsub|<math|2>>>=0.0532<br>><next-line><math|y<subs|O<rsub|<math|2>>>=0.185><next-line><math|p=0.288<br>>
  </answer>

  <\soln>
    \ Partial pressures at sea level: <vs><math|p<subs|N<rsub|<math|2>>>=y<subs|N<rsub|<math|2>>>p=<around|(|0.788|)><around|(|1.00<br>|)>=0.788<br>>
    <vs><math|p<subs|O<rsub|<math|2>>>=y<subs|O<rsub|<math|2>>>p=<around|(|0.212|)><around|(|1.00<br>|)>=0.212<br>>
    <vs>Calculate the partial pressures at elevation <math|h> with the
    equation <math|p<rsub|i><around|(|h|)>=p<rsub|i><around|(|0|)>*e<rsup|-M<rsub|i>*g*h/R*T>>
    (Sec. <reference|9-gas mixt in grav field>): <vs>
    <math|<D>p<subs|N<rsub|<math|2>>>=<around|(|0.788<br>|)>*exp
    <around*|[|<frac|-<around|(|28.01<timesten|-3><units|k*g*<space|0.17em>m*o*l<per>>|)>*<around|(|9.806*<space|0.17em>65<units|m*<space|0.17em>s<rsup|<math|-2>>>|)><around|(|10.0<timesten|3><units|m>|)>|<around|(|<R>|)><around|(|273.15<K>|)>>|]>>
    <vs> <math|<phantom|p<subs|N<rsub|<math|2>>>>=0.235<br>> <vs>
    <math|<D>p<subs|O<rsub|<math|2>>>=<around|(|0.212<br>|)>*exp
    <around*|[|<frac|-<around|(|32.00<timesten|-3><units|k*g*<space|0.17em>m*o*l<per>>|)>*<around|(|9.806*<space|0.17em>65<units|m*<space|0.17em>s<rsup|<math|-2>>>|)><around|(|10.0<timesten|3><units|m>|)>|<around|(|<R>|)><around|(|273.15<K>|)>>|]>>
    <vs> <math|<phantom|p<subs|O<rsub|<math|2>>>>=0.0532<br>> <vs>The total
    pressure and mole fractions are found from <vs>
    <math|p=p<subs|N<rsub|<math|2>>>+p<subs|O<rsub|<math|2>>>=0.288<br>> <vs>
    <math|y<subs|N<rsub|<math|2>>>=p<subs|N<rsub|<math|2>>>/p=0.815>
    <space|2em> <math|y<subs|O<rsub|<math|2>>>=p<subs|O<rsub|<math|2>>>/p=0.185>
    <vs>The composition of the mixture has shifted toward a higher mole
    fraction of N<rsub|<math|2>> at the higher elevation, because of the
    lower molar mass of N<rsub|<math|2>>.
  </soln>

  <problem>Consider a tall column of a dilute binary liquid solution at
  equilibrium in a gravitational field.

  <\problemparts>
    \ <problempart>Derive an expression for <math|ln
    <space|0.17em><around|[|<space|0.17em>c<B><around|(|h|)>/c<B><around|(|0|)><space|0.17em>|]>>,
    where <math|c<B><around|(|h|)>> and <math|c<B><around|(|0|)>> are the
    solute concentrations at elevations <math|h> and <math|0>. Your
    expression should be a function of <math|h>, <math|M<B>>, <math|T>,
    <math|\<rho\>>, and the partial specific volume of the solute at infinite
    dilution, <math|v<B><rsup|\<infty\>>>. For the dependence of pressure on
    elevation, you may use the hydrostatic formula
    <math|<difp><space|-0.17em>=<space|-0.17em>-\<rho\>*g<dif>h> (Eq.
    <reference|dp=-rho g dh><vpageref|dp=-rho g dh>) and assume the solution
    density <math|\<rho\>> is the same at all elevations. Hint: use the
    derivation leading to Eq. <reference|ln(c''/c') (centr)> as a guide.
    <soln| Only an outline of the derivation is given here. Students should
    be expected to provide a more complete explanation of the various steps.
    <vs>Using <math|<difp>=-\<rho\>*g<dif>h>:
    <vs><math|<D>p<around|(|h|)>-p<around|(|0|)>=-\<rho\>*g*<big|int><rsub|0><rsup|h><space|-0.17em><dif>h=-\<rho\>*g*h>
    <vs>Reversible elevation of small sample of mass <math|m>:
    <vs><math|<dw><rprime|'>=m*g<dif>h=<around|(|n<A>M<A>+n<B>M<B>|)>*g<dif>h>
    <vs><math|<dif>G=-S<dif>T+V<difp>+\<mu\><A><dif>n<A>+\<mu\><B><dif>n<B>+<around|(|n<A>M<A>+n<B>M<B>|)>*g<dif>h>
    <vs><math|<D><Pd|\<mu\><B>|h|T,p,n<A>,n<B>>=<bPd|<around|(|n<A>M<A>+n<B>M<B>|)>*g|n<B>|T,p,n<A>,h>=M<B>g>
    <vs><math|\<mu\><B><around|(|h|)>=\<mu\><B><around|(|0|)>+M<B>g*h>
    (<math|a<cbB><around|(|h|)>=a<cbB><around|(|0|)>><space|0.17em>)
    <vs>General relation: <vs><math|\<mu\><B><around|(|h|)>=\<mu\><cbB><st>+R*T*ln
    a<cbB><around|(|h|)>+M<B>g*h> <vs>At equilibrium:
    <vs><math|\<mu\><B><around|(|h|)>=\<mu\><B><around|(|0|)>>
    <vs><math|\<mu\><cbB><st>+R*T*ln a<cbB><around|(|h|)>+M<B>g*h=\<mu\><cbB><st>+R*T*ln
    a<cbB><around|(|0|)>> <vs><math|<D>ln
    <frac|a<cbB><around|(|h|)>|a<cbB><around|(|0|)>>=-<frac|M<B>g*h|R*T>>
    <vs><math|<D>a<cbB>=<G><cbB><g><cbB>c<B>/c<st>\<approx\>exp
    <around*|[|<frac|V<B><rsup|\<infty\>><around|(|p-p<st>|)>|R*T>|]><frac|c<B>|c<st>>>
    <vs><math|<D>ln <frac|a<cbB><around|(|h|)>|a<cbB><around|(|0|)>>=<frac|V<B><rsup|\<infty\>><around|[|p<around|(|h|)>-p<around|(|0|)>|]>|R*T>+ln
    <frac|c<B><around|(|h|)>|c<B><around|(|0|)>>> <vs><math|<D><phantom|ln
    <frac|a<cbB><around|(|h|)>|a<cbB><around|(|0|)>>>=-<frac|V<B><rsup|\<infty\>>\<rho\>*g*h|R*T>+ln
    <frac|c<B><around|(|h|)>|c<B><around|(|0|)>>=-<frac|M<B>v<B><rsup|\<infty\>>\<rho\>*g*h|R*T>+ln
    <frac|c<B><around|(|h|)>|c<B><around|(|0|)>>> <vs>Equate the two
    expressions for <math|ln <around|[|a<cbB><around|(|h|)>/a<cbB><around|(|0|)>|]>>:
    <vs><math|<D>ln <frac|c<B><around|(|h|)>|c<B><around|(|0|)>>=-<frac|M<B>g*h*<around*|(|1-v<B><rsup|\<infty\>>\<rho\>|)>|R*T>>>
    <problempartAns>Suppose you have a tall vessel containing a dilute
    solution of a macromolecule solute of molar mass
    <math|M<B>=10.0<units|k*g*<space|0.17em>m*o*l<per>>> and partial specific
    volume <math|v<B><rsup|\<infty\>>=0.78<units|c*m<rsup|<math|3>>*<space|0.17em>g<per>>>.
    The solution density is <math|\<rho\>=1.00<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>
    and the temperature is <math|T=300<K>>. Find the height <math|h> from the
    bottom of the vessel at which, in the equilibrium state, the
    concentration <math|c<B>> has decreased to <math|99> percent of the
    concentration at the bottom.

    <answer|<math|h=1.2<units|m>>>

    <soln| <math|<D>ln <frac|c<B><around|(|h|)>|c<B><around|(|0|)>>=ln
    0.99=-<frac|M<B>g*h*<around*|(|1-v<B><rsup|\<infty\>>\<rho\>|)>|R*T>>
    <vs><math|v<B><rsup|\<infty\>>\<rho\>=<around|(|0.78<units|c*m<rsup|<math|3>>*<space|0.17em>g<per>>|)><around|(|1.00<units|g*<space|0.17em>c*m<rsup|<math|-3>>>|)>=0.78>
    <vs> <math|<D>h=-<frac|R*T*ln 0.99|M<B>g*<around*|(|1-v<B><rsup|\<infty\>>\<rho\>|)>>=-<frac|<around|(|<R>|)><around|(|300<K>|)><around|(|ln
    0.99|)>|<around|(|10.0<units|k*g*<space|0.17em>m*o*l<per>>|)><around|(|9.81<units|m*<space|0.17em>s<rsup|<math|-2>>>|)><around|(|0.22|)>>=1.2<units|m>>>
  </problemparts>

  <\big-figure>
    <boxedfigure|<image|./09-SUP/EqmSed.eps||||> <capt|Sedimentation
    equilibrium of a dilute solution of the FhuA-dodecyl maltoside
    aggregate.<label|fig:9-sed. Eq.>>>
  </big-figure|>

  <problem><label|prb:9-centrifuge> FhuA is a protein found in the outer
  membrane of the <em|Escherichia coli> bacterium. From the known amino acid
  sequence, its molar mass is calculated to be
  <math|78.804<units|k*g*<space|0.17em>m*o*l<per>>>. In aqueous solution,
  molecules of the detergent dodecyl maltoside bind to a FhuA molecule to
  form an aggregate that behaves as a single solute species. Figure
  <reference|fig:9-sed. Eq.><vpageref|fig:9-sed. Eq.> shows data collected in
  a sedimentation equilibrium experiment with a dilute solution of the
  aggregate.<footnote|Ref. <cite|boulanger-96>.> In the graph, <math|A> is
  the absorbance measured at a wavelength of <math|280<units|n*m>> (a
  property that is a linear function of the aggregate concentration) and
  <math|r> is the radial distance from the axis of rotation of the centrifuge
  rotor. The experimental points fall very close to the straight line shown
  in the graph. The sedimentation conditions were
  <math|\<omega\>=838<units|s<per>>> and <math|T=293<K>>. The authors used
  the values <math|v<B><rsup|\<infty\>>=0.776<units|c*m<rsup|<math|3>>*<space|0.17em>g<per>>>
  and <math|\<rho\>=1.004<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>.

  <\problemparts>
    \ <problempartAns>The values of <math|r> at which the absorbance was
    measured range from <math|6.95<units|c*m>> to <math|7.20<units|c*m>>.
    Find the difference of pressure in the solution between these two
    positions.

    <answer|<math|p<around|(|7.20<units|c*m>|)>-p<around|(|6.95<units|c*m>|)>=1.2<br>>>

    <soln| <math|<D>p<rprime|''>-p<rprime|'>=<frac|\<omega\><rsup|2>*\<rho\>|2>*<around*|[|<around|(|r<rprime|''>|)><rsup|2>-<around|(|r<rprime|'>|)><rsup|2>|]>=<frac|<around|(|838<units|s<per>>|)><rsup|2><around|(|1.004<units|g*<space|0.17em>c*m<rsup|<math|-3>>>|)>*<around|(|1<units|k*g>/10<rsup|3><units|g>|)>|2>>
    <vs> <math|<phantom|p<rprime|''>-p<rprime|'>=>\<times\><around*|[|<around|(|7.20<units|c*m>|)><rsup|2>-<around|(|6.95<units|c*m>|)><rsup|2>|]>*<around|(|1<units|c*m>/10<rsup|-2><units|m>|)>>
    <vs> <math|<phantom|p<rprime|''>-p<rprime|'>>=1.2<timesten|5><units|k*g*<space|0.17em>m<per><space|0.17em>s<rsup|<math|-2>>>=1.2<br>>>
    <problempartAns>Find the molar mass of the aggregate solute species, and
    use it to estimate the mass binding ratio (the mass of bound detergent
    divided by the mass of protein).

    <answer|<math|M<B>=187<units|k*g*<space|0.17em>m*o*l<per>>><next-line>mass
    binding ratio <math|=1.37>>

    <soln| The slope of the line is <math|<dif>ln
    A/<dif>r<rsup|2>\<approx\>0.594<units|c*m<rsup|<math|-2>>>>. From Eq.
    <reference|ln(c''/c') (centr)>: <vs> <math|<D>M<B>=<frac|2*R*T<dif>ln
    A/<dif>r<rsup|2>|\<omega\><rsup|2>*<around|(|1-v<B><rsup|\<infty\>>\<rho\>|)>>>
    <vs> <math|<D><phantom|M<B>>=<frac|2<around|(|<R>|)><around|(|293<K>|)><around|(|0.594<units|c*m<rsup|<math|-2>>>|)>*<around|(|1<units|c*m>/10<rsup|-2><units|m>|)><rsup|2>|<around|(|838<units|s<per>>|)><rsup|2>*<around|[|1-<around|(|0.776<units|c*m<rsup|<math|3>>*<space|0.17em>g<per>>|)><around|(|1.004<units|g*<space|0.17em>c*m<rsup|<math|-3>>>|)>|]>>>
    <vs> <math|<phantom|M<B>>=187<units|k*g*<space|0.17em>m*o*l<per>>>
    <vs>Binding ratio: <math|<D><frac|<around|(|187-78.8|)><units|k*g*<space|0.17em>m*o*l<per>>|78.8<units|k*g*<space|0.17em>m*o*l<per>>>=1.37>>
  </problemparts>
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>