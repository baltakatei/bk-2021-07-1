<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|Pure Substances in Single Phases>

  <paragraphfootnotes><label|Chap. 7>

  This chapter applies concepts introduced in earlier chapters to the
  simplest kind of system, one consisting of a pure substance or a single
  component in a single phase. The system has three independent variables if
  it is open, and two if it is closed. Relations among various properties of
  a single phase are derived, including temperature, pressure, and volume.
  The important concepts of standard states and chemical potential are
  introduced.

  <section|Volume Properties>

  Two volume properties of a closed system are defined as follows:

  <alignat|2|<tformat|<table|<row|<cell|>|<cell|<index|Cubic expansion
  coefficient><newterm|cubic expansion coefficient>>|<cell|<space|1em>\<alpha\>>|<cell|<defn><frac|1|V><Pd|V|T|<space|-0.17em>p><eq-number><label|alpha
  def>>>|<row|<cell|>|<cell|<subindex|Isothermal|compressibility><newterm|isothermal
  compressibility>>|<cell|<space|1em><kT>>|<cell|<defn>-<frac|1|V><Pd|V|p|T><eq-number><label|kappaT
  def>>>>>>

  <\quote-env>
    The cubic expansion coefficient is also called the <index|Coefficient of
    thermal expansion>coefficient of thermal expansion and the
    <index|Expansivity coefficient>expansivity coefficient. Other symbols for
    the isothermal compressibility are <math|\<beta\>> and
    <math|<g><rsub|T>>.
  </quote-env>

  These definitions show that <math|\<alpha\>> is the fractional volume
  increase per unit temperature increase at constant pressure, and
  <math|<kT>> is the fractional volume decrease per unit pressure increase at
  constant temperature. Both quantities are <em|intensive> properties. Most
  substances have positive values of <math|\<alpha\>>,<footnote|The cubic
  expansion coefficient is not always positive. <math|\<alpha\>> is negative
  for liquid water below its temperature of maximum density,
  <math|3.98<units|<degC>>>. The crystalline ceramics zirconium tungstate
  (ZrW<rsub|<math|2>>O<rsub|<math|8>>) and hafnium tungstate
  (HfW<rsub|<math|2>>O<rsub|<math|8>>) have the remarkable behavior of
  contracting uniformly and continuously in all three dimensions when they
  are heated from <math|0.3<K>> to about <math|1050<K>>; <subindex|Cubic
  expansion coefficient|negative values of><math|\<alpha\>> is negative
  throughout this very wide temperature range (Ref. <cite|mary-96>). The
  intermetallic compound YbGaGe has been found to have a value of
  <math|\<alpha\>> that is practically zero in the range
  <math|100>--<math|300<K>> (Ref. <cite|Salvador-03>).> and all substances
  have positive values of <math|<kT>>, because a pressure increase at
  constant temperature requires a volume decrease.

  If an amount <math|n> of a substance is in a single phase, we can divide
  the numerator and denominator of the right sides of Eqs. <reference|alpha
  def> and <reference|kappaT def> by <math|n> to obtain the alternative
  expressions

  <\gather>
    <tformat|<table|<row|<cell|\<alpha\>=<frac|1|V<m>><Pd|V<m>|T|<space|-0.17em>p><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e,<math|P=1>|)>><eq-number><label|alpha(Vm)>>>>>
  </gather>

  <\gather>
    <tformat|<table|<row|<cell|<kT>=-<frac|1|V<m>><Pd|V<m>|p|T><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e,<math|P=1>|)>><eq-number><label|kappaT(Vm)>>>>>
  </gather>

  where <math|V<m>> is the molar volume. <math|P> in the conditions of
  validity is the number of phases. Note that only intensive properties
  appear in Eqs. <reference|alpha(Vm)> and <reference|kappaT(Vm)>; the amount
  of the substance is irrelevant. Figures <reference|fig:7-alpha vs T>

  <\big-figure>
    <boxedfigure|<image|./07-SUP/alpha.eps||||> <capt|The cubic expansion
    coefficient of several substances and an ideal gas as functions of
    temperature at <math|p=1<br>>.<space|.15em><footnote|Based on data in
    Ref. <cite|eisenberg-69>, p. 104; Ref. <cite|grigull-90>; and Ref.
    <cite|ICT-3>, p. 28.> Note that because liquid water has a density
    maximum at <math|4<units|<degC>>>, <math|\<alpha\>> is zero at that
    temperature.<label|fig:7-alpha vs T>>>
  </big-figure|>

  and <reference|fig:7-kappaT vs T>

  <\big-figure>
    <boxedfigure|<image|./07-SUP/kappaT.eps||||> <capt|The isothermal
    compressibility of several substances as a function of temperature at
    <math|p=1<br>>. (Based on data in Ref. <cite|eisenberg-69>; Ref.
    <cite|kell-75>; and Ref. <cite|ICT-3>, p. 28.)<label|fig:7-kappaT vs T>>>
  </big-figure|>

  show the temperature variation of <math|\<alpha\>> and <math|<kT>> for
  several substances.

  If we choose <math|T> and <math|p> as the independent variables of the
  closed system, the total differential of <math|V> is given by

  <\equation>
    <dif>V=<Pd|V|T|<space|-0.17em>p><dif>T+<Pd|V|p|T><difp>
  </equation>

  With the substitutions <math|<pd|V|T|p>=\<alpha\>*V> (from Eq.

  <reference|alpha def>) and <math|<pd|V|p|T>=-<kT>V> (from Eq.

  <reference|kappaT def>), the expression for the total differential of
  <math|V> becomes

  <\gather>
    <tformat|<table|<row|<cell|<dif>V=\<alpha\>*V<dif>T-<kT>V<difp><cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>)><eq-number><label|dV=alpha*VdT-kappaT*Vdp>>>>>
  </gather>

  To find how <math|p> varies with <math|T> in a closed system kept at
  constant volume, we set <math|<dif>V> equal to zero in Eq.
  <reference|dV=alpha*VdT-kappaT*Vdp>: <math|0=\<alpha\>*V<dif>T-\<kappa\><rsub|T>*V<difp>>,
  or <math|<difp>/<dif>T=\<alpha\>/\<kappa\><rsub|T>>. Since
  <math|<difp>/<dif>T> under the condition of constant volume is the partial
  derivative <math|<pd|p|T|V>>, we have the general relation

  <\gather>
    <tformat|<table|<row|<cell|<Pd|p|T|V>=<frac|\<alpha\>|<kT>><cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>)><eq-number><label|dp/dT=alpha/kappaT>>>>>
  </gather>

  <section|Internal Pressure><label|7-int pressure>

  <I|Internal!pressure\|(><I|Pressure!internal\|(>The partial derivative
  <math|<pd|U|V|T>> applied to a fluid phase in a closed system is called the
  <subindex|Internal|pressure><subindex|Pressure|internal><newterm|internal
  pressure>. (Note that <math|U> and <math|p*V> have dimensions of energy;
  therefore, <math|U/V> has dimensions of pressure.)

  To relate the internal pressure to other properties, we divide Eq.

  <reference|dU=TdS-pdV> by <math|<dif>V>:
  <math|<dif>U/<dif>V=T*<around|(|<dif>S/<dif>V|)>-p>. Then we impose a
  condition of constant <math|T>: <math|<pd|U|V|T>=T<pd|S|V|T>-p>. When we
  make a substitution for <math|<pd|S|V|T>> from the Maxwell relation of Eq.
  <reference|dS/dV=dp/dT>, we obtain

  <\gather>
    <tformat|<table|<row|<cell|<Pd|U|V|T>=T<Pd|p|T|V>-p<cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|f*l*u*i*d*p*h*a*s*e,<math|C=1>)><eq-number><label|dU/dV=Tdp/dT-p>>>>>
  </gather>

  This equation is sometimes called the <subindex|Thermodynamic|equation of
  state><subindex|Equation of state|thermodynamic>\Pthermodynamic equation of
  state\Q of the fluid.

  For an ideal-gas phase, we can write <math|p=n*R*T/V> and then

  <\equation>
    <Pd|p|T|V>=<frac|n*R|V>=<frac|p|T>
  </equation>

  Making this substitution in Eq. <reference|dU/dV=Tdp/dT-p> gives us

  <\gather>
    <tformat|<table|<row|<cell|<Pd|U|V|T>=0<cond|<around|(|c*l*o*s*e*d*s*y*s*t*e*m*o*f*a*n*i*d*e*a*l*g*a*s|)>><eq-number><label|dU/dV=0
    (id gas)>>>>>
  </gather>

  showing that the <I|Internal!pressure!ideal gas@of an ideal
  gas\|reg><subindex|Ideal gas|internal pressure>internal pressure of an
  ideal gas is zero.

  <\quote-env>
    \ In Sec. <reference|3-U of ideal gas>, an ideal gas was defined as a gas
    (1) that obeys the ideal gas equation, and (2) for which <math|U> in a
    closed system depends only on <math|T>. Equation <reference|dU/dV=0 (id
    gas)>, derived from the first part of this definition, expresses the
    second part. It thus appears that the second part of the definition is
    redundant, and that we could define an ideal gas simply as a gas obeying
    the ideal gas equation. This argument is valid only if we assume the
    <I|Temperature!ideal gas@ideal-gas\|reg>ideal-gas temperature is the same
    as the <subindex|Temperature|thermodynamic><subindex|Thermodynamic|temperature>thermodynamic
    temperature (Secs. <reference|2-temperature> and
    <reference|4-thermodynamic temperature>) since this assumption is
    required to derive Eq. <reference|dU/dV=0 (id gas)>. Without this
    assumption, we can't define an ideal gas solely by <math|p*V=n*R*T>,
    where <math|T> is the ideal gas temperature.
  </quote-env>

  Here is a simplified interpretation of the significance of the internal
  pressure. When the volume of a fluid increases, the average distance
  between molecules increases and the potential energy due to intermolecular
  forces changes. If attractive forces dominate, as they usually do unless
  the fluid is highly compressed, expansion causes the potential energy to
  <em|increase>. The internal energy is the sum of the potential energy and
  thermal energy. The internal pressure, <math|<pd|U|V|T>>, is the rate at
  which the internal energy changes with volume at constant temperature. At
  constant temperature, the thermal energy is constant so that the internal
  pressure is the rate at which just the potential energy changes with
  volume. Thus, the internal pressure is a measure of the strength of the
  intermolecular forces and is positive if attractive forces
  dominate.<footnote|These attractive intermolecular forces are the cohesive
  forces that can allow a <subindex|Pressure|negative>negative pressure to
  exist in a liquid; see page <pageref|neg p>.> In an ideal gas,
  intermolecular forces are absent and therefore the internal pressure of an
  ideal gas is zero.

  With the substitution <math|<pd|p|T|V>=\<alpha\>/<kT>> (Eq.
  <reference|dp/dT=alpha/kappaT>), Eq. <reference|dU/dV=Tdp/dT-p> becomes

  <\gather>
    <tformat|<table|<row|<cell|<Pd|U|V|T>=<frac|\<alpha\>*T|<kT>>-p<cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|f*l*u*i*d*p*h*a*s*e,<math|C=1>)><eq-number><label|dU/dV=alpha*T/kappaT-p>>>>>
  </gather>

  The internal pressure of a liquid at <math|p=1<br>> is typically much
  larger than <math|1<br>> (see Prob. 7.<reference|prb:7-aniline>). Equation
  <reference|dU/dV=alpha*T/kappaT-p> shows that, in this situation, the
  internal pressure is approximately equal to <math|\<alpha\>*T/<kT>>.
  <I|Internal!pressure\|)><I|Pressure!internal\|)>

  <section|Thermal Properties>

  <paragraphfootnotes>

  For convenience in derivations to follow, expressions from Chap.
  <reference|Chap. 5> are repeated here that apply to processes in a closed
  system in the absence of nonexpansion work (i.e., <math|<dw><rprime|'>=0>).
  For a process at <em|constant volume> we have<footnote|Eqs.
  <reference|dU=dq (dV=0)> and <reference|CV=dU/dT>.>

  <\equation>
    <label|dU=dq,C_V=dU/dT><dif>U=<dq><space|2em>C<rsub|V>=<Pd|U|T|V>
  </equation>

  and for a process at <em|constant pressure> we have<footnote|Eqs.
  <reference|dH=dq (dp=0)> and <reference|Cp=dH/dT>.>

  <\equation>
    <label|dH=dq,C_p=dH/dT><dif>H=<dq><space|2em>C<rsub|p>=<Pd|H|T|<space|-0.17em>p>
  </equation>

  A closed system of one component in a single phase has only two independent
  variables. In such a system, the partial derivatives above are complete and
  unambiguous definitions of <math|C<rsub|V>> and <math|C<rsub|p>> because
  they are expressed with two independent variables\V<math|T> and <math|V>
  for <math|C<rsub|V>>, and <math|T> and <math|p> for <math|C<rsub|p>>. As
  mentioned on page <pageref|ht cap conditions>, additional conditions would
  have to be specified to define <math|C<rsub|V>> for a more complicated
  system; the same is true for <math|C<rsub|p>>.

  For a closed system of an <em|ideal gas> we have<footnote|Eqs.
  <reference|CV=dU/dT (id gas)> and <reference|Cp=dH/dT (id gas)>.>

  <\equation>
    <label|C_V=,C_p=(id gas)>C<rsub|V>=<frac|<dif>U|<dif>T>*<space|2em>C<rsub|p>=<frac|<dif>H|<dif>T>
  </equation>

  <plainfootnotes>

  <subsection|The relation between <math|<mathbold|C<rsub|V<space|-0.17em><space|-0.17em>,<tx|m>>>>
  and <math|<mathbold|<Cpm>>>><label|7-relation between Cpm and CVm>

  <I|Heat capacity!constant volume and constant pressure@at constant volume
  and constant pressure, relation between\|reg>The value of <math|<Cpm>> for
  a substance is greater than <math|<CVm>>. The derivation is simple in the
  case of a fixed amount of an <em|ideal gas>. Using substitutions from Eq.
  <reference|C_V=,C_p=(id gas)>, we write

  <\equation>
    C<rsub|p>-C<rsub|V>=<frac|<dif>H|<dif>T>-<frac|<dif>U|<dif>T>=<frac|<dif><around|(|H-U|)>|<dif>T>=<frac|<dif><around|(|p*V|)>|<dif>T>=n*R
  </equation>

  Division by <math|n> to obtain molar quantities and rearrangement then
  gives

  <\gather>
    <tformat|<table|<row|<cell|<Cpm>=<CVm>+R<cond|<around|(|i*d*e*a*l*g*a*s,p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|Cpm=CVm+R>>>>>
  </gather>

  For any phase in general, we proceed as follows. First we write

  <\equation>
    C<rsub|p>=<Pd|H|T|<space|-0.17em>p>=<bPd|<around|(|U+p*V|)>|T|p>=<Pd|U|T|<space|-0.17em>p>+p<Pd|V|T|<space|-0.17em>p>
  </equation>

  Then we write the total differential of <math|U> with <math|T> and <math|V>
  as independent variables and identify one of the coefficients as
  <math|C<rsub|V>>:

  <\equation>
    <dif>U=<Pd|U|T|V><dif>T+<Pd|U|V|T><dif>V=C<rsub|V><dif>T+<Pd|U|V|T><dif>V
  </equation>

  When we divide both sides of the preceding equation by <math|<dif>T> and
  impose a condition of constant <math|p>, we obtain

  <\equation>
    <Pd|U|T|<space|-0.17em>p>=C<rsub|V>+<Pd|U|V|T><Pd|V|T|<space|-0.17em>p>
  </equation>

  Substitution of this expression for <math|<pd|U|T|p>> in the equation for
  <math|C<rsub|p>> yields

  <\equation>
    C<rsub|p>=C<rsub|V>+<around*|[|<Pd|U|V|T>+p|]><Pd|V|T|<space|-0.17em>p>
  </equation>

  Finally we set the partial derivative <math|<pd|U|V|T>> (the
  <subindex|Pressure|internal><subindex|Internal|pressure>internal pressure)
  equal to <math|<around|(|\<alpha\>*T/<kT>|)>-p> (Eq.
  <reference|dU/dV=alpha*T/kappaT-p>) and <math|<pd|V|T|p>> equal to
  <math|\<alpha\>*V> to obtain

  <\equation>
    C<rsub|p>=C<rsub|V>+<frac|\<alpha\><rsup|2>*T*V|<kT>>
  </equation>

  and divide by <math|n> to obtain molar quantities:

  <\equation>
    <label|Cpm=CVm+alpha^2...><Cpm>=<CVm>+<frac|\<alpha\><rsup|2>*T*V<m>|<kT>>
  </equation>

  Since the quantity <math|\<alpha\><rsup|2>*T*V<m>/<kT>> must be positive,
  <math|<Cpm>> is greater than <math|<CVm>>.

  <subsection|The measurement of heat capacities><label|7-ht cap measurement>

  The most accurate method of evaluating the <I|Heat capacity!measurement of,
  by calorimetry\|reg><I|Calorimetry!measure heat capacities@to measure heat
  capacities\|reg>heat capacity of a phase is by measuring the temperature
  change resulting from heating with <subindex|Electrical|work><subindex|Work|electrical><subindex|Electrical|heating><subindex|Heating|electrical>electrical
  work. The procedure in general is called calorimetry, and the apparatus
  containing the phase of interest and the electric heater is a
  <index|Calorimeter><newterm|calorimeter>. The principles of three
  commonly-used types of calorimeters with electrical heating are described
  below.

  <subsubsection|Adiabatic calorimeters>

  An <I|Adiabatic!calorimeter\|(><I|Calorimeter!adiabatic\|(>adiabatic
  calorimeter is designed to have negligible heat flow to or from its
  surroundings. The calorimeter contains the phase of interest, kept at
  either constant volume or constant pressure, and also an electric heater
  and a temperature-measuring device such as a platinum resistance
  thermometer, thermistor, or quartz crystal oscillator. The contents may be
  stirred to ensure temperature uniformity.

  To minimize conduction and convection, the calorimeter usually is
  surrounded by a jacket separated by an air gap or an evacuated space. The
  outer surface of the calorimeter and inner surface of the jacket may be
  polished to minimize radiation emission from these surfaces. These
  measures, however, are not sufficient to ensure a completely adiabatic
  boundary, because energy can be transferred by heat along the mounting
  hardware and through the electrical leads. Therefore, the temperature of
  the jacket, or of an outer metal shield, is adjusted throughout the course
  of the experiment so as to be as close as possible to the varying
  temperature of the calorimeter. This goal is most easily achieved when the
  temperature change is slow.

  To make a heat capacity measurement, a constant
  <subindex|Electric|current><index|Current, electric>electric current is
  passed through the <index|Heater circuit><subindex|Circuit|heater>heater
  circuit for a known period of time. The <em|system> is the calorimeter and
  its contents. The <subindex|Electrical|work><subindex|Work|electrical>electrical
  work <math|w<el>> performed on the system by the heater circuit is
  calculated from the integrated form of Eq.
  <reference|dw=I2*Rdt><vpageref|dw=I2*Rdt>:
  <math|w<el>=I<rsup|2>*R<el><Del>t>, where <math|I> is the
  <subindex|Electric|current><index|Current, electric>electric current,
  <math|R<el>> is the <subindex|Electric|resistance><subindex|Resistance|electric>electric
  resistance, and <math|<Del>t> is the time interval. We assume the boundary
  is adiabatic and write the first law in the form

  <\equation>
    <label|dU=-pdV+dw(el)+dw(cont)><dif>U=-p<dif>V+<dw><el>+<dw><subs|c*o*n*t>
  </equation>

  where <math|-p<dif>V> is expansion work and <math|w<subs|c*o*n*t>> is any
  continuous mechanical work from stirring (the subscript \Pcont\Q stands for
  continuous). If <subindex|Electrical|work><subindex|Work|electrical>electrical
  work is done on the system by a thermometer using an external electrical
  circuit, such as a platinum resistance thermometer, this work is included
  in <math|w<subs|c*o*n*t>>.

  Consider first an adiabatic calorimeter in which the heating process is
  carried out at <subindex|Calorimeter|constant-volume><em|constant volume>.
  There is no expansion work, and Eq. <reference|dU=-pdV+dw(el)+dw(cont)>
  becomes

  <\gather>
    <tformat|<table|<row|<cell|<dif>U=<dw><el>+<dw><subs|c*o*n*t><cond|<around|(|c*o*n*s*t*a*n*t<math|V>|)>><eq-number><label|dU=dw(el)+dw(cont)>>>>>
  </gather>

  An example of a <I|Heating!curve, of a calorimeter\|reg>measured heating
  curve (temperature <math|T> as a function of time <math|t>) is shown in
  Fig. <reference|fig:7-ad heating curve>.

  <\big-figure>
    <boxedfigure|<image|./07-SUP/adheat.eps||||> <capt|Typical heating curve
    of an adiabatic calorimeter.<label|fig:7-ad heating curve>>>
  </big-figure|>

  We select two points on the heating curve, indicated in the figure by open
  circles. Time <math|t<rsub|1>> is at or shortly before the instant the
  <index|Heater circuit><subindex|Circuit|heater>heater circuit is closed and
  <subindex|Electrical|heating><subindex|Heating|electrical>electrical
  heating begins, and time <math|t<rsub|2>> is after the heater circuit has
  been opened and the slope of the curve has become essentially constant.

  In the time periods before <math|t<rsub|1>> and after <math|t<rsub|2>>, the
  temperature may exhibit a slow rate of increase due to the continuous work
  <math|w<subs|c*o*n*t>> from stirring and temperature measurement. If this
  work is performed at a constant rate throughout the course of the
  experiment, the slope is constant and the same in both time periods as
  shown in the figure.

  The relation between the slope and the rate of work is given by a quantity
  called the <index|Energy equivalent><newterm|energy equivalent>,
  <math|\<epsilon\>>.<label|energy equiv>The energy equivalent is the heat
  capacity of the calorimeter under the conditions of an experiment. The heat
  capacity of a constant-volume calorimeter is given by
  <math|\<epsilon\>=<pd|U|T|V>> (Eq. <reference|CV=dU/dT>). Thus, at times
  before <math|t<rsub|1>> or after <math|t<rsub|2>>, when <math|<dw><el>> is
  zero and <math|<dif>U> equals <math|<dw><subs|c*o*n*t>>, the slope <math|r>
  of the heating curve is given by

  <\equation>
    r=<frac|<dif>T|<dt>>=<frac|<dif>T|<dif>U>*<frac|<dif>U|<dt>>=<frac|1|\<epsilon\>>*<frac|<dw><subs|c*o*n*t>|<dt>>
  </equation>

  The rate of the continuous work is therefore
  <math|<dw><subs|c*o*n*t>/<dt>=\<epsilon\>*r>. This rate is constant
  throughout the experiment. In the time interval from <math|t<rsub|1>> to
  <math|t<rsub|2>>, the total quantity of continuous work is
  <math|w<subs|c*o*n*t>=\<epsilon\>*r*<around|(|t<rsub|2>-t<rsub|1>|)>>,
  where <math|r> is the slope of the heating curve measured <em|outside> this
  time interval.

  To find the energy equivalent, we integrate Eq.
  <reference|dU=dw(el)+dw(cont)> between the two points on the curve:

  <\gather>
    <tformat|<table|<row|<cell|<Del>U=w<el>+w<subs|c*o*n*t>=w<el>+\<epsilon\>*r*<around|(|t<rsub|2>-t<rsub|1>|)><cond|<around|(|c*o*n*s*t*a*n*t<math|V>|)>><eq-number><label|delU=w(el)+er(t2-t1)>>>>>
  </gather>

  Then the average heat capacity between temperatures <math|T<rsub|1>> and
  <math|T<rsub|2>> is

  <\equation>
    \<epsilon\>=<frac|<Del>U|T<rsub|2>-T<rsub|1>>=<frac|w<el>+\<epsilon\>*r*<around|(|t<rsub|2>-t<rsub|1>|)>|T<rsub|2>-T<rsub|1>>
  </equation>

  Solving for <math|\<epsilon\>>, we obtain

  <\equation>
    <label|e=w(el)/(T2-T1-r(t2-t1))>\<epsilon\>=<frac|w<el>|T<rsub|2>-T<rsub|1>-r*<around|(|t<rsub|2>-t<rsub|1>|)>>
  </equation>

  The value of the denominator on the right side is indicated by the vertical
  line in Fig. <reference|fig:7-ad heating curve>. It is the temperature
  change that would have been observed if the same quantity of
  <subindex|Electrical|work><subindex|Work|electrical>electrical work had
  been performed without the continuous work.

  Next, consider the heating process in a calorimeter at
  <subindex|Calorimeter|constant-pressure><em|constant pressure>. In this
  case the enthalpy change is given by <math|<dif>H=<dif>U+p<dif>V> which,
  with substitution from Eq. <reference|dU=-pdV+dw(el)+dw(cont)>, becomes

  <\gather>
    <tformat|<table|<row|<cell|<dif>H=<dw><el>+<dw><inactive|<subs|c*o*n*t>><cond|<around|(|c*o*n*s*t*a*n*t<math|p>|)>><eq-number><label|dH=dw(el)+dw(cont)>>>>>
  </gather>

  We follow the same procedure as for the constant-volume calorimeter, using
  Eq. <reference|dH=dw(el)+dw(cont)> in place of Eq.
  <reference|dU=dw(el)+dw(cont)> and equating the <index|Energy
  equivalent>energy equivalent <math|\<epsilon\>> to <math|<pd|H|T|p>>, the
  heat capacity of the calorimeter at constant pressure (Eq.
  <reference|Cp=dH/dT>). We obtain the relation

  <\gather>
    <tformat|<table|<row|<cell|<Del>H=w<el>+w<subs|c*o*n*t>=w<el>+\<epsilon\>*r*<around|(|t<rsub|2>-t<rsub|1>|)><cond|<around|(|c*o*n*s*t*a*n*t<math|p>|)>><eq-number><label|delH=w(el)+er(t2-t1)>>>>>
  </gather>

  in place of Eq. <reference|delU=w(el)+er(t2-t1)> and end up again with the
  expression of Eq. <reference|e=w(el)/(T2-T1-r(t2-t1))> for
  <math|\<epsilon\>>.

  The value of <math|\<epsilon\>> calculated from Eq.
  <reference|e=w(el)/(T2-T1-r(t2-t1))> is an <em|average> value for the
  temperature interval from <math|T<rsub|1>> to <math|T<rsub|2>>, and we can
  identify this value with the heat capacity at the temperature of the
  midpoint of the interval. By taking the difference of values of
  <math|\<epsilon\>> measured with and without the phase of interest present
  in the calorimeter, we obtain <math|C<rsub|V>> or <math|C<rsub|p>> for the
  phase alone.

  It may seem paradoxical that we can use an adiabatic process, one without
  heat, to evaluate a quantity defined by heat
  (<math|<tx|h*e*a*t*c*a*p*a*c*i*t*y>=<dq>/<dif>T>). The explanation is that
  energy transferred into the adiabatic calorimeter as
  <subindex|Electrical|work><subindex|Work|electrical>electrical work, and
  <subindex|Energy|dissipation of><index|Dissipation of energy>dissipated
  completely to thermal energy, substitutes for the heat that would be needed
  for the same change of state without electrical work.
  <I|Adiabatic!calorimeter\|)><I|Calorimeter!adiabatic\|)>

  <subsubsection|Isothermal-jacket calorimeters><label|7-isothermal-jacket>

  <I|Calorimeter!isothermal-jacket\|(>A second common type of calorimeter is
  similar in construction to an adiabatic calorimeter, except that the
  surrounding jacket is maintained at constant temperature. It is sometimes
  called an <index|Isoperibol calorimeter><subindex|Calorimeter|isoperibol><em|isoperibol
  calorimeter>. A correction is made for heat transfer resulting from the
  difference in temperature across the gap separating the jacket from the
  outer surface of the calorimeter. It is important in making this correction
  that the outer surface have a uniform temperature without \Phot spots.\Q

  Assume the outer surface of the calorimeter has a uniform temperature
  <math|T> that varies with time, the jacket temperature has a constant value
  <math|T<subs|e*x*t>>, and convection has been eliminated by evacuating the
  gap. Then heat transfer is by conduction and radiation, and its rate is
  given by <I|Newton's law of cooling\|reg>Newton's law of cooling

  <\equation>
    <label|dq/dt=-k(T-T(ext))><frac|<dq>|<dt>>=-k*<around|(|T-T<subs|e*x*t>|)>
  </equation>

  where <math|k> is a constant (the <subindex|Thermal|conductance>thermal
  conductance). Heat flows from a warmer to a cooler body, so
  <math|<dq>/<dt>> is positive if <math|T> is less than <math|T<subs|e*x*t>>
  and negative if <math|T> is greater than <math|T<subs|e*x*t>>.

  The possible kinds of work are the same as for the adiabatic calorimeter:
  expansion work <math|-p<dif>V>, intermittent work <math|w<el>> done by the
  <index|Heater circuit><subindex|Circuit|heater>heater circuit, and
  continuous work <math|w<subs|c*o*n*t>>. By combining the first law and Eq.
  <reference|dq/dt=-k(T-T(ext))>, we obtain the following relation for the
  rate at which the internal energy changes:

  <\equation>
    <frac|<dif>U|<dt>>=<frac|<dq>|<dt>>+<frac|<dw>|<dt>>=-k*<around|(|T-T<subs|e*x*t>|)>-p*<frac|<dif>V|<dt>>+<frac|<dw><el>|<dt>>+<frac|<dw><subs|c*o*n*t>|<dt>>
  </equation>

  For heating at constant <em|volume> (<math|<dif>V/<dt>=0>), this relation
  becomes

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif>U|<dt>>=-k*<around|(|T-T<subs|e*x*t>|)>+<frac|<dw><el>|<dt>>+<frac|<dw><subs|c*o*n*t>|<dt>><cond|<around|(|c*o*n*s*t*a*n*t<math|V>|)>><eq-number><label|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt>>>>>
  </gather>

  An example of a <I|Heating!curve, of a calorimeter\|reg>heating curve is
  shown in Fig. <reference|fig:7-heating curve>.

  <\big-figure>
    <boxedfigure|<image|./07-SUP/heatcurv.eps||||> <capt|Typical heating
    curve of an isothermal-jacket calorimeter.<label|fig:7-heating curve>>>
  </big-figure|>

  In contrast to the curve of Fig. <reference|fig:7-ad heating curve>, the
  slopes are different before and after the heating interval due to the
  changed rate of heat flow. Times <math|t<rsub|1>> and <math|t<rsub|2>> are
  before and after the <index|Heater circuit><subindex|Circuit|heater>heater
  circuit is closed. In any time interval before time <math|t<rsub|1>> or
  after time <math|t<rsub|2>>, the system behaves as if it is approaching a
  steady state of constant temperature <math|T<rsub|\<infty\>>> (called the
  <index|Convergence temperature><subindex|Temperature|convergence>convergence
  temperature), which it would eventually reach if the experiment were
  continued without closing the heater circuit. <math|T<rsub|\<infty\>>> is
  greater than <math|T<subs|e*x*t>> because of the energy transferred to the
  system by stirring and electrical temperature measurement. By setting
  <math|<dif>U/<dt>> and <math|<dw><el>/<dt>> equal to zero and <math|T>
  equal to <math|T<rsub|\<infty\>>> in Eq.
  <reference|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt>, we obtain
  <math|<dw><subs|c*o*n*t>/<dt>=k*<around|(|T<rsub|\<infty\>>-T<subs|e*x*t>|)>>.
  We assume <math|<dw><subs|c*o*n*t>/<dt>> is constant. Substituting this
  expression into Eq. <reference|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt>
  gives us a general expression for the rate at which <math|U> changes in
  terms of the unknown quantities <math|k> and <math|T<rsub|\<infty\>>>:

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif>U|<dt>>=-k*<around|(|T-T<rsub|\<infty\>>|)>+<frac|<dw><el>|<dt>><cond|<around|(|c*o*n*s*t*a*n*t<math|V>|)>><eq-number><label|dU/dt=-k(T-T(inf)+dw(el)/dt>>>>>
  </gather>

  This relation is valid throughout the experiment, not only while the heater
  circuit is closed. If we multiply by <math|<dt>> and integrate from
  <math|t<rsub|1>> to <math|t<rsub|2>>, we obtain the internal energy change
  in the time interval from <math|t<rsub|1>> to <math|t<rsub|2>>:

  <\gather>
    <tformat|<table|<row|<cell|<Del>U=-k*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><around|(|T-T<rsub|\<infty\>>|)><dt>+w<el><cond|<around|(|c*o*n*s*t*a*n*t<math|V>|)>><eq-number><label|delU=-k
    int((T-T(inf))dt+w(el)>>>>>
  </gather>

  All the intermittent work <math|w<el>> is performed in this time interval.

  <\quote-env>
    \ The derivation of Eq. <reference|delU=-k int((T-T(inf))dt+w(el)> is a
    general one. The equation can be applied also to a isothermal-jacket
    calorimeter in which a reaction is occurring. Section <reference|11-bomb
    calorimeter> will mention the use of this equation for an internal energy
    correction of a reaction calorimeter with an isothermal jacket.
  </quote-env>

  The average value of the <index|Energy equivalent>energy equivalent in the
  temperature range <math|T<rsub|1>> to <math|T<rsub|2>> is

  <\equation>
    \<epsilon\>=<frac|<Del>U|T<rsub|2>-T<rsub|1>>=<frac|<D>-\<epsilon\>*<around|(|k/\<epsilon\>|)>*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><around|(|T-T<rsub|\<infty\>>|)><dt>+w<el>|T<rsub|2>-T<rsub|1>>
  </equation>

  Solving for <math|\<epsilon\>>, we obtain

  <\equation>
    <label|e=w(el)/[(T2-T1+(k/e)int(T-T(inf))dt]>\<epsilon\>=<frac|w<el>|<D><around|(|T<rsub|2>-T<rsub|1>|)>+<around|(|k/\<epsilon\>|)>*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><around|(|T-T<rsub|\<infty\>>|)><dt>>
  </equation>

  The value of <math|w<el>> is known from <math|w<el>=I<rsup|2>*R<el><Del>t>,
  where <math|<Del>t> is the time interval during which the <index|Heater
  circuit><subindex|Circuit|heater>heater circuit is closed. The integral can
  be evaluated numerically once <math|T<rsub|\<infty\>>> is known.

  For heating at constant <em|pressure>, <math|<dif>H> is equal to
  <math|<dif>U+p<dif>V>, and we can write

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif>H|<dt>>=<frac|<dif>U|<dt>>+p*<frac|<dif>V|<dt>>=-k*<around|(|T-T<subs|e*x*t>|)>+<frac|<dw><el>|<dt>>+<frac|<dw><subs|c*o*n*t>|<dt>><cond|<around|(|c*o*n*s*t*a*n*t<math|p>|)>><eq-number>>>>>
  </gather>

  which is analogous to Eq. <reference|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt>.
  By the procedure described above for the case of constant <math|V>, we
  obtain

  <\gather>
    <tformat|<table|<row|<cell|<Del>H=-k*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><around|(|T-T<rsub|\<infty\>>|)><dt>+w<el><cond|<around|(|c*o*n*s*t*a*n*t<math|p>|)>><eq-number><label|delH=-k
    int((T-T(inf))dt+w(el)>>>>>
  </gather>

  At constant <math|p>, the <index|Energy equivalent>energy equivalent is
  equal to <math|C<rsub|p>=<Del>H/<around|(|T<rsub|2>-T<rsub|1>|)>>, and the
  final expression for <math|\<epsilon\>> is the same as that given by Eq.
  <reference|e=w(el)/[(T2-T1+(k/e)int(T-T(inf))dt]>.

  To obtain values of <math|k/\<epsilon\>> and <math|T<rsub|\<infty\>>> for
  use in Eq. <reference|e=w(el)/[(T2-T1+(k/e)int(T-T(inf))dt]>, we need the
  slopes of the <I|Heating!curve, of a calorimeter\|reg>heating curve in time
  intervals (rating periods) just before <math|t<rsub|1>> and just after
  <math|t<rsub|2>>. Consider the case of constant <em|volume>. In these
  intervals, <math|<dw><el>/<dt>> is zero and <math|<dif>U/<dt>> equals
  <math|-k*<around|(|T-T<rsub|\<infty\>>|)>> (from Eq.
  <reference|dU/dt=-k(T-T(inf)+dw(el)/dt>). The heat capacity at constant
  volume is <math|C<rsub|V>=<dif>U/<dif>T>. The slope <math|r> in general is
  then given by

  <\equation>
    r=<frac|<dif>T|<dt>>=<frac|<dif>T|<dif>U>*<frac|<dif>U|<dt>>=-<around|(|k/\<epsilon\>|)>*<around|(|T-T<rsub|\<infty\>>|)>
  </equation>

  Applying this relation to the points at times <math|t<rsub|1>> and
  <math|t<rsub|2>>, we have the following simultaneous equations in the
  unknowns <math|k/\<epsilon\>> and <math|T<rsub|\<infty\>>>:

  <\equation>
    r<rsub|1>=-<around|(|k/\<epsilon\>|)>*<around|(|T<rsub|1>-T<rsub|\<infty\>>|)>*<space|2em>r<rsub|2>=-<around|(|k/\<epsilon\>|)>*<around|(|T<rsub|2>-T<rsub|\<infty\>>|)>
  </equation>

  The solutions are

  <\equation>
    <around|(|k/\<epsilon\>|)>=<frac|r<rsub|1>-r<rsub|2>|T<rsub|2>-T<rsub|1>>*<space|2em>T<rsub|\<infty\>>=<frac|r<rsub|1>*T<rsub|2>-r<rsub|2>*T<rsub|1>|r<rsub|1>-r<rsub|2>>
  </equation>

  Finally, <math|k> is given by

  <\equation>
    <label|k=[(r1-r2)/(T2-T1)]e>k=<around|(|k/\<epsilon\>|)>*\<epsilon\>=<around*|(|<frac|r<rsub|1>-r<rsub|2>|T<rsub|2>-T<rsub|1>>|)>*\<epsilon\>
  </equation>

  When the <em|pressure> is constant, this procedure yields the same
  relations for <math|k/\<epsilon\>>, <math|T<rsub|\<infty\>>>, and <math|k>.
  <I|Calorimeter!isothermal-jacket\|)>

  <subsubsection|Continuous-flow calorimeters>

  A flow calorimeter <I|Calorimeter!continuous-flow\|(>is a third type of
  calorimeter used to measure the heat capacity of a fluid phase. The gas or
  liquid flows through a tube at a known constant rate past an
  <subindex|Electrical|heating><subindex|Heating|electrical>electrical heater
  of known constant power input. After a steady state has been achieved in
  the tube, the temperature increase <math|<Del>T> at the heater is measured.

  If <math|<dw><el>/<dt>> is the rate at which
  <subindex|Electrical|work><subindex|Work|electrical>electrical work is
  performed (the <subindex|Electric|power>electric power) and
  <math|<dif>m/<dt>> is the mass flow rate, then in time interval
  <math|<Del>t> a quantity <math|w=<around|(|<dw><el>/<dt>|)><Del>t> of work
  is performed on an amount <math|n=<around|(|<dif>m/<dt>|)><Del>t/M> of the
  fluid (where <math|M> is the molar mass). If heat flow is negligible, the
  molar heat capacity of the substance is given by

  <\equation>
    <Cpm>=<frac|w|n<Del>T>=<frac|M*<around|(|<dw><el>/<dt>|)>|<Del>T*<around|(|<dif>m/<dt>|)>>
  </equation>

  To correct for the effects of heat flow, <math|<Del>T> is usually measured
  over a range of flow rates and the results extrapolated to infinite flow
  rate. <I|Calorimeter!continuous-flow\|)>

  <subsection|Typical values>

  Figure <reference|fig:7-Cp vs T><vpageref|fig:7-Cp vs T>

  <\big-figure>
    <boxedfigure|<image|./07-SUP/Cp-T.eps||||> <capt|Temperature dependence
    of molar heat capacity at constant pressure (<math|p=1<br>>) of
    H<rsub|<math|2>>O, N<rsub|<math|2>>, and C(graphite).<label|fig:7-Cp vs
    T>>>
  </big-figure|>

  shows the temperature dependence of <math|<Cpm>> for several substances.
  The discontinuities seen at certain temperatures occur at equilibrium phase
  transitions. At these temperatures the heat capacity is in effect infinite,
  since the phase transition of a pure substance involves finite heat with
  zero temperature change.

  <section|Heating at Constant Volume or Pressure><label|7-heating at const V
  or p>

  <I|Heating!constant volume@at constant volume or pressure\|(>Consider the
  process of changing the temperature of a phase at constant
  volume.<footnote|Keeping the volume exactly constant while increasing the
  temperature is not as simple as it may sound. Most solids expand when
  heated, unless we arrange to increase the external pressure at the same
  time. If we use solid walls to contain a fluid phase, the container volume
  will change with temperature. For practical purposes, these volume changes
  are usually negligible.> The rate of change of internal energy with
  <math|T> under these conditions is the heat capacity at constant volume:
  <math|C<rsub|V>=<pd|U|T|V>> (Eq. <reference|dU=dq,C_V=dU/dT>). Accordingly,
  an infinitesimal change of <math|U> is given by

  <\gather>
    <tformat|<table|<row|<cell|<dif>U=C<rsub|V><dif>T<cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>,constant
    <math|V>)><eq-number><label|dU=CVdT>>>>>
  </gather>

  and the finite change of <math|U> between temperatures <math|T<rsub|1>> and
  <math|T<rsub|2>> is

  <\gather>
    <tformat|<table|<row|<cell|<Del>U=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em>C<rsub|V><dif>T<cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>,constant
    <math|V>)><eq-number><label|delU=int(CV)dT>>>>>
  </gather>

  Three comments, relevant to these and other equations in this chapter, are
  in order:

  <\enumerate>
    <item>Equation <reference|delU=int(CV)dT> allows us to calculate the
    finite change of a state function, <math|U>, by integrating
    <math|C<rsub|V>> over <math|T>. The equation was derived under the
    condition that <math|V> is constant during the process, and the use of
    the integration variable <math|T> implies that the system has a single,
    uniform temperature at each instant during the process. The integrand
    <math|C<rsub|V>> may depend on both <math|V> and <math|T>, and we should
    integrate with <math|V> held constant and <math|C<rsub|V>> treated as a
    function only of <math|T>.

    <item>Suppose we want to evaluate <math|<Del>U> for a process in which
    the volume is the same in the initial and final states
    (<math|V<rsub|2>=V<rsub|1>>) but is different in some intermediate
    states, and the temperature is <em|not> uniform in some of the
    intermediate states. We know the change of a state function depends only
    on the initial and final states, so we can still use Eq.
    <reference|delU=int(CV)dT> to evaluate <math|<Del>U> for this process. We
    integrate with <math|V> held constant, although <math|V> was not constant
    during the actual process.

    In general: A finite change <math|<Del>X> of a state function, evaluated
    under the condition that another state function <math|Y> is constant, is
    the same as <math|<Del>X> under the less stringent condition
    <math|Y<rsub|2>=Y<rsub|1>>. (Another application of this principle was
    mentioned in Sec. <reference|4-rev expansion>.)

    <item>For a pure substance, we may convert an expression for an
    infinitesimal or finite change of an extensive property to an expression
    for the change of the corresponding <subindex|Molar|quantity><subindex|Property|molar><em|molar>
    property by dividing by <math|n>. For instance, Eq. <reference|dU=CVdT>
    becomes

    <\equation>
      <dif>U<m>=<CVm><dif>T
    </equation>

    and Eq. <reference|delU=int(CV)dT> becomes

    <\equation>
      <Del>U<m>=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em><CVm><dif>T
    </equation>
  </enumerate>

  If, at a fixed volume and over the temperature range <math|T<rsub|1>> to
  <math|T<rsub|2>>, the value of <math|C<rsub|V>> is essentially constant
  (i.e., independent of <math|T>), Eq. <reference|delU=int(CV)dT> becomes

  <\gather>
    <tformat|<table|<row|<cell|<Del>U=C<rsub|V>*<around|(|T<rsub|2>-T<rsub|1>|)><cond|(c*l*o*s*e*d*s*y*s*t*e*m,<math|C=1>,><nextcond|<math|P=1>,constant
    <math|V> and <math|C<rsub|V>>)><eq-number><label|delU=CV(T2-T1)>>>>>
  </gather>

  \;

  <I|Entropy!change!constant volume@at constant volume\|reg>An infinitesimal
  entropy change during a reversible process in a closed system is given
  according to the second law by <math|<dif>S=<dq>/T>. At constant volume,
  <math|<dq>> is equal to <math|<dif>U> which in turn equals
  <math|C<rsub|V><dif>T>. Therefore, the entropy change is

  <\gather>
    <tformat|<table|<row|<cell|<dif>S=<frac|C<rsub|V>|T><dif>T<cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>,constant
    <math|V>)><eq-number><label|dS=(CV/T)dT>>>>>
  </gather>

  Integration yields the finite change

  <\gather>
    <tformat|<table|<row|<cell|<Del>S=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|C<rsub|V>|T><dif>T<cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>,constant
    <math|V>)><eq-number><label|delS=int(CV/T)dT>>>>>
  </gather>

  If <math|C<rsub|V>> is treated as constant, Eq.
  <reference|delS=int(CV/T)dT> becomes

  <\gather>
    <tformat|<table|<row|<cell|<Del>S=C<rsub|V>*ln
    <frac|T<rsub|2>|T<rsub|1>><cond|(c*l*o*s*e*d*s*y*s*t*e*m,<math|C=1>,><nextcond|<math|P=1>,constant
    <math|V> and <math|C<rsub|V>>)><eq-number><label|delS=CV*ln(T2/T1)>>>>>
  </gather>

  (More general versions of the two preceding equations have already been
  given in Sec. <reference|4-rev heating \ expansion>.)

  Since <math|C<rsub|V>> is positive, we see from Eqs.
  <reference|delU=int(CV)dT> and <reference|delS=int(CV/T)dT> that heating a
  phase at constant volume causes both <math|U> and <math|S> to increase.

  We may derive relations for a temperature change at constant <em|pressure>
  by the same methods. From <math|C<rsub|p>=<pd|H|T|p>> (Eq.
  <reference|dH=dq,C_p=dH/dT>), we obtain <subindex|Enthalpy|change at
  constant pressure>

  <\gather>
    <tformat|<table|<row|<cell|<Del>H=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>>C<rsub|p><dif>T<cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>,constant
    <math|p>)><eq-number><label|delH=int(Cp)dT>>>>>
  </gather>

  If <math|C<rsub|p>> is treated as constant, Eq. <reference|delH=int(Cp)dT>
  becomes

  <\gather>
    <tformat|<table|<row|<cell|<Del>H=C<rsub|p>*<around|(|T<rsub|2>-T<rsub|1>|)><cond|(c*l*o*s*e*d*s*y*s*t*e*m,<math|C=1>,><nextcond|<math|P=1>,constant
    <math|p> and <math|C<rsub|p>>)><eq-number><label|delH=(Cp)delT>>>>>
  </gather>

  From <math|<dif>S=<dq>/T> and Eq. <reference|dH=dq,C_p=dH/dT> we obtain for
  the entropy change at constant pressure <I|Entropy!change!constant
  pressure@at constant pressure\|reg>

  <\gather>
    <tformat|<table|<row|<cell|<dif>S=<frac|C<rsub|p>|T><dif>T<cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>,constant
    <math|p>)><eq-number><label|dS=(Cp/T)dT>>>>>
  </gather>

  Integration gives

  <\gather>
    <tformat|<table|<row|<cell|<Del>S=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|C<rsub|p>|T><dif>T<cond|(c*l*o*s*e*d*s*y*s*t*e*m,><nextcond|<math|C=1>,<math|P=1>,constant
    <math|p>)><eq-number><label|delS=int(Cp/T)dT>>>>>
  </gather>

  or, with <math|C<rsub|p>> treated as constant,

  <\gather>
    <tformat|<table|<row|<cell|<Del>S=C<rsub|p>*ln
    <frac|T<rsub|2>|T<rsub|1>><cond|(c*l*o*s*e*d*s*y*s*t*e*m,<math|C=1>,><nextcond|<math|P=1>,constant
    <math|p> and <math|C<rsub|p>>)><eq-number><label|delS=Cp*ln(T2/T1)>>>>>
  </gather>

  <math|C<rsub|p>> is positive, so heating a phase at constant pressure
  causes <math|H> and <math|S> to increase.

  The Gibbs energy changes according to <math|<pd|G|T|p>=-S> (Eq.
  <reference|dG/dT=-S>), so heating at constant pressure causes <math|G> to
  decrease. <I|Heating!constant volume@at constant volume or pressure\|)>

  <section|Partial Derivatives with Respect to <math|T>, <math|p>, and
  <math|V>>

  <subsection|Tables of partial derivatives><label|7-partial derivatives>

  <I|Partial derivative!expressions at constant <math|T>, <math|p>, and
  <math|V>\|(>The tables in this section collect useful expressions for
  partial derivatives of the eight state functions <math|T>, <math|p>,
  <math|V>, <math|U>, <math|H>, <math|A>, <math|G>, and <math|S> in a closed,
  single-phase system. Each derivative is taken with respect to one of the
  three easily-controlled variables <math|T>, <math|p>, or <math|V> while
  another of these variables is held constant. We have already seen some of
  these expressions, and the derivations of the others are indicated below.

  We can use these partial derivatives (1) for writing an expression for the
  total differential of any of the eight quantities, and (2) for expressing
  the finite change in one of these quantities as an integral under
  conditions of constant <math|T>, <math|p>, or <math|V>. For instance, given
  the expressions

  <\equation>
    <Pd|S|T|<space|-0.17em>p>=<frac|C<rsub|p>|T><space|2em><tx|a*n*d><space|2em><Pd|S|p|T>=-\<alpha\>*V
  </equation>

  we may write the total differential of <math|S>, taking <math|T> and
  <math|p> as the independent variables, as

  <\equation>
    <dif>S=<frac|C<rsub|p>|T><dif>T-\<alpha\>*V<difp>
  </equation>

  Furthermore, the first expression is equivalent to the differential form

  <\equation>
    <dif>S=<frac|C<rsub|p>|T><dif>T
  </equation>

  provided <math|p> is constant; we can integrate this equation to obtain the
  finite change <math|<Del>S> under isobaric conditions as shown in Eq.
  <reference|delS=int(Cp/T)dT>.

  Both general expressions and expressions valid for an ideal gas are given
  in Tables <reference|tbl:7-const T>, <reference|tbl:7-const p>, and
  <reference|tbl:7-const V>.

  <\big-table>
    <minipagetable|11.0cm|<math|<tabular*|<tformat|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|2|2|1|-1|cell-bborder|1ln>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|5|5|1|-1|cell-valign|top>|<cwith|5|5|1|-1|cell-vmode|exact>|<cwith|5|5|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|6|6|1|-1|cell-valign|top>|<cwith|6|6|1|-1|cell-vmode|exact>|<cwith|6|6|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|7|7|1|-1|cell-valign|top>|<cwith|7|7|1|-1|cell-vmode|exact>|<cwith|7|7|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|8|8|1|-1|cell-valign|top>|<cwith|8|8|1|-1|cell-vmode|exact>|<cwith|8|8|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|8|8|1|-1|cell-bborder|1ln>|<table|<row|<cell|<tx|P*a*r*t*i*a*l>>|<cell|<tx|G*e*n*e*r*a*l>>|<cell|<tx|I*d*e*a*l>>|<cell|<tx|P*a*r*t*i*a*l>>|<cell|<tx|G*e*n*e*r*a*l>>|<cell|<tx|I*d*e*a*l>>>|<row|<cell|<tx|d*e*r*i*v*a*t*i*v*e>>|<cell|<tx|e*x*p*r*e*s*s*i*o*n>>|<cell|<tx|g*a*s>>|<cell|<tx|d*e*r*i*v*a*t*i*v*e>>|<cell|<tx|e*x*p*r*e*s*s*i*o*n>>|<cell|<tx|g*a*s>>>|<row|<cell|<Strut|.*6*c*m><D><Pd|p|V|T>>|<cell|<D>-<frac|1|<kT>V>>|<cell|<D>-<frac|p|V>>|<cell|<D><Pd|A|p|T>>|<cell|<D><kT>p*V>|<cell|V>>|<row|<cell|<D><Pd|V|p|T>>|<cell|<D>-<kT>V>|<cell|<D>-<frac|V|p>>|<cell|<D><Pd|A|V|T>>|<cell|<D>-p>|<cell|-p>>|<row|<cell|<D><Pd|U|p|T>>|<cell|<D><around|(|-\<alpha\>*T+<kT>p|)>*V>|<cell|0>|<cell|<D><Pd|G|p|T>>|<cell|<D>V>|<cell|V>>|<row|<cell|<D><Pd|U|V|T>>|<cell|<D><frac|\<alpha\>*T|<kT>>-p>|<cell|0>|<cell|<D><Pd|G|V|T>>|<cell|<D>-<frac|1|<kT>>>|<cell|-p>>|<row|<cell|<D><Pd|H|p|T>>|<cell|<D><around|(|1-\<alpha\>*T|)>*V>|<cell|0>|<cell|<D><Pd|S|p|T>>|<cell|<D>-\<alpha\>*V>|<cell|<D>-<frac|V|T>>>|<row|<cell|<D><Pd|H|V|T>>|<cell|<D><frac|\<alpha\>*T-1|<kT>>>|<cell|0>|<cell|<D><Pd|S|V|T>>|<cell|<D><frac|\<alpha\>|<kT>>>|<cell|<D><frac|p|T>>>>>>>>
  </big-table|<em|Constant temperature>: expressions for partial derivatives
  of state functions with respect to pressure and volume in a closed,
  single-phase system>

  <\big-table>
    <minipagetable|11.2cm|<label|tbl:7-const
    p><math|<tabular*|<tformat|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|2|2|1|-1|cell-bborder|1ln>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|5|5|1|-1|cell-valign|top>|<cwith|5|5|1|-1|cell-vmode|exact>|<cwith|5|5|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|6|6|1|-1|cell-valign|top>|<cwith|6|6|1|-1|cell-vmode|exact>|<cwith|6|6|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|7|7|1|-1|cell-valign|top>|<cwith|7|7|1|-1|cell-vmode|exact>|<cwith|7|7|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|8|8|1|-1|cell-valign|top>|<cwith|8|8|1|-1|cell-vmode|exact>|<cwith|8|8|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|8|8|1|-1|cell-bborder|1ln>|<table|<row|<cell|<tx|P*a*r*t*i*a*l>>|<cell|<tx|G*e*n*e*r*a*l>>|<cell|<tx|I*d*e*a*l>>|<cell|<tx|P*a*r*t*i*a*l>>|<cell|<tx|G*e*n*e*r*a*l>>|<cell|<tx|I*d*e*a*l>>>|<row|<cell|<tx|d*e*r*i*v*a*t*i*v*e>>|<cell|<tx|e*x*p*r*e*s*s*i*o*n>>|<cell|<tx|g*a*s>>|<cell|<tx|d*e*r*i*v*a*t*i*v*e>>|<cell|<tx|e*x*p*r*e*s*s*i*o*n>>|<cell|<tx|g*a*s>>>|<row|<cell|<Strut|.*6*c*m><D><Pd|T|V|<space|-0.17em>p>>|<cell|<D><frac|1|\<alpha\>*V>>|<cell|<D><frac|T|V>>|<cell|<D><Pd|A|T|<space|-0.17em>p>>|<cell|<D>-\<alpha\>*p*V-S>|<cell|<D>-<frac|p*V|T>-S>>|<row|<cell|<D><Pd|V|T|<space|-0.17em>p>>|<cell|<D>\<alpha\>*V>|<cell|<D><frac|V|T>>|<cell|<D><Pd|A|V|<space|-0.17em>p>>|<cell|<D>-p-<frac|S|\<alpha\>*V>>|<cell|<D>-p-<frac|T*S|V>>>|<row|<cell|<D><Pd|U|T|<space|-0.17em>p>>|<cell|<D>C<rsub|p>-\<alpha\>*p*V>|<cell|C<rsub|V>>|<cell|<D><Pd|G|T|<space|-0.17em>p>>|<cell|<D>-S>|<cell|-S>>|<row|<cell|<D><Pd|U|V|<space|-0.17em>p>>|<cell|<D><frac|C<rsub|p>|\<alpha\>*V>-p>|<cell|<D><frac|C<rsub|V>*T|V>>|<cell|<D><Pd|G|V|<space|-0.17em>p>>|<cell|<D>-<frac|S|\<alpha\>*V>>|<cell|<D>-<frac|T*S|V>>>|<row|<cell|<D><Pd|H|T|<space|-0.17em>p>>|<cell|<D>C<rsub|p>>|<cell|C<rsub|p>>|<cell|<D><Pd|S|T|<space|-0.17em>p>>|<cell|<D><frac|C<rsub|p>|T>>|<cell|<D><frac|C<rsub|p>|T>>>|<row|<cell|<D><Pd|H|V|<space|-0.17em>p>>|<cell|<D><frac|C<rsub|p>|\<alpha\>*V>>|<cell|<D><frac|C<rsub|p>*T|V>>|<cell|<D><Pd|S|V|<space|-0.17em>p>>|<cell|<D><frac|C<rsub|p>|\<alpha\>*T*V>>|<cell|<D><frac|C<rsub|p>|V>>>>>>>>
  </big-table|<em|Constant pressure>: expressions for partial derivatives of
  state functions with respect to temperature and volume in a closed,
  single-phase system>

  <\big-table>
    <minipagetable|12.5cm|<label|tbl:7-const
    V><math|<tabular*|<tformat|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|2|2|1|-1|cell-bborder|1ln>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|5|5|1|-1|cell-valign|top>|<cwith|5|5|1|-1|cell-vmode|exact>|<cwith|5|5|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|6|6|1|-1|cell-valign|top>|<cwith|6|6|1|-1|cell-vmode|exact>|<cwith|6|6|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|7|7|1|-1|cell-valign|top>|<cwith|7|7|1|-1|cell-vmode|exact>|<cwith|7|7|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|8|8|1|-1|cell-valign|top>|<cwith|8|8|1|-1|cell-vmode|exact>|<cwith|8|8|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|8|8|1|-1|cell-bborder|1ln>|<table|<row|<cell|<tx|P*a*r*t*i*a*l>>|<cell|<tx|G*e*n*e*r*a*l>>|<cell|<tx|I*d*e*a*l>>|<cell|<tx|P*a*r*t*i*a*l>>|<cell|<tx|G*e*n*e*r*a*l>>|<cell|<tx|I*d*e*a*l>>>|<row|<cell|<tx|d*e*r*i*v*a*t*i*v*e>>|<cell|<tx|e*x*p*r*e*s*s*i*o*n>>|<cell|<tx|g*a*s>>|<cell|<tx|d*e*r*i*v*a*t*i*v*e>>|<cell|<tx|e*x*p*r*e*s*s*i*o*n>>|<cell|<tx|g*a*s>>>|<row|<cell|<Strut|.*6*c*m><D><Pd|T|p|V>>|<cell|<D><frac|<kT>|\<alpha\>>>|<cell|<D><frac|T|p>>|<cell|<D><Pd|A|T|V>>|<cell|<D>-S>|<cell|<D>-S>>|<row|<cell|<D><Pd|p|T|V>>|<cell|<D><frac|\<alpha\>|<kT>>>|<cell|<D><frac|p|T>>|<cell|<D><Pd|A|p|V>>|<cell|<D>-<frac|<kT>S|\<alpha\>>>|<cell|<D>-<frac|T*S|p>>>|<row|<cell|<D><Pd|U|T|V>>|<cell|<D>C<rsub|V>>|<cell|C<rsub|V>>|<cell|<D><Pd|G|T|V>>|<cell|<D><frac|\<alpha\>*V|<kT>>-S>|<cell|<D><frac|p*V|T>-S>>|<row|<cell|<D><Pd|U|p|V>>|<cell|<D><frac|<kT>C<rsub|p>|\<alpha\>>-\<alpha\>*T*V>|<cell|<D><frac|T*C<rsub|V>|p>>|<cell|<D><Pd|G|p|V>>|<cell|<D>V-<frac|<kT>S|\<alpha\>>>|<cell|<D>V-<frac|T*S|p>>>|<row|<cell|<D><Pd|H|T|V>>|<cell|<D>C<rsub|p>+<frac|\<alpha\>*V|<kT>>*<around|(|1-\<alpha\>*T|)>>|<cell|C<rsub|p>>|<cell|<D><Pd|S|T|V>>|<cell|<D><frac|C<rsub|V>|T>>|<cell|<D><frac|C<rsub|V>|T>>>|<row|<cell|<D><Pd|H|p|V>>|<cell|<D><frac|<kT>C<rsub|p>|\<alpha\>>+V*<around|(|1-\<alpha\>*T|)>>|<cell|<D><frac|C<rsub|p>*T|p>>|<cell|<D><Pd|S|p|V>>|<cell|<D><frac|<kT>C<rsub|p>|\<alpha\>*T>-\<alpha\>*V>|<cell|<D><frac|C<rsub|V>|p>>>>>>>>
  </big-table|<em|Constant volume>: expressions for partial derivatives of
  state functions with respect to temperature and pressure in a closed,
  single-phase system>

  <\quote-env>
    \ We may derive the general expressions as follows. We are considering
    differentiation with respect only to <math|T>, <math|p>, and <math|V>.
    Expressions for <math|<pd|V|T|p>>, <math|<pd|V|p|T>>, and
    <math|<pd|p|T|V>> come from Eqs. <reference|alpha def>, <reference|kappaT
    def>, and <reference|dp/dT=alpha/kappaT> and are shown as functions of
    <math|\<alpha\>> and <math|<kT>>. The reciprocal of each of these three
    expressions provides the expression for another partial derivative from
    the general relation

    <\equation>
      <pd|y|x|z>=<frac|1|<pd|x|y|z>>
    </equation>

    This procedure gives us expressions for the six partial derivatives of
    <math|T>, <math|p>, and <math|V>.

    The remaining expressions are for partial derivatives of <math|U>,
    <math|H>, <math|A>, <math|G>, and <math|S>. We obtain the expression for
    <math|<pd|U|T|V>> from Eq. <reference|dU=dq,C_V=dU/dT>, for
    <math|<pd|U|V|T>> from Eq. <reference|dU/dV=alpha*T/kappaT-p>, for
    <math|<pd|H|T|p>> from Eq. <reference|dH=dq,C_p=dH/dT>, for
    <math|<pd|A|T|V>> from Eq. <reference|dA/dT=-S>, for <math|<pd|A|V|T>>
    from Eq. <reference|dA/dV=-p>, for <math|<pd|G|p|T>> from Eq.
    <reference|dG/dp=V>, for <math|<pd|G|T|p>> from Eq. <reference|dG/dT=-S>,
    for <math|<pd|S|T|V>> from Eq. <reference|dS=(CV/T)dT>, for
    <math|<pd|S|T|p>> from Eq. <reference|dS=(Cp/T)dT>, and for
    <math|<pd|S|p|T>> from Eq. <reference|-dS/dp=dV/dT>.

    We can transform each of these partial derivatives, and others derived in
    later steps, to two other partial derivatives with the same variable held
    constant and the variable of differentiation changed. The transformation
    involves multiplying by an appropriate partial derivative of <math|T>,
    <math|p>, or <math|V>. For instance, from the partial derivative
    <math|<pd|U|V|T>=<around|(|\<alpha\>*T/<kT>|)>-p>, we obtain

    <\equation>
      <Pd|U|p|T>=<Pd|U|V|T><Pd|V|p|T>=<around*|(|<frac|\<alpha\>*T|<kT>>-p|)>*<around*|(|-<kT>V|)>=<around*|(|-\<alpha\>*T+<kT>p|)>*V
    </equation>

    The remaining partial derivatives can be found by differentiating
    <math|U=H-p*V>, <math|H=U+p*V>, <math|A=U-T*S>, and <math|G=H-T*S> and
    making appropriate substitutions. Whenever a partial derivative appears
    in a derived expression, it is replaced with an expression derived in an
    earlier step. The expressions derived by these steps constitute the full
    set shown in Tables <reference|tbl:7-const T>, <reference|tbl:7-const p>,
    and <reference|tbl:7-const V>.

    <index|Bridgman, Percy>Bridgman<footnote|Ref. <cite|bridgman-14>; Ref.
    <cite|Bridgman-61>, p. 199--241.> devised a simple method to obtain
    expressions for these and many other partial derivatives from a
    relatively small set of formulas.
  </quote-env>

  \ <I|Partial derivative!expressions at constant <math|T>, <math|p>, and
  <math|V>\|)>

  <subsection|The Joule\UThomson coefficient><label|7-JK coeff>

  <I|Joule--Thomson!coefficient\|(>The Joule\UThomson coefficient of a gas
  was defined in Eq. <reference|mu(JT) def><vpageref|mu(JT) def> by
  <math|\<mu\><subs|J*T>=<pd|T|p|H>>. It can be evaluated with measurements
  of <math|T> and <math|p> during adiabatic throttling processes as described
  in Sec. <reference|6-J-T expansion>.

  To relate <math|\<mu\><subs|J*T>> to other properties of the gas, we write
  the total differential of the enthalpy of a closed, single-phase system in
  the form

  <\equation>
    <dif>H=<Pd|H|T|<space|-0.17em>p><dif>T+<Pd|H|p|T><difp>
  </equation>

  and divide both sides by <math|<difp>>:

  <\equation>
    <frac|<dif>H|<difp>>=<Pd|H|T|<space|-0.17em>p><frac|<dif>T|<difp>>+<Pd|H|p|T>
  </equation>

  Next we impose a condition of constant <math|H>; the ratio
  <math|<dif>T/<difp>> becomes a partial derivative:

  <\equation>
    0=<Pd|H|T|<space|-0.17em>p><Pd|T|p|H>+<Pd|H|p|T>
  </equation>

  Rearrangement gives

  <\equation>
    <Pd|T|p|H>=-<frac|<pd|H|p|T>|<pd|H|T|p>>
  </equation>

  The left side of this equation is the Joule\UThomson coefficient. An
  expression for the partial derivative <math|<pd|H|p|T>> is given in Table
  <reference|tbl:7-const T>, and the partial derivative <math|<pd|H|T|p>> is
  the heat capacity at constant pressure (Eq. <reference|Cp=dH/dT>). These
  substitutions give us the desired relation

  <\equation>
    \<mu\><subs|J*T>=<frac|<around|(|\<alpha\>*T-1|)>*V|C<rsub|p>>=<frac|<around|(|\<alpha\>*T-1|)>*V<m>|<Cpm>>
  </equation>

  <I|Joule--Thomson!coefficient\|)>

  <section|Isothermal Pressure Changes><label|7-isothermal p changes>

  <I|Isothermal!pressure changes\|(><I|Pressure!changes, isothermal\|(>In
  various applications, we will need expressions for the effect of changing
  the pressure at constant temperature on the internal energy, enthalpy,
  entropy, and Gibbs energy of a phase. We obtain the expressions by
  integrating expressions found in Table <reference|tbl:7-const T>. For
  example, <math|<Del>U> is given by <math|<big|int><pd|U|p|T><difp>>. The
  results are listed in the second column of Table
  <reference|tbl:7-isothermal p change><vpageref|tbl:7-isothermal p change>.

  <\big-table>
    <minipagetable|11.6cm|<label|tbl:7-isothermal p
    change><math|<tabular*|<tformat|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|2|2|1|-1|cell-bborder|1ln>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|5|5|1|-1|cell-valign|top>|<cwith|5|5|1|-1|cell-vmode|exact>|<cwith|5|5|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|6|6|1|-1|cell-valign|top>|<cwith|6|6|1|-1|cell-vmode|exact>|<cwith|6|6|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|7|7|1|-1|cell-valign|top>|<cwith|7|7|1|-1|cell-vmode|exact>|<cwith|7|7|1|-1|cell-height|<plus|1fn|2ex>>|<cwith|7|7|1|-1|cell-bborder|1ln>|<table|<row|<cell|State
    function>|<cell|>|<cell|>|<cell|Approximate
    expression>>|<row|<cell|change>|<cell|General expression>|<cell|Ideal
    gas>|<cell|for liquid or solid>>|<row|<cell|<Strut|.*6*c*m><Del>U>|<cell|<D><big|int><rsub|p<rsub|1>><rsup|p<rsub|2>><around|(|-\<alpha\>*T+<kT>p|)>*V<difp>>|<cell|0>|<cell|-\<alpha\>*T*V<Del>p>>|<row|<cell|<Del>H>|<cell|<D><big|int><rsub|p<rsub|1>><rsup|p<rsub|2>><around|(|1-\<alpha\>*T|)>*V<difp>>|<cell|0>|<cell|<around|(|1-\<alpha\>*T|)>*V<Del>p>>|<row|<cell|<Del>A>|<cell|<D><big|int><rsub|p<rsub|1>><rsup|p<rsub|2>><kT>p*V<difp>>|<cell|<D>n*R*T*ln
    <frac|p<rsub|2>|p<rsub|1>>>|<cell|<D><kT>V*<around|(|p<rsub|2><rsup|2>-p<rsub|1><rsup|2>|)>/2>>|<row|<cell|<Del>G>|<cell|<D><big|int><rsub|p<rsub|1>><rsup|p<rsub|2>>V<difp>>|<cell|<D>n*R*T*ln
    <frac|p<rsub|2>|p<rsub|1>>>|<cell|V<Del>p>>|<row|<cell|<Del>S>|<cell|<D>-<big|int><rsub|p<rsub|1>><rsup|p<rsub|2>>\<alpha\>*V<difp>>|<cell|<D>-n*R*ln
    <frac|p<rsub|2>|p<rsub|1>>>|<cell|-\<alpha\>*V<Del>p>>>>>>>
  </big-table|Changes of state functions during an isothermal pressure change
  in a closed, single-phase system>

  <subsection|Ideal gases>

  <I|Isothermal!pressure changes!ideal gas@of an ideal
  gas\|reg><I|Pressure!changes, isothermal!ideal gas@of an ideal
  gas\|reg>Simplifications result when the phase is an ideal gas. In this
  case, we can make the substitutions <math|V=n*R*T/p>, <math|\<alpha\>=1/T>,
  and <math|<kT>=1/p>, resulting in the expressions in the third column of
  Table <reference|tbl:7-isothermal p change>.

  The expressions in the third column of Table <reference|tbl:7-isothermal p
  change> may be summarized by the statement that, when an ideal gas expands
  isothermally, the internal energy and enthalpy stay constant, the entropy
  increases, and the Helmholtz energy and Gibbs energy decrease.

  <subsection|Condensed phases>

  <I|Isothermal!pressure changes!condensed phase@of a condensed
  phase\|reg><I|Pressure!changes, isothermal!condensed phase@of a condensed
  phase\|reg>Solids, and liquids under conditions of temperature and pressure
  not close to the critical point, are much less compressible than gases.
  Typically the <I|Isothermal!compressibility!liquid or solid@of a liquid or
  solid\|reg>isothermal compressibility, <math|<kT>>, of a liquid or solid at
  room temperature and atmospheric pressure is no greater than
  <math|1<timesten|-4><units|b*a*r<per>>> (see Fig. <reference|fig:7-kappaT
  vs T><vpageref|fig:7-kappaT vs T>), whereas an ideal gas under these
  conditions has <math|<kT>=1/p=1<units|b*a*r<per>>>. Consequently, it is
  frequently valid to treat <math|V> for a liquid or solid as essentially
  constant during a pressure change at constant temperature. Because
  <math|<kT>> is small, the product <math|<kT><space|-0.17em>p> for a liquid
  or solid is usually much smaller than the product <math|\<alpha\>*T>.
  Furthermore, <math|<kT>> for liquids and solids does not change rapidly
  with <math|p> as it does for gases, and neither does <math|\<alpha\>>.

  With the approximations that <math|V>, <math|\<alpha\>>, and <math|<kT>>
  are constant during an isothermal pressure change, and that
  <math|<kT><space|-0.17em>p> is negligible compared with <math|\<alpha\>*T>,
  we obtain the expressions in the last column of Table
  <reference|tbl:7-isothermal p change>.

  <I|Isothermal!pressure changes\|)><I|Pressure!changes, isothermal\|)>

  <section|Standard States of Pure Substances><label|7-st states of pure
  substances>

  It is often useful to refer to a reference pressure, the
  <subindex|Standard|pressure><subindex|Pressure|standard><newterm|standard
  pressure>, denoted <math|p<st>>. The standard pressure has an arbitrary but
  constant value in any given application. Until 1982, chemists used a
  standard pressure of <math|1<units|a*t*m>>
  (<math|1.01325<timesten|5><Pa>>). The IUPAC now recommends the value
  <math|p<st>=1<br>> (exactly <math|10<rsup|5><Pa>>). This book uses the
  latter value unless stated otherwise. (Note that there is no defined
  standard <em|temperature>.)

  A superscript degree symbol (<with|font-size|0.71|mode|math|\<circ\>>)
  denotes a standard quantity or standard-state conditions. An alternative
  symbol for this purpose, used extensively outside the U.S., is a
  superscript <index|Plimsoll mark>Plimsoll mark
  (<with|font-size|0.71|<math|\<circ\><space|-.075in>->).
  ><with|font-size|1|<footnote|The Plimsoll mark is named after the British
  merchant Samuel Plimsoll, at whose instigation Parliament passed an act in
  1875 requiring the symbol to be placed on the hulls of cargo ships to
  indicate the maximum depth for safe loading.>>

  A <I|Standard state!pure substance@of a pure
  substance\|reg><I|State!standard\|seeStandard state><newterm|standard
  state> of a pure substance is a particular reference state appropriate for
  the kind of phase and is described by intensive variables. This book
  follows the recommendations of the <index|IUPAC Green Book>IUPAC Green
  Book<footnote|Ref. <cite|greenbook-3>, p. 61--62.> for various standard
  states.

  <\itemize>
    <item>The <I|Standard state!gas@of a gas\|reg>standard state of a
    <em|pure gas> is the hypothetical state in which the gas is at pressure
    <math|p<st>> and the temperature of interest, and the gas behaves as an
    ideal gas. The molar volume of a gas at <math|1<br>> may have a
    measurable deviation from the molar volume predicted by the ideal gas
    equation due to intermolecular forces. We must imagine the standard state
    in this case to consist of the gas with the intermolecular forces
    magically \Pturned off\Q and the molar volume adjusted to the ideal-gas
    value <math|R*T/p<st>>.

    <item>The standard state of a <I|Standard state!pure liquid or solid@of a
    pure liquid or solid\|reg><em|pure liquid or solid> is the unstressed
    liquid or solid at pressure <math|p<st>> and the temperature of interest.
    If the liquid or solid is stable under these conditions, this is a real
    (not hypothetical) state.
  </itemize>

  Section <reference|9-activities> will introduce additional standard states
  for constituents of mixtures.

  <section|Chemical Potential and Fugacity><label|7-chem pot>

  The <I|Chemical potential!pure substance@of a pure
  substance\|reg><newterm|chemical potential>, <math|\<mu\>>, of a pure
  substance has as one of its definitions (page <pageref|mu = Gm>)

  <\gather>
    <tformat|<table|<row|<cell|\<mu\><defn>G<m>=<frac|G|n><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|mu=Gm>>>>>
  </gather>

  That is, <math|\<mu\>> is equal to the <subindex|Gibbs energy|molar>molar
  Gibbs energy of the substance at a given temperature and pressure. (Section
  <reference|9-chem pot of species in a mixt> will introduce a more general
  definition of chemical potential that applies also to a constituent of a
  mixture.) The chemical potential is an intensive state function.

  The <I|Total differential!Gibbs energy of a pure substance@of the Gibbs
  energy of a pure substance\|reg>total differential of the Gibbs energy of a
  fixed amount of a pure substance in a single phase, with <math|T> and
  <math|p> as independent variables, is <math|<dif>G=-S<dif>T+V<difp>> (Eq.
  <reference|dG=-SdT+Vdp>). Dividing both sides of this equation by <math|n>
  gives the total differential of the chemical potential with these same
  independent variables:

  <\gather>
    <tformat|<table|<row|<cell|<dif>\<mu\>=-S<m><dif>T+V<m><difp><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e,<math|P=1>|)>><eq-number><label|dmu=-(Sm)dT+(Vm)dp>>>>>
  </gather>

  (Since all quantities in this equation are intensive, it is not necessary
  to specify a closed system; the amount of the substance in the system is
  irrelevant.)

  We identify the coefficients of the terms on the right side of Eq.
  <reference|dmu=-(Sm)dT+(Vm)dp> as the partial derivatives

  <\gather>
    <tformat|<table|<row|<cell|<Pd|\<mu\>|T|<space|-0.17em>p>=-S<m><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e,<math|P=1>|)>><eq-number><label|dmu/dT=-Sm>>>>>
  </gather>

  and

  <\gather>
    <tformat|<table|<row|<cell|<Pd|\<mu\>|p|T>=V<m><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e,<math|P=1>|)>><eq-number><label|dmu/dp=Vm>>>>>
  </gather>

  Since <math|V<m>> is positive, Eq. <reference|dmu/dp=Vm> shows that the
  chemical potential increases with increasing pressure in an isothermal
  process.

  The <I|Chemical potential!standard!pure substance@of a pure
  substance\|reg><I|Standard!chemical potential\|seeChemical potential,
  standard><newterm|standard chemical potential>, <math|\<mu\><st>>, of a
  pure substance in a given phase and at a given temperature is the chemical
  potential of the substance when it is in the standard state of the phase at
  this temperature and the standard pressure <math|p<st>>.

  There is no way we can evaluate the absolute value of <math|\<mu\>> at a
  given temperature and pressure, or of <math|\<mu\><st>> at the same
  temperature,<footnote|At least not to any useful degree of precision. The
  values of <math|\<mu\>> and <math|\<mu\><st>> include the molar internal
  energy whose absolute value can only be calculated from the <index|Einstein
  energy relation>Einstein relation; see Sec. <reference|2-internal energy>.>
  but we can measure or calculate the <em|difference>
  <math|\<mu\>-\<mu\><st>>. The general procedure is to integrate
  <math|<dif>\<mu\>=V<m><difp>> (Eq. <reference|dmu=-(Sm)dT+(Vm)dp> with
  <math|<dif>T> set equal to zero) from the standard state at pressure
  <math|p<st>> to the experimental state at pressure <math|p<rprime|'>>:

  <\gather>
    <tformat|<table|<row|<cell|\<mu\><around|(|p<rprime|'>|)>-\<mu\><st>=<big|int><rsub|p<st>><rsup|p<rprime|'>>V<m><difp><cond|<around|(|c*o*n*s*t*a*n*t<math|T>|)>><eq-number><label|mu-mu^o=int(Vm
    dp)>>>>>
  </gather>

  <subsection|Gases><label|7-chem pot and fugacity - gases>

  For the <I|Chemical potential!standard!gas@of a gas\|reg>standard chemical
  potential of a gas, this book will usually use the notation
  <math|\<mu\><st><gas>> to emphasize the choice of a <em|gas> standard
  state.

  An <em|ideal gas> is in its standard state at a given temperature when its
  pressure is the standard pressure. We find the relation of the chemical
  potential of an ideal gas to its pressure and its standard chemical
  potential at the same temperature by setting <math|V<m>> equal to
  <math|R*T/p> in Eq. <reference|mu-mu^o=int(Vm dp)>:
  <math|\<mu\><around|(|p<rprime|'>|)>-\<mu\><st>=<big|int><rsub|p<st>><rsup|p<rprime|'>><around|(|R*T/p|)><difp>=R*T*ln
  <around|(|p<rprime|'>/p<st>|)>>. The general relation for <math|\<mu\>> as
  a function of <math|p>, then, is

  <\gather>
    <tformat|<table|<row|<cell|\<mu\>=\<mu\><st><gas>+R*T*ln
    <frac|p|p<st>><cond|<around|(|p*u*r*e*i*d*e*a*l*g*a*s,c*o*n*s*t*a*n*t<math|T>|)>><eq-number><label|mu=muo(g)+RT*ln(p/po)>>>>>
  </gather>

  This function is shown as the dashed curve in Fig. <reference|fig:7-mu vs
  p><vpageref|fig:7-mu vs p>.

  <\big-figure>
    <boxedfigure|<image|./07-SUP/mu-p.eps||||> <capt|Chemical potential as a
    function of pressure at constant temperature, for a real gas (solid
    curve) and the same gas behaving ideally (dashed curve). Point A is the
    gas standard state. Point B is a state of the real gas at pressure
    <math|p<rprime|'>>. The fugacity <math|<fug><around|(|p<rprime|'>|)>> of
    the real gas at pressure <math|p<rprime|'>> is equal to the pressure of
    the ideal gas having the same chemical potential as the real gas (point
    C).<label|fig:7-mu vs p>>>
  </big-figure|>

  If a gas is <em|not> an ideal gas, its standard state is a hypothetical
  state. The <I|Fugacity!gas@of a gas\|reg><newterm|fugacity>, <math|<fug>>,
  of a real gas (a gas that is not necessarily an ideal gas) is defined by an
  equation with the same form as Eq. <reference|mu=muo(g)+RT*ln(p/po)>:

  <\gather>
    <tformat|<table|<row|<cell|\<mu\>=\<mu\><st><gas>+R*T*ln
    <frac|<fug>|p<st>><cond|<around|(|p*u*r*e*g*a*s|)>><eq-number><label|mu=muo(g)+RT*ln(f/po)>>>>>
  </gather>

  or

  <\gather>
    <tformat|<table|<row|<cell|<fug><defn>p<st>exp
    <around*|[|<frac|\<mu\>-\<mu\><st><gas>|R*T>|]><cond|<around|(|p*u*r*e*g*a*s|)>><eq-number>>>>>
  </gather>

  Note that fugacity has the dimensions of pressure. Fugacity is a kind of
  effective pressure. Specifically, it is the pressure that the hypothetical
  ideal gas (the gas with intermolecular forces \Pturned off\Q) would need to
  have in order for its chemical potential at the given temperature to be the
  same as the chemical potential of the real gas (see point C in Fig.
  <reference|fig:7-mu vs p>). If the gas is an ideal gas, its fugacity is
  equal to its pressure.

  To evaluate the fugacity of a real gas at a given <math|T> and <math|p>, we
  must relate the chemical potential to the pressure\Uvolume behavior. Let
  <math|\<mu\><rprime|'>> be the chemical potential and
  <math|<fug><rprime|'>> be the fugacity at the pressure <math|p<rprime|'>>
  of interest; let <math|\<mu\><rprime|''>> be the chemical potential and
  <math|<fug><rprime|''>> be the fugacity of the same gas at some low
  pressure <math|p<rprime|''>> (all at the same temperature). Then we use Eq.
  <reference|mu-mu^o=int(Vm dp)> to write
  <math|\<mu\><rprime|'>-\<mu\><st><gas>=R*T*ln
  <around|(|<fug><rprime|'>/p<st>|)>> and
  <math|\<mu\><rprime|''>-\<mu\><st><gas>=R*T*ln
  <around|(|<fug><rprime|''>/p<st>|)>>, from which we obtain

  <\equation>
    \<mu\><rprime|'>-\<mu\><rprime|''>=R*T*ln
    <frac|<fug><rprime|'>|<fug><rprime|''>>
  </equation>

  By integrating <math|<dif>\<mu\>=V<m><difp>> from pressure
  <math|p<rprime|''>> to pressure <math|p<rprime|'>>, we obtain

  <\equation>
    \<mu\><rprime|'>-\<mu\><rprime|''>=<big|int><rsub|\<mu\><rprime|''>><rsup|\<mu\><rprime|'>><dif>\<mu\>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>>V<m><difp>
  </equation>

  Equating the two expressions for <math|\<mu\><rprime|'>-\<mu\><rprime|''>>
  and dividing by <math|R*T> gives

  <\equation>
    <label|ln(f/f'')=int (Vm/RT)dp>ln <frac|<fug><rprime|'>|<fug><rprime|''>>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>><frac|V<m>|R*T><difp>
  </equation>

  In principle, we could use the integral on the right side of Eq.
  <reference|ln(f/f'')=int (Vm/RT)dp> to evaluate <math|<fug><rprime|'>> by
  choosing the lower integration limit <math|p<rprime|''>> to be such a low
  pressure that the gas behaves as an ideal gas and replacing
  <math|<fug><rprime|''>> by <math|p<rprime|''>>. However, because the
  integrand <math|V<m>/R*T> becomes very large at low pressure, the integral
  is difficult to evaluate. We avoid this difficulty by subtracting from the
  preceding equation the identity

  <\equation>
    ln <frac|p<rprime|'>|p<rprime|''>>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>><frac|<difp>|p>
  </equation>

  which is simply the result of integrating the function <math|1/p> from
  <math|p<rprime|''>> to <math|p<rprime|'>>. The result is

  <\equation>
    <label|ln(f'p''/f''p')=>ln <frac|<fug><rprime|'>p<rprime|''>|<fug><rprime|''>p<rprime|'>>=<big|int><rsub|p<rprime|''>><rsup|p<rprime|'>><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)><difp>
  </equation>

  Now we take the limit of both sides of Eq. <reference|ln(f'p''/f''p')=> as
  <math|p<rprime|''>> approaches zero. In this limit, the gas at pressure
  <math|p<rprime|''>> approaches ideal-gas behavior, <math|<fug><rprime|''>>
  approaches <math|p<rprime|''>>, and the ratio
  <math|<fug><rprime|'>p<rprime|''>/<fug><rprime|''>p<rprime|'>> approaches
  <math|<fug><rprime|'>/p<rprime|'>>:

  <\equation>
    <label|ln(f/p)=int((Vm/RT)-(1/p))dp>ln
    <frac|<fug><rprime|'>|p<rprime|'>>=<big|int><rsub|0><rsup|p<rprime|'>><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)><difp>
  </equation>

  The integrand <math|<around|(|V<m>/R*T-1/p|)>> of this integral approaches
  zero at low pressure, making it feasible to evaluate the integral from
  experimental data.

  The <I|Fugacity coefficient!gas@of a gas\|reg><newterm|fugacity
  coefficient> <math|\<phi\>> of a gas is defined by

  <\gather>
    <tformat|<table|<row|<cell|\<phi\><defn><frac|<fug>|p><space|1em><tx|o*r><space|1em><fug>=\<phi\>*p<cond|<around|(|p*u*r*e*g*a*s|)>><eq-number><label|f=phi*p>>>>>
  </gather>

  The fugacity coefficient at pressure <math|p<rprime|'>> is then given by
  Eq. <reference|ln(f/p)=int((Vm/RT)-(1/p))dp>:

  <\gather>
    <tformat|<table|<row|<cell|ln \<phi\><around|(|p<rprime|'>|)>=<big|int><rsub|0><rsup|p<rprime|'>><around*|(|<frac|V<m>|R*T>-<frac|1|p>|)><difp><cond|<around|(|p*u*r*e*g*a*s,c*o*n*s*t*a*n*t<math|T>|)>><eq-number><label|ln(phi)=int(Vm/RT-1/p)dp>>>>>
  </gather>

  The isothermal behavior of real gases at low to moderate pressures (up to
  at least <math|1<br>>) is usually adequately described by a two-term
  equation of state of the form given in Eq. <reference|Vm=RT/p+B>:

  <\equation>
    <label|Vm approx RT/p + B>V<m>\<approx\><frac|R*T|p>+B
  </equation>

  Here <math|B> is the second virial coefficient, a function of <math|T>.
  With this equation of state, Eq. <reference|ln(phi)=int(Vm/RT-1/p)dp>
  becomes

  <\equation>
    <label|ln(phi)=Bp/RT>ln \<phi\>\<approx\><frac|B*p|R*T>
  </equation>

  For a real gas at temperature <math|T> and pressure <math|p>, Eq.
  <reference|ln(phi)=int(Vm/RT-1/p)dp> or <reference|ln(phi)=Bp/RT> allows us
  to evaluate the fugacity coefficient from an experimental equation of state
  or a second virial coefficient. We can then find the fugacity from
  <math|<fug>=\<phi\>*p>.

  <\quote-env>
    As we will see in Sec. <reference|9-activities>, the dimensionless ratio
    <math|\<phi\>=<fug>/p> is an example of an <I|Activity coefficient!gas@of
    a gas\|reg><em|activity coefficient> and the dimensionless ratio
    <math|<fug>/p<st>> is an example of an <I|Activity!gas@of a
    gas\|reg><em|activity>.
  </quote-env>

  <subsection|Liquids and solids>

  The dependence of the <I|Chemical potential!liquid or solid@of a liquid or
  solid\|reg>chemical potential on pressure at constant temperature is given
  by Eq. <reference|mu-mu^o=int(Vm dp)>. With an approximation of zero
  compressibility, this becomes

  <\gather>
    <tformat|<table|<row|<cell|\<mu\>\<approx\>\<mu\><st>+V<m><around|(|p-p<st>|)><cond|(p*u*r*e*l*i*q*u*i*d*o*r*s*o*l*i*d,><nextcond|c*o*n*s*t*a*n*t<math|T>)><eq-number><label|mu=mo+Vm(p-po)>>>>>
  </gather>

  <section|Standard Molar Quantities of a Gas><label|7-st molar fncs of a
  gas>

  <I|Standard molar!quantity!gas@of a gas\|(>

  A <subindex|Standard molar|quantity><newterm|standard molar quantity> of a
  substance is the molar quantity in the standard state at the temperature of
  interest. We have seen (Sec. <reference|7-st states of pure substances>)
  that the standard state of a pure <em|liquid> or <em|solid> is a real
  state, so any standard molar quantity of a pure liquid or solid is simply
  the molar quantity evaluated at the standard pressure and the temperature
  of interest.

  The standard state of a <em|gas>, however, is a hypothetical state in which
  the gas behaves ideally at the standard pressure without influence of
  intermolecular forces. The properties of the gas in this standard state are
  those of an ideal gas. We would like to be able to relate molar properties
  of the real gas at a given temperature and pressure to the molar properties
  in the standard state at the same temperature.

  We begin by using Eq. <reference|mu=muo(g)+RT*ln(f/po)> to write an
  expression for the chemical potential of the real gas at pressure
  <math|p<rprime|'>>:

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|\<mu\><around|(|p<rprime|'>|)>>|<cell|=\<mu\><st><gas>+R*T*ln
      <frac|<fug><around|(|p<rprime|'>|)>|p<st>>>>|<row|<cell|>|<cell|=\<mu\><st><gas>+R*T*ln
      <frac|p<rprime|'>|p<st>>+R*T*ln <frac|<fug><around|(|p<rprime|'>|)>|p<rprime|'>>>>>>
    </eqsplit>
  </equation>

  We then substitute from Eq. <reference|ln(f/p)=int((Vm/RT)-(1/p))dp> to
  obtain a relation between the chemical potential, the standard chemical
  potential, and measurable properties, all at the same temperature:

  <\gather>
    <tformat|<table|<row|<cell|\<mu\><around|(|p<rprime|'>|)>=\<mu\><st><gas>+R*T*ln
    <frac|p<rprime|'>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><space|-0.17em><space|-0.17em><around*|(|V<m>-<frac|R*T|p>|)><difp><cond|<around|(|p*u*r*e*g*a*s|)>><eq-number><label|mu=muo+RT*ln(p/po)+int...>>>>>
  </gather>

  Note that this expression for <math|\<mu\>> is not what we would obtain by
  simply integrating <math|<dif>\<mu\>=V<m><difp>> from <math|p<st>> to
  <math|p<rprime|'>>, because the real gas is not necessarily in its standard
  state of ideal-gas behavior at a pressure of <math|1<br>>.

  Recall that the chemical potential <math|\<mu\>> of a pure substance is
  also its molar Gibbs energy <math|G<m>=G/n>. The standard chemical
  potential <math|\<mu\><st><gas>> of the gas is the standard molar Gibbs
  energy, <math|G<m><st><gas>>. Therefore Eq.
  <reference|mu=muo+RT*ln(p/po)+int...> can be rewritten in the form

  <\equation>
    <label|Gm=Gmo+RT*ln(p/po)+int...>G<m><around|(|p<rprime|'>|)>=G<m><st><gas>+R*T*ln
    <frac|p<rprime|'>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><space|-0.17em><space|-0.17em><around*|(|V<m>-<frac|R*T|p>|)><difp>
  </equation>

  The middle column of Table <reference|tbl:7-gas standard
  molar><vpageref|tbl:7-gas standard molar> contains an expression for
  <math|G<m><around|(|p<rprime|'>|)>-G<m><st><gas>> taken from this equation.

  <\big-table>
    <math|<with|math-display|true|<tabular*|<tformat|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|2|2|1|-1|cell-bborder|1ln>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|3ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|3ex>>|<cwith|5|5|1|-1|cell-valign|top>|<cwith|5|5|1|-1|cell-vmode|exact>|<cwith|5|5|1|-1|cell-height|<plus|1fn|3ex>>|<cwith|6|6|1|-1|cell-valign|top>|<cwith|6|6|1|-1|cell-vmode|exact>|<cwith|6|6|1|-1|cell-height|<plus|1fn|3ex>>|<cwith|7|7|1|-1|cell-valign|top>|<cwith|7|7|1|-1|cell-vmode|exact>|<cwith|7|7|1|-1|cell-height|<plus|1fn|3ex>>|<cwith|8|8|1|-1|cell-valign|top>|<cwith|8|8|1|-1|cell-vmode|exact>|<cwith|8|8|1|-1|cell-height|<plus|1fn|3ex>>|<cwith|8|8|1|-1|cell-bborder|1ln>|<table|<row|<cell|>|<cell|General
    expression>|<cell|Equation of state>>|<row|<cell|Difference>|<cell|at
    pressure p>|<cell|V=n*R*T/p+n*B>>|<row|<cell|U<m>-U<m><st><gas>>|<cell|<big|int><rsub|0><rsup|p<rprime|'>><around*|[|V<m>-T<Pd|V<m>|T|<space|-0.17em>p>|]><difp>+R*T-p<rprime|'>*V<m>>|<cell|-p*T*<frac|<dif>B|<dif>T>>>|<row|<cell|H<m>-H<m><st><gas>>|<cell|<big|int><rsub|0><rsup|p<rprime|'>><around*|[|V<m>-T<Pd|V<m>|T|<space|-0.17em>p>|]><difp>>|<cell|p*<around*|(|B-T*<frac|<dif>B|<dif>T>|)>>>|<row|<cell|A<m>-A<m><st><gas>>|<cell|R*T*ln
    <frac|p<rprime|'>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><around*|(|V<m>-<frac|R*T|p>|)><difp>+R*T-p<rprime|'>*V<m>>|<cell|R*T*ln
    <frac|p|p<st>>>>|<row|<cell|G<m>-G<m><st><gas>>|<cell|R*T*ln
    <frac|p<rprime|'>|p<st>>+<big|int><rsub|0><rsup|p<rprime|'>><around*|(|V<m>-<frac|R*T|p>|)><difp>>|<cell|R*T*ln
    <frac|p|p<st>>+B*p>>|<row|<cell|S<m>-S<m><st><gas>>|<cell|-R*ln
    <frac|p<rprime|'>|p<st>>-<big|int><rsub|0><rsup|p<rprime|'>><around*|[|<Pd|V<m>|T|<space|-0.17em>p>-<frac|R|p>|]><difp>>|<cell|-R*ln
    <frac|p|p<st>>-p*<frac|<dif>B|<dif>T>>>|<row|<cell|<Cpm>-<Cpm><st><gas>>|<cell|-<big|int><rsub|0><rsup|p<rprime|'>>T<Pd|<rsup|2>V<m>|T<rsup|2>|<space|-0.17em>p><difp>>|<cell|-p*T*<frac|<dif><rsup|2>B|<dif>T<rsup|2>>>>>>>>>
  </big-table|Real gases: expressions for differences between molar
  properties and standard molar values at the same temperature>

  This expression contains all the information needed to find a relation
  between any other molar property and its standard molar value in terms of
  measurable properties. The way this can be done is as follows.

  The relation between the chemical potential of a pure substance and its
  molar entropy is given by Eq. <reference|dmu/dT=-Sm>:

  <\equation>
    <label|Sm=-dmu/dT>S<m>=-<Pd|\<mu\>|T|<space|-0.17em>p>
  </equation>

  The <I|Entropy!standard molar!gas@of a gas\|reg>standard molar entropy of
  the gas is found from Eq. <reference|Sm=-dmu/dT> by changing <math|\<mu\>>
  to <math|\<mu\><st><gas>>:

  <\equation>
    <label|Smo=-dmuo/dT>S<m><st><gas>=-<Pd|\<mu\><st><gas>|T|<space|-0.17em>p>
  </equation>

  By substituting the expression for <math|\<mu\>> given by Eq.
  <reference|mu=muo+RT*ln(p/po)+int...> into Eq. <reference|Sm=-dmu/dT> and
  comparing the result with Eq. <reference|Smo=-dmuo/dT>, we obtain

  <\equation>
    <label|Sm=Smo-R*ln(p/po)-int...>S<m><around|(|p<rprime|'>|)>=S<m><st><gas>-R*ln
    <frac|p<rprime|'>|p<st>>-<big|int><rsub|0><rsup|p<rprime|'>><around*|[|<Pd|V<m>|T|<space|-0.17em>p>-<frac|R|p>|]><difp>
  </equation>

  The expression for <math|S<m>-S<m><st><gas>> in the middle column of Table
  <reference|tbl:7-gas standard molar> comes from this equation. The
  equation, together with a value of <math|S<m>> for a real gas obtained by
  the calorimetric method described in Sec. <reference|6-third law molar
  entropies>, can be used to evaluate <math|S<m><st><gas>>.

  Now we can use the expressions for <math|G<m>> and <math|S<m>> to find
  expressions for molar quantities such as <math|H<m>> and <math|<Cpm>>
  relative to the respective standard molar quantities. The general procedure
  for a molar quantity <math|X<m>> is to write an expression for <math|X<m>>
  as a function of <math|G<m>> and <math|S<m>> and an analogous expression
  for <math|X<m><st><gas>> as a function of <math|G<m><st><gas>> and
  <math|S<m><st><gas>>. Substitutions for <math|G<m>> and <math|S<m>> from
  Eqs. <reference|Gm=Gmo+RT*ln(p/po)+int...> and
  <reference|Sm=Smo-R*ln(p/po)-int...> are then made in the expression for
  <math|X<m>>, and the difference <math|X<m>-X<m><st><gas>> taken.

  For example, the expression for <math|U<m>-U<m><st><gas>> in the middle
  column Table <reference|tbl:7-gas standard molar> was derived as follows.
  The equation defining the Gibbs energy, <math|G=U-T*S+p*V>, was divided by
  the amount <math|n> and rearranged to

  <\equation>
    <label|Um=Gm+TSm-pVm>U<m>=G<m>+T*S<m>-p*V<m>
  </equation>

  The standard-state version of this relation is

  <\equation>
    <label|Um^o(g)=>U<m><st><gas>=G<m><st><gas>+T*S<m><st><gas>-p<st>V<m><st><gas>
  </equation>

  where from the ideal gas law <math|p<st>V<m><st><gas>> can be replaced by
  <math|R*T>. Substitutions from Eqs. <reference|Gm=Gmo+RT*ln(p/po)+int...>
  and <reference|Sm=Smo-R*ln(p/po)-int...> were made in Eq.
  <reference|Um=Gm+TSm-pVm> and the expression for <math|U<m><st><gas>> in
  Eq. <reference|Um^o(g)=> was subtracted, resulting in the expression in the
  table.

  For a real gas at low to moderate pressures, we can approximate <math|V<m>>
  by <math|<around|(|R*T/p|)>+B> where <math|B> is the second virial
  coefficient (Eq. <reference|Vm approx RT/p + B>). Equation
  <reference|mu=muo+RT*ln(p/po)+int...> then becomes

  <\equation>
    \<mu\>\<approx\>\<mu\><st><gas>+R*T*ln <frac|p|p<st>>+B*p
  </equation>

  The expressions in the last column of Table <reference|tbl:7-gas standard
  molar> use this equation of state. We can see what the expressions look
  like if the gas is ideal simply by setting <math|B> equal to zero. They
  show that when the pressure of an ideal gas increases at constant
  temperature, <math|G<m>> and <math|A<m>> increase, <math|S<m>> decreases,
  and <math|U<m>>, <math|H<m>>, and <math|<Cpm>> are unaffected.

  <I|Standard molar!quantity!gas@of a gas\|)>

  <new-page><phantomsection><addcontentsline|toc|section|Problems>
  <paragraphfootnotes><problems| <input|07-problems><page-break>>
  <plainfootnotes>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|7-JK coeff|<tuple|1.5.2|?>>
    <associate|7-chem pot|<tuple|1.8|?>>
    <associate|7-chem pot and fugacity - gases|<tuple|1.8.1|?>>
    <associate|7-heating at const V or p|<tuple|1.4|?>>
    <associate|7-ht cap measurement|<tuple|1.3.2|?>>
    <associate|7-int pressure|<tuple|1.2|?>>
    <associate|7-isothermal p changes|<tuple|1.6|?>>
    <associate|7-isothermal-jacket|<tuple|1.3.2.2|?>>
    <associate|7-partial derivatives|<tuple|1.5.1|?>>
    <associate|7-relation between Cpm and CVm|<tuple|1.3.1|?>>
    <associate|7-st molar fncs of a gas|<tuple|1.9|?>>
    <associate|7-st states of pure substances|<tuple|1.7|?>>
    <associate|C_V=,C_p=(id gas)|<tuple|1.3.3|?>>
    <associate|Chap. 7|<tuple|1|?>>
    <associate|Cpm=CVm+R|<tuple|1.3.5|?>>
    <associate|Cpm=CVm+alpha^2...|<tuple|1.3.11|?>>
    <associate|Gm=Gmo+RT*ln(p/po)+int...|<tuple|1.9.3|?>>
    <associate|Sm=-dmu/dT|<tuple|1.9.4|?>>
    <associate|Sm=Smo-R*ln(p/po)-int...|<tuple|1.9.6|?>>
    <associate|Smo=-dmuo/dT|<tuple|1.9.5|?>>
    <associate|Um=Gm+TSm-pVm|<tuple|1.9.7|?>>
    <associate|Um^o(g)=|<tuple|1.9.8|?>>
    <associate|Vm approx RT/p + B|<tuple|1.8.17|?>>
    <associate|alpha def|<tuple|1.1.1|?>>
    <associate|alpha(Vm)|<tuple|1.1.3|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|1.1.2|?>>
    <associate|auto-100|<tuple|1.7|?>>
    <associate|auto-101|<tuple|Standard|?>>
    <associate|auto-102|<tuple|Pressure|?>>
    <associate|auto-103|<tuple|standard pressure|?>>
    <associate|auto-104|<tuple|Plimsoll mark|?>>
    <associate|auto-105|<tuple|standard state|?>>
    <associate|auto-106|<tuple|IUPAC Green Book|?>>
    <associate|auto-107|<tuple|1.8|?>>
    <associate|auto-108|<tuple|chemical potential|?>>
    <associate|auto-109|<tuple|Gibbs energy|?>>
    <associate|auto-11|<tuple|1.1.2|?>>
    <associate|auto-110|<tuple|standard chemical potential|?>>
    <associate|auto-111|<tuple|Einstein energy relation|?>>
    <associate|auto-112|<tuple|1.8.1|?>>
    <associate|auto-113|<tuple|1.8.1|?>>
    <associate|auto-114|<tuple|fugacity|?>>
    <associate|auto-115|<tuple|fugacity coefficient|?>>
    <associate|auto-116|<tuple|1.8.2|?>>
    <associate|auto-117|<tuple|1.9|?>>
    <associate|auto-118|<tuple|Standard molar|?>>
    <associate|auto-119|<tuple|standard molar quantity|?>>
    <associate|auto-12|<tuple|1.2|?>>
    <associate|auto-120|<tuple|1.9.1|?>>
    <associate|auto-13|<tuple|Internal|?>>
    <associate|auto-14|<tuple|Pressure|?>>
    <associate|auto-15|<tuple|internal pressure|?>>
    <associate|auto-16|<tuple|Thermodynamic|?>>
    <associate|auto-17|<tuple|Equation of state|?>>
    <associate|auto-18|<tuple|Ideal gas|?>>
    <associate|auto-19|<tuple|Temperature|?>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-20|<tuple|Thermodynamic|?>>
    <associate|auto-21|<tuple|Pressure|?>>
    <associate|auto-22|<tuple|1.3|?>>
    <associate|auto-23|<tuple|1.3.1|?>>
    <associate|auto-24|<tuple|Pressure|?>>
    <associate|auto-25|<tuple|Internal|?>>
    <associate|auto-26|<tuple|1.3.2|?>>
    <associate|auto-27|<tuple|Electrical|?>>
    <associate|auto-28|<tuple|Work|?>>
    <associate|auto-29|<tuple|Electrical|?>>
    <associate|auto-3|<tuple|Cubic expansion coefficient|?>>
    <associate|auto-30|<tuple|Heating|?>>
    <associate|auto-31|<tuple|Calorimeter|?>>
    <associate|auto-32|<tuple|calorimeter|?>>
    <associate|auto-33|<tuple|1.3.2.1|?>>
    <associate|auto-34|<tuple|Electric|?>>
    <associate|auto-35|<tuple|Current, electric|?>>
    <associate|auto-36|<tuple|Heater circuit|?>>
    <associate|auto-37|<tuple|Circuit|?>>
    <associate|auto-38|<tuple|Electrical|?>>
    <associate|auto-39|<tuple|Work|?>>
    <associate|auto-4|<tuple|cubic expansion coefficient|?>>
    <associate|auto-40|<tuple|Electric|?>>
    <associate|auto-41|<tuple|Current, electric|?>>
    <associate|auto-42|<tuple|Electric|?>>
    <associate|auto-43|<tuple|Resistance|?>>
    <associate|auto-44|<tuple|Electrical|?>>
    <associate|auto-45|<tuple|Work|?>>
    <associate|auto-46|<tuple|Calorimeter|?>>
    <associate|auto-47|<tuple|1.3.1|?>>
    <associate|auto-48|<tuple|Heater circuit|?>>
    <associate|auto-49|<tuple|Circuit|?>>
    <associate|auto-5|<tuple|Isothermal|?>>
    <associate|auto-50|<tuple|Electrical|?>>
    <associate|auto-51|<tuple|Heating|?>>
    <associate|auto-52|<tuple|Energy equivalent|?>>
    <associate|auto-53|<tuple|energy equivalent|?>>
    <associate|auto-54|<tuple|Electrical|?>>
    <associate|auto-55|<tuple|Work|?>>
    <associate|auto-56|<tuple|Calorimeter|?>>
    <associate|auto-57|<tuple|Energy equivalent|?>>
    <associate|auto-58|<tuple|Electrical|?>>
    <associate|auto-59|<tuple|Work|?>>
    <associate|auto-6|<tuple|isothermal compressibility|?>>
    <associate|auto-60|<tuple|Energy|?>>
    <associate|auto-61|<tuple|Dissipation of energy|?>>
    <associate|auto-62|<tuple|1.3.2.2|?>>
    <associate|auto-63|<tuple|Isoperibol calorimeter|?>>
    <associate|auto-64|<tuple|Calorimeter|?>>
    <associate|auto-65|<tuple|Thermal|?>>
    <associate|auto-66|<tuple|Heater circuit|?>>
    <associate|auto-67|<tuple|Circuit|?>>
    <associate|auto-68|<tuple|1.3.2|?>>
    <associate|auto-69|<tuple|Heater circuit|?>>
    <associate|auto-7|<tuple|Coefficient of thermal expansion|?>>
    <associate|auto-70|<tuple|Circuit|?>>
    <associate|auto-71|<tuple|Convergence temperature|?>>
    <associate|auto-72|<tuple|Temperature|?>>
    <associate|auto-73|<tuple|Energy equivalent|?>>
    <associate|auto-74|<tuple|Heater circuit|?>>
    <associate|auto-75|<tuple|Circuit|?>>
    <associate|auto-76|<tuple|Energy equivalent|?>>
    <associate|auto-77|<tuple|1.3.2.3|?>>
    <associate|auto-78|<tuple|Electrical|?>>
    <associate|auto-79|<tuple|Heating|?>>
    <associate|auto-8|<tuple|Expansivity coefficient|?>>
    <associate|auto-80|<tuple|Electrical|?>>
    <associate|auto-81|<tuple|Work|?>>
    <associate|auto-82|<tuple|Electric|?>>
    <associate|auto-83|<tuple|1.3.3|?>>
    <associate|auto-84|<tuple|1.3.3|?>>
    <associate|auto-85|<tuple|1.4|?>>
    <associate|auto-86|<tuple|Molar|?>>
    <associate|auto-87|<tuple|Property|?>>
    <associate|auto-88|<tuple|Enthalpy|?>>
    <associate|auto-89|<tuple|1.5|?>>
    <associate|auto-9|<tuple|Cubic expansion coefficient|?>>
    <associate|auto-90|<tuple|1.5.1|?>>
    <associate|auto-91|<tuple|1.5.1|?>>
    <associate|auto-92|<tuple|1.5.2|?>>
    <associate|auto-93|<tuple|1.5.3|?>>
    <associate|auto-94|<tuple|Bridgman, Percy|?>>
    <associate|auto-95|<tuple|1.5.2|?>>
    <associate|auto-96|<tuple|1.6|?>>
    <associate|auto-97|<tuple|1.6.1|?>>
    <associate|auto-98|<tuple|1.6.1|?>>
    <associate|auto-99|<tuple|1.6.2|?>>
    <associate|dH=dq,C_p=dH/dT|<tuple|1.3.2|?>>
    <associate|dH=dw(el)+dw(cont)|<tuple|1.3.18|?>>
    <associate|dS=(CV/T)dT|<tuple|1.4.6|?>>
    <associate|dS=(Cp/T)dT|<tuple|1.4.11|?>>
    <associate|dU/dV=0 (id gas)|<tuple|1.2.3|?>>
    <associate|dU/dV=Tdp/dT-p|<tuple|1.2.1|?>>
    <associate|dU/dV=alpha*T/kappaT-p|<tuple|1.2.4|?>>
    <associate|dU/dt=-k(T-T(ext)+dw(el)/dt+dw(cont)/dt|<tuple|1.3.22|?>>
    <associate|dU/dt=-k(T-T(inf)+dw(el)/dt|<tuple|1.3.23|?>>
    <associate|dU=-pdV+dw(el)+dw(cont)|<tuple|1.3.12|?>>
    <associate|dU=CVdT|<tuple|1.4.1|?>>
    <associate|dU=dq,C_V=dU/dT|<tuple|1.3.1|?>>
    <associate|dU=dw(el)+dw(cont)|<tuple|1.3.13|?>>
    <associate|dV=alpha*VdT-kappaT*Vdp|<tuple|1.1.6|?>>
    <associate|delH=(Cp)delT|<tuple|1.4.10|?>>
    <associate|delH=-k int((T-T(inf))dt+w(el)|<tuple|1.3.28|?>>
    <associate|delH=int(Cp)dT|<tuple|1.4.9|?>>
    <associate|delH=w(el)+er(t2-t1)|<tuple|1.3.19|?>>
    <associate|delS=CV*ln(T2/T1)|<tuple|1.4.8|?>>
    <associate|delS=Cp*ln(T2/T1)|<tuple|1.4.13|?>>
    <associate|delS=int(CV/T)dT|<tuple|1.4.7|?>>
    <associate|delS=int(Cp/T)dT|<tuple|1.4.12|?>>
    <associate|delU=-k int((T-T(inf))dt+w(el)|<tuple|1.3.24|?>>
    <associate|delU=CV(T2-T1)|<tuple|1.4.5|?>>
    <associate|delU=int(CV)dT|<tuple|1.4.2|?>>
    <associate|delU=w(el)+er(t2-t1)|<tuple|1.3.15|?>>
    <associate|dmu/dT=-Sm|<tuple|1.8.3|?>>
    <associate|dmu/dp=Vm|<tuple|1.8.4|?>>
    <associate|dmu=-(Sm)dT+(Vm)dp|<tuple|1.8.2|?>>
    <associate|dp/dT=alpha/kappaT|<tuple|1.1.7|?>>
    <associate|dq/dt=-k(T-T(ext))|<tuple|1.3.20|?>>
    <associate|e=w(el)/(T2-T1-r(t2-t1))|<tuple|1.3.17|?>>
    <associate|e=w(el)/[(T2-T1+(k/e)int(T-T(inf))dt]|<tuple|1.3.26|?>>
    <associate|energy equiv|<tuple|energy equivalent|?>>
    <associate|f=phi*p|<tuple|1.8.15|?>>
    <associate|fig:7-Cp vs T|<tuple|1.3.3|?>>
    <associate|fig:7-ad heating curve|<tuple|1.3.1|?>>
    <associate|fig:7-alpha vs T|<tuple|1.1.2|?>>
    <associate|fig:7-heating curve|<tuple|1.3.2|?>>
    <associate|fig:7-kappaT vs T|<tuple|1.1.2|?>>
    <associate|fig:7-mu vs p|<tuple|1.8.1|?>>
    <associate|footnote-1.1.1|<tuple|1.1.1|?>>
    <associate|footnote-1.1.2|<tuple|1.1.2|?>>
    <associate|footnote-1.2.1|<tuple|1.2.1|?>>
    <associate|footnote-1.3.1|<tuple|1.3.1|?>>
    <associate|footnote-1.3.2|<tuple|1.3.2|?>>
    <associate|footnote-1.3.3|<tuple|1.3.3|?>>
    <associate|footnote-1.4.1|<tuple|1.4.1|?>>
    <associate|footnote-1.5.1|<tuple|1.5.1|?>>
    <associate|footnote-1.7.1|<tuple|1.7.1|?>>
    <associate|footnote-1.7.2|<tuple|1.7.2|?>>
    <associate|footnote-1.8.1|<tuple|1.8.1|?>>
    <associate|footnr-1.1.1|<tuple|Cubic expansion coefficient|?>>
    <associate|footnr-1.1.2|<tuple|1.1.2|?>>
    <associate|footnr-1.2.1|<tuple|Pressure|?>>
    <associate|footnr-1.3.1|<tuple|1.3.1|?>>
    <associate|footnr-1.3.2|<tuple|1.3.2|?>>
    <associate|footnr-1.3.3|<tuple|1.3.3|?>>
    <associate|footnr-1.4.1|<tuple|1.4.1|?>>
    <associate|footnr-1.5.1|<tuple|1.5.1|?>>
    <associate|footnr-1.7.1|<tuple|1.7.1|?>>
    <associate|footnr-1.7.2|<tuple|1.7.2|?>>
    <associate|footnr-1.8.1|<tuple|Einstein energy relation|?>>
    <associate|k=[(r1-r2)/(T2-T1)]e|<tuple|1.3.32|?>>
    <associate|kappaT def|<tuple|1.1.2|?>>
    <associate|kappaT(Vm)|<tuple|1.1.4|?>>
    <associate|ln(f'p''/f''p')=|<tuple|1.8.13|?>>
    <associate|ln(f/f'')=int (Vm/RT)dp|<tuple|1.8.11|?>>
    <associate|ln(f/p)=int((Vm/RT)-(1/p))dp|<tuple|1.8.14|?>>
    <associate|ln(phi)=Bp/RT|<tuple|1.8.18|?>>
    <associate|ln(phi)=int(Vm/RT-1/p)dp|<tuple|1.8.16|?>>
    <associate|mu-mu^o=int(Vm dp)|<tuple|1.8.5|?>>
    <associate|mu=Gm|<tuple|1.8.1|?>>
    <associate|mu=mo+Vm(p-po)|<tuple|1.8.19|?>>
    <associate|mu=muo(g)+RT*ln(f/po)|<tuple|1.8.7|?>>
    <associate|mu=muo(g)+RT*ln(p/po)|<tuple|1.8.6|?>>
    <associate|mu=muo+RT*ln(p/po)+int...|<tuple|1.9.2|?>>
    <associate|tbl:7-const V|<tuple|1.5.3|?>>
    <associate|tbl:7-const p|<tuple|1.5.2|?>>
    <associate|tbl:7-isothermal p change|<tuple|1.6.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      mary-96

      Salvador-03

      eisenberg-69

      grigull-90

      ICT-3

      eisenberg-69

      kell-75

      ICT-3

      bridgman-14

      Bridgman-61

      greenbook-3
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1.1.1>||>|<pageref|auto-10>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.1.2>||>|<pageref|auto-11>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.1>||>|<pageref|auto-47>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.2>||>|<pageref|auto-68>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.3>||>|<pageref|auto-84>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.8.1>||>|<pageref|auto-113>>
    </associate>
    <\associate|gly>
      <tuple|normal|cubic expansion coefficient|<pageref|auto-4>>

      <tuple|normal|isothermal compressibility|<pageref|auto-6>>

      <tuple|normal|internal pressure|<pageref|auto-15>>

      <tuple|normal|calorimeter|<pageref|auto-32>>

      <tuple|normal|energy equivalent|<pageref|auto-53>>

      <tuple|normal|standard pressure|<pageref|auto-103>>

      <tuple|normal|standard state|<pageref|auto-105>>

      <tuple|normal|chemical potential|<pageref|auto-108>>

      <tuple|normal|standard chemical potential|<pageref|auto-110>>

      <tuple|normal|fugacity|<pageref|auto-114>>

      <tuple|normal|fugacity coefficient|<pageref|auto-115>>

      <tuple|normal|standard molar quantity|<pageref|auto-119>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Cubic expansion coefficient>|<pageref|auto-3>>

      <tuple|<tuple|Isothermal|compressibility>|<pageref|auto-5>>

      <tuple|<tuple|Coefficient of thermal expansion>|<pageref|auto-7>>

      <tuple|<tuple|Expansivity coefficient>|<pageref|auto-8>>

      <tuple|<tuple|Cubic expansion coefficient|negative values
      of>|<pageref|auto-9>>

      <tuple|<tuple|Internal|pressure>|<pageref|auto-13>>

      <tuple|<tuple|Pressure|internal>|<pageref|auto-14>>

      <tuple|<tuple|Thermodynamic|equation of state>|<pageref|auto-16>>

      <tuple|<tuple|Equation of state|thermodynamic>|<pageref|auto-17>>

      <tuple|<tuple|Ideal gas|internal pressure>|<pageref|auto-18>>

      <tuple|<tuple|Temperature|thermodynamic>|<pageref|auto-19>>

      <tuple|<tuple|Thermodynamic|temperature>|<pageref|auto-20>>

      <tuple|<tuple|Pressure|negative>|<pageref|auto-21>>

      <tuple|<tuple|Pressure|internal>|<pageref|auto-24>>

      <tuple|<tuple|Internal|pressure>|<pageref|auto-25>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-27>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-28>>

      <tuple|<tuple|Electrical|heating>|<pageref|auto-29>>

      <tuple|<tuple|Heating|electrical>|<pageref|auto-30>>

      <tuple|<tuple|Calorimeter>|<pageref|auto-31>>

      <tuple|<tuple|Electric|current>|<pageref|auto-34>>

      <tuple|<tuple|Current, electric>|<pageref|auto-35>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-36>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-37>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-38>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-39>>

      <tuple|<tuple|Electric|current>|<pageref|auto-40>>

      <tuple|<tuple|Current, electric>|<pageref|auto-41>>

      <tuple|<tuple|Electric|resistance>|<pageref|auto-42>>

      <tuple|<tuple|Resistance|electric>|<pageref|auto-43>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-44>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-45>>

      <tuple|<tuple|Calorimeter|constant-volume>|<pageref|auto-46>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-48>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-49>>

      <tuple|<tuple|Electrical|heating>|<pageref|auto-50>>

      <tuple|<tuple|Heating|electrical>|<pageref|auto-51>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-52>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-54>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-55>>

      <tuple|<tuple|Calorimeter|constant-pressure>|<pageref|auto-56>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-57>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-58>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-59>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-60>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-61>>

      <tuple|<tuple|Isoperibol calorimeter>|<pageref|auto-63>>

      <tuple|<tuple|Calorimeter|isoperibol>|<pageref|auto-64>>

      <tuple|<tuple|Thermal|conductance>|<pageref|auto-65>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-66>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-67>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-69>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-70>>

      <tuple|<tuple|Convergence temperature>|<pageref|auto-71>>

      <tuple|<tuple|Temperature|convergence>|<pageref|auto-72>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-73>>

      <tuple|<tuple|Heater circuit>|<pageref|auto-74>>

      <tuple|<tuple|Circuit|heater>|<pageref|auto-75>>

      <tuple|<tuple|Energy equivalent>|<pageref|auto-76>>

      <tuple|<tuple|Electrical|heating>|<pageref|auto-78>>

      <tuple|<tuple|Heating|electrical>|<pageref|auto-79>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-80>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-81>>

      <tuple|<tuple|Electric|power>|<pageref|auto-82>>

      <tuple|<tuple|Molar|quantity>|<pageref|auto-86>>

      <tuple|<tuple|Property|molar>|<pageref|auto-87>>

      <tuple|<tuple|Enthalpy|change at constant pressure>|<pageref|auto-88>>

      <tuple|<tuple|Bridgman, Percy>|<pageref|auto-94>>

      <tuple|<tuple|Standard|pressure>|<pageref|auto-101>>

      <tuple|<tuple|Pressure|standard>|<pageref|auto-102>>

      <tuple|<tuple|Plimsoll mark>|<pageref|auto-104>>

      <tuple|<tuple|IUPAC Green Book>|<pageref|auto-106>>

      <tuple|<tuple|Gibbs energy|molar>|<pageref|auto-109>>

      <tuple|<tuple|Einstein energy relation>|<pageref|auto-111>>

      <tuple|<tuple|Standard molar|quantity>|<pageref|auto-118>>
    </associate>
    <\associate|table>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1.5.1>||<with|font-shape|<quote|italic>|Constant
      temperature>: expressions for partial derivatives of state functions
      with respect to pressure and volume in a closed, single-phase
      system>|<pageref|auto-91>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.5.2>||<with|font-shape|<quote|italic>|Constant
      pressure>: expressions for partial derivatives of state functions with
      respect to temperature and volume in a closed, single-phase
      system>|<pageref|auto-92>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.5.3>||<with|font-shape|<quote|italic>|Constant
      volume>: expressions for partial derivatives of state functions with
      respect to temperature and pressure in a closed, single-phase
      system>|<pageref|auto-93>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.6.1>||Changes of
      state functions during an isothermal pressure change in a closed,
      single-phase system>|<pageref|auto-97>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.9.1>||Real gases:
      expressions for differences between molar properties and standard molar
      values at the same temperature>|<pageref|auto-120>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Pure
      Substances in Single Phases> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Volume Properties <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      1.2<space|2spc>Internal Pressure <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>

      1.3<space|2spc>Thermal Properties <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-22>

      <with|par-left|<quote|1tab>|1.3.1<space|2spc>The relation between
      <with|mode|<quote|math>|<error|compound mathbold>> and
      <with|mode|<quote|math>|<error|compound mathbold>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-23>>

      <with|par-left|<quote|1tab>|1.3.2<space|2spc>The measurement of heat
      capacities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-26>>

      <with|par-left|<quote|2tab>|1.3.2.1<space|2spc>Adiabatic calorimeters
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-33>>

      <with|par-left|<quote|2tab>|1.3.2.2<space|2spc>Isothermal-jacket
      calorimeters <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-62>>

      <with|par-left|<quote|2tab>|1.3.2.3<space|2spc>Continuous-flow
      calorimeters <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-77>>

      <with|par-left|<quote|1tab>|1.3.3<space|2spc>Typical values
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-83>>

      1.4<space|2spc>Heating at Constant Volume or Pressure
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-85>

      1.5<space|2spc>Partial Derivatives with Respect to
      <with|mode|<quote|math>|T>, <with|mode|<quote|math>|p>, and
      <with|mode|<quote|math>|V> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-89>

      <with|par-left|<quote|1tab>|1.5.1<space|2spc>Tables of partial
      derivatives <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-90>>

      <with|par-left|<quote|1tab>|1.5.2<space|2spc>The Joule\UThomson
      coefficient <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-95>>

      1.6<space|2spc>Isothermal Pressure Changes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-96>

      <with|par-left|<quote|1tab>|1.6.1<space|2spc>Ideal gases
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-98>>

      <with|par-left|<quote|1tab>|1.6.2<space|2spc>Condensed phases
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-99>>

      1.7<space|2spc>Standard States of Pure Substances
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-100>

      1.8<space|2spc>Chemical Potential and Fugacity
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-107>

      <with|par-left|<quote|1tab>|1.8.1<space|2spc>Gases
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-112>>

      <with|par-left|<quote|1tab>|1.8.2<space|2spc>Liquids and solids
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-116>>

      1.9<space|2spc>Standard Molar Quantities of a Gas
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-117>
    </associate>
  </collection>
</auxiliary>