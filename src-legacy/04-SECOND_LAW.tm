<TeXmacs|1.99.19>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|The Second Law>

  <paragraphfootnotes><label|Chap. 4>

  The second law of thermodynamics concerns entropy and the spontaneity of
  processes. This chapter discusses theoretical aspects and practical
  applications.

  We have seen that the first law allows us to set up a balance sheet for
  energy changes during a process, but says nothing about why some processes
  occur spontaneously and others are impossible. The laws of physics explain
  some spontaneous changes. For instance, unbalanced forces on a body cause
  acceleration, and a temperature gradient at a diathermal boundary causes
  heat transfer. But how can we predict whether a phase change, a transfer of
  solute from one solution phase to another, or a chemical reaction will
  occur spontaneously under the existing conditions? The second law provides
  the principle we need to answer these and other questions\Va general
  criterion for spontaneity in a closed system.

  <section|Types of Processes><label|4-types of processes>

  Any conceivable process is either spontaneous, reversible, or impossible.
  These three possibilities were discussed in Sec. <reference|3-spont and rev
  processes> and are summarized below.

  <\itemize>
    <item>A <index|Spontaneous process><subindex|Process|spontaneous><em|spontaneous>
    process is a real process that can actually take place in a finite time
    period.

    <item>A <subindex|Reversible|process><subindex|Process|reversible><em|reversible>
    process is an imaginary, idealized process in which the system passes
    through a continuous sequence of equilibrium states. This sequence of
    states can be approached by a spontaneous process in the limit of
    infinite slowness, and so also can the reverse sequence of states.

    <item>An <subindex|Process|impossible><em|impossible><label|impossible
    process defn>process is a change that cannot occur under the existing
    conditions, even in a limiting sense. It is also known as an unnatural or
    disallowed process. Sometimes it is useful to describe a hypothetical
    impossible process that we can imagine but that does not occur in
    reality. The second law of thermodynamics will presently be introduced
    with two such impossible processes.
  </itemize>

  The <subindex|Process|spontaneous><index|Spontaneous process>spontaneous
  processes relevant to chemistry are <subindex|Process|irreversible><index|Irreversible
  process><em|irreversible>. An irreversible process is a spontaneous process
  whose reverse is an impossible process.

  There is also the special category, of little interest to chemists, of
  purely mechanical processes. A purely mechanical process is a spontaneous
  process whose reverse is also spontaneous.

  It is true that <subindex|Process|reversible><subindex|Reversible|process>reversible
  processes and purely mechanical processes are idealized processes that
  cannot occur in practice, but a spontaneous process can be <em|practically>
  reversible if carried out sufficiently slowly, or <em|practically> purely
  mechanical if friction and temperature gradients are negligible. In that
  sense, they are not impossible processes. This book will reserve the term
  \Pimpossible\Q for a process that cannot be approached by any spontaneous
  process, no matter how slowly or how carefully it is carried out.

  <section|Statements of the Second Law><label|4-second law statements>

  A description of the <subindex|Second law of thermodynamics|mathematical
  statement><newterm|mathematical statement of the second law> is given in
  the box below.<label|second law box>

  \;

  <\framed>
    \;
  </framed>

  \;

  The box includes three distinct parts. First, there is the assertion that a
  property called <index|Entropy><newterm|entropy>, <math|S>, is an extensive
  state function. Second, there is an equation for calculating the entropy
  change of a closed system during a <subindex|Reversible|process><subindex|Process|reversible>reversible
  change of state: <math|<dvar|S>> is equal to <label|T replaces
  T_b><math|<dbar|q>/T<rsub|<text|b>>>.<footnote|During a reversible process,
  the temperature usually has the same value <math|T> throughout the system,
  in which case we can simply write <math|<dif>S=<dq>/T>. The equation
  <math|<dif>S=<dq>/T<bd>> allows for the possibility that in an equilibrium
  state the system has phases of different temperatures separated by internal
  adiabatic partitions.> Third, there is a criterion for spontaneity:
  <math|<dvar|S>> is greater than <math|<dbar|q>/T<rsub|<text|b>>> during an
  irreversible change of state. The temperature <math|T<rsub|<text|b>>> is a
  thermodynamic temperature, which will be defined in Sec.
  <reference|4-thermodynamic temperature>.

  Each of the three parts is an essential component of the second law, but is
  somewhat abstract. What fundamental principle, based on experimental
  observation, may we take as the starting point to obtain them? Two
  principles are available, one associated with <index|Clausius,
  Rudolf>Clausius and the other with <index|Kelvin, Baron of Largs>Kelvin and
  <index|Planck, Max>Planck. Both principles are equivalent statements of the
  second law. Each asserts that a certain kind of process is impossible, in
  agreement with common experience.

  <I|Process!impossible\|(>

  Consider the process depicted in Fig. <reference|fig:4-impossible1>(a)
  <vpageref|fig:4-impossible1>.

  <\big-figure>
    <\boxedfigure>
      <image|./04-SUP/imposs-1.eps||||>

      <\capt>
        Two impossible processes in isolated systems.

        \ (a)<nbsp>Heat transfer from a cool to a warm body.

        \ (b)<nbsp>The same, with a device that operates in a
        cycle.<label|fig:4-impossible1>
      </capt>
    </boxedfigure>
  </big-figure|>

  The system is isolated, and consists of a cool body in thermal contact with
  a warm body. During the process, energy is transferred by means of heat
  from the cool to the warm body, causing the temperature of the cool body to
  decrease and that of the warm body to increase. Of course, this process is
  impossible; we never observe heat flow from a cooler to a warmer body. (In
  contrast, the reverse process, heat transfer from the warmer to the cooler
  body, is spontaneous and irreversible.) Note that this impossible process
  does not violate the first law, because energy is conserved.

  Suppose we attempt to bring about the same changes in the two bodies by
  interposing a device of some sort between them, as depicted in Fig.
  <reference|fig:4-impossible1>(b). Here is how we would like the device to
  operate in the isolated system: Heat should flow from the cool body to the
  device, an equal quantity of heat should flow from the device to the warm
  body, and the final state of the device should be the same as its initial
  state. In other words, we want the device to transfer energy quantitatively
  by means of heat from the cool body to the warm body while operating in a
  <em|cycle>. If the device could do this, there would be no limit to the
  quantity of energy that could be transferred by heat, because after each
  cycle the device would be ready to repeat the process. But experience shows
  that <em|it is impossible to build such a device>! The proposed process of
  Fig. <reference|fig:4-impossible1>(b) is impossible even in the limit of
  infinite slowness.

  The general principle was expressed by <index|Clausius, Rudolf>Rudolph
  Clausius<footnote|Ref. <cite|clausius-1854>, page 117.> in the words:
  \PHeat can never pass from a colder to a warmer body without some other
  change, connected therewith, occurring at the same time.\Q For use in the
  derivation to follow, the statement can be reworded as
  follows.<label|4-Clausius statement>

  <\plainlist>
    <item><label|4-Clausius> <with|font-series|bold|The Clausius statement of
    the second law:> \ <subindex|Clausius|statement of the second
    law><subindex|Second law of thermodynamics|Clausius statement>It is
    impossible to construct a device whose only effect, when it operates in a
    cycle, is heat transfer from a body to the device and the transfer by
    heat of an equal quantity of energy from the device to a warmer body.
  </plainlist>

  Next consider the impossible process shown in Fig.
  <reference|fig:4-impossible2>(a) <vpageref|fig:4-impossible2>.

  <\big-figure>
    <\boxedfigure>
      <image|./04-SUP/imposs-2.eps||||>

      <\capt>
        Two more impossible processes.

        \ (a)<nbsp>A weight rises as a liquid becomes cooler.

        \ (b)<nbsp>The same, with a heat engine.<label|fig:4-impossible2>
      </capt>
    </boxedfigure>
  </big-figure|>

  A <subindex|Joule|paddle wheel><subindex|Paddle wheel|Joule>Joule paddle
  wheel rotates in a container of water as a weight rises. As the weight
  gains potential energy, the water loses thermal energy and its temperature
  decreases. Energy is conserved, so there is no violation of the first law.
  This process is just the reverse of the Joule paddle-wheel experiment (Sec.
  <reference|3-Joule paddle wheel>) and its impossibility was discussed on
  page <pageref|reverse paddle wheel>.

  We might again attempt to use some sort of device operating in a cycle to
  accomplish the same overall process, as in Fig.
  <reference|fig:4-impossible2>(b). A closed system that operates in a cycle
  and does net work on the surroundings is called a <index|Heat
  engine><newterm|heat engine>. The heat engine shown in Fig.
  <reference|fig:4-impossible2>(b) is a special one. During one cycle, a
  quantity of energy is transferred by heat from a
  <subindex|Heat|reservoir>heat reservoir to the engine, and the engine
  performs an <em|equal> quantity of work on a weight, causing it to rise. At
  the end of the cycle, the engine has returned to its initial state. This
  would be a very desirable engine, because it could convert thermal energy
  into an equal quantity of useful mechanical work with no other effect on
  the surroundings.<footnote|This hypothetical process is called
  <index|Perpetual motion of the second kind>``perpetual motion of the second
  kind.''> The engine could power a ship; it would use the ocean as a
  <subindex|Heat|reservoir>heat reservoir and require no fuel. Unfortunately,
  <em|it is impossible to construct such a heat engine>!

  The principle was expressed by <index|Thomson, William>William Thomson
  <index|Kelvin, Baron of Largs>(Lord Kelvin) in 1852 as follows: \PIt is
  impossible by means of inanimate material agency to derive mechanical
  effect from any portion of matter by cooling it below the temperature of
  the coldest of the surrounding objects.\Q <index|Planck, Max>Max
  Planck<footnote|Ref. <cite|planck-22>, p. 89.> gave this statement: \PIt is
  impossible to construct an engine which will work in a complete cycle, and
  produce no effect except the raising of a weight and the cooling of a
  heat-reservoir.\Q For the purposes of this chapter, the principle can be
  reworded as follows.

  <\plainlist>
    <item><with|font-series|bold|The Kelvin--Planck statement of the second
    law:> \ <index|Kelvin--Planck statement of the second
    law><subindex|Second law of thermodynamics|Kelvin--Planck statement>It is
    impossible to construct a heat engine whose only effect, when it operates
    in a cycle, is heat transfer from a <subindex|Heat|reservoir>heat
    reservoir to the engine and the performance of an equal quantity of work
    on the surroundings.
  </plainlist>

  Both the Clausius statement and the Kelvin\UPlanck statement assert that
  certain processes, although they do not violate the first law, are
  nevertheless <em|impossible>.

  <\minor>
    \ These processes would not be impossible if we could control the
    trajectories of large numbers of individual particles. Newton's laws of
    motion are invariant to time reversal. Suppose we could measure the
    position and velocity of each molecule of a macroscopic system in the
    final state of an irreversible process. Then, if we could somehow arrange
    at one instant to place each molecule in the same position with its
    velocity reversed, and if the molecules behaved classically, they would
    retrace their trajectories in reverse and we would observe the reverse
    ``impossible'' process.
  </minor>

  <I|Process!impossible\|)>

  The plan of the remaining sections of this chapter is as follows. In Sec.
  <reference|4-Carnot engines>, a hypothetical device called a Carnot engine
  is introduced and used to prove that the two physical statements of the
  second law (the Clausius statement and the Kelvin\UPlanck statement) are
  equivalent, in the sense that if one is true, so is the other. An
  expression is also derived for the efficiency of a Carnot engine for the
  purpose of defining thermodynamic temperature. Section <reference|4-rev
  processes> combines Carnot cycles and the Kelvin\UPlanck statement to
  derive the existence and properties of the state function called entropy.
  Section <reference|4-irrev processes> uses irreversible processes to
  complete the derivation of the mathematical statements given in the box on
  page <pageref|second law box>, Sec. <reference|4-applications> describes
  some applications, and Sec. <reference|4-summary> is a summary. Finally,
  Sec. <reference|4-statistical> briefly describes a microscopic, statistical
  interpretation of entropy.

  <\minor>
    \ Carnot engines and Carnot cycles are admittedly outside the normal
    experience of chemists, and using them to derive the mathematical
    statement of the second law may seem arcane. <index|Lewis, Gilbert
    Newton>G. N. Lewis and <index|Randall, Merle>M. Randall, in their classic
    1923 book <em|Thermodynamics and the Free Energy of Chemical
    Substances>,<footnote|Ref. <cite|lewis-23>, p. 2.> complained of the
    presentation of `` `cyclical processes' limping about eccentric and not
    quite completed cycles.'' There seems, however, to be no way to carry out
    a rigorous <em|general> derivation without invoking thermodynamic cycles.
    You may avoid the details by skipping Secs. <reference|4-Carnot
    engines>--<reference|4-irrev processes>. (Incidently, the cycles
    described in these sections are complete!)
  </minor>

  <section|Concepts Developed with Carnot Engines><label|4-Carnot engines>

  <subsection|Carnot engines and Carnot cycles><label|4-Carnot engines and
  cycles>

  <I|Carnot!engine\|(><I|Carnot!cycle\|(>A <index|Heat engine>heat engine, as
  mentioned in Sec. <reference|4-second law statements>, is a closed system
  that converts heat to work and operates in a cycle. A
  <subindex|Carnot|engine><newterm|Carnot engine> is a particular kind of
  heat engine, one that performs <subindex|Carnot|cycle><newterm|Carnot
  cycles> with a <index|Working substance>working substance. A Carnot cycle
  has four reversible steps, alternating isothermal and adiabatic; see the
  examples in Figs. <reference|fig:4-ig Carnot engine>

  <\big-figure>
    <boxedfigure|<image|./04-sup/carnot.eps||||> <capt|Indicator diagram for
    a Carnot engine using an ideal gas as the working substance. In this
    example, <math|T<subs|h>=400<K>>, <math|T<subs|c>=300<K>>,
    <math|\<epsilon\>=1/4>, <math|<CVm>=<around|(|3/2|)>*R>,
    <math|n=2.41<units|m*m*o*l>>. The processes of paths A<math|<ra>>B and
    C<math|<ra>>D are isothermal; those of paths B<math|<ra>>C,
    B<math|<rprime|'><ra>>C<math|<rprime|'>>, and D<math|<ra>>A are
    adiabatic. The cycle A<math|<ra>>B<math|<ra>>C<math|<ra>>D<math|<ra>>A
    has net work <math|w=-1.0<units|J>>; the cycle
    A<math|<ra>>B<math|<rprime|'><ra>>C<math|<rprime|'><ra>>D<math|<ra>>A has
    net work <math|w=-0.5<units|J>>.<label|fig:4-ig Carnot engine>>>
  </big-figure|>

  and <reference|fig:4-water Carnot engine>

  <\big-figure>
    <boxedfigure|<image|./04-sup/watercyl.eps||||> <capt|Indicator diagram
    for a Carnot engine using H<rsub|<math|2>>O as the working substance. In
    this example, <math|T<subs|h>=400<K>>, <math|T<subs|c>=396<K>>,
    <math|\<epsilon\>=1/100>, <math|w=-1.0<units|J>>. In state A, the system
    consists of one mole of H<rsub|<math|2>>O(l). The processes (all carried
    out reversibly) are: A<math|<ra>>B, vaporization of
    <math|2.54<units|m*m*o*l>> H<rsub|<math|2>>O at <math|400<K>>;
    B<math|<ra>>C, adiabatic expansion, causing vaporization of an additional
    <math|7.68<units|m*m*o*l>>; C<math|<ra>>D, condensation of
    <math|2.50<units|m*m*o*l>> at <math|396<K>>; D<math|<ra>>A, adiabatic
    compression returning the system to the initial state.<label|fig:4-water
    Carnot engine>>>
  </big-figure|>

  in which the working substances are an ideal gas and H<rsub|<math|2>>O,
  respectively.

  <input|./bio/carnot>

  The steps of a Carnot cycle are as follows. In this description, the
  <em|system> is the working substance.

  <\plainlist>
    <item>Path A<math|<ra>>B: A quantity of heat <math|q<subs|h>> is
    transferred reversibly and isothermally from a
    <subindex|Heat|reservoir>heat reservoir (the ``hot'' reservoir) at
    temperature <math|T<subs|h>> to the system, also at temperature
    <math|T<subs|h>>. <math|q<subs|h>> is positive because energy is
    transferred into the system.

    <item>Path B<math|<ra>>C: The system undergoes a reversible adiabatic
    change that does work on the surroundings and reduces the system
    temperature to <math|T<subs|c>>.

    <item>Path C<math|<ra>>D: A quantity of heat <math|q<subs|c>> is
    transferred reversibly and isothermally from the system to a second
    <subindex|Heat|reservoir>heat reservoir (the ``cold'' reservoir) at
    temperature <math|T<subs|c>>. <math|q<subs|c>> is negative.

    <item>Path D<math|<ra>>A: The system undergoes a reversible adiabatic
    change in which work is done on the system, the temperature returns to
    <math|T<subs|h>>, and the system returns to its initial state to complete
    the cycle.
  </plainlist>

  In one cycle, a quantity of heat is transferred from the hot reservoir to
  the system, a portion of this energy is transferred as heat to the cold
  reservoir, and the remainder of the energy is the negative net work
  <math|w> done on the surroundings. (It is the heat transfer to the cold
  reservoir that keeps the Carnot engine from being an impossible
  Kelvin\UPlanck engine.) Adjustment of the length of path A<math|<ra>>B
  makes the magnitude of <math|w> as large or small as desired\Vnote the two
  cycles with different values of <math|w> described in the caption of Fig.
  <reference|fig:4-ig Carnot engine>.

  <\minor>
    \ The Carnot engine is an idealized heat engine because its paths are
    reversible processes. It does not resemble the design of any practical
    <index|Steam engine>steam engine. In a typical working steam engine, such
    as those once used for motive power in train locomotives and steamships,
    the cylinder contains an <em|open> system that undergoes the following
    irreversible steps in each cycle: (1) high-pressure steam enters the
    cylinder from a boiler and pushes the piston from the closed end toward
    the open end of the cylinder; (2) the supply valve closes and the steam
    expands in the cylinder until its pressure decreases to atmospheric
    pressure; (3) an exhaust valve opens to release the steam either to the
    atmosphere or to a condenser; (4) the piston returns to its initial
    position, driven either by an external force or by suction created by
    steam condensation.
  </minor>

  The energy transfers involved in one cycle of a Carnot engine are shown
  schematically in Fig. <reference|fig:4-one cycle>(a) <vpageref|fig:4-one
  cycle>.

  <\big-figure>
    <\boxedfigure>
      <image|./04-SUP/onecycle.eps||||>

      <\capt>
        (a)<nbsp>One cycle of a Carnot engine that does work on the
        surroundings.

        \ (b)<nbsp>The same system run in reverse as a Carnot heat pump.

        \ Figures <reference|fig:4-one cycle>--<reference|fig:4-impossible4>
        use the following symbols: A square box represents a system (a Carnot
        engine or Carnot heat pump). Vertical arrows indicate heat and
        horizontal arrows indicate work; each arrow shows the direction of
        energy transfer into or out of the system. The number next to each
        arrow is an absolute value of <math|q>/J or <math|w>/J in the cycle.
        For example, (a) shows <math|4> joules of heat transferred to the
        system from the hot reservoir, <math|3> joules of heat transferred
        from the system to the cold reservoir, and <math|1> joule of work
        done by the system on the surroundings.<label|fig:4-one cycle>
      </capt>
    </boxedfigure>
  </big-figure|>

  When the cycle is reversed, as shown in Fig. <reference|fig:4-one
  cycle>(b), the device is called a <subindex|Carnot|heat
  pump><newterm|Carnot heat pump>. In each cycle of a Carnot heat pump,
  <math|q<subs|h>> is negative and <math|q<subs|c>> is positive. Since each
  step of a Carnot engine or Carnot heat pump is a reversible process,
  neither device is an impossible device.
  <I|Carnot!engine\|)><I|Carnot!cycle\|)>

  <subsection|The equivalence of the Clausius and Kelvin\UPlanck statements>

  <I|Second law of thermodynamics!equivalence of Clausius and Kelvin--Planck
  statements\|(>We can use the logical tool of <em|reductio ad absurdum> to
  prove the equivalence of the Clausius and Kelvin\UPlanck statements of the
  second law.

  <input|./bio/clausius>

  Let us assume for the moment that the Clausius statement is incorrect, and
  that the device the Clausius statement claims is impossible (a \PClausius
  device\Q) is actually possible. If the Clausius device is possible, then we
  can combine one of these devices with a Carnot engine as shown in Fig.
  <reference|fig:4-impossible3>(a)<vpageref|fig:4-impossible3>. We adjust the
  cycles of the Clausius device and Carnot engine to transfer equal
  quantities of heat from and to the cold reservoir. The combination of the
  Clausius device and Carnot engine is a system. When the Clausius device and
  Carnot engine each performs one cycle, the system has performed one cycle
  as shown in Fig. <reference|fig:4-impossible3>(b). There has been a
  transfer of heat into the system and the performance of an equal quantity
  of work on the surroundings, with no other net change. This system is a
  heat engine that according to the Kelvin\UPlanck statement is impossible.

  Thus, if the Kelvin\UPlanck statement is correct, it is impossible to
  operate the Clausius device as shown, and our provisional assumption that
  the Clausius statement is incorrect must be wrong. In conclusion, if the
  Kelvin\UPlanck statement is correct, then the Clausius statement must also
  be correct.

  <\big-figure>
    <\boxedfigure>
      <image|./04-SUP/imposs-3.eps||||>

      <\capt>
        (a)<nbsp>A Clausius device combined with the Carnot engine of Fig.
        <reference|fig:4-one cycle>(a).

        \ (b)<nbsp>The resulting impossible Kelvin--Planck engine.

        \ (c)<nbsp>A Kelvin--Planck engine combined with the Carnot heat pump
        of Fig. <reference|fig:4-one cycle>(b).

        \ (d)<nbsp>The resulting impossible Clausius
        device.<label|fig:4-impossible3>
      </capt>
    </boxedfigure>
  </big-figure|>

  We can apply a similar line of reasoning to the heat engine that the
  Kelvin\UPlanck statement claims is impossible (a \PKelvin\UPlanck engine\Q)
  by seeing what happens if we assume this engine is actually possible. We
  combine a Kelvin\UPlanck engine with a Carnot heat pump, and make the work
  performed on the Carnot heat pump in one cycle equal to the work performed
  by the Kelvin\UPlanck engine in one cycle, as shown in Fig.
  <reference|fig:4-impossible3>(c). One cycle of the combined system, shown
  in Fig. <reference|fig:4-impossible3>(d), shows the system to be a device
  that the Clausius statement says is impossible. We conclude that if the
  Clausius statement is correct, then the Kelvin\UPlanck statement must also
  be correct.

  These conclusions complete the proof that the Clausius and Kelvin\UPlanck
  statements are equivalent: the truth of one implies the truth of the other.
  We may take either statement as the fundamental physical principle of the
  second law, and use it as the starting point for deriving the mathematical
  statement of the second law. The derivation will be taken up in Sec.
  <reference|4-rev processes>. <I|Second law of thermodynamics!equivalence of
  Clausius and Kelvin--Planck statements\|)>

  <subsection|The efficiency of a Carnot engine>

  Integrating the first-law equation <math|<dif>U=<dq>+<dw>> over one cycle
  of a Carnot engine, we obtain

  <\gather>
    <tformat|<table|<row|<cell|<s|0=q<subs|h>+q<subs|c>+w><cond|<around|(|o*n*e*c*y*c*l*e*o*f*a*C*a*r*n*o*t*e*n*g*i*n*e|)>><eq-number><label|0=qh+qc+w>>>>>
  </gather>

  The <I|Efficiency!heat engine@of a heat engine\|reg><newterm|efficiency>
  <math|\<epsilon\>> of a heat engine is defined as the fraction of the heat
  input <math|q<subs|h>> that is returned as net work done on the
  surroundings:

  <\equation>
    \<epsilon\><defn><frac|-w|q<subs|h>>
  </equation>

  By substituting for <math|w> from Eq. <reference|0=qh+qc+w>, we obtain

  <\gather>
    <tformat|<table|<row|<cell|<s|\<epsilon\>=1+<frac|q<subs|c>|q<subs|h>>><cond|<around|(|C*a*r*n*o*t*e*n*g*i*n*e|)>><eq-number><label|epsilon=1+qc/qh>>>>>
  </gather>

  Because <math|q<subs|c>> is negative, <math|q<subs|h>> is positive, and
  <math|q<subs|c>> is smaller in magnitude than <math|q<subs|h>>, the
  efficiency is less than one.<label|efficiency\<less\>1>The example shown in
  Fig. <reference|fig:4-one cycle>(a) is a Carnot engine with
  <math|\<epsilon\>=1/4>.

  <I|Efficiency!Carnot engine@of a Carnot engine\|(>We will be able to reach
  an important conclusion regarding efficiency by considering a Carnot engine
  operating between the temperatures <math|T<subs|h>> and <math|T<subs|c>>,
  combined with a Carnot heat pump operating between the same two
  temperatures. The combination is a supersystem, and one cycle of the engine
  and heat pump is one cycle of the supersystem. We adjust the cycles of the
  engine and heat pump to produce zero net work for one cycle of the
  supersystem.

  Could the efficiency of the Carnot engine be different from the efficiency
  the heat pump would have when run in reverse as a Carnot engine? If so,
  either the supersystem is an impossible Clausius device as shown in Fig.
  <reference|fig:4-impossible4>(b) <vpageref|fig:4-impossible4>,

  <\big-figure>
    <\boxedfigure>
      <image|./04-SUP/imposs-4.eps||||>

      <\capt>
        (a)<nbsp>A Carnot engine of efficiency <math|\<epsilon\>=1/4>
        combined with a Carnot engine of efficiency <math|\<epsilon\>=1/5>
        run in reverse.

        \ (b)<nbsp>The resulting impossible Clausius device.

        \ (c)<nbsp>A Carnot engine of efficiency <math|\<epsilon\>=1/3>
        combined with the Carnot engine of efficiency <math|\<epsilon\>=1/4>
        run in reverse.

        \ (d)<nbsp>The resulting impossible Clausius
        device.<label|fig:4-impossible4>
      </capt>
    </boxedfigure>
  </big-figure|>

  or the supersystem operated in reverse (with the engine and heat pump
  switching roles) is an impossible Clausius device as shown in Fig.
  <reference|fig:4-impossible4>(d). We conclude that <em|all Carnot engines
  operating between the same two temperatures have the same efficiency>.

  <\minor>
    \ This is a good place to pause and think about the meaning of this
    statement in light of the fact that the steps of a Carnot engine, being
    reversible changes, cannot take place in a real system (Sec.
    <reference|3-spont and rev processes>). How can an engine operate that is
    not real? The statement is an example of a common kind of thermodynamic
    shorthand. To express the same idea more accurately, one could say that
    all heat engines (real systems) operating between the same two
    temperatures have the same <em|limiting> efficiency, where the limit is
    the reversible limit approached as the steps of the cycle are carried out
    more and more slowly. You should interpret any statement involving a
    reversible process in a similar fashion: a reversible process is an
    idealized <em|limiting> process that can be approached but never quite
    reached by a real system.
  </minor>

  Thus, the efficiency of a Carnot engine must depend only on the values of
  <math|T<subs|c>> and <math|T<subs|h>> and not on the properties of the
  working substance. Since the efficiency is given by
  <math|\<epsilon\>=1+q<subs|c>/q<subs|h>>, the ratio
  <math|q<subs|c>/q<subs|h>> must be a unique function of <math|T<subs|c>>
  and <math|T<subs|h>> only. To find this function for temperatures on the
  ideal-gas temperature scale, it is simplest to choose as the working
  substance an ideal gas.

  An ideal gas has the equation of state <math|p*V=n*R*T>. Its internal
  energy change in a closed system is given by <math|<dif>U=C<rsub|V><dif>T>
  (Eq. <reference|dU=C_V dT (ig)>), where <math|C<rsub|V>> (a function only
  of <math|T>) is the heat capacity at constant volume. Reversible expansion
  work is given by <math|<dw>=-p<dif>V>, which for an ideal gas becomes
  <math|<dw>=-<around|(|n*R*T/V|)><dif>V>. Substituting these expressions for
  <math|<dif>U> and <math|<dw>> in the first law, <math|<dif>U=<dq>+<dw>>,
  and solving for <math|<dq>>, we obtain

  <\gather>
    <tformat|<table|<row|<cell|<s|<dq>=C<rsub|V><dif>T+<frac|n*R*T|V><dif>V><cond|(i*d*e*a*l*g*a*s,r*e*v*e*r*s*i*b*l*e><nextcond|e*x*p*a*n*s*i*o*n*w*o*r*k*o*n*l*y)><eq-number><label|dq=CVdT+(nRT/V)dV>>>>>
  </gather>

  Dividing both sides by <math|T> gives

  <\gather>
    <tformat|<table|<row|<cell|<s|<frac|<dq>|T>=<frac|C<rsub|V><dif>T|T>+n*R*<frac|<dif>V|V>><cond|(i*d*e*a*l*g*a*s,r*e*v*e*r*s*i*b*l*e><nextcond|e*x*p*a*n*s*i*o*n*w*o*r*k*o*n*l*y)><eq-number><label|dq/T=CVdT/T+nRdV/V>>>>>
  </gather>

  In the two adiabatic steps of the Carnot cycle, <math|<dq>> is zero. We
  obtain a relation among the volumes of the four labeled states shown in
  Fig. <reference|fig:4-ig Carnot engine> by integrating Eq.
  <reference|dq/T=CVdT/T+nRdV/V> over these steps and setting the integrals
  equal to zero:

  <\gather>
    <tformat|<table|<row|<cell|<tx|P*a*t*h*B<math|<ra>>C:><space|2em><big|int><space|-0.17em><frac|<dq>|T>=<big|int><rsub|T<subs|h>><rsup|T<subs|c>><frac|C<rsub|V><dif>T|T>+n*R*ln
    <frac|V<subs|C>|V<subs|B>>=0<eq-number>>>|<row|<cell|<tx|P*a*t*h*D<math|<ra>>A:><space|2em><big|int><space|-0.17em><frac|<dq>|T>=<big|int><rsub|T<subs|c>><rsup|T<subs|h>><frac|C<rsub|V><dif>T|T>+n*R*ln
    <frac|V<subs|A>|V<subs|D>>=0<eq-number>>>>>
  </gather>

  Adding these two equations (the integrals shown with limits cancel) gives
  the relation

  <\equation>
    n*R*ln <frac|V<subs|A>V<subs|C>|V<subs|B>V<subs|D>>=0
  </equation>

  which we can rearrange to

  <\gather>
    <tformat|<table|<row|<cell|<s|ln <around|(|V<subs|B>/V<subs|A>|)>=-ln
    <around|(|V<subs|D>/V<subs|C>|)>><cond|<around|(|i*d*e*a*l*g*a*s,C*a*r*n*o*t*c*y*c*l*e|)>><eq-number><label|ln(V2/V1)=-ln(V4/V3)>>>>>
  </gather>

  We obtain expressions for the heat in the two isothermal steps by
  integrating Eq. <reference|dq=CVdT+(nRT/V)dV> with <math|<dif>T> set equal
  to 0.

  <\gather>
    <tformat|<table|<row|<cell|<tx|P*a*t*h*A<math|<ra>>B>:<space|2em>q<subs|h>=n*R*T<subs|h>ln
    <around|(|V<subs|B>/V<subs|A>|)><eq-number>>>|<row|<cell|<tx|P*a*t*h*C<math|<ra>>D>:<space|2em>q<subs|c>=n*R*T<subs|c>ln
    <around|(|V<subs|D>/V<subs|C>|)><eq-number>>>>>
  </gather>

  The ratio of <math|q<subs|c>> and <math|q<subs|h>> obtained from these
  expressions is

  <\equation>
    <frac|q<subs|c>|q<subs|h>>=<frac|T<subs|c>|T<subs|h>>\<times\><frac|ln
    <around|(|V<subs|D>/V<subs|C>|)>|ln <around|(|V<subs|B>/V<subs|A>|)>>
  </equation>

  By means of Eq. <reference|ln(V2/V1)=-ln(V4/V3)>, this ratio becomes

  <\gather>
    <tformat|<table|<row|<cell|<s|<frac|q<subs|c>|q<subs|h>>=-<frac|T<subs|c>|T<subs|h>>><cond|<around|(|C*a*r*n*o*t*c*y*c*l*e|)>><eq-number><label|qc/qh=-Tc/Th>>>>>
  </gather>

  Accordingly, the unique function of <math|T<subs|c>> and <math|T<subs|h>>
  we seek that is equal to <math|q<subs|c>/q<subs|h>> is the ratio
  <math|-T<subs|c>/T<subs|h>>. The efficiency, from Eq.
  <reference|epsilon=1+qc/qh>, is then given by

  <\gather>
    <tformat|<table|<row|<cell|<s|\<epsilon\>=1-<frac|T<subs|c>|T<subs|h>>><cond|<around|(|C*a*r*n*o*t*e*n*g*i*n*e|)>><eq-number><label|epsilon=1-Tc/Th>>>>>
  </gather>

  In Eqs. <reference|qc/qh=-Tc/Th> and <reference|epsilon=1-Tc/Th>,
  <math|T<subs|c>> and <math|T<subs|h>> are temperatures on the ideal-gas
  scale. As we have seen, these equations must be valid for <index|Working
  substance><em|any> working substance; it is not necessary to specify as a
  condition of validity that the system is an ideal gas.

  The ratio <math|T<subs|c>/T<subs|h>> is positive but less than one, so the
  efficiency is less than one as deduced earlier on page
  <pageref|efficiency\<less\>1>. This conclusion is an illustration of the
  Kelvin\UPlanck statement of the second law: A heat engine cannot have an
  efficiency of unity\Vthat is, it cannot in one cycle convert all of the
  energy transferred by heat from a single <subindex|Heat|reservoir>heat
  reservoir into work. The example shown in Fig. <reference|fig:4-one
  cycle><vpageref|fig:4-one cycle>, with <math|\<epsilon\>=1/4>, must have
  <math|T<subs|c>/T<subs|h>=3/4> (e.g., <math|T<subs|c>=300<K>> and
  <math|T<subs|h>=400<K>>).

  Keep in mind that a Carnot engine operates <em|reversibly> between two heat
  reservoirs. The expression of Eq. <reference|epsilon=1-Tc/Th> gives the
  efficiency of this kind of idealized heat engine only. If any part of the
  cycle is carried out irreversibly, <subindex|Energy|dissipation
  of><index|Dissipation of energy>dissipation of mechanical energy will cause
  the efficiency to be <em|lower> than the theoretical value given by Eq.
  <reference|epsilon=1-Tc/Th>. <I|Efficiency!Carnot engine@of a Carnot
  engine\|)>

  <subsection|Thermodynamic temperature><label|4-thermodynamic temperature>

  <I|Thermodynamic!temperature\|(><I|Temperature!thermodynamic\|(>

  The negative ratio <math|q<subs|c>/q<subs|h>> for a Carnot cycle depends
  only on the temperatures of the two <subindex|Heat|reservoir>heat
  reservoirs. <index|Kelvin, Baron of Largs>Kelvin (1848) proposed that this
  ratio be used to establish an \Pabsolute\Q temperature scale. The physical
  quantity now called <subindex|Thermodynamic|temperature><subindex|Temperature|thermodynamic><newterm|thermodynamic
  temperature> is defined by the relation

  <\gather>
    <tformat|<table|<row|<cell|<s|<frac|T<subs|c>|T<subs|h>>=-<frac|q<subs|c>|q<subs|h>>><cond|<around|(|C*a*r*n*o*t*c*y*c*l*e|)>><eq-number><label|Tc/Th=-qc/qh>>>>>
  </gather>

  That is, the ratio of the thermodynamic temperatures of two heat reservoirs
  is equal, by definition, to the ratio of the absolute quantities of heat
  transferred in the isothermal steps of a Carnot cycle operating between
  these two temperatures. In principle, a measurement of
  <math|q<subs|c>/q<subs|h>> during a Carnot cycle, combined with a defined
  value of the thermodynamic temperature of one of the heat reservoirs, can
  establish the thermodynamic temperature of the other heat reservoir. This
  defined value is provided by the triple point of H<rsub|<math|2>>O; its
  thermodynamic temperature is defined as exactly <math|273.16> kelvins (page
  <pageref|273.16K=water triple pt>).

  Just as measurements with a gas thermometer in the limit of zero pressure
  establish the ideal-gas temperature scale (Sec. <reference|2-gas
  thermometry>), the behavior of a heat engine in the reversible limit
  establishes the thermodynamic temperature scale. Note, however, that a
  reversible Carnot engine used as a \Pthermometer\Q to measure thermodynamic
  temperature is only a theoretical concept and not a practical instrument,
  since a completely-reversible process cannot occur in practice.

  <input|./bio/kelvin>

  It is now possible to justify the statement in Sec.
  <reference|2-temperature> that the <I|Temperature!ideal
  gas@ideal-gas\|reg><index|Ideal-gas temperature>ideal-gas temperature scale
  is proportional to the thermodynamic temperature scale. Both Eq.

  <reference|qc/qh=-Tc/Th> and Eq. <reference|Tc/Th=-qc/qh> equate the ratio
  <math|T<subs|c>/T<subs|h>> to <math|-q<subs|c>/q<subs|h>>; but whereas
  <math|T<subs|c>> and <math|T<subs|h>> refer in Eq. <reference|qc/qh=-Tc/Th>
  to the <index|Ideal-gas temperature><I|Temperature!ideal
  gas@ideal-gas\|reg><em|ideal-gas> temperatures of the
  <subindex|Heat|reservoir>heat reservoirs, in Eq. <reference|Tc/Th=-qc/qh>
  they refer to the <em|thermodynamic> temperatures. This means that the
  ratio of the ideal-gas temperatures of two bodies is equal to the ratio of
  the thermodynamic temperatures of the same bodies, and therefore the two
  scales are proportional to one another. The proportionality factor is
  arbitrary, but must be unity if the same unit (e.g., kelvins) is used in
  both scales. Thus, as stated on page <pageref|identical temperature
  scales>, the two scales expressed in kelvins are identical.

  <I|Thermodynamic!temperature\|)><I|Temperature!thermodynamic\|)>

  <section|The Second Law for Reversible Processes><label|4-rev processes>

  This section derives the existence and properties of the state function
  called entropy. To begin, a useful relation called the Clausius inequality
  will be derived.

  <subsection|The Clausius inequality><label|4-Clausius inequality>

  Consider an arbitrary cyclic process of a closed system. To avoid
  confusion, this system will be the \Pexperimental system\Q and the process
  will be the \Pexperimental process\Q or \Pexperimental cycle.\Q There are
  no restrictions on the contents of the experimental system\Vit may have any
  degree of complexity whatsoever. The experimental process may involve more
  than one kind of work, phase changes and reactions may occur, there may be
  temperature and pressure gradients, constraints and external fields may be
  present, and so on. All parts of the process must be either irreversible or
  reversible, but not impossible.

  <input|./bio/planck>

  We imagine that the experimental cycle is carried out in a special way that
  allows us to apply the Kelvin\UPlanck statement of the second law. The heat
  transferred across the boundary of the experimental system in each
  infinitesimal path element of the cycle is exchanged with a hypothetical
  Carnot engine. The combination of the experimental system and the Carnot
  engine is a closed <index|Supersystem><em|supersystem> (see Fig.
  <reference|fig:4-supersystem><vpageref|fig:4-supersystem>).

  <\big-figure>
    <\boxedfigure>
      <image|./04-SUP/supersys.eps||||>

      <\capt>
        Experimental system, Carnot engine (represented by a small square
        box), and heat reservoir. The dashed lines indicate the boundary of
        the supersystem.

        \ (a)<nbsp>Reversible heat transfer between heat reservoir and Carnot
        engine.

        \ (b)<nbsp>Heat transfer between Carnot engine and experimental
        system. The infinitesimal quantities <math|<dq><rprime|'>> and
        <math|<dq>> are positive for transfer in the directions indicated by
        the arrows.<label|fig:4-supersystem>
      </capt>
    </boxedfigure>
  </big-figure|>

  In the surroundings of the supersystem is a <subindex|Heat|reservoir>heat
  reservoir of arbitrary constant temperature <math|T<subs|r*e*s>>. By
  allowing the supersystem to exchange heat with only this single heat
  reservoir, we will be able to apply the Kelvin\UPlanck statement to a cycle
  of the supersystem.<footnote|This procedure is similar to ones described in
  Ref. <cite|hats-65>, Sec. 16.1; Ref. <cite|pippard-66>, p. 36; Ref.
  <cite|pauli-73>, p. 21-23; Ref. <cite|adkins-83>, p. 68-69; and Ref.
  <cite|norton-16>.>

  We assume that we are able to control changes of the work coordinates of
  the experimental system from the surroundings of the supersystem. We are
  also able to control the Carnot engine from these surroundings, for example
  by moving the piston of a cylinder-and-piston device containing the working
  substance. Thus the energy transferred by <em|work> across the boundary of
  the experimental system, and the work required to operate the Carnot
  engine, is exchanged with the surroundings of the supersystem.

  During each stage of the experimental process with nonzero heat, we allow
  the Carnot engine to undergo many infinitesimal Carnot cycles with
  infinitesimal quantities of heat and work. In one of the isothermal steps
  of each Carnot cycle, the Carnot engine is in thermal contact with the
  <subindex|Heat|reservoir>heat reservoir, as depicted in Fig.
  <reference|fig:4-supersystem>(a). In this step the Carnot engine has the
  same temperature as the heat reservoir, and reversibly exchanges heat
  <math|<dq><rprime|'>> with it. The sign convention is that
  <math|<dq><rprime|'>> is positive if heat is transferred in the direction
  of the arrow, from the heat reservoir to the Carnot engine.

  In the other isothermal step of the Carnot cycle, the Carnot engine is in
  thermal contact with the experimental system at a portion of the system's
  boundary as depicted in Fig. <reference|fig:4-supersystem>(b). The Carnot
  engine now has the same temperature, <math|T<bd>>, as the experimental
  system at this part of the boundary, and exchanges heat with it. The heat
  <math|<dq>> is positive if the transfer is into the experimental system.

  The relation between temperatures and heats in the isothermal steps of a
  Carnot cycle is given by Eq. <reference|Tc/Th=-qc/qh>. From this relation
  we obtain, for one infinitesimal Carnot cycle, the relation
  <math|T<bd>/T<subs|r*e*s>=<dq>/<dq><rprime|'>>, or

  <\equation>
    <label|dq'=Tres(dq/Tb)><dq><rprime|'>=T<subs|r*e*s><frac|<dq>|T<bd>>
  </equation>

  After many infinitesimal Carnot cycles, the experimental cycle is complete,
  the experimental system has returned to its initial state, and the Carnot
  engine has returned to its initial state in thermal contact with the
  <subindex|Heat|reservoir>heat reservoir. Integration of Eq.
  <reference|dq'=Tres(dq/Tb)> around the experimental cycle gives the net
  heat entering the <index|Supersystem>supersystem during the process:

  <\gather>
    <tformat|<table|<row|<cell|q<rprime|'>=T<subs|r*e*s><big|oint><space|-0.17em><frac|<dq>|T<bd>><eq-number><label|q'=(Tres)int(dq/Tb)>>>>>
  </gather>

  The integration here is over each path element of the experimental process
  and over each surface element of the boundary of the experimental system.

  Keep in mind that the value of the cyclic integral
  <math|<big|oint><around|(|<dq>/T<bd>|)>> depends only on the path of the
  experimental cycle, that this process can be reversible or irreversible,
  and that <math|T<subs|r*e*s>> is a positive constant.

  In this experimental cycle, could the net heat <math|q<rprime|'>>
  transferred to the <index|Supersystem>supersystem be positive? If so, the
  net work would be negative (to make the internal energy change zero) and
  the supersystem would have converted heat from a single
  <subindex|Heat|reservoir>heat reservoir completely into work, a process the
  <index|Kelvin--Planck statement of the second law><subindex|Second law of
  thermodynamics|Kelvin--Planck statement>Kelvin\UPlanck statement of the
  second law says is impossible. Therefore it is impossible for
  <math|q<rprime|'>> to be positive, and from Eq.
  <reference|q'=(Tres)int(dq/Tb)> we obtain the relation

  <\gather>
    <tformat|<table|<row|<cell|<s|<big|oint><space|-0.17em><frac|<dq>|T<bd>>\<leq\>0><cond|<around|(|c*y*c*l*i*c*p*r*o*c*e*s*s*o*f*a*c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|oint(dq/Tb)\<less\>=0>>>>>
  </gather>

  This relation is known as the <subindex|Clausius|inequality><newterm|Clausius
  inequality>. It is valid only if the integration is taken around a cyclic
  path in a direction with nothing but reversible and irreversible
  changes\Vthe path must not include an impossible change, such as the
  reverse of an irreversible change. The Clausius inequality says that if a
  cyclic path meets this specification, it is impossible for the cyclic
  integral <math|<big|oint><around|(|<dq>/T<bd>|)>> to be positive.

  If the entire experimental cycle is adiabatic (which is only possible if
  the process is reversible), the Carnot engine is not needed and Eq.
  <reference|oint(dq/Tb)\<less\>=0> can be replaced by
  <math|<big|oint><around|(|<dq>/T<bd>|)>=0>.

  <subsection|Using reversible processes to define the entropy><label|4-rev
  processes to define entropy>

  <I|Second law of thermodynamics!mathematical statement!derivation\|(>Next
  let us investigate a <em|reversible> nonadiabatic process of the closed
  experimental system. Starting with a particular equilibrium state A, we
  carry out a reversible process in which there is a net flow of heat into
  the system, and in which <math|<dq>> is either positive or zero in each
  path element. The final state of this process is equilibrium state B. Let
  <math|<dq><rev>> denote an infinitesimal quantity of heat in a reversible
  process. If <math|<dq><rev>> is positive or zero during the process, then
  the integral <math|<big|int><rsub|<tx|A>><rsup|<tx|B>><around|(|<dq><rev>/T<bd>|)>>
  must be positive. In this case the Clausius inequality tells us that if the
  system completes a cycle by returning from state B back to state A by a
  different path, the integral <math|<big|int><rsub|<tx|B>><rsup|<tx|A>><around|(|<dq><rev>/T<bd>|)>>
  for this second path must be negative. Therefore the change B<math|<ra>>A
  cannot be carried out by any <em|adiabatic> process.

  Any reversible process can be carried out in reverse. Thus, by reversing
  the reversible nonadiabatic process, it is possible to change the state
  from B to A by a reversible process with a net flow of heat out of the
  system and with <math|<dq><rev>> either negative or zero in each element of
  the reverse path. In contrast, the absence of an adiabatic path from B to A
  means that it is impossible to carry out the change A<math|<ra>>B by a
  reversible adiabatic process.

  The general rule, then, is that whenever equilibrium state A of a closed
  system can be changed to equilibrium state B by a reversible process with
  finite \Pone-way\Q heat (i.e., the flow of heat is either entirely into the
  system or else entirely out of it), it is impossible for the system to
  change from either of these states to the other by a reversible adiabatic
  process.

  <\minor>
    \ A simple example will relate this rule to experience. We can increase
    the temperature of a liquid by allowing heat to flow reversibly into the
    liquid. It is impossible to duplicate this change of state by a
    reversible process without heat<emdash>that is, by using some kind of
    reversible work. The reason is that reversible work involves the change
    of a work coordinate that brings the system to a different final state.
    There is nothing in the rule that says we can't increase the temperature
    <em|irreversibly> without heat, as we can for instance with stirring
    work.
  </minor>

  States A and B can be arbitrarily close. We conclude that <em|every
  equilibrium state of a closed system has other equilibrium states
  infinitesimally close to it that are inaccessible by a reversible adiabatic
  process>. This is <I|Carathéodory's principle of adiabatic
  inaccessibility>Carathéodory's principle of adiabatic
  inaccessibility.<footnote|Constantin Carathéodory in 1909 combined this
  principle with a mathematical theorem (Carathéodory's theorem) to deduce
  the existence of the entropy function. The derivation outlined here avoids
  the complexities of that mathematical treatment and leads to the same
  results.>

  Next let us consider the reversible adiabatic processes that <em|are>
  possible. To carry out a reversible adiabatic process, starting at an
  initial equilibrium state, we use an adiabatic boundary and slowly vary one
  or more of the <subindex|Work|coordinate>work coordinates. A certain final
  temperature will result. It is helpful in visualizing this process to think
  of an <math|N><space|-.18em>-<space|.05em>dimensional space in which each
  axis represents one of the <math|N> <I|Independent variables!equilibrium
  state@of an equilibrium state\|reg>independent variables needed to describe
  an equilibrium state. A point in this space represents an equilibrium
  state, and the path of a reversible process can be represented as a curve
  in this space.

  A suitable set of independent variables for equilibrium states of a closed
  system of uniform temperature consists of the temperature <math|T> and each
  of the work coordinates (Sec. <reference|3-generalities>). We can vary the
  work coordinates independently while keeping the boundary adiabatic, so the
  paths for possible reversible adiabatic processes can connect any arbitrary
  combinations of work coordinate values.

  There is, however, the additional dimension of temperature in the
  <math|N><space|-.18em>-<space|.05em>dimensional space. Do the paths for
  possible reversible adiabatic processes, starting from a common initial
  point, lie in a <em|volume> in the <math|N><space|-.18em>-<space|.05em>dimensional
  space? Or do they fall on a <em|surface> described by <math|T> as a
  function of the work coordinates? If the paths lie in a volume, then every
  point in a volume element surrounding the initial point must be accessible
  from the initial point by a reversible adiabatic path. This accessibility
  is precisely what Carathéodory's principle of adiabatic inaccessibility
  denies. Therefore, the paths for all possible reversible adiabatic
  processes with a common initial state must lie on a unique <em|surface>.
  This is an <math|<around|(|N<space|-0.17em>-<space|-0.17em>1|)>>-<space|.05em>dimensional
  hypersurface in the <math|N><space|-.18em>-<space|.05em>dimensional space,
  or a curve if <math|N> is <math|2>. One of these surfaces or curves will be
  referred to as a <subindex|Reversible|adiabatic surface><newterm|reversible
  adiabatic surface>.<label|rev ad surface>

  Now consider the initial and final states of a reversible process with
  one-way heat (i.e., each nonzero infinitesimal quantity of heat
  <math|<dq><rev>> has the same sign). Since we have seen that it is
  impossible for there to be a reversible <em|adiabatic> path between these
  states, the points for these states must lie on different reversible
  adiabatic surfaces that do not intersect anywhere in the
  <math|N><space|-.18em>-<space|.05em>dimensional space. Consequently, there
  is an infinite number of nonintersecting reversible adiabatic surfaces
  filling the <math|N><space|-.18em>-<space|.05em>dimensional space. (To
  visualize this for <math|N=3>, think of a flexed stack of paper sheets;
  each sheet represents a different reversible adiabatic surface in
  three-dimensional space.) A reversible, nonadiabatic process with one-way
  heat is represented by a path beginning at a point on one reversible
  adiabatic surface and ending at a point on a different surface. If <math|q>
  is positive, the final surface lies on one side of the initial surface, and
  if <math|q> is negative, the final surface is on the opposite side.

  The existence of reversible adiabatic surfaces is the justification for
  defining a new state function <math|S>, the
  <index|Entropy><newterm|entropy>. <math|S> is specified to have the same
  value everywhere on one of these surfaces, and a different, unique value on
  each different surface. In other words, the reversible adiabatic surfaces
  are surfaces of <em|constant entropy> in the
  <math|N><space|-.18em>-<space|.05em>dimensional space. The fact that the
  surfaces fill this space without intersecting ensures that <math|S> is a
  state function for equilibrium states, because any point in this space
  represents an equilibrium state and also lies on a single reversible
  adiabatic surface with a definite value of <math|S>.

  We know the entropy function must exist, because the reversible adiabatic
  surfaces exist. For instance, Fig. <reference|fig:4-VT
  adiabats><vpageref|fig:4-VT adiabats>

  <\big-figure>
    <boxedfigure|<image|./04-SUP/adiabats.eps||||> <capt|A family of
    reversible adiabatic curves (two-dimensional reversible adiabatic
    surfaces) for an ideal gas with <math|V> and <math|T> as independent
    variables. A reversible adiabatic process moves the state of the system
    along a curve, whereas a reversible process with positive heat moves the
    state from one curve to another above and to the right. The curves are
    calculated for <math|n=1<mol>> and <math|<CVm>=<around|(|3/2|)>*R>.
    Adjacent curves differ in entropy by <math|1<units|J*<space|0.17em>K<per>>>.<label|fig:4-VT
    adiabats>>>
  </big-figure|>

  shows a family of these surfaces for a closed system of a pure substance in
  a single phase. In this system, <math|N> is equal to 2, and the surfaces
  are two-dimensional curves. Each curve is a contour of constant <math|S>.
  At this stage in the derivation, our assignment of values of <math|S> to
  the different curves is entirely arbitrary.

  How can we assign a unique value of <math|S> to each reversible adiabatic
  surface? We can order the values by letting a reversible process with
  <em|positive> one-way heat, which moves the point for the state to a new
  surface, correspond to an <em|increase> in the value of <math|S>. Negative
  one-way heat will then correspond to decreasing <math|S>. We can assign an
  arbitrary value to the entropy on one particular reversible adiabatic
  surface. <index|Third law of thermodynamics>(The third law of
  thermodynamics is used for this purpose\Vsee Sec. <reference|6-entropy
  zero>.) Then all that is needed to assign a value of <math|S> to each
  equilibrium state is a formula for evaluating the <em|difference> in the
  entropies of any two surfaces.

  Consider a reversible process with <em|positive> one-way heat that changes
  the system from state A to state B. The path for this process must move the
  system from a reversible adiabatic surface of a certain entropy to a
  different surface of greater entropy. An example is the path A<math|<ra>>B
  in Fig. <reference|fig:4-rev paths>(a)<vpageref|fig:4-rev paths>.

  <\big-figure>
    <\boxedfigure>
      <image|./04-SUP/rev_paths.eps||||>

      <\capt>
        Reversible paths in <math|V>--<math|T> space. The thin curves are
        reversible adiabatic surfaces.

        \ (a)<nbsp>Two paths connecting the same pair of reversible adiabatic
        surfaces.

        \ (b)<nbsp>A cyclic path.<label|fig:4-rev paths>
      </capt>
    </boxedfigure>
  </big-figure|>

  (The adiabatic surfaces in this figure are actually two-dimensional
  curves.) As before, we combine the experimental system with a Carnot engine
  to form a <index|Supersystem>supersystem that exchanges heat with a single
  <subindex|Heat|reservoir>heat reservoir of constant temperature
  <math|T<subs|r*e*s>>. The net heat entering the supersystem, found by
  integrating Eq. <reference|dq'=Tres(dq/Tb)>, is

  <\equation>
    <label|q(ss)=int(AB)>q<rprime|'>=T<subs|r*e*s><big|int><rsub|<tx|A>><rsup|<tx|B>><frac|<dq><rev>|T<bd>>
  </equation>

  and it is positive.

  Suppose the same experimental system undergoes a second reversible process,
  not necessarily with one-way heat, along a different path connecting the
  same pair of reversible adiabatic surfaces. This could be path
  C<math|<ra>>D in Fig. <reference|fig:4-rev paths>(a). The net heat entering
  the supersystem during this second process is <math|q<rprime|''>>:

  <\equation>
    <label|q'(ss)=int(CD)>q<rprime|''>=T<subs|r*e*s><big|int><rsub|<tx|C>><rsup|<tx|D>><frac|<dq><rev>|T<bd>>
  </equation>

  We can then devise a <em|cycle> of the <index|Supersystem>supersystem in
  which the experimental system undergoes the reversible path
  A<math|<ra>>B<math|<ra>>D<math|<ra>>C<math|<ra>>A, as shown in Fig.
  <reference|fig:4-rev paths>(b). Step A<math|<ra>>B is the first process
  described above, step D<math|<ra>>C is the reverse of the second process
  described above, and steps B<math|<ra>>D and C<math|<ra>>A are reversible
  and adiabatic. The net heat entering the supersystem in the cycle is
  <math|q<rprime|'>-q<rprime|''>>. In the reverse cycle the net heat is
  <math|q<rprime|''>-q<rprime|'>>. In both of these cycles the heat is
  exchanged with a single <subindex|Heat|reservoir>heat reservoir; therefore,
  according to the <subindex|Second law of thermodynamics|Kelvin--Planck
  statement>Kelvin\UPlanck statement, neither cycle can have positive net
  heat. Therefore <math|q<rprime|'>> and <math|q<rprime|''>> must be equal,
  and Eqs. <reference|q(ss)=int(AB)> and <reference|q'(ss)=int(CD)> then show
  the integral <math|<big|int><around|(|<dq><rev>/T<bd>|)>> has the same
  value when evaluated along either of the reversible paths from the lower to
  the higher entropy surface.

  Note that since the second path (C<math|<ra>>D) does not necessarily have
  one-way heat, it can take the experimental system through any sequence of
  intermediate entropy values, provided it starts at the lower entropy
  surface and ends at the higher. Furthermore, since the path is reversible,
  it can be carried out in reverse resulting in reversal of the signs of
  <math|<Del>S> and <math|<big|int><around|(|<dq><rev>/T<bd>|)>>.

  It should now be apparent that a satisfactory formula for defining the
  entropy change of a reversible process in a closed system is

  <\gather>
    <tformat|<table|<row|<cell|<s|<Del>S=<big|int><space|-0.17em><frac|<dq><rev>|T<bd>>><cond|(r*e*v*e*r*s*i*b*l*e*p*r*o*c*e*s*s,><nextcond|c*l*o*s*e*d*s*y*s*t*e*m)><eq-number><label|delS=int(dq/Tb)>>>>>
  </gather>

  This formula satisfies the necessary requirements: it makes the value of
  <math|<Del>S> positive if the process has positive one-way heat, negative
  if the process has negative one-way heat, and zero if the process is
  adiabatic. It gives the same value of <math|<Del>S> for any reversible
  change between the same two reversible adiabatic surfaces, and it makes the
  sum of the <math|<Del>S> values of several consecutive reversible processes
  equal to <math|<Del>S> for the overall process.

  In Eq. <reference|delS=int(dq/Tb)>, <math|<Del>S> is the entropy change
  when the system changes from one arbitrary equilibrium state to another. If
  the change is an infinitesimal path element of a reversible process, the
  equation becomes

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>S=<frac|<dq><rev>|T<bd>>><cond|(r*e*v*e*r*s*i*b*l*e*p*r*o*c*e*s*s,><nextcond|c*l*o*s*e*d*s*y*s*t*e*m)><eq-number><label|dS=dq/Tb>>>>>
  </gather>

  <\minor>
    \ In Eq. <reference|dS=dq/Tb>, the quantity <math|1/T<bd>> is called an
    <index|Integrating factor><em|integrating factor> for <math|<dq><rev>>, a
    factor that makes the product <math|<around|(|1/T<bd>|)><dq><rev>> be an
    exact differential and the infinitesimal change of a state function. The
    quantity <math|c/T<bd>>, where <math|c> is any nonzero constant, would
    also be a satisfactory integrating factor; so the definition of entropy,
    using <math|c=1>, is actually one of an infinite number of possible
    choices for assigning values to the reversible adiabatic surfaces.
  </minor>

  <subsection|Alternative derivation of entropy as a state
  function><label|4-alternative derivation>

  The Clausius inequality <math|<big|oint><around|(|<dq>/T<bd>|)>\<leq\>0>
  (Eq. <reference|oint(dq/Tb)\<less\>=0>) can be used to show, by a more
  direct route than in the preceding section, that
  <math|<around|(|<dq><rev>/T<bd>|)>> is an exact differential during a
  reversible process of a closed system. When we equate <math|<dif>S> to this
  differential, as in Eq. <reference|dS=dq/Tb>, the entropy <math|S> can be
  shown to be a state function.

  The proof uses the fact that when a reversible process is reversed and the
  system passes through the same continuous sequence of equilibrium states in
  reverse order, the heat <math|<dq><rev>> in each infinitesimal step changes
  its sign but not its magnitude (Sec. <reference|3-reversible processes>).
  As a result, the integral <math|<big|int><around|(|<dq><rev>/T<bd>|)>>
  changes its sign but not its magnitude when the process is reversed.

  Consider an arbitrary reversible cyclic process of a closed system. Could
  the cyclic integral <math|<big|oint><around|(|<dq><rev>/T<bd>|)>> for this
  process be <em|positive>? No, that is impossible according to the Clausius
  inequality. Could the cyclic integral be <em|negative>? No, because in this
  case <math|<big|oint><around|(|<dq><rev>/T<bd>|)>> for the reverse cycle is
  positive, which is also impossible. Thus the value of the cyclic integral
  for a reversible cyclic process must be <em|zero>:

  <\gather>
    <tformat|<table|<row|<cell|<s|<big|oint><space|-0.17em><frac|<dq><rev>|T<bd>>=0><cond|(r*e*v*e*r*s*i*b*l*e*c*y*c*l*i*c*p*r*o*c*e*s*s><nextcond|o*f*a*c*l*o*s*e*d*s*y*s*t*e*m)><eq-number><label|oint(dq(rev)/Tb)=0>>>>>
  </gather>

  Let A and B be any two equilibrium states. Let path 1 and path 2 be two
  arbitrary but different reversible paths starting at state A and ending at
  state B, and let path 3 be the path from state B to state A that is the
  reverse of path 2. When the system changes from state A to state B along
  path 1, and then changes back to state A along path 3, it has undergone a
  reversible cyclic process. From Eq. <reference|oint(dq(rev)/Tb)=0>, the sum
  of the integrals of <math|<around|(|<dq><rev>/T<bd>|)>> along paths 1 and 3
  is zero. The integral of <math|<around|(|<dq><rev>/T<bd>|)>> along path 3
  has the same magnitude and opposite sign of the integral of
  <math|<around|(|<dq><rev>/T<bd>|)>> along path 2. Therefore the integral
  <math|<big|int><rsub|<tx>>A<rsup|<tx>>*B*<around|(|<dq><rev>/T<bd>|)>> must
  have the same value along paths 1 and 2. The result would be the same for a
  reversible cycle using any other two paths from state A to state B. We
  conclude that the value of <math|<around|(|<dq><rev>/T<bd>|)>> integrated
  over a reversible path between any two equilibrium states depends only on
  the initial and final states and not on the path; that is,
  <math|<around|(|<dq><rev>/T<bd>|)>> is an exact differential as defined on
  page <pageref|exact differential>.

  When we equate <math|<dif>S> to <math|<around|(|<dq><rev>/T<bd>|)>>, the
  entropy change along a reversible path from any initial equilibrium state A
  to any final equilibrium state B is given by

  <\equation>
    <label|delS=int(dS)=int(dq/Tb)><Del>S<subs|A<math|<ra>>B>=S<rsub|<tx>>*B-S<rsub|<tx>>*A=<big|int><rsub|<tx>>A<rsup|<tx>>*B<space|-0.17em><space|-0.17em><dif>S=<big|int><rsub|<tx>>A<rsup|<tx>>*B<frac|<dq><rev>|T<bd>>
  </equation>

  Since the value of <math|<big|int><rsub|<tx>>A<rsup|<tx>>*B*<around|(|<dq><rev>/T<bd>|)>>
  depends only on the initial and final states A and B, so also does the
  value of <math|<Del>S<subs|A<math|<ra>>B>>. If a value of <math|S> is
  assigned to a reference state, Eq. <reference|delS=int(dS)=int(dq/Tb)> in
  principle allows the value of <math|S> to be evaluated for any other
  equilibrium state of the system. Each value of <math|S> then depends only
  on the state and not on the past or future history of the system.
  Therefore, by the definition in Sec. <reference|2-state fncs \ ind
  variables><vpageref|<tformat|<table|<row|<cell|2-state fncs>|<cell|ind
  variables>>>>>, the entropy is a state function. <I|Second law of
  thermodynamics!mathematical statement!derivation\|)>

  <subsection|Some properties of the entropy>

  <plainfootnotes><label|4-properties of entropy>

  It is not difficult to show that the entropy of a closed system in an
  equilibrium state is an <I|Entropy!extensive@an extensive
  property\|reg><em|extensive> property. Suppose a system of uniform
  temperature <math|T> is divided into two closed subsystems A and B. When a
  reversible infinitesimal change occurs, the entropy changes of the
  subsystems are <math|<dif>S<subs|A>=<dq><subs|A>/T> and
  <math|<dif>S<subs|B>=<dq><subs|B>/T> and of the system
  <math|<dif>S=<dq><rev>/T>. But <math|<dq><rev>> is the sum of
  <math|<dq><subs|A>> and <math|<dq><subs|B>>, which gives
  <math|<dif>S=<dif>S<subs|A>+<dif>S<subs|B>>. Thus, the entropy changes are
  additive, so that entropy must be extensive:
  <math|S>=S<subs|A>+S<subs|B>.<footnote|The argument is not quite complete,
  because we have not shown that when each subsystem has an entropy of zero,
  so does the entire system. The zero of entropy will be discussed in Sec.
  <reference|6-entropy zero>.>

  How can we evaluate the entropy of a particular equilibrium state of the
  system? We must assign an arbitrary value to one state and then evaluate
  the entropy change along a reversible path from this state to the state of
  interest using <math|<Del>S=<big|int><around|(|<dq><rev>/T<bd>|)>>.

  We may need to evaluate the entropy of a <I|Entropy!nonequilibrium@of a
  nonequilibrium state\|reg><em|non>equilibrium state. To do this, we imagine
  imposing hypothetical internal constraints that change the nonequilibrium
  state to a constrained equilibrium state with the same internal structure.
  Some examples of such internal constraints were given in Sec.
  <reference|2-eq states>, and include rigid adiabatic partitions between
  phases of different temperature and pressure, semipermeable membranes to
  prevent transfer of certain species between adjacent phases, and inhibitors
  to prevent chemical reactions.

  We assume that we can, in principle, impose or remove such constraints
  reversibly without heat, so there is no entropy change. If the
  nonequilibrium state includes macroscopic internal motion, the imposition
  of internal constraints involves negative reversible work to bring moving
  regions of the system to rest.<footnote|This concept amounts to defining
  the entropy of a state with macroscopic internal motion to be the same as
  the entropy of a state with the same internal structure but without the
  motion, i.e., the same state frozen in time. By this definition,
  <math|<Del>S> for a purely mechanical process (Sec. <reference|3-purely
  mechanical processes>) is zero.> If the system is nonuniform over its
  extent, the internal constraints will partition it into practically-uniform
  regions whose entropy is additive. The entropy of the nonequilibrium state
  is then found from <math|<Del>S=<big|int><around|(|<dq><rev>/T<bd>|)>>
  using a reversible path that changes the system from an equilibrium state
  of known entropy to the constrained equilibrium state with the same entropy
  as the state of interest. This procedure allows every possible state (at
  least conceptually) to have a definite value of <math|S>.

  <section|The Second Law for Irreversible Processes>

  <paragraphfootnotes><label|4-irrev processes>

  <I|Irreversible process\|(><I|Process!irreversible\|(>

  We know that during a reversible process of a closed system, each
  infinitesimal entropy change <math|<dif>S> is equal to <math|<dq>/T<bd>>
  and the finite change <math|<Del>S> is equal to the integral
  <math|<big|int><around|(|<dq>/T<bd>|)>>\Vbut what can we say about
  <math|<dif>S> and <math|<Del>S> for an <em|irreversible> process?

  The derivation of this section will show that for an infinitesimal
  irreversible change of a closed system, <math|<dif>S> is greater than
  <math|<dq>/T<bd>>, and for an entire irreversible process <math|<Del>S> is
  greater than <math|<big|int><around|(|<dq>/T<bd>|)>>. That is, the
  <em|equalities> that apply to a reversible process are replaced, for an
  irreversible process, by <em|inequalities>.

  The derivation begins with irreversible processes that are adiabatic, and
  is then extended to irreversible processes in general.

  <subsection|Irreversible adiabatic processes><label|4-irrev ad processes>

  Consider an arbitrary irreversible adiabatic process of a closed system
  starting with a particular initial state A. The final state B depends on
  the path of this process. We wish to investigate the sign of the entropy
  change <math|<Del>S<subs|A<math|<ra>>B>>. Our reasoning will depend on
  whether or not there is work during the process.

  If there is work along any infinitesimal path element of the irreversible
  adiabatic process (<math|<dw>\<ne\>0>), we know from experience that this
  work would be different if the work coordinate or coordinates were changing
  at a different rate, because <subindex|Energy|dissipation
  of><index|Dissipation of energy>energy dissipation from internal friction
  would then be different. In the limit of infinite slowness, an adiabatic
  process with initial state A and the same change of work coordinates would
  become reversible, and the net work and final internal energy would differ
  from those of the irreversible process. Because the final state of the
  reversible adiabatic process is different from B, there is no reversible
  adiabatic path with work between states A and B.

  <\minor>
    \ All states of a reversible process, including the initial and final
    states, must be equilibrium states. There is therefore a conceptual
    difficulty in considering reversible paths between two states if either
    of these states are nonequilibrium states. In such a case we will assume
    that the state has been replaced by a constrained equilibrium state of
    the same entropy, as described in Sec. <reference|4-properties of
    entropy>.
  </minor>

  If, on the other hand, there is no work along any infinitesimal path
  element of the irreversible adiabatic process (<math|<dw|=>0>), the process
  is taking place at constant internal energy <math|U> in an <em|isolated>
  system. A reversible limit cannot be reached without heat or work (page
  <pageref|no rev limit in isolated system>). Thus any reversible adiabatic
  change from state A would require work, causing a change of <math|U> and
  preventing the system from reaching state B by any reversible adiabatic
  path.

  So regardless of whether or not an irreversible adiabatic process
  A<math|<ra>>B involves work, there is no <em|reversible> adiabatic path
  between A and B. The only reversible paths between these states must be
  <em|non>adiabatic. It follows that the entropy change
  <math|<Del>S<subs|A<math|<ra>>B>>, given by the value of
  <math|<dq><rev>/T<bd>> integrated over a reversible path from A to B,
  cannot be zero.

  Next we ask whether <math|<Del>S<subs|A<math|<ra>>B>> could be negative. In
  each infinitesimal path element of the irreversible adiabatic process
  A<math|<ra>>B, <math|<dq>> is zero and the integral
  <math|<big|int><rsub|<tx|A>><rsup|<tx|B>><around|(|<dq>/T<bd>|)>> along the
  path of this process is zero. Suppose the system completes a cycle by
  returning along a different, reversible path from state B back to state A.
  The Clausius inequality (Eq. <reference|oint(dq/Tb)\<less\>=0>) tells us
  that in this case the integral <math|<big|int><rsub|<tx|B>><rsup|<tx|A>><around|(|<dq><rev>/T<bd>|)>>
  along the reversible path cannot be positive. But this integral for the
  reversible path is equal to <math|-<Del>S<subs|A<math|<ra>>B>>, so
  <math|<Del>S<subs|A<math|<ra>>B>> cannot be negative.

  We conclude that because the entropy change of the irreversible adiabatic
  process A<math|<ra>>B cannot be zero, and it cannot be negative, it must be
  <em|positive>.

  In this derivation, the initial state A is arbitrary and the final state B
  is reached by an irreversible adiabatic process. If the two states are only
  infinitesimally different, then the change is infinitesimal. Thus for an
  infinitesimal change that is irreversible and adiabatic, <math|<dif>S> must
  be <em|positive>.

  <subsection|Irreversible processes in general><label|4-irrev processes in
  general>

  To treat an irreversible process of a closed system that is nonadiabatic,
  we proceed as follows. As in Sec. <reference|4-Clausius inequality>, we use
  a Carnot engine for heat transfer across the boundary of the experimental
  system. We move the boundary of the <index|Supersystem>supersystem of Fig.
  <reference|fig:4-supersystem> so that the supersystem now includes the
  experimental system, the Carnot engine, and a <subindex|Heat|reservoir>heat
  reservoir of constant temperature <math|T<subs|r*e*s>>, as depicted in Fig.
  <reference|fig:4-adiabatic_supersystem><vpageref|fig:4-adiabatic<rsub|s>upersystem>.

  <\big-figure>
    <boxedfigure|<image|./04-SUP/SS-2.eps||||> <capt|Supersystem including
    the experimental system, a Carnot engine (square box), and a heat
    reservoir. The dashed rectangle indicates the boundary of the
    supersystem.<label|fig:4-adiabatic_supersystem>>>
  </big-figure|>

  During an irreversible change of the experimental system, the Carnot engine
  undergoes many infinitesimal cycles. During each cycle, the Carnot engine
  exchanges heat <math|<dq><rprime|'>> at temperature <math|T<subs|r*e*s>>
  with the heat reservoir and heat <math|<dq>> at temperature <math|T<bd>>
  with the experimental system, as indicated in the figure. We use the sign
  convention that <math|<dq><rprime|'>> is positive if heat is transferred to
  the Carnot engine, and <math|<dq>> is positive if heat is transferred to
  the experimental system, in the directions of the arrows in the figure.

  The <index|Supersystem>supersystem exchanges work, but not heat, with its
  surroundings. (The work involves the Carnot engine, but not necessarily the
  experimental system.) During one infinitesimal cycle of the Carnot engine,
  the net entropy change of the Carnot engine is zero, the entropy change of
  the experimental system is <math|<dif>S>, the heat transferred between the
  Carnot engine and the experimental system is <math|<dq>>, and the heat
  transferred between the heat reservoir and the Carnot engine is given by
  <math|<dq><rprime|'>=T<subs|r*e*s><dq>/T<bd>> (Eq.
  <reference|dq'=Tres(dq/Tb)>). The heat transfer between the heat reservoir
  and Carnot engine is reversible, so the entropy change of the heat
  reservoir is

  <\equation>
    <label|dS(res)=><dif>S<subs|r*e*s>=-<frac|<dq><rprime|'>|T<subs|r*e*s>>=-<frac|<dq>|T<bd>>
  </equation>

  The entropy change of the supersystem is the sum of the entropy changes of
  its parts:

  <\equation>
    <label|dS(ss)=sum><dif>S<subs|s*s>=<dif>S+<dif>S<subs|r*e*s>=<dif>S-<frac|<dq>|T<bd>>
  </equation>

  The process within the <index|Supersystem>supersystem is adiabatic and
  includes an irreversible change within the experimental system, so
  according to the conclusions of Sec. <reference|4-irrev ad processes>,
  <math|<dif>S<subs|s*s>> is positive. Equation <reference|dS(ss)=sum> then
  shows that <math|<dif>S>, the infinitesimal entropy change during the
  irreversible change of the experimental system, must be greater than
  <math|<dq>/T<bd>>:

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>S\<gtr\><frac|<dq>|T<bd>>><cond|<around|(|i*r*r*e*v*e*r*s*i*b*l*e*c*h*a*n*g*e,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|dS\<gtr\>int(dq/Tb)>>>>>
  </gather>

  This relation includes the case of an irreversible <em|adiabatic> change,
  because it shows that if <math|<dq>> is zero, <math|<dif>S> is greater than
  zero.

  By integrating both sides of Eq. <reference|dS\<gtr\>int(dq/Tb)> between
  the initial and final states of the irreversible process, we obtain a
  relation for the finite entropy change corresponding to many infinitesimal
  cycles of the Carnot engine:

  <\gather>
    <tformat|<table|<row|<cell|<s|<Del>S\<gtr\><big|int><space|-0.17em><frac|<dq>|T<bd>>><cond|<around|(|i*r*r*e*v*e*r*s*i*b*l*e*p*r*o*c*e*s*s,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|del
    S\<gtr\>int(dq/Tb)>>>>>
  </gather>

  <I|Irreversible process\|)><I|Process!irreversible\|)>

  <section|Applications><label|4-applications>

  The lengthy derivation in Secs. <reference|4-Carnot
  engines>\U<reference|4-irrev processes> is based on the Kelvin\UPlanck
  statement describing the impossibility of converting completely into work
  the energy transferred into the system by heat from a single
  <subindex|Heat|reservoir>heat reservoir. The derivation has now given us
  all parts of the mathematical statement of the second law shown in the box
  on page <pageref|second law box>. The mathematical statement includes an
  equality, <math|<dif>S=<dq><rev>/T<bd>>, that applies to an infinitesimal
  <em|reversible> change, and an inequality, <math|<dif>S\<gtr\><dq>/T<bd>>,
  that applies to an infinitesimal <em|irreversible> change. It is convenient
  to combine the equality and inequality in a single relation that is a
  general mathematical statement of the second law: <subindex|Second law of
  thermodynamics|mathematical statement>

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>S\<geq\><frac|<dq>|T<bd>>><cond|<around|(|<ir>,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|dS
    \<gtr\>= dq/Tb>>>>>
  </gather>

  The inequality refers to an irreversible change and the equality to a
  reversible change, as indicated by the notation <ir> in the conditions of
  validity.<label|meaning of ir> The integrated form of this relation is

  <\gather>
    <tformat|<table|<row|<cell|<s|<Del>S\<geq\><big|int><space|-0.17em><frac|<dq>|T<bd>>><cond|<around|(|<ir>,c*l*o*s*e*d*s*y*s*t*e*m|)>><eq-number><label|delS
    \<gtr\>= dq/Tb>>>>>
  </gather>

  During a reversible process, the states are equilibrium states and the
  temperature is usually uniform throughout the system. The only exception is
  if the system happens to have internal adiabatic partitions that allow
  phases of different temperatures in an equilibrium state. As mentioned in
  the footnote on page <pageref|T replaces T<rsub|b>>, when the process is
  reversible and the temperature is uniform, we can replace
  <math|<dif>S=<dq><rev>/T<bd>> by <math|<dif>S=<dq><rev>/T>.

  The rest of Sec. <reference|4-applications> will apply Eqs. <reference|dS
  \<gtr\>= dq/Tb> and <reference|delS \<gtr\>= dq/Tb> to various reversible
  and irreversible processes.

  <subsection|Reversible heating><label|4-rev heating \ expansion>

  <I|Heating!reversible\|(><I|Reversible!heating\|(>

  The definition of the heat capacity <math|C> of a closed system is given by
  Eq. <reference|heat capacity def><vpageref|heat capacity def>:
  <math|C<defn><dq>/<dif>T>. For reversible heating or cooling of a
  homogeneous phase, <math|<dq>> is equal to <math|T<dif>S> and we can write

  <\equation>
    <label|del S=int(C/T)dT><Del>S=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em><frac|C|T><dif>T
  </equation>

  where <math|C> should be replaced by <math|C<rsub|V>> if the volume is
  constant, or by <math|C<rsub|p>> if the pressure is constant (Sec.
  <reference|3-heat capacity>). If the heat capacity has a constant value
  over the temperature range from <math|T<rsub|1>> to <math|T<rsub|2>>, the
  equation becomes

  <\equation>
    <label|Del S=Cln(T2/T1)><Del>S=C*ln <frac|T<rsub|2>|T<rsub|1>>
  </equation>

  Heating increases the entropy, and cooling decreases it.

  <I|Heating!reversible\|)><I|Reversible!heating\|)>

  <subsection|Reversible expansion of an ideal gas><label|4-rev expansion>

  <subindex|Expansionreversible, of an ideal
  gas><subindex|Reversible|expansion of an ideal gas>

  When the volume of an ideal gas, or of any other fluid, is changed
  reversibly and <em|adiabatically>, there is of course no entropy change.

  When the volume of an ideal gas is changed reversibly and
  <em|isothermally>, there is expansion work given by <math|w=-n*R*T*ln
  <around|(|V<rsub|2>/V<rsub|1>|)>> (Eq. <reference|w=-nRT ln(V2/V1)>). Since
  the internal energy of an ideal gas is constant at constant temperature,
  there must be heat of equal magnitude and opposite sign: <math|q=n*R*T*ln
  <around|(|V<rsub|2>/V<rsub|1>|)>>. The entropy change is therefore

  <\gather>
    <tformat|<table|<row|<cell|<s|<Del>S=n*R*ln
    <frac|V<rsub|2>|V<rsub|1>>><cond|(r*e*v*e*r*s*i*b*l*e*i*s*o*t*h*e*r*m*a*l*v*o*l*u*m*e><nextcond|c*h*a*n*g*e*o*f*a*n*i*d*e*a*l*g*a*s)><eq-number><label|Del
    S=nRln(V2/V1)>>>>>
  </gather>

  Isothermal expansion increases the entropy, and isothermal compression
  decreases it.

  Since the change of a state function depends only on the initial and final
  states, Eq. <reference|Del S=nRln(V2/V1)> gives a valid expression for
  <math|<Del>S> of an ideal gas under the less stringent condition
  <math|T<rsub|2>=T<rsub|1>>; it is not necessary for the intermediate states
  to be equilibrium states of the same temperature.

  <subsection|Spontaneous changes in an isolated system>

  <I|Isolated system!spontaneous changes in\|(>

  An isolated system is one that exchanges no matter or energy with its
  surroundings. Any change of state of an isolated system that actually
  occurs is spontaneous, and arises solely from conditions within the system,
  uninfluenced by changes in the surroundings\Vthe process occurs by itself,
  of its own accord. The initial state and the intermediate states of the
  process must be nonequilibrium states, because by definition an equilibrium
  state would not change over time in the isolated system.

  Unless the spontaneous change is purely mechanical, it is
  <subindex|Process|irreversible><index|Irreversible process>irreversible.
  According to the second law, during an infinitesimal change that is
  irreversible and adiabatic, the entropy increases. For the isolated system,
  we can therefore write

  <\gather>
    <tformat|<table|<row|<cell|<s|<dif>S\<gtr\>0><cond|<around|(|i*r*r*e*v*e*r*s*i*b*l*e*c*h*a*n*g*e,i*s*o*l*a*t*e*d*s*y*s*t*e*m|)>><eq-number><label|dS\<gtr\>0
    (irrev, isolated)>>>>>
  </gather>

  In later chapters, the inequality of Eq. <reference|dS\<gtr\>0 (irrev,
  isolated)> will turn out to be one of the most useful for deriving
  conditions for spontaneity and equilibrium in chemical systems: <em|The
  entropy of an isolated system continuously increases during a spontaneous,
  irreversible process until it reaches a maximum value at equilibrium>.

  If we treat the universe as an isolated system (although cosmology provides
  no assurance that this is a valid concept), we can say that as spontaneous
  changes occur in the universe, its entropy continuously increases.
  <index|Clausius, Rudolf>Clausius summarized the first and second laws in a
  famous statement: <em|Die Energie der Welt ist constant; die Entropie der
  Welt strebt einem Maximum zu> (the energy of the universe is constant; the
  entropy of the universe strives toward a maximum).

  <I|Isolated system!spontaneous changes in\|)>

  <subsection|Internal heat flow in an isolated system>

  <I|Heat!flow in an isolated system\|(>

  Suppose the system is a solid body whose temperature initially is
  nonuniform. Provided there are no internal adiabatic partitions, the
  initial state is a nonequilibrium state lacking internal thermal
  equilibrium. If the system is surrounded by thermal insulation, and volume
  changes are negligible, this is an isolated system. There will be a
  spontaneous, irreversible internal redistribution of thermal energy that
  eventually brings the system to a final equilibrium state of uniform
  temperature.

  In order to be able to specify internal temperatures at any instant, we
  treat the system as an assembly of phases, each having a uniform
  temperature that can vary with time. To describe a region that has a
  continuous temperature gradient, we approximate the region with a very
  large number of very small phases or parcels, each having a temperature
  infinitesimally different from its neighbors.

  We use Greek letters to label the phases. The temperature of phase
  <math|<pha>> at any given instant is <math|T<aph>>. We can treat each phase
  as a subsystem with a boundary across which there can be energy transfer in
  the form of heat. Let <math|<dq><rsub|<pha><phb>>> represent an
  infinitesimal quantity of heat transferred during an infinitesimal interval
  of time to phase <math|<pha>> from phase <math|<phb>>. The heat transfer,
  if any, is to the cooler from the warmer phase. If phases <math|<pha>> and
  <math|<phb>> are in thermal contact and <math|T<aph>> is less than
  <math|T<bph>>, then <math|<dq><rsub|<pha><phb>>> is positive; if the phases
  are in thermal contact and <math|T<aph>> is greater than <math|T<bph>>,
  <math|<dq><rsub|<pha><phb>>> is negative; and if neither of these
  conditions is satisfied, <math|<dq><rsub|<pha><phb>>> is zero.

  To evaluate the entropy change, we need a reversible path from the initial
  to the final state. The net quantity of heat transferred to phase
  <math|<pha>> during an infinitesimal time interval is
  <math|<dq><aph>=<big|sum><rsub|<phb>\<neq\><pha>><dq><rsub|<pha><phb>>>.
  The entropy change of phase <math|<pha>> is the same as it would be for the
  reversible transfer of this heat from a <subindex|Heat|reservoir>heat
  reservoir of temperature <math|T<aph>>:
  <math|<dif>S<aph>=<dq><aph>/T<aph>>. The entropy change of the entire
  system along the reversible path is found by summing over all phases:

  <\equation>
    <label|dS=sum(a)sum(b)>

    <\eqsplit>
      <tformat|<table|<row|<cell|<dif>S>|<cell|=<big|sum><rsub|<pha>><dif>S<aph>=<big|sum><rsub|<pha>><frac|<dq><aph>|T<aph>>=<big|sum><rsub|<pha>><big|sum><rsub|<phb>\<neq\><pha>><frac|<dq><rsub|<pha><phb>>|T<aph>>>>|<row|<cell|>|<cell|=<big|sum><rsub|<pha>><big|sum><rsub|<phb>\<gtr\><pha>><around*|(|<frac|<dq><rsub|<pha><phb>>|T<aph>>+<frac|<dq><rsub|<phb><pha>>|T<bph>>|)>>>>>
    </eqsplit>
  </equation>

  There is also the condition of quantitative energy transfer,
  <math|<dq><rsub|<phb><pha>>=-<dq><rsub|<pha><phb>>>, which we use to
  rewrite Eq. <reference|dS=sum(a)sum(b)> in the form

  <\equation>
    <label|dS=sum'(a)sum(b)><dif>S=<big|sum><rsub|<pha>><big|sum><rsub|<phb>\<gtr\><pha>><around*|(|<frac|1|T<aph>>-<frac|1|T<bph>>|)><dq><rsub|<pha><phb>>
  </equation>

  Consider an individual term of the sum on the right side of Eq.
  <reference|dS=sum'(a)sum(b)> that has a nonzero value of
  <math|<dq><rsub|<pha><phb>>> due to finite heat transfer between phases
  <math|<pha>> and <math|<phb>>. If <math|T<aph>> is less than <math|T<bph>>,
  then both <math|<dq><rsub|<pha><phb>>> and
  <math|<around|(|1/T<aph>-1/T<bph>|)>> are positive. If, on the other hand,
  <math|T<aph>> is greater than <math|T<bph>>, both
  <math|<dq><rsub|<pha><phb>>> and <math|<around|(|1/T<aph>-1/T<bph>|)>> are
  negative. Thus each term of the sum is either zero or positive, and as long
  as phases of different temperature are present, <math|<dif>S> is positive.

  This derivation shows that during a spontaneous thermal equilibration
  process in an isolated system, starting with any initial distribution of
  the internal temperatures, the entropy continuously increases until the
  system reaches a state of thermal equilibrium with a single uniform
  temperature throughout.<footnote|Leff, in Ref. <cite|leff-77>, obtains the
  same result by a more complicated derivation.> The result agrees with Eq.
  <reference|dS\<gtr\>0 (irrev, isolated)>.

  <I|Heat!flow in an isolated system\|)>

  <subsection|Free expansion of a gas>

  <I|Free expansion\|(>

  Consider the free expansion of a gas shown in Fig. <reference|fig:3-free
  expansion><vpageref|fig:3-free expansion>. The <em|system> is the gas.
  Assume that the vessel walls are rigid and adiabatic, so that the system is
  isolated. When the stopcock between the two vessels is opened, the gas
  expands irreversibly into the vacuum without heat or work and at constant
  internal energy. To carry out the same change of state reversibly, we
  confine the gas at its initial volume and temperature in a
  cylinder-and-piston device and use the piston to expand the gas
  adiabatically with negative work. Positive heat is then needed to return
  the internal energy reversibly to its initial value. Because the reversible
  path has positive heat, the entropy change is positive.

  This is an example of an <subindex|Process|irreversible><index|Irreversible
  process>irreversible process in an isolated system for which a reversible
  path between the initial and final states has both heat and work.

  <I|Free expansion\|)>

  <subsection|Adiabatic process with work><label|4-adiabatic processes with
  work>

  <subindex|Process|adiabatic><subindex|Adiabatic|process>In general (page
  <pageref|minimal work principle>), an adiabatic process with a given
  initial equilibrium state and a given change of a work coordinate has the
  least positive or most negative work in the reversible limit. Consider an
  irreversible adiabatic process with work <math|w<irr>>. The same change of
  state can be accomplished reversibly by the following two steps: (1) a
  reversible adiabatic change of the work coordinate with work <math|w<rev>>,
  followed by (2) reversible transfer of heat <math|q<rev>> with no further
  change of the work coordinate. Since <math|w<rev>> is algebraically less
  than <math|w<irr>>, <math|q<rev>> must be positive in order to make
  <math|<Del>U> the same in the irreversible and reversible paths. The
  positive heat increases the entropy along the reversible path, and
  consequently the irreversible adiabatic process has a positive entropy
  change. This conclusion agrees with the second-law inequality of Eq.
  <reference|dS \<gtr\>= dq/Tb>.

  <section|Summary><label|4-summary>

  Some of the important terms and definitions discussed in this chapter are
  as follows.

  <\itemize>
    <item>Any conceivable process is either spontaneous, reversible, or
    impossible.

    <item>A <subindex|Reversible|process><subindex|Process|reversible><em|reversible>
    process proceeds by a continuous sequence of equilibrium states.

    <item>A <index|Spontaneous process><subindex|Process|spontaneous><em|spontaneous>
    process is one that proceeds naturally at a finite rate.

    <item>An <index|Irreversible process><subindex|Process|irreversible><em|irreversible>
    process is a spontaneous process whose reverse is impossible.

    <item>A <subindex|Process|mechanical><em|purely mechanical process> is an
    idealized process without temperature gradients, and without friction or
    other <subindex|Energy|dissipation of><index|Dissipation of
    energy>dissipative effects, that is spontaneous in either direction. This
    kind of process will be ignored in the remaining chapters of this book.

    <item>Except for a purely mechanical process, the terms <em|spontaneous>
    and <em|irreversible> are equivalent.
  </itemize>

  The derivation of the mathematical statement of the second law shows that
  during a reversible process of a closed system, the infinitesimal quantity
  <math|<dq>/T<bd>> equals the infinitesimal change of a state function
  called the <index|Entropy>entropy, <math|S>. Here <math|<dq>> is heat
  transferred at the boundary where the temperature is <math|T<bd>>.

  In each infinitesimal path element of a process of a closed system,
  <math|<dif>S> is equal to <math|<dq>/T<bd>> if the process is reversible,
  and is greater than <math|<dq>/T<bd>> if the process is irreversible, as
  summarized by the relation <math|<dif>S\<geq\><dq>/T<bd>>.

  Consider two particular equilibrium states <math|1> and <math|2> of a
  closed system. The system can change from state <math|1> to state <math|2>
  by either a reversible process, with <math|<Del>S> equal to the integral
  <math|<big|int><around|(|<dq>/T<bd>|)>>, or an irreversible process, with
  <math|<Del>S> greater than <math|<big|int><around|(|<dq>/T<bd>|)>>. It is
  important to keep in mind the point made by Fig. <reference|fig:4-two
  paths><vpageref|fig:4-two paths>:

  <\big-figure>
    <boxedfigure|<image|./04-SUP/twopaths.eps||||> <capt|Reversible and
    irreversible paths between the same initial and final equilibrium states
    of a closed system. The value of <math|<Del>S> is the same for both
    paths, but the values of the integral
    <math|<big|int><around|(|<dq>/T<bd>|)>> are different.<label|fig:4-two
    paths>>>
  </big-figure|>

  because <math|S> is a state function, it is the value of the integral that
  is different in the two cases, and not the value of <math|<Del>S>.

  The second law establishes no general relation between entropy changes and
  heat in an open system, or for an impossible process. The entropy of an
  open system may increase or decrease depending on whether matter enters or
  leaves. It is possible to imagine different impossible processes in which
  <math|<dif>S> is less than, equal to, and greater than <math|<dq>/T<bd>>.

  <section|The Statistical Interpretation of Entropy><label|4-statistical>

  Because entropy is such an important state function, it is natural to seek
  a description of its meaning on the microscopic level.

  <I|Entropy!measure of disorder@as a measure of
  disorder\|reg><index|Disorder>Entropy is sometimes said to be a measure of
  \Pdisorder.\Q According to this idea, the entropy increases whenever a
  closed system becomes more disordered on a microscopic scale. This
  description of entropy as a measure of disorder is highly misleading. It
  does not explain why entropy is increased by reversible heating at constant
  volume or pressure, or why it increases during the reversible isothermal
  expansion of an ideal gas. Nor does it seem to agree with the freezing of a
  supercooled liquid or the formation of crystalline solute in a
  supersaturated solution; these processes can take place spontaneously in an
  isolated system, yet are accompanied by an apparent <em|decrease> of
  disorder.

  Thus we should not interpret entropy as a measure of disorder. We must look
  elsewhere for a satisfactory microscopic interpretation of entropy.

  A rigorous interpretation is provided by the discipline of
  <index|Statistical mechanics><em|statistical mechanics>, which derives a
  precise expression for entropy based on the behavior of macroscopic amounts
  of microscopic particles. Suppose we focus our attention on a particular
  macroscopic equilibrium state. Over a period of time, while the system is
  in this equilibrium state, the system at each instant is in a
  <index|Microstate><em|microstate>, or stationary quantum state, with a
  definite energy. The microstate is one that is <em|accessible> to the
  system\Vthat is, one whose wave function is compatible with the system's
  volume and with any other conditions and constraints imposed on the system.
  The system, while in the equilibrium state, continually jumps from one
  accessible microstate to another, and the macroscopic state functions
  described by classical thermodynamics are time averages of these
  microstates.

  The fundamental assumption of <index|Statistical mechanics>statistical
  mechanics is that accessible microstates of equal energy are equally
  probable, so that the system while in an equilibrium state spends an equal
  fraction of its time in each such microstate. The statistical entropy of
  the equilibrium state then turns out to be given by the equation

  <\equation>
    <label|S=k ln W>S<subs|s*t*a*t>=k*ln W+C
  </equation>

  where <math|k> is the Boltzmann constant <math|k=R/N<subs|A>>, <math|W> is
  the number of accessible microstates, and <math|C> is a constant.

  In the case of an equilibrium state of a perfectly-isolated system of
  constant internal energy <math|U>, the accessible microstates are the ones
  that are compatible with the constraints and whose energies all have the
  same value, equal to the value of <math|U>.

  It is more realistic to treat an equilibrium state with the assumption the
  system is in thermal equilibrium with an external constant-temperature
  <subindex|Heat|reservoir>heat reservoir. The internal energy then
  fluctuates over time with extremely small deviations from the average value
  <math|U>, and the accessible microstates are the ones with energies close
  to this average value. In the language of <index|Statistical
  mechanics>statistical mechanics, the results for an isolated system are
  derived with a microcanonical ensemble, and for a system of constant
  temperature with a canonical ensemble.

  A change <math|<Del>S<subs|s*t*a*t>> of the statistical entropy function
  given by Eq. <reference|S=k ln W> is the same as the change <math|<Del>S>
  of the macroscopic second-law entropy, because the derivation of Eq.
  <reference|S=k ln W> is based on the macroscopic relation
  <math|<dif>S<subs|s*t*a*t>=<dq>/T=<around|(|<dif>U-<dw>|)>/T> with
  <math|<dif>U> and <math|<dw>> given by statistical theory. If the
  integration constant <math|C> is set equal to zero, <math|S<subs|s*t*a*t>>
  becomes the third-law entropy <math|S> to be described in Chap.
  <reference|Chap. 6>.

  Equation <reference|S=k ln W> shows that a reversible process in which
  entropy increases is accompanied by an increase in the number of accessible
  microstates of equal, or nearly equal, internal energies. This
  interpretation of entropy increase has been described as the spreading and
  sharing of energy<footnote|Ref. <cite|leff-96>.> and as the dispersal of
  energy.<footnote|Ref. <cite|lambert-02a>.> It has even been proposed that
  entropy should be thought of as a \Pspreading function\Q with its symbol
  <math|S> suggesting <em|spreading>.<footnote|Ref.
  <cite|lambert-09>.><footnote|The symbol <math|S> for entropy seems
  originally to have been an arbitrary choice by <index|Clausius,
  Rudolf>Clausius; see Ref. <cite|howard-01>.>

  <new-page><phantomsection><addcontentsline|toc|section|Problems>
  <paragraphfootnotes><problems| <input|04-problems><page-break>>
  <plainfootnotes>

  <initial|<\collection>
  </collection>>

  <\references>
    <\collection>
      <associate|0=qh+qc+w|<tuple|1|?>>
      <associate|4-Carnot engines|<tuple|3|?>>
      <associate|4-Carnot engines and cycles|<tuple|3.1|?>>
      <associate|4-Clausius|<tuple|<with|mode|math|<with|font-series|bold|math-font-series|bold|<rigid|\<ast\>>>>|?>>
      <associate|4-Clausius inequality|<tuple|4.1|?>>
      <associate|4-Clausius statement|<tuple|2|?>>
      <associate|4-adiabatic processes with work|<tuple|6.6|?>>
      <associate|4-alternative derivation|<tuple|4.3|?>>
      <associate|4-applications|<tuple|6|?>>
      <associate|4-irrev ad processes|<tuple|5.1|?>>
      <associate|4-irrev processes|<tuple|5|?>>
      <associate|4-irrev processes in general|<tuple|5.2|?>>
      <associate|4-properties of entropy|<tuple|4.4|?>>
      <associate|4-rev expansion|<tuple|6.2|?>>
      <associate|4-rev heating \ expansion|<tuple|6.1|?>>
      <associate|4-rev processes|<tuple|4|?>>
      <associate|4-rev processes to define entropy|<tuple|4.2|?>>
      <associate|4-second law statements|<tuple|2|?>>
      <associate|4-statistical|<tuple|8|?>>
      <associate|4-summary|<tuple|7|?>>
      <associate|4-thermodynamic temperature|<tuple|3.4|?>>
      <associate|4-types of processes|<tuple|1|?>>
      <associate|Chap. 4|<tuple|1|?>>
      <associate|Del S=Cln(T2/T1)|<tuple|32|?>>
      <associate|Del S=nRln(V2/V1)|<tuple|33|?>>
      <associate|S=k ln W|<tuple|37|?>>
      <associate|T replaces T_b|<tuple|2|?>>
      <associate|Tc/Th=-qc/qh|<tuple|15|?>>
      <associate|auto-1|<tuple|1|?>>
      <associate|auto-10|<tuple|5|?>>
      <associate|auto-11|<tuple|3.2|?>>
      <associate|auto-12|<tuple|6|?>>
      <associate|auto-13|<tuple|3.3|?>>
      <associate|auto-14|<tuple|7|?>>
      <associate|auto-15|<tuple|3.4|?>>
      <associate|auto-16|<tuple|4|?>>
      <associate|auto-17|<tuple|4.1|?>>
      <associate|auto-18|<tuple|8|?>>
      <associate|auto-19|<tuple|4.2|?>>
      <associate|auto-2|<tuple|1|?>>
      <associate|auto-20|<tuple|9|?>>
      <associate|auto-21|<tuple|10|?>>
      <associate|auto-22|<tuple|4.3|?>>
      <associate|auto-23|<tuple|4.4|?>>
      <associate|auto-24|<tuple|5|?>>
      <associate|auto-25|<tuple|5.1|?>>
      <associate|auto-26|<tuple|5.2|?>>
      <associate|auto-27|<tuple|11|?>>
      <associate|auto-28|<tuple|6|?>>
      <associate|auto-29|<tuple|6.1|?>>
      <associate|auto-3|<tuple|2|?>>
      <associate|auto-30|<tuple|6.2|?>>
      <associate|auto-31|<tuple|6.3|?>>
      <associate|auto-32|<tuple|6.4|?>>
      <associate|auto-33|<tuple|6.5|?>>
      <associate|auto-34|<tuple|6.6|?>>
      <associate|auto-35|<tuple|7|?>>
      <associate|auto-36|<tuple|12|?>>
      <associate|auto-37|<tuple|8|?>>
      <associate|auto-4|<tuple|1|?>>
      <associate|auto-5|<tuple|2|?>>
      <associate|auto-6|<tuple|3|?>>
      <associate|auto-7|<tuple|3.1|?>>
      <associate|auto-8|<tuple|3|?>>
      <associate|auto-9|<tuple|4|?>>
      <associate|dS \<gtr\>= dq/Tb|<tuple|29|?>>
      <associate|dS(res)=|<tuple|25|?>>
      <associate|dS(ss)=sum|<tuple|26|?>>
      <associate|dS\<gtr\>0 (irrev, isolated)|<tuple|34|?>>
      <associate|dS\<gtr\>int(dq/Tb)|<tuple|27|?>>
      <associate|dS=dq/Tb|<tuple|22|?>>
      <associate|dS=sum'(a)sum(b)|<tuple|36|?>>
      <associate|dS=sum(a)sum(b)|<tuple|35|?>>
      <associate|del S\<gtr\>int(dq/Tb)|<tuple|28|?>>
      <associate|del S=int(C/T)dT|<tuple|31|?>>
      <associate|delS \<gtr\>= dq/Tb|<tuple|30|?>>
      <associate|delS=int(dS)=int(dq/Tb)|<tuple|24|?>>
      <associate|delS=int(dq/Tb)|<tuple|21|?>>
      <associate|dq'=Tres(dq/Tb)|<tuple|16|?>>
      <associate|dq/T=CVdT/T+nRdV/V|<tuple|5|?>>
      <associate|dq=CVdT+(nRT/V)dV|<tuple|4|?>>
      <associate|efficiency\<less\>1|<tuple|3|?>>
      <associate|epsilon=1+qc/qh|<tuple|3|?>>
      <associate|epsilon=1-Tc/Th|<tuple|14|?>>
      <associate|fig:4-VT adiabats|<tuple|9|?>>
      <associate|fig:4-adiabatic_supersystem|<tuple|11|?>>
      <associate|fig:4-ig Carnot engine|<tuple|3|?>>
      <associate|fig:4-impossible1|<tuple|1|?>>
      <associate|fig:4-impossible2|<tuple|2|?>>
      <associate|fig:4-impossible3|<tuple|6|?>>
      <associate|fig:4-impossible4|<tuple|7|?>>
      <associate|fig:4-one cycle|<tuple|5|?>>
      <associate|fig:4-rev paths|<tuple|10|?>>
      <associate|fig:4-supersystem|<tuple|8|?>>
      <associate|fig:4-two paths|<tuple|12|?>>
      <associate|fig:4-water Carnot engine|<tuple|4|?>>
      <associate|footnote-1|<tuple|1|?>>
      <associate|footnote-10|<tuple|10|?>>
      <associate|footnote-11|<tuple|11|?>>
      <associate|footnote-12|<tuple|12|?>>
      <associate|footnote-13|<tuple|13|?>>
      <associate|footnote-14|<tuple|14|?>>
      <associate|footnote-2|<tuple|2|?>>
      <associate|footnote-3|<tuple|3|?>>
      <associate|footnote-4|<tuple|4|?>>
      <associate|footnote-5|<tuple|5|?>>
      <associate|footnote-6|<tuple|6|?>>
      <associate|footnote-7|<tuple|7|?>>
      <associate|footnote-8|<tuple|8|?>>
      <associate|footnote-9|<tuple|9|?>>
      <associate|footnr-1|<tuple|1|?>>
      <associate|footnr-10|<tuple|10|?>>
      <associate|footnr-11|<tuple|11|?>>
      <associate|footnr-12|<tuple|12|?>>
      <associate|footnr-13|<tuple|13|?>>
      <associate|footnr-14|<tuple|14|?>>
      <associate|footnr-2|<tuple|2|?>>
      <associate|footnr-3|<tuple|3|?>>
      <associate|footnr-4|<tuple|4|?>>
      <associate|footnr-5|<tuple|5|?>>
      <associate|footnr-6|<tuple|6|?>>
      <associate|footnr-7|<tuple|7|?>>
      <associate|footnr-8|<tuple|8|?>>
      <associate|footnr-9|<tuple|9|?>>
      <associate|impossible process defn|<tuple|<with|mode|math|\<bullet\>>|?>>
      <associate|ln(V2/V1)=-ln(V4/V3)|<tuple|9|?>>
      <associate|meaning of ir|<tuple|29|?>>
      <associate|oint(dq(rev)/Tb)=0|<tuple|23|?>>
      <associate|oint(dq/Tb)\<less\>=0|<tuple|18|?>>
      <associate|q'(ss)=int(CD)|<tuple|20|?>>
      <associate|q'=(Tres)int(dq/Tb)|<tuple|17|?>>
      <associate|q(ss)=int(AB)|<tuple|19|?>>
      <associate|qc/qh=-Tc/Th|<tuple|13|?>>
      <associate|rev ad surface|<tuple|7|?>>
      <associate|second law box|<tuple|2|?>>
    </collection>
  </references>

  <\auxiliary>
    <\collection>
      <\associate|bib>
        clausius-1854

        planck-22

        lewis-23

        hats-65

        pippard-66

        pauli-73

        adkins-83

        norton-16

        leff-77

        leff-96

        lambert-02a

        lambert-09

        howard-01
      </associate>
      <\associate|figure>
        <tuple|normal|<surround|<hidden-binding|<tuple>|1>||>|<pageref|auto-4>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|2>||>|<pageref|auto-5>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|3>||>|<pageref|auto-8>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|4>||>|<pageref|auto-9>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|5>||>|<pageref|auto-10>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|6>||>|<pageref|auto-12>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|7>||>|<pageref|auto-14>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|8>||>|<pageref|auto-18>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|9>||>|<pageref|auto-20>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|10>||>|<pageref|auto-21>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|11>||>|<pageref|auto-27>>

        <tuple|normal|<surround|<hidden-binding|<tuple>|12>||>|<pageref|auto-36>>
      </associate>
      <\associate|toc>
        <vspace*|2fn><with|font-series|bold|math-font-series|bold|font-size|1.19|1<space|2spc>The
        Second Law> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-1><vspace|1fn>

        <vspace*|1fn><with|font-series|bold|math-font-series|bold|1<space|2spc>Types
        of Processes> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-2><vspace|0.5fn>

        <vspace*|1fn><with|font-series|bold|math-font-series|bold|2<space|2spc>Statements
        of the Second Law> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-3><vspace|0.5fn>

        <vspace*|1fn><with|font-series|bold|math-font-series|bold|3<space|2spc>Concepts
        Developed with Carnot Engines> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-6><vspace|0.5fn>

        <with|par-left|1tab|3.1<space|2spc>Carnot engines and Carnot cycles
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-7>>

        <with|par-left|1tab|3.2<space|2spc>The equivalence of the Clausius
        and Kelvin\UPlanck statements <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-11>>

        <with|par-left|1tab|3.3<space|2spc>The efficiency of a Carnot engine
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-13>>

        <with|par-left|1tab|3.4<space|2spc>Thermodynamic temperature
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-15>>

        <vspace*|1fn><with|font-series|bold|math-font-series|bold|4<space|2spc>The
        Second Law for Reversible Processes>
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-16><vspace|0.5fn>

        <with|par-left|1tab|4.1<space|2spc>The Clausius inequality
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-17>>

        <with|par-left|1tab|4.2<space|2spc>Using reversible processes to
        define the entropy <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-19>>

        <with|par-left|1tab|4.3<space|2spc>Alternative derivation of entropy
        as a state function <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-22>>

        <with|par-left|1tab|4.4<space|2spc>Some properties of the entropy
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-23>>

        <vspace*|1fn><with|font-series|bold|math-font-series|bold|5<space|2spc>The
        Second Law for Irreversible Processes>
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-24><vspace|0.5fn>

        <with|par-left|1tab|5.1<space|2spc>Irreversible adiabatic processes
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-25>>

        <with|par-left|1tab|5.2<space|2spc>Irreversible processes in general
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-26>>

        <vspace*|1fn><with|font-series|bold|math-font-series|bold|6<space|2spc>Applications>
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-28><vspace|0.5fn>

        <with|par-left|1tab|6.1<space|2spc>Reversible heating
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-29>>

        <with|par-left|1tab|6.2<space|2spc>Reversible expansion of an ideal
        gas <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-30>>

        <with|par-left|1tab|6.3<space|2spc>Spontaneous changes in an isolated
        system <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-31>>

        <with|par-left|1tab|6.4<space|2spc>Internal heat flow in an isolated
        system <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-32>>

        <with|par-left|1tab|6.5<space|2spc>Free expansion of a gas
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-33>>

        <with|par-left|1tab|6.6<space|2spc>Adiabatic process with work
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-34>>

        <vspace*|1fn><with|font-series|bold|math-font-series|bold|7<space|2spc>Summary>
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-35><vspace|0.5fn>

        <vspace*|1fn><with|font-series|bold|math-font-series|bold|8<space|2spc>The
        Statistical Interpretation of Entropy>
        <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
        <no-break><pageref|auto-37><vspace|0.5fn>
      </associate>
    </collection>
  </auxiliary>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|0=qh+qc+w|<tuple|1.3.1|?>>
    <associate|4-Carnot engines|<tuple|1.3|?>>
    <associate|4-Carnot engines and cycles|<tuple|1.3.1|?>>
    <associate|4-Clausius|<tuple|<with|mode|<quote|math>|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|<rigid|\<ast\>>>>|?>>
    <associate|4-Clausius inequality|<tuple|1.4.1|?>>
    <associate|4-Clausius statement|<tuple|1.2.2|?>>
    <associate|4-adiabatic processes with work|<tuple|1.6.6|?>>
    <associate|4-alternative derivation|<tuple|1.4.3|?>>
    <associate|4-applications|<tuple|1.6|?>>
    <associate|4-irrev ad processes|<tuple|1.5.1|?>>
    <associate|4-irrev processes|<tuple|1.5|?>>
    <associate|4-irrev processes in general|<tuple|1.5.2|?>>
    <associate|4-properties of entropy|<tuple|1.4.4|?>>
    <associate|4-rev expansion|<tuple|1.6.2|?>>
    <associate|4-rev heating \ expansion|<tuple|1.6.1|?>>
    <associate|4-rev processes|<tuple|1.4|?>>
    <associate|4-rev processes to define entropy|<tuple|1.4.2|?>>
    <associate|4-second law statements|<tuple|1.2|?>>
    <associate|4-statistical|<tuple|1.8|?>>
    <associate|4-summary|<tuple|1.7|?>>
    <associate|4-thermodynamic temperature|<tuple|1.3.4|?>>
    <associate|4-types of processes|<tuple|1.1|?>>
    <associate|Chap. 4|<tuple|1|?>>
    <associate|Del S=Cln(T2/T1)|<tuple|1.6.4|?>>
    <associate|Del S=nRln(V2/V1)|<tuple|1.6.5|?>>
    <associate|S=k ln W|<tuple|1.8.1|?>>
    <associate|T replaces T_b|<tuple|Process|?>>
    <associate|Tc/Th=-qc/qh|<tuple|1.3.15|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|Process|?>>
    <associate|auto-100|<tuple|1.4.3|?>>
    <associate|auto-101|<tuple|Supersystem|?>>
    <associate|auto-102|<tuple|Heat|?>>
    <associate|auto-103|<tuple|Supersystem|?>>
    <associate|auto-104|<tuple|Heat|?>>
    <associate|auto-105|<tuple|Second law of thermodynamics|?>>
    <associate|auto-106|<tuple|Integrating factor|?>>
    <associate|auto-107|<tuple|1.4.3|?>>
    <associate|auto-108|<tuple|1.4.4|?>>
    <associate|auto-109|<tuple|1.5|?>>
    <associate|auto-11|<tuple|Irreversible process|?>>
    <associate|auto-110|<tuple|1.5.1|?>>
    <associate|auto-111|<tuple|Energy|?>>
    <associate|auto-112|<tuple|Dissipation of energy|?>>
    <associate|auto-113|<tuple|1.5.2|?>>
    <associate|auto-114|<tuple|Supersystem|?>>
    <associate|auto-115|<tuple|Heat|?>>
    <associate|auto-116|<tuple|1.5.1|?>>
    <associate|auto-117|<tuple|Supersystem|?>>
    <associate|auto-118|<tuple|Supersystem|?>>
    <associate|auto-119|<tuple|1.6|?>>
    <associate|auto-12|<tuple|Process|?>>
    <associate|auto-120|<tuple|Heat|?>>
    <associate|auto-121|<tuple|Second law of thermodynamics|?>>
    <associate|auto-122|<tuple|1.6.1|?>>
    <associate|auto-123|<tuple|1.6.2|?>>
    <associate|auto-124|<tuple|Expansionreversible, of an ideal gas|?>>
    <associate|auto-125|<tuple|Reversible|?>>
    <associate|auto-126|<tuple|1.6.3|?>>
    <associate|auto-127|<tuple|Process|?>>
    <associate|auto-128|<tuple|Irreversible process|?>>
    <associate|auto-129|<tuple|Clausius, Rudolf|?>>
    <associate|auto-13|<tuple|Reversible|?>>
    <associate|auto-130|<tuple|1.6.4|?>>
    <associate|auto-131|<tuple|Heat|?>>
    <associate|auto-132|<tuple|1.6.5|?>>
    <associate|auto-133|<tuple|Process|?>>
    <associate|auto-134|<tuple|Irreversible process|?>>
    <associate|auto-135|<tuple|1.6.6|?>>
    <associate|auto-136|<tuple|Process|?>>
    <associate|auto-137|<tuple|Adiabatic|?>>
    <associate|auto-138|<tuple|1.7|?>>
    <associate|auto-139|<tuple|Reversible|?>>
    <associate|auto-14|<tuple|1.2|?>>
    <associate|auto-140|<tuple|Process|?>>
    <associate|auto-141|<tuple|Spontaneous process|?>>
    <associate|auto-142|<tuple|Process|?>>
    <associate|auto-143|<tuple|Irreversible process|?>>
    <associate|auto-144|<tuple|Process|?>>
    <associate|auto-145|<tuple|Process|?>>
    <associate|auto-146|<tuple|Energy|?>>
    <associate|auto-147|<tuple|Dissipation of energy|?>>
    <associate|auto-148|<tuple|Entropy|?>>
    <associate|auto-149|<tuple|1.7.1|?>>
    <associate|auto-15|<tuple|Second law of thermodynamics|?>>
    <associate|auto-150|<tuple|1.8|?>>
    <associate|auto-151|<tuple|Disorder|?>>
    <associate|auto-152|<tuple|Statistical mechanics|?>>
    <associate|auto-153|<tuple|Microstate|?>>
    <associate|auto-154|<tuple|Statistical mechanics|?>>
    <associate|auto-155|<tuple|Heat|?>>
    <associate|auto-156|<tuple|Statistical mechanics|?>>
    <associate|auto-157|<tuple|Clausius, Rudolf|?>>
    <associate|auto-16|<tuple|mathematical statement of the second law|?>>
    <associate|auto-17|<tuple|Entropy|?>>
    <associate|auto-18|<tuple|entropy|?>>
    <associate|auto-19|<tuple|Reversible|?>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-20|<tuple|Process|?>>
    <associate|auto-21|<tuple|Clausius, Rudolf|?>>
    <associate|auto-22|<tuple|Kelvin, Baron of Largs|?>>
    <associate|auto-23|<tuple|Planck, Max|?>>
    <associate|auto-24|<tuple|1.2.1|?>>
    <associate|auto-25|<tuple|Clausius, Rudolf|?>>
    <associate|auto-26|<tuple|Clausius|?>>
    <associate|auto-27|<tuple|Second law of thermodynamics|?>>
    <associate|auto-28|<tuple|1.2.2|?>>
    <associate|auto-29|<tuple|Joule|?>>
    <associate|auto-3|<tuple|Spontaneous process|?>>
    <associate|auto-30|<tuple|Paddle wheel|?>>
    <associate|auto-31|<tuple|Heat engine|?>>
    <associate|auto-32|<tuple|heat engine|?>>
    <associate|auto-33|<tuple|Heat|?>>
    <associate|auto-34|<tuple|Perpetual motion of the second kind|?>>
    <associate|auto-35|<tuple|Heat|?>>
    <associate|auto-36|<tuple|Thomson, William|?>>
    <associate|auto-37|<tuple|Kelvin, Baron of Largs|?>>
    <associate|auto-38|<tuple|Planck, Max|?>>
    <associate|auto-39|<tuple|Kelvin--Planck statement of the second law|?>>
    <associate|auto-4|<tuple|Process|?>>
    <associate|auto-40|<tuple|Second law of thermodynamics|?>>
    <associate|auto-41|<tuple|Heat|?>>
    <associate|auto-42|<tuple|Lewis, Gilbert Newton|?>>
    <associate|auto-43|<tuple|Randall, Merle|?>>
    <associate|auto-44|<tuple|1.3|?>>
    <associate|auto-45|<tuple|1.3.1|?>>
    <associate|auto-46|<tuple|Heat engine|?>>
    <associate|auto-47|<tuple|Carnot|?>>
    <associate|auto-48|<tuple|Carnot engine|?>>
    <associate|auto-49|<tuple|Carnot|?>>
    <associate|auto-5|<tuple|Reversible|?>>
    <associate|auto-50|<tuple|Carnot cycles|?>>
    <associate|auto-51|<tuple|Working substance|?>>
    <associate|auto-52|<tuple|1.3.1|?>>
    <associate|auto-53|<tuple|1.3.2|?>>
    <associate|auto-54|<tuple|Heat|?>>
    <associate|auto-55|<tuple|Heat|?>>
    <associate|auto-56|<tuple|Steam engine|?>>
    <associate|auto-57|<tuple|1.3.3|?>>
    <associate|auto-58|<tuple|Carnot|?>>
    <associate|auto-59|<tuple|Carnot heat pump|?>>
    <associate|auto-6|<tuple|Process|?>>
    <associate|auto-60|<tuple|1.3.2|?>>
    <associate|auto-61|<tuple|1.3.4|?>>
    <associate|auto-62|<tuple|1.3.3|?>>
    <associate|auto-63|<tuple|efficiency|?>>
    <associate|auto-64|<tuple|1.3.5|?>>
    <associate|auto-65|<tuple|Working substance|?>>
    <associate|auto-66|<tuple|Heat|?>>
    <associate|auto-67|<tuple|Energy|?>>
    <associate|auto-68|<tuple|Dissipation of energy|?>>
    <associate|auto-69|<tuple|1.3.4|?>>
    <associate|auto-7|<tuple|Process|?>>
    <associate|auto-70|<tuple|Heat|?>>
    <associate|auto-71|<tuple|Kelvin, Baron of Largs|?>>
    <associate|auto-72|<tuple|Thermodynamic|?>>
    <associate|auto-73|<tuple|Temperature|?>>
    <associate|auto-74|<tuple|thermodynamic temperature|?>>
    <associate|auto-75|<tuple|Ideal-gas temperature|?>>
    <associate|auto-76|<tuple|Ideal-gas temperature|?>>
    <associate|auto-77|<tuple|Heat|?>>
    <associate|auto-78|<tuple|1.4|?>>
    <associate|auto-79|<tuple|1.4.1|?>>
    <associate|auto-8|<tuple|Process|?>>
    <associate|auto-80|<tuple|Supersystem|?>>
    <associate|auto-81|<tuple|1.4.1|?>>
    <associate|auto-82|<tuple|Heat|?>>
    <associate|auto-83|<tuple|Heat|?>>
    <associate|auto-84|<tuple|Heat|?>>
    <associate|auto-85|<tuple|Supersystem|?>>
    <associate|auto-86|<tuple|Supersystem|?>>
    <associate|auto-87|<tuple|Heat|?>>
    <associate|auto-88|<tuple|Kelvin--Planck statement of the second law|?>>
    <associate|auto-89|<tuple|Second law of thermodynamics|?>>
    <associate|auto-9|<tuple|Spontaneous process|?>>
    <associate|auto-90|<tuple|Clausius|?>>
    <associate|auto-91|<tuple|Clausius inequality|?>>
    <associate|auto-92|<tuple|1.4.2|?>>
    <associate|auto-93|<tuple|Work|?>>
    <associate|auto-94|<tuple|Reversible|?>>
    <associate|auto-95|<tuple|reversible adiabatic surface|?>>
    <associate|auto-96|<tuple|Entropy|?>>
    <associate|auto-97|<tuple|entropy|?>>
    <associate|auto-98|<tuple|1.4.2|?>>
    <associate|auto-99|<tuple|Third law of thermodynamics|?>>
    <associate|dS \<gtr\>= dq/Tb|<tuple|1.6.1|?>>
    <associate|dS(res)=|<tuple|1.5.1|?>>
    <associate|dS(ss)=sum|<tuple|1.5.2|?>>
    <associate|dS\<gtr\>0 (irrev, isolated)|<tuple|1.6.6|?>>
    <associate|dS\<gtr\>int(dq/Tb)|<tuple|1.5.3|?>>
    <associate|dS=dq/Tb|<tuple|1.4.7|?>>
    <associate|dS=sum'(a)sum(b)|<tuple|1.6.8|?>>
    <associate|dS=sum(a)sum(b)|<tuple|1.6.7|?>>
    <associate|del S\<gtr\>int(dq/Tb)|<tuple|1.5.4|?>>
    <associate|del S=int(C/T)dT|<tuple|1.6.3|?>>
    <associate|delS \<gtr\>= dq/Tb|<tuple|1.6.2|?>>
    <associate|delS=int(dS)=int(dq/Tb)|<tuple|1.4.9|?>>
    <associate|delS=int(dq/Tb)|<tuple|1.4.6|?>>
    <associate|dq'=Tres(dq/Tb)|<tuple|1.4.1|?>>
    <associate|dq/T=CVdT/T+nRdV/V|<tuple|1.3.5|?>>
    <associate|dq=CVdT+(nRT/V)dV|<tuple|1.3.4|?>>
    <associate|efficiency\<less\>1|<tuple|1.3.3|?>>
    <associate|epsilon=1+qc/qh|<tuple|1.3.3|?>>
    <associate|epsilon=1-Tc/Th|<tuple|1.3.14|?>>
    <associate|fig:4-VT adiabats|<tuple|1.4.2|?>>
    <associate|fig:4-adiabatic_supersystem|<tuple|1.5.1|?>>
    <associate|fig:4-ig Carnot engine|<tuple|1.3.1|?>>
    <associate|fig:4-impossible1|<tuple|1.2.1|?>>
    <associate|fig:4-impossible2|<tuple|1.2.2|?>>
    <associate|fig:4-impossible3|<tuple|1.3.4|?>>
    <associate|fig:4-impossible4|<tuple|1.3.5|?>>
    <associate|fig:4-one cycle|<tuple|1.3.3|?>>
    <associate|fig:4-rev paths|<tuple|1.4.3|?>>
    <associate|fig:4-supersystem|<tuple|1.4.1|?>>
    <associate|fig:4-two paths|<tuple|1.7.1|?>>
    <associate|fig:4-water Carnot engine|<tuple|1.3.2|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-1.2.1|<tuple|1.2.1|?>>
    <associate|footnote-1.2.2|<tuple|1.2.2|?>>
    <associate|footnote-1.2.3|<tuple|1.2.3|?>>
    <associate|footnote-1.2.4|<tuple|1.2.4|?>>
    <associate|footnote-1.2.5|<tuple|1.2.5|?>>
    <associate|footnote-1.4.1|<tuple|1.4.1|?>>
    <associate|footnote-1.4.2|<tuple|1.4.2|?>>
    <associate|footnote-1.4.3|<tuple|1.4.3|?>>
    <associate|footnote-1.4.4|<tuple|1.4.4|?>>
    <associate|footnote-1.6.1|<tuple|1.6.1|?>>
    <associate|footnote-1.8.1|<tuple|1.8.1|?>>
    <associate|footnote-1.8.2|<tuple|1.8.2|?>>
    <associate|footnote-1.8.3|<tuple|1.8.3|?>>
    <associate|footnote-1.8.4|<tuple|1.8.4|?>>
    <associate|footnr-1.2.1|<tuple|1.2.1|?>>
    <associate|footnr-1.2.2|<tuple|1.2.2|?>>
    <associate|footnr-1.2.3|<tuple|Perpetual motion of the second kind|?>>
    <associate|footnr-1.2.4|<tuple|1.2.4|?>>
    <associate|footnr-1.2.5|<tuple|1.2.5|?>>
    <associate|footnr-1.4.1|<tuple|1.4.1|?>>
    <associate|footnr-1.4.2|<tuple|1.4.2|?>>
    <associate|footnr-1.4.3|<tuple|1.4.3|?>>
    <associate|footnr-1.4.4|<tuple|1.4.4|?>>
    <associate|footnr-1.6.1|<tuple|1.6.1|?>>
    <associate|footnr-1.8.1|<tuple|1.8.1|?>>
    <associate|footnr-1.8.2|<tuple|1.8.2|?>>
    <associate|footnr-1.8.3|<tuple|1.8.3|?>>
    <associate|footnr-1.8.4|<tuple|Clausius, Rudolf|?>>
    <associate|impossible process defn|<tuple|Process|?>>
    <associate|ln(V2/V1)=-ln(V4/V3)|<tuple|1.3.9|?>>
    <associate|meaning of ir|<tuple|1.6.1|?>>
    <associate|oint(dq(rev)/Tb)=0|<tuple|1.4.8|?>>
    <associate|oint(dq/Tb)\<less\>=0|<tuple|1.4.3|?>>
    <associate|q'(ss)=int(CD)|<tuple|1.4.5|?>>
    <associate|q'=(Tres)int(dq/Tb)|<tuple|1.4.2|?>>
    <associate|q(ss)=int(AB)|<tuple|1.4.4|?>>
    <associate|qc/qh=-Tc/Th|<tuple|1.3.13|?>>
    <associate|rev ad surface|<tuple|reversible adiabatic surface|?>>
    <associate|second law box|<tuple|mathematical statement of the second
    law|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      clausius-1854

      planck-22

      lewis-23

      hats-65

      pippard-66

      pauli-73

      adkins-83

      norton-16

      leff-77

      leff-96

      lambert-02a

      lambert-09

      howard-01
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.1>||>|<pageref|auto-24>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.2>||>|<pageref|auto-28>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.1>||>|<pageref|auto-52>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.2>||>|<pageref|auto-53>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.3>||>|<pageref|auto-57>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.4>||>|<pageref|auto-61>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.5>||>|<pageref|auto-64>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.4.1>||>|<pageref|auto-81>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.4.2>||>|<pageref|auto-98>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.4.3>||>|<pageref|auto-100>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.5.1>||>|<pageref|auto-116>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.7.1>||>|<pageref|auto-149>>
    </associate>
    <\associate|gly>
      <tuple|normal|mathematical statement of the second
      law|<pageref|auto-16>>

      <tuple|normal|entropy|<pageref|auto-18>>

      <tuple|normal|heat engine|<pageref|auto-32>>

      <tuple|normal|Carnot engine|<pageref|auto-48>>

      <tuple|normal|Carnot cycles|<pageref|auto-50>>

      <tuple|normal|Carnot heat pump|<pageref|auto-59>>

      <tuple|normal|efficiency|<pageref|auto-63>>

      <tuple|normal|thermodynamic temperature|<pageref|auto-74>>

      <tuple|normal|Clausius inequality|<pageref|auto-91>>

      <tuple|normal|reversible adiabatic surface|<pageref|auto-95>>

      <tuple|normal|entropy|<pageref|auto-97>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Spontaneous process>|<pageref|auto-3>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-4>>

      <tuple|<tuple|Reversible|process>|<pageref|auto-5>>

      <tuple|<tuple|Process|reversible>|<pageref|auto-6>>

      <tuple|<tuple|Process|impossible>|<pageref|auto-7>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-8>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-9>>

      <tuple|<tuple|Process|irreversible>|<pageref|auto-10>>

      <tuple|<tuple|Irreversible process>|<pageref|auto-11>>

      <tuple|<tuple|Process|reversible>|<pageref|auto-12>>

      <tuple|<tuple|Reversible|process>|<pageref|auto-13>>

      <tuple|<tuple|Second law of thermodynamics|mathematical
      statement>|<pageref|auto-15>>

      <tuple|<tuple|Entropy>|<pageref|auto-17>>

      <tuple|<tuple|Reversible|process>|<pageref|auto-19>>

      <tuple|<tuple|Process|reversible>|<pageref|auto-20>>

      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-21>>

      <tuple|<tuple|Kelvin, Baron of Largs>|<pageref|auto-22>>

      <tuple|<tuple|Planck, Max>|<pageref|auto-23>>

      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-25>>

      <tuple|<tuple|Clausius|statement of the second law>|<pageref|auto-26>>

      <tuple|<tuple|Second law of thermodynamics|Clausius
      statement>|<pageref|auto-27>>

      <tuple|<tuple|Joule|paddle wheel>|<pageref|auto-29>>

      <tuple|<tuple|Paddle wheel|Joule>|<pageref|auto-30>>

      <tuple|<tuple|Heat engine>|<pageref|auto-31>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-33>>

      <tuple|<tuple|Perpetual motion of the second kind>|<pageref|auto-34>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-35>>

      <tuple|<tuple|Thomson, William>|<pageref|auto-36>>

      <tuple|<tuple|Kelvin, Baron of Largs>|<pageref|auto-37>>

      <tuple|<tuple|Planck, Max>|<pageref|auto-38>>

      <tuple|<tuple|Kelvin--Planck statement of the second
      law>|<pageref|auto-39>>

      <tuple|<tuple|Second law of thermodynamics|Kelvin--Planck
      statement>|<pageref|auto-40>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-41>>

      <tuple|<tuple|Lewis, Gilbert Newton>|<pageref|auto-42>>

      <tuple|<tuple|Randall, Merle>|<pageref|auto-43>>

      <tuple|<tuple|Heat engine>|<pageref|auto-46>>

      <tuple|<tuple|Carnot|engine>|<pageref|auto-47>>

      <tuple|<tuple|Carnot|cycle>|<pageref|auto-49>>

      <tuple|<tuple|Working substance>|<pageref|auto-51>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-54>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-55>>

      <tuple|<tuple|Steam engine>|<pageref|auto-56>>

      <tuple|<tuple|Carnot|heat pump>|<pageref|auto-58>>

      <tuple|<tuple|Working substance>|<pageref|auto-65>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-66>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-67>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-68>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-70>>

      <tuple|<tuple|Kelvin, Baron of Largs>|<pageref|auto-71>>

      <tuple|<tuple|Thermodynamic|temperature>|<pageref|auto-72>>

      <tuple|<tuple|Temperature|thermodynamic>|<pageref|auto-73>>

      <tuple|<tuple|Ideal-gas temperature>|<pageref|auto-75>>

      <tuple|<tuple|Ideal-gas temperature>|<pageref|auto-76>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-77>>

      <tuple|<tuple|Supersystem>|<pageref|auto-80>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-82>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-83>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-84>>

      <tuple|<tuple|Supersystem>|<pageref|auto-85>>

      <tuple|<tuple|Supersystem>|<pageref|auto-86>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-87>>

      <tuple|<tuple|Kelvin--Planck statement of the second
      law>|<pageref|auto-88>>

      <tuple|<tuple|Second law of thermodynamics|Kelvin--Planck
      statement>|<pageref|auto-89>>

      <tuple|<tuple|Clausius|inequality>|<pageref|auto-90>>

      <tuple|<tuple|Work|coordinate>|<pageref|auto-93>>

      <tuple|<tuple|Reversible|adiabatic surface>|<pageref|auto-94>>

      <tuple|<tuple|Entropy>|<pageref|auto-96>>

      <tuple|<tuple|Third law of thermodynamics>|<pageref|auto-99>>

      <tuple|<tuple|Supersystem>|<pageref|auto-101>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-102>>

      <tuple|<tuple|Supersystem>|<pageref|auto-103>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-104>>

      <tuple|<tuple|Second law of thermodynamics|Kelvin--Planck
      statement>|<pageref|auto-105>>

      <tuple|<tuple|Integrating factor>|<pageref|auto-106>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-111>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-112>>

      <tuple|<tuple|Supersystem>|<pageref|auto-114>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-115>>

      <tuple|<tuple|Supersystem>|<pageref|auto-117>>

      <tuple|<tuple|Supersystem>|<pageref|auto-118>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-120>>

      <tuple|<tuple|Second law of thermodynamics|mathematical
      statement>|<pageref|auto-121>>

      <tuple|<tuple|Expansionreversible, of an ideal
      gas|<uninit>>|<pageref|auto-124>>

      <tuple|<tuple|Reversible|expansion of an ideal gas>|<pageref|auto-125>>

      <tuple|<tuple|Process|irreversible>|<pageref|auto-127>>

      <tuple|<tuple|Irreversible process>|<pageref|auto-128>>

      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-129>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-131>>

      <tuple|<tuple|Process|irreversible>|<pageref|auto-133>>

      <tuple|<tuple|Irreversible process>|<pageref|auto-134>>

      <tuple|<tuple|Process|adiabatic>|<pageref|auto-136>>

      <tuple|<tuple|Adiabatic|process>|<pageref|auto-137>>

      <tuple|<tuple|Reversible|process>|<pageref|auto-139>>

      <tuple|<tuple|Process|reversible>|<pageref|auto-140>>

      <tuple|<tuple|Spontaneous process>|<pageref|auto-141>>

      <tuple|<tuple|Process|spontaneous>|<pageref|auto-142>>

      <tuple|<tuple|Irreversible process>|<pageref|auto-143>>

      <tuple|<tuple|Process|irreversible>|<pageref|auto-144>>

      <tuple|<tuple|Process|mechanical>|<pageref|auto-145>>

      <tuple|<tuple|Energy|dissipation of>|<pageref|auto-146>>

      <tuple|<tuple|Dissipation of energy>|<pageref|auto-147>>

      <tuple|<tuple|Entropy>|<pageref|auto-148>>

      <tuple|<tuple|Disorder>|<pageref|auto-151>>

      <tuple|<tuple|Statistical mechanics>|<pageref|auto-152>>

      <tuple|<tuple|Microstate>|<pageref|auto-153>>

      <tuple|<tuple|Statistical mechanics>|<pageref|auto-154>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-155>>

      <tuple|<tuple|Statistical mechanics>|<pageref|auto-156>>

      <tuple|<tuple|Clausius, Rudolf>|<pageref|auto-157>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>The
      Second Law> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Types of Processes <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      1.2<space|2spc>Statements of the Second Law
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>

      1.3<space|2spc>Concepts Developed with Carnot Engines
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-44>

      <with|par-left|<quote|1tab>|1.3.1<space|2spc>Carnot engines and Carnot
      cycles <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-45>>

      <with|par-left|<quote|1tab>|1.3.2<space|2spc>The equivalence of the
      Clausius and Kelvin\UPlanck statements
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-60>>

      <with|par-left|<quote|1tab>|1.3.3<space|2spc>The efficiency of a Carnot
      engine <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-62>>

      <with|par-left|<quote|1tab>|1.3.4<space|2spc>Thermodynamic temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-69>>

      1.4<space|2spc>The Second Law for Reversible Processes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-78>

      <with|par-left|<quote|1tab>|1.4.1<space|2spc>The Clausius inequality
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-79>>

      <with|par-left|<quote|1tab>|1.4.2<space|2spc>Using reversible processes
      to define the entropy <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-92>>

      <with|par-left|<quote|1tab>|1.4.3<space|2spc>Alternative derivation of
      entropy as a state function <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-107>>

      <with|par-left|<quote|1tab>|1.4.4<space|2spc>Some properties of the
      entropy <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-108>>

      1.5<space|2spc>The Second Law for Irreversible Processes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-109>

      <with|par-left|<quote|1tab>|1.5.1<space|2spc>Irreversible adiabatic
      processes <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-110>>

      <with|par-left|<quote|1tab>|1.5.2<space|2spc>Irreversible processes in
      general <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-113>>

      1.6<space|2spc>Applications <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-119>

      <with|par-left|<quote|1tab>|1.6.1<space|2spc>Reversible heating
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-122>>

      <with|par-left|<quote|1tab>|1.6.2<space|2spc>Reversible expansion of an
      ideal gas <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-123>>

      <with|par-left|<quote|1tab>|1.6.3<space|2spc>Spontaneous changes in an
      isolated system <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-126>>

      <with|par-left|<quote|1tab>|1.6.4<space|2spc>Internal heat flow in an
      isolated system <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-130>>

      <with|par-left|<quote|1tab>|1.6.5<space|2spc>Free expansion of a gas
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-132>>

      <with|par-left|<quote|1tab>|1.6.6<space|2spc>Adiabatic process with
      work <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-135>>

      1.7<space|2spc>Summary <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-138>

      1.8<space|2spc>The Statistical Interpretation of Entropy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-150>
    </associate>
  </collection>
</auxiliary>