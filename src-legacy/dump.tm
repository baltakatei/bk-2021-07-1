<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <Ans|3|3|(b)| <math|q=-w=1.00<timesten|5><units|J>>> <Ans|3|4|(c)|
  <math|w=1.99<timesten|3><units|J>>, <math|q=-1.99<timesten|3><units|J>>.>
  <Ans|3|5|()| <math|0.0079%>> <Ans|3|6|(c)|
  <math|V<rsub|2><ra>n*R*V<rsub|1>/<around|(|C<rsub|V>+n*R|)>>,
  <math|T<rsub|2><ra>\<infty\>>. For <math|C<rsub|V>=<around|(|3/2|)>*n*R>,
  <math|V<rsub|2>/V<rsub|1><ra>0.4>.> <Ans|3|11|()|
  <math|9.58<timesten|3><units|s>> (<math|2<units|h*r>>
  <math|40<units|m*i*n>>)> <Ans|4|4|()| <math|<Del>S=0.054<units|J*<space|0.17em>K<per>>>>
  <Ans|4|5|()| <math|<Del>S=549<units|J*<space|0.17em>K<per>>> for both
  processes; <math|<big|int><space|-0.17em><dq>/T<subs|e*x*t>=333<units|J*<space|0.17em>K<per>>>
  and <math|0>.> <Ans|5|4|(a)| <math|<D>S=n*R*ln
  <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>+<around*|(|<frac|5|2>|)>*n*R>>
  <Ans|5|5|(a)| <math|q=0>, <math|w=1.50<timesten|4><units|J>>,
  <math|<Del>U=1.50<timesten|4><units|J>>,
  <math|<Del>H=2.00<timesten|4><units|J>>> <Ans|5|5|(c)|
  <math|<Del>S=66.7<units|J*<space|0.17em>K<per>>>> <Ans|6|1|()|
  <math|S<m>\<approx\>151.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>
  <Ans|7|6|(a)| <math|\<alpha\>=8.519<timesten|-4><units|K<per>>><next-line><math|\<kappa\><rsub|t>=4.671<timesten|-5><units|b*a*r<per>>><next-line><math|<pd|p|T|V>=18.24<units|b*a*r*<space|0.17em>K<per>>><next-line><math|<pd|U|V|T>=5437<br>>>
  <Ans|7|6|(b)| <math|<Del>p\<approx\>1.8<br>>> <Ans|7|7|(b)|
  <math|<pd|<Cpm>|p|T>><next-line><math|=-4.210<timesten|-8><units|J*<space|0.17em>K<per><space|0.17em>P*a<per><space|0.17em>m*o*l<per>>>>
  <Ans|7|8|(b)| <math|8<timesten|-4><units|K<per>>>> <Ans|7|11|()|
  <math|5.001<timesten|3><units|J>>> <Ans|7|12|()|
  <math|<Del>H=2.27<timesten|4><units|J>>,
  <math|<Del>S=43.6<units|J*<space|0.17em>K<per>>>> <Ans|7|13|(a)|
  <math|<Cpm><st>=42.3<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>
  <Ans|7|13|(b)| <math|<Cpm>\<approx\>52.0<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>
  <Ans|7|14|(a)| <math|2.56<units|J*<space|0.17em>K<per><space|0.17em>g<per>>>>
  <Ans|7|15|(b)| <math|<fug>=17.4<br>>> <Ans|7|16|(a)| <math|\<phi\>=0.739>,
  <math|<fug>=148<br>>> <Ans|7|16|(b)| <math|B=-7.28<timesten|-5><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>>
  <Ans|8|2|(a)| <math|S<m><st><liquid>=253.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>
  <Ans|8|2|(b)| <math|<Delsub|v*a*p>S<st>=88.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>,
  <math|<Delsub|v*a*p>H<st>=2.748<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>
  <Ans|8|4|()| <math|4.5<timesten|-3><br>>> <Ans|8|5|()|
  <math|19<units|J*<space|0.17em>m*o*l<per>>>> <Ans|8|6|(a)|
  <math|352.82<K>>> <Ans|8|6|(b)| <math|3.4154<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>
  <Ans|8|7|(a)| <math|3.62<timesten|3><units|P*a*<space|0.17em>K<per>>>>
  <Ans|8|7|(b)| <math|3.56<timesten|3><units|P*a*<space|0.17em>K<per>>>>
  <Ans|8|7|(c)| <math|99.60<units|<degC>>>> <Ans|8|8|(b)|
  <math|<Delsub|v*a*p>H<st>=4.084<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>
  <Ans|8|9|()| <math|0.93<units|m*o*l>>> <Ans|9|2|(b)|
  <math|V<A><around|(|x<B>=0.5|)>\<approx\>125.13<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>><next-line><math|V<B><around|(|x<B>=0.5|)>\<approx\>158.01<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>><next-line><math|V<B><rsup|\<infty\>>\<approx\>157.15<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>
  <Ans|9|4|()| real gas: <math|p=1.9743<br>><next-line>ideal gas:
  <math|p=1.9832<br>>> <Ans|9|5|(a)| <math|x<subs|N<rsub|<math|2>>>=8.83<timesten|-6>><next-line><math|x<subs|O<rsub|<math|2>>>=4.65<timesten|-6>><next-line><math|y<subs|N<rsub|<math|2>>>=0.763><next-line><math|y<subs|O<rsub|<math|2>>>=0.205>>
  <Ans|9|5|(b)| <math|x<subs|N<rsub|<math|2>>>=9.85<timesten|-6>><next-line><math|x<subs|O<rsub|<math|2>>>=2.65<timesten|-6>><next-line><math|y<subs|N<rsub|<math|2>>>=0.851><next-line><math|y<subs|O<rsub|<math|2>>>=0.117>>
  <Ans|9|7|(b)| <math|<fug><A>=0.03167<br>>, <math|<fug><A>=0.03040<br>>>
  <Ans|9|8|(a)| In the mixture of composition <math|x<A>=0.9782>, the
  activity coefficient is <math|<g><B>\<approx\>11.5>.> <Ans|9|9|(d)|
  <math|k<subs|H,A>\<approx\>680<units|k*P*a>>> <Ans|9|11|()| Values for
  <math|m<B>/m<st>=20>: <math|<g><A>=1.026>, <math|<g><mbB>=0.526>; the
  limiting slopes are <math|<dif><g><A>/<dif><around|(|m<B>/m<st>|)>=0>,
  <math|<dif><g><mbB>/<dif><around|(|m<B>/m<st>|)>=-0.09>> <Ans|9|13|()|
  <math|p<subs|N<rsub|<math|2>>>=0.235<br>><next-line><math|y<subs|N<rsub|<math|2>>>=0.815><next-line><math|p<subs|O<rsub|<math|2>>>=0.0532<br>><next-line><math|y<subs|O<rsub|<math|2>>>=0.185><next-line><math|p=0.288<br>>>
  <Ans|9|14|(b)| <math|h=1.2<units|m>>> <Ans|9|15|(a)|
  <math|p<around|(|7.20<units|c*m>|)>-p<around|(|6.95<units|c*m>|)>=1.2<br>>>
  <Ans|9|15|(b)| <math|M<B>=187<units|k*g*<space|0.17em>m*o*l<per>>><next-line>mass
  binding ratio <math|=1.37>> <Ans|10|2|()| <math|<g|\<pm\>>=0.392>>
  <Ans|11|1|()| <math|<Delsub|r>H<st>=-63.94<units|k*J*<space|0.17em>m*o*l<per>>><next-line><math|K=4.41<timesten|-2>>>
  <Ans|11|2|(b)| <math|<Delsub|f>H<st>>: no
  change<next-line><math|<Delsub|f>S<st>>: subtract
  <math|0.219<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>><next-line><math|<Delsub|f>G<st>>:
  add <math|65<units|J*<space|0.17em>m*o*l<per>>>> <Ans|11|3|()|
  <math|p<around|(|298.15<K>|)>=2.6<timesten|-6><br>><next-line><math|p<around|(|273.15<K>|)>=2.7<timesten|-7><br>>>
  <Ans|11|4|(a)| <math|-240.34<units|k*J*<space|0.17em>m*o*l<per>>>,
  <math|-470.36<units|k*J*<space|0.17em>m*o*l<per>>>,
  <math|-230.02<units|k*J*<space|0.17em>m*o*l<per>>>> <Ans|11|4|(b)|
  <math|-465.43<units|k*J*<space|0.17em>m*o*l<per>>>> <Ans|11|4|(c)|
  <math|-39.82<units|k*J*<space|0.17em>m*o*l<per>>>> <Ans|11|5|()|
  <math|<Del>H=0.92<units|k*J>>> <Ans|11|6|()|
  <math|L<A>=-0.405<units|J*<space|0.17em>m*o*l<per>>><next-line><math|L<B>=0.810<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|11|7|(a)| State 1:<next-line><math|n<subs|C<rsub|<math|6>>*H<rsub|<math|14>>>=7.822<timesten|-3><mol>><next-line><math|n<subs|H<rsub|<math|2>>*O>=0.05560<mol>><next-line>amount
  of O<rsub|<math|2>> consumed: <math|0.07431<mol>><next-line>State
  2:<next-line><math|n<subs|H<rsub|<math|2>>*O>=0.11035<mol>><next-line><math|n<subs|C*O<rsub|<math|2>>>=0.04693<mol>><next-line><tx|mass
  of H<rsub|<math|2>>O>=<math|1.9880<units|g>>> <Ans|11|7|(b)|
  <math|V<m><tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>=131.61<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>><next-line><math|V<m><tx|<around|(|H<rsub|<math|2>>*O|)>>=18.070<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>>
  <Ans|11|7|(c)| State 1: <math|V<tx|<around|(|C<rsub|<math|6>>*H<rsub|<math|14>>|)>>=1.029<units|c*m<rsup|<math|3>>>><next-line><math|V<tx|<around|(|H<rsub|<math|2>>*O|)>>=1.005<units|c*m<rsup|<math|3>>>><next-line><math|V<sups|g>=348.0<units|c*m<rsup|<math|3>>>><next-line>State
  2:<next-line><math|V<tx|<around|(|H<rsub|<math|2>>*O|)>>=1.994<units|c*m<rsup|<math|3>>>><next-line><math|V<sups|g>=348.0<units|c*m<rsup|<math|3>>>>>
  <Ans|11|7|(d)| State 1:<next-line><math|n<subs|O<rsub|<math|2>>>=0.429<mol>><next-line>State
  2:<next-line><math|n<subs|O<rsub|<math|2>>>=0.355<mol>><next-line><math|y<subs|O<rsub|<math|2>>>=0.883><next-line><math|y<subs|C*O<rsub|<math|2>>>=0.117>>
  <Ans|11|7|(e)| State 2:<next-line><math|p<rsub|2>=27.9<br>><next-line><math|p<subs|O<rsub|<math|2>>>=24.6<br>><next-line><math|p<subs|C*O<rsub|<math|2>>>=3.26<br>>>
  <Ans|11|7|(f)| <math|<fug><subs|H<rsub|<math|2>>*O><around|(|0.03169<br>|)>=0.03164<br>><next-line>State
  1: <math|<fug><subs|H<rsub|<math|2>>*O>=0.03234<br>><next-line>State 2:
  <math|<fug><subs|H<rsub|<math|2>>*O>=0.03229<br>>> <Ans|11|7|(g)| State
  1:<next-line><math|\<phi\><subs|H<rsub|<math|2>>*O>=0.925><next-line><math|\<phi\><subs|O<rsub|<math|2>>>=0.981><next-line><math|<fug><subs|O<rsub|<math|2>>>=29.4<br>><next-line>State
  2:<next-line><math|\<phi\><subs|H<rsub|<math|2>>*O>=0.896><next-line><math|\<phi\><subs|O<rsub|<math|2>>>=0.983><next-line><math|\<phi\><subs|C*O<rsub|<math|2>>>=0.910><next-line><math|<fug><subs|O<rsub|<math|2>>>=24.2<br>><next-line><math|<fug><subs|C*O<rsub|<math|2>>>=2.97<br>>>
  <Ans|11|7|(h)| State 1:<next-line><math|n<subs|H<rsub|<math|2>>*O><sups|g>=5.00<timesten|-4><mol>><next-line><math|n<subs|H<rsub|<math|2>>*O><sups|l>=0.05510<mol>><next-line>State
  2: <math|n<subs|H<rsub|<math|2>>*O><sups|g>=5.19<timesten|-4><mol>><next-line><math|n<subs|H<rsub|<math|2>>*O><sups|l>=0.10983<mol>>>
  <Ans|11|7|(i)| State 1:<next-line><math|k<rsub|m,<tx|O<rsub|<math|2>>>>=825<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>><next-line><math|n<subs|O<rsub|<math|2>>>=3.57<timesten|-5><mol>><next-line>State
  2:<next-line><math|k<rsub|m,<tx|O<rsub|<math|2>>>>=823<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>><next-line><math|k<rsub|m,<tx|C*O<rsub|<math|2>>>>=30.8<units|b*a*r*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>><next-line><math|n<subs|O<rsub|<math|2>>>=5.85<timesten|-5><mol>><next-line><math|n<subs|C*O<rsub|<math|2>>>=1.92<timesten|-4><mol>>>
  <Ans|11|7|(j)| H<rsub|<math|2>>O vaporization:
  <math|<Del>U=+20.8<units|J>><next-line>H<rsub|<math|2>>O condensation:
  <math|<Del>U=-21.6<units|J>>> <Ans|11|7|(k)| O<rsub|<math|2>> dissolution:
  <math|<Del>U=-0.35<units|J>><next-line>O<rsub|<math|2>> desolution:
  <math|<Del>U=0.57<units|J>><next-line>CO<rsub|<math|2>> desolution:
  <math|<Del>U=3.32<units|J>>> <Ans|11|7|(l)|
  C<rsub|<math|6>>H<rsub|<math|14>>(l) compression:
  <math|<Del>U=-1.226<units|J>><next-line>solution compression:
  <math|<Del>U=-0.225<units|J>><next-line>solution decompression:
  <math|<Del>U=0.414<units|J>>> <Ans|11|7|(m)| O<rsub|<math|2>> compression:
  <math|<Del>U=-81<units|J>><next-line>gas mixture:
  <math|<dif>B/<dif>T=0.26<timesten|-6><units|m<rsup|<math|3>>*K<per>mol<per>>><next-line>gas
  mixture expansion: <math|<Del>U=87<units|J>>> <Ans|11|7|(n)|
  <math|<Del>U=8<units|J>>> <Ans|11|7|(o)|
  <math|<Delsub|c>U<st>=-4154.4<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|11|7|(p)| <math|<Delsub|c>H<st>=-4163.1<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|11|8|()| <math|<Delsub|f>H<st>=-198.8<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|11|9|()| <math|T<rsub|2>=2272<K>>> <Ans|11|10|()|
  <math|p<tx|<around|(|O<rsub|<math|2>>|)>>=2.55<timesten|-5><br>>>
  <Ans|11|11|(a)| <math|K=3.5<timesten|41>>> <Ans|11|11|(b)|
  <math|p<subs|H<rsub|<math|2>>>=2.8<timesten|-42><br>><next-line><math|N<subs|H<rsub|<math|2>>>=6.9<timesten|-17>>>
  <Ans|11|11|(c)| <math|t=22<units|s>>> <Ans|11|12|(b)|
  <math|p\<approx\>1.5<timesten|4><br>>> <Ans|11|13|(c)| <math|K=0.15>>
  <Ans|12|1|(b)| <math|T=1168<K>><next-line><math|<Delsub|r>H<st>=1.64<timesten|5><units|J*<space|0.17em>m*o*l<per>>>>
  <Ans|12|4|()| <math|K<subs|f>=1.860<units|K*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>><next-line><math|K<bd>=0.5118<units|K*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>>
  <Ans|12|5|()| <math|M<B>\<approx\>5.6<timesten|4><units|g*<space|0.17em>m*o*l<per>>>>
  <Ans|12|6|()| <math|<Delsub|s*o*l,B>H<st>/<tx|k*J*<space|0.17em>m*o*l<per>>=-3.06,0,6.35><next-line><math|<Delsub|s*o*l,B>S<st>/<tx|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>><next-line><math|<phantom|m*m>=-121.0,-110.2,-88.4>>
  <Ans|12|7|(a)| <math|m<rsub|+><aph>=m<rsub|-><aph>=1.20<timesten|-3><units|m*o*l*<space|0.17em>k*g<per>>><next-line><math|m<rsub|+><bph>=1.80<timesten|-3><units|m*o*l*<space|0.17em>k*g<per>>><next-line><math|m<rsub|-><bph>=0.80<timesten|-3><units|m*o*l*<space|0.17em>k*g<per>>><next-line><math|m<subs|P>=2.00<timesten|-6><units|m*o*l*<space|0.17em>k*g<per>>>>
  <Ans|12|8|(a)| <math|p<sups|l>=2.44<br>>> <Ans|12|8|(b)|
  <math|<fug><around|(|2.44<br>|)>-<fug><around|(|1.00<br>|)>><next-line><math|<phantom|m*m>=3.4<timesten|-5><br>>>
  <Ans|12|10|(a)| <math|x<B>=1.8<timesten|-7>><next-line><math|m<B>=1.0<timesten|-5><units|m*o*l*<space|0.17em>k*g<per>>>>
  <Ans|12|10|(b)| <math|<Delsub|s*o*l,B>H<st>=-1.99<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>
  <Ans|12|10|(c)| <math|K=4.4<timesten|-7>><next-line><math|<Delsub|r>H<st>=9.3<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|12|13|(a)| <math|p=92399.6<Pa>>, <math|y<B>=0.965724>> <Ans|12|13|(b)|
  <math|\<phi\><A>=0.995801>> <Ans|12|13|(c)| <math|<fug><A>=3164.47<Pa>>>
  <Ans|12|13|(d)| <math|y<B>=0.965608>> <Ans|12|13|(e)| <math|Z=0.999319>>
  <Ans|12|13|(f)| <math|p=92347.7<Pa>>> <Ans|12|13|(g)|
  <math|<kHB>=4.40890<timesten|9><Pa>>> <Ans|12|15|(a)|
  <math|<g><xbB>=0.9826>> <Ans|12|15|(b)| <math|x<B>=4.19<timesten|-4>>>
  <Ans|12|16|()| <math|K=1.2<timesten|-6>>> <Ans|12|17|(a)|
  <math|\<alpha\>=0.129><next-line><math|m<rsub|+>=1.29<timesten|-3><units|m*o*l*<space|0.17em>k*g<per>>>>
  <Ans|12|17|(b)| <math|\<alpha\>=0.140>> <Ans|12|18|()|
  <math|<Delsub|f>H<st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>=-167.15<units|k*J*<space|0.17em>m*o*l<per>>><next-line><math|S<m><st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>=56.46<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>
  <Ans|12|19|(a)| <math|K<subs|s>=1.783<timesten|-10>>> <Ans|12|20|(a)|
  <math|<Delsub|r>H<st>=-65.769<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|12|20|(b)| <math|<Delsub|f>H<st><tx|<around|(|A*g<rsup|<math|+>>,<space|0.17em>aq|)>>=105.84<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|13|1|(a)| <math|F=4>> <Ans|13|1|(b)| <math|F=3>> <Ans|13|1|(c)|
  <math|F=2>> <Ans|13|10|(a)| <math|x<B><tx|<around|(|t*o*p|)>>=0.02>,
  <math|x<B><tx|<around|(|b*o*t*t*o*m|)>>=0.31>> <Ans|13|10|(b)|
  <math|n<A>=2.1<mol>>, <math|n<B>=1.0<mol>>> <Ans|14|3|(a)|
  <math|<Delsub|r>G<st>=-21.436<units|k*J*<space|0.17em>m*o*l<per>>><next-line><math|<Delsub|r>S<st>=-62.35<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>><next-line><math|<Delsub|r>H<st>=-40.03<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|14|3|(b)| <math|<Delsub|f>H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-127.05<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|14|3|(c)| <math|S<m><st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=96.16<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>><next-line><math|<Delsub|f>S<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-57.93><units|J<space|0.17em>K<per><space|0.17em>mol<per>><next-line><math|<Delsub|f>G<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-109.78<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|14|4|(b)| <math|<Delsub|f>H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-126.81<units|k*J*<space|0.17em>m*o*l<per>>><next-line><math|<Delsub|f>G<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-109.59<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|14|5|()| <math|K<subs|s>=1.76<timesten|-10>>> <Ans|14|6|(b)|
  <math|<g><rsub|\<pm\>>=0.756>> <Ans|14|7|(b)|
  <math|<Delsub|f>G<st>=-210.72<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|14|7|(c)| <math|K<subs|s>=1.4<timesten|-18>>> <Ans|14|8|()|
  <math|E<st>=0.071<V>>> <Ans|14|9|(c)| <math|<Eeq><st>=1.36<V>>>
  <Ans|14|9|(d)| In the cell:<next-line><math|<dq>/<dif>\<xi\>=2.27<units|k*J*<space|0.17em>m*o*l<per>>><next-line>In
  a reaction vessel:<next-line><math|<dq>/<dif>\<xi\>=-259.67<units|k*J*<space|0.17em>m*o*l<per>>>>
  <Ans|14|9|(e)| <math|<dif><Eeq><st>/<dif>T=3.9<timesten|-5><units|V*<space|0.17em>K<per>>>>
  </body>

<initial|<\collection>
</collection>>