<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <new-page><label|epigraphs><new-page><thispagestyle|empty>

  <pdfbookmark*|0|Epigraphs|e>

  <\epigraphs>
    <qitem|A theory is the more impressive the greater the simplicity of its
    premises is, the more different kinds of things it relates, and the more
    extended is its area of applicability. Therefore the deep impression
    which classical thermodynamics made upon me. It is the only physical
    theory of universal content concerning which I am convinced that, within
    the framework of the applicability of its basic concepts, it will never
    be overthrown.>Albert Einstein

    <vspace*|baselineskip><qitem|Thermodynamics is a discipline that involves
    a formalization of a large number of intuitive concepts derived from
    common experience.>J. G. Kirkwood and I. Oppenheim, <em|Chemical
    Thermodynamics>, 1961

    <vspace*|baselineskip><qitem|The first law of thermodynamics is nothing
    more than the principle of the conservation of energy applied to
    phenomena involving the production or absorption of heat.>Max Planck,
    <em|Treatise on Thermodynamics>, 1922

    <vspace*|baselineskip><qitem|The law that entropy always
    increases<emdash>the second law of thermodynamics<emdash>holds, I think,
    the supreme position among the laws of Nature. If someone points out to
    you that your pet theory of the universe is in disagreement with
    Maxwell's equations<emdash>then so much the worse for Maxwell's
    equations. If it is found to be contradicted by observation<emdash>well,
    these experimentalists do bungle things sometimes. But if your theory is
    found to be against the second law of thermodynamics I can give you no
    hope; there is nothing for it but to collapse in deepest humiliation.>Sir
    Arthur Eddington, <em|The Nature of the Physical World>, 1928

    <vspace*|baselineskip><qitem|Thermodynamics is a collection of useful
    relations between quantities, every one of which is independently
    measurable. What do such relations ``tell one'' about one's system, or in
    other words what do we learn from thermodynamics about the microscopic
    explanations of macroscopic changes? Nothing whatever. What then is the
    <em|use> of thermodynamics? Thermodynamics is useful precisely because
    some quantities are easier to measure than others, and that is all.>M. L.
    McGlashan, <em|J. Chem. Educ.>, <with|font-series|bold|43>, 226--232
    (1966)

    <vspace*|baselineskip><qitem|``When <em|I> use a word,'' Humpty Dumpty
    said, in rather a scornful tone, ``it means just what I choose it to
    mean<emdash>neither more nor less.''<next-line><space|.4cm>``The question
    is,'' said Alice,``whether you <em|can> make words mean so many different
    things.''<next-line><space|.4cm>``The question is,'' said Humpty Dumpty,
    ``which is to be master<emdash> that's all.''>Lewis Carroll, <em|Through
    the Looking-Glass>
  </epigraphs>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|epigraphs|<tuple|?|?>>
  </collection>
</references>