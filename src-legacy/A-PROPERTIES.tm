<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <appendix|Standard Molar Thermodynamic Properties><label|app:props>

  <index-complex|<tuple|standard molar|properties, values of>||app:props
  idx1|<tuple|Standard molar|properties, values of>>The values in this table
  are for a temperature of <math|298.15 <text|K>> (<math|25.00 <degC>>) and
  the standard pressure <math|p<st>=1 <text|bar>>. Solute standard states are
  based on molality. A crystalline solid is denoted by cr.

  Most of the values in this table come from a project of the
  <index|CODATA>Committee on Data for Science and Technology (CODATA) to
  establish a set of recommended, internally consistent values of
  thermodynamic properties. The values of <math|<Delsub|f>H<st>> and
  <math|S<m><st>> shown with uncertainties are values recommended by
  CODATA.<footnote|Ref. <cite|cox-89>; also available online at
  <slink|http://www.codata.info/resources/databases/key1.html>.>

  <\longtable|c|l r@c@l r@c@l r@c@l @>
    <\tformat>
      <\table|<row|<cell|<hline><multicolumn|1|@l|Species>>|<cell|<multicolumn|3|c|<math|<D><frac|<Delsub|f>H<st>|<tx|k*J*<space|0.17em>m*o*l<per>>>>>>|<cell|<multicolumn|3|c|<math|<D><frac|S<m><st>|<tx|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>>>|<cell|<multicolumn|3|c|<math|<D><frac|<Delsub|f>G<st>|<tx|k*J*<space|0.17em>m*o*l<per>>>>>
      <rule*|-4mm|0in|10mm>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|0.5ex>
      <hline>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|-1.5ex>
      <endfirsthead><multicolumn|10|@l|<with|font-size|0.84|(continued from
      previous page)>>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|0.5ex>
      <hline><multicolumn|1|@l|Species>>|<cell|<multicolumn|3|c|<math|<D><frac|<Delsub|f>H<st>|<tx|k*J*<space|0.17em>m*o*l<per>>>>>>|<cell|<multicolumn|3|c|<math|<D><frac|S<m><st>|<tx|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>>>|<cell|<multicolumn|3|c|<math|<D><frac|<Delsub|f>G<st>|<tx|k*J*<space|0.17em>m*o*l<per>>>>>
      <rule*|-4mm|0in|10mm>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|0.5ex>
      <hline>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|-1.5ex>
      <endhead>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|-3.4mm>
      <hline><endfoot>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|<vspace*|-3.8ex>
      <multicolumn|4|@l|<em|Inorganic substances>>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>|<row|<cell|Ag(cr)>|<cell|0>|<cell|>|<cell|>|<cell|42>|<cell|.>|<cell|<math|55\<pm\>0.20>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|AgCl(cr)>|<cell|<math|-127>>|<cell|.>|<cell|<math|01\<pm\>0.05>>|<cell|96>|<cell|.>|<cell|<math|25\<pm\>0.20>>|<cell|<math|-109>>|<cell|.>|<cell|<math|77>>>|<row|<cell|C(cr,<space|0.17em>graphite)>|<cell|0>|<cell|>|<cell|>|<cell|5>|<cell|.>|<cell|<math|74\<pm\>0.10>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|CO(g)>|<cell|<math|-110>>|<cell|.>|<cell|<math|53\<pm\>0.17>>|<cell|197>|<cell|.>|<cell|<math|660\<pm\>0.004>>|<cell|<math|-137>>|<cell|.>|<cell|17>>|<row|<cell|CO<rsub|<math|2>>(g)>|<cell|<math|-393>>|<cell|.>|<cell|<math|51\<pm\>0.13>>|<cell|213>|<cell|.>|<cell|<math|785\<pm\>0.010>>|<cell|<math|-394>>|<cell|.>|<cell|41>>|<row|<cell|Ca(cr)>|<cell|0>|<cell|>|<cell|>|<cell|41>|<cell|.>|<cell|<math|59\<pm\>0.40>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|CaCO<rsub|<math|3>>(cr,<space|0.17em>calcite)>|<cell|<math|-1206>>|<cell|.>|<cell|9>|<cell|92>|<cell|.>|<cell|9>|<cell|<math|-1128>>|<cell|.>|<cell|8>>|<row|<cell|CaO(cr)>|<cell|<math|-634>>|<cell|.>|<cell|<math|92\<pm\>0.90>>|<cell|38>|<cell|.>|<cell|<math|1\<pm\>0.4>>|<cell|<math|-603>>|<cell|.>|<cell|31>>|<row|<cell|Cl<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|223>|<cell|.>|<cell|<math|081\<pm\>0.010>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|F<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|202>|<cell|.>|<cell|<math|791\<pm\>0.005>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|H<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|130>|<cell|.>|<cell|<math|680\<pm\>0.003>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|HCl(g)>|<cell|<math|-92>>|<cell|.>|<cell|<math|31\<pm\>0.10>>|<cell|186>|<cell|.>|<cell|<math|902\<pm\>0.005>>|<cell|<math|-95>>|<cell|.>|<cell|30>>|<row|<cell|HF(g)>|<cell|<math|-273>>|<cell|.>|<cell|<math|30\<pm\>0.70>>|<cell|173>|<cell|.>|<cell|<math|779\<pm\>0.003>>|<cell|<math|-275>>|<cell|.>|<cell|40>>|<row|<cell|HI(g)>|<cell|26>|<cell|.>|<cell|<math|50\<pm\>0.10>>|<cell|206>|<cell|.>|<cell|<math|590\<pm\>0.004>>|<cell|1>|<cell|.>|<cell|70>>|<row|<cell|H<rsub|<math|2>>O(l)>|<cell|<math|-285>>|<cell|.>|<cell|<math|830\<pm\>0.040>>|<cell|69>|<cell|.>|<cell|<math|95\<pm\>0.03>>|<cell|<math|-237>>|<cell|.>|<cell|16>>|<row|<cell|H<rsub|<math|2>>O(g)>|<cell|<math|-241>>|<cell|.>|<cell|<math|826\<pm\>0.040>>|<cell|188>|<cell|.>|<cell|<math|835\<pm\>0.010>>|<cell|<math|-228>>|<cell|.>|<cell|58>>|<row|<cell|H<rsub|<math|2>>S(g)>|<cell|<math|-20>>|<cell|.>|<cell|<math|6\<pm\>0.5>>|<cell|205>|<cell|.>|<cell|<math|81\<pm\>0.05>>|<cell|<math|-33>>|<cell|.>|<cell|44>>|<row|<cell|Hg(l)>|<cell|0>|<cell|>|<cell|>|<cell|75>|<cell|.>|<cell|<math|90\<pm\>0.12>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|Hg(g)>|<cell|61>|<cell|.>|<cell|<math|38\<pm\>0.04>>|<cell|174>|<cell|.>|<cell|<math|971\<pm\>0.005>>|<cell|31>|<cell|.>|<cell|84>>|<row|<cell|HgO(cr,<space|0.17em>red)>|<cell|<math|-90>>|<cell|.>|<cell|<math|79\<pm\>0.12>>|<cell|70>|<cell|.>|<cell|<math|25\<pm\>0.30>>|<cell|<math|-58>>|<cell|.>|<cell|54>>|<row|<cell|Hg<rsub|<math|2>>Cl<rsub|<math|2>>(cr)>|<cell|<math|-265>>|<cell|.>|<cell|<math|37\<pm\>0.40>>|<cell|191>|<cell|.>|<cell|<math|6\<pm\>0.8>>|<cell|<math|-210>>|<cell|.>|<cell|72>>|<row|<cell|I<rsub|<math|2>>(cr)>|<cell|0>|<cell|>|<cell|>|<cell|116>|<cell|.>|<cell|<math|14\<pm\>0.30>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|K(cr)>|<cell|0>|<cell|>|<cell|>|<cell|64>|<cell|.>|<cell|<math|68\<pm\>0.20>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|KI(cr)>|<cell|<math|-327>>|<cell|.>|<cell|90>|<cell|106>|<cell|.>|<cell|37>|<cell|<math|-323>>|<cell|.>|<cell|03>>|<row|<cell|KOH(cr)>|<cell|<math|-424>>|<cell|.>|<cell|72>|<cell|78>|<cell|.>|<cell|90>|<cell|<math|-378>>|<cell|.>|<cell|93>>|<row|<cell|N<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|191>|<cell|.>|<cell|<math|609\<pm\>0.004>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|NH<rsub|<math|3>>(g)>|<cell|<math|-45>>|<cell|.>|<cell|<math|94\<pm\>0.35>>|<cell|192>|<cell|.>|<cell|<math|77\<pm\>0.05>>|<cell|<math|-16>>|<cell|.>|<cell|41>>|<row|<cell|NO<rsub|<math|2>>(g)>|<cell|33>|<cell|.>|<cell|10>|<cell|240>|<cell|.>|<cell|04>|<cell|51>|<cell|.>|<cell|22>>|<row|<cell|N<rsub|<math|2>>O<rsub|<math|4>>(g)>|<cell|9>|<cell|.>|<cell|08>|<cell|304>|<cell|.>|<cell|38>|<cell|97>|<cell|.>|<cell|72>>|<row|<cell|Na(cr)>|<cell|0>|<cell|>|<cell|>|<cell|51>|<cell|.>|<cell|<math|30\<pm\>0.20>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|NaCl(cr)>|<cell|<math|-411>>|<cell|.>|<cell|12>|<cell|72>|<cell|.>|<cell|11>|<cell|<math|-384>>|<cell|.>|<cell|02>>|<row|<cell|O<rsub|<math|2>>(g)>|<cell|0>|<cell|>|<cell|>|<cell|205>|<cell|.>|<cell|<math|152\<pm\>0.005>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|O<rsub|<math|3>>(g)>|<cell|142>|<cell|.>|<cell|67>|<cell|238>|<cell|.>|<cell|92>|<cell|163>|<cell|.>|<cell|14>>|<row|<cell|P(cr,<space|0.17em>white)>|<cell|0>|<cell|>|<cell|>|<cell|41>|<cell|.>|<cell|<math|09\<pm\>0.25>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|S(cr,<space|0.17em>rhombic)>|<cell|0>|<cell|>|<cell|>|<cell|32>|<cell|.>|<cell|<math|054\<pm\>0.050>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|SO<rsub|<math|2>>(g)>|<cell|<math|-296>>|<cell|.>|<cell|<math|81\<pm\>0.20>>|<cell|248>|<cell|.>|<cell|<math|223\<pm\>0.050>>|<cell|<math|-300>>|<cell|.>|<cell|09>>|<row|<cell|Si(cr)>|<cell|0>|<cell|>|<cell|>|<cell|18>|<cell|.>|<cell|<math|81\<pm\>0.08>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|SiF<rsub|<math|4>>(g)>|<cell|<math|-1615>>|<cell|.>|<cell|<math|0\<pm\>0.8>>|<cell|282>|<cell|.>|<cell|<math|76\<pm\>0.50>>|<cell|<math|-1572>>|<cell|.>|<cell|8>>|<row|<cell|SiO<rsub|<math|2>>(cr,<space|0.17em><math|\<alpha\>>-quartz)>|<cell|<math|-910>>|<cell|.>|<cell|<math|7\<pm\>1.0>>|<cell|41>|<cell|.>|<cell|<math|46\<pm\>0.20>>|<cell|<math|-856>>|<cell|.>|<cell|3>>|<row|<cell|<vspace*|1mm>
      Zn(cr)>|<cell|0>|<cell|>|<cell|>|<cell|41>|<cell|.>|<cell|<math|63\<pm\>0.15>>|<cell|0>|<cell|>|<cell|>>|<row|<cell|<vspace*|1mm>
      ZnO(cr)>|<cell|<math|-350>>|<cell|.>|<cell|<math|46\<pm\>0.27>>|<cell|43>|<cell|.>|<cell|<math|65\<pm\>0.40>>|<cell|<math|-320>>|<cell|.>|<cell|48>>>
        <\row>
          <\cell>
            <vspace*|1mm>

            <hline><multicolumn|4|@l|<em|Organic compounds>> <tablestrut>
          </cell>
        </row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>
      <|table|<row|<cell|CH<rsub|<math|4>>(g)>|<cell|<math|-74>>|<cell|.>|<cell|87>|<cell|186>|<cell|.>|<cell|25>|<cell|<math|-50>>|<cell|.>|<cell|77>>|<row|<cell|CH<rsub|<math|3>>OH(l)>|<cell|<math|-238>>|<cell|.>|<cell|9>|<cell|127>|<cell|.>|<cell|2>|<cell|<math|-166>>|<cell|.>|<cell|6>>|<row|<cell|CH<rsub|<math|3>>CH<rsub|<math|2>>OH(l)>|<cell|<math|-277>>|<cell|.>|<cell|0>|<cell|159>|<cell|.>|<cell|9>|<cell|<math|-173>>|<cell|.>|<cell|8>>|<row|<cell|C<rsub|<math|2>>H<rsub|<math|2>>(g)>|<cell|226>|<cell|.>|<cell|73>|<cell|200>|<cell|.>|<cell|93>|<cell|209>|<cell|.>|<cell|21>>|<row|<cell|C<rsub|<math|2>>H<rsub|<math|4>>(g)>|<cell|52>|<cell|.>|<cell|47>|<cell|219>|<cell|.>|<cell|32>|<cell|68>|<cell|.>|<cell|43>>|<row|<cell|C<rsub|<math|2>>H<rsub|<math|6>>(g)>|<cell|<math|-83>>|<cell|.>|<cell|85>|<cell|229>|<cell|.>|<cell|6>|<cell|<math|-32>>|<cell|.>|<cell|00>>|<row|<cell|C<rsub|<math|3>>H<rsub|<math|8>>(g)>|<cell|<math|-104>>|<cell|.>|<cell|7>|<cell|270>|<cell|.>|<cell|31>|<cell|<math|-24>>|<cell|.>|<cell|3>>|<row|<cell|C<rsub|<math|6>>H<rsub|<math|6>>(l,<space|0.17em>benzene)>|<cell|49>|<cell|.>|<cell|04>|<cell|173>|<cell|.>|<cell|26>|<cell|124>|<cell|.>|<cell|54>>>
        <\row>
          <\cell>
            <vspace*|1mm>

            <hline><multicolumn|4|@l|<em|Ionic solutes>> <tablestrut>
          </cell>
        </row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>
      </table|<row|<cell|Ag<rsup|<math|+>>(aq)>|<cell|105>|<cell|.>|<cell|<math|79\<pm\>0.08>>|<cell|73>|<cell|.>|<cell|<math|45\<pm\>0.40>>|<cell|77>|<cell|.>|<cell|10>>|<row|<cell|CO<math|<rsub|3><rsup|2->>(aq)>|<cell|<math|-675>>|<cell|.>|<cell|<math|23\<pm\>0.25>>|<cell|<math|-50>>|<cell|.>|<cell|<math|0\<pm\>1.0>>|<cell|<math|-527>>|<cell|.>|<cell|90>>|<row|<cell|Ca<rsup|<math|2+>>(aq)>|<cell|<math|-543>>|<cell|.>|<cell|<math|0\<pm\>1.0>>|<cell|<math|-56>>|<cell|.>|<cell|<math|2\<pm\>1.0>>|<cell|<math|-552>>|<cell|.>|<cell|8>>|<row|<cell|Cl<rsup|<math|->>(aq)>|<cell|<math|-167>>|<cell|.>|<cell|<math|08\<pm\>0.10>>|<cell|56>|<cell|.>|<cell|<math|60\<pm\>0.20>>|<cell|<math|-131>>|<cell|.>|<cell|22>>|<row|<cell|F<rsup|<math|->>(aq)>|<cell|<math|-335>>|<cell|.>|<cell|<math|35\<pm\>0.65>>|<cell|<math|-13>>|<cell|.>|<cell|<math|8\<pm\>0.8>>|<cell|<math|-281>>|<cell|.>|<cell|52>>|<row|<cell|H<rsup|<math|+>>(aq)>|<cell|0>|<cell|>|<cell|>|<cell|0>|<cell|>|<cell|>|<cell|0>|<cell|>|<cell|>>|<row|<cell|HCO<math|<rsub|3><rsup|->>(aq)>|<cell|<math|-689>>|<cell|.>|<cell|<math|93\<pm\>2.0>>|<cell|98>|<cell|.>|<cell|<math|4\<pm\>0.5>>|<cell|<math|-586>>|<cell|.>|<cell|90>>|<row|<cell|HS<rsup|<math|->>(aq)>|<cell|<math|-16>>|<cell|.>|<cell|<math|3\<pm\>1.5>>|<cell|67>|<cell|>|<cell|<math|\<pm\><space|0.17em>5>>|<cell|12>|<cell|.>|<cell|2>>|<row|<cell|HSO<math|<rsub|4><rsup|->>(aq)>|<cell|<math|-886>>|<cell|.>|<cell|<math|9\<pm\>1.0>>|<cell|131>|<cell|.>|<cell|<math|7\<pm\>3.0>>|<cell|<math|-755>>|<cell|.>|<cell|4>>|<row|<cell|Hg<math|<rsub|2><rsup|2+>>(aq)>|<cell|166>|<cell|.>|<cell|<math|87\<pm\>0.50>>|<cell|65>|<cell|.>|<cell|<math|74\<pm\>0.80>>|<cell|153>|<cell|.>|<cell|57>>|<row|<cell|I<rsup|<math|->>(aq)>|<cell|<math|-56>>|<cell|.>|<cell|<math|78\<pm\>0.05>>|<cell|106>|<cell|.>|<cell|<math|45\<pm\>0.30>>|<cell|<math|-51>>|<cell|.>|<cell|72>>|<row|<cell|K<rsup|<math|+>>(aq)>|<cell|<math|-252>>|<cell|.>|<cell|<math|14\<pm\>0.08>>|<cell|101>|<cell|.>|<cell|<math|20\<pm\>0.20>>|<cell|<math|-282>>|<cell|.>|<cell|52>>|<row|<cell|NH<math|<rsub|4><rsup|+>>(aq)>|<cell|<math|-133>>|<cell|.>|<cell|<math|26\<pm\>0.25>>|<cell|111>|<cell|.>|<cell|<math|17\<pm\>0.40>>|<cell|<math|-79>>|<cell|.>|<cell|40>>|<row|<cell|NO<math|<rsub|3><rsup|->>(aq)>|<cell|<math|-206>>|<cell|.>|<cell|<math|85\<pm\>0.40>>|<cell|146>|<cell|.>|<cell|<math|70\<pm\>0.40>>|<cell|<math|-110>>|<cell|.>|<cell|84>>|<row|<cell|Na<rsup|<math|+>>(aq)>|<cell|<math|-240>>|<cell|.>|<cell|<math|34\<pm\>0.06>>|<cell|58>|<cell|.>|<cell|<math|45\<pm\>0.15>>|<cell|<math|-261>>|<cell|.>|<cell|90>>|<row|<cell|OH<rsup|<math|->>(aq)>|<cell|<math|-230>>|<cell|.>|<cell|<math|015\<pm\>0.040>>|<cell|<math|-10>>|<cell|.>|<cell|<math|90\<pm\>0.20>>|<cell|<math|-157>>|<cell|.>|<cell|24>>|<row|<cell|S<rsup|<math|2->>(aq)>|<cell|33>|<cell|.>|<cell|1>|<cell|<math|-14>>|<cell|.>|<cell|6>|<cell|86>|<cell|.>|<cell|0>>|<row|<cell|SO<math|<rsub|4><rsup|2->>(aq)>|<cell|<math|-909>>|<cell|.>|<cell|<math|34\<pm\>0.40>>|<cell|18>|<cell|.>|<cell|<math|50\<pm\>0.40>>|<cell|<math|-744>>|<cell|.>|<cell|00>>|<row|<cell|<new-line>>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>>
    </tformat>
  </longtable>

  <index-complex|<tuple|standard molar|properties, values of>||app:props
  idx1|<tuple|Standard molar|properties, values of>>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|app:props|<tuple|A|?>>
    <associate|auto-1|<tuple|A|?>>
    <associate|auto-2|<tuple|<tuple|standard molar|properties, values of>|?>>
    <associate|auto-3|<tuple|CODATA|?>>
    <associate|auto-4|<tuple|<tuple|standard molar|properties, values of>|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnr-1|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      cox-89
    </associate>
    <\associate|idx>
      <tuple|<tuple|CODATA>|<pageref|auto-2>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Standard
      Molar Thermodynamic Properties> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>