<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|Symbols for Physical Quantities><label|app:sym>

  <pagestyle|nosectioninheader>

  <I|Symbols for physical quantities\|(><I|Physical quantities, symbols
  for\|(>This appendix lists the symbols for most of the variable physical
  quantities used in this book. The symbols are those recommended in the
  IUPAC Green Book (Ref. <cite|greenbook-2>) except for quantities followed
  by an asterisk (<rsup|<math|\<ast\>>>).

  <\big-table>
    <tformat|<table|<row|<cell|<hline><multicolumn|1|@l|Symbol>>|<cell|Physical
    quantity>|<cell|SI unit <tablestrut>>>|<row|<cell|<vspace*|0.5ex>
    <hline>>|<cell|>|<cell|>>|<row|<cell|<vspace*|-1.5ex>
    <endfirsthead><multicolumn|3|@l|<with|font-size|0.84|(continued from
    previous page)>>>|<cell|>|<cell|>>|<row|<cell|<vspace*|0.5ex>
    <hline><multicolumn|1|@l|Symbol>>|<cell|Physical quantity>|<cell|SI unit
    <tablestrut>>>|<row|<cell|<vspace*|0.5ex>
    <hline>>|<cell|>|<cell|>>|<row|<cell|<vspace*|-1.5ex>
    <endhead>>|<cell|>|<cell|>>|<row|<cell|<vspace*|-3.4mm>
    <hline><endfoot><multicolumn|2|@l|<em|Roman
    letters>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|A>>|<cell|Helmholtz
    energy>|<cell|J>>|<row|<cell|<math|A<subs|r>>>|<cell|relative atomic mass
    (atomic weight)>|<cell|(dimensionless)>>|<row|<cell|<math|<As>>>|<cell|surface
    area>|<cell|m<rsup|<math|2>>>>|<row|<cell|<math|a>>|<cell|activity>|<cell|(dimensionless)>>|<row|<cell|<math|B>>|<cell|second
    virial coefficient>|<cell|m<rsup|<math|3>><space|0.17em>mol<per>>>|<row|<cell|<math|C>>|<cell|number
    of components<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|C<rsub|p>>>|<cell|heat
    capacity at constant pressure>|<cell|J<space|0.17em>K<per>>>|<row|<cell|<math|C<rsub|V>>>|<cell|heat
    capacity at constant volume>|<cell|J<space|0.17em>K<per>>>|<row|<cell|<math|c>>|<cell|concentration>|<cell|mol<space|0.17em>m<rsup|<math|-3>>>>|<row|<cell|<math|E>>|<cell|energy>|<cell|J>>|<row|<cell|<no-page-break>>|<cell|electrode
    potential>|<cell|V>>|<row|<cell|<math|\<b-E\>>>|<cell|electric field
    strength>|<cell|V<space|0.17em>m<per>>>|<row|<cell|<math|E>>|<cell|cell
    potential>|<cell|V>>|<row|<cell|<math|<Ej>>>|<cell|liquid junction
    potential>|<cell|V>>|<row|<cell|<math|E<sys>>>|<cell|system energy in a
    lab frame>|<cell|J>>|<row|<cell|<math|F>>|<cell|force>|<cell|N>>|<row|<cell|<no-page-break>>|<cell|number
    of degrees of freedom<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|<fug>>>|<cell|fugacity>|<cell|Pa>>|<row|<cell|<math|g>>|<cell|acceleration
    of free fall>|<cell|m<space|0.17em>s<rsup|<math|-2>>>>|<row|<cell|<math|G>>|<cell|Gibbs
    energy>|<cell|J>>|<row|<cell|<math|h>>|<cell|height,
    elevation>|<cell|m>>|<row|<cell|<math|H>>|<cell|enthalpy>|<cell|J>>|<row|<cell|<math|\<b-H\>>>|<cell|magnetic
    field strength>|<cell|A<space|0.17em>m<per>>>|<row|<cell|<math|I>>|<cell|electric
    current>|<cell|A>>|<row|<cell|<math|I<rsub|m>>>|<cell|ionic strength,
    molality basis>|<cell|mol<space|0.17em>kg<per>>>|<row|<cell|<math|I<rsub|c>>>|<cell|ionic
    strength, concentration basis>|<cell|mol<space|0.17em>m<rsup|<math|-3>>>>|<row|<cell|<math|K>>|<cell|thermodynamic
    equilibrium constant>|<cell|(dimensionless)>>|<row|<cell|<math|K<subs|a>>>|<cell|acid
    dissociation constant>|<cell|(dimensionless)>>|<row|<cell|<math|K<rsub|p>>>|<cell|equilibrium
    constant, pressure basis>|<cell|Pa<rsup|<math|<big|sum>\<nu\>>>>>|<row|<cell|<math|K<subs|s>>>|<cell|solubility
    product>|<cell|(dimensionless)>>|<row|<cell|<math|<kHi>>>|<cell|Henry's
    law constant of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>mole
    fraction basis>|<cell|Pa>>|<row|<cell|<math|k<rsub|c,i>>>|<cell|Henry's
    law constant of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>concentration
    basis<rsup|<math|\<ast\>>>>|<cell|Pa<space|0.17em>m<rsup|<math|3>><space|0.17em>mol<per>>>|<row|<cell|<math|k<rsub|m,i>>>|<cell|Henry's
    law constant of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>molality
    basis<rsup|<math|\<ast\>>>>|<cell|Pa<space|0.17em>kg<space|0.17em>mol<per>>>|<row|<cell|<math|l>>|<cell|length,
    distance>|<cell|m>>|<row|<cell|<math|L>>|<cell|relative partial molar
    enthalpy<rsup|<math|\<ast\>>>>|<cell|J<space|0.17em>mol<per>>>|<row|<cell|<math|M>>|<cell|molar
    mass>|<cell|kg<space|0.17em>mol<per>>>|<row|<cell|<math|\<b-M\>>>|<cell|magnetization>|<cell|A<space|0.17em>m<per>>>|<row|<cell|<math|M<subs|r>>>|<cell|relative
    molecular mass (molecular weight)>|<cell|(dimensionless)>>|<row|<cell|<math|m>>|<cell|mass>|<cell|kg>>|<row|<cell|<math|m<rsub|i>>>|<cell|molality
    of species <math|i>>|<cell|mol<space|0.17em>kg<per>>>|<row|<cell|<math|N>>|<cell|number
    of entities (molecules, atoms, ions,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>formula
    units, etc.)>|<cell|(dimensionless)>>|<row|<cell|<math|n>>|<cell|amount
    of substance>|<cell|mol>>|<row|<cell|<math|P>>|<cell|number of
    phases<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|p>>|<cell|pressure>|<cell|Pa>>|<row|<cell|<no-page-break>>|<cell|partial
    pressure>|<cell|Pa>>|<row|<cell|<math|\<b-P\>>>|<cell|dielectric
    polarization>|<cell|C<space|0.17em>m<rsup|<math|-2>>>>|<row|<cell|<math|Q>>|<cell|electric
    charge>|<cell|C>>|<row|<cell|<math|Q<sys>>>|<cell|charge entering system
    at right conductor<rsup|<math|\<ast\>>>>|<cell|C>>|<row|<cell|<math|Q<subs|r*x*n>>>|<cell|reaction
    quotient<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|q>>|<cell|heat>|<cell|J>>|<row|<cell|<math|R<el>>>|<cell|electric
    resistance<rsup|<math|\<ast\>>>>|<cell|<math|<upOmega>>>>|<row|<cell|<math|S>>|<cell|entropy>|<cell|J<space|0.17em>K<per>>>|<row|<cell|<math|s>>|<cell|solubility>|<cell|mol<space|0.17em>m<rsup|<math|-3>>>>|<row|<cell|<no-page-break>>|<cell|number
    of species<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|T>>|<cell|thermodynamic
    temperature>|<cell|K>>|<row|<cell|<math|t>>|<cell|time>|<cell|s>>|<row|<cell|<no-page-break>>|<cell|Celsius
    temperature>|<cell|<math|<degC>>>>|<row|<cell|<math|U>>|<cell|internal
    energy>|<cell|J>>|<row|<cell|<math|V>>|<cell|volume>|<cell|m<rsup|<math|3>>>>|<row|<cell|<math|v>>|<cell|specific
    volume>|<cell|m<rsup|<math|3>> kg<per>>>|<row|<cell|>|<cell|velocity,
    speed>|<cell|m<space|0.17em>s<per>>>|<row|<cell|<math|w>>|<cell|work>|<cell|J>>|<row|<cell|>|<cell|mass
    fraction (weight fraction)>|<cell|(dimensionless)>>|<row|<cell|<math|w<el>>>|<cell|electrical
    work<rsup|<math|\<ast\>>>>|<cell|J>>|<row|<cell|<math|w<rprime|'>>>|<cell|nonexpansion
    work<rsup|<math|\<ast\>>>>|<cell|J>>|<row|<cell|<math|x>>|<cell|mole
    fraction in a phase>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|Cartesian
    space coordinate>|<cell|m>>|<row|<cell|<math|y>>|<cell|mole fraction in
    gas phase>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|Cartesian
    space coordinate>|<cell|m>>|<row|<cell|<math|Z>>|<cell|compression factor
    (compressibility factor)>|<cell|(dimensionless)>>|<row|<cell|<math|z>>|<cell|mole
    fraction in multiphase system<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|charge
    number of an ion>|<cell|(dimensionless)>>|<row|<cell|>|<cell|electron
    number of cell reaction>|<cell|(dimensionless)>>|<row|<cell|>|<cell|Cartesian
    space coordinate>|<cell|m>>|<row|<cell|>|<cell|>|<cell|>>|<row|<cell|<multicolumn|2|@l|<em|Greek
    letters>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><multicolumn|1|@l|<em|alpha>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<alpha\>>>|<cell|degree
    of reaction, dissociation, etc.>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|cubic
    expansion coefficient>|<cell|K<per>>>|<row|<cell|<multicolumn|1|@l|<em|gamma>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|<g>>>|<cell|surface
    tension>|<cell|N<space|0.17em>m<per>,
    J<space|0.17em>m<rsup|<math|-2>>>>|<row|<cell|<math|<g><rsub|i>>>|<cell|activity
    coefficient of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>pure
    liquid or solid standard state<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<math|<g><rsub|m,i>>>|<cell|activity
    coefficient of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>molality
    basis>|<cell|(dimensionless)>>|<row|<cell|<math|<g><rsub|c,i>>>|<cell|activity
    coefficient of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>concentration
    basis>|<cell|(dimensionless)>>|<row|<cell|<math|<g><rsub|x,i>>>|<cell|activity
    coefficient of species <math|i>,>|<cell|>>|<row|<cell|<no-page-break>>|<cell|<space|1em>mole
    fraction basis>|<cell|(dimensionless)>>|<row|<cell|<math|<g><rsub|\<pm\>>>>|<cell|mean
    ionic activity coefficient>|<cell|(dimensionless)>>|<row|<cell|<math|<G>>>|<cell|pressure
    factor (activity of a reference state)<rsup|<math|\<ast\>>>>|<cell|(dimensionless)>>|<row|<cell|<multicolumn|1|@l|<em|epsilon>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<epsilon\>>>|<cell|efficiency
    of a heat engine>|<cell|(dimensionless)>>|<row|<cell|>|<cell|energy
    equivalent of a calorimeter<rsup|<math|\<ast\>>>>|<cell|J<space|0.17em>K<per>>>|<row|<cell|<multicolumn|1|@l|<em|theta>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<vartheta\>>>|<cell|angle
    of rotation>|<cell|(dimensionless)>>|<row|<cell|<multicolumn|1|@l|<em|kappa>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<kappa\>>>|<cell|reciprocal
    radius of ionic atmosphere>|<cell|m<per>>>|<row|<cell|<math|\<kappa\><rsub|T>>>|<cell|isothermal
    compressibility>|<cell|Pa<per>>>|<row|<cell|<multicolumn|1|@l|<em|mu>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<mu\>>>|<cell|chemical
    potential>|<cell|J<space|0.17em>mol<per>>>|<row|<cell|<math|\<mu\><subs|J*T>>>|<cell|Joule--Thomson
    coefficient>|<cell|K<space|0.17em>Pa<per>>>|<row|<cell|<multicolumn|1|@l|<em|nu>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<nu\>>>|<cell|number
    of ions per formula unit>|<cell|(dimensionless)>>|<row|<cell|>|<cell|stoichiometric
    number>|<cell|(dimensionless)>>|<row|<cell|<math|\<nu\><rsub|+>>>|<cell|number
    of cations per formula unit>|<cell|(dimensionless)>>|<row|<cell|<math|\<nu\><rsub|->>>|<cell|number
    of anions per formula unit>|<cell|(dimensionless)>>|<row|<cell|<multicolumn|1|@l|<em|xi>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<xi\>>>|<cell|advancement
    (extent of reaction)>|<cell|mol>>|<row|<cell|<multicolumn|1|@l|<em|pi>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|<varPi>>>|<cell|osmotic
    pressure>|<cell|Pa>>|<row|<cell|<multicolumn|1|@l|<em|rho>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<rho\>>>|<cell|density>|<cell|kg<space|0.17em>m<rsup|<math|-3>>>>|<row|<cell|<multicolumn|1|@l|<em|tau>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<tau\>>>|<cell|torque<rsup|<math|\<ast\>>>>|<cell|J>>|<row|<cell|<multicolumn|1|@l|<em|phi>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<phi\>>>|<cell|fugacity
    coefficient>|<cell|(dimensionless)>>|<row|<cell|<no-page-break>>|<cell|electric
    potential>|<cell|V>>|<row|<cell|<math|<Del>\<phi\>>>|<cell|electric
    potential difference>|<cell|V>>|<row|<cell|<math|\<phi\><rsub|m>>>|<cell|osmotic
    coefficient, molality basis>|<cell|(dimensionless)>>|<row|<cell|<math|<varPhi><rsub|L>>>|<cell|relative
    apparent molar enthalpy of solute<rsup|<math|\<ast\>>>>|<cell|J<space|0.17em>mol<per>>>|<row|<cell|<multicolumn|1|@l|<em|omega>>>|<cell|>|<cell|>>|<row|<cell|<no-page-break><math|\<omega\>>>|<cell|angular
    velocity>|<cell|s<per>>>|<row|<cell|>|<cell|>|<cell|>>>>
  </big-table>

  <I|Symbols for physical quantities\|)><I|Physical quantities, symbols
  for\|)>
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>