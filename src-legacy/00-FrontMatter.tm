<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <frontmatter>*

  <thispagestyle|empty>

  <pdfbookmark*|0|Cover|c>

  <pagecolor*|rgb|.09,.21,.06> <with|color|rgb:.99,.75,.27|}}
  <next-line><vspace*|1.7cm> <boxedminipage|14.0cm| <with|par-mode|center|
  <rule|13.45cm|.2cm|<covertitle><rule|0cm|1.9cm>THERMODYNAMICS<next-line><vspace*|.5cm>AND<next-line><vspace*|.5cm>CHEMISTRY<next-line>>
  <vspace|1.5cm><coveredition|SECOND EDITION><next-line>
  <vspace|2.4cm><image|./cover-swash.eps||||><next-line><vspace|5.4cm><coverauthor|HOWARD
  DEVOE><next-line> <vspace|.8cm><rule|13.45cm|.2cm>>>>

  <\with|color|rgb:.99,.75,.27>
    <new-page>

    <pdfbookmark*|0|Title page|t>

    <pagecolor|white>
  </with>

  <with|color|black|<thispagestyle|empty>>

  <\with|color|black>
    <vspace*|.25in><rule|4.5in|.2cm><next-line><vspace|.1in><with|font-size|2|Thermodynamics<next-line><vspace|.2in>and
    Chemistry><next-line><vspace|.2in><with|font-size|1.41|Second
    Edition<next-line>Version <version>, <compilationdate>><next-line><vspace|.16in><rule|4.5in|.2cm>

    <vspace*|2.0in><with|font-size|1.41|Howard
    DeVoe<next-line><vspace|.1in>><with|font-size|1.19|Associate Professor of
    Chemistry Emeritus<next-line>University of Maryland, College Park,
    Maryland<next-line>hdevoe@umd.edu>
  </with>

  <\with|font-size|1.19>
    <with|color|black|<new-page><thispagestyle|empty>
    <pdfbookmark*|0|Copyright page|cp>>

    <with|color|black|<no-indent>The first edition of this book was
    previously published by Pearson Education, Inc. It was copyright
    \<copyright\>2001 by Prentice-Hall, Inc.<next-line>>

    <with|color|black|<no-indent>The second edition, version <version>is
    copyright \<copyright\><copyrightyear>by Howard DeVoe.<next-line>>

    <with|color|black|<no-indent><image|./00-SUP/by-2.eps||||><next-line>This
    work is licensed under a Creative Commons Attribution 4.0 International
    License: <vspace|-.3cm>>

    <with|color|black|<padded-center|<slink|https://creativecommons.org/licenses/by/4.0/>
    >>

    <with|color|black|<no-indent>The book was typeset using the <LaTeX>
    typesetting system and the memoir class. Most of the figures were
    produced with PSTricks, a related software program. The fonts are Adobe
    Times, MathTime, and Computer Modern Typewriter.<next-line>>

    <with|color|black|<no-indent>A Solutions Manual is available at the Web
    site linked below.<next-line>>

    <with|color|black|<no-indent>I thank the Department of Chemistry and
    Biochemistry, University of Maryland, College Park, Maryland for hosting
    the Web site for this book: <vspace|-.3cm>>

    <with|color|black|<padded-center|<slink|http://www.chem.umd.edu/thermobook>
    ><vspace|-.3cm>>

    <with|color|black|<new-page><assign|*|<macro|<contentsname>>>Short
    Contents <reset-counter|tocdepth><pagestyle|nosectioninheader>
    <markboth|Short Contents|Short Contents> <pdfbookmark*|0|Short
    Contents|stoc>>

    <\with|color|black>
      <\table-of-contents|toc>
        \;
      </table-of-contents>

      *
    </with>

    <with|color|black|<new-page><assign|*|<macro|<contentsname>>>Contents
    <assign|tocdepth-nr|2><pagestyle|nosectioninheader>
    <markboth|Contents|Contents> <pdfbookmark*|0|Contents|toc>>

    <\with|color|black>
      <\table-of-contents|toc>
        \;
      </table-of-contents>

      *
    </with>

    <with|color|black|<new-page>>

    <with|color|black|<chapter|Biographical Sketches>>

    <with|color|black|<listofbios>>

    <with|color|black|<chapter|Preface to the Second Edition>>

    <with|color|black|This second edition of <em|Thermodynamics and
    Chemistry> is a revised and enlarged version of the first edition
    published by Prentice Hall in 2001. The book is designed primarily as a
    textbook for a one-semester course for graduate or undergraduate students
    who have already been introduced to thermodynamics in a physical
    chemistry course.>

    <with|color|black|<aboutlinks>>

    <with|color|black|Scattered through the text are sixteen one-page
    biographical sketches of some of the historical giants of thermodynamics.
    A list is given on the preceding page. The sketches are not intended to
    be comprehensive biographies, but rather to illustrate the human side of
    thermodynamics\Vthe struggles and controversies by which the concepts and
    experimental methodology of the subject were developed.>

    <with|color|black|The epigraphs on page <pageref|epigraphs> are intended
    to suggest the nature and importance of classical thermodynamics. You may
    wonder about the conversation between Alice and Humpty Dumpty. Its point,
    particularly important in the study of thermodynamics, is the need to pay
    attention to definitions\Vthe intended meanings of words.>

    <with|color|black|I welcome comments and suggestions for improving this
    book. My e-mail address appears below.<next-line>>

    <with|color|black|<padded-left-aligned|Howard
    DeVoe<next-line>hdevoe@umd.edu>>

    <with|color|black|<chapter|From the Preface to the First Edition>>
  </with>

  <\with|font-size|1.19>
    <with|color|black|Classical thermodynamics, the subject of this book, is
    concerned with macroscopic aspects of the interaction of matter with
    energy in its various forms. This book is designed as a text for a
    one-semester course for senior undergraduate or graduate students who
    have already been introduced to thermodynamics in an undergraduate
    physical chemistry course.>

    <with|color|black|Anyone who studies and uses thermodynamics knows that a
    deep understanding of this subject does not come easily. There are
    subtleties and interconnections that are difficult to grasp at first. The
    more times one goes through a thermodynamics course (as a student or a
    teacher), the more insight one gains. Thus, this text will reinforce and
    extend the knowledge gained from an earlier exposure to thermodynamics.
    To this end, there is fairly intense discussion of some basic topics,
    such as the nature of spontaneous and reversible processes, and inclusion
    of a number of advanced topics, such as the reduction of bomb calorimetry
    measurements to standard-state conditions.>

    <with|color|black|This book makes no claim to be an exhaustive treatment
    of thermodynamics. It concentrates on derivations of fundamental
    relations starting with the thermodynamic laws and on applications of
    these relations in various areas of interest to chemists. Although
    classical thermodynamics treats matter from a purely macroscopic
    viewpoint, the book discusses connections with molecular properties when
    appropriate.>

    <with|color|black|In deriving equations, I have strived for rigor,
    clarity, and a minimum of mathematical complexity. I have attempted to
    clearly state the conditions under which each theoretical relation is
    valid because only by understanding the assumptions and limitations of a
    derivation can one know when to use the relation and how to adapt it for
    special purposes. I have taken care to be consistent in the use of
    symbols for physical properties. The choice of symbols follows the
    current recommendations of the International Union of Pure and Applied
    Chemistry (IUPAC) with a few exceptions made to avoid ambiguity.>

    <with|color|black|I owe much to J. Arthur Campbell, Luke E. Steiner, and
    William Moffitt, gifted teachers who introduced me to the elegant logic
    and practical utility of thermodynamics. I am immensely grateful to my
    wife Stephanie for her continued encouragement and patience during the
    period this book went from concept to reality.>

    <with|color|black|I would also like to acknowledge the help of the
    following reviewers: James L. Copeland, Kansas State University; Lee
    Hansen, Brigham Young University; Reed Howald, Montana State
    University\UBozeman; David W. Larsen, University of Missouri\USt. Louis;
    Mark Ondrias, University of New Mexico; Philip H. Rieger, Brown
    University; Leslie Schwartz, St. John Fisher College; Allan L. Smith,
    Drexel University; and Paul E. Smith, Kansas State University.>
  </with>

  <new-page><label|epigraphs><new-page><thispagestyle|empty>

  <pdfbookmark*|0|Epigraphs|e>

  <\epigraphs>
    <qitem|A theory is the more impressive the greater the simplicity of its
    premises is, the more different kinds of things it relates, and the more
    extended is its area of applicability. Therefore the deep impression
    which classical thermodynamics made upon me. It is the only physical
    theory of universal content concerning which I am convinced that, within
    the framework of the applicability of its basic concepts, it will never
    be overthrown.>Albert Einstein

    <vspace*|baselineskip><qitem|Thermodynamics is a discipline that involves
    a formalization of a large number of intuitive concepts derived from
    common experience.>J. G. Kirkwood and I. Oppenheim, <em|Chemical
    Thermodynamics>, 1961

    <vspace*|baselineskip><qitem|The first law of thermodynamics is nothing
    more than the principle of the conservation of energy applied to
    phenomena involving the production or absorption of heat.>Max Planck,
    <em|Treatise on Thermodynamics>, 1922

    <vspace*|baselineskip><qitem|The law that entropy always
    increases<emdash>the second law of thermodynamics<emdash>holds, I think,
    the supreme position among the laws of Nature. If someone points out to
    you that your pet theory of the universe is in disagreement with
    Maxwell's equations<emdash>then so much the worse for Maxwell's
    equations. If it is found to be contradicted by observation<emdash>well,
    these experimentalists do bungle things sometimes. But if your theory is
    found to be against the second law of thermodynamics I can give you no
    hope; there is nothing for it but to collapse in deepest humiliation.>Sir
    Arthur Eddington, <em|The Nature of the Physical World>, 1928

    <vspace*|baselineskip><qitem|Thermodynamics is a collection of useful
    relations between quantities, every one of which is independently
    measurable. What do such relations ``tell one'' about one's system, or in
    other words what do we learn from thermodynamics about the microscopic
    explanations of macroscopic changes? Nothing whatever. What then is the
    <em|use> of thermodynamics? Thermodynamics is useful precisely because
    some quantities are easier to measure than others, and that is all.>M. L.
    McGlashan, <em|J. Chem. Educ.>, <with|font-series|bold|43>, 226--232
    (1966)

    <vspace*|baselineskip><qitem|``When <em|I> use a word,'' Humpty Dumpty
    said, in rather a scornful tone, ``it means just what I choose it to
    mean<emdash>neither more nor less.''<next-line><space|.4cm>``The question
    is,'' said Alice,``whether you <em|can> make words mean so many different
    things.''<next-line><space|.4cm>``The question is,'' said Humpty Dumpty,
    ``which is to be master<emdash> that's all.''>Lewis Carroll, <em|Through
    the Looking-Glass>
  </epigraphs>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|2|?>>
    <associate|auto-3|<tuple|3|?>>
    <associate|epigraphs|<tuple|3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|1<space|2spc>Biographical
      Sketches> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|1fn>

      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|2<space|2spc>Preface
      to the Second Edition> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|1fn>

      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|3<space|2spc>From
      the Preface to the First Edition> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3><vspace|1fn>
    </associate>
  </collection>
</auxiliary>