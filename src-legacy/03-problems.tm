<TeXmacs|1.99.20>

<style|<tuple|book|std-latex>>

<\body>
  <\hide-preamble>
    <assign|R|<macro|\<bbb-R\>>>
  </hide-preamble>

  <label|Chap3><problem>Assume you have a metal spring that obeys Hooke's
  law: <math|F=c*<around|(|l-l<rsub|0>|)>>, where <math|F> is the force
  exerted on the spring of length <math|l>, <math|l<rsub|0>> is the length of
  the unstressed spring, and <math|c> is the spring constant. Find an
  expression for the work done on the spring when you reversibly compress it
  from length <math|l<rsub|0>> to a shorter length <math|l<rprime|'>>. <soln|
  <math|<D>w=<big|int><rsub|l<rsub|0>><rsup|l<rprime|'>>F<dif>l=c*<big|int><rsub|l<rsub|0>><rsup|l<rprime|'>><around|(|l-l<rsub|0>|)><dif>l=<around*|\<nobracket\>|<onehalf>c*<around|(|l-l<rsub|0>|)><rsup|2>|\|><rsub|l<rsub|0>><rsup|l<rprime|'>>=<onehalf>c*<around|(|l<rprime|'>-l<rsub|0>|)><rsup|2>>>

  <\big-figure>
    <boxedfigure|<image|./03-SUP/marble-2.eps||||><capt|<label|fig:3-marble>>>
  </big-figure|>

  <problem>The apparatus shown in Fig. <reference|fig:3-marble><vpageref|fig:3-marble>
  consists of fixed amounts of water and air and an incompressible solid
  glass sphere (a marble), all enclosed in a rigid vessel resting on a lab
  bench. Assume the marble has an adiabatic outer layer so that its
  temperature cannot change, and that the walls of the vessel are also
  adiabatic.

  Initially the marble is suspended above the water. When released, it falls
  through the air into the water and comes to rest at the bottom of the
  vessel, causing the water and air (but not the marble) to become slightly
  warmer. The process is complete when the system returns to an equilibrium
  state. The system energy change during this process depends on the frame of
  reference and on how the system is defined. <math|<Del>E<sys>> is the
  energy change in a lab frame, and <math|<Del>U> is the energy change in a
  specified local frame.

  For each of the following definitions of the system, give the <em|sign>
  (positive, negative, or zero) of both <math|<Del>E<sys>> and <math|<Del>U>,
  and state your reasoning. Take the local frame for each system to be a
  center-of-mass frame. <soln| Because <math|q> is zero in each part of this
  problem, <math|<Del>E<sys>> is equal to <math|w<lab>> and <math|<Del>U> is
  equal to <math|w>. We can use Eq. <reference|w-w(lab)=> with
  <math|<Del><space|-0.17em><around*|(|v<rsup|2><cm>|)>> set equal to zero:
  <math|<Del>U-<Del>E<sys>=w<lab>-w=-m*g<Del>z<cm>> or
  <math|<Del>E<sys>=<Del>U+m*g<Del>z<cm>>.> <problemparts| <problempart>The
  system is the marble. <soln| <math|<Del>U> is zero, because the state of
  the system is unchanged.<next-line><math|<Del>E<sys>> is negative, because
  in the lab frame the marble does work on the water (page
  <pageref|free-falling body>). This can also be deduced using
  <math|<Del>E<sys>=<Del>U+m*g<Del>z<cm>> and the fact that <math|<Del>U> is
  zero and <math|<Del>z<cm>> is negative.> <problempart>The system is the
  combination of water and air. <soln| <math|<Del>U> is positive, because the
  system's temperature increases at constant
  volume.<next-line><math|<Del>E<sys>> is positive, because both
  <math|<Del>U> and <math|<Del>z<cm>> are positive (the center of gravity of
  the water rises when the marble enters the water). This can also be deduced
  by considering that the net force exerted by the sinking marble on the
  water and the displacement of the boundary at the marble are in the same
  direction (downward).> <problempart>The system is the combination of water,
  air, and marble. <soln| <math|<Del>E<sys>> is zero, because <math|w<lab>>
  is zero (there is no displacement of the system boundary in the lab
  frame).<next-line><math|<Del>U> is positive, because <math|<Del>E<sys>> is
  zero and <math|<Del>z<cm>> is negative. This can also be deduced from the
  fact that <math|U> is an extensive property, so that <math|<Del>U> for this
  system is equal to the sum of the internal energy change of the marble and
  the internal energy change of the water and air. In parts (a) and (b) these
  changes were found to be zero and positive, respectively.>>

  <\big-figure>
    <boxedfigure|<image|./03-SUP/por-plug.eps||3.5cm||><capt|<label|fig:3-porous
    plug>>>
  </big-figure|>

  <problem><label|prb:3-porous plug> Figure <reference|fig:3-porous
  plug><vpageref|fig:3-porous plug> shows the initial state of an apparatus
  consisting of an ideal gas in a bulb, a stopcock, a porous plug, and a
  cylinder containing a frictionless piston. The walls are diathermal, and
  the surroundings are at a constant temperature of <math|300.0<K>> and a
  constant pressure of <math|1.00<br>>.

  When the stopcock is opened, the gas diffuses slowly through the porous
  plug, and the piston moves slowly to the right. The process ends when the
  pressures are equalized and the piston stops moving. The <em|system> is the
  gas. Assume that during the process the temperature throughout the system
  differs only infinitesimally from <math|300.0<K>> and the pressure on both
  sides of the piston differs only infinitesimally from <math|1.00<br>>.

  <\problemparts>
    \ <problempart>Which of these terms correctly describes the process:
    isothermal, isobaric, isochoric, reversible, irreversible? <soln| The
    process is isothermal and irreversible, but not isobaric, isochoric, or
    reversible. Note that the pressure gradient across the porous plug
    prevents intermediate states of the process from being equilibrium
    states, and keeps the process from being reversible; there is no
    infinitesimal change that can reverse the motion of the piston.>
    <problempart>Calculate <math|q> and <math|w>.

    <answer|<math|q=-w=1.00<timesten|5><units|J>>>

    <soln| Because <math|T> is constant and the gas is ideal, the relation
    <math|p<rsub|1>*V<rsub|1>=p<rsub|2>*V<rsub|2>> holds, and the final
    volume is found from <vs><math|<D>V<rsub|2>=<frac|p<rsub|1>*V<rsub|1>|p<rsub|2>>=<frac|<around|(|3.00<br>|)><around|(|0.500<units|m<rsup|<math|3>>>|)>|1.00<br>>=1.50<units|m<rsup|<math|3>>>>
    <vs>The work must be calculated from the pressure at the moving portion
    of the boundary (the inner surface of the piston); this is a constant
    pressure of <math|1.00<br>>: <vs><math|<D>w=-<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>><space|-0.17em>p<dif>V=-p*<around|(|V<rsub|2>-V<rsub|1>|)>=-<around|(|1.00<timesten|5><Pa>|)>*<around|(|1.50-0.500|)><tx|m<rsup|<math|3>>>><no-page-break><vs><math|<D><phantom|w>=-1.00<timesten|5><units|J>>
    <vs><math|<D>q=<Del>U-w=0-w=1.00<timesten|5><units|J>>>
  </problemparts>

  <problem>Consider a horizontal cylinder-and-piston device similar to the
  one shown in Fig. <reference|fig:3-cylinder><vpageref|fig:3-cylinder>. The
  piston has mass <math|m>. The cylinder wall is diathermal and is in thermal
  contact with a heat reservoir of temperature <math|T<subs|e*x*t>>. The
  <em|system> is an amount <math|n> of an ideal gas confined in the cylinder
  by the piston.

  The initial state of the system is an equilibrium state described by
  <math|p<rsub|1>> and <math|T=T<subs|e*x*t>>. There is a constant external
  pressure <math|p<subs|e*x*t>>, equal to twice <math|p<rsub|1>>, that
  supplies a constant external force on the piston. When the piston is
  released, it begins to move to the left to compress the gas. Make the
  idealized assumptions that (1) the piston moves with negligible friction;
  and (2) the gas remains practically uniform (because the piston is massive
  and its motion is slow) and has a practically constant temperature
  <math|T=T<subs|e*x*t>> (because temperature equilibration is rapid).

  <\problemparts>
    \ <problempart>Describe the resulting process. <soln| The piston will
    oscillate; the gas volume will change back and forth between the initial
    value <math|V<rsub|1>> and a minimum value <math|V<rsub|2>>.>
    <problempart>Describe how you could calculate <math|w> and <math|q>
    during the period needed for the piston velocity to become zero again.
    <soln| The relation between <math|V<rsub|1>> and <math|V<rsub|2>> is
    found by equating the work done on the gas by the piston, <math|-n*R*T*ln
    <around|(|V<rsub|2>/V<rsub|1>|)>>, to the work done on the piston by the
    external pressure, <math|-p<subs|e*x*t><around|(|V<rsub|2>-V<rsub|1>|)>>,
    where <math|p<subs|e*x*t>> is given by
    <math|p<subs|e*x*t>=2*p<rsub|1>=2*n*R*T/V<rsub|1>>. The result is
    <math|V<rsub|2>=0.2032*V<rsub|1>>, <math|w=1.5936*n*R*T>,
    <math|q=-w=-1.5936*n*R*T>.> <problempartAns>Calculate <math|w> and
    <math|q> during this period for <math|0.500<mol>> gas at <math|300<K>>.

    <answer|<math|w=1.99<timesten|3><units|J>>,
    <math|q=-1.99<timesten|3><units|J>>.>

    <soln| <math|w=<around|(|1.5936|)><around|(|0.500<mol>|)><around|(|<R>|)><around|(|300<K>|)>=1.99<timesten|3><units|J>>,
    <math|q=-w=-1.99<timesten|3><units|J>>.>
  </problemparts>

  <\big-figure>
    <boxedfigure|<image|./03-SUP/vertical-cyl.eps||||><capt|<label|fig:3-vertical
    cylinder>>>
  </big-figure|>

  <problemAns>This problem is designed to test the assertion on page
  <pageref|w approx equal to w(lab)> that for typical thermodynamic processes
  in which the elevation of the center of mass changes, it is usually a good
  approximation to set <math|w> equal to <math|w<lab>>. The cylinder shown in
  Fig. <reference|fig:3-vertical cylinder><vpageref|fig:3-vertical cylinder>
  has a vertical orientation, so the elevation of the center of mass of the
  gas confined by the piston changes as the piston slides up or down. The
  <em|system> is the gas. Assume the gas is nitrogen
  (<math|M=28.0<units|g*<space|0.17em>m*o*l<per>>>) at <math|300<K>>, and
  initially the vertical length <math|l> of the gas column is one meter.
  Treat the nitrogen as an ideal gas, use a center-of-mass local frame, and
  take the center of mass to be at the midpoint of the gas column. Find the
  difference between the values of <math|w> and <math|w<lab>>, expressed as a
  percentage of <math|w>, when the gas is expanded reversibly and
  isothermally to twice its initial volume.

  <\answer>
    <math|0.0079%>
  </answer>

  <\soln>
    \ Use Eq. <reference|w-w(lab)=>: <math|w-w<lab>=-<onehalf>m<Del><space|-0.17em><around*|(|v<rsup|2><cm>|)>-m*g<Del>z<cm>>.
    <vs><math|<Del><space|-0.17em><around*|(|v<rsup|2><cm>|)>> is zero, and
    <math|<D><Del>z<cm>> is <math|<frac|l<rsub|2>|2>-<frac|l<rsub|1>|2>=<frac|2*l<rsub|1>|2>-<frac|l<rsub|1>|2>=<onehalf>l<rsub|1>>;
    therefore <math|w-w<lab>=-<onehalf>m*g*l<rsub|1>>. <vs>From Eq.
    <reference|w=-nRT ln(V2/V1)>, which assumes the local frame is a lab
    frame:<next-line><math|<D>w<lab>=-n*R*T*ln
    <frac|V<rsub|2>|V<rsub|1>>=-n*R*T*ln 2>. <vs>Use these relations to
    obtain <math|w=w<lab>-<onehalf>m*g*l<rsub|1>=-n*R*T*ln
    2-<onehalf>m*g*l<rsub|1>>. <vs><math|<D><frac|w-w<lab>|w>=<frac|-<onehalf>m*g*l<rsub|1>|-n*R*T*ln
    2-<onehalf>m*g*l<rsub|1>>=<frac|1|<D><frac|2*n*R*T*ln
    2|m*g*l<rsub|1>>+1>=<frac|1|<D><frac|2*R*T*ln 2|M*g*l<rsub|1>>+1>>
    <vs><math|<D><phantom|<D><frac|w-w<lab>|w>>=<frac|1|<D><frac|<around|(|2|)><around|(|<R>|)><around|(|300<K>|)><around|(|ln
    2|)>|(28.0<timesten|-3><units|k*g*<space|0.17em>m*o*l<per>)<around|(|9.81<units|m*<space|0.17em>s<rsup|<math|-2>>>|)>*<around|(|1*<space|0.17em>m|)>>>+1>=7.9<timesten|-5>>
    <vs>which is <math|0.0079%>.
  </soln>

  <\big-figure>
    <boxedfigure|<image|./03-SUP/sponcomp.eps||||><capt|<label|fig:3-spont
    compression>>>
  </big-figure|>

  <problem>Figure <reference|fig:3-spont compression><vpageref|fig:3-spont
  compression> shows an ideal gas confined by a frictionless piston in a
  vertical cylinder. The <em|system> is the gas, and the boundary is
  adiabatic. The downward force on the piston can be varied by changing the
  weight on top of it.

  <\problemparts>
    \ <problempart>Show that when the system is in an equilibrium state, the
    gas pressure is given by <math|p=m*g*h/V> where <math|m> is the combined
    mass of the piston and weight, <math|g> is the acceleration of free fall,
    and <math|h> is the elevation of the piston shown in the figure. <soln|
    The piston must be stationary in order for the system to be in an
    equilibrium state. Therefore the net force on the piston is zero:
    <math|p<As>-m*g=0> (where <math|<As>> is the cross-section area of the
    cylinder). This gives <math|p=m*g/<As>=m*g/<around|(|V/h|)>=m*g*h/V>.>
    <problempart>Initially the combined mass of the piston and weight is
    <math|m<rsub|1>>, the piston is at height <math|h<rsub|1>>, and the
    system is in an equilibrium state with conditions <math|p<rsub|1>> and
    <math|V<rsub|1>>. The initial temperature is
    <math|T<rsub|1>=p<rsub|1>*V<rsub|1>/n*R>. Suppose that an additional
    weight is suddenly placed on the piston, so that <math|m> increases from
    <math|m<rsub|1>> to <math|m<rsub|2>>, causing the piston to sink and the
    gas to be compressed adiabatically and spontaneously. Pressure gradients
    in the gas, a form of friction, eventually cause the piston to come to
    rest at a final position <math|h<rsub|2>>. Find the final volume,
    <math|V<rsub|2>>, as a function of <math|p<rsub|1>>, <math|p<rsub|2>>,
    <math|V<rsub|1>>, and <math|C<rsub|V>>. (Assume that the heat capacity of
    the gas, <math|C<rsub|V>>, is independent of temperature.) Hint: The
    potential energy of the surroundings changes by <math|m<rsub|2>*g<Del>h>;
    since the kinetic energy of the piston and weights is zero at the
    beginning and end of the process, and the boundary is adiabatic, the
    internal energy of the gas must change by
    <math|-m<rsub|2>*g<Del>h=-m<rsub|2>*g<Del>V/<As>=-p<rsub|2><Del>V>.
    <soln| There are two expressions for <math|<Del>U>:
    <vs><math|<Del>U=C<rsub|V>*<around|(|T<rsub|2>-T<rsub|1>|)><space|1em>>
    and <math|<space|1em><Del>U=-p<rsub|2>*<around|(|V<rsub|2>-V<rsub|1>|)>>
    <vs>Equate the two expressions and substitute
    <math|T<rsub|1>=p<rsub|1>*V<rsub|1>/n*R> and
    <math|T<rsub|2>=p<rsub|2>*V<rsub|2>/n*R>:
    <vs><math|<around|(|C<rsub|V>/n*R|)>*<around|(|p<rsub|2>*V<rsub|2>-p<rsub|1>*V<rsub|1>|)>=-p<rsub|2>*<around|(|V<rsub|2>-V<rsub|1>|)>>
    <vs>Solve for <math|V<rsub|2>>: <math|<D>V<rsub|2>=<frac|C<rsub|V>*p<rsub|1>+n*R*p<rsub|2>|p<rsub|2>*<around|(|C<rsub|V>+n*R|)>>*V<rsub|1>>>
    <problempartAns>It might seem that by making the weight placed on the
    piston sufficiently large, <math|V<rsub|2>> could be made as close to
    zero as desired. Actually, however, this is not the case. Find
    expressions for <math|V<rsub|2>> and <math|T<rsub|2>> in the limit as
    <math|m<rsub|2>> approaches infinity, and evaluate
    <math|V<rsub|2>/V<rsub|1>> in this limit if the heat capacity is
    <math|C<rsub|V>=<around|(|3/2|)>*n*R> (the value for an ideal monatomic
    gas at room temperature).

    <answer|<math|V<rsub|2><ra>n*R*V<rsub|1>/<around|(|C<rsub|V>+n*R|)>>,
    <math|T<rsub|2><ra>\<infty\>>. For <math|C<rsub|V>=<around|(|3/2|)>*n*R>,
    <math|V<rsub|2>/V<rsub|1><ra>0.4>.>

    <soln| Since <math|p<rsub|2>> is equal to <math|m<rsub|2>*g/<As>>,
    <math|p<rsub|2>> must approach <math|\<infty\>> as <math|m<rsub|2>>
    approaches <math|\<infty\>>. In the expression for <math|V<rsub|2>>, the
    term <math|C<rsub|V>*p<rsub|1>> becomes negligible as <math|p<rsub|2>>
    approaches <math|\<infty\>>; then <math|p<rsub|2>> cancels from the
    numerator and denominator giving <vs><math|<D>V<rsub|2><ra><frac|n*R|C<rsub|V>+n*R>*V<rsub|1>>
    <vs>The relation <math|T<rsub|2>=p<rsub|2>*V<rsub|2>/n*R> shows that with
    a finite limiting value of <math|V<rsub|2>>, <math|T<rsub|2>> must
    approach <math|\<infty\>> as <math|p<rsub|2>> does. If <math|C<rsub|V>>
    equals <math|<around|(|3/2|)>*n*R>, then <math|V<rsub|2>/V<rsub|1>>
    approaches <math|2/5=0.4>.>
  </problemparts>

  <problem>The solid curve in Fig. <reference|fig:3-adiabat and
  isotherms><vpageref|fig:3-adiabat and isotherms> shows the path of a
  reversible adiabatic expansion or compression of a fixed amount of an ideal
  gas. Information about the gas is given in the figure caption. For
  compression along this path, starting at
  <math|V=0.3000<units|d*m<rsup|<math|3>>>> and <math|T=300.0<K>> and ending
  at <math|V=0.1000<units|d*m<rsup|<math|3>>>>, find the final temperature to
  <math|0.1<K>> and the work. <soln| <math|C<rsub|V>=n<CVm>=n*<around|(|1.500*R|)>=<around|(|0.0120<mol>|)><around|(|1.500|)><around|(|<R>|)>=0.1497<units|J*<space|0.17em>K<per>>>
  <vs><math|<D>T<rsub|2>=T<rsub|1><around*|(|<frac|V<rsub|1>|V<rsub|2>>|)><rsup|n*R/C<rsub|V>>=<around|(|300.0<K>|)><around|(|3.000|)><rsup|<around|(|1/1.500|)>>=624.0<K>>
  <vs><math|w=C<rsub|V>*<around|(|T<rsub|2>-T<rsub|1>|)>=<around|(|0.1497<units|J*<space|0.17em>K<per>>|)>*<around|(|624.0<K>-300.0<K>|)>=48.5<units|J>>>

  <\big-figure>
    <boxedfigure|<image|./03-SUP/evac-ves.eps||||><capt|<label|fig:3-evacuated
    vessel>>>
  </big-figure|>

  <problem><label|prb:3-evacuated vessel> Figure <reference|fig:3-evacuated
  vessel><vpageref|fig:3-evacuated vessel> shows the initial state of an
  apparatus containing an ideal gas. When the stopcock is opened, gas passes
  into the evacuated vessel. The <em|system> is the gas. Find <math|q>,
  <math|w>, and <math|<Del>U> under the following conditions. <problemparts|
  <problempart>The vessels have adiabatic walls. <soln| <math|q=0> because
  the process is adiabatic; <math|w=0> because it is a free expansion;
  therefore, <math|<Del>U=q+w=0>.> <problempart>The vessels have diathermal
  walls in thermal contact with a water bath maintained at <math|300.<K>>,
  and the final temperature in both vessels is <math|T=300.<K>>. <soln|
  <math|<Del>U=0> because the gas is ideal and the final and initial
  temperatures are the same; <math|w=0> because it is a free expansion;
  therefore <math|q=<Del>U-w=0>.>>

  <problem>Consider a reversible process in which the shaft of system A in
  Fig. <reference|fig:3-shaft work> makes one revolution in the direction of
  increasing <math|\<vartheta\>>. Show that the gravitational work of the
  weight is the same as the shaft work given by
  <math|w=m*g*r<Del>\<vartheta\>>. <soln| The circumference of the shaft at
  the point where the cord is attached is <math|2*\<pi\>*r>. When the shaft
  makes one revolution in the direction of increasing <math|\<vartheta\>>, a
  length of cord equal to <math|2*\<pi\>*r> becomes wrapped around the shaft,
  and the weight rises by a distance <math|<Del>z=2*\<pi\>*r>. The
  gravitational work, ignoring the buoyant force of the air, is
  <math|w=m*g<Del>z=m*g*r*<around|(|2*\<pi\>|)>>, which is the same as the
  shaft work <math|m*g*r<Del>\<vartheta\>> with
  <math|<Del>\<vartheta\>=2*\<pi\>>.>

  <\big-table>
    <plainfootnotes>

    <minipagetable|11.1cm|}<label|tbl:3-Joule
    data><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|2>|<cwith|6|6|1|1|cell-col-span|2>|<cwith|12|12|1|-1|cell-bborder|1ln>|<table|<row|<cell|Properties
    of the paddle wheel apparatus:>|<cell|>>|<row|<cell|<space|2ex>combined
    mass of the two lead weights<dotfill>>|<cell|<math|26.3182<units|k*g>>>>|<row|<cell|<space|2ex>mass
    of water in vessel<dotfill>>|<cell|<math|6.04118<units|k*g>>>>|<row|<cell|<space|2ex>mass
    of water with same heat capacity>|<cell|>>|<row|<cell|<space|5ex>as
    paddle wheel, vessel, and lid<footnote|Calculated from the masses and
    specific heat capacities of the materials.><dotfill>>|<cell|<math|0.27478<units|k*g>>>>|<row|<cell|<addlinespace>Measurements
    during the experiment:>|<cell|>>|<row|<cell|<space|2ex>number of times
    weights were wound up and released ...>|<cell|<math|20>>>|<row|<cell|<space|2ex>change
    of elevation of weights during each descent<dotfill>>|<cell|<math|-1.5898<units|m>>>>|<row|<cell|<space|2ex>final
    downward velocity of weights during descent<dotfill>>|<cell|<math|0.0615<units|m*<space|0.17em>s<per>>>>>|<row|<cell|<space|2ex>initial
    temperature in vessel<dotfill>>|<cell|<math|288.829<K>>>>|<row|<cell|<space|2ex>final
    temperature in vessel<dotfill>>|<cell|<math|289.148<K>>>>|<row|<cell|<space|2ex>mean
    air temperature<dotfill>>|<cell|<math|289.228<K>>>>>>>>
  </big-table|Data for Problem 3.<reference|prb:3-Joule_expt>. The values are
  from Joule's 1850 paper<space|.15em><footnote|Ref. <cite|joule-1850>, p.
  67, experiment 5.> and have been converted to SI units.>

  <problem><label|prb:3-Joule_expt>

  This problem guides you through a calculation of the mechanical equivalent
  of heat using data from one of <I|Joule, James Prescott\|p>James Joule's
  experiments with a paddle wheel apparatus (see Sec. <reference|3-Joule
  paddle wheel>). The experimental data are collected in Table
  <reference|tbl:3-Joule data><vpageref|tbl:3-Joule data>.

  In each of his experiments, Joule allowed the weights of the apparatus to
  sink to the floor twenty times from a height of about <math|1.6<units|m>>,
  using a crank to raise the weights before each descent (see Fig.
  <reference|fig:3-Joule apparatus><vpageref|fig:3-Joule apparatus>). The
  paddle wheel was engaged to the weights through the roller and strings only
  while the weights descended. Each descent took about <math|26> seconds, and
  the entire experiment lasted <math|35> minutes. Joule measured the water
  temperature with a sensitive mercury-in-glass thermometer at both the start
  and finish of the experiment.

  For the purposes of the calculations, define the <em|system> to be the
  combination of the vessel, its contents (including the paddle wheel and
  water), and its lid. All energies are measured in a lab frame. Ignore the
  small quantity of expansion work occurring in the experiment. It helps
  conceptually to think of the cellar room in which Joule set up his
  apparatus as being effectively isolated from the rest of the universe; then
  the only surroundings you need to consider for the calculations are the
  part of the room outside the system.

  <\problemparts>
    \ <problempart>Calculate the change of the gravitational potential energy
    <math|E<subs|p>> of the lead weights during each of the descents. For the
    acceleration of free fall at Manchester, England (where Joule carried out
    the experiment) use the value <math|g=9.813<units|m*<space|0.17em>s<rsup|<math|-2>>>>.
    This energy change represents a decrease in the energy of the
    surroundings, and would be equal in magnitude and opposite in sign to the
    stirring work done on the system if there were no other changes in the
    surroundings. <soln| <math|<Del>E<subs|p>=m*g<Del>h=<around|(|26.3182<units|k*g>|)><around|(|9.813<units|m*<space|0.17em>s<rsup|<math|-2>>>|)>*<around|(|-1.5898<units|m>|)>=-410.6<units|J>>>
    <problempart>Calculate the kinetic energy <math|E<subs|k>> of the
    descending weights just before they reached the floor. This represents an
    increase in the energy of the surroundings. (This energy was dissipated
    into thermal energy in the surroundings when the weights came to rest on
    the floor.) <soln| <math|E<subs|k>=<around|(|1/2|)>*m*v<rsup|2>=<around|(|1/2|)><around|(|26.3182<units|k*g>|)><around|(|0.0615<units|m*<space|0.17em>s*e*c<per>>|)><rsup|2>=0.0498<units|J>>>
    <problempart>Joule found that during each descent of the weights,
    friction in the strings and pulleys decreased the quantity of work
    performed on the system by <math|2.87<units|J>>. This quantity represents
    an increase in the thermal energy of the surroundings. Joule also
    considered the slight stretching of the strings while the weights were
    suspended from them: when the weights came to rest on the floor, the
    tension was relieved and the potential energy of the strings changed by
    <math|-1.15<units|J>>. Find the total change in the energy of the
    surroundings during the entire experiment from all the effects described
    to this point. Keep in mind that the weights descended <math|20> times
    during the experiment. <soln| <math|<Del>E<subs|s*u*r>=<around|(|20|)>*<around*|(|-410.6+0.0498+2.87-1.15|)><units|J>=-8176.6<units|J>>>
    <problempart>Data in Table <reference|tbl:3-Joule data> show that change
    of the temperature of the system during the experiment was

    <\equation*>
      <Del>T=<around|(|289.148-288.829|)><K>=+0.319<K>
    </equation*>

    The paddle wheel vessel had no thermal insulation, and the air
    temperature was slighter warmer, so during the experiment there was a
    transfer of some heat into the system. From a correction procedure
    described by Joule, the temperature change that would have occurred if
    the vessel had been insulated is estimated to be <math|+0.317<K>>.

    Use this information together with your results from part (c) to evaluate
    the work needed to increase the temperature of one gram of water by one
    kelvin. This is the ``mechanical equivalent of heat'' at the average
    temperature of the system during the experiment. (As mentioned on p.
    <pageref|Joule's mech equiv heat>, Joule obtained the value
    <math|4.165<units|J>> based on all <math|40> of his experiments.) <soln|
    <math|<Del>E<sys>=-<Del>E<subs|s*u*r>=8176.6<units|J>> <vs>
    <math|<frac|<D>8176.6<units|J>|<D><around|(|6.04118<units|k*g>+0.27478<units|k*g>|)><around|(|10<rsup|3><units|g*k*g<math|<per>>>|)><around|(|0.317<K>|)>>=4.08<units|J*<space|0.17em>g<per><space|0.17em>K<per>>>>
  </problemparts>

  <problemAns>Refer to the apparatus depicted in Fig. <reference|fig:3-paddle
  \ heater><vpageref|<tformat|<table|<row|<cell|fig:3-paddle>|<cell|heater>>>>>.
  Suppose the mass of the external weight is <math|m=1.50<units|k*g>>, the
  resistance of the electrical resistor is
  <math|R<el>=5.50<units|k<math|<upOmega>>>>, and the acceleration of free
  fall is <math|g=9.81<units|m*<space|0.17em>s<rsup|<math|-2>>>>. For how
  long a period of time will the external cell need to operate, providing an
  electric potential difference <math|<around|\||<Del>\<phi\>|\|>=1.30<units|V>>,
  to cause the same change in the state of the system as the change when the
  weight sinks <math|20.0<units|c*m>> without electrical work? Assume both
  processes occur adiabatically.

  <\answer>
    <math|9.58<timesten|3><units|s>> (<math|2<units|h*r>>
    <math|40<units|m*i*n>>)
  </answer>

  <\soln>
    \ The value of <math|<Del>U> is the same in both
    processes.<next-line>Sinking of weight:<next-line><math|<Del>U=w=m*g<Del>h=<around|(|1.50<units|k*g>|)><around|(|9.81<units|m*<space|0.17em>s<rsup|<math|-2>>>|)><around|(|20.0<timesten|-2><units|m>|)>=2.943<units|J>><next-line>Electrical
    work:<next-line><math|<Del>U=w=I<rsup|2>*R<el>t>
    <space|1em><math|<Del>\<phi\>=I*R<el>> (Ohm's law);<next-line>therefore,
    <math|<Del>U=<around|(|<Del>\<phi\>|)><rsup|2>*t/R<el>>.
    <vs><math|<D>t=<frac|R<el><Del>U|<around|(|<Del>\<phi\>|)><rsup|2>>=<frac|<around|(|5.50<timesten|3><units|<math|<upOmega>>>|)><around|(|2.943<units|J>|)>|<around|(|1.30<units|V>|)><rsup|2>>=9.58<timesten|3><units|s>>
  </soln>
</body>

<\initial>
  <\collection>
    <associate|info-flag|detailed>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|Chap3|<tuple|?|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|2|?>>
    <associate|auto-3|<tuple|3|?>>
    <associate|auto-4|<tuple|4|?>>
    <associate|auto-5|<tuple|5|?>>
    <associate|auto-6|<tuple|1|?>>
    <associate|fig:3-evacuated vessel|<tuple|5|?>>
    <associate|fig:3-marble|<tuple|1|?>>
    <associate|fig:3-porous plug|<tuple|2|?>>
    <associate|fig:3-spont compression|<tuple|4|?>>
    <associate|fig:3-vertical cylinder|<tuple|3|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|prb:3-Joule_expt|<tuple|9|?>>
    <associate|prb:3-evacuated vessel|<tuple|7|?>>
    <associate|prb:3-porous plug|<tuple|3|?>>
    <associate|tbl:3-Joule data|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      joule-1850
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1>||>|<pageref|auto-1>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|2>||>|<pageref|auto-2>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|3>||>|<pageref|auto-3>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|4>||>|<pageref|auto-4>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|5>||>|<pageref|auto-5>>
    </associate>
    <\associate|table>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1>||Data for Problem
      3.<reference|prb:3-Joule_expt>. The values are from Joule's 1850
      paper<space|.15em><assign|footnote-nr|2><hidden-binding|<tuple>|2><\float|footnote|>
        <with|font-size|<quote|0.771>|<with|par-mode|<quote|justify>|par-left|<quote|0cm>|par-right|<quote|0cm>|font-shape|<quote|right>|dummy|<quote|<macro|<tex-footnote-sep>>>|dummy|<quote|<macro|<tex-footnote-tm-barlen>>>|<surround|<locus|<id|%401CAB568-400764040>|<link|hyperlink|<id|%401CAB568-400764040>|<url|#footnr-2>>|2>.
        |<hidden-binding|<tuple|footnote-2>|2><specific|texmacs|<htab|0fn|first>>|Ref.
        [<write|bib|joule-1850><reference|bib-joule-1850>], p. 67, experiment
        5.>>>
      </float><space|0spc><rsup|<with|font-shape|<quote|right>|<reference|footnote-2>>>
      and have been converted to SI units.>|<pageref|auto-6>>
    </associate>
  </collection>
</auxiliary>