<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|Phase Transitions and Equilibria of Pure Substances>

  <paragraphfootnotes><label|Chap. 8>

  A system of two or more phases of a single substance, in the absence of
  internal constraints, is in an equilibrium state when each phase has the
  same temperature, the same pressure, and the same chemical potential. This
  chapter describes the derivation and consequences of this simple principle,
  the general appearance of phase diagrams of single-substance systems, and
  quantitative aspects of the equilibrium phase transitions of these systems.

  <section|Phase Equilibria>

  <subsection|Equilibrium conditions><label|8-eqm conditions>

  If the state of an isolated system is an equilibrium state, this state does
  not change over time (Sec. <reference|2-eq states>). We expect an isolated
  system that is <em|not> in an equilibrium state to undergo a spontaneous,
  irreversible process and eventually to reach an equilibrium state. Just how
  rapidly this process occurs is a matter of kinetics, not thermodynamics.
  During this irreversible adiabatic process, the entropy increases until it
  reaches a maximum in the equilibrium state.

  A general procedure will now be introduced for finding conditions for
  equilibrium with given constraints. The procedure is applied to phase
  equilibria of single-substance, multiphase systems in the next section, to
  transfer equilibria in multicomponent, multiphase systems in Sec.
  <reference|9-eqm conditions>, and to reaction equilibria in Sec.
  <reference|11-gen rxn eqm>.

  The procedure has five steps:

  <\enumerate>
    <item>Write an expression for the total differential of the internal
    energy <math|U> consistent with any constraints and with the number of
    <index|Independent variables>independent variables of the system.

    <item>Impose conditions of isolation for the system, including
    <math|<dif>U=0>, thereby reducing the number of independent variables.

    <item>Designate a particular phase, <math|<pha><rprime|'>>, as a
    reference phase and make the substitution
    <math|<dif>S<aphp>=<dif>S-<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>S<aph>>.
    (This is valid because entropy is extensive:
    <math|S=<big|sum><rsub|<pha>>S<aph>>,
    <math|<dif>S=<big|sum><rsub|<pha>><dif>S<aph>>.)

    <item>Rearrange to obtain an expression for the total differential of the
    entropy consistent with the reduced number of independent variables.

    <item>The conditions for an equilibrium state are those that make the
    infinitesimal entropy change, <math|<dif>S>, equal to zero for all
    infinitesimal changes of the independent variables of the isolated
    system.
  </enumerate>

  <subsection|Equilibrium in a multiphase system><label|8-multiphase>

  <I|Equilibrium conditions!multiphase one component system@in a multiphase
  one-component system\|(>

  In this section we consider a system of a single substance in two or more
  uniform phases with distinctly different intensive properties. For
  instance, one phase might be a liquid and another a gas. We assume the
  phases are not separated by internal partitions, so that there is no
  constraint preventing the transfer of matter and energy among the phases.
  (A tall column of gas in a gravitational field is a different kind of
  system in which intensive properties of an equilibrium state vary
  continuously with elevation; this case will be discussed in Sec.
  <reference|8-gas in gravity>.)

  Phase <math|<pha><rprime|'>> will be the reference phase. Since internal
  energy is extensive, we can write <math|U=U<aphp>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>>U<aph>>
  and <math|<dif>U=<dif>U<aphp>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>U<aph>>.
  We assume any changes are slow enough to allow each phase to be practically
  uniform at all times. Treating each phase as an open subsystem with
  expansion work only, we use the relation
  <math|<dif>U=T<dif>S-p<dif>V+\<mu\><dif>n> (Eq.
  <reference|dU=TdS-pdV+(mu)dn>) to replace each <math|<dif>U<aph>> term:

  <\equation>
    <label|dU=T(alpha')dS(alpha')...>

    <\eqsplit>
      <tformat|<table|<row|<cell|<dif>U>|<cell|=<around|(|T<aphp><dif>S<aphp>-p<aphp><dif>V<aphp>+\<mu\><aphp><dif>n<aphp>|)>>>|<row|<cell|>|<cell|<space|1em>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><around|(|T<aph><dif>S<aph>-p<aph><dif>V<aph>+\<mu\><aph><dif>n<aph>|)>>>>>
    </eqsplit>
  </equation>

  This is an expression for the total differential of <math|U> when there are
  no constraints.

  We isolate the system by enclosing it in a rigid, stationary adiabatic
  container. The constraints needed to isolate the system, then, are given by
  the relations

  <alignat|2|<tformat|<table|<row|<cell|>|<cell|<dif>U=0>|<cell|<space|2em>>|<cell|<tx|<around|(|constant
  internal energy|)>><eq-number>>>|<row|<cell|>|<cell|<dif>V<aphp>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>V<aph>=0>|<cell|<space|2em>>|<cell|<tx|<around|(|no
  expansion work|)>><eq-number>>>|<row|<cell|>|<cell|<dif>n<aphp>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>n<aph>=0>|<cell|<space|2em>>|<cell|<tx|<around|(|closed
  system|)>><eq-number>>>>>>

  Each of these relations is an independent restriction that reduces the
  number of independent variables by one. When we substitute expressions for
  <math|<dif>U>, <math|<dif>V<aphp>>, and <math|<dif>n<aphp>> from these
  relations into Eq. <reference|dU=T(alpha')dS(alpha')...>, make the further
  substitution <math|<dif>S<aphp>=<dif>S-<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><dif>S<aph>>,
  and collect term with the same differentials on the right side, we obtain

  <\equation>
    <label|0=T^alpha'dS^alpha'+...>

    <\eqsplit>
      <tformat|<table|<row|<cell|0>|<cell|=T<aphp><dif>S+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><around|(|T<aph>-T<aphp>|)><dif>S<aph>-<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><around|(|p<aph>-p<aphp>|)><dif>V<aph>>>|<row|<cell|>|<cell|<space|1em>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><around|(|\<mu\><aph>-\<mu\><aphp>|)><dif>n<aph>>>>>
    </eqsplit>
  </equation>

  Solving for <math|<dif>S>, we obtain

  <\equation>
    <label|dS=()dS(alpha)-()dV(alpha)+()dn(alpha)>

    <\eqsplit>
      <tformat|<table|<row|<cell|<dif>S>|<cell|=<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|T<aphp>-T<aph>|T<aphp>><dif>S<aph>-<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|p<aphp>-p<aph>|T<aphp>><dif>V<aph>>>|<row|<cell|>|<cell|<space|1em>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|\<mu\><aphp>-\<mu\><aph>|T<aphp>><dif>n<aph>>>>>
    </eqsplit>
  </equation>

  This is an expression for the total differential of <math|S> in the
  isolated system.

  In an isolated system, an equilibrium state cannot change spontaneously to
  a different state. Once the isolated system has reached an equilibrium
  state, an imagined finite change of any of the independent variables
  consistent with the constraints (a so-called <index|Virtual
  displacement><em|virtual displacement>) corresponds to an impossible
  process with an entropy decrease. Thus, the equilibrium state has the
  <em|maximum> entropy that is possible for the isolated system. In order for
  <math|S> to be a maximum, <math|<dif>S> must be zero for an infinitesimal
  change of any of the independent variables of the isolated system.

  This requirement is satisfied in the case of the multiphase system only if
  the coefficient of each term in the sums on the right side of Eq.
  <reference|dS=()dS(alpha)-()dV(alpha)+()dn(alpha)> is zero. Therefore, in
  an equilibrium state the temperature of each phase is equal to the
  temperature <math|T<aphp>> of the reference phase, the pressure of each
  phase is equal to <math|p<aphp>>, and the chemical potential in each phase
  is equal to <math|\<mu\><aphp>>. That is, at equilibrium the temperature,
  pressure, and chemical potential are uniform throughout the system. These
  are, respectively, the conditions described in Sec. <reference|2-eq states>
  of <em|thermal equilibrium>, <em|mechanical equilibrium>, and <em|transfer
  equilibrium>. These conditions must hold in order for a multiphase system
  of a pure substance without internal partitions to be in an equilibrium
  state, regardless of the process by which the system attains that state.

  <subsection|Simple derivation of equilibrium conditions>

  Here is a simpler, less formal derivation of the three equilibrium
  conditions in a multiphase system of a single substance.

  It is intuitively obvious that, unless there are special constraints (such
  as internal partitions), an equilibrium state must have thermal and
  mechanical equilibrium. A temperature difference between two phases would
  cause a spontaneous transfer of heat from the warmer to the cooler phase; a
  pressure difference would cause spontaneous flow of matter.

  When some of the substance is transferred from one phase to another under
  conditions of constant <math|T> and <math|p>, the intensive properties of
  each phase remains the same including the chemical potential. The chemical
  potential of a pure phase is the Gibbs energy per amount of substance in
  the phase. We know that in a closed system of constant <math|T> and
  <math|p> with expansion work only, the total Gibbs energy decreases during
  a spontaneous process and is constant during a reversible process (Eq.
  <reference|dG\<less\>=-SdT+Vdp+dw'>). The Gibbs energy will decrease only
  if there is a transfer of substance from a phase of higher chemical
  potential to a phase of lower chemical potential, and this will be a
  spontaneous change. No spontaneous transfer is possible if both phases have
  the same chemical potential, so this is a condition for an equilibrium
  state.

  <I|Equilibrium conditions!multiphase one component system@in a multiphase
  one-component system\|)>

  <subsection|Tall column of gas in a gravitational field><label|8-gas in
  gravity>

  <subindex|Field|gravitational><subindex|Gravitational|field>The earth's
  gravitational field is an example of an <index|External
  field><subindex|Field|external>external force field that acts on a system
  placed in it. Usually we ignore its effects on the state of the system. If,
  however, the system's vertical extent is considerable we must take the
  presence of the field into account to explain, for example, why gas
  pressure varies with elevation in an equilibrium state.

  A tall column of gas whose intensive properties are a function of elevation
  may be treated as an infinite number of uniform phases, each of
  infinitesimal vertical height. We can approximate this system with a
  vertical stack of many slab-shaped gas phases, each thin enough to be
  practically uniform in its intensive properties, as depicted in Fig.
  <reference|fig:8-slabs>.

  <\big-figure>
    <boxedfigure|<image|./08-SUP/slabs.eps||||> <capt|Closed system of
    constant-volume slab-shaped fluid phases stacked in the vertical
    direction. The shaded phase is reference phase
    <math|<pha><rprime|'>>.<label|fig:8-slabs>>>
  </big-figure|>

  The system can be isolated from the surroundings by confining the gas in a
  rigid adiabatic container. In order to be able to associate each of the
  thin slab-shaped phases with a definite constant elevation, we specify that
  the volume of each phase is constant so that in the rigid container the
  vertical thickness of a phase cannot change.

  <I|Equilibrium conditions!gravitational field@in a gravitational
  field\|reg>We can use the phase of lowest elevation as the reference phase
  <math|<pha><rprime|'>>, as indicated in the figure. We repeat the
  derivation of Sec. <reference|8-multiphase> with one change: for each phase
  <math|<pha>> the volume change <math|<dif>V<aph>> is set equal to zero.
  Then the second sum on the right side of Eq.
  <reference|dS=()dS(alpha)-()dV(alpha)+()dn(alpha)>, with terms proportional
  to <math|<dif>V<aph>>, drops out and we are left with

  <\equation>
    <label|dS=(gas col)><dif>S=<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|T<aphp>-T<aph>|T<aphp>><dif>S<aph>+<big|sum><rsub|<pha>\<ne\><pha><rprime|'>><frac|\<mu\><aphp>-\<mu\><aph>|T<aphp>><dif>n<aph>
  </equation>

  In the equilibrium state of the isolated system, <math|<dif>S> is equal to
  zero for an infinitesimal change of any of the independent variables. In
  this state, therefore, the coefficient of each term in the sums on the
  right side of Eq. <reference|dS=(gas col)> must be zero. We conclude that
  in an equilibrium state of a tall column of a pure gas, <em|the temperature
  and chemical potential are uniform throughout>. The equation, however,
  gives us no information about pressure.

  We will use this result to derive an expression for the dependence of the
  fugacity <math|<fug>> on elevation in an equilibrium state. We pick an
  arbitrary position such as the earth's surface for a reference elevation at
  which <math|h> is zero, and define the standard chemical potential
  <math|\<mu\><st><gas>> as the chemical potential of the gas under standard
  state conditions at this reference elevation. At <math|h=0>, the chemical
  potential and fugacity are related by Eq. <reference|mu=muo(g)+RT*ln(f/po)>
  which we write in the following form, indicating the elevation in
  parentheses:

  <\equation>
    <label|mu(0)=mu^o+RTln(f/p^o)>\<mu\><around|(|0|)>=\<mu\><st><gas>+R*T*ln
    <frac|<fug><around|(|0|)>|p<st>>
  </equation>

  Imagine a small sample of gas of mass <math|m> that is initially at
  elevation <math|h=0>. The vertical extent of this sample should be small
  enough for the variation of the gravitational force field within the sample
  to be negligible. The gravitational work needed to raise the gas to an
  arbitrary elevation <math|h> is <math|w<rprime|'>=m*g*h> (page
  <pageref|grav work with Del E(k)=0>). We assume this process is carried out
  reversibly at constant volume and without heat, so that there is no change
  in <math|T>, <math|p>, <math|V>, <math|S>, or <math|<fug>>. The internal
  energy <math|U> of the gas must increase by <math|m*g*h=n*M*g*h>, where
  <math|M> is the molar mass. Then, because the Gibbs energy <math|G> depends
  on <math|U> according to <math|G=U-T*S+p*V>, <math|G> must also increase by
  <math|n*M*g*h>.

  The chemical potential <math|\<mu\>> is the molar Gibbs energy <math|G/n>.
  During the elevation process, <math|f> remains the same and <math|\<mu\>>
  increases by <math|M*g*h>:

  <\gather>
    <tformat|<table|<row|<cell|\<mu\><around|(|h|)>=\<mu\><around|(|0|)>+M*g*h<cond|<around|(|<math|<fug><around|(|h|)>=<fug><around|(|0|)>><space|0.17em>|)>><eq-number><label|mu(h)=mu(0)+Mgh>>>>>
  </gather>

  From Eqs. <reference|mu(0)=mu^o+RTln(f/p^o)> and
  <reference|mu(h)=mu(0)+Mgh>, we can deduce the following general relation
  between chemical potential, fugacity, and elevation:

  <\gather>
    <tformat|<table|<row|<cell|\<mu\><around|(|h|)>=\<mu\><st><gas>+R*T*ln
    <frac|<fug><around|(|h|)>|p<st>>+M*g*h<cond|(p*u*r*e*g*a*s*i*n><nextcond|g*r*a*v*i*t*a*t*i*o*n*a*l*f*i*e*l*d)><eq-number><label|mu=muo+ln(f/po)+Mgh>>>>>
  </gather>

  Compare this relation with the equation that defines the fugacity when the
  effect of a gravitational field is negligible:
  <math|\<mu\>=\<mu\><st><gas>+R*T*ln <around|(|<fug>/p<st>|)>> (Eq.
  <reference|mu=muo(g)+RT*ln(f/po)><vpageref|mu=muo(g)+RT*ln(f/po)>). The
  additional term <math|M*g*h> is needed when the vertical extent of the gas
  is considerable.

  <\quote-env>
    <label|8-total chem. pot.>Some thermodynamicists call the expression on
    the right side of Eq. <reference|mu=muo+ln(f/po)+Mgh> the
    <subindex|Chemical potential|total><index|Gravitochemical
    potential>\Ptotal chemical potential\Q or \Pgravitochemical potential\Q
    and reserve the term \Pchemical potential\Q for the function
    <math|\<mu\><st><gas>+R*T*ln <around|(|<fug>/p<st>|)>>. With these
    definitions, in an equilibrium state the \Ptotal chemical potential\Q is
    the same at all elevations and the \Pchemical potential\Q decreases with
    increasing elevation.

    This book instead defines the chemical potential <math|\<mu\>> of a pure
    substance at any elevation as the molar Gibbs energy at that elevation,
    as recommended in a 2001 IUPAC technical report.<footnote|Ref.
    <cite|alberty-01>.> When the chemical potential is defined in this way,
    it has the same value at all elevations in an equilibrium state.
  </quote-env>

  We know that in the equilibrium state of the gas column, the chemical
  potential <math|\<mu\><around|(|h|)>> has the same value at each elevation
  <math|h>. Equation <reference|mu=muo+ln(f/po)+Mgh> shows that in order for
  this to be possible, the fugacity must decrease with increasing elevation.
  By equating expressions from Eq. <reference|mu=muo+ln(f/po)+Mgh> for
  <math|\<mu\><around|(|h|)>> at an arbitrary elevation <math|h>, and for
  <math|\<mu\><around|(|0|)>> at the reference elevation, we obtain

  <\equation>
    \<mu\><st><gas>+R*T*ln <frac|<fug><around|(|h|)>|p<st>>+M*g*h=\<mu\><st><gas>+R*T*ln
    <frac|<fug><around|(|0|)>|p<st>>
  </equation>

  Solving for <math|<fug><around|(|h|)>> gives

  <\gather>
    <tformat|<table|<row|<cell|<fug><around|(|h|)>=<fug><around|(|0|)>*e<rsup|-M*g*h/R*T><cond|(p*u*r*e*g*a*s*a*t*e*q*u*i*l*i*b*r*i*u*m><nextcond|i*n*g*r*a*v*i*t*a*t*i*o*n*a*l*f*i*e*l*d)><eq-number>>>>>
  </gather>

  If we treat the gas as ideal, so that the fugacity equals the pressure,
  this equation becomes

  <\gather>
    <tformat|<table|<row|<cell|p<around|(|h|)>=p<around|(|0|)>*e<rsup|-M*g*h/R*T><cond|(p*u*r*e*i*d*e*a*l*g*a*s*a*t*e*q*u*i*l*i*b*r*i*u*m><nextcond|i*n*g*r*a*v*i*t*a*t*i*o*n*a*l*f*i*e*l*d)><eq-number><label|p=po*exp(-Mgh/RT)>>>>>
  </gather>

  Equation <reference|p=po*exp(-Mgh/RT)> is the <index|Barometric
  formula><em|barometric formula> for a pure ideal gas. It shows that in the
  equilibrium state of a tall column of an ideal gas, the pressure decreases
  exponentially with increasing elevation.

  This derivation of the barometric formula has introduced a method that will
  be used in Sec. <reference|9-gas mixt in grav field> for dealing with
  <em|mixtures> in a gravitational field. There is, however, a shorter
  derivation based on <I|Newton's second law of motion\|reg>Newton's second
  law and not involving the chemical potential. Consider one of the thin
  slab-shaped phases of Fig. <reference|fig:8-slabs>. Let the density of the
  phase be <math|\<rho\>>, the area of each horizontal face be <math|<As>>,
  and the thickness of the slab be <math|\<up-delta\>*h>. The mass of the
  phase is then <math|m=\<rho\><As>\<up-delta\>*h>. The pressure difference
  between the top and bottom of the phase is
  <math|\<up-delta\>*<space|-0.17em>p>. Three vertical forces act on the
  phase: an upward force <math|p<As>> at its lower face, a downward force
  <math|-<around|(|p+\<up-delta\>*<space|-0.17em>p|)><As>> at its upper face,
  and a downward <subindex|Gravitational|force><subindex|Force|gravitational>gravitational
  force <math|-m*g=-\<rho\><As>g*\<up-delta\>*h>. If the phase is at rest,
  the net vertical force is zero: <math|p<As>-<around|(|p+\<up-delta\>*<space|-0.17em>p|)><As>-\<rho\><As>g*\<up-delta\>*h=0>,
  or <math|\<up-delta\>*<space|-0.17em>p=-\<rho\>*g*\<up-delta\>*h>. In the
  limit as the number of phases becomes infinite and <math|\<up-delta\>*h>
  and <math|\<up-delta\>*<space|-0.17em>p> become infinitesimal, this becomes

  <\gather>
    <tformat|<table|<row|<cell|<difp>=-\<rho\>*g<dif>h<cond|(f*l*u*i*d*a*t*e*q*u*i*l*i*b*r*i*u*m><nextcond|i*n*g*r*a*v*i*t*a*t*i*o*n*a*l*f*i*e*l*d)><eq-number><label|dp=-rho
    g dh>>>>>
  </gather>

  Equation <reference|dp=-rho g dh> is a general relation between changes in
  elevation and hydrostatic pressure in <em|any> fluid. To apply it to an
  ideal gas, we replace the density by <math|\<rho\>=n*M/V=M/V<m>=M*p/R*T>
  and rearrange to <math|<difp>/p=-<around|(|g*M/R*T|)><dif>h>. Treating
  <math|g> and <math|T> as constants, we integrate from <math|h=0> to an
  arbitrary elevation <math|h> and obtain the same result as Eq.
  <reference|p=po*exp(-Mgh/RT)>.

  <subsection|The pressure in a liquid droplet><label|8-liquid droplet>

  The equilibrium shape of a small liquid droplet surrounded by vapor of the
  same substance, when the effects of gravity and other external forces are
  negligible, is spherical. This is the result of the surface tension of the
  liquid\Ugas interface which acts to minimize the ratio of surface to
  volume. The interface acts somewhat like the stretched membrane of an
  inflated balloon, resulting in a greater <I|Pressure!liquid droplet@in a
  liquid droplet\|reg>pressure inside the droplet than the pressure of the
  vapor in equilibrium with it.

  We can derive the pressure difference by considering a closed system
  containing a spherical liquid droplet and surrounding vapor. We treat both
  phases as open subsystems. An infinitesimal change <math|<dif>U> of the
  internal energy is the sum of contributions from the liquid and gas phases
  and from the surface work <math|<g><dif><As>>, where <math|<g>> is the
  surface tension of the liquid\Ugas interface and <math|<As>> is the surface
  area of the droplet (Sec. <reference|5-surface work>):

  <\equation>
    <label|dU (droplet)>

    <\eqsplit>
      <tformat|<table|<row|<cell|<dif>U>|<cell|=<dif>U<rsup|<text|l>>+<dif>U<rsup|<text|g>>+<g><dif><As>>>|<row|<cell|>|<cell|=T<rsup|<text|l>><dif>S<rsup|<text|l>>-p<rsup|<text|l>><dif>V<rsup|<text|l>>+\<mu\><rsup|<text|l>><dif>n<rsup|<text|l>>>>|<row|<cell|>|<cell|<space|1em>+T<rsup|<text|g>><dif>S<rsup|<text|g>>-p<rsup|<text|g>><dif>V<rsup|<text|g>>+\<mu\><rsup|<text|g>><dif>n<rsup|<text|g>>+<g><dif><As>>>>>
    </eqsplit>
  </equation>

  Note that Eq. <reference|dU (droplet)> is not an expression for the total
  differential of <math|U>, because <math|V<rsup|<text|l>>> and <math|<As>>
  are not independent variables. A derivation by a procedure similar to the
  one used in Sec. <reference|8-multiphase> shows that at equilibrium the
  liquid and gas have equal temperatures and equal chemical potentials, and
  the pressure in the droplet is greater than the gas pressure by an amount
  that depends on <math|r>:

  <\equation>
    <label|p(l)=p(g)+2gamma/r>p<rsup|<text|l>>=p<rsup|<text|g>>+<frac|2<g>|r>
  </equation>

  Equation <reference|p(l)=p(g)+2gamma/r> is the <index|Laplace
  equation><em|Laplace equation>. The pressure difference is significant if
  <math|r> is small, and decreases as <math|r> increases. The limit
  <math|r><ra><math|\<infty\>> represents the flat surface of bulk liquid
  with <math|p<rsup|<text|l>>> equal to <math|p<rsup|<text|g>>>.

  The derivation of Eq. <reference|p(l)=p(g)+2gamma/r> is left as an exercise
  (Prob. 8.<reference|prb:8-droplet>). The Laplace equation is valid also for
  a liquid droplet in which the liquid and the surrounding gas may both be
  mixtures (Prob. 9.<reference|prb:9-droplet> on page
  <pageref|prb:9-droplet>).

  The Laplace equation can also be applied to the pressure in a gas
  <em|bubble> surrounded by liquid. In this case the liquid and gas phases
  switch roles, and the equation becomes <math|p<rsup|<text|g>>=p<rsup|<text|l>>+2<g>/r>.

  <subsection|The number of independent variables><label|8-ind variables>

  From this point on in this book, unless stated otherwise, the discussions
  of multiphase systems will implicitly assume the existence of thermal,
  mechanical, and transfer equilibrium. Equations will not explicitly show
  these equilibria as a condition of validity.

  In the rest of this chapter, we shall assume the state of each phase can be
  described by the usual variables: temperature, pressure, and amount. That
  is, variables such as elevation in a gravitational field, interface surface
  area, and extent of stretching of a solid, are not relevant.

  <subindex|Independent variables|number of>How many of the usual variables
  of an open multiphase one-substance equilibrium system are independent? To
  find out, we go through the following argument. In the absence of any kind
  of equilibrium, we could treat phase <math|<pha>> as having the three
  independent variables <math|T<aph>>, <math|p<aph>>, and <math|n<aph>>, and
  likewise for every other phase. A system of <math|P> phases without
  thermal, mechanical, or transfer equilibrium would then have <math|3*P>
  independent variables.

  <\quote-env>
    \ We must decide how to count the number of phases. It is usually of no
    thermodynamic significance whether a phase, with particular values of its
    intensive properties, is contiguous. For instance, splitting a crystal
    into several pieces is not usually considered to change the number of
    phases or the state of the system, provided the increased surface area
    makes no significant contribution to properties such as internal energy.
    Thus, the number of phases <math|P> refers to the number of different
    <em|kinds> of phases.
  </quote-env>

  Each independent relation resulting from equilibrium imposes a restriction
  on the system and reduces the number of independent variables by one. A
  two-phase system with thermal equilibrium has the single relation
  <math|T<bph>=T<aph>>. For a three-phase system, there are two such
  relations that are independent, for instance <math|T<bph>=T<aph>> and
  <math|T<gph>=T<aph>>. (The additional relation <math|T<gph>=T<bph>> is not
  independent since we may deduce it from the other two.) In general, thermal
  equilibrium gives <math|P-1> independent relations among temperatures.

  By the same reasoning, mechanical equilibrium involves <math|P-1>
  independent relations among pressures, and transfer equilibrium involves
  <math|P-1> independent relations among chemical potentials.

  The total number of independent relations for equilibrium is
  <math|3*<around|(|P-1|)>>, which we subtract from <math|3*P> (the number of
  independent variables in the absence of equilibrium) to obtain the number
  of independent variables in the equilibrium system:
  <math|3*P-3*<around|(|P-1|)>=3>. Thus, <em|an open single-substance system
  with any number of phases has at equilibrium three independent variables>.
  For example, in equilibrium states of a two-phase system we may vary
  <math|T>, <math|n<aph>>, and <math|n<bph>> independently, in which case
  <math|p> is a dependent variable; for a given value of <math|T>, the value
  of <math|p> is the one that allows both phases to have the same chemical
  potential.

  <subsection|The Gibbs phase rule for a pure substance><label|8-Gibbs phase
  rule>

  <I|Gibbs!phase rule!pure substance@for a pure
  substance\|reg><I|Phase!rule\|seeGibbs phase rule>The complete description
  of the state of a system must include the value of an <em|extensive>
  variable of each phase (e.g., the volume, mass, or amount) in order to
  specify how much of the phase is present. For an equilibrium system of
  <math|P> phases with a total of <math|3> independent variables, we may
  choose the remaining <math|3-P> variables to be <em|intensive>. The number
  of these intensive independent variables is called the <index|Degrees of
  freedom><newterm|number of degrees of freedom> or
  <index|Variance><newterm|variance>, <math|F>, of the system:

  <\gather>
    <tformat|<table|<row|<cell|F=3-P<cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|F=3-P
    (pure)>>>>>
  </gather>

  <\quote-env>
    The application of the phase rule to multicomponent systems will be taken
    up in Sec. <reference|13-phase rule>. Equation <reference|F=3-P (pure)>
    is a special case, for <math|C=1>, of the more general Gibbs phase rule
    <math|F=C-P+2>.
  </quote-env>

  We may interpret the variance <math|F> in either of two ways:

  <\itemize>
    <item><math|F> is the number of intensive variables needed to describe an
    equilibrium state, in addition to the amount of each phase;

    <item><math|F> is the maximum number of intensive properties that we may
    vary independently while the phases remain in equilibrium.
  </itemize>

  A system with two degrees of freedom is called <index|Bivariant
  system><em|bivariant>, one with one degree of freedom is <index|Univariant
  system><em|univariant>, and one with no degrees of freedom is
  <index|Invariant system><em|invariant>. For a system of a pure substance,
  these three cases correspond to one, two, and three phases respectively.
  For instance, a system of liquid and gaseous H<rsub|<math|2>>O (and no
  other substances) is univariant (<math|F=3-P=3-2=1>); we are able to
  independently vary only one intensive property, such as <math|T>, while the
  liquid and gas remain in equilibrium.

  <section|Phase Diagrams of Pure Substances><label|8-phase diagrams>

  A <I|Phase diagram!pure substance@of a pure substance\|reg><newterm|phase
  diagram> is a two-dimensional map showing which phase or phases are able to
  exist in an equilibrium state under given conditions. This chapter
  describes pressure\Uvolume and pressure\Utemperature phase diagrams for a
  single substance, and Chap. <reference|Chap. 13> will describe numerous
  types of phase diagrams for multicomponent systems.

  <subsection|Features of phase diagrams><label|8-features of phase diagrams>

  Two-dimensional phase diagrams for a single-substance system can be
  generated as projections of a three-dimensional surface in a coordinate
  system with Cartesian axes <math|p>, <math|V/n>, and <math|T>. A point on
  the three-dimensional surface corresponds to a physically-realizable
  combination of values, for an equilibrium state of the system containing a
  total amount <math|n> of the substance, of the variables <math|p>,
  <math|V/n>, and <math|T>.

  The concepts needed to interpret single-substance phase diagrams will be
  illustrated with carbon dioxide.

  Three-dimensional surfaces for carbon dioxide are shown at two different
  scales in Fig. <reference|fig:8-CO2><vpageref|fig:8-CO2>

  <\big-figure>
    <\boxedfigure>
      <image|./08-sup/CO2.eps||||>

      <\capt>
        Relations among <math|p>, <math|V/n>, and <math|T> for carbon
        dioxide.<space|.15em><footnote|Based on data in Refs.
        <cite|nist-webbook> and <cite|angus-76>.> Areas are labeled with the
        stable phase or phases (scf stands for supercritical fluid). The open
        circle indicates the critical point.

        \ (a)<nbsp>Three-dimensional <math|p>--<math|<around|(|V/n|)>>--<math|T>
        surface. The dashed curve is the critical isotherm at
        <math|T=304.21<K>>, and the dotted curve is a portion of the critical
        isobar at <math|p=73.8<br>>.

        \ (b)<nbsp>Pressure--volume phase diagram (projection of the surface
        onto the <math|p>--<math|<around|(|V/n|)>> plane).

        \ (c)<nbsp>Pressure--temperature phase diagram (projection of the
        surface onto the <math|p>--<math|T> plane).<label|fig:8-CO2>
      </capt>
    </boxedfigure>
  </big-figure|>

  and in Fig. <reference|fig:8-CO2-2><vpageref|fig:8-CO2-2>.

  <\big-figure>
    <boxedfigure|<image|./08-sup/CO2-2.eps||||> <capt|Three-dimensional
    <math|p>--<math|<around|(|V/n|)>>--<math|T> surface for
    CO<rsub|<math|2>>, magnified along the <math|V/n> axis compared to Fig.
    <reference|fig:8-CO2>. The open circle is the critical point, the dashed
    curve is the critical isotherm, and the dotted curve is a portion of the
    critical isobar.<label|fig:8-CO2-2>>>
  </big-figure|>

  In these figures, some areas of the surface are labeled with a single
  physical state: solid, liquid, gas, or supercritical fluid. A point in one
  of these areas corresponds to an equilibrium state of the system containing
  a single phase of the labeled physical state. The shape of the surface in
  this one-phase area gives the equation of state of the phase (i.e., the
  dependence of one of the variables on the other two). A point in an area
  labeled with two physical states corresponds to two coexisting phases. The
  <subindex|Triple|line><newterm|triple line> is the locus of points for all
  possible equilibrium systems of three coexisting phases, which in this case
  are solid, liquid, and gas. A point on the triple line can also correspond
  to just one or two phases (see the discussion on page<nbsp><pageref|triple
  line>).

  The two-dimensional projections shown in Figs. <reference|fig:8-CO2>(b) and
  <reference|fig:8-CO2>(c) are pressure\Uvolume and pressure\Utemperature
  phase diagrams. Because all phases of a multiphase equilibrium system have
  the same temperature and pressure,<footnote|This statement assumes there
  are no constraints such as internal adiabatic partitions.> the projection
  of each two-phase area onto the pressure\Utemperature diagram is a curve,
  called a <index|Coexistence curve><newterm|coexistence curve> or
  <subindex|Phase|boundary><newterm|phase boundary>, and the projection of
  the triple line is a point, called a <subindex|Triple|point><newterm|triple
  point>.

  How may we use a phase diagram? The two axes represent values of two
  independent variables, such as <math|p> and <math|V/n> or <math|p> and
  <math|T>. For given values of these variables, we place a point on the
  diagram at the intersection of the corresponding coordinates; this is the
  <index|System point><newterm|system point>. Then depending on whether the
  system point falls in an area or on a coexistence curve, the diagram tells
  us the number and kinds of phases that can be present in the equilibrium
  system.

  If the system point falls within an area labeled with the physical state of
  a <em|single> phase, only that one kind of phase can be present in the
  equilibrium system. A system containing a pure substance in a single phase
  is bivariant (<math|F=3-1=2>), so we may vary two intensive properties
  independently. That is, the system point may move independently along two
  coordinates (<math|p> and <math|V/n>, or <math|p> and <math|T>) and still
  remain in the one-phase area of the phase diagram. When <math|V> and
  <math|n> refer to a single phase, the variable <math|V/n> is the molar
  volume <math|V<m>> in the phase.

  If the system point falls in an area of the pressure\Uvolume phase diagram
  labeled with symbols for <em|two> phases, these two phases coexist in
  equilibrium. The phases have the same pressure and different molar volumes.
  To find the molar volumes of the individual phases, we draw a horizontal
  line of constant pressure, called a <index|Tie line><newterm|tie
  line>,<label|tie line> through the system point and extending from one edge
  of the area to the other. The horizontal position of each end of the tie
  line, where it terminates at the boundary with a one-phase area, gives the
  molar volume in that phase in the two-phase system. For an example of a tie
  line, see Fig. <reference|fig:8-tie line><vpageref|fig:8-tie line>.

  The triple line on the pressure\Uvolume diagram represents the range of
  values of <math|V/n> in which three phases (solid, liquid, and gas) can
  coexist at equilibrium.<footnote|<index|Helium>Helium is the only substance
  lacking a solid--liquid--gas triple line. When a system containing the
  coexisting liquid and gas of <rsup|<math|4>>He is cooled to <math|2.17<K>>,
  a triple point is reached in which the third phase is a liquid called
  He-II, which has the unique property of superfluidity. It is only at high
  pressures (<math|10<br>> or greater) that solid helium can exist.> A
  three-phase one-component system is invariant (<math|F=3-3=0>); there is
  only one temperature (the triple-point temperature
  <math|T<rsub|<text|tp>>>) and one pressure (the triple-point pressure
  <math|p<rsub|<text|tp>>>) at which the three phases can coexist. The values
  of <math|T<rsub|<text|tp>>> and <math|p<rsub|<text|tp>>> are unique to each
  substance, and are shown by the position of the triple point on the
  pressure\Utemperature phase diagram. The molar volumes in the three
  coexisting phases are given by the values of <math|V/n> at the three points
  on the pressure\Uvolume diagram where the triple line touches a one-phase
  area. These points are at the two ends and an intermediate position of the
  triple line. If<label|triple line>the system point is at either end of the
  triple line, only the one phase of corresponding molar volume at
  temperature <math|T<rsub|<text|tp>>> and pressure <math|p<rsub|<text|tp>>>
  can be present. When the system point is on the triple line anywhere
  between the two ends, either two or three phases can be present. If the
  system point is at the position on the triple line corresponding to the
  phase of intermediate molar volume, there might be only that one phase
  present.

  At high pressures, a substance may have additional triple points for two
  solid phases and the liquid, or for three solid phases. This is illustrated
  by the pressure\Utemperature phase diagram of H<rsub|<math|2>>O in Fig.
  <reference|fig:8-ice high p><vpageref|fig:8-ice high p>,

  <\big-figure>
    <boxedfigure|<image|./08-SUP/ice-Tp.eps||||> <capt|High-pressure
    pressure--temperature phase diagram of
    H<rsub|<math|2>>O<@>.<space|.15em><footnote|Based on data in Refs.
    <cite|eisenberg-69>, Table 3.5, and <cite|pist-63>.> The roman numerals
    designate seven forms of ice.<label|fig:8-ice high p>>>
  </big-figure|>

  which extends to pressures up to <math|30<units|k*b*a*r>>. (On this scale,
  the liquid\Ugas coexistence curve lies too close to the horizontal axis to
  be visible.) The diagram shows seven different solid phases of
  H<rsub|<math|2>>O differing in crystal structure and designated ice I,
  <index|Ice, high pressure forms>ice II, and so on. Ice I is the ordinary
  form of ice, stable below <math|2<br>>. On the diagram are four triple
  points for two solids and the liquid and three triple points for three
  solids. Each triple point is invariant. Note how H<rsub|<math|2>>O can
  exist as solid ice VI or ice VII above its standard melting point of
  <math|273<K>> if the pressure is high enough (\Phot ice\Q).

  <subsection|Two-phase equilibrium><label|8-ph diagrams - 2 phase eqm>

  A system containing two phases of a pure substance in equilibrium is
  univariant. Both phases have the same values of <math|T> and of <math|p>,
  but these values are not independent because of the requirement that the
  phases have equal chemical potentials. We may vary only one intensive
  variable of a pure substance (such as <math|T> or <math|p>) independently
  while two phases coexist in equilibrium.

  At a given temperature, the pressure at which solid and gas or liquid and
  gas are in equilibrium is called the <index|Vapor
  pressure><I|Pressure!vapor\|seeVapor pressure><newterm|vapor pressure> or
  <subindex|Saturation|vapor pressure><subindex|Vapor
  pressure|saturation><newterm|saturation vapor pressure> of the solid or
  liquid.<footnote|In a system of more than one substance, <em|vapor
  pressure> can refer to the partial pressure of a substance in a gas mixture
  equilibrated with a solid or liquid of that substance. The effect of total
  pressure on vapor pressure will be discussed in Sec. <reference|12-effect
  of p on fug>. This book refers to the <em|saturation> vapor pressure of a
  liquid when it is necessary to indicate that it is the pure liquid and pure
  gas phases that are in equilibrium at the same pressure.> The vapor
  pressure of a solid is sometimes called the
  <subindex|Sublimation|pressure><subindex|Pressure|sublimation><newterm|sublimation
  pressure>. We may measure the vapor pressure of a liquid at a fixed
  temperature with a simple device called an <index|Isoteniscope>isoteniscope
  (Fig. <reference|fig:8-isoteniscope><vpageref|fig:8-isoteniscope>).

  <\big-figure>
    <boxedfigure|<image|./08-SUP/isotenis.eps||||> <capt|An isoteniscope. The
    liquid to be investigated is placed in the vessel and
    <with|font-family|ss|U>-tube, as indicated by shading, and maintained at
    a fixed temperature in the bath. The pressure in the side tube is reduced
    until the liquid boils gently and its vapor sweeps out the air. The
    pressure is adjusted until the liquid level is the same in both limbs of
    the <with|font-family|ss|U>-tube; the vapor pressure of the liquid is
    then equal to the pressure in the side tube, which can be measured with a
    manometer.<label|fig:8-isoteniscope>>>
  </big-figure|>

  At a given pressure, the <index|Melting point><newterm|melting point> or
  <index|Freezing point><newterm|freezing point> is the temperature at which
  solid and liquid are in equilibrium, the <index|Boiling
  point><newterm|boiling point> or <subindex|Saturation|temperature><newterm|saturation
  temperature> is the temperature at which liquid and gas are in equilibrium,
  and the <subindex|Sublimation|temperature><newterm|sublimation temperature>
  or <subindex|Sublimation|point><newterm|sublimation point> is the
  temperature at which solid and gas are in equilibrium.

  The relation between temperature and pressure in a system with two phases
  in equilibrium is shown by the coexistence curve separating the two
  one-phase areas on the pressure\Utemperature diagram (see Fig.
  <reference|fig:8-pT phase diagram of H2O><vpageref|fig:8-pT phase diagram
  of H2O>).

  <\big-figure>
    <boxedfigure|<image|./08-SUP/H2O-pT.eps||||> <capt|Pressure--temperature
    phase diagram of H<rsub|<math|2>>O<@>. (Based on data in Ref.
    <cite|nist-webbook>.)<label|fig:8-pT phase diagram of H2O>>>
  </big-figure|>

  Consider the liquid\Ugas curve. If we think of <math|T> as the independent
  variable, the curve is a <subindex|Vapor
  pressure|curve><newterm|vapor-pressure curve> showing how the vapor
  pressure of the liquid varies with temperature. If, however, <math|p> is
  the independent variable, then the curve is a <subindex|Boiling
  point|curve><newterm|boiling-point curve> showing the dependence of the
  boiling point on pressure.

  The <subindex|Normal|melting point><subindex|Normal|boiling
  point><em|normal> melting point or boiling point refers to a pressure of
  one atmosphere, and the <subindex|Standard|boiling
  point><subindex|Standard|melting point><em|standard> melting point or
  boiling point refers to the standard pressure. Thus, the normal boiling
  point of water (<math|99.97<units|<degC>>>) is the boiling point at
  <math|1<units|a*t*m>>; this temperature is also known as the <index|Steam
  point><em|steam point>. The standard boiling point of water
  (<math|99.61<units|<degC>>>) is the boiling point at the slightly lower
  pressure of <math|1<br>>.

  Coexistence curves will be discussed further in Sec.
  <reference|8-coexistence curves>.

  <subsection|The critical point><label|8-crit pt>

  Every substance has a certain temperature, the
  <subindex|Critical|temperature><subindex|Temperature|critical><newterm|critical
  temperature>, above which only one fluid phase can exist at any volume and
  pressure (Sec. <reference|2-fluids>). The <I|Critical!point!pure
  substance@of a pure substance\|reg><newterm|critical point> is the point on
  a phase diagram corresponding to liquid\Ugas coexistence at the critical
  temperature, and the <subindex|Critical|pressure><newterm|critical
  pressure> is the pressure at this point.

  To observe the critical point of a substance experimentally, we can
  evacuate a glass vessel, introduce an amount of the substance such that
  <math|V/n> is approximately equal to the molar volume at the critical
  point, seal the vessel, and raise the temperature above the critical
  temperature. The vessel now contains a single fluid phase. When the
  substance is slowly cooled to a temperature slightly above the critical
  temperature, it exhibits a cloudy appearance, a phenomenon
  called<label|crit opal><subindex|Critical|opalescence><em|critical
  opalescence> (Fig. <reference|fig:8-critical_opalescence><vpageref|fig:8-critical_opalescence>).

  <\big-figure>
    <\boxedfigure>
      <\minipage|t|14cm>
        \;

        <\with|par-mode|center>
          <minipage|t|61pt| <with|par-mode|center|<image|./08-SUP/critical/critical-a.eps||||><next-line><vspace|.15cm>(a)>>

          <space|.4cm><minipage|t|60pt| <with|par-mode|center|<image|./08-SUP/critical/critical-b.eps||||><next-line><vspace|.15cm>(b)>>

          <space|.4cm><minipage|t|61pt| <with|par-mode|center|<image|./08-SUP/critical/critical-c.eps||||><next-line><vspace|.15cm>(c)>>

          <space|.4cm><minipage|t|62pt| <with|par-mode|center|<image|./08-SUP/critical/critical-d.eps||||><next-line><vspace|.15cm>(d)>>

          <minipage|t|5pt|<rotatebox|90|<miniscule><with|font-family|sf|<definecolor|darkgray|gray|.5><with|color|darkgray|PHOTOS
          BY RAY RAKOW>>>>
        </with>
      </minipage>

      <\capt>
        Glass bulb filled with CO<rsub|<math|2>> at a value of <math|V/n>
        close to the critical value, viewed at four different temperatures.
        The three balls have densities less than, approximately equal to, and
        greater than the critical density.<space|.15em><footnote|Ref.
        <cite|sengers-68>.>

        (a) Supercritical fluid at a temperature above the critical
        temperature.

        (b) Intense opalescence just above the critical temperature.

        (c) Meniscus formation slightly below the critical temperature;
        liquid and gas of nearly the same density.

        (d) Temperature well below the critical temperature; liquid and gas
        of greatly different densities.<label|fig:8-critical_opalescence>
      </capt>
    </boxedfigure>
  </big-figure|>

  The opalescence is the scattering of light caused by large local density
  fluctuations. At the critical temperature, a meniscus forms between liquid
  and gas phases of practically the same density. With further cooling, the
  density of the liquid increases and the density of the gas decreases.

  At temperatures above the critical temperature and pressures above the
  critical pressure, the one existing fluid phase is called a
  <index|Supercritical fluid><subindex|Fluid|supercritical><newterm|supercritical
  fluid>. Thus, a supercritical fluid of a pure substance is a fluid that
  does not undergo a phase transition to a different fluid phase when we
  change the pressure at constant temperature or change the temperature at
  constant pressure.<footnote|If, however, we increase <math|p> at constant
  <math|T>, the supercritical fluid will change to a solid. In the phase
  diagram of H<rsub|<math|2>>O, the coexistence curve for ice VII and liquid
  shown in Fig. <reference|fig:8-ice high p> extends to a higher temperature
  than the critical temperature of <math|647<K>>. Thus, supercritical water
  can be converted to ice VII by isothermal compression.>

  A fluid in the supercritical region can have a density comparable to that
  of the liquid, and can be more compressible than the liquid. Under
  supercritical conditions, a substance is often an excellent solvent for
  solids and liquids. By varying the pressure or temperature, the solvating
  power can be changed; by reducing the pressure isothermally, the substance
  can be easily removed as a gas from dissolved solutes. These properties
  make supercritical fluids useful for chromatography and solvent extraction.

  The critical temperature of a substance can be measured quite accurately by
  observing the appearance or disappearance of a liquid\Ugas meniscus, and
  the critical pressure can be measured at this temperature with a
  high-pressure manometer. To evaluate the density at the critical point, it
  is best to extrapolate the mean density of the coexisting liquid and gas
  phases, <math|<around|(|\<rho\><rsup|<text|l>>+\<rho\><rsup|<text|g>>|)>/2>,
  to the critical temperature as illustrated in Fig.
  <reference|fig:8-rectilinear><vpageref|fig:8-rectilinear>.

  <\big-figure>
    <boxedfigure|<image|./08-SUP/CO2-SF6-rect.eps||||> <capt|Densities of
    coexisting gas and liquid phases close to the critical point as functions
    of temperature for (a) CO<rsub|<math|2>>;<space|.15em><footnote|Based on
    data in Ref. <cite|michels-37>.> (b) SF<rsub|<math|6>>.<space|.15em><footnote|Data
    of Ref. <cite|pestak-87>, Table VII.> Experimental gas densities are
    shown by open squares and experimental liquid densities by open
    triangles. The mean density at each experimental temperature is shown by
    an open circle. The open diamond is at the critical temperature and
    critical density.<label|fig:8-rectilinear>>>
  </big-figure|>

  The observation that the mean density closely approximates a linear
  function of temperature, as shown in the figure, is known as the
  <I|Law!rectilinear@of rectilinear diameters\|reg><index|Rectilinear
  diameters, law of><newterm|law of rectilinear diameters>, or the
  <I|Law!Cailletet@of Cailletet and Matthias\|reg><index|Cailletet and
  Matthias, law of>law of Cailletet and Matthias. This law is an
  approximation, as can be seen by the small deviation of the mean density of
  SF<rsub|<math|6>> from a linear relation very close to the critical point
  in Fig. <reference|fig:8-rectilinear>(b). This failure of the law of
  rectilinear diameters is predicted by recent theoretical
  treatments.<footnote|Refs. <cite|wang-07> and <cite|behnejad-10>.>

  <subsection|The lever rule><label|8-lever rule>

  Consider a single-substance system whose system point <I|Lever rule!one
  substance@for one substance in two phases\|reg>is in a two-phase area of a
  pressure\Uvolume phase diagram. How can we determine the amounts in the two
  phases?

  As an example, let the system contain a fixed amount <math|n> of a pure
  substance divided into liquid and gas phases, at a temperature and pressure
  at which these phases can coexist in equilibrium. When heat is transferred
  into the system at this <math|T> and <math|p>, some of the liquid vaporizes
  by a liquid\Ugas phase transition and <math|V> increases; withdrawal of
  heat at this <math|T> and <math|p> causes gas to condense and <math|V> to
  decrease. The molar volumes and other intensive properties of the
  individual liquid and gas phases remain constant during these changes at
  constant <math|T> and <math|p>. On the pressure\Uvolume phase diagram of
  Fig. <reference|fig:8-tie line><vpageref|fig:8-tie line>,

  <\big-figure>
    <boxedfigure|<image|./08-SUP/tie-line.eps||||> <capt|Tie line (dashed) at
    constant <math|T> and <math|p> in the liquid--gas area of a
    pressure--volume phase diagram. Points A and B are at the ends of the tie
    line, and point S is a system point on the tie line.
    <math|L<rsup|<text|l>>> and <math|L<rsup|<text|g>>> are the lengths AS
    and SB, respectively.<label|fig:8-tie line>>>
  </big-figure|>

  the volume changes correspond to movement of the system point to the right
  or left along the tie line AB.

  When enough heat is transferred into the system to vaporize all of the
  liquid at the given <math|T> and <math|p>, the system point moves to point
  B at the right end of the tie line. <math|V/n> at this point must be the
  same as the molar volume of the gas, <math|V<m><rsup|<text|g>>>. We can see
  this because the system point could have moved from within the one-phase
  gas area to this position on the boundary without undergoing a phase
  transition.

  When, on the other hand, enough heat is transferred out of the system to
  condense all of the gas, the system point moves to point A at the left end
  of the tie line. <math|V/n> at this point is the molar volume of the
  liquid, <math|V<m><rsup|<text|l>>>.

  When the system point is at position S on the tie line, both liquid and gas
  are present. Their amounts must be such that the total volume is the sum of
  the volumes of the individual phases, and the total amount is the sum of
  the amounts in the two phases:

  <\equation>
    V=V<rsup|<text|l>>+V<rsup|<text|g>>=n<rsup|<text|l>>V<m><rsup|<text|l>>+n<rsup|<text|g>>V<m><rsup|<text|g>>
  </equation>

  <\equation>
    n=n<rsup|<text|l>>+n<rsup|<text|g>>
  </equation>

  The value of <math|V/n> at the system point is then given by the equation

  <\equation>
    <frac|V|n>=<frac|n<rsup|<text|l>>V<m><rsup|<text|l>>+n<rsup|<text|g>>V<m><rsup|<text|g>>|n<rsup|<text|l>>+n<rsup|<text|g>>>
  </equation>

  which can be rearranged to

  <\equation>
    <label|n(l)L(l)=n(g)L(g)>n<rsup|<text|l>><around*|(|V<m><rsup|<text|l>>-<frac|V|n>|)>=n<rsup|<text|g>><around*|(|<frac|V|n>-V<m><rsup|<text|g>>|)>
  </equation>

  The quantities <math|V<m><rsup|<text|l>>-V/n> and
  <math|V/n-V<m><rsup|<text|g>>> are the lengths <math|L<rsup|<text|l>>> and
  <math|L<rsup|<text|g>>>, respectively, defined in the figure and measured
  in units of <math|V/n>. This gives us the <index|Lever rule><newterm|lever
  rule> for liquid\Ugas equilibrium:<footnote|The relation is called the
  lever rule by analogy to a stationary mechanical lever, each end of which
  has the same value of the product of applied force and distance from the
  fulcrum.>

  <\gather>
    <tformat|<table|<row|<cell|n<rsup|<text|l>>L<rsup|<text|l>>=n<rsup|<text|g>>L<rsup|<text|g>><space|1em><tx|o*r><space|1em><frac|n<rsup|<text|g>>|n<rsup|<text|l>>>=<frac|L<rsup|<text|l>>|L<rsup|<text|g>>><cond|(c*o*e*x*i*s*t*i*n*g*l*i*q*u*i*d*a*n*d*g*a*s><nextcond|p*h*a*s*e*s*o*f*a*p*u*r*e*s*u*b*s*t*a*n*c*e)><eq-number><label|l-g
    lever rule>>>>>
  </gather>

  In Fig. <reference|fig:8-tie line> the system point S is positioned on the
  tie line two thirds of the way from the left end, making length
  <math|L<rsup|<text|l>>> twice as long as <math|L<rsup|<text|g>>>. The lever
  rule then gives the ratio of amounts: <math|n<rsup|<text|g>>/n<rsup|<text|l>>=L<rsup|<text|l>>/L<rsup|<text|g>>=2>.
  One-third of the total amount is liquid and two-thirds is gas.

  We cannot apply the lever rule to a point on the triple line, because we
  need more than the value of <math|V/n> to determine the relative amounts
  present in three phases.

  <\quote-env>
    \ <subindex|Lever rule|general form>We can derive a more general form of
    the lever rule that will be needed in Chap. <reference|Chap. 13> for
    phase diagrams of multicomponent systems. This general form can be
    applied to any two-phase area of a two-dimensional phase diagram in which
    a tie-line construction is valid, with the position of the system point
    along the tie line given by the variable

    <\equation>
      F<defn><frac|a|b>
    </equation>

    where <math|a> and <math|b> are extensive state functions. (In the
    pressure--volume phase diagram of Fig. <reference|fig:8-tie line>, these
    functions are <math|a=V> and <math|b=n> and the system point position is
    given by <math|F=V/n>.) We repeat the steps of the derivation above,
    labeling the two phases by superscripts <math|<pha>> and <math|<phb>>
    instead of <math|l> and <math|g>. The relation corresponding to Eq.
    <reference|n(l)L(l)=n(g)L(g)> is

    <\equation>
      <label|tie line eq>b<aph><around|(|F<aph>-F|)>=b<bph><around|(|F-F<bph>|)>
    </equation>

    If <math|L<aph>> and <math|L<bph>> are lengths measured along the tie
    line from the system point to the ends of the tie line at single phases
    <math|<pha>> and <math|<phb>>, respectively, Eq. <reference|tie line eq>
    is equivalent to the general lever rule

    <\equation>
      <label|lever rule>b<aph>L<aph>=b<bph>L<bph><space|2em><tx|o*r><space|2em><frac|b<bph>|b<aph>>=<frac|L<aph>|L<bph>>
    </equation>
  </quote-env>

  <subsection|Volume properties>

  Figure <reference|fig:8-pV phase diagram of H2O><vpageref|fig:8-pV phase
  diagram of H2O>

  <\big-figure>
    <boxedfigure|<image|./08-SUP/H2O-pV.eps||||> <capt|Isotherms for the
    fluid phases of H<rsub|<math|2>>O<@>.<space|.15em><footnote|Based on data
    in Ref. <cite|nist-webbook>.> The open circle indicates the critical
    point, the dashed curve is the critical isotherm at
    <math|373.95<units|<degC>>>, and the dotted curve encloses the two-phase
    area of the pressure--volume phase diagram. The triple line lies too
    close to the bottom of the diagram to be visible on this
    scale.<label|fig:8-pV phase diagram of H2O>>>
  </big-figure|>

  is a pressure\Uvolume phase diagram for H<rsub|<math|2>>O<@>. On the
  diagram are drawn <index|Isotherm><em|isotherms> (curves of constant
  <math|T>). These isotherms define the shape of the three-dimensional
  <math|p>\U<math|<around|(|V/n|)>>\U<math|T> surface. The area containing
  the horizontal isotherm segments is the two-phase area for coexisting
  liquid and gas phases. The boundary of this area is defined by the dotted
  curve drawn through the ends of the horizontal segments. The one-phase
  liquid area lies to the left of this curve, the one-phase gas area lies to
  the right, and the critical point lies at the top.

  The diagram contains the information needed to evaluate the molar volume at
  any temperature and pressure in the one-phase region and the derivatives of
  the molar volume with respect to temperature and pressure. At a system
  point in the one-phase region, the slope of the isotherm passing through
  the point is the partial derivative <math|<pd|p|V<m>|T>>. Since the
  <subindex|Isothermal|compressibility>isothermal compressibility is given by
  <math|<kT>=-<around|(|1/V<m>|)><pd|V<m>|p|T>>, we have

  <\equation>
    <kT>=-<frac|1|V<m>\<times\><tx|s*l*o*p*e*o*f*i*s*o*t*h*e*r*m>>
  </equation>

  We see from Fig. <reference|fig:8-pV phase diagram of H2O> that the slopes
  of the isotherms are large and negative in the liquid region, smaller and
  negative in the gas and <index|Supercritical
  fluid><subindex|Fluid|supercritical>supercritical fluid regions, and
  approach zero at the critical point. Accordingly, the
  <subindex|Isothermal|compressibility>isothermal compressibility of the gas
  and the supercritical fluid is much greater than that of the liquid,
  approaching infinity at the critical point. The critical opalescence seen
  in Fig. <reference|fig:8-critical_opalescence> is caused by local density
  fluctuations, which are large when <math|<kT>> is large.

  Figure <reference|fig:8-T-V phase diagram of H2O><vpageref|fig:8-T-V phase
  diagram of H2O>

  <\big-figure>
    <\boxedfigure>
      <image|./08-SUP/H2O-T-V.eps||||>

      <\capt>
        Isobars for the fluid phases of H<rsub|<math|2>>O<@>.<space|.15em><footnote|Based
        on data in Ref. <cite|nist-webbook>.> The open circle indicates the
        critical point, the dashed curve is the critical isobar at
        <math|220.64<br>>, and the dotted curve encloses the two-phase area
        of the temperature--volume phase diagram.

        \ Solid curves: a, <math|p=200<br>>; b, <math|p=210<br>>; c,
        <math|p=230<br>>; d, <math|p=240<br>>.<label|fig:8-T-V phase diagram
        of H2O>
      </capt>
    </boxedfigure>
  </big-figure|>

  shows isobars for H<rsub|<math|2>>O instead of isotherms. At a system point
  in the one-phase region, the slope of the isobar passing through the point
  is the partial derivative <math|<pd|T|V<m>|<space|-0.17em>p>>. The
  <index|Cubic expansion coefficient>cubic expansion coefficient
  <math|\<alpha\>> is equal to <math|<around|(|1/V<m>|)><pd|V<m>|T|<space|-0.17em>p>>,
  so we have

  <\equation>
    \<alpha\>=<frac|1|V<m>\<times\><tx|s*l*o*p*e*o*f*i*s*o*b*a*r>>
  </equation>

  The figure shows that the slopes of the isobars are large and positive in
  the liquid region, smaller and negative in the gas and <index|Supercritical
  fluid><subindex|Fluid|supercritical>supercritical fluid regions, and
  approach zero at the critical point. Thus the gas and the supercritical
  fluid have much larger cubic expansion coefficients than the liquid. The
  value of <math|\<alpha\>> approaches infinity at the critical point,
  meaning that in the critical region the density distribution is greatly
  affected by temperature gradients. This may account for the low position of
  the middle ball in Fig. <reference|fig:8-critical_opalescence>(b).

  <section|Phase Transitions><label|8-phase transitions>

  Recall (Sec. <reference|2-phase coexistence>) that an equilibrium phase
  transition of a pure substance is a process in which some or all of the
  substance is transferred from one coexisting phase to another at constant
  temperature and pressure.

  <subsection|Molar transition quantities><label|8-molar trans quantities>

  The quantity <math|<Delsub|v*a*p>H> is the molar enthalpy change for the
  reversible process in which liquid changes to gas <em|at a temperature and
  pressure at which the two phases coexist at equilibrium>. This quantity is
  called the <I|Enthalpy!vaporization@of vaporization!molar\|reg><subindex|Vaporization|molar
  enthalpy of><newterm|molar enthalpy of vaporization>.<footnote|Because
  <math|<Delsub|v*a*p>H> is an enthalpy <em|change> per amount of
  vaporization, it would be more accurate to call it the ``molar enthalpy
  change of vaporization.''> Since the pressure is constant during the
  process, <math|<Delsub|v*a*p>H> is equal to the heat per amount of
  vaporization (Eq. <reference|delH=q (dp=0)>). Hence, <math|<Delsub|v*a*p>H>
  is also called the <subindex|Vaporization|molar heat of><newterm|molar heat
  of vaporization>.

  <\quote-env>
    \ The first edition of this book used the notation
    <math|<Delsub|v*a*p>H<m>>, with subscript m, in order to make it clear
    that it refers to a <em|molar> enthalpy of vaporization. The most recent
    edition of the <index|IUPAC Green Book>IUPAC Green Book<footnote|Ref.
    <cite|greenbook-3>, p. 58.> recommends that <math|<Delsub|p>> be
    interpreted as an operator symbol: <math|<Delsub|p><defn>\<partial\>/\<partial\>*\<xi\><rsub|<text|p>>>,
    where ``p'' is the abbreviation for a process at constant <math|T> and
    <math|p> (in this case ``vap'') and <math|\<xi\><rsub|<text|p>>> is its
    advancement. Thus <math|<Delsub|v*a*p>H> is the same as
    <math|<pd|H|\<xi\><rsub|<text|vap>>|T,p>> where
    <math|\<xi\><rsub|<text|vap>>> is the amount of liquid changed to gas.
  </quote-env>

  Here is a list of symbols for the molar enthalpy changes of various
  equilibrium phase transitions:

  <tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|l>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|l>|<cwith|1|-1|2|2|cell-rborder|0ln>|<cwith|1|-1|1|-1|cell-valign|c>|<table|<row|<cell|<math|<Delsub|v*a*p>H>>|<cell|molar
  enthalpy of vaporization (liquid<math|<ra>>gas)>>|<row|<cell|<math|<Delsub|s*u*b>H>>|<cell|molar
  enthalpy of sublimation (solid<math|<ra>>gas)>>|<row|<cell|<math|<Delsub|f*u*s>H>>|<cell|molar
  enthalpy of fusion (solid<math|<ra>>liquid)>>|<row|<cell|<math|<Delsub|t*r*s>H>>|<cell|molar
  enthalpy of a transition between any two phases in general>>>>>

  <no-indent>Molar enthalpies of vaporization, sublimation, and fusion are
  <em|positive>. The reverse processes of condensation
  (gas<math|<ra>>liquid), condensation or deposition (gas<math|<ra>>solid),
  and freezing (liquid<math|<ra>>solid) have <em|negative> enthalpy changes.

  The subscripts in the list above are also used for other molar transition
  quantities. Thus, there is the molar entropy of vaporization
  <math|<Delsub|v*a*p>S>, the molar internal energy of sublimation
  <math|<Delsub|s*u*b>U>, and so on.

  A molar transition quantity of a pure substance is the change of an
  extensive property divided by the amount transferred between the phases.
  For example, when an amount <math|n> in a liquid phase is allowed to
  vaporize to gas at constant <math|T> and <math|p>, the enthalpy change is
  <math|<Del>H=n*H<m><rsup|<text|g>>-n*H<m><rsup|<text|l>>> and the molar
  enthalpy of vaporization is

  <\gather>
    <tformat|<table|<row|<cell|<Delsub|v*a*p>H=<frac|<Del>H|n>=H<m><rsup|<text|g>>-H<m><rsup|<text|l>><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|del(vap)Hm=del(vap)H/n>>>>>
  </gather>

  In other words, <math|<Delsub|v*a*p>H> is the enthalpy change per amount
  vaporized and is also the difference between the molar enthalpies of the
  two phases.

  A molar property of a phase, being intensive, usually depends on two
  independent intensive variables such as <math|T> and <math|p>. Despite the
  fact that <math|<Delsub|v*a*p>H> is the difference of the two molar
  properties <math|H<m><rsup|<text|g>>> and <math|H<m><rsup|<text|l>>>, its
  value depends on only <em|one> intensive variable, because the two phases
  are in transfer equilibrium and the system is univariant. Thus, we may
  treat <math|<Delsub|v*a*p>H> as a function of <math|T> only. The same is
  true of any other molar transition quantity.

  The molar Gibbs energy of an equilibrium phase transition,
  <math|<Delsub|t*r*s>G>, is a special case. For the phase transition
  <math|<pha>><ra><math|<phb>>, we may write an equation analogous to Eq.
  <reference|del(vap)Hm=del(vap)H/n> and equate the molar Gibbs energy in
  each phase to a chemical potential (see Eq. <reference|mu=Gm>):

  <\gather>
    <tformat|<table|<row|<cell|<Delsub|t*r*s>G=G<m><bph>-G<m><aph>=\<mu\><bph>-\<mu\><aph><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|del(trs)Gm=mu(beta)-mu(alpha)>>>>>
  </gather>

  But the transition is between two phases at equilibrium, requiring both
  phases to have the same chemical potential:
  <math|\<mu\><bph>-\<mu\><aph>=0>. Therefore, the molar Gibbs energy of
  <em|any> equilibrium phase transition is zero:

  <\gather>
    <tformat|<table|<row|<cell|<Delsub|t*r*s>G=0<cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|del(trs)Gm=0>>>>>
  </gather>

  Since the Gibbs energy is defined by <math|G=H-T*S>, in phase <math|<pha>>
  we have <math|G<m><aph>=G<aph>/n<aph>=H<m><aph>-T*S<m><aph>>. Similarly, in
  phase <math|<phb>> we have <math|G<m><bph>=H<m><bph>-T*S<m><bph>>. When we
  substitute these expressions in <math|<Delsub|t*r*s>G=G<m><bph>-G<m><aph>>
  (Eq. <reference|del(trs)Gm=mu(beta)-mu(alpha)>) and set <math|T> equal to
  the transition temperature <math|T<rsub|<text|trs>>>, we obtain

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<Delsub|t*r*s>G>|<cell|=<around|(|H<m><bph>-H<m><aph>|)>-T<rsub|<text|trs>><around|(|S<m><bph>-S<m><aph>|)>>>|<row|<cell|>|<cell|=<Delsub|t*r*s>H-T<rsub|<text|trs>><Delsub|t*r*s>S>>>>
    </eqsplit>
  </equation>

  Then, by setting <math|<Delsub|t*r*s>G> equal to zero, we find the molar
  entropy and molar enthalpy of the equilibrium phase transition are related
  by

  <\gather>
    <tformat|<table|<row|<cell|<Delsub|t*r*s>S=<frac|<Delsub|t*r*s>H|T<rsub|<text|trs>>><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|del(trs)Sm=del(trs)Hm/T(trs)>>>>>
  </gather>

  where <math|<Delsub|t*r*s>S> and <math|<Delsub|t*r*s>H> are evaluated at
  the transition temperature <math|T<rsub|<text|trs>>>.

  <\quote-env>
    \ We may obtain Eq. <reference|del(trs)Sm=del(trs)Hm/T(trs)> directly
    from the second law. With the phases in equilibrium, the transition
    process is reversible. The second law gives
    <math|<Del>S=q/T<rsub|<text|trs>>=<Del>H/T<rsub|<text|trs>>>. Dividing by
    the amount transferred between the phases gives Eq.
    <reference|del(trs)Sm=del(trs)Hm/T(trs)>.
  </quote-env>

  <subsection|Calorimetric measurement of transition
  enthalpies><label|8-calorimetric>

  The most precise measurement of the molar enthalpy of an equilibrium phase
  transition uses <subindex|Electrical|work><subindex|Work|electrical>electrical
  work. A known quantity of electrical work is performed on a system
  containing coexisting phases, in a <subindex|Calorimeter|constant-pressure><I|Calorimetry!measure
  transition enthalpies@to measure transition
  enthalpies\|reg>constant-pressure adiabatic calorimeter, and the resulting
  amount of substance transferred between the phases is measured. The first
  law shows that the electrical work <math|I<rsup|2>*R<el><Del>t> equals the
  heat that would be needed to cause the same change of state. This heat, at
  constant <math|p>, is the enthalpy change of the process.

  The method is similar to that used to measure the heat capacity of a phase
  at constant pressure (Sec. <reference|7-ht cap measurement>), except that
  now the temperature remains constant and there is no need to make a
  correction for the heat capacity of the calorimeter.

  <subsection|Standard molar transition quantities><label|8-st molar trans
  quantities>

  The <I|Enthalpy!vaporization@of vaporization!standard
  molar\|reg><em|standard> molar enthalpy of vaporization,
  <math|<Delsub|v*a*p>H<st>>, is the enthalpy change when pure liquid in its
  standard state at a specified temperature changes to gas in its standard
  state at the same temperature, divided by the amount changed.

  Note that the initial state of this process is a real one (the pure liquid
  at pressure <math|p<st>>), but the final state (the gas behaving ideally at
  pressure <math|p<st>>) is hypothetical. The liquid and gas are not
  necessarily in equilibrium with one another at pressure <math|p<st>> and
  the temperature of interest, and we cannot evaluate
  <math|<Delsub|v*a*p>H<st>> from a calorimetric measurement with electrical
  work without further corrections. The same difficulty applies to the
  evaluation of <math|<Delsub|s*u*b>H<st>>. In contrast,
  <math|<Delsub|v*a*p>H> and <math|<Delsub|s*u*b>H> (without the <math|<st>>
  symbol), as well as <math|<Delsub|f*u*s>H<st>>, all refer to reversible
  transitions between two <em|real> phases coexisting in equilibrium.

  Let <math|X> represent one of the thermodynamic potentials or the entropy
  of a phase. The standard molar transition quantities
  <math|<Delsub|v*a*p>X<st>=X<m><st><gas>-X<m><s><liquid>> and
  <math|<Delsub|s*u*b>X<st>=X<m><st><gas>-X<m><s><solid>> are functions only
  of <math|T>. To evaluate <math|<Delsub|v*a*p>X<st>> or
  <math|<Delsub|s*u*b>X<st>> at a given temperature, we must calculate the
  change of <math|X<m>> for a path that connects the standard state of the
  liquid or solid with that of the gas. The simplest choice of path is one of
  constant temperature <math|T> with the following steps:

  <\enumerate>
    <item>Isothermal change of the pressure of the liquid or solid, starting
    with the standard state at pressure <math|p<st>> and ending with the
    pressure equal to the vapor pressure <math|p<rsub|<text|vap>>> of the
    condensed phase at temperature <math|T>. The value of <math|<Del>X<m>> in
    this step can be obtained from an expression in the second column of
    Table <reference|tbl:7-isothermal p change>, or from an approximation in
    the last column of the table.

    <item>Reversible vaporization or sublimation to form the real gas at
    <math|T> and <math|p<rsub|<text|vap>>>. The change of <math|X<m>> in this
    step is either <math|<Delsub|v*a*p>X> or <math|<Delsub|s*u*b>X>, which
    can be evaluated experimentally.

    <item>Isothermal change of the real gas at pressure
    <math|p<rsub|<text|vap>>> to the hypothetical ideal gas at pressure
    <math|p<st>>. Table <reference|tbl:7-gas standard molar> has the relevant
    formulas relating molar quantities of a real gas to the corresponding
    standard molar quantities.
  </enumerate>

  The sum of <math|<Del>X<m>> for these three steps is the desired quantity
  <math|<Delsub|v*a*p>X<st>> or <math|<Delsub|s*u*b>X<st>>.

  <section|Coexistence Curves><label|8-coexistence curves>

  A <index|Coexistence curve>coexistence curve on a pressure\Utemperature
  phase diagram shows the conditions under which two phases can coexist in
  equilibrium, as explained in Sec. <reference|8-ph diagrams - 2 phase eqm>.

  <subsection|Chemical potential surfaces>

  <I|Chemical potential!function of T and p@as a function of <math|T> and
  <math|p>\|reg>We may treat the chemical potential <math|\<mu\>> of a pure
  substance in a single phase as a function of the independent variables
  <math|T> and <math|p>, and represent the function by a three-dimensional
  surface. Since the condition for equilibrium between two phases of a pure
  substance is that both phases have the same <math|T>, <math|p>, and
  <math|\<mu\>>, equilibrium in a two-phase system can exist only along the
  intersection of the surfaces of the two phases as illustrated in Fig.
  <reference|fig:8-mu surfaces of H2O><vpageref|fig:8-mu surfaces of H2O>.

  <\big-figure>
    <boxedfigure|<image|./08-SUP/T-p-mu.eps||||> <capt|Top: chemical
    potential surfaces of the liquid and gas phases of H<rsub|<math|2>>O; the
    two phases are at equilibrium along the intersection (heavy curve). (The
    vertical scale for <math|\<mu\>> has an arbitrary zero.) Bottom:
    projection of the intersection onto the <math|p>--<math|T> plane,
    generating the coexistence curve. (Based on data in Ref.
    <cite|grigull-90>.)<label|fig:8-mu surfaces of H2O>>>
  </big-figure|>

  The shape of the surface for each phase is determined by the partial
  derivatives of the chemical potential with respect to temperature and
  pressure as given by Eqs. <reference|dmu/dT=-Sm> and <reference|dmu/dp=Vm>:

  <\equation>
    <Pd|\<mu\>|T|<space|-0.17em>p>=-S<m><space|2em><Pd|\<mu\>|p|T>=V<m>
  </equation>

  Let us explore how <math|\<mu\>> varies with <math|T> at constant <math|p>
  for the different physical states of a substance. The stable phase at each
  temperature is the one of lowest <math|\<mu\>>, since transfer of a
  substance from a higher to a lower <math|\<mu\>> at constant <math|T> and
  <math|p> is spontaneous.

  From the relation <math|<pd|\<mu\>|T|p>=-S<m>>, we see that at constant
  <math|p> the slope of <math|\<mu\>> versus <math|T> is negative since molar
  entropy is always positive. Furthermore, the magnitude of the slope
  increases on going from solid to liquid and from liquid to gas, because the
  molar entropies of sublimation and vaporization are positive. This
  difference in slope is illustrated by the curves for H<rsub|<math|2>>O in
  Fig. <reference|fig:8-mu vs T>(a) <vpageref|fig:8-mu vs T>.

  <\big-figure>
    <\boxedfigure>
      <image|./08-SUP/H2O-mu-T.eps||||>

      <\capt>
        Phase stability of H<rsub|<math|2>>O<@>.<space|.15em><footnote|Based
        on data in Refs. <cite|grigull-90> and <cite|keenan-92>.>

        \ (a)<nbsp>Chemical potentials of different physical states as
        functions of temperature. (The scale for <math|\<mu\>> has an
        arbitrary zero.) Chemical potentials of the gas are shown at
        <math|0.03<br>> and <math|0.003<br>>. The effect of pressure on the
        curves for the solid and liquid is negligible. At <math|p=0.03<br>>,
        solid and liquid coexist at <math|T=273.16<K>> (point A) and liquid
        and gas coexist at <math|T=297.23<K>> (point B). At
        <math|p=0.003<br>>, solid and gas coexist at <math|T=264.77<K>>
        (point C).

        \ (b)<nbsp>Pressure--temperature phase diagram with points
        corresponding to those in (a).<label|fig:8-mu vs T>
      </capt>
    </boxedfigure>
  </big-figure|>

  The triple-point pressure of H<rsub|<math|2>>O is <math|0.0062<br>>. At a
  pressure of <math|0.03<br>>, greater than the triple-point pressure, the
  curves for solid and liquid intersect at a melting point (point A) and the
  curves for liquid and gas intersect at a boiling point (point B).

  From <math|<pd|\<mu\>|p|T>=V<m>>, we see that a pressure reduction at
  constant temperature lowers the chemical potential of a phase. The result
  of a pressure reduction from <math|0.03<br>> to <math|0.003<br>> (below the
  triple-point pressure of H<rsub|<math|2>>O) is a downward shift of each of
  the curves of Fig. <reference|fig:8-mu vs T>(a) by a distance proportional
  to the molar volume of the phase. The shifts of the solid and liquid curves
  are too small to see (<math|<Del>\<mu\>> is only
  <math|-0.002<units|k*J*<space|0.17em>m*o*l<per>>>). Because the gas has a
  large molar volume, the gas curve shifts substantially to a position where
  it intersects with the solid curve at a sublimation point (point C). At
  <math|0.003<br>>, or any other pressure below the triple-point pressure,
  only a solid\Ugas equilibrium is possible for H<rsub|<math|2>>O<@>. The
  liquid phase is not stable at any pressure below the triple-point pressure,
  as shown by the pressure\Utemperature phase diagram of H<rsub|<math|2>>O in
  Fig. <reference|fig:8-mu vs T>(b).

  <subsection|The Clapeyron equation>

  If we start with two coexisting phases, <math|<pha>> and <math|<phb>>, of a
  pure substance and change the temperature of both phases equally without
  changing the pressure, the phases will no longer be in equilibrium, because
  their chemical potentials change unequally. In order for the phases to
  remain in equilibrium during the temperature change <math|<dif>T> of both
  phases, there must be a certain simultaneous change <math|<difp>> in the
  pressure of both phases. The changes <math|<dif>T> and <math|<difp>> must
  be such that the chemical potentials of both phases change equally so as to
  remain equal to one another: <math|<dif>\<mu\><aph>=<dif>\<mu\><bph>>.

  The infinitesimal change of <math|\<mu\>> in a phase is given by
  <math|<dif>\<mu\>=-S<m>*<dif>T+V<m>*<difp>> (Eq.
  <reference|dmu=-(Sm)dT+(Vm)dp>). Thus, the two phases remain in equilibrium
  if <math|<dif>T> and <math|<difp>> satisfy the relation

  <\equation>
    -S<m><aph>*<dif>T+V<m><aph>*<difp>=-S<m><bph>*<dif>T+V<m><bph>*<difp>
  </equation>

  which we rearrange to

  <\equation>
    <frac|<difp>|<dif>T>=<frac|S<m><bph>-S<m><aph>|V<m><bph>-V<m><aph>>
  </equation>

  or

  <\gather>
    <tformat|<table|<row|<cell|<frac|<difp>|<dif>T>=<frac|<Delsub|t*r*s>S|<Delsub|t*r*s>V><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|dp/dT=del(trs)Sm/del(trs)Vm>>>>>
  </gather>

  Equation <reference|dp/dT=del(trs)Sm/del(trs)Vm> is one form of the
  <index|Clapeyron equation><newterm|Clapeyron equation>, which contains no
  approximations. We find an alternative form by substituting
  <math|<Delsub|t*r*s>S=<Delsub|t*r*s>H/T<rsub|<text|trs>>> (Eq.
  <reference|del(trs)Sm=del(trs)Hm/T(trs)>):

  <\gather>
    <tformat|<table|<row|<cell|<frac|<difp>|<dif>T>=<frac|<Delsub|t*r*s>H|T<Delsub|t*r*s>V><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|dp/dT=del(trs)Hm/T*del(trs)Vm>>>>>
  </gather>

  <input|./bio/clapeyron>

  Equations <reference|dp/dT=del(trs)Sm/del(trs)Vm> and
  <reference|dp/dT=del(trs)Hm/T*del(trs)Vm> give the slope of the coexistence
  curve, <math|<difp>/<dif>T>, as a function of quantities that can be
  measured. For the sublimation and vaporization processes, both
  <math|<Delsub|t*r*s>H> and <math|<Delsub|t*r*s>V> are positive. Therefore,
  according to Eq. <reference|dp/dT=del(trs)Hm/T*del(trs)Vm>, the solid\Ugas
  and liquid\Ugas coexistence curves have positive slopes. For the fusion
  process, however, <math|<Delsub|f*u*s>H> is positive, but
  <math|<Delsub|f*u*s>V> may be positive or negative depending on the
  substance, so that the slope of the solid\Uliquid coexistence curve may be
  either positive or negative. The absolute value of <math|<Delsub|f*u*s>V>
  is small, causing the solid\Uliquid coexistence curve to be relatively
  steep; see Fig. <reference|fig:8-mu vs T>(b) for an example.

  <\quote-env>
    \ Most substances <em|expand> on melting, making the slope of the
    solid--liquid coexistence curve positive. This is true of carbon dioxide,
    although in Fig. <reference|fig:8-CO2>(c) <vpageref|fig:8-CO2> the curve
    is so steep that it is difficult to see the slope is positive. Exceptions
    at ordinary pressures, substances that <em|contract> on melting, are
    H<rsub|<math|2>>O, rubidium nitrate, and the elements antimony, bismuth,
    and gallium.

    The phase diagram for H<rsub|<math|2>>O in Fig. <reference|fig:8-ice high
    p><vpageref|fig:8-ice high p> clearly shows that the coexistence curve
    for ice I and liquid has a negative slope due to ordinary ice being less
    dense than liquid water. The high-pressure forms of ice are more dense
    than the liquid, causing the slopes of the other solid--liquid
    coexistence curves to be positive. The ice VII--ice VIII coexistence
    curve is vertical, because these two forms of ice have identical crystal
    structures, except for the orientations of the H<rsub|<math|2>>O
    molecule; therefore, within experimental uncertainty, the two forms have
    equal molar volumes.
  </quote-env>

  We may rearrange Eq. <reference|dp/dT=del(trs)Hm/T*del(trs)Vm> to give the
  variation of <math|p> with <math|T> along the coexistence curve:

  <\equation>
    <label|dp=()dT><difp>=<frac|<Delsub|t*r*s>H|<Delsub|t*r*s>V>\<cdot\><frac|<dif>T|T>
  </equation>

  Consider the transition from solid to liquid (fusion). Because of the fact
  that the <index|Cubic expansion coefficient>cubic expansion coefficient and
  <subindex|Isothermal|compressibility>isothermal compressibility of a
  condensed phase are relatively small, <math|<Delsub|f*u*s>V> is
  approximately constant for small changes of <math|T> and <math|p>. If
  <math|<Delsub|f*u*s>H> is also practically constant, integration of Eq.
  <reference|dp=()dT> yields the relation

  <\equation>
    p<rsub|2>-p<rsub|1>\<approx\><frac|<Delsub|f*u*s>H|<Delsub|f*u*s>V>*ln
    <frac|T<rsub|2>|T<rsub|1>>
  </equation>

  or

  <\gather>
    <tformat|<table|<row|<cell|T<rsub|2>\<approx\>T<rsub|1>*exp
    <around*|[|<frac|<Delsub|f*u*s>V*<around|(|p<rsub|2>-p<rsub|1>|)>|<Delsub|f*u*s>H>|]><cond|<around|(|p*u*r*e*s*u*b*s*t*a*n*c*e|)>><eq-number><label|T2=T1*exp(delVm(p2-p1)/delHm)>>>>>
  </gather>

  from which we may estimate the dependence of the melting point on pressure.

  <subsection|The Clausius\UClapeyron equation><label|8-Clausius-Clapeyron
  eq>

  When the gas phase of a substance coexists in equilibrium with the liquid
  or solid phase, and provided <math|T> and <math|p> are not close to the
  critical point, the molar volume of the gas is much greater than that of
  the condensed phase. Thus, we may write for the processes of vaporization
  and sublimation

  <\equation>
    <Delsub|v*a*p>V=V<m><rsup|<text|g>>-V<m><rsup|<text|l>>\<approx\>V<m><rsup|<text|g>><space|2em><Delsub|s*u*b>V=V<m><rsup|<text|g>>-V<m><rsup|<text|s>>\<approx\>V<m><rsup|<text|g>>
  </equation>

  The further approximation that the gas behaves as an ideal gas,
  <math|V<m><rsup|<text|g>>\<approx\>R*T/p>, then changes Eq.
  <reference|dp/dT=del(trs)Hm/T*del(trs)Vm> to

  <\gather>
    <tformat|<table|<row|<cell|<frac|<difp>|<dif>T>\<approx\><frac|p<Delsub|t*r*s>H|R*T<rsup|2>><cond|(p*u*r*e*s*u*b*s*t*a*n*c*e,><nextcond|v*a*p*o*r*i*z*a*t*i*o*n*o*r*s*u*b*l*i*m*a*t*i*o*n)><eq-number><label|dp/dT=p*delHm/RT2>>>>>
  </gather>

  Equation <reference|dp/dT=p*delHm/RT2> is the <index|Clausius--Clapeyron
  equation><newterm|Clausius--Clapeyron equation>. It gives an approximate
  expression for the slope of a liquid\Ugas or solid\Ugas coexistence curve.
  The expression is not valid for coexisting solid and liquid phases, or for
  coexisting liquid and gas phases close to the critical point.

  At the temperature and pressure of the triple point, it is possible to
  carry out all three equilibrium phase transitions of fusion, vaporization,
  and sublimation. When fusion is followed by vaporization, the net change is
  sublimation. Therefore, the molar transition enthalpies at the triple point
  are related by

  <\equation>
    <Delsub|f*u*s>H+<Delsub|v*a*p>H=<Delsub|s*u*b>H
  </equation>

  Since all three of these transition enthalpies are positive, it follows
  that <math|<Delsub|s*u*b>H> is greater than <math|<Delsub|v*a*p>H> at the
  triple point. Therefore, according to Eq. <reference|dp/dT=p*delHm/RT2>,
  the slope of the solid\Ugas coexistence curve at the triple point is
  slightly greater than the slope of the liquid\Ugas coexistence curve.

  We divide both sides of Eq. <reference|dp/dT=p*delHm/RT2> by <math|p<st>>
  and rearrange to the form

  <\equation>
    <label|dln(p/p^o)/dT=DelHm/RT^2><frac|<dif><around|(|p/p<st>|)>|p/p<st>>\<approx\><frac|<Delsub|t*r*s>H|R>\<cdot\><frac|<dif>T|T<rsup|2>>
  </equation>

  Then, using the mathematical identities
  <math|<dif><around|(|p/p<st>|)>/<around|(|p/p<st>|)>=<dif>ln
  <around|(|p/p<st>|)>> and <math|<dif>T/T<rsup|2>=-<dif><around|(|1/T|)>>,
  we can write Eq. <reference|dln(p/p^o)/dT=DelHm/RT^2> in three alternative
  forms:

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif>ln
    <around|(|p/p<st>|)>|<dif>T>\<approx\><frac|<Delsub|t*r*s>H|R*T<rsup|2>><cond|(p*u*r*e*s*u*b*s*t*a*n*c*e,><nextcond|v*a*p*o*r*i*z*a*t*i*o*n*o*r*s*u*b*l*i*m*a*t*i*o*n)><eq-number><label|dln(p/po)/dT=delHm/RT2>>>>>
  </gather>

  <\gather>
    <tformat|<table|<row|<cell|<dif>ln <around|(|p/p<st>|)>\<approx\>-<frac|<Delsub|t*r*s>H|R><dif><around|(|1/T|)><cond|(p*u*r*e*s*u*b*s*t*a*n*c*e,><nextcond|v*a*p*o*r*i*z*a*t*i*o*n*o*r*s*u*b*l*i*m*a*t*i*o*n)><eq-number><label|dln(p/po)=-(delHm/R)d(1/T)>>>>>
  </gather>

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif>ln
    <around|(|p/p<st>|)>|<dif><around|(|1/T|)>>\<approx\>-<frac|<Delsub|t*r*s>H|R><cond|(p*u*r*e*s*u*b*s*t*a*n*c*e,><nextcond|v*a*p*o*r*i*z*a*t*i*o*n*o*r*s*u*b*l*i*m*a*t*i*o*n)><eq-number><label|dln(p/po)/d(1/T)=-delHm/R>>>>>
  </gather>

  Equation <reference|dln(p/po)/d(1/T)=-delHm/R> shows that the curve of a
  plot of <math|ln <around|(|p/p<st>|)>> versus <math|1/T> (where <math|p> is
  the vapor pressure of a pure liquid or solid) has a slope at each
  temperature equal, usually to a high degree of accuracy, to
  <math|-<Delsub|v*a*p>H/R> or <math|-<Delsub|s*u*b>H/R> at that temperature.
  This kind of plot provides an alternative to calorimetry for evaluating
  molar enthalpies of vaporization and sublimation.

  <\quote-env>
    \ If we use the recommended standard pressure of <math|1<br>>, the ratio
    <math|p/p<st>> appearing in these equations becomes <math|p/<tx|b*a*r>>.
    That is, <math|p/p<st>> is simply the numerical value of <math|p> when
    <math|p> is expressed in bars. For the purpose of using Eq.
    <reference|dln(p/po)/d(1/T)=-delHm/R> to evaluate <math|<Delsub|t*r*s>H>,
    we can replace <math|p<st>> by any convenient value. Thus, the curves of
    plots of <math|ln <around|(|p/<tx|b*a*r>|)>> versus <math|1/T>, <math|ln
    <around|(|p/<tx|P*a>|)>> versus <math|1/T>, and <math|ln
    <around|(|p/<tx|T*o*r*r>|)>> versus <math|1/T> using the same temperature
    and pressure data all have the same slope (but different intercepts) and
    yield the same value of <math|<Delsub|t*r*s>H>.
  </quote-env>

  If we assume <math|<Delsub|v*a*p>H> or <math|<Delsub|s*u*b>H> is
  essentially constant in a temperature range, we may integrate Eq.
  <reference|dln(p/po)=-(delHm/R)d(1/T)> from an initial to a final state
  along the coexistence curve to obtain

  <\gather>
    <tformat|<table|<row|<cell|ln <frac|p<rsub|2>|p<rsub|1>>\<approx\>-<frac|<Delsub|t*r*s>H|R>*<around*|(|<frac|1|T<rsub|2>>-<frac|1|T<rsub|1>>|)><cond|(p*u*r*e*s*u*b*s*t*a*n*c*e,><nextcond|v*a*p*o*r*i*z*a*t*i*o*n*o*r*s*u*b*l*i*m*a*t*i*o*n)><eq-number><label|ln(p2/p1)=-(delHm/R)(1/T2-1/T1)>>>>>
  </gather>

  Equation <reference|ln(p2/p1)=-(delHm/R)(1/T2-1/T1)> allows us to estimate
  any one of the quantities <math|p<rsub|1>>, <math|p<rsub|2>>,
  <math|T<rsub|1>>, <math|T<rsub|2>>, or <math|<Delsub|t*r*s>H>, given values
  of the other four.

  <new-page><phantomsection><addcontentsline|toc|section|Problems>
  <paragraphfootnotes><problems| <input|08-problems><page-break>>
  <plainfootnotes>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|0=T^alpha'dS^alpha'+...|<tuple|1.1.5|?>>
    <associate|8-Clausius-Clapeyron eq|<tuple|1.4.3|?>>
    <associate|8-Gibbs phase rule|<tuple|1.1.7|?>>
    <associate|8-calorimetric|<tuple|1.3.2|?>>
    <associate|8-coexistence curves|<tuple|1.4|?>>
    <associate|8-crit pt|<tuple|1.2.3|?>>
    <associate|8-eqm conditions|<tuple|1.1.1|?>>
    <associate|8-features of phase diagrams|<tuple|1.2.1|?>>
    <associate|8-gas in gravity|<tuple|1.1.4|?>>
    <associate|8-ind variables|<tuple|1.1.6|?>>
    <associate|8-lever rule|<tuple|1.2.4|?>>
    <associate|8-liquid droplet|<tuple|1.1.5|?>>
    <associate|8-molar trans quantities|<tuple|1.3.1|?>>
    <associate|8-multiphase|<tuple|1.1.2|?>>
    <associate|8-ph diagrams - 2 phase eqm|<tuple|1.2.2|?>>
    <associate|8-phase diagrams|<tuple|1.2|?>>
    <associate|8-phase transitions|<tuple|1.3|?>>
    <associate|8-st molar trans quantities|<tuple|1.3.3|?>>
    <associate|8-total chem. pot.|<tuple|1.1.10|?>>
    <associate|Chap. 8|<tuple|1|?>>
    <associate|F=3-P (pure)|<tuple|1.1.17|?>>
    <associate|T2=T1*exp(delVm(p2-p1)/delHm)|<tuple|1.4.8|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|Gravitational|?>>
    <associate|auto-100|<tuple|1.2.4|?>>
    <associate|auto-101|<tuple|1.2.8|?>>
    <associate|auto-102|<tuple|Lever rule|?>>
    <associate|auto-103|<tuple|lever rule|?>>
    <associate|auto-104|<tuple|Lever rule|?>>
    <associate|auto-105|<tuple|1.2.5|?>>
    <associate|auto-106|<tuple|1.2.12|?>>
    <associate|auto-107|<tuple|Isotherm|?>>
    <associate|auto-108|<tuple|Isothermal|?>>
    <associate|auto-109|<tuple|Supercritical fluid|?>>
    <associate|auto-11|<tuple|External field|?>>
    <associate|auto-110|<tuple|Fluid|?>>
    <associate|auto-111|<tuple|Isothermal|?>>
    <associate|auto-112|<tuple|1.2.13|?>>
    <associate|auto-113|<tuple|Cubic expansion coefficient|?>>
    <associate|auto-114|<tuple|Supercritical fluid|?>>
    <associate|auto-115|<tuple|Fluid|?>>
    <associate|auto-116|<tuple|1.3|?>>
    <associate|auto-117|<tuple|1.3.1|?>>
    <associate|auto-118|<tuple|Vaporization|?>>
    <associate|auto-119|<tuple|molar enthalpy of vaporization|?>>
    <associate|auto-12|<tuple|Field|?>>
    <associate|auto-120|<tuple|Vaporization|?>>
    <associate|auto-121|<tuple|molar heat of vaporization|?>>
    <associate|auto-122|<tuple|IUPAC Green Book|?>>
    <associate|auto-123|<tuple|1.3.2|?>>
    <associate|auto-124|<tuple|Electrical|?>>
    <associate|auto-125|<tuple|Work|?>>
    <associate|auto-126|<tuple|Calorimeter|?>>
    <associate|auto-127|<tuple|1.3.3|?>>
    <associate|auto-128|<tuple|1.4|?>>
    <associate|auto-129|<tuple|Coexistence curve|?>>
    <associate|auto-13|<tuple|1.1.1|?>>
    <associate|auto-130|<tuple|1.4.1|?>>
    <associate|auto-131|<tuple|1.4.1|?>>
    <associate|auto-132|<tuple|1.4.1|?>>
    <associate|auto-133|<tuple|1.4.2|?>>
    <associate|auto-134|<tuple|Clapeyron equation|?>>
    <associate|auto-135|<tuple|Clapeyron equation|?>>
    <associate|auto-136|<tuple|Cubic expansion coefficient|?>>
    <associate|auto-137|<tuple|Isothermal|?>>
    <associate|auto-138|<tuple|1.4.3|?>>
    <associate|auto-139|<tuple|Clausius--Clapeyron equation|?>>
    <associate|auto-14|<tuple|Chemical potential|?>>
    <associate|auto-140|<tuple|Clausius--Clapeyron equation|?>>
    <associate|auto-15|<tuple|Gravitochemical potential|?>>
    <associate|auto-16|<tuple|Barometric formula|?>>
    <associate|auto-17|<tuple|Gravitational|?>>
    <associate|auto-18|<tuple|Force|?>>
    <associate|auto-19|<tuple|1.1.5|?>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-20|<tuple|Laplace equation|?>>
    <associate|auto-21|<tuple|1.1.6|?>>
    <associate|auto-22|<tuple|Independent variables|?>>
    <associate|auto-23|<tuple|1.1.7|?>>
    <associate|auto-24|<tuple|Degrees of freedom|?>>
    <associate|auto-25|<tuple|number of degrees of freedom|?>>
    <associate|auto-26|<tuple|Variance|?>>
    <associate|auto-27|<tuple|variance|?>>
    <associate|auto-28|<tuple|Bivariant system|?>>
    <associate|auto-29|<tuple|Univariant system|?>>
    <associate|auto-3|<tuple|1.1.1|?>>
    <associate|auto-30|<tuple|Invariant system|?>>
    <associate|auto-31|<tuple|1.2|?>>
    <associate|auto-32|<tuple|phase diagram|?>>
    <associate|auto-33|<tuple|1.2.1|?>>
    <associate|auto-34|<tuple|1.2.1|?>>
    <associate|auto-35|<tuple|1.2.2|?>>
    <associate|auto-36|<tuple|Triple|?>>
    <associate|auto-37|<tuple|triple line|?>>
    <associate|auto-38|<tuple|Coexistence curve|?>>
    <associate|auto-39|<tuple|coexistence curve|?>>
    <associate|auto-4|<tuple|Independent variables|?>>
    <associate|auto-40|<tuple|Phase|?>>
    <associate|auto-41|<tuple|phase boundary|?>>
    <associate|auto-42|<tuple|Triple|?>>
    <associate|auto-43|<tuple|triple point|?>>
    <associate|auto-44|<tuple|System point|?>>
    <associate|auto-45|<tuple|system point|?>>
    <associate|auto-46|<tuple|Tie line|?>>
    <associate|auto-47|<tuple|tie line|?>>
    <associate|auto-48|<tuple|Helium|?>>
    <associate|auto-49|<tuple|1.2.4|?>>
    <associate|auto-5|<tuple|1.1.2|?>>
    <associate|auto-50|<tuple|Ice, high pressure forms|?>>
    <associate|auto-51|<tuple|1.2.2|?>>
    <associate|auto-52|<tuple|Vapor pressure|?>>
    <associate|auto-53|<tuple|vapor pressure|?>>
    <associate|auto-54|<tuple|Saturation|?>>
    <associate|auto-55|<tuple|Vapor pressure|?>>
    <associate|auto-56|<tuple|saturation vapor pressure|?>>
    <associate|auto-57|<tuple|Sublimation|?>>
    <associate|auto-58|<tuple|Pressure|?>>
    <associate|auto-59|<tuple|sublimation pressure|?>>
    <associate|auto-6|<tuple|Virtual displacement|?>>
    <associate|auto-60|<tuple|Isoteniscope|?>>
    <associate|auto-61|<tuple|1.2.4|?>>
    <associate|auto-62|<tuple|Melting point|?>>
    <associate|auto-63|<tuple|melting point|?>>
    <associate|auto-64|<tuple|Freezing point|?>>
    <associate|auto-65|<tuple|freezing point|?>>
    <associate|auto-66|<tuple|Boiling point|?>>
    <associate|auto-67|<tuple|boiling point|?>>
    <associate|auto-68|<tuple|Saturation|?>>
    <associate|auto-69|<tuple|saturation temperature|?>>
    <associate|auto-7|<tuple|1.1.3|?>>
    <associate|auto-70|<tuple|Sublimation|?>>
    <associate|auto-71|<tuple|sublimation temperature|?>>
    <associate|auto-72|<tuple|Sublimation|?>>
    <associate|auto-73|<tuple|sublimation point|?>>
    <associate|auto-74|<tuple|1.2.5|?>>
    <associate|auto-75|<tuple|Vapor pressure|?>>
    <associate|auto-76|<tuple|vapor-pressure curve|?>>
    <associate|auto-77|<tuple|Boiling point|?>>
    <associate|auto-78|<tuple|boiling-point curve|?>>
    <associate|auto-79|<tuple|Normal|?>>
    <associate|auto-8|<tuple|1.1.4|?>>
    <associate|auto-80|<tuple|Normal|?>>
    <associate|auto-81|<tuple|Standard|?>>
    <associate|auto-82|<tuple|Standard|?>>
    <associate|auto-83|<tuple|Steam point|?>>
    <associate|auto-84|<tuple|1.2.3|?>>
    <associate|auto-85|<tuple|Critical|?>>
    <associate|auto-86|<tuple|Temperature|?>>
    <associate|auto-87|<tuple|critical temperature|?>>
    <associate|auto-88|<tuple|critical point|?>>
    <associate|auto-89|<tuple|Critical|?>>
    <associate|auto-9|<tuple|Field|?>>
    <associate|auto-90|<tuple|critical pressure|?>>
    <associate|auto-91|<tuple|Critical|?>>
    <associate|auto-92|<tuple|1.2.6|?>>
    <associate|auto-93|<tuple|Supercritical fluid|?>>
    <associate|auto-94|<tuple|Fluid|?>>
    <associate|auto-95|<tuple|supercritical fluid|?>>
    <associate|auto-96|<tuple|1.2.9|?>>
    <associate|auto-97|<tuple|Rectilinear diameters, law of|?>>
    <associate|auto-98|<tuple|law of rectilinear diameters|?>>
    <associate|auto-99|<tuple|Cailletet and Matthias, law of|?>>
    <associate|crit opal|<tuple|critical pressure|?>>
    <associate|dS=()dS(alpha)-()dV(alpha)+()dn(alpha)|<tuple|1.1.6|?>>
    <associate|dS=(gas col)|<tuple|1.1.7|?>>
    <associate|dU (droplet)|<tuple|1.1.15|?>>
    <associate|dU=T(alpha')dS(alpha')...|<tuple|1.1.1|?>>
    <associate|del(trs)Gm=0|<tuple|1.3.3|?>>
    <associate|del(trs)Gm=mu(beta)-mu(alpha)|<tuple|1.3.2|?>>
    <associate|del(trs)Sm=del(trs)Hm/T(trs)|<tuple|1.3.5|?>>
    <associate|del(vap)Hm=del(vap)H/n|<tuple|1.3.1|?>>
    <associate|dln(p/p^o)/dT=DelHm/RT^2|<tuple|1.4.12|?>>
    <associate|dln(p/po)/d(1/T)=-delHm/R|<tuple|1.4.15|?>>
    <associate|dln(p/po)/dT=delHm/RT2|<tuple|1.4.13|?>>
    <associate|dln(p/po)=-(delHm/R)d(1/T)|<tuple|1.4.14|?>>
    <associate|dp/dT=del(trs)Hm/T*del(trs)Vm|<tuple|1.4.5|?>>
    <associate|dp/dT=del(trs)Sm/del(trs)Vm|<tuple|1.4.4|?>>
    <associate|dp/dT=p*delHm/RT2|<tuple|1.4.10|?>>
    <associate|dp=()dT|<tuple|1.4.6|?>>
    <associate|dp=-rho g dh|<tuple|1.1.14|?>>
    <associate|fig:8-CO2|<tuple|1.2.1|?>>
    <associate|fig:8-CO2-2|<tuple|1.2.2|?>>
    <associate|fig:8-T-V phase diagram of H2O|<tuple|1.2.13|?>>
    <associate|fig:8-critical_opalescence|<tuple|1.2.6|?>>
    <associate|fig:8-ice high p|<tuple|1.2.4|?>>
    <associate|fig:8-isoteniscope|<tuple|1.2.4|?>>
    <associate|fig:8-mu surfaces of H2O|<tuple|1.4.1|?>>
    <associate|fig:8-mu vs T|<tuple|1.4.1|?>>
    <associate|fig:8-pT phase diagram of H2O|<tuple|1.2.5|?>>
    <associate|fig:8-pV phase diagram of H2O|<tuple|1.2.12|?>>
    <associate|fig:8-rectilinear|<tuple|1.2.9|?>>
    <associate|fig:8-slabs|<tuple|1.1.1|?>>
    <associate|fig:8-tie line|<tuple|1.2.8|?>>
    <associate|footnote-1.1.1|<tuple|1.1.1|?>>
    <associate|footnote-1.2.1|<tuple|1.2.1|?>>
    <associate|footnote-1.2.10|<tuple|1.2.10|?>>
    <associate|footnote-1.2.11|<tuple|1.2.11|?>>
    <associate|footnote-1.2.12|<tuple|1.2.12|?>>
    <associate|footnote-1.2.13|<tuple|1.2.13|?>>
    <associate|footnote-1.2.2|<tuple|1.2.2|?>>
    <associate|footnote-1.2.3|<tuple|1.2.3|?>>
    <associate|footnote-1.2.4|<tuple|1.2.4|?>>
    <associate|footnote-1.2.5|<tuple|1.2.5|?>>
    <associate|footnote-1.2.6|<tuple|1.2.6|?>>
    <associate|footnote-1.2.7|<tuple|1.2.7|?>>
    <associate|footnote-1.2.8|<tuple|1.2.8|?>>
    <associate|footnote-1.2.9|<tuple|1.2.9|?>>
    <associate|footnote-1.3.1|<tuple|1.3.1|?>>
    <associate|footnote-1.3.2|<tuple|1.3.2|?>>
    <associate|footnote-1.4.1|<tuple|1.4.1|?>>
    <associate|footnr-1.1.1|<tuple|1.1.1|?>>
    <associate|footnr-1.2.1|<tuple|1.2.1|?>>
    <associate|footnr-1.2.10|<tuple|1.2.10|?>>
    <associate|footnr-1.2.11|<tuple|1.2.11|?>>
    <associate|footnr-1.2.12|<tuple|1.2.12|?>>
    <associate|footnr-1.2.13|<tuple|1.2.13|?>>
    <associate|footnr-1.2.2|<tuple|1.2.2|?>>
    <associate|footnr-1.2.3|<tuple|Helium|?>>
    <associate|footnr-1.2.4|<tuple|1.2.4|?>>
    <associate|footnr-1.2.5|<tuple|1.2.5|?>>
    <associate|footnr-1.2.6|<tuple|1.2.6|?>>
    <associate|footnr-1.2.7|<tuple|1.2.7|?>>
    <associate|footnr-1.2.8|<tuple|1.2.8|?>>
    <associate|footnr-1.2.9|<tuple|1.2.9|?>>
    <associate|footnr-1.3.1|<tuple|1.3.1|?>>
    <associate|footnr-1.3.2|<tuple|1.3.2|?>>
    <associate|footnr-1.4.1|<tuple|1.4.1|?>>
    <associate|l-g lever rule|<tuple|1.2.5|?>>
    <associate|lever rule|<tuple|1.2.8|?>>
    <associate|ln(p2/p1)=-(delHm/R)(1/T2-1/T1)|<tuple|1.4.16|?>>
    <associate|mu(0)=mu^o+RTln(f/p^o)|<tuple|1.1.8|?>>
    <associate|mu(h)=mu(0)+Mgh|<tuple|1.1.9|?>>
    <associate|mu=muo+ln(f/po)+Mgh|<tuple|1.1.10|?>>
    <associate|n(l)L(l)=n(g)L(g)|<tuple|1.2.4|?>>
    <associate|p(l)=p(g)+2gamma/r|<tuple|1.1.16|?>>
    <associate|p=po*exp(-Mgh/RT)|<tuple|1.1.13|?>>
    <associate|tie line|<tuple|tie line|?>>
    <associate|tie line eq|<tuple|1.2.7|?>>
    <associate|triple line|<tuple|Helium|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      alberty-01

      nist-webbook

      angus-76

      eisenberg-69

      pist-63

      nist-webbook

      sengers-68

      michels-37

      pestak-87

      wang-07

      behnejad-10

      nist-webbook

      nist-webbook

      greenbook-3

      grigull-90

      grigull-90

      keenan-92
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1.1.1>||>|<pageref|auto-13>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.1>||>|<pageref|auto-34>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.2>||>|<pageref|auto-35>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.3>||>|<pageref|auto-49>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.4>||>|<pageref|auto-61>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.5>||>|<pageref|auto-74>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.6>||>|<pageref|auto-92>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.7>||>|<pageref|auto-96>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.8>||>|<pageref|auto-101>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.9>||>|<pageref|auto-106>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.10>||>|<pageref|auto-112>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.4.1>||>|<pageref|auto-131>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.4.2>||>|<pageref|auto-132>>
    </associate>
    <\associate|gly>
      <tuple|normal|number of degrees of freedom|<pageref|auto-25>>

      <tuple|normal|variance|<pageref|auto-27>>

      <tuple|normal|phase diagram|<pageref|auto-32>>

      <tuple|normal|triple line|<pageref|auto-37>>

      <tuple|normal|coexistence curve|<pageref|auto-39>>

      <tuple|normal|phase boundary|<pageref|auto-41>>

      <tuple|normal|triple point|<pageref|auto-43>>

      <tuple|normal|system point|<pageref|auto-45>>

      <tuple|normal|tie line|<pageref|auto-47>>

      <tuple|normal|vapor pressure|<pageref|auto-53>>

      <tuple|normal|saturation vapor pressure|<pageref|auto-56>>

      <tuple|normal|sublimation pressure|<pageref|auto-59>>

      <tuple|normal|melting point|<pageref|auto-63>>

      <tuple|normal|freezing point|<pageref|auto-65>>

      <tuple|normal|boiling point|<pageref|auto-67>>

      <tuple|normal|saturation temperature|<pageref|auto-69>>

      <tuple|normal|sublimation temperature|<pageref|auto-71>>

      <tuple|normal|sublimation point|<pageref|auto-73>>

      <tuple|normal|vapor-pressure curve|<pageref|auto-76>>

      <tuple|normal|boiling-point curve|<pageref|auto-78>>

      <tuple|normal|critical temperature|<pageref|auto-87>>

      <tuple|normal|critical point|<pageref|auto-88>>

      <tuple|normal|critical pressure|<pageref|auto-90>>

      <tuple|normal|supercritical fluid|<pageref|auto-95>>

      <tuple|normal|law of rectilinear diameters|<pageref|auto-98>>

      <tuple|normal|lever rule|<pageref|auto-103>>

      <tuple|normal|molar enthalpy of vaporization|<pageref|auto-119>>

      <tuple|normal|molar heat of vaporization|<pageref|auto-121>>

      <tuple|normal|Clapeyron equation|<pageref|auto-135>>

      <tuple|normal|Clausius--Clapeyron equation|<pageref|auto-140>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Independent variables>|<pageref|auto-4>>

      <tuple|<tuple|Virtual displacement>|<pageref|auto-6>>

      <tuple|<tuple|Field|gravitational>|<pageref|auto-9>>

      <tuple|<tuple|Gravitational|field>|<pageref|auto-10>>

      <tuple|<tuple|External field>|<pageref|auto-11>>

      <tuple|<tuple|Field|external>|<pageref|auto-12>>

      <tuple|<tuple|Chemical potential|total>|<pageref|auto-14>>

      <tuple|<tuple|Gravitochemical potential>|<pageref|auto-15>>

      <tuple|<tuple|Barometric formula>|<pageref|auto-16>>

      <tuple|<tuple|Gravitational|force>|<pageref|auto-17>>

      <tuple|<tuple|Force|gravitational>|<pageref|auto-18>>

      <tuple|<tuple|Laplace equation>|<pageref|auto-20>>

      <tuple|<tuple|Independent variables|number of>|<pageref|auto-22>>

      <tuple|<tuple|Degrees of freedom>|<pageref|auto-24>>

      <tuple|<tuple|Variance>|<pageref|auto-26>>

      <tuple|<tuple|Bivariant system>|<pageref|auto-28>>

      <tuple|<tuple|Univariant system>|<pageref|auto-29>>

      <tuple|<tuple|Invariant system>|<pageref|auto-30>>

      <tuple|<tuple|Triple|line>|<pageref|auto-36>>

      <tuple|<tuple|Coexistence curve>|<pageref|auto-38>>

      <tuple|<tuple|Phase|boundary>|<pageref|auto-40>>

      <tuple|<tuple|Triple|point>|<pageref|auto-42>>

      <tuple|<tuple|System point>|<pageref|auto-44>>

      <tuple|<tuple|Tie line>|<pageref|auto-46>>

      <tuple|<tuple|Helium>|<pageref|auto-48>>

      <tuple|<tuple|Ice, high pressure forms>|<pageref|auto-50>>

      <tuple|<tuple|Vapor pressure>|<pageref|auto-52>>

      <tuple|<tuple|Saturation|vapor pressure>|<pageref|auto-54>>

      <tuple|<tuple|Vapor pressure|saturation>|<pageref|auto-55>>

      <tuple|<tuple|Sublimation|pressure>|<pageref|auto-57>>

      <tuple|<tuple|Pressure|sublimation>|<pageref|auto-58>>

      <tuple|<tuple|Isoteniscope>|<pageref|auto-60>>

      <tuple|<tuple|Melting point>|<pageref|auto-62>>

      <tuple|<tuple|Freezing point>|<pageref|auto-64>>

      <tuple|<tuple|Boiling point>|<pageref|auto-66>>

      <tuple|<tuple|Saturation|temperature>|<pageref|auto-68>>

      <tuple|<tuple|Sublimation|temperature>|<pageref|auto-70>>

      <tuple|<tuple|Sublimation|point>|<pageref|auto-72>>

      <tuple|<tuple|Vapor pressure|curve>|<pageref|auto-75>>

      <tuple|<tuple|Boiling point|curve>|<pageref|auto-77>>

      <tuple|<tuple|Normal|melting point>|<pageref|auto-79>>

      <tuple|<tuple|Normal|boiling point>|<pageref|auto-80>>

      <tuple|<tuple|Standard|boiling point>|<pageref|auto-81>>

      <tuple|<tuple|Standard|melting point>|<pageref|auto-82>>

      <tuple|<tuple|Steam point>|<pageref|auto-83>>

      <tuple|<tuple|Critical|temperature>|<pageref|auto-85>>

      <tuple|<tuple|Temperature|critical>|<pageref|auto-86>>

      <tuple|<tuple|Critical|pressure>|<pageref|auto-89>>

      <tuple|<tuple|Critical|opalescence>|<pageref|auto-91>>

      <tuple|<tuple|Supercritical fluid>|<pageref|auto-93>>

      <tuple|<tuple|Fluid|supercritical>|<pageref|auto-94>>

      <tuple|<tuple|Rectilinear diameters, law of>|<pageref|auto-97>>

      <tuple|<tuple|Cailletet and Matthias, law of>|<pageref|auto-99>>

      <tuple|<tuple|Lever rule>|<pageref|auto-102>>

      <tuple|<tuple|Lever rule|general form>|<pageref|auto-104>>

      <tuple|<tuple|Isotherm>|<pageref|auto-107>>

      <tuple|<tuple|Isothermal|compressibility>|<pageref|auto-108>>

      <tuple|<tuple|Supercritical fluid>|<pageref|auto-109>>

      <tuple|<tuple|Fluid|supercritical>|<pageref|auto-110>>

      <tuple|<tuple|Isothermal|compressibility>|<pageref|auto-111>>

      <tuple|<tuple|Cubic expansion coefficient>|<pageref|auto-113>>

      <tuple|<tuple|Supercritical fluid>|<pageref|auto-114>>

      <tuple|<tuple|Fluid|supercritical>|<pageref|auto-115>>

      <tuple|<tuple|Vaporization|molar enthalpy of>|<pageref|auto-118>>

      <tuple|<tuple|Vaporization|molar heat of>|<pageref|auto-120>>

      <tuple|<tuple|IUPAC Green Book>|<pageref|auto-122>>

      <tuple|<tuple|Electrical|work>|<pageref|auto-124>>

      <tuple|<tuple|Work|electrical>|<pageref|auto-125>>

      <tuple|<tuple|Calorimeter|constant-pressure>|<pageref|auto-126>>

      <tuple|<tuple|Coexistence curve>|<pageref|auto-129>>

      <tuple|<tuple|Clapeyron equation>|<pageref|auto-134>>

      <tuple|<tuple|Cubic expansion coefficient>|<pageref|auto-136>>

      <tuple|<tuple|Isothermal|compressibility>|<pageref|auto-137>>

      <tuple|<tuple|Clausius--Clapeyron equation>|<pageref|auto-139>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Phase
      Transitions and Equilibria of Pure Substances>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Phase Equilibria <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      <with|par-left|<quote|1tab>|1.1.1<space|2spc>Equilibrium conditions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.1.2<space|2spc>Equilibrium in a
      multiphase system <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|1.1.3<space|2spc>Simple derivation of
      equilibrium conditions <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|1tab>|1.1.4<space|2spc>Tall column of gas in a
      gravitational field <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1tab>|1.1.5<space|2spc>The pressure in a liquid
      droplet <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-19>>

      <with|par-left|<quote|1tab>|1.1.6<space|2spc>The number of independent
      variables <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21>>

      <with|par-left|<quote|1tab>|1.1.7<space|2spc>The Gibbs phase rule for a
      pure substance <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-23>>

      1.2<space|2spc>Phase Diagrams of Pure Substances
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-31>

      <with|par-left|<quote|1tab>|1.2.1<space|2spc>Features of phase diagrams
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-33>>

      <with|par-left|<quote|1tab>|1.2.2<space|2spc>Two-phase equilibrium
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-51>>

      <with|par-left|<quote|1tab>|1.2.3<space|2spc>The critical point
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-84>>

      <with|par-left|<quote|1tab>|1.2.4<space|2spc>The lever rule
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-100>>

      <with|par-left|<quote|1tab>|1.2.5<space|2spc>Volume properties
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-105>>

      1.3<space|2spc>Phase Transitions <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-116>

      <with|par-left|<quote|1tab>|1.3.1<space|2spc>Molar transition
      quantities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-117>>

      <with|par-left|<quote|1tab>|1.3.2<space|2spc>Calorimetric measurement
      of transition enthalpies <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-123>>

      <with|par-left|<quote|1tab>|1.3.3<space|2spc>Standard molar transition
      quantities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-127>>

      1.4<space|2spc>Coexistence Curves <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-128>

      <with|par-left|<quote|1tab>|1.4.1<space|2spc>Chemical potential
      surfaces <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-130>>

      <with|par-left|<quote|1tab>|1.4.2<space|2spc>The Clapeyron equation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-133>>

      <with|par-left|<quote|1tab>|1.4.3<space|2spc>The Clausius\UClapeyron
      equation <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-138>>
    </associate>
  </collection>
</auxiliary>