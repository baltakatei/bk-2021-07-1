<TeXmacs|1.99.20>

<style|<tuple|generic|std-latex>>

<\body>
  <\hide-preamble>
    <assign|R|<macro|\<bbb-R\>>>
  </hide-preamble>

  <label|Chap8><problem><label|prb:8-droplet> Consider the system described
  in Sec. <reference|8-liquid droplet> containing a spherical liquid droplet
  of radius <math|r> surrounded by pure vapor. Starting with Eq.
  <reference|dU (droplet)>, find an expression for the total differential of
  <math|U>. Then impose conditions of isolation and show that the equilibrium
  conditions are <math|T<sups|g>=T<sups|l>>,
  <math|\<mu\><sups|g>=\<mu\><sups|l>>, and
  <math|p<sups|l>=p<sups|g>+2<g>/r>, where <math|<g>> is the surface tension.
  <soln| Internal energy change without constraints (Eq. <reference|dU
  (droplet)>): <vs><math|<D><dif>U=T<sups|l><dif>S<sups|l>-p<sups|l><dif>V<sups|l>+\<mu\><sups|l><dif>n<sups|l>+T<sups|g><dif>S<sups|g>-p<sups|g><dif>V<sups|g>+\<mu\><sups|g><dif>n<sups|g>+<g><dif><As>>
  <vs>Because the system boundary contacts only the vapor phase, the
  constraint for no work is <math|-p<sups|g><dif><around|(|V<sups|l>+V<sups|g>|)>=0>.
  <vs><math|V<sups|l>=<around|(|4/3|)>*\<pi\>*r<rsup|3><space|2em><As>=4*\<pi\>*r<rsup|2>>
  <vs>From the constraints <math|<dif>V<sups|l>=4*\<pi\>*r<rsup|2><dif>r> and
  <math|<dif><As>=8*\<pi\>*r<dif>r>, obtain
  <math|<dif><As>=<around|(|2/r|)><dif>V<sups|l>>. <vs>Conditions for an
  isolated system: <math|<dif>U=0>, <math|p<sups|g><dif>V<sups|g>=-p<sups|g><dif>V<sups|l>>,
  <math|<dif>n<sups|g>=-<dif>n<sups|l>>. <vs>After substitution in the
  expression for <math|<dif>U>: <vs><math|<D>0=T<sups|l><dif>S<sups|l>+T<sups|g><dif>S<sups|g>-<around|(|p<sups|l>-p<sups|g>-2<g>/r|)><dif>V<sups|l>+<around|(|\<mu\><sups|l>-\<mu\><sups|g>|)><dif>n<sups|l>>
  <vs>Substitute <math|<dif>S<sups|l>=<dif>S-<dif>S<sups|g>> and solve for
  <math|<dif>S>: <vs><math|<D><dif>S=<frac|<around|(|T<sups|l>-T<sups|g>|)>|T<sups|l>><dif>S<sups|g>+<frac|<around|(|p<sups|l>-p<sups|g>-2<g>/r|)>|T<sups|l>><dif>V<sups|l>-<frac|<around|(|\<mu\><sups|l>-\<mu\><sups|g>|)>|T<sups|l>><dif>n<sups|l>>
  <vs>This is the total differential of <math|S> in the isolated system with
  <math|S<sups|g>>, <math|V<sups|l>>, and <math|n<sups|l>> as independent
  variables. In the equilibrium state, <math|<dif>S> is zero for each
  independent change: <vs><math|<D>T<sups|g>=T<sups|l><space|2em>\<mu\><sups|g>=\<mu\><sups|l><space|2em>p<sups|l>=p<sups|g>+<frac|2<g>|r>>>

  <problem>This problem concerns diethyl ether at <math|T=298.15<K>>. At this
  temperature, the standard molar entropy of the gas calculated from
  spectroscopic data is <math|S<m><st><gas>=342.2<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>.
  The saturation vapor pressure of the liquid at this temperature is
  <math|0.6691<br>>, and the molar enthalpy of vaporization is
  <math|<Delsub|v*a*p>H=27.10<units|k*J*<space|0.17em>m*o*l<per>>>. The
  second virial coefficient of the gas at this temperature has the value
  <math|B=-1.227<timesten|-3><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>>,
  and its variation with temperature is given by
  <math|<dif>B/<dif>T=1.50<timesten|-5><units|m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>>.

  <\problemparts>
    \ <problempartAns>Use these data to calculate the standard molar entropy
    of liquid diethyl ether at <math|298.15<K>>. A small pressure change has
    a negligible effect on the molar entropy of a liquid, so that it is a
    good approximation to equate <math|S<m><st><liquid>> to
    <math|S<m><liquid>> at the saturation vapor pressure.

    <answer|<math|S<m><st><liquid>=253.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>

    <soln| Gas at <math|p=0.6691<br>> (from Table <reference|tbl:7-gas
    standard molar>): <vs><math|<D>S<m><gas>-S<m><st><gas>\<approx\>-R*ln
    <frac|p|p<st>>-p<frac|<dif>B|<dif>T>><no-page-break><vs>
    <math|<D><phantom|S<m><gas>-S<m><st><gas>>=342.2<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>-<around|(|<R>|)>*ln
    <around|(|0.6691|)>> <vs> <math|<D><phantom|S<m><gas>-S<m><st><gas>=>-<around|(|0.6691<timesten|5><Pa>|)><around|(|1.50<timesten|-5><units|m<rsup|<math|3>><space|0.17em><K><per><space|0.17em>mol<per>>|)>>
    <vs> <math|<D><phantom|S<m><gas>-S<m><st><gas>>=344.5<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs>Molar entropy of reversible vaporization at <math|p=0.6691<br>>:
    <vs><math|<D><Del><subs|v*a*p>S=<frac|<Delsub|v*a*p>H|T>=<frac|27.10<timesten|3><units|J*<space|0.17em>m*o*l<per>>|298.15<K>>=90.9<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs>Standard molar entropy of liquid diethyl ether:
    <vs><math|<D>S<m><st><liquid>=S<m><gas>-<around|[|S<m><gas>-S<m><liquid>|]>+<around|[|S<m><st><liquid>-S<m><liquid>|]>>
    <vs> <math|<phantom|S<m><st><liquid>>\<approx\>S<m><gas>-<Delsub|v*a*p>S=<around|(|344.5-90.9|)><units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>=253.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>
    <problempartAns>Calculate the standard molar entropy of vaporization and
    the standard molar enthalpy of vaporization of diethyl ether at
    <math|298.15<K>>. It is a good approximation to equate
    <math|H<m><st><liquid>> to <math|H<m><liquid>> at the saturation vapor
    pressure.

    <answer|<math|<Delsub|v*a*p>S<st>=88.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>,
    <math|<Delsub|v*a*p>H<st>=2.748<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>

    <soln| <math|<D><Delsub|v*a*p>S<st>=S<m><st><gas>-S<m><st><liquid>=<around|(|342.2-253.6|)><units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>=88.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs>Gas at <math|p=0.6691<br>> (from Table <reference|tbl:7-gas standard
    molar>): <vs><math|<D>H<m><gas>-H<m><st><gas>\<approx\>p*<around*|(|B-T*<frac|<dif>B|<dif>T>|)>>
    <vs> <math|<D><phantom|H<m><gas>-H<m><st><gas>>=<around|(|0.6691<timesten|5><Pa>|)>[<around|(|-1.227<timesten|-3><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>>
    <vs> <math|<D><phantom|H<m><gas>-H<m><st><gas>=>-<around|(|298.15<K>|)><around|(|1.50<timesten|-5><units|m<rsup|<math|3>><space|0.17em><K><per><space|0.17em>mol<per>>|)>]>
    <vs> <math|<D><phantom|H<m><gas>-H<m><st><gas>>=-381.3<units|J*<space|0.17em>m*o*l<per>>>
    <vs><math|<D><Delsub|v*a*p>H<st>=H<m><st><gas>-H<m><st><liquid>> <vs>
    <math|<D><phantom|<Delsub|v*a*p>H<st>>=<around|[|H<m><gas>-H<m><liquid>|]>-<around|[|H<m><gas>-H<m><st><gas>|]>+<around|[|H<m><liquid>-H<m><st><liquid>|]>>
    <vs> <math|<D><phantom|<Delsub|v*a*p>H<st>>\<approx\><Delsub|v*a*p>H-<around|[|H<m><gas>-H<m><st><gas>|]>>
    <vs> <math|<D><phantom|<Delsub|v*a*p>H<st>>=<around|(|27.10<timesten|3>+381.3|)><units|J*<space|0.17em>m*o*l<per>>=2.748<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>
  </problemparts>

  <problem>Explain why the chemical potential surfaces shown in Fig.
  <reference|fig:8-mu surfaces of H2O> are concave downward; that is, why
  <math|<pd|\<mu\>|T|p>> becomes more negative with increasing <math|T> and
  <math|<pd|\<mu\>|p|T>> becomes less positive with increasing <math|p>.
  <soln| The partial derivatives are given by Eqs. <reference|dmu/dT=-Sm> and
  <reference|dmu/dp=Vm>: <vs><math|<D><Pd|\<mu\>|T|<space|-0.17em>p>=-S<m><space|2em><Pd|\<mu\>|p|T>=V<m>>
  <vs>Since <math|S<m>> increases with increasing <math|T> at constant
  <math|p> (<math|<pd|S|T|p>=C<rsub|p>/T>), and molar volume decreases with
  increasing <math|p> at constant <math|T>, both partial derivatives are
  negative.>

  <problemAns>Potassium has a standard boiling point of
  <math|773<units|<degC>>> and a molar enthalpy of vaporization
  <math|<Delsub|v*a*p>H=84.9<units|k*J*<space|0.17em>m*o*l<per>>>. Estimate
  the saturation vapor pressure of liquid potassium at
  <math|400.<units|<degC>>>.

  <\answer>
    <math|4.5<timesten|-3><br>>
  </answer>

  <\soln>
    \ <math|<D>ln <frac|p<rsub|2>|p<rsub|1>>\<approx\>-<frac|<Delsub|v*a*p>H|R>*<around*|(|<frac|1|T<rsub|2>>-<frac|1|T<rsub|1>>|)>=-<frac|84.9<timesten|3><units|J*<space|0.17em>m*o*l<per>>|<R>>*<around*|(|<frac|1|673<K>>-<frac|1|1046<K>>|)>=-5.41>
    <vs><math|<D>p<rsub|2>/p<rsub|1>=4.5<timesten|-3><space|2em>p<rsub|2>=<around|(|4.5<timesten|-3>|)><around|(|1<br>|)>=4.5<timesten|-3><br>>
  </soln>

  <problemAns>Naphthalene has a melting point of <math|78.2<units|<degC>>> at
  <math|1<br>> and <math|81.7<units|<degC>>> at <math|100<br>>. The molar
  volume change on melting is <math|<Delsub|f*u*s>V=0.019<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>.
  Calculate the molar enthalpy of fusion to two significant figures.

  <\answer>
    <math|19<units|J*<space|0.17em>m*o*l<per>>>
  </answer>

  <\soln>
    \ Rearrange Eq. <reference|T2=T1*exp(delVm(p2-p1)/delHm)>:
    <vs><math|<D><Delsub|f*u*s>H\<approx\><frac|<Delsub|f*u*s>V*<around|(|p<rsub|2>-p<rsub|1>|)>|ln
    <around|(|T<rsub|2>/T<rsub|1>|)>>> <vs>
    <math|<D><phantom|<Delsub|f*u*s>H>=<frac|<around|(|0.019<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>*<around|(|10<rsup|-2><units|m>/<tx|c*m>|)><rsup|3>*<around|(|100-1|)>*10<rsup|5><Pa>|ln
    <around|(|354.9<K>/351.4<K>|)>>=19<units|J*<space|0.17em>m*o*l<per>>>
  </soln>

  <problem><label|prb:8-Antoine> The dependence of the vapor pressure of a
  liquid on temperature, over a limited temperature range, is often
  represented by the <I|Antoine equation\|p><em|Antoine equation>,
  <math|log<rsub|10><around|(|p/<tx|T*o*r*r>|)>=A-B/<around|(|t+C|)>>, where
  <math|t> is the Celsius temperature and <math|A>, <math|B>, and <math|C>
  are constants determined by experiment. A variation of this equation, using
  a natural logarithm and the thermodynamic temperature, is

  <\equation*>
    ln <around|(|p/<tx|b*a*r>|)>=a-<frac|b|T+c>
  </equation*>

  The vapor pressure of liquid benzene at temperatures close to <math|298<K>>
  is adequately represented by the preceding equation with the following
  values of the constants:

  <\equation*>
    a=9.25092*<space|2em>b=2771.233<K><space|2em>c=-53.262<K>
  </equation*>

  <\problemparts>
    \ <problempartAns>Find the standard boiling point of benzene.

    <answer|<math|352.82<K>>>

    <soln| Solve the equation for <math|T> and set <math|p> equal to
    <math|1<br>>: <vs><math|<D>T=<frac|b|a-ln
    <around|(|p/<tx|b*a*r>|)>>-c=<frac|2771.233<K>|9.25092-ln
    <around|(|1|)>>-53.262<K>=352.82<K>>> <problempartAns>Use the
    Clausius--Clapeyron equation to evaluate the molar enthalpy of
    vaporization of benzene at <math|298.15<K>>.

    <answer|<math|3.4154<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>
  </problemparts>

  \ <soln| Rearrange Eq. <reference|dln(p/po)/dT=delHm/RT2> to
  <vs><math|<D><Delsub|t*r*s>H\<approx\>R*T<rsup|2>*<frac|<dif>ln
  <around|(|p/p<st>|)>|<dif>T>=R*T<rsup|2><around*|[|<frac|b|<around|(|T+c|)><rsup|2>>|]>=3.4154<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>

  <problem>At a pressure of one atmosphere, water and steam are in
  equilibrium at <math|99.97<units|<degC>>> (the normal boiling point of
  water). At this pressure and temperature, the water density is
  <math|0.958<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>, the steam density
  is <math|5.98<timesten|-4><units|g*<space|0.17em>c*m<rsup|<math|-3>>>>, and
  the molar enthalpy of vaporization is <math|40.66<units|k*J*<space|0.17em>m*o*l<per>>>.

  <\problemparts>
    \ <problempartAns>Use the Clapeyron equation to calculate the slope
    <math|<difp>/<dif>T> of the liquid--gas coexistence curve at this point.

    <answer|<math|3.62<timesten|3><units|P*a*<space|0.17em>K<per>>>>

    <soln| <math|<D><Delsub|v*a*p>V=<around|(|18.0153<units|g*<space|0.17em>m*o*l<per>>|)>*<around*|(|<frac|1|5.98<timesten|-4><units|g*<space|0.17em>c*m<rsup|<math|-3>>>>-<frac|1|0.958<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>|)>>
    <vs> <math|<D><phantom|<Delsub|v*a*p>V>=3.011<timesten|4><units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    <vs><math|<D><frac|p|T>=<frac|<Delsub|v*a*p>H|T<Delsub|v*a*p>V>=<frac|40.66<timesten|3><units|J*<space|0.17em>m*o*l<per>>|<around|(|373.13<K>|)><around|(|3.011<timesten|-2><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>>=3.62<timesten|3><units|P*a*<space|0.17em>K<per>>>>
    <problempartAns>Repeat the calculation using the Clausius--Clapeyron
    equation.

    <answer|<math|3.56<timesten|3><units|P*a*<space|0.17em>K<per>>>>

    <soln| <math|<D><frac|p|T>\<approx\><frac|<Delsub|v*a*p>H*p|R*T<rsup|2>>=<frac|<around|(|40.66<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)><around|(|1.01325<timesten|5><Pa>|)>|<around|(|<R>|)><around|(|373.13<K>|)><rsup|2>>=3.56<timesten|3><units|P*a*<space|0.17em>K<per>>>>
    <problempartAns>Use your results to estimate the standard boiling point
    of water. (Note: The experimental value is <math|99.61<units|<degC>>>.)

    <answer|<math|99.60<units|<degC>>>>

    <soln| <math|<D>T<subs|v*a*p><around|(|p<rsub|2>|)>\<approx\>T<subs|v*a*p><around|(|p<rsub|1>|)>+<frac|p<rsub|2>-p<rsub|1>|<difp>/<dif>T>>
    <vs>Use the value of <math|<difp>/<dif>T> from the Clapeyron equation,
    which is more accurate: <vs><math|<D>T<subs|v*a*p><around|(|1<br>|)>\<approx\>373.12<K>+<frac|<around|(|1-1.01325|)><timesten|5><Pa>|3.62<timesten|3><units|P*a*<space|0.17em>K<per>>>=373.12<K>-0.366<K>>
    <vs> <math|<D><phantom|T<subs|v*a*p><around|(|1<br>|)>>=372.75<K>>
    (<math|99.60<units|<degC>>>)>
  </problemparts>

  <problem>At the standard pressure of <math|1<br>>, liquid and gaseous
  H<rsub|<math|2>>O coexist in equilibrium at <math|372.76<K>>, the standard
  boiling point of water.

  <\problemparts>
    \ <problempart>Do you expect the standard molar enthalpy of vaporization
    to have the same value as the molar enthalpy of vaporization at this
    temperature? Explain. <soln| The values of <math|<Delsub|v*a*p>H<st>> and
    <math|<Delsub|v*a*p>H> are different because the standard state of a gas
    is the hypothetical ideal gas at the standard pressure, which is not the
    same as the real gas at the standard pressure.> <problempartAns>The molar
    enthalpy of vaporization at <math|372.76<K>> has the value
    <math|<Delsub|v*a*p>H=40.67<units|k*J*<space|0.17em>m*o*l<per>>>.
    Estimate the value of <math|<Delsub|v*a*p>H<st>> at this temperature with
    the help of Table <reference|tbl:7-gas standard molar> and the following
    data for the second virial coefficient of gaseous H<rsub|<math|2>>O at
    <math|372.76<K>>:

    <\equation*>
      B=-4.60<timesten|-4><units|m<rsup|<math|3>>*<space|0.17em>mol<per>><space|2em><dif>B/<dif>T=3.4<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>
    </equation*>

    <answer|<math|<Delsub|v*a*p>H<st>=4.084<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>

    <soln| <math|<D>H<m><gas>\<approx\>H<m><st><gas>+p*<around*|(|B-T*<frac|<dif>B|<dif>T>|)>>
    <vs> <math|<D><phantom|H<m><gas>>=H<m><st><gas>+<around|(|1<timesten|5><Pa>|)><around*|[|<around|(|-4.60<timesten|-4><units|m<rsup|<math|3>>*<space|0.17em>mol<per>>|)>|\<nobracket\>>>
    <vs> <math|<D><phantom|H<m><gas>\<approx\>>-<around*|\<nobracket\>|<around|(|372.76<K>|)><around|(|3.4<timesten|-6><units|m<rsup|<math|3>>*<space|0.17em>K<per><space|0.17em>mol<per>>|)>|]>>
    <vs> <math|<D><phantom|H<m><gas>>=H<m><st><gas>-1.73<timesten|2><units|J*<space|0.17em>m*o*l<per>>>
    <vs><math|<D><Delsub|v*a*p>H<st>=<Delsub|v*a*p>H+H<m><st><gas>-H<m><gas>=40.67<timesten|3><units|J*<space|0.17em>m*o*l<per>>+1.73<timesten|2><units|J*<space|0.17em>m*o*l<per>>>
    <vs> <math|<D><phantom|<Delsub|v*a*p>H<st>>=4.084<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>
    <problempart>Would you expect the values of <math|<Delsub|f*u*s>H> and
    <math|<Delsub|f*u*s>H<st>> to be equal at the standard freezing point of
    water? Explain. <soln| Yes, because the standard states of the solid and
    liquid are the same as the real coexisting phases at the standard
    pressure.>
  </problemparts>

  <problemAns>The standard boiling point of H<rsub|<math|2>>O is
  <math|99.61<units|<degC>>>. The molar enthalpy of vaporization at this
  temperature is <math|<Delsub|v*a*p>H=40.67<units|k*J*<space|0.17em>m*o*l<per>>>.
  The molar heat capacity of the liquid at temperatures close to this value
  is given by

  <\equation*>
    <Cpm>=a+b*<around|(|t-c|)>
  </equation*>

  where <math|t> is the Celsius temperature and the constants have the values

  <\equation*>
    a=75.94<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>><space|2em>b=0.022<units|J*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>><space|2em>c=99.61<units|<degC>>
  </equation*>

  Suppose <math|100.00<mol>> of liquid H<rsub|<math|2>>O is placed in a
  container maintained at a constant pressure of <math|1<br>>, and is
  carefully heated to a temperature <math|5.00<units|<degC>>> above the
  standard boiling point, resulting in an unstable phase of superheated
  water. If the container is enclosed with an adiabatic boundary and the
  system subsequently changes spontaneously to an equilibrium state, what
  amount of water will vaporize? (Hint: The temperature will drop to the
  standard boiling point, and the enthalpy change will be zero.)

  <\answer>
    <math|0.93<units|m*o*l>>
  </answer>

  <\soln>
    \ The enthalpy change for the process is the same as the enthalpy change
    when the water is cooled to <math|99.61<units|<degC>>> and vaporization
    then takes place reversibly: <vs><math|<D><Del>H=0=n*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><Cpm><dt>+n<sups|g><Delsub|v*a*p>H>
    <vs><math|<D>n<sups|g>=<frac|<D>-n*<big|int><rsub|t<rsub|1>><rsup|t<rsub|2>><Cpm><dt>|<Delsub|v*a*p>H>>
    <vs>Write <math|x=t-c>, <math|<Cpm>=a+b*x>:
    <vs><math|<D>n<sups|g>=<frac|<D>-n*<big|int><rsub|x<rsub|1>><rsup|x<rsub|2>><around|(|a+b*x|)><dx>|<Delsub|v*a*p>H>=<frac|-n*<around*|[|a*<around|(|x<rsub|2>-x<rsub|1>|)>+<around|(|b/2|)>*<around|(|x<rsub|2><rsup|2>-x<rsub|1><rsup|2>|)>|]>|<Delsub|v*a*p>H>>
    <vs> <math|<D><phantom|n<sups|g>>=<frac|-<around|(|100.00<units|m*o*l>|)>*<around*|[|<around|(|75.94<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>|)>*<around|(|-5.00<K>|)>+<around|(|0.022<space|0.17em><units|J*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>|)>*<around|(|-5.00<K>|)><rsup|2>/2|]>|4.067<timesten|4><units|J*<space|0.17em>m*o*l<per>>>>
    <vs> <math|<D><phantom|n<sups|g>>=0.93<units|m*o*l>>
  </soln>
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>