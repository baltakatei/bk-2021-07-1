<TeXmacs|1.99.20>

<style|<tuple|generic|std-latex>>

<\body>
  <label|Chap6><problemAns>Calculate the molar entropy of carbon disulfide at
  <math|25.00<units|<degC>>> and <math|1<br>> from the heat capacity data for
  the solid in Table <reference|tbl:6-CS2><vpageref|tbl:6-CS2>

  <\big-table>
    <minipagetable|4.5cm|}<tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|2|2|cell-col-span|1>|<cwith|1|1|2|2|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|11|11|1|-1|cell-bborder|1ln>|<table|<row|<cell|w<ccol|<math|T/>K>>|<cell|<math|<Cpm>/>J<space|0.17em>K<per><space|0.17em>mol<per>>>|<row|<cell|15.05>|<cell|6.9>>|<row|<cell|20.15>|<cell|12.0>>|<row|<cell|29.76>|<cell|20.8>>|<row|<cell|42.22>|<cell|29.2>>|<row|<cell|57.52>|<cell|35.6>>|<row|<cell|75.54>|<cell|40.0>>|<row|<cell|94.21>|<cell|45.0>>|<row|<cell|108.93>|<cell|48.5>>|<row|<cell|131.54>|<cell|52.6>>|<row|<cell|156.83>|<cell|56.6>>>>>>
  </big-table|Molar heat capacity of CS<rsub|<math|2>>(s) at
  <math|p=1<br>><space|.15em><footnote|Ref. <cite|brown-37>.>>

  and the following data for <math|p=1<br>>. At the melting point,
  <math|161.11<K>>, the molar enthalpy of fusion is
  <math|<Delsub|f*u*s>H=4.39<timesten|3><units|J*<space|0.17em>m*o*l<per>>>.
  The molar heat capacity of the liquid in the range 161\U300<nbsp>K is
  described by <math|<Cpm>=a+b*T>, where the constants have the values
  <math|a=74.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>> and
  <math|b=0.0034<units|J*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>>.

  <\answer>
    <math|S<m>\<approx\>151.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
  </answer>

  <\soln>
    \ Debye extrapolation from <math|0<K>> to <math|15.05<K>>:
    <vs><math|<D><Del>S<m>=<around|(|6.9<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>|)>/3=2.3<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs>Molar entropy of heating the solid from <math|15.05<K>> to the
    melting point: When the molar heat capacity of the solid is plotted
    versus <math|ln <around|(|T/<tx|K>|)>>, a practically-linear relation is
    observed (Fig. <reference|fig:6-CS2><vpageref|fig:6-CS2>).

    <\big-figure>
      <boxedfigure|<image|./06-SUP/CS2.eps||||><capt|<label|fig:6-CS2>>>
    </big-figure|>

    The sloped line is drawn through the first and last points and extended a
    very short distance to the melting point at <math|ln
    <around|(|161.11|)>=5.08>. The area under this line is
    <math|75.7<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>.
    Numerical integration of a curve fitted to the points will yield a
    similar value.<next-line>Molar entropy of fusion:
    <vs><math|<D><Delsub|f*u*s>S=<frac|<Delsub|f*u*s>H|T<subs|f*u*s>>=<frac|4.39<timesten|3><units|J*<space|0.17em>m*o*l<per>>|161.11<K>>=27.2<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs>Molar entropy change of heating the liquid to <math|298.15<K>>:
    <vs><math|<D><Del>S<m>=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|<Cpm>|T><dif>T=<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|a+b*T|T><dif>T=a*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><frac|<dif>T|T>+b*<big|int><rsub|T<rsub|1>><rsup|T<rsub|2>><space|-0.17em><dif>T>
    <vs> <math|<D><phantom|<Del>S<m>>=<around|(|74.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>|)>*ln
    <frac|298.15<K>|161.11<K>>> <vs> <math|<D><phantom|<Del>S<m>=>+<around|(|0.0034<units|J*<space|0.17em>K<rsup|<math|-2>>*<space|0.17em>mol<per>>|)>*<around|(|298.15<K>-161.11<K>|)>>
    <vs> <math|<D><phantom|<Del>S<m>>=46.4<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs>Total molar entropy change: <vs><math|<D><Del>S<m>=<around|(|2.3+75.7+27.2+46.4|)><units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>=151.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs>Thus, the molar entropy at <math|25.00<units|<degC>>> and
    <math|1<br>> is close to <math|151.6<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>.
  </soln>

  \;
</body>

<\initial>
  <\collection>
    <associate|info-flag|detailed>
    <associate|preamble|true>
  </collection>
</initial>