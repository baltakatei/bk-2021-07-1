<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <Opensolutionfile*|ANS|dump> <assign|the-chapter|<macro|<number|<chapter-nr>|arabic>>><reset-counter|chapter><assign|the-table|<macro|<number|<table-nr>|arabic>>><assign|the-figure|<macro|<number|<figure-nr>|arabic>>>

  <assign|page-nr|1><reset-counter|footnote><with|par-columns|1|<markboth||>
  <thispagestyle|empty> <with|par-mode|center|
  <vspace|.25in><rule|4.5in|.08in><next-line><vspace|.1in><with|font-size|1.68|Solutions
  Manual for<next-line><vspace|.2in>Thermodynamics and
  Chemistry><next-line><vspace|.2in><with|font-size|1.41|Second
  Edition><next-line><vspace|.16in><rule|4.5in|.08in><next-line><vspace|.75in><with|font-size|1.19|by<next-line><vspace|.1in><with|font-size|1.41|Howard
  DeVoe<next-line><vspace|.1in>>Associate Professor of Chemistry
  Emeritus<next-line>University of Maryland, College Park,
  Maryland<next-line>hdevoe@umd.edu>>>

  <\with|font-size|1.19>
    <vspace*|1.5in><no-indent>Copyright 2020 by Howard DeVoe

    <no-indent><image|./00-SUP/by-2.eps||||><next-line>This work is licensed
    under a Creative Commons Attribution 4.0 International License:
    <vspace|-.3cm>

    <padded-center|<slink|https://creativecommons.org/licenses/by/4.0/> >

    <new-page><thispagestyle|empty><no-indent><with|font-family|sf|font-series|bold|font-size|1.19|Contents><next-line><vspace*|4ex>
    <assign|arraystretch|<macro|1.5>>

    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<twith|table-width|5in>|<table|<row|<cell|>|<cell|Preface>|<cell|<pageref|preface>>>|<row|<cell|1>|<cell|<ChapOne>>|<cell|<pageref|Chap1>>>|<row|<cell|2>|<cell|<ChapTwo>>|<cell|<pageref|Chap2>>>|<row|<cell|3>|<cell|<ChapThree>>|<cell|<pageref|Chap3>>>|<row|<cell|4>|<cell|<ChapFour>>|<cell|<pageref|Chap4>>>|<row|<cell|5>|<cell|<ChapFive>>|<cell|<pageref|Chap5>>>|<row|<cell|6>|<cell|<ChapSix>>|<cell|<pageref|Chap6>>>|<row|<cell|7>|<cell|<ChapSeven>>|<cell|<pageref|Chap7>>>|<row|<cell|8>|<cell|<ChapEight>>|<cell|<pageref|Chap8>>>|<row|<cell|9>|<cell|<ChapNine>>|<cell|<pageref|Chap9>>>|<row|<cell|10>|<cell|<ChapTen>>|<cell|<pageref|Chap10>>>|<row|<cell|11>|<cell|<ChapEleven>>|<cell|<pageref|Chap11>>>|<row|<cell|12>|<cell|<ChapTwelve>>|<cell|<pageref|Chap12>>>|<row|<cell|13>|<cell|<ChapThirteen>>|<cell|<pageref|Chap13>>>|<row|<cell|14>|<cell|<ChapFourteen>>|<cell|<pageref|Chap14>>>>>>

    <new-page><thispagestyle|empty><label|preface><no-indent><with|font-family|sf|font-series|bold|font-size|1.41|Preface><next-line><vspace*|.5ex>

    <no-indent>This manual contains detailed solutions to the problems
    appearing at the end of each chapter of the text <em|Thermodynamics and
    Chemistry>.

    Each problem printed in the text is reproduced in this manual, followed
    by a worked-out solution. If a figure or table accompanies a problem in
    the text, it is also reproduced here. Included within a solution may be
    an additional figure or table that does not appear in the text. All
    figures, tables, and footnotes in this manual are numbered consecutively
    (Figure 1, Figure 2, etc.) and so do not agree with the numbering in the
    text.

    In most cases of a numerical calculation involving physical quantities,
    the setup in this manual shows the values of given individual physical
    quantities expressed in SI base units and SI derived units, without
    prefixes. The result of the calculation is then expressed in SI base
    units and SI derived units appropriate to the physical quantity being
    evaluated. Since the factors needed to convert the units of the given
    quantities to the units of the calculated quantity all have numerical
    values of unity when this procedure is followed, the conversion factors
    are not shown.

    Of course, the solution given in this manual for any particular problem
    is probably not the only way the problem can be solved; other solutions
    may be equally valid.
  </with>

  <\with|font-size|0.84>
    <Solns|<ChapOne>| <input|01-problems>>
    <reset-counter|table><Solns|<ChapTwo>| <input|02-problems>>
    <Solns|<ChapThree>| <input|03-problems>> <Solns|<ChapFour>|
    <input|04-problems>> <Solns|<ChapFive>| <input|05-problems>>
    <Solns|<ChapSix>| <input|06-problems>> <Solns|<ChapSeven>|
    <input|07-problems>> <Solns|<ChapEight>| <input|08-problems>>
    <Solns|<ChapNine>| <input|09-problems>> <Solns|<ChapTen>|
    <input|10-problems>> <Solns|<ChapEleven>| <input|11-problems>>
    <Solns|<ChapTwelve>| <input|12-problems>> <Solns|<ChapThirteen>|
    <input|13-problems>> <Solns|<ChapFourteen>| <input|14-problems>>

    <backmatter> <with|font-size|0.84|par-mode|left|}<label|bib>>
  </with>

  <\with|font-size|0.84>
    <bib-list|[99]|>
  </with>

  <with|font-size|0.84|<Closesolutionfile|ANS>>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|bib|<tuple|?|?>>
    <associate|preface|<tuple|?|?>>
  </collection>
</references>