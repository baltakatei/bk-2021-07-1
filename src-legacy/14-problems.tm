<TeXmacs|2.1>

<style|<tuple|generic|std-latex>>

<\body>
  <\hide-preamble>
    <assign|R|<macro|\<bbb-R\>>>
  </hide-preamble>

  <label|Chap14>

  <problem>The state of a galvanic cell without liquid junction, when its
  temperature and pressure are uniform, can be fully described by values of
  the variables <math|T>, <math|p>, and <math|\<xi\>>. Find an expression for
  <math|<dif>G> during a reversible advancement of the cell reaction, and use
  it to derive the relation <math|\<Delta\><rsub|<text|r>>*G=-z*F<Eeq>> (Eq.
  <reference|del(r)G(cell)=-zFE>). (Hint: Eq.
  <reference|dw(el)=E(cell)dQ(cell)>.)\ 

  <soln| Equation <reference|dw(el)=E(cell)dQ(cell)> is a general formula for
  electrical work when the cell is part of an electrical circuit:
  <vs><math|<dw><el>=E<dQ><sys>> <vs>Combine this relation with
  <math|<dQ><sys>=-z*F<dif>\<xi\>> (Eq. <reference|dQ=-zFdxi>):
  <vs><math|<dw><el>=-z*F*E<dif>\<xi\>> <vs>The intermediate states of a
  reversible cell reaction have uniform <math|T> and <math|p> and an
  equilibrium cell potential: <math|E=<Eeq>>. <math|<dif>G> during a
  reversible process of a closed system with nonexpansion work is given by
  the equality of Eq. <reference|dG\<less\>=-SdT+Vdp+dw'>. In this equality,
  replace <math|<dw><rprime|'>> by <math|<dw><el>=-z*F<Eeq><dif>\<xi\>>:
  <vs><math|<dif>G=-S<dif>T+V<difp>-z*F<Eeq><dif>\<xi\>> <vs>This is an
  expression for the total differential of G with <math|T>, <math|p>, and
  <math|\<xi\>> as the independent variables. Identify the coefficient of
  <math|<dif>\<xi\>> as the partial derivative <math|<pd|G|\<xi\>|T,p>>,
  which is the molar reaction Gibbs energy of the cell reaction:
  <vs><math|\<Delta\><rsub|<text|r>>*G=-z*F<Eeq>>>

  \;

  <problem>Before 1982 the <I|Standard!pressure\|p><I|Pressure!standard\|p>standard
  pressure was usually taken as <math|1<units|a*t*m>>. For the cell shown in
  Fig. <reference|fig:14-galvanic cell>, what correction is needed, for a
  value of <math|<Eeq><st>> obtained at <math|25<units|<degC>>> and using the
  older convention, to change the value to one corresponding to a standard
  pressure of <math|1<br>>? Equation <reference|del(r)Go=-zFEo> can be used
  for this calculation.

  <soln| From Eq. <reference|del(r)Go=-zFEo>:
  <vs><math|<D><Eeq><st>=-<frac|\<Delta\><rsub|<text|r>>*G<st>|z*F>> <vs>where the standard
  molar reaction Gibbs energy is given by
  <math|\<Delta\><rsub|<text|r>>*G<st>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i><st>>
  (Eq. <reference|del(r)Gmo=sum(nu_i)(mu_io)>). The effect of a small
  pressure change on the chemical potential of a solid or solute is
  negligible. The standard state of the gaseous H<rsub|<math|2>> is the pure
  gas behaving as an ideal gas. Apply Eq. <reference|dmu/dp=Vm> to the
  isothermal pressure change of an ideal gas:
  <vs><math|<D><Pd|\<mu\>|p|T>=V<m>=<frac|R*T|p>*<space|2em>\<mu\><around|(|p<rsub|2>|)>-\<mu\><around|(|p<rsub|1>|)>=R*T*<big|int><rsub|p<rsub|1>><rsup|p<rsub|2>><frac|<difp>|p>=R*T*ln
  <frac|p<rsub|2>|p<rsub|1>>> <vs>For a pressure change from
  <math|1<units|a*t*m>> to <math|1<br>> the change of chemical potential is
  <vs><math|<D>\<mu\><around|(|1<br>|)>-\<mu\><around|(|1<units|a*t*m>|)>=<around|(|<R>|)><around|(|298.15<K>|)>*ln
  <frac|1<br>|1.01325<br>>> <vs><math|<phantom|\<mu\><around|(|1<br>|)>-\<mu\><around|(|1<units|a*t*m>|)>>=-32.63<units|J*<space|0.17em>m*o*l<per>>>
  <vs>Therefore for the cell reaction <math|<chem>H<rsub|2><around|(|g|)>+2*<space|0.17em>A*g*C*l<around|(|s|)><ra>2*<space|0.17em>A*g<around|(|s|)>+2*<space|0.17em>H<rsup|+>*<around|(|a*q|)>+2*<space|0.17em>C*l<rsup|->*<around|(|a*q|)>>
  the pressure change causes <math|<Eeq><st>> to change by
  <vs><math|<D><Eeq><st><around|(|p<st>=1<br>|)>-<Eeq><st><around|(|p<st>=1<units|a*t*m>|)>=-<frac|-<around|(|-32.63<units|J*<space|0.17em>m*o*l<per>>|)>|<around|(|2|)><around|(|96,485<units|C*<space|0.17em>m*o*l<per>>|)>>=-1.691<timesten|-4><V>>
  <vs>The correction to <math|<Eeq><st>> based on the older convention is
  <math|-0.17<units|m*V>>.>

  \;

  <problem><label|prb:14-1/2H2+AgCl-\<gtr\>> Careful
  measurements<footnote|Ref. <cite|bates-54>.> of the equilibrium cell
  potential of the cell

  <\equation*>
    <chem>P*t<jn>H<rsub|2><around|(|g|)><jn>H*C*l*<around|(|a*q|)><jn>A*g*C*l<around|(|s|)><jn>A*g
  </equation*>

  yielded, at <math|298.15<K>> and using a standard pressure of <math|1<br>>,
  the values <math|<Eeq><st>=0.22217<V>> and
  <math|<dif><Eeq><st>/<dif>T=-6.462<timesten|-4><units|V*<space|0.17em>K<per>>>.
  (The requested calculated values are close to, but not exactly the same as,
  the values listed in Appendix <reference|app:props>, which are based on the
  same data combined with data of other workers.)

  <\problemparts>
    \ <problempartAns><label|1/2H2+AgCl-\<gtr\> a> Evaluate
    <math|\<Delta\><rsub|<text|r>>*G<st>>, <math|\<Delta\><rsub|<text|r>>*S<st>>, and
    <math|\<Delta\><rsub|<text|r>>*H<st>> at <math|298.15<K>> for the reaction

    <\equation*>
      <with|math-display|false|<chem><frac|1|2>*H<rsub|2><around|(|g|)>+A*g*C*l<around|(|s|)><arrow>H<rsup|+>*<around|(|a*q|)>+C*l<rsup|->*<around|(|a*q|)>+A*g<around|(|s|)>>
    </equation*>

    <answer|<math|\<Delta\><rsub|<text|r>>*G<st>=-21.436<units|k*J*<space|0.17em>m*o*l<per>>><next-line><math|\<Delta\><rsub|<text|r>>*S<st>=-62.35<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>><next-line><math|\<Delta\><rsub|<text|r>>*H<st>=-40.03<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| From Eq. <reference|del(r)Go=-zFEo>:
    <vs><math|\<Delta\><rsub|<text|r>>*G<st>=-z*F<Eeq><st>=-<around|(|1|)><around|(|96,485<units|C*<space|0.17em>m*o*l<per>>|)><around|(|0.22217<V>|)>=-21.436<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs>From Eq. <reference|del(r)Smo=(nuF)dEo/dT>:
    <vs><math|<D>\<Delta\><rsub|<text|r>>*S<st>=z*F*<frac|<dif><Eeq><st>|<dif>T>=<around|(|1|)><around|(|96,485<units|C*<space|0.17em>m*o*l<per>>|)>*<around|(|-6.462<timesten|-4><units|V*<space|0.17em>K<per>>|)>><no-page-break><vs><math|<phantom|\<Delta\><rsub|<text|r>>*S<st>>=-62.35<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>
    <vs><math|\<Delta\><rsub|<text|r>>*H<st>=T\<Delta\><rsub|<text|r>>*S<st>+\<Delta\><rsub|<text|r>>*G<st>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*H<st>>=<around|(|298.15<K>|)>*<around|(|-62.35<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>|)>+<around|(|-21.436<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*H<st>>=-40.03<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs>The value of <math|\<Delta\><rsub|<text|r>>*H<st>> may also be calculated with Eq.
    <reference|del(r)Hmo=nuF..>.>

    \;

    <problempartAns><label|1/2H2+AgCl-\<gtr\> b> Problem
    12.<reference|prb:12-Cl ion> showed how the standard molar enthalpy of
    formation of the aqueous chloride ion may be evaluated based on the
    convention <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|H<rsup|<math|+>>,<space|0.17em>aq>|)>=0>.
    If this value is combined with the value of <math|\<Delta\><rsub|<text|r>>*H<st>>
    obtained in part <reference|1/2H2+AgCl-\<gtr\> a> of the present problem,
    the standard molar enthalpy of formation of crystalline silver chloride
    can be evaluated. Carry out this calculation for <math|T=298.15<K>> using
    the value <math|\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>=-167.08<units|k*J*<space|0.17em>m*o*l<per>>>
    (Appendix <reference|app:props>).

    <answer|<math|\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-127.05<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| Apply the general relation <math|\<Delta\><rsub|<text|r>>*H<st>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>\<Delta\><rsub|<text|f>>*H<st><around|(|i|)>>
    (Eq. <reference|Hess's law>) to the reaction of part
    <reference|1/2H2+AgCl-\<gtr\> a>: <vs><math|\<Delta\><rsub|<text|r>>*H<st>=-<frac|1|2>\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|H<rsub|<math|2>>,<space|0.17em>g>|)>-\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>+\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|H<rsup|<math|+>>,<space|0.17em>aq>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*H<st>=>+\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>+\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|A*g,<space|0.17em>s>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*H<st>>=-<frac|1|2><around|(|0|)>-\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>+0+\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>+0>
    <vs><math|\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-\<Delta\><rsub|<text|r>>*H<st>+\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>>=-<around|(|-40.03<units|k*J*<space|0.17em>m*o*l<per>>|)>+<around|(|-167.08<units|k*J*<space|0.17em>m*o*l<per>>|)>=-127.05<units|k*J*<space|0.17em>m*o*l<per>>>>\ 

    \;

    <problempartAns><label|1/2H2+AgCl-\<gtr\> c> By a similar procedure,
    evaluate the standard molar entropy, the standard molar entropy of
    formation, and the standard molar Gibbs energy of formation of
    crystalline silver chloride at <math|298.15<K>>. You need the following
    standard molar entropies evaluated from spectroscopic and calorimetric
    data:<next-line><next-line>

    <tabular*|<tformat|<cwith|1|-1|1|1|cell-halign|l>|<cwith|1|-1|1|1|cell-lborder|0ln>|<cwith|1|-1|2|2|cell-halign|l>|<cwith|1|-1|3|3|cell-halign|l>|<cwith|1|-1|3|3|cell-rborder|0ln>|<cwith|1|-1|1|-1|cell-valign|c>|<table|<row|<cell|<math|S<m><st><around|(|<tx|H<rsub|<math|2>>,<space|0.17em>g>|)>=130.68<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>|<cell|<space|2em>>|<cell|<math|S<m><st><around|(|<tx|C*l<rsub|<math|2>>,<space|0.17em>g>|)>=223.08<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>>|<row|<cell|<math|S<m><st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>=56.60<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>|<cell|>|<cell|<math|S<m><st><around|(|<tx|A*g,<space|0.17em>s>|)>=42.55<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>>>>><next-line>

    <answer|<math|S<m><st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=96.16<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>><next-line><math|\<Delta\><rsub|<text|f>>*S<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-57.93><units|J<space|0.17em>K<per><space|0.17em>mol<per>><next-line><math|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-109.78<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| For the standard molar reaction entropy, apply the general
    relation <math|\<Delta\><rsub|<text|r>>*S<st>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*S<rsub|i><st>>
    (Eq. <reference|del(r)Smo=sum(nu_i)Smio>) to the reaction of part
    <reference|1/2H2+AgCl-\<gtr\> a>: <vs><math|\<Delta\><rsub|<text|r>>*S<st>=-<frac|1|2>*S<m><st><around|(|<tx|H<rsub|<math|2>>,<space|0.17em>g>|)>-S<m><st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>+S<m><st><around|(|<tx|H<rsup|<math|+>>,<space|0.17em>aq>|)>+S<m><st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>+S<m><st><around|(|<tx|A*g,<space|0.17em>s>|)>>
    <vs>By convention, <math|S<m><st><around|(|<tx|H<rsup|<math|+>>,<space|0.17em>aq>|)>>
    is zero. <vs><math|S<m><st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-\<Delta\><rsub|<text|r>>*S<st>-<frac|1|2>*S<m><st><around|(|<tx|H<rsub|<math|2>>,<space|0.17em>g>|)>+S<m><st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>+S<m><st><around|(|<tx|A*g,<space|0.17em>s>|)>>
    <vs><math|S<m><st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>/<tx|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>=-<around|(|-62.35|)>-<frac|1|2><around|(|130.68|)>+56.60+42.55=96.16>
    <vs>The formation reaction of AgCl is
    <math|<chem>A*g<around|(|s|)>+<frac|1|2>*C*l<rsub|2><around|(|g|)><ra>A*g*C*l<around|(|s|)>>:
    <vs><math|\<Delta\><rsub|<text|f>>*S<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-S<m><st><around|(|<tx|A*g,<space|0.17em>s>|)>-<frac|1|2>*S<m><st><around|(|<tx|C*l<rsub|<math|2>>,<space|0.17em>g>|)>+S<m><st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>>
    <vs><math|\<Delta\><rsub|<text|f>>*S<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>/<tx|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>=-<around|(|42.55|)>-<frac|1|2><around|(|223.08|)>+96.16=-57.93>
    <vs><math|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>-T\<Delta\><rsub|<text|f>>*S<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>>=-127.05<timesten|3><units|J*<space|0.17em>m*o*l<per>>-<around|(|298.15<K>|)>*<around|(|-57.93<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>>=-109.78<units|k*J*<space|0.17em>m*o*l<per>>>>
  </problemparts>

  <longpage>

  <problem><label|prb:14-AgCl formation>The standard cell potential of the
  cell

  <\equation*>
    <chem>A*g<jn>A*g*C*l<around|(|s|)><jn>H*C*l*<around|(|a*q|)><jn>C*l<rsub|2><around|(|g|)><jn>P*t
  </equation*>

  has been determined over a range of temperature.<footnote|Ref.
  <cite|faita-67>.> At <math|T=298.15<K>>, the standard cell potential was
  found to be <math|<Eeq><st>=1.13579<V>>, and its temperature derivative was
  found to be <math|<dif><Eeq><st>/<dif>T=-5.9863<timesten|-4><units|V*<space|0.17em>K<per>>>.

  <\problemparts>
    <problempart>Write the cell reaction for this cell.

    <soln| <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<table|<row|<cell|left
    electrode reaction (oxidation)>|<cell|<space|2em>>|<cell|<math|<chem>2*<around*|[|A*g<around|(|s|)>+C*l<rsup|->*<around|(|a*q|)><arrow>A*g*C*l<around|(|s|)>+e<rsup|->|]>>>>|<row|<cell|right
    electrode reaction (reduction)>|<cell|>|<cell|<math|<chem>C*l<rsub|2><around|(|g|)>+2*<space|0.17em>e<rsup|-><arrow>2*<space|0.17em>C*l<rsup|->*<around|(|a*q|)>>>>|<row|<cell|cell
    reaction>|<cell|>|<cell|<math|<chem>2*<space|0.17em>A*g<around|(|s|)>+C*l<rsub|2><around|(|g|)><arrow>2*<space|0.17em>A*g*C*l<around|(|s|)>>>>>>>>

    \;

    <problempartAns>Use the data to evaluate the standard molar enthalpy of
    formation and the standard molar Gibbs energy of formation of crystalline
    silver chloride at <math|298.15<K>>. (Note that this calculation provides
    values of quantities also calculated in Prob.
    14.<reference|prb:14-1/2H2+AgCl-\<gtr\>> using independent data.)

    <answer|<math|\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-126.81<units|k*J*<space|0.17em>m*o*l<per>>><next-line><math|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=-109.59<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| The cell reaction is the formation reaction of AgCl(s). As written
    in the solution for part (a), the electron number is <math|z=2>. To
    simplify the calculation, divide the coefficients by 2:
    <vs><math|<chem>A*g<around|(|s|)>+<frac|1|2>*C*l<rsub|2><around|(|g|)><arrow>A*g*C*l<around|(|s|)>*<space|2em>z=1>
    <vs>Then we have, from Eq. <reference|del(r)Hmo=nuF..>:
    <vs><math|<D>\<Delta\><rsub|<text|f>>*H<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=\<Delta\><rsub|<text|r>>*H<st>=z*F*<around*|(|T*<frac|<dif><Eeq><st>|<dif>T>-<Eeq><st>|)>>
    <vs><math|<phantom|m*m*m>=<around|(|1|)><around|(|96,485<units|C*<space|0.17em>m*o*l<per>>|)>*<around*|[|<around|(|298.15<K>|)>*<around|(|-5.9863<timesten|-4><units|V*<space|0.17em>K<per>>|)>-1.13579<V>|]>>
    <vs><math|<phantom|m*m*m>=-126.81<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs>and from Eq. <reference|del(r)Go=-zFEo>:
    <vs><math|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>=\<Delta\><rsub|<text|r>>*G<st>=-z*F<Eeq><st>=-<around|(|1|)><around|(|96,485<units|C*<space|0.17em>m*o*l<per>>|)><around|(|1.13579<V>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|A*g*C*l,<space|0.17em>s>|)>>=-109.59<units|k*J*<space|0.17em>m*o*l<per>>>>
  </problemparts>

  <problemAns><label|prb:14-Ks(AgCl)>Use data in Sec. <reference|14-st molar
  rxn quantities> to evaluate the solubility product of silver chloride at
  <math|298.15<K>>.

  <\answer>
    <math|K<subs|s>=1.76<timesten|-10>>
  </answer>

  <\soln>
    \ From Eq. <reference|ln(K)=nuFEo/RT>: <vs><math|<D>ln
    K<subs|s>=<frac|z*F|R*T><Eeq><st>=<frac|<around|(|1|)>(96,485<units|C*<space|0.17em>m*o*l<per>)>|<around|(|<R>|)><around|(|298.15<K>|)>>*<around|(|-0.5770<V>|)>=-22.46>
    <vs><math|K<subs|s>=1.76<timesten|-10>>
  </soln>

  \;

  <problem>The equilibrium cell potential of the galvanic cell

  <\equation*>
    <tx|P*t><jn><tx|H<rsub|<math|2>>*<around|(|g,<space|0.17em><math|<fug|=>1<br>>|)>><jn><tx|H*C*l*<around|(|a*q,<space|0.17em><math|0.500<units|m*o*l*<space|0.17em>k*g<per>>>|)>><jn><tx|C*l<rsub|<math|2>>*<around|(|g,<space|0.17em><math|<fug|=>1<br>>|)>><jn><tx|P*t>
  </equation*>

  is found to be <math|<Eeq>=1.410<V>> at <math|298.15<K>>. The standard cell
  potential is <math|<Eeq><st>=1.360<V>>.

  <\problemparts>
    <problempart>Write the cell reaction and calculate its thermodynamic
    equilibrium constant at <math|298.15<K>>.

    <soln| <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<table|<row|<cell|left
    electrode reaction (oxidation)>|<cell|<space|2em>>|<cell|<math|<chem>H<rsub|2><around|(|g|)><arrow>2*<space|0.17em>H<rsup|+>*<around|(|a*q|)>+2*<space|0.17em>e<rsup|->>>>|<row|<cell|right
    electrode reaction (reduction)>|<cell|>|<cell|<math|<chem>C*l<rsub|2><around|(|g|)>+2*<space|0.17em>e<rsup|-><arrow>2*<space|0.17em>C*l<rsup|->*<around|(|a*q|)>>>>|<row|<cell|cell
    reaction>|<cell|>|<cell|<math|<chem>H<rsub|2><around|(|g|)>+C*l<rsub|2><around|(|g|)><arrow>2*<space|0.17em>H<rsup|+>*<around|(|a*q|)>+2*<space|0.17em>C*l<rsup|->*<around|(|a*q|)>>>>>>>
    <vs>From Eq. <reference|ln(K)=nuFEo/RT>: <vs><math|<D>ln
    K<subs|s>=<frac|z*F|R*T><Eeq><st>=<frac|<around|(|2|)>(96,485<units|C*<space|0.17em>m*o*l<per>)>|<around|(|<R>|)><around|(|298.15<K>|)>><around|(|1.360<V>|)>=105.9>
    <vs><math|K<subs|s>=9<timesten|45>>>\ 

    \;

    <problempartAns>Use the cell measurement to calculate the mean ionic
    activity coefficient of aqueous HCl at <math|298.15<K>> and a molality of
    <math|0.500<units|m*o*l*<space|0.17em>k*g<per>>>.

    <answer|<math|<g><rsub|\<pm\>>=0.756>>

    <soln| From Eq. <reference|Nernst eq, 298K>:
    <vs><math|<D><Eeq>=<Eeq><st>-<frac|0.02569<V>|z>*ln
    Q<subs|r*x*n>=<Eeq><st>-<frac|0.02569<V>|z>*ln
    <frac|<g><rsub|\<pm\>><rsup|4><around*|(|m<B>/m<st>|)><rsup|4>|<around|(|<fug><subs|H<rsub|<math|2>>>/p<st>|)>*<around|(|<fug><subs|C*l<rsub|<math|2>>>/p<st>|)>>>
    <vs><math|<D><phantom|<Eeq>>=<Eeq><st>-<frac|0.02569<V>|2>*ln
    <frac|<g><rsub|\<pm\>><rsup|4><around|(|0.500|)><rsup|4>|<around|(|1|)><around|(|1|)>>>
    <vs><math|<D>ln <around|(|0.500<g><rsub|\<pm\>>|)>=<frac|<Eeq><st>-<Eeq>|2\<times\>0.02569<V>>=<frac|<around|(|1.360-1.410|)><V>|2\<times\>0.02569<V>>=-0.973>
    <vs><math|<g><rsub|\<pm\>>=0.756>>
  </problemparts>

  \;

  <problem>Consider the following galvanic cell, which combines a hydrogen
  electrode and a calomel electrode:

  <\equation*>
    <chem>P*t<jn>H<rsub|2><around|(|g|)><jn>H*C*l*<around|(|a*q|)><jn>H*g<rsub|2>*C*l<rsub|2><around|(|s|)><jn>H*g<around|(|l|)><jn>P*t
  </equation*>

  <\problemparts>
    <problempart>Write the cell reaction.

    <soln| <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<table|<row|<cell|left
    electrode reaction (oxidation)>|<cell|<space|2em>>|<cell|<math|<chem>H<rsub|2><around|(|g|)><arrow>2*<space|0.17em>H<rsup|+>*<around|(|a*q|)>+2*<space|0.17em>e<rsup|->>>>|<row|<cell|right
    electrode reaction (reduction)>|<cell|>|<cell|<math|<chem>H*g<rsub|2>*C*l<rsub|2><around|(|s|)>+2*<space|0.17em>e<rsup|-><arrow>2*<space|0.17em>H*g<around|(|l|)>+2*<space|0.17em>C*l<rsup|->*<around|(|a*q|)>>>>>>>
    <vs>cell reaction: <math|<chem>H<rsub|2><around|(|g|)>+H*g<rsub|2>*C*l<rsub|2><around|(|s|)><arrow>2*<space|0.17em>H<rsup|+>*<around|(|a*q|)>+2*<space|0.17em>C*l<rsup|->*<around|(|a*q|)>+2*<space|0.17em>H*g<around|(|l|)>>>\ 

    \;

    <problempartAns>At <math|298.15<K>>, the standard cell potential of this
    cell is <math|<Eeq><st>=0.2680<V>>. Using the value of
    <math|\<Delta\><rsub|<text|f>>*G<st>> for the aqueous chloride ion in Appendix
    <reference|app:props>, calculate the standard molar Gibbs energy of
    formation of crystalline mercury(I) chloride (calomel) at
    <math|298.15<K>>.

    <answer|<math|\<Delta\><rsub|<text|f>>*G<st>=-210.72<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| From Eq. <reference|del(r)Go=-zFEo>:
    <vs><math|\<Delta\><rsub|<text|r>>*G<st>=-z*F<Eeq><st>=-<around|(|2|)><around|(|96,485<units|C*<space|0.17em>m*o*l<per>>|)><around|(|0.2680<V>|)>=-5.172<timesten|4><units|J*<space|0.17em>m*o*l<per>>>
    <vs><math|\<Delta\><rsub|<text|r>>*G<st>=<big|sum>\<nu\><rsub|i>\<Delta\><rsub|<text|f>>*G<st><around|(|i|)>=-<around|(|0|)>-\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|H*g<rsub|<math|2>>*Cl<rsub|<math|2>>,<space|0.17em>s>|)>+2<around|(|0|)>+2\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>+2<around|(|0|)>>
    <vs><math|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|H*g<rsub|<math|2>>*Cl<rsub|<math|2>>,<space|0.17em>s>|)>=-\<Delta\><rsub|<text|r>>*G<st>+2\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|H*g<rsub|<math|2>>*Cl<rsub|<math|2>>,<space|0.17em>s>|)>>=-<around|(|-5.172<timesten|4><units|J*<space|0.17em>m*o*l<per>>|)>+2*<around|(|-131.22<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|H*g<rsub|<math|2>>*Cl<rsub|<math|2>>,<space|0.17em>s>|)>>=-210.72<units|k*J*<space|0.17em>m*o*l<per>>>>

    \;

    <problempartAns>Calculate the solubility product of mercury(I) chloride
    at <math|298.15<K>>. The dissolution equilibrium is
    <math|<chem>H*g<rsub|2>*C*l<rsub|2><around|(|s|)><arrows|H*g<rsub|2>><rsup|2+><around|(|a*q|)>+2*<space|0.17em>C*l<rsup|->*<around|(|a*q|)>>.
    Take values for the standard molar Gibbs energies of formation of the
    aqueous ions from Appendix <reference|app:props>.

    <answer|<math|K<subs|s>=1.4<timesten|-18>>>

    <soln| <math|\<Delta\><rsub|<text|r>>*G<st>=<big|sum>\<nu\><rsub|i>\<Delta\><rsub|<text|f>>*G<st><around|(|i|)>=-\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|H*g<rsub|<math|2>>*Cl<rsub|<math|2>>,<space|0.17em>s>|)>+\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|H*g<math|<rsub|2><rsup|2+>>,<space|0.17em>aq>|)>+2\<Delta\><rsub|<text|f>>*G<st><around|(|<tx|C*l<rsup|<math|->>,<space|0.17em>aq>|)>>
    <vs><math|\<Delta\><rsub|<text|r>>*G<st>/<tx|k*J*<space|0.17em>m*o*l<per>>=-<around|(|-210.72|)>+<around|(|153.57|)>+2*<around|(|-131.22|)>=101.85>
    <vs><math|<D>K<subs|s>=exp <around*|(|-<frac|\<Delta\><rsub|<text|r>>*G<st>|R*T>|)>=exp
    <around*|[|-<frac|101.85<timesten|3><units|J*<space|0.17em>m*o*l<per>>|<around|(|<R>|)><around|(|298.15<K>|)>>|]>=1.4<timesten|-18>>>
  </problemparts>

  \;

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|9|9|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|m<B>><space|0.17em>/<space|0.17em>(mol<space|0.17em>kg<per>)>|<cell|<math|<Eeq>><space|0.17em>/<space|0.17em>V>>|<row|<cell|0.0004042>|<cell|0.47381>>|<row|<cell|0.0008444>|<cell|0.43636>>|<row|<cell|0.0008680>|<cell|0.43499>>|<row|<cell|0.0013554>|<cell|0.41243>>|<row|<cell|0.001464>|<cell|0.40864>>|<row|<cell|0.001850>|<cell|0.39667>>|<row|<cell|0.002396>|<cell|0.38383>>|<row|<cell|0.003719>|<cell|0.36173>>>>>
  </big-table|<label|tbl:14-Ag-AgBr>Equilibrium cell potential as a function
  of HBr molality <math|m<B>>.>

  \;

  <problemAns><label|prb:14-AgAgBr> Table
  <reference|tbl:14-Ag-AgBr><vpageref|tbl:14-Ag-AgBr> lists equilibrium cell
  potentials obtained with the following cell at
  <math|298.15<K>>:<footnote|Ref. <cite|keston-35>.>

  <\equation*>
    <tx|P*t><jn><tx|H><rsub|2><tx|<around|(|g,<space|0.17em><math|1.01<br>>|)>><jn><tx|H*B*r*<around|(|a*q,<space|0.17em><math|m<B>>|)>><jn><tx|A*g*B*r<around|(|s|)>><jn><tx|A*g>
  </equation*>

  Use these data to evaluate the standard electrode potential of the
  silver-silver bromide electrode at this temperature to the nearest
  millivolt. (Since the electrolyte solutions are quite dilute, you may
  ignore the term <math|B*a*<sqrt|m<B>>> in Eq. <reference|E'=E-...>.)

  <\answer>
    <math|E<st>=0.071<V>>
  </answer>

  <\soln>
    \ The cell has a hydrogen electrode at the left and a silver-silver
    bromide electrode at the right. The standard electrode potential
    <math|E<st>> of the silver-silver bromide electrode is therefore equal to
    the standard cell potential <math|<Eeq><st>> of this cell. <vs>Figure
    <reference|fig:14-AgAgBr><vpageref|fig:14-AgAgBr>

    <\big-figure>
      <boxedfigure|<image|./14-SUP/Ag-AgBr.eps||||> <capt|Graph for Problem
      14.<reference|prb:14-AgAgBr>.<label|fig:14-AgAgBr>>>
    </big-figure|>

    shows values of <math|E<rprime|'>> plotted versus <math|m<B>> (open
    circles), where the function <math|E<rprime|'>> is defined by Eq.
    <reference|E'=E-...> with <math|a> set equal to zero:
    <vs><math|<D>E<rprime|'>=<Eeq>+<frac|2*R*T|F>*<around|(|-A*<sqrt|m<B>>|)>+<frac|2*R*T|F>*ln
    <frac|m<B>|m<st>>-<frac|R*T|2*F>*ln <frac|<fug><subs|H<rsub|<math|2>>>|p<st>>>
    <vs><math|<D><phantom|E<rprime|'>>=<Eeq>+<frac|R*T|F>*<around*|(|-2*A*<sqrt|m<B>>+2*ln
    <frac|m<B>|m<st>>-<frac|1|2>*ln <frac|<fug><subs|H<rsub|<math|2>>>|p<st>>|)>>
    <vs><math|<D><phantom|E<rprime|'>>=<Eeq>+<frac|<around|(|<R>|)><around|(|298.15<K>|)>|96,485<units|C*<space|0.17em>m*o*l<per>>>>
    <vs><math|<D><phantom|E<rprime|'>=<Eeq>+>\<times\><around*|[|-<around|(|2|)><around|(|1.1744<units|k*g<rsup|<math|1/2>>*<space|0.17em>mol<rsup|<math|-1/2>>>|)><sqrt|m<B>>+2*ln
    <around|(|m<B><space|0.17em>/<units|m*o*l*<space|0.17em>k*g<per>>|)>-<frac|1|2>*ln
    <around|(|1.01|)>|]>> <vs>To the nearest millivolt, <math|E<rprime|'>>
    extrapolated to <math|m<B>=0> (filled circle) has the value
    <math|E<st>=<Eeq><st>=0.071<V>>.
  </soln>

  \;

  <problem>The cell diagram of a mercury cell can be written

  <\equation*>
    <chem>Z*n<around|(|s|)><jn>Z*n*O<around|(|s|)><jn>N*a*O*H*<around|(|a*q|)><jn>H*g*O<around|(|s|)><jn>H*g<around|(|l|)>
  </equation*>

  <\problemparts>
    \ <problempart>Write the electrode reactions and cell reaction with
    electron number <math|z=2>.

    <soln| At the left electrode: <math|<chem>Z*n<around|(|s|)>+2*<space|0.17em>O*H<rsup|->*<around|(|a*q|)><ra>Z*n*O<around|(|s|)>+H<rsub|2>*O<around|(|l|)>+2*<space|0.17em>e<rsup|->>
    <vs>At the right electrode: <math|<chem>H*g*O<around|(|s|)>+H<rsub|2>*O<around|(|l|)>+2*<space|0.17em>e<rsup|-><ra>H*g<around|(|l|)>+2*<space|0.17em>O*H<rsup|->*<around|(|a*q|)>>
    <vs>Cell reaction: <math|<chem>Z*n<around|(|s|)>+H*g*O<around|(|s|)><ra>Z*n*O<around|(|s|)>+H*g<around|(|l|)>>>\ 

    \;

    <problempart>Use data in Appendix <reference|app:props> to calculate the
    standard molar reaction quantities <math|\<Delta\><rsub|<text|r>>*H<st>>,
    <math|\<Delta\><rsub|<text|r>>*G<st>>, and <math|\<Delta\><rsub|<text|r>>*S<st>> for the cell reaction
    at <math|298.15<K>>.

    <soln| <math|\<Delta\><rsub|<text|r>>*H<st>=\<Delta\><rsub|<text|f>>*H<st><tx|<around|(|Z*n*O|)>>+\<Delta\><rsub|<text|f>>*H<st><tx|<around|(|H*g|)>>-\<Delta\><rsub|<text|f>>*H<st><tx|<around|(|Z*n|)>>-\<Delta\><rsub|<text|f>>*H<st><tx|H*g*O)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*H<st>>=<around|[|<around|(|-350.46|)>+0-0-<around|(|-90.79|)>|]><units|k*J*<space|0.17em>m*o*l<per>>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*H<st>>=-259.67<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs><math|\<Delta\><rsub|<text|r>>*G<st>=\<Delta\><rsub|<text|f>>*G<st><tx|<around|(|Z*n*O|)>>+\<Delta\><rsub|<text|f>>*G<st><tx|<around|(|H*g|)>>-\<Delta\><rsub|<text|f>>*G<st><tx|<around|(|Z*n|)>>-\<Delta\><rsub|<text|f>>*G<st><tx|H*g*O)>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*G<st>>=<around|[|<around|(|-320.48|)>+0-0-<around|(|-58.54|)>|]><units|k*J*<space|0.17em>m*o*l<per>>>
    <vs><math|<phantom|\<Delta\><rsub|<text|r>>*G<st>>=-261.94<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs><math|T\<Delta\><rsub|<text|r>>*S<st>=\<Delta\><rsub|<text|r>>*H<st>-\<Delta\><rsub|<text|r>>*G<st>=2.27<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs><math|<D>\<Delta\><rsub|<text|r>>*S<st>=<frac|2.27<timesten|3><units|J*<space|0.17em>m*o*l<per>>|298.15<K>>=7.61<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>>>

    \;

    <problempartAns>Calculate the standard cell potential of the mercury cell
    at <math|298.15<K>> to the nearest <math|0.01<V>>.

    <answer|<math|<Eeq><st>=1.36<V>>>

    <soln| <math|<D><Eeq><st>=-<frac|\<Delta\><rsub|<text|r>>*G<st>|z*F>=-<frac|<around|(|-261.94<timesten|3><units|J*<space|0.17em>m*o*l<per>>|)>|<around|(|2|)><around|(|96,485<units|C*<space|0.17em>m*o*l<per>>|)>>=1.36<V>>>

    \;

    <problempartAns>Evaluate the ratio of heat to advancement,
    <math|<dq>/<dif>\<xi\>>, at a constant temperature of <math|298.15<K>>
    and a constant pressure of <math|1<br>>, for the cell reaction taking
    place in two different ways: reversibly in the cell, and spontaneously in
    a reaction vessel that is not part of an electrical circuit.

    <answer|In the cell:<next-line><math|<dq>/<dif>\<xi\>=2.27<units|k*J*<space|0.17em>m*o*l<per>>><next-line>In
    a reaction vessel:<next-line><math|<dq>/<dif>\<xi\>=-259.67<units|k*J*<space|0.17em>m*o*l<per>>>>

    <soln| In the cell: <vs><math|<D><frac|<dq>|<dif>\<xi\>>=T\<Delta\><rsub|<text|r>>*S<st>=2.27<units|k*J*<space|0.17em>m*o*l<per>>>
    <vs>In a reaction vessel, from Eq. <reference|del(r)H=dq/dxi>:
    <vs><math|<dq>/<dif>\<xi\>=\<Delta\><rsub|<text|r>>*H<st>=-259.67<units|k*J*<space|0.17em>m*o*l<per>>>>

    \;

    <problempartAns>Evaluate <math|<dif><Eeq><st>/<dif>T>, the temperature
    coefficient of the standard cell potential.

    <answer|<math|<dif><Eeq><st>/<dif>T=3.9<timesten|-5><units|V*<space|0.17em>K<per>>>>
  </problemparts>

  \ <soln| From Eq. <reference|del(r)Smo=(nuF)dEo/dT>:
  <vs><math|<D><frac|<dif><Eeq><st>|<dif>T>=<frac|\<Delta\><rsub|<text|r>>*S<st>|z*F>=<frac|7.61<units|J*<space|0.17em>K<per><space|0.17em>m*o*l<per>>|<around|(|2|)><around|(|96,485<units|C*<space|0.17em>m*o*l<per>>|)>>=3.9<timesten|-5><units|V*<space|0.17em>K<per>>>>
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|footnote-1|<tuple|1|?>>
  </collection>
</references>