<TeXmacs|2.1>

<style|source>

<\body>
  <\active*>
    <\src-title>
      <src-package|devoe-text-macros|1.0.1>

      <src-purpose|Style package for Thermodynamics and Chemistry TeXmacs
      source files.>
    </src-title>
  </active*>

  <use-package|number-long-article|termes-font>

  <\active*>
    <\src-comment>
      Style parameters.
    </src-comment>
  </active*>

  <assign|info-flag|detailed>

  <assign|page-height|auto>

  <assign|page-width|auto>

  <assign|page-type|letter>

  <\active*>
    <\src-comment>
      Macro definitions.
    </src-comment>
  </active*>

  <assign|equation-lab-cov|<\macro|body|lab|cov>
    <\surround|<set-binding|<arg|lab>>|<space|5mm><tabular|<tformat|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|(<arg|lab>)>>|<row|<cell|(<arg|cov>)>>>>>>
      <\equation*>
        <arg|body>
      </equation*>
    </surround>
  </macro>>

  <assign|equation-cov|<\macro|body|cov>
    <\surround|<next-equation>|>
      <equation-lab-cov|<arg|body>|<the-equation>|<arg|cov>>
    </surround>
  </macro>>

  <assign|chem|<macro|formula|charge|<text|<math|<arg|formula>>><rsup|><math|<rsup|<arg|charge>>>>>

  <assign|vpageref|<macro|name|<reference|<arg|name>> on page
  <pageref|<arg|name>>>>

  <assign|dbar|<macro|var|<math|<text|�>*<arg|var>>>>

  <assign|dvar|<macro|var|<math|<text|d>*<arg|var>>>>

  <assign|newterm|<macro|term|<strong|<arg|term>><glossary|<arg|term>>>>

  <assign|newterm-fmt|<macro|term|term-disp|<strong|<arg|term><glossary|<arg|term>>>>>

  <assign|newterm-explain|<macro|term|explanation|<strong|<arg|term>><glossary-explain|<arg|term>|<arg|explanation>>>>

  <assign|newterm-fmt-explain|<macro|term|term-disp|explain|<strong|<arg|term-disp>><glossary-explain|<arg|term>|<arg|explain>>>>

  <assign|bk-equal-def|<macro|<above|<text|<space|0.5em>>=<text|<space|0.5em>>|<text|def>>>>

  <assign|equation-lab-cov2|<\macro|body|lab|cov>
    <\surround|<set-binding|<arg|lab>>|<space|5mm><tabular|<tformat|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|(<arg|lab>)>>|<row|<cell|<arg|cov>>>>>>>
      <\equation*>
        <arg|body>
      </equation*>
    </surround>
  </macro>>

  <assign|equation-cov2|<\macro|body|cov>
    <surround|<next-equation>||<equation-lab-cov2|<arg|body>|<the-equation>|<arg|cov>>>
  </macro>>

  <assign|ir|<macro|<dilate|.6|.6|<tabular|<tformat|<cwith|1|-1|1|1|cell-halign|c>|<cwith|1|-1|1|1|cell-lsep|0ln>|<cwith|1|-1|1|1|cell-rsep|0ln>|<cwith|1|-1|1|1|cell-bsep|0ln>|<cwith|1|-1|1|1|cell-tsep|0ln>|<table|<row|<cell|irrev>>|<row|<cell|rev>>>>>>>>

  <assign|ra|<macro|<math|\<rightarrow\>>>>

  <assign|pha|<macro|<text|\<#3B1\>>>>

  <assign|phb|<macro|<text|\<#3B2\>>>>

  <assign|aph|<macro|<rsup|<text|\<#3B1\>>>>>

  <assign|bph|<macro|<rsup|<text|\<#3B2\>>>>>

  <assign|Pd|<macro|num|den|const|<math|<around*|(|<dfrac|\<partial\>*<arg|num>|\<partial\>*<arg|den>>|)><rsub|<arg|const>>>>>

  <assign|kT|<macro|<math|\<kappa\><rsub|T>>>>

  <assign|dif|<\macro>
    <text|d>*
  </macro>>

  <assign|difp|<macro|<text|d>*p>>

  <assign|dq|<macro|<text|�>*q>>

  <assign|bd|<macro|<rsub|<text|b>>>>

  <assign|dw|<macro|<text|�>*w>>

  <assign|pd|<macro|num|den|const|<around*|(|\<partial\>*<arg|num>/\<partial\>*<arg|den>|)><rsub|<arg|const>>>>

  <assign|defn|<macro|<bk-equal-def> >>

  <assign|df|<macro|<dvar|f>>>

  <assign|dx|<macro|<dvar|x>>>

  <assign|CVm|<macro|C<rsub|V,<text|m>>>>

  <assign|Cpm|<macro|C<rsub|p,<text|m>>>>

  <assign|g|<macro|\<gamma\>>>

  <assign|degC|<macro|<rsup|\<circ\>><text|C>>>

  <assign|upOmega|<macro|\<Omega\>>>

  <assign|timesten|<macro|var|\<times\>10<rsup|<arg|var>>>>

  <assign|Del|<macro|\<Delta\>*>>

  <assign|m|<macro|<rsub|<text|m>>>>

  <assign|st|<macro|<rsup|\<circ\>>>>

  <assign|gas|<macro|<math| <around*|(|<text|g>|)>>>>

  <assign|fug|<macro|f>>

  <assign|bPd|<macro|numer|denom|constant|<around*|[|<dfrac|\<partial\>*<arg|numer>|\<partial\>*<arg|denom>>|]><rsub|<arg|constant>>>>

  <assign|br|<macro|<math| <text|bar>>>>

  <assign|el|<macro|<math|<inactive*|<rsub|<text|el>>>>>>

  <assign|dt|<macro|<dvar|t>>>

  <assign|K|<macro|<math|<inactive*| <text|K>>>>>

  <assign|As|<macro|<math|A<rsub|<text|s>>>>>

  <assign|aphp|<macro|<rsup|<math|\<alpha\><rprime|'>>>>>

  <assign|gph|<macro|<rsup|\<gamma\>>>>

  <assign|liquid|<macro|<around*|(|<text|l>|)>>>

  <assign|A|<macro|<rsub|<text|A>>>>

  <assign|B|<macro|<rsub|<text|B>>>>

  <assign|C|<macro|<rsub|<text|C>>>>

  <assign|mA|<macro|<rsub|<text|m>,<text|A>>>>

  <assign|mB|<macro|<rsub|<text|m>,<text|B>>>>

  <assign|mol|<macro|<text|mol>>>

  <assign|mix|<macro|<around*|(|<text|mix>|)>>>

  <assign|bpd|<macro|numer|denom|const|<math|<around*|[|\<partial\><arg|numer>/\<partial\><arg|denom>|]><rsub|<arg|const>>>>>

  <assign|allni|<macro|<math|<around*|{|n<rsub|i>|}>>>>

  <assign|kHi|<macro|k<rsub|<text|H>,i>>>

  <assign|kHB|<macro|<math|k<rsub|<text|H>,<text|B>>>>>

  <assign|cbB|<macro|<rsub|c,<text|B>>>>

  <assign|mbB|<macro|<rsub|m,<text|B>>>>

  <assign|xbB|<macro|<rsub|x,<text|B>>>>

  <assign|rf|<macro|<rsup|<text|ref>>>>

  <assign|varPi|<macro|\<#1D6F1\>>>

  <assign|G|<macro|\<#1D6E4\>>>

  <assign|sur|<macro|<rsup|<text|sur>>>>

  <assign|E|<macro|<rsup|<text|<with|font-family|ss|E>>>>>

  <assign|arrow|<macro|\<rightarrow\>>>

  <assign|arrows|<macro|\<rightleftharpoons\>>>

  <assign|rxn|<macro|<around*|(|<text|rxn>|)>>>

  <assign|sol|<macro|<around*|(|<text|sol>|)>>>

  <assign|solmB|<macro|<around*|(|<text|sol>,m<rsub|<text|B>>|)>>>

  <assign|dil|<macro|<around*|(|<text|dil>|)>>>

  <assign|varPhi|<macro|\<#1D6F7\>>>

  <assign|expt|<macro|<inactive|<around|(|<text|expt>|)>>>>

  <assign|eq|<macro|<rsub|<text|eq>>>>

  <assign|f|<macro|<rsub|<text|f>>>>

  <assign|solid|<macro|<around*|(|<text|s>|)>>>

  <assign|fA|<macro|<rsub|<text|f>,<text|A>>>>

  <assign|sln|<macro|<inactive|<around|(|<text|sln>|)>>>>

  <assign|fB|<macro|<rsub|<text|f>,<text|B>>>>

  <assign|xbC|<macro|<rsub|<text|x>,<text|C>>>>

  <assign|br|<macro|<math| <text|bar>>>>

  <assign|Rsix|<macro|8.31447 <text|J>\<cdot\><text|K><rsup|-1>\<cdot\><text|mol><rsup|-1>>>

  <assign|description-aligned|<\macro|body>
    <compound|<if|<and|<value|<merge|prefix-|description-aligned>>|<unequal|<value|last-item-nr>|0>>|list*|list>|<macro|name|<aligned-item|<item-strong|<arg|name><item-spc>>>>|<macro|name|<with|mode|math|<with|font-series|bold|math-font-series|bold|<rigid|\<ast\>>>>>|<arg|body>>
  </macro>>

  <assign|rsupout|<macro|body|<rsup|><rsup|<arg|body>>>>

  <assign|jn|<macro|<text| \| >>>

  <assign|ljn|<macro| \<#00a6\> >>

  <assign|lljn|<macro| \<#00a6\>\<#00a6\> >>

  <assign|dQ|<macro|<dbar|Q>>>

  <assign|sys|<macro|<rsub|<text|sys>>>>

  <assign|per|<macro|<rsup|-1>>>

  <assign|Eeq|<macro|<math|<inactive*|E<rsub|<text|cell>,<text|eq>>>>>>

  <assign|mue|<macro|\<mu\><rsub|<text|e>>>>

  <assign|Ej|<macro|E<rsub|<text|j>>>>

  <assign|V|<macro|<text|V>>>

  <assign|bktable3|<macro|body|<tformat|<twith|table-min-rows|2>|<twith|table-min-cols|2>|<cwith|1|1|1|-1|cell-tborder|2ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|-1|-1|1|-1|cell-bborder|2ln>|<cwith|1|-1|1|-1|cell-bsep|0.25fn>|<cwith|1|-1|1|-1|cell-tsep|0.25fn>|<cwith|1|-1|1|-1|cell-lsep|0.50fn>|<cwith|1|-1|1|-1|cell-rsep|0.50fn>|<arg|body>>>>

  <assign|P|<macro|<math|<slanted|\<Pi\>>>>>

  <assign|Delsub|<macro|body|\<Delta\><rsub|<text|<arg|body>>>*>>

  <assign|dotprod|<macro|\<bullet\>>>

  <assign|sups|<macro|body|<rsup|<text|<arg|body>>>>>

  <assign|subs|<macro|body|<rsub|<text|<arg|body>>>>>

  <assign|onehalf|<macro|<tfrac|1|2>*>>

  <assign|lab|<macro|<rsub|<text|lab>>>>

  <assign|cm|<macro|<rsub|<text|cm>>>>

  \;
</body>

<\initial>
  <\collection>
    <associate|preamble|true>
  </collection>
</initial>