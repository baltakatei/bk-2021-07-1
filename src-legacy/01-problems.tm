<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <label|Chap1><comment| need more problems>

  <problem>Consider the following equations for the pressure of a real gas.
  For each equation, find the dimensions of the constants <math|a> and
  <math|b> and express these dimensions in SI units.

  <\problemparts>
    \ <problempart> <I|Dieterici equation\|p>The Dieterici equation:

    <\equation*>
      p=<frac|R*T*e<rsup|-<around|(|a*n/V*R*T|)>>|<around|(|V/n|)>-b>
    </equation*>

    <soln| Since <math|a*n/V*R*T> is a power, it is dimensionless and
    <math|a> has the same dimensions as <math|V*R*T/n><@>. These dimensions
    are volume<space|0.17em><math|\<cdot\><space|0.17em>>energy/amount<rsup|<math|2>>,
    expressed in m<rsup|<math|3>><space|0.17em>J<space|0.17em>mol<rsup|<math|-2>>.
    <math|b> has the same dimensions as <math|V/n>, which are volume/amount
    expressed in m<rsup|<math|3>><space|0.17em>mol<per>.> <problempart>
    <I|Redlich--Kwong equation\|p>The Redlich--Kwong equation:

    <\equation*>
      p=<frac|R*T|<around|(|V/n|)>-b>-<frac|a*n<rsup|2>|T<rsup|1/2>*V*<around|(|V+n*b|)>>
    </equation*>

    <soln| The term <math|a*n<rsup|2>/T<rsup|1/2>*V*<around|(|V+n*b|)>> has
    the same dimensions as <math|p>, so <math|a> has the same dimensions as
    <math|T<rsup|1/2>*V<rsup|2>*p*n<rsup|-2>>. The SI units are
    K<rsup|<math|1/2>><space|0.17em>m<rsup|<math|6>><space|0.17em>Pa<space|0.17em>mol<rsup|<math|-2>>.
    <math|b> has the same dimensions as <math|V/n>, which are volume/amount
    expressed in m<rsup|<math|3>><space|0.17em>mol<per>.>
  </problemparts>

  \;
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|Chap1|<tuple|?|?>>
  </collection>
</references>