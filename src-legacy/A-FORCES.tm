<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;

    \;

    \;
  </hide-preamble>

  <appendix|Forces, Energy, and Work><label|app:forces>

  The aim of this appendix is to describe a simple model that will help to
  clarify the meaning of energy and mechanical work in macroscopic systems.
  The appendix applies fundamental principles of classical mechanics to a
  collection of material particles representing a closed system and its
  surroundings. Although classical mechanics cannot duplicate all features of
  a chemical system\Vfor instance, quantum properties of atoms are
  ignored\Vthe behavior of the particles and their interactions will show us
  how to evaluate the thermodynamic work in a real system.

  <index-complex|<tuple|force>||app:forces idx1|<tuple|Force>>In broad
  outline the derivation is as follows. An <subindex|Reference
  frame|inertial><subsubindex|Frame|reference|inertial>inertial reference
  frame in which Newton's laws of motion are valid is used to describe the
  positions and velocities of the particles. The particles are assumed to
  exert central forces on one another, such that between any two particles
  the force is a function only of the interparticle distance and is directed
  along the line between the particles.

  We define the kinetic energy of the collection of particles as the sum for
  all particles of <math|<onehalf>m*v<rsup|2>> (where <math|m> is mass and
  <math|v> is velocity). We define the potential energy as the sum over
  pairwise particle\Uparticle interactions of potential functions that depend
  only on the interparticle distances. The total energy is the sum of the
  kinetic and potential energies. With these definitions and Newton's laws, a
  series of mathematical operations leads to the principle of the
  conservation of energy: the total energy remains constant over time.

  Continuing the derivation, we consider one group of particles to represent
  a closed thermodynamic system and the remaining particles to constitute the
  surroundings. The system particles may interact with an external force
  field, such as a gravitational field, created by some of the particles in
  the surroundings. The energy of the system is considered to be the sum of
  the kinetic energy of the system particles, the potential energy of
  pairwise particle\Uparticle interactions within the system, and the
  potential energy of the system particles in any external field or fields.
  The change in the system energy during a time interval is then found to be
  given by a certain sum of integrals which, in the transition to a
  macroscopic model, becomes the sum of heat and thermodynamic work in accord
  with the first law of thermodynamics.

  A similar derivation, using a slightly different notation, is given in Ref.
  <cite|devoe-07>.

  <section|Forces between Particles><label|app-forces
  between-part><label|A-forces between particles>

  A <em|material particle> is a body that has mass and is so small that it
  behaves as a point, without rotational energy or internal structure. We
  assume each particle has a constant mass, ignoring relativistic effects
  that are important only when the particle moves at a speed close to the
  speed of light.

  Consider a collection of an arbitrary number of material particles that
  have interactions only among themselves and with no other particles. Later
  we will consider some of the particles to constitute a thermodynamic
  <em|system> and the others to be the <em|surroundings>.

  Newton's laws of motion are obeyed only in an <index|Inertial reference
  frame><subsubindex|Frame|reference|inertial><em|inertial> reference frame.
  A reference frame that is fixed or moving at a constant velocity relative
  to local stars is practically an inertial reference frame. To a good
  approximation, a reference frame fixed relative to the earth's surface is
  also an inertial system (the necessary corrections are discussed in Sec.
  <reference|A-earth-fixed frame>). This reference frame will be called
  simply the <index|Lab frame><subindex|Frame|lab><em|lab frame>, and treated
  as an inertial frame in order that we may apply Newton's laws.

  It will be assumed that the Cartesian components of all vector quantities
  appearing in Sections <reference|A-forces between
  particles>\U<reference|A-macr work> are measured in an inertial lab frame.

  Classical mechanics is based on the idea that one material particle acts on
  another by means of a <em|force> that is independent of the reference
  frame. Let the vector <math|\<b-F\><rsub|i*j>> denote the force exerted on
  particle <math|i> by particle <math|j>.<footnote|This and the next two
  footnotes are included for readers who are not familiar with vector
  notation. The quantity <math|\<b-F\><rsub|i*j>> is printed in boldface to
  indicate it is a <em|vector> having both magnitude and direction.> The
  <em|net> force <math|\<b-F\><rsub|i>> acting on particle <math|i> is the
  vector sum of the individual forces exerted on it by the other
  particles:<footnote|The rule for adding vectors, as in the summation shown
  here, is that the sum is a vector whose component along each axis of a
  Cartesian coordinate system is the sum of the components along that axis of
  the vectors being added. For example, the vector
  <math|\<b-C\>=\<b-A\>+\<b-B\>> has components
  <math|C<rsub|x>=A<rsub|x>+B<rsub|x>>, <math|C<rsub|y>=A<rsub|y>+B<rsub|y>>,
  and <math|C<rsub|z>=A<rsub|z>+B<rsub|z>>.>

  <\equation>
    <label|Fi=sum(Fij)>\<b-F\><rsub|i>=<big|sum><rsub|j\<ne\>i>\<b-F\><rsub|i*j>
  </equation>

  (The term in which <math|j> equals <math|i> has to be omitted because a
  particle does not act on itself.) According to <index|Newton's second law
  of motion>Newton's second law of motion, the net force
  <math|\<b-F\><rsub|i>> acting on particle <math|i> is equal to the product
  of its mass <math|m<rsub|i>> and its acceleration:

  <\equation>
    <label|Fi=mdv(i)/dt>\<b-F\><rsub|i>=m<rsub|i>*<frac|<dif>\<b-v\><rsub|i>|<dt>>
  </equation>

  Here <math|\<b-v\><rsub|i>> is the particle's velocity in the <index|Lab
  frame><subindex|Frame|lab>lab frame and <math|t> is time.

  A nonzero net force causes particle <math|i> to accelerate and its velocity
  and position to change. The <index-complex|<tuple|work>||app-forces
  between-part idx1|<tuple|Work>><em|work> done by the net force acting on
  the particle in a given time interval is defined by the
  integral<footnote|The dot between the vectors in the integrand indicates a
  scalar product or dot product, which is a <em|non>vector quantity. The
  general definition of the scalar product of two vectors, <math|\<b-A\>> and
  <math|\<b-B\>>, is <math|\<b-A\><dotprod>\<b-B\>=A*B*cos \<alpha\>> where
  <math|A> and <math|B> are the magnitudes of the two vectors and
  <math|\<alpha\>> is the angle between their positive directions.>

  \;

  <\equation>
    <label|Wi=int F(i)dr(i)>W<rsub|i>=<big|int><space|-0.17em>\<b-F\><rsub|i><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  where <math|\<b-r\><rsub|i>> is the position vector of the particle\Va
  vector from the origin of the <index|Lab frame><subindex|Frame|lab>lab
  frame to the position of the particle.

  <\quote-env>
    The integral on the right side of Eq. <reference|Wi=int F(i)dr(i)> is an
    example of a <em|line integral>. It indicates that the scalar product of
    the net force acting on the particle and the particle's displacement is
    to be integrated over time during the time interval. The integral can be
    written without vectors in the form <math|<big|int><space|-0.17em>F<rsub|i>*cos
    \<alpha\>*<around|(|<dif>s/<dt>|)><dt>> where <math|F<rsub|i>> is the
    magnitude of the net force, <math|<dif>s/<dt>> is the magnitude of the
    velocity of the particle along its path in three-dimensional space, and
    <math|\<alpha\>> is the angle between the force and velocity vectors. The
    three quantities <math|F<rsub|i>>, <math|cos \<alpha\>>, and
    <math|<dif>s/<dt>> are all functions of time, <math|t>, and the
    integration is carried out with time as the integration variable.
  </quote-env>

  By substituting the expression for <math|\<b-F\><rsub|i>> (Eq.
  <reference|Fi=mdv(i)/dt>) in Eq. <reference|Wi=int F(i)dr(i)>, we obtain

  <\eqnarray*>
    <tformat|<table|<row|<cell|W<rsub|i>>|<cell|=>|<cell|m<rsub|i>*<big|int><space|-0.17em><frac|<dif>\<b-v\><rsub|i>|<dt>><dotprod><dif>\<b-r\><rsub|i>=m<rsub|i>*<big|int><space|-0.17em><frac|<dif>\<b-r\><rsub|i>|<dt>><dotprod><dif>\<b-v\><rsub|i>=m<rsub|i>*<big|int><space|-0.17em>\<b-v\><rsub|i><dotprod><dif>\<b-v\><rsub|i>=m<rsub|i>*<big|int><space|-0.17em>v<rsub|i>*<dif>v<rsub|i>>>|<row|<cell|>|<cell|=>|<cell|<Del><space|-0.17em><around*|(|<tfrac|1|2>*m<rsub|i>*v<rsub|i><rsup|2>|)><eq-number><label|intF(i)dr(i)=Del(KE)>>>>>
  </eqnarray*>

  where <math|v<rsub|i>> is the magnitude of the velocity.

  The quantity <math|<frac|1|2>*m<rsub|i>*v<rsub|i><rsup|2>> is called the
  <index-complex|<tuple|energy>||app-forces between-part
  idx2|<tuple|Energy>><subindex|Energy|kinetic><index|Kinetic
  energy><em|kinetic energy> of particle <math|i>. This kinetic energy
  depends only on the magnitude of the velocity (i.e., on the speed) and not
  on the particle's position.

  The <em|total> work <math|W<rsub|<text|tot>>> done by all forces acting on
  all particles during the time interval is the sum of <math|W<rsub|i>> for
  all particles: <math|W<rsub|<text|tot>>=<big|sum><rsub|i><space|-0.17em>W<rsub|i>>.<footnote|The
  work <math|W<subs|tot>> defined here is not the same as the thermodynamic
  work appearing in the first law of thermodynamics.> Equation
  <reference|intF(i)dr(i)=Del(KE)> then gives us

  <\equation>
    <label|W(tot)=Del(KE)>W<rsub|<text|tot>>=<big|sum><rsub|i><Del><space|-0.17em><around*|(|<tfrac|1|2>*m<rsub|i>*v<rsub|i><rsup|2>|)>=<Del><space|-0.17em><around*|(|<big|sum><rsub|i><tfrac|1|2>*m<rsub|i>*v<rsub|i><rsup|2>|)>
  </equation>

  Equation <reference|W(tot)=Del(KE)> shows that the total work during a time
  interval is equal to the change in the total kinetic energy in this
  interval. This result is called the \Pwork-energy principle\Q by
  physicists.<footnote|Ref. <cite|sears-70>, p. 95.>

  From Eqs. <reference|Fi=sum(Fij)> and <reference|Wi=int F(i)dr(i)> we
  obtain a second expression for <math|W<rsub|<text|tot>>>:

  <\equation>
    <label|W(tot)=sum(i)sum(j)int>W<rsub|<text|tot>>=<big|sum><rsub|i>W<rsub|i>=<big|sum><rsub|i><big|int><space|-0.17em><big|sum><rsub|j\<ne\>i>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>=<big|sum><rsub|i><big|sum><rsub|j\<ne\>i><big|int><space|-0.17em><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  The double sum in the right-most expression can be written as a sum over
  pairs of particles, the term for the pair <math|i> and <math|j> being

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>+<big|int><space|-0.17em>\<b-F\><rsub|j*i><dotprod><dif>\<b-r\><rsub|j>>|<cell|=>|<cell|<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>-<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|j>>>|<row|<cell|>|<cell|=>|<cell|<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif><around|(|\<b-r\><rsub|i>-\<b-r\><rsub|j>|)>=<big|int><space|-0.17em><around|(|\<b-F\><rsub|i*j><dotprod>\<b-e\><rsub|i*j>|)><dif>r<rsub|i*j><eq-number><label|F(ij)dr(i)+F(ji)dr(j)>>>>>
  </eqnarray*>

  Here we have used the relations <math|\<b-F\><rsub|j*i>=-\<b-F\><rsub|i*j>>
  (from <index|Newton's third law of action and reaction>Newton's third law)
  and <math|<around|(|\<b-r\><rsub|i>-\<b-r\><rsub|j>|)>=\<b-e\><rsub|i*j>*r<rsub|i*j>>,
  where <math|\<b-e\><rsub|i*j>> is a unit vector pointing from <math|j> to
  <math|i> and <math|r<rsub|i*j>> is the distance between the particles.
  Equation <reference|W(tot)=sum(i)sum(j)int> becomes

  <\equation>
    <label|W(tot)=sum(i)sum(j\<gtr\>i)>W<rsub|<text|tot>>=<big|sum><rsub|i><big|sum><rsub|j\<ne\>i><big|int><space|-0.17em><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>=<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><big|int><space|-0.17em><around|(|\<b-F\><rsub|i*j><dotprod>\<b-e\><rsub|i*j>|)><dif>r<rsub|i*j>
  </equation>

  Next we look in detail at the force that particle <math|j> exerts on
  particle <math|i>. This force depends on the nature of the two particles
  and on the distance between them. For instance, <index|Newton's law of
  universal gravitation>Newton's law of universal gravitation gives the
  magnitude of a <subindex|Force|gravitational><subindex|Gravitational|force><em|gravitational>
  force as <math|G*m<rsub|i>*m<rsub|j>/r<rsub|i*j><rsup|2>>, where <math|G>
  is the gravitational constant. <index|Coulomb's law>Coulomb's law gives the
  magnitude of an <subindex|Force|electrical><subindex|Electrical|force><em|electrical>
  force between stationary charged particles as
  <math|Q<rsub|i>*Q<rsub|j>/<around|(|4*\<pi\>*\<epsilon\><rsub|0>*r<rsub|i*j><rsup|2>|)>>,
  where <math|Q<rsub|i>> and <math|Q<rsub|j>> are the charges and
  <math|\<epsilon\><rsub|0>> is the electric constant (or permittivity of
  vacuum). These two kinds of forces are central forces that obey
  <index|Newton's third law of action and reaction>Newton's third law of
  action and reaction, namely, that the forces exerted by two particles on
  one another are equal in magnitude and opposite in direction and are
  directed along the line joining the two particles. (In contrast, the
  <em|electromagnetic> force between charged particles in relative motion
  does <em|not> obey Newton's third law.)

  We will assume the force <math|\<b-F\><rsub|i*j>> exerted on particle
  <math|i> by particle <math|j> has a magnitude that depends only on the
  interparticle distance <math|r<rsub|i*j>> and is directed along the line
  between <math|i> and <math|j>, as is true of gravitational and
  electrostatic forces and on intermolecular forces in general. Then we can
  define a <subindex|Potential|function><em|potential function>,
  <math|<varPhi><rsub|i*j>>, for this force that will be a contribution to
  the potential energy. To see how <math|<varPhi><rsub|i*j>> is related to
  <math|\<b-F\><rsub|i*j>>, we look at Eq. <reference|F(ij)dr(i)+F(ji)dr(j)>.
  The left-most expression, <math|<big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>+<big|int><space|-0.17em>\<b-F\><rsub|j*i><dotprod><dif>\<b-r\><rsub|j>>,
  is the change in the kinetic energies of particles <math|i> and <math|j>
  during a time interval (see Eq. <reference|intF(i)dr(i)=Del(KE)>). If these
  were the only particles, their total energy should be constant for
  conservation of energy; thus <math|<Del><varPhi><rsub|i*j>> should have the
  same magnitude and the opposite sign of the kinetic energy change:

  <\equation>
    <label|del phi><Del><varPhi><rsub|i*j>=-<big|int><space|-0.17em><around|(|\<b-F\><rsub|i*j><dotprod>\<b-e\><rsub|i*j>|)><dif>r<rsub|i*j>
  </equation>

  The value of <math|<varPhi><rsub|i*j>> at any interparticle distance
  <math|r<rsub|i*j>> is fully defined by Eq. <reference|del phi> and the
  choice of an arbitrary zero. The quantity
  <math|<around|(|\<b-F\><rsub|i*j><dotprod>\<b-e\><rsub|i*j>|)>> is simply
  the component of the force along the line between the particles, and is
  negative for an attractive force (one in which <math|\<b-F\><rsub|i*j>>
  points from <math|i> to <math|j>) and positive for a repulsive force. If
  the force is attractive, the value of <math|<varPhi><rsub|i*j>> increases
  with increasing <math|r<rsub|i*j>>; if the force is repulsive,
  <math|<varPhi><rsub|i*j>> decreases with increasing <math|r<rsub|i*j>>.
  Since <math|<varPhi><rsub|i*j>> is a function only of <math|r<rsub|i*j>>,
  it is independent of the choice of reference frame.

  Equations <reference|W(tot)=sum(i)sum(j\<gtr\>i)> and <reference|del phi>
  can be combined to give

  <\equation>
    <label|W(tot)=-Del(PE)>W<subs|tot>=-<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><Del><varPhi><rsub|i*j>=-<Del><space|-0.17em><around*|(|<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>|)>
  </equation>

  By equating the expressions for <math|W<subs|tot>> given by Eqs.
  <reference|W(tot)=Del(KE)> and <reference|W(tot)=-Del(PE)> and rearranging,
  we obtain

  <\equation>
    <label|E(tot)2=E(tot)1><Del><space|-0.17em><around*|(|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>|)>+<Del><space|-0.17em><around*|(|<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>|)>=0
  </equation>

  This equation shows that the quantity

  <\equation>
    <label|E(tot)=KE+PE>E<subs|tot>=<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>
  </equation>

  is constant over time as the particles move in response to the forces
  acting on them. The first term on the right side of Eq.
  <reference|E(tot)=KE+PE> is the total kinetic energy of the particles. The
  second term is the pairwise sum of particle\Uparticle potential functions;
  this term is called the <subindex|Energy|potential><subindex|Potential|energy><em|potential
  energy> of the particles. Note that the kinetic energy depends only on
  particle speeds and the potential energy depends only on particle
  positions.

  The significance of Eq. <reference|E(tot)2=E(tot)1> is that the total
  energy <math|E<subs|tot>> defined by Eq. <reference|E(tot)=KE+PE> is
  <em|conserved>. This will be true provided the <index|Inertial reference
  frame><subsubindex|Frame|reference|inertial>reference frame used for
  kinetic energy is inertial and the only forces acting on the particles are
  those responsible for the particle\Uparticle potential functions.

  <section|The System and Surroundings><label|app-forces sys-surround>

  Now we are ready to assign the particles to two groups: particles in the
  system and those in the surroundings. This section will use the following
  convention: indices <math|i> and <math|j> refer to particles in the
  <em|system>; indices <math|k> and <math|l> refer to particles in the
  <em|surroundings>. This division of particles is illustrated schematically
  in Fig. <reference|fig:A-forces>(a).<\float|float|thb>
    <\framed>
      <\big-figure|<image|../src/BACK-SUP/forces.eps|230pt|105pt||>>
        <label|fig:A-forces>Assignment of particles to groups, and some
        representative particle--particle potential functions (schematic).
        The closed dashed curve represents the system boundary.

        <\enumerate-alpha>
          <item>The open circles represent particles in the system, and the
          filled circles are particles in the surroundings.

          <item>The filled triangles are particles in the surroundings that
          are the source of a conservative force field for particles in the
          system.
        </enumerate-alpha>
      </big-figure>
    </framed>
  </float> With this change in notation, Eq. <reference|E(tot)=KE+PE> becomes

  <\equation>
    <label|E(tot)=sums>E<subs|tot>=<big|sum><rsub|i><tfrac|1|2>*m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><big|sum><rsub|k><varPhi><rsub|i*k>+<big|sum><rsub|k><tfrac|1|2>*m<rsub|k>*v<rsub|k><rsup|2>+<big|sum><rsub|k><big|sum><rsub|l\<gtr\>k><varPhi><rsub|k*l>
  </equation>

  A portion of the surroundings may create a time-independent conservative
  force field (an \Pexternal\Q field) for a particle in the system. In order
  for such a field to be present, its contribution to the force exerted on
  the particle and to the particle's potential energy must depend only on the
  particle's position in the <index|Lab frame><subindex|Frame|lab>lab frame.
  The usual gravitational and electrostatic fields are of this type.

  In order to clarify the properties of a conservative external field, the
  index <math|k<rprime|'>> will be used for those particles in the
  surroundings that are not the source of an external field, and
  <math|k<rprime|''>> for those that are, as indicated in Fig.
  <reference|fig:A-forces>(b). Then the force exerted on system particle
  <math|i> due to the field is <math|\<b-F\><rsub|i><sups|field>=<big|sum><rsub|k<rprime|''>>\<b-F\><rsub|i*k<rprime|''>>>.
  If this were the only force acting on particle <math|i>, the change in its
  kinetic energy during a time interval would be
  <math|<big|int><space|-0.17em>\<b-F\><rsub|i><sups|field><dotprod><dif>\<b-r\><rsub|i>>
  (Eq. <reference|intF(i)dr(i)=Del(KE)>). For conservation of energy, the
  potential energy change in the time interval should have the same magnitude
  and the opposite sign:

  <\equation>
    <label|del phi_i(field)><Del><varPhi><rsub|i><sups|field>=-<big|int><space|-0.17em>\<b-F\><rsub|i><sups|field><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  Only if the integral <math|<big|int><space|-0.17em>\<b-F\><rsub|i><sups|field><dotprod><dif>\<b-r\><rsub|i>>
  has the same value for all paths between the initial and final positions of
  the particle does a conservative force field exist; otherwise the concept
  of a potential energy <math|<varPhi><rsub|i><sups|field>> is not valid.

  Taking a gravitational field as an example of a conservative external
  field, we replace <math|\<b-F\><rsub|i><sups|field>> and
  <math|<varPhi><rsub|i><sups|field>> by <math|\<b-F\><rsub|i><sups|grav>>
  and <math|<varPhi><rsub|i><sups|grav>>:
  <math|<Del><varPhi><rsub|i><sups|grav>=-<big|int><space|-0.17em>\<b-F\><rsub|i><sups|grav><dotprod><dif>\<b-r\><rsub|i>>.
  The gravitational force on particle <math|i> is, from Newton's second law,
  the product of the particle mass and its acceleration
  <math|-g*\<b-e\><rsub|z>> in the gravitational field:
  <math|\<b-F\><rsub|i><sups|grav>=-m<rsub|i>*g*\<b-e\><rsub|z>> where
  <math|g> is the acceleration of free fall and <math|\<b-e\><rsub|z>> is a
  unit vector in the vertical (upward) <math|z> direction. The change in the
  gravitational potential energy given by Eq. <reference|del phi_i(field)> is

  <\equation>
    <label|delPhi_i(grav)=m_i g del z_i><Del><varPhi><rsub|i><sups|grav>=m<rsub|i>*g*<big|int><space|-0.17em>\<b-e\><rsub|z><dotprod><dif>\<b-r\><rsub|i>=m<rsub|i>*g<space|0.17em><Del>z<rsub|i>
  </equation>

  (The range of elevations of the system particles is assumed to be small
  compared with the earth's radius, so that each system particle experiences
  essentially the same constant value of <math|g>.) Thus we can define the
  gravitational potential energy of particle <math|i>, which is a function
  only of the particle's vertical position in the <index|Lab
  frame><subindex|Frame|lab>lab frame, by
  <math|<varPhi><rsub|i><sups|grav>=m<rsub|i>*g*z<rsub|i>+C<rsub|i>> where
  <math|C<rsub|i>> is an arbitrary constant.

  Returning to Eq. <reference|E(tot)=sums> for the total energy, we can now
  write the third term on the right side in the form

  <\equation>
    <label|sum_i sum_k phi(ik)><big|sum><rsub|i><big|sum><rsub|k><varPhi><rsub|i*k>=<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>+<big|sum><rsub|i><varPhi><rsub|i><sups|field>
  </equation>

  To divide the expression for the total energy into meaningful parts, we
  substitute Eq. <reference|sum_i sum_k phi(ik)> in Eq.
  <reference|E(tot)=sums> and rearrange in the form

  <\eqnarray*>
    <tformat|<table|<row|<cell|E<subs|tot>>|<cell|=>|<cell|<around*|[|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><varPhi><rsub|i><sups|field>|]>>>|<row|<cell|>|<cell|=>|<cell|+<around*|[|<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>|]>+<around*|[|<big|sum><rsub|k><onehalf>m<rsub|k>*v<rsub|k><rsup|2>+<big|sum><rsub|k><big|sum><rsub|l\<gtr\>k><varPhi><rsub|k*l>|]><eq-number><label|E(tot)(grouped)>>>>>
  </eqnarray*>

  The terms on the right side of this equation are shown grouped with
  brackets into three quantities. The first quantity depends only on the
  speeds and positions of the particles in the <em|system>, and thus
  represents the energy of the system:

  <\equation>
    <label|E(sys)=>E<sys>=<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><varPhi><rsub|i><sups|field>
  </equation>

  The three terms in this expression for <math|E<sys>> are, respectively, the
  kinetic energy of the system particles relative to the <index|Lab
  frame><subindex|Frame|lab>lab frame, the potential energy of interaction
  among the system particles, and the total potential energy of the system in
  the external field.

  The last bracketed quantity on the right side of Eq.
  <reference|E(tot)(grouped)> depends only on the speeds and positions of all
  the particles in the <em|surroundings>, so that this quantity is the energy
  of the surroundings, <math|E<subs|surr>>. Thus, an abbreviated form of Eq.
  <reference|E(tot)(grouped)> is

  <\equation>
    <label|E(tot)=E(sys)+...>E<subs|tot>=E<sys>+<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>+E<subs|surr>
  </equation>

  The quantity <math|<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>>
  represents potential energy shared by both the system and surroundings on
  account of forces acting across the system boundary, other than
  gravitational forces or forces from other external fields. The forces
  responsible for the quantity <math|<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>>
  are generally significant only between particles in the immediate vicinity
  of the system boundary, and will presently turn out to be the crucial
  forces for evaluating thermodynamic work.

  <section|System Energy Change><label|app-forces sys-echange>

  This section derives an important relation between the change
  <math|<Del>E<sys>> of the energy of the system measured in a <index|Lab
  frame><subindex|Frame|lab>lab frame, and the forces exerted by the
  surroundings on the system particles. The indices <math|i> and <math|j>
  will refer to only the particles in the <em|system>.

  We write the net force on particle <math|i> in the form

  <\equation>
    <label|F_i=sum...>\<b-F\><rsub|i>=<big|sum><rsub|j\<ne\>i>\<b-F\><rsub|i*j>+\<b-F\><rsub|i><sups|field>+\<b-F\><sur><rsub|i>
  </equation>

  where <math|\<b-F\><rsub|i*j>> is the force exerted on particle <math|i> by
  particle <math|j>, both particles being in the system, and
  <math|\<b-F\><sur><rsub|i>=<big|sum><rsub|k<rprime|'>>\<b-F\><rsub|i*k<rprime|'>>>
  is the net force exerted on particle <math|i> by the particles in the
  surroundings that are not the source of an external field. During a given
  period of time, the work done by forces acting on only the system particles
  is

  <\equation>
    <label|sum intF(i)dr(i)><big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><rsub|i><dotprod><dif>\<b-r\><rsub|i>=<big|sum><rsub|i><big|sum><rsub|j\<ne\>i><big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>+<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><rsub|i><sups|field><dotprod><dif>\<b-r\><rsub|i>+<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  We can replace the first three sums in this equation with new expressions.
  Using Eq. <reference|intF(i)dr(i)=Del(KE)>, we have

  <\equation>
    <label|sum=del(KE)><big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><rsub|i><dotprod><dif>\<b-r\><rsub|i>=<Del><around*|(|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>|)>
  </equation>

  From Eqs. <reference|W(tot)=sum(i)sum(j\<gtr\>i)> and <reference|del phi>
  we obtain

  <\equation>
    <label|sum=del(PE)><big|sum><rsub|i><big|sum><rsub|j\<ne\>i><big|int><space|-0.17em>\<b-F\><rsub|i*j><dotprod><dif>\<b-r\><rsub|i>=-<Del><around*|(|<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>|)>
  </equation>

  where the sums are over the system particles. From Eq. <reference|del
  phi_i(field)> we can write

  <\equation>
    <label|del phi(field)><big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><rsub|i><sups|field><dotprod><dif>\<b-r\><rsub|i>=-<Del><around*|(|<big|sum><rsub|i><varPhi><rsub|i><sups|field>|)>
  </equation>

  Combining Eqs. <reference|sum intF(i)dr(i)>\U<reference|del phi(field)> and
  rearranging, we obtain

  <\equation>
    <big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i>=<Del><around*|(|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><varPhi><rsub|i><sups|field>|)>
  </equation>

  Comparison of the expression on the right side of this equation with Eq.
  <reference|E(sys)=> shows that the expression is the same as the change of
  <math|E<sys>>:

  <\equation>
    <label|DelE(sys)=><Del>E<sys>=<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i>
  </equation>

  Recall that the vector <math|\<b-F\><sur><rsub|i>> is the force exerted on
  particle <math|i>, in the system, by the particles in the surroundings
  other than those responsible for an external field. Thus <math|<Del>E<sys>>
  is equal to the total work done on the system by the surroundings, other
  than work done by an external field such as a gravitational field.

  <\quote-env>
    \ It might seem strange that work done by an external field is not
    included in <math|<Del>E<sys>>. The reason it is not included is that
    <math|<varPhi><rsub|i><sups|field>> was defined to be a potential energy
    belonging only to the system, and is thus irrelevant to energy transfer
    from or to the surroundings.

    As a simple example of how this works, consider a system consisting of a
    solid body in a gravitational field. If the only force exerted on the
    body is the downward gravitational force, then the body is in free fall
    but <math|<Del>E<sys>> in the <index|Lab frame><subindex|Frame|lab>lab
    frame is zero; the loss of gravitational potential energy as the body
    falls is equal to the gain of kinetic energy. On the other hand, work
    done on the system by an external force that <em|opposes> the
    gravitational force is included in <math|<Del>E<sys>>. For example, if
    the body is pulled upwards at a constant speed with a string, its
    potential energy increases while its kinetic energy remains constant, and
    <math|E<sys>> increases.
  </quote-env>

  <section|Macroscopic Work><label|app-forces macro-work><label|A-macr work>

  In thermodynamics we are interested in the quantity of work done on
  <em|macroscopic> parts of the system during a process, rather than the work
  done on individual particles. Macroscopic work is the energy transferred
  across the system boundary due to concerted motion of many particles on
  which the surroundings exert a force. Macroscopic <em|mechanical> work
  occurs when there is displacement of a macroscopic portion of the system on
  which a short-range <em|contact force> <subindex|Contact|force><subindex|Force|contact>acts
  across the system boundary. This force could be, for instance, the pressure
  of an external fluid at a surface element of the boundary multiplied by the
  area of the surface element, or it could be the tension in a cord at the
  point where the cord passes through the boundary.

  The symbol <math|w<lab>> will refer to macroscopic work measured with
  displacements in the <index|Lab frame><subindex|Frame|lab>lab frame.

  At any given instant, only the system particles that are close to the
  boundary will have nonnegligible contact forces exerted on them. We can
  define an <em|interaction layer>, a thin shell-like layer within the system
  and next to the system boundary that contains all the system particles with
  appreciable contact forces. We imagine the interaction layer to be divided
  into volume elements, or segments, each of which either moves as a whole
  during the process or else is stationary. Let <math|\<b-R\><rsub|\<tau\>>>
  be a position vector from the origin of the lab frame to a point fixed in
  the boundary at segment <math|\<tau\>>, and let
  <math|\<b-r\><rsub|i*\<tau\>>> be a vector from this point to particle
  <math|i> (Fig. <reference|fig:A-boundary>).<\float|float|thb>
    <\framed>
      <\big-figure|<image|../src/BACK-SUP/boundary.eps|106pt|114pt||Position
      vectors within the system. Segment <math|\<tau\>> of the interaction
      layer lies within the heavy curve (representing the system boundary)
      and the dashed lines. Open circle: origin of lab frame; open square:
      point fixed in system boundary at segment <math|\<tau\>>; filled
      circle: particle <math|i>.<label|fig:A-boundary>>>
        <label|fig:A-boundary>Position vectors within the system. Segment
        <math|\<tau\>> of the interaction layer lies within the heavy curve
        (representing the system boundary) and the dashed lines. Open circle:
        origin of lab frame; open square: point fixed in system boundary at
        segment <math|\<tau\>>; filled circle: particle <math|i>.
      </big-figure>
    </framed>
  </float> Then the position vector for particle <math|i> can be written
  <math|\<b-r\><rsub|i>=\<b-R\><rsub|\<tau\>>+\<b-r\><rsub|i*\<tau\>>>. Let
  <math|\<b-F\><sur><rsub|\<tau\>>> be the total contact force exerted by the
  surroundings on the system particles in segment <math|\<tau\>>:
  <math|\<b-F\><sur><rsub|\<tau\>>=<big|sum><rsub|i>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i>>,
  where <math|\<up-delta\><rsub|i*\<tau\>>> is equal to <math|1> when
  particle <math|i> is in segment <math|\<tau\>> and is zero otherwise.

  The change in the system energy during a process is, from Eq.
  <reference|DelE(sys)=>,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<Del>E<sys>>|<cell|=>|<cell|<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i>=<big|sum><rsub|\<tau\>><big|sum><rsub|i><big|int><space|-0.17em>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i><dotprod><dif><around*|(|\<b-R\><rsub|\<tau\>>+\<b-r\><rsub|i*\<tau\>>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rsub|\<tau\>>+<big|sum><rsub|\<tau\>><big|sum><rsub|i><big|int><space|-0.17em>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i*\<tau\>><eq-number><label|Del
    E(sys)=2 terms>>>>>
  </eqnarray*>

  We recognize the integral <math|<big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rsub|\<tau\>>>
  as the macroscopic work at surface element <math|\<tau\>>, because it is
  the integrated scalar product of the force exerted by the surroundings and
  the displacement. The total macroscopic work during the process is then
  given by

  <\equation>
    <label|w(lab)=sum int>w<lab>=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rsub|\<tau\>>
  </equation>

  <em|Heat>, <index|Heat><math|q<lab>>, can be defined as energy transfer to
  or from the system that is not accounted for by macroscopic work. This
  transfer occurs by means of chaotic motions and collisions of individual
  particles at the boundary. With this understanding, Eq. <reference|Del
  E(sys)=2 terms> becomes

  <\equation>
    <label|delE(sys)=q(lab)+w(lab)><Del>E<sys>=q<lab>+w<lab>
  </equation>

  with <math|w<lab>> given by the expression in Eq. <reference|w(lab)=sum
  int> and <math|q<lab>> given by

  <\equation>
    <label|q(lab)=sum int>q<lab>=<big|sum><rsub|\<tau\>><big|sum><rsub|i><big|int><space|-0.17em>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i*\<tau\>>
  </equation>

  <section|The Work Done on the System and Surroundings><label|app-forces
  work-done><label|A-work done>

  An additional comment can be made about the transfer of energy between the
  system and the surroundings. We may use Eq. <reference|w(lab)=sum int>,
  with appropriate redefinition of the quantities on the right side, to
  evaluate the work done on the <em|surroundings>. This work may be equal in
  magnitude and opposite in sign to the work <math|w<lab>> done on the
  system. A necessary condition for this equality is that the interacting
  parts of the system and surroundings have equal displacements; that is,
  that there be continuity of motion at the system boundary. We expect there
  to be continuity of motion when a fluid contacts a moving piston or paddle.

  Suppose, however, that the system is stationary and an interacting part of
  the surroundings moves. Then according to Eq. <reference|w(lab)=sum int>,
  <math|w<lab>> is zero, whereas the work done on or by that part of the
  surroundings is <em|not> zero. How can this be, considering that
  <math|E<subs|tot>> remains constant? One possibility, discussed by
  <index|Bridgman, Percy>Bridgman,<footnote|Ref. <cite|bridgman-41>, p.
  47--56.> is <subindex|Friction|sliding>sliding friction at the boundary:
  energy lost by the surroundings in the form of work is gained by the system
  and surroundings in the form of <subindex|Energy|thermal><subindex|Thermal|energy>thermal
  energy. Since the effect on the system is the same as a flow of heat from
  the surroundings, the division of energy transfer into heat and work can be
  ambiguous when there is <subindex|Friction|sliding>sliding friction at the
  boundary.<footnote|The ambiguity can be removed by redefining the system
  boundary so that a thin stationary layer next to the sliding interface, on
  the side that was originally part of the system, is considered to be
  included in the surroundings instead of the system. The layer removed from
  the system by this change can be so thin that the values of the system's
  extensive properties are essentially unaffected. With this redefined
  boundary, the energy transfer across the boundary is entirely by means of
  heat.>

  Another way work can have different magnitudes for system and surroundings
  is a change in potential energy shared by the system and surroundings. This
  shared energy is associated with forces acting across the boundary, other
  than from a time-independent external field, and is represented in Eq.
  <reference|E(tot)=E(sys)+...> by the sum
  <math|<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>>.
  In the usual types of processes this sum is either practically constant, or
  else each term falls off so rapidly with distance that the sum is
  negligible. Since <math|E<subs|tot>> is constant, during such processes the
  quantity <math|E<sys>+E<subs|surr>> remains essentially constant.\ 

  <index-complex|<tuple|force>||app:forces
  idx1|<tuple|Force>><index-complex|<tuple|work>||app-forces between-part
  idx1|<tuple|Work>><index-complex|<tuple|energy>||app-forces between-part
  idx2|<tuple|Energy>>

  <section|The Local Frame and Internal Energy><label|app-forces
  local-frame><label|A-local frame>

  As explained in Sec. <reference|2-internal energy>, a lab frame may not be
  an appropriate reference frame in which to measure changes in the system's
  energy. This is the case when the system as a whole moves or rotates in the
  lab frame, so that <math|E<sys>> depends in part on external coordinates
  that are not state functions. In this case it may be possible to define a
  <index|Local frame><subindex|Frame|local><em|local frame> moving with the
  system in which the energy of the system is a state function, the internal
  energy <math|U>.

  As before, <math|\<b-r\><rsub|i>> is the position vector of particle
  <math|i> in a <index|Lab frame><subindex|Frame|lab>lab frame. A prime
  notation will be used for quantities measured in the local frame. Thus the
  position of particle <math|i> relative to the local frame is given by
  vector <math|\<b-r\><rprime|'><rsub|i>>, which points from the origin of
  the local frame to particle <math|i> (see Fig.
  <reference|fig:A-localframe>).<\float|float|thb>
    <\framed>
      <\big-figure|<image|../src/BACK-SUP/localframe.eps|132pt|114pt||>>
        <label|fig:A-localframe>Vectors in the lab and local reference
        frames. Open circle: origin of lab frame; open triangle: origin of
        local frame; open square: point fixed in system boundary at segment
        <math|\<tau\>>; filled circle: particle <math|i>. The thin lines are
        the Cartesian axes of the reference frames.
      </big-figure>
    </framed>
  </float> The velocity of the particle in the local frame is
  <math|\<b-v\><rprime|'><rsub|i>=<dif>\<b-r\><rprime|'><rsub|i>/<dt>>.

  We continue to treat the earth-fixed lab frame as an inertial frame,
  although this is not strictly true (Sec. <reference|A-earth-fixed frame>).
  If the origin of the local frame moves at constant velocity in the lab
  frame, with Cartesian axes that do not rotate with respect to those of the
  lab frame, then the local frame is also inertial but <math|U> is not equal
  to <math|E<sys>> and the change <math|<Del>U> during a process is not
  necessarily equal to <math|<Del>E<sys>>.

  If the origin of the local frame moves with nonconstant velocity in the lab
  frame, or if the <subindex|Local frame|rotating><subsubindex|Frame|local|rotating>local
  frame rotates with respect to the <index|Lab frame><subindex|Frame|lab>lab
  frame, then the local frame has finite acceleration and is noninertial. In
  this case the motion of particle <math|i> in the local frame does not obey
  Newton's second law as it does in an inertial frame. We can, however,
  define an <subindex|Force|effective><em|effective> net force
  <math|\<b-F\><sups|eff><rsub|i>> whose relation to the particle's
  acceleration in the local frame has the same form as Newton's second law:

  <\equation>
    <label|F_i(eff)=>\<b-F\><sups|eff><rsub|i>=m<rsub|i>*<frac|<dif>\<b-v\><rprime|'><rsub|i>|<dt>>
  </equation>

  To an observer who is stationary in the local frame, the effective force
  will appear to make the particle's motion obey Newton's second law even
  though the frame is not inertial.

  The net force on particle <math|i> from interactions with other particles
  is given by Eq. <reference|F_i=sum...>:
  <math|\<b-F\><rsub|i>=<big|sum><rsub|j\<ne\>i>\<b-F\><rsub|i*j>+\<b-F\><rsub|i><sups|field>+\<b-F\><sur><rsub|i>>.
  The effective force can be written

  <\equation>
    <label|F_i(eff)=F_i+F_i(accel)>\<b-F\><sups|eff><rsub|i>=\<b-F\><rsub|i>+\<b-F\><rsub|i><sups|accel>
  </equation>

  where <math|\<b-F\><rsub|i><sups|accel>> is the contribution due to
  acceleration. <math|\<b-F\><rsub|i><sups|accel>> is not a true force in the
  sense of resulting from the interaction of particle <math|i> with other
  particles. Instead, it is an <subindex|Force|apparent>apparent or
  <subindex|Force|fictitious>fictitious force introduced to make it possible
  to write Eq. <reference|F_i(eff)=> which resembles Newton's second law. The
  motion of particle <math|i> in an inertial frame is given by
  <math|m<rsub|i><dif>\<b-v\><rsub|i>/<dt>=\<b-F\><rsub|i>>, whereas the
  motion in the local frame is given by <math|m<rsub|i><dif>\<b-v\><rprime|'><rsub|i>/<dt>=\<b-F\><rsub|i>+\<b-F\><rsub|i><sups|accel>>.

  A simple example may make these statements clear. Consider a small
  unattached object suspended in the \Pweightless\Q environment of an
  orbiting space station. Assume the object is neither moving nor spinning
  relative to the station. Let the object be the system, and fix the local
  frame in the space station. The local frame rotates with respect to local
  stars as the station orbits around the earth; the local frame is therefore
  noninertial. The only true force exerted on the object is a gravitational
  force directed toward the earth. This force explains the object's
  acceleration relative to local stars. The fact that the object has no
  acceleration in the local frame can be explained by the presence of a
  fictitious centrifugal force having the same magnitude as the gravitational
  force but directed in the opposite direction, so that the <em|effective>
  force on the object as a whole is zero.

  The reasoning used to derive the equations in Secs. <reference|A-forces
  between particles>\U<reference|A-macr work> can be applied to an arbitrary
  <index|Local frame><subindex|Frame|local>local frame. To carry out the
  derivations we replace <math|\<b-F\><rsub|i>> by
  <math|\<b-F\><sups|eff><rsub|i>>, <math|\<b-r\><rsub|i>> by
  <math|\<b-r\><rprime|'><rsub|i>>, and <math|\<b-v\><rsub|i>> by
  <math|\<b-v\><rprime|'><rsub|i>>, and use the local frame to measure the
  Cartesian components of all vectors. We need two new potential energy
  functions for the local frame, defined by the relations

  <\equation>
    <label|del phi'(field)_1=><Del><varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i><defn>-<big|int><space|-0.17em>\<b-F\><sups|field><rsub|i><dotprod><dif>\<b-r\>*'<rsub|i>
  </equation>

  <\equation>
    <label|del phi(accel)_i=><Del><varPhi><sups|accel><rsub|i><defn>-<big|int><space|-0.17em>\<b-F\><sups|accel><rsub|i><dotprod><dif>\<b-r\><rprime|'><rsub|i>
  </equation>

  Both <math|<varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i>> and
  <math|<varPhi><sups|accel>> must be time-independent functions of the
  position of particle <math|i> in the local frame in order to be valid
  potential functions. (If the local frame is inertial,
  <math|\<b-F\><sups|accel><rsub|i>> and <math|<varPhi><sups|accel><rsub|i>>
  are zero.)

  The detailed line of reasoning in Secs. <reference|A-forces between
  particles>\U<reference|A-macr work> will not be repeated here, but the
  reader can verify the following results. The <em|total energy> of the
  system and surroundings measured in the <em|local> frame is given by
  <math|E<rprime|'><subs|tot>=U+<big|sum><rsub|i><big|sum><rsub|k<rprime|'>><varPhi><rsub|i*k<rprime|'>>+E<rprime|'><subs|surr>>
  where the index <math|k<rprime|'>> is for particles in the surroundings
  that are not the source of an external field for the system. The energy of
  the <em|system> (the internal energy) is given by

  <\equation>
    <label|U=4 terms>U=<big|sum><rsub|i><onehalf>m<rsub|i><around|(|v<rprime|'><rsub|i>|)><rsup|2>+<big|sum><rsub|i><big|sum><rsub|j\<gtr\>i><varPhi><rsub|i*j>+<big|sum><rsub|i><varPhi><rsub|i><sups|<math|<rprime|'>><space|0.17em>field>+<big|sum><rsub|i><varPhi><rsub|i><sups|accel>
  </equation>

  where the indices <math|i> and <math|j> are for system particles. The
  energy of the <em|surroundings> measured in the <index|Local
  frame><subindex|Frame|local>local frame is

  <\equation>
    E<rprime|'><subs|surr>=<big|sum><rsub|k><onehalf>m<rsub|k><around|(|v<rprime|'><rsub|k>|)><rsup|2>+<big|sum><rsub|k><big|sum><rsub|l\<gtr\>k><varPhi><rsub|k*l>+<big|sum><rsub|k><varPhi><rsub|k><sups|accel>
  </equation>

  where <math|k> and <math|l> are indices for particles in the surroundings.
  The value of <math|E<rprime|'><subs|tot>> is found to be constant over
  time, meaning that energy is conserved in the local frame. The internal
  energy change during a process is the sum of the heat <math|q> measured in
  the local frame and the macroscopic work <math|w> in this frame:

  <\equation>
    <label|Del U=q+w><Del>U=q+w
  </equation>

  The expressions for <math|q> and <math|w>, analogous to Eqs.
  <reference|q(lab)=sum int> and <reference|w(lab)=sum int>, are found to be

  <\equation>
    <label|q=sum int>q=<big|sum><rsub|\<tau\>><big|sum><rsub|i><big|int><space|-0.17em>\<up-delta\><rsub|i*\<tau\>>*\<b-F\><sur><rsub|i><dotprod><dif>\<b-r\><rsub|i*\<tau\>>
  </equation>

  <\equation>
    <label|w=sum int>w=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rprime|'><rsub|\<tau\>>
  </equation>

  In these equations <math|\<b-R\><rprime|'><rsub|\<tau\>>> is a vector from
  the origin of the local frame to a point fixed in the system boundary at
  segment <math|\<tau\>>, and <math|\<b-r\><rsub|i*\<tau\>>> is a vector from
  this point to particle <math|i> (see Fig. <reference|fig:A-localframe>).

  We expect that an observer in the <index|Local
  frame><subindex|Frame|local>local frame will find the laws of
  thermodynamics are obeyed.<label|thermo-noninertial frame>For instance, the
  Clausius statement of the second law (Sec. <reference|4-Clausius
  statement>) is as valid in a manned orbiting space laboratory as it is in
  an earth-fixed laboratory: nothing the investigator can do will allow
  energy to be transferred by heat from a colder to a warmer body through a
  device operating in a cycle. Equation <reference|Del U=q+w> is a statement
  of the first law of thermodynamics (box on page <pageref|first law>) in the
  local frame. Accordingly, we may assume that the thermodynamic derivations
  and relations treated in the body of this book are valid in any local
  frame, whether or not it is inertial, when <math|U> and <math|w> are
  defined by Eqs. <reference|U=4 terms> and <reference|w=sum int>.

  In the body of the book, <math|w> is called the <em|thermodynamic work>, or
  simply the work. Note the following features brought out by the derivation
  of the expression for <math|w>:

  <\itemize>
    <item>The equation <math|w=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rprime|'><rsub|\<tau\>>>
    has been derived for a <em|closed> system.

    <item>The equation shows how we can evaluate the thermodynamic work
    <math|w> done on the system. For each moving surface element of the
    system boundary at segment <math|\<tau\>> of the interaction layer, we
    need to know the contact force <math|\<b-F\><sur><rsub|\<tau\>>> exerted
    by the surroundings and the displacement
    <math|<dif>\<b-R\><rprime|'><rsub|\<tau\>>> in the <index|Local
    frame><subindex|Frame|local>local frame.

    <item>We could equally well calculate <math|w> from the force exerted by
    the <em|system> on the surroundings. According to <index|Newton's third
    law of action and reaction>Newton's third law, the force
    <math|\<b-F\><sups|sys><rsub|\<tau\>>> exerted by segment <math|\<tau\>>
    has the same magnitude as <math|\<b-F\><sur><rsub|\<tau\>>> and the
    opposite direction: <math|\<b-F\><sups|sys><rsub|\<tau\>>=-\<b-F\><sur><rsub|\<tau\>>>.

    <item>

    During a process, a point fixed in the system boundary at segment
    <math|\<tau\>> is either stationary or traverses a path in
    three-dimensional space. At each intermediate stage of the process, let
    <math|s<rsub|\<tau\>>> be the length of the path that began in the
    initial state. We can write the infinitesimal quantity
    <math|\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rprime|'><rsub|\<tau\>>>
    in the form <math|F<sur><rsub|\<tau\>>cos
    \<alpha\><rsub|\<tau\>><dif>s<rsub|\<tau\>>>, where
    <math|F<sur><rsub|\<tau\>>> is the magnitude of the force,
    <math|<dif>s<rsub|\<tau\>>> is an infinitesimal change of the path
    length, and <math|\<alpha\><rsub|\<tau\>>> is the angle between the
    directions of the force and the displacement. We then obtain the
    following integrated and differential forms of the work:

    <\equation>
      <label|w=sum int F cos(alpha)ds dw=>w=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>F<sur><rsub|\<tau\>>cos
      \<alpha\><rsub|\<tau\>><dif>s<rsub|\<tau\>><space|2em><dw>=<big|sum><rsub|\<tau\>>F<sur><rsub|\<tau\>>cos
      \<alpha\><rsub|\<tau\>><dif>s<rsub|\<tau\>>
    </equation>

    <item>

    If only one portion of the boundary moves in the <index|Local
    frame><subindex|Frame|local>local frame, and this portion has linear
    motion parallel to the <math|x<rprime|'>> axis, we can replace
    <math|\<b-F\><sur><rsub|\<tau\>><dotprod><dif>\<b-R\><rprime|'><rsub|\<tau\>>>
    by <math|F<sur><rsub|x<rprime|'>><dx><rprime|'>>, where
    <math|F<sur><rsub|x<rprime|'>>> is the <math|x<rprime|'>> component of
    the force exerted by the surroundings on the moving boundary and
    <math|<dx><rprime|'>> is an infinitesimal displacement of the boundary.
    In this case we can write the following integrated and differential forms
    of the work:

    <\equation>
      <label|w=int F(sur)dx' dw=>w=<big|int><space|-0.17em>F<sur><rsub|x<rprime|'>><dx><rprime|'><space|2em><dw>=F<sur><rsub|x<rprime|'>><dx><rprime|'>
    </equation>

    <item>The work <math|w> does not include work done internally by one part
    of the system on another part.

    <item>In the calculation of work with Eqs. <reference|w=sum
    int>\U<reference|w=int F(sur)dx' dw=>, we do not include forces from an
    external field such as a gravitational field, or
    <subindex|Force|fictitious>fictitious forces
    <math|\<b-F\><sups|accel><rsub|i>> if present.
  </itemize>

  <section|Nonrotating Local Frame><label|app-forces
  nonrotating-frame><label|A-nonrotating frame>

  <subindex|Local frame|nonrotating><subsubindex|Frame|local|nonrotating>Consider
  the case of a nonrotating local frame whose origin moves in the lab frame
  but whose Cartesian axes <math|x<rprime|'>>, <math|y<rprime|'>>,
  <math|z<rprime|'>> remain parallel to the axes <math|x>, <math|y>, <math|z>
  of the lab frame. In this case the Cartesian components of
  <math|\<b-F\><sur><rsub|i>> for particle <math|i> are the same in both
  frames, and so also are the Cartesian components of the infinitesimal
  vector displacement <math|<dif>\<b-r\><rsub|i*\<tau\>>>. According to Eqs.
  <reference|q(lab)=sum int> and <reference|q=sum int>, then, for an
  arbitrary process the value of the heat <math|q> in the local frame is the
  same as the value of the heat <math|q<lab>> in the <index|Lab
  frame><subindex|Frame|lab>lab frame.

  From Eqs. <reference|delE(sys)=q(lab)+w(lab)> and <reference|Del U=q+w>
  with <math|q<lab>> set equal to <math|q>, we obtain the useful relation

  <\equation>
    <label|delU-delE(sys)=w-w(lab)><Del>U-<Del>E<sys>=w-w<lab>
  </equation>

  This equation is not valid if the local frame has rotational motion with
  respect to the lab frame.

  The vector <math|\<b-R\><rprime|'><rsub|\<tau\>>> has the same Cartesian
  components in the lab frame as in the nonrotating local frame, so we can
  write <math|\<b-R\><rsub|\<tau\>>-\<b-R\><rprime|'><rsub|\<tau\>>=\<b-R\><subs|loc>>
  where <math|\<b-R\><subs|loc>> is the position vector in the lab frame of
  the origin of the local frame (see Fig. <reference|fig:A-localframe>). From
  Eqs. <reference|w(lab)=sum int> and <reference|w=sum int>, setting
  <math|<around|(|\<b-R\><rsub|\<tau\>>-\<b-R\><rprime|'><rsub|\<tau\>>|)>>
  equal to <math|\<b-R\><subs|loc>>, we obtain the relation

  <\equation>
    <label|w-w(lab)=int>w-w<lab>=<big|sum><rsub|\<tau\>><big|int><space|-0.17em>\<b-F\><sur><rsub|\<tau\>><dotprod><dif><around|(|\<b-R\><rprime|'><rsub|\<tau\>>-\<b-R\><rsub|\<tau\>>|)>=-<big|int><space|-0.17em><around*|(|<big|sum><rsub|\<tau\>>\<b-F\><sur><rsub|\<tau\>>|)><dotprod><dif>\<b-R\><subs|loc>
  </equation>

  The sum <math|<big|sum><rsub|\<tau\>>\<b-F\><sur><rsub|\<tau\>>> is the net
  contact force exerted on the system by the surroundings. For example,
  suppose the system is a fluid in a gravitational field. Let the system
  boundary be at the inner walls of the container, and let the local frame be
  fixed with respect to the container and have negligible acceleration in the
  lab frame. At each surface element of the boundary, the force exerted by
  the pressure of the fluid on the container wall is equal in magnitude and
  opposite in direction to the contact force exerted by the surroundings on
  the fluid. The horizontal components of the contact forces on opposite
  sides of the container cancel, but the vertical components do not cancel
  because of the hydrostatic pressure. The net contact force is
  <math|m*g*\<b-e\><rsub|z>>, where <math|m> is the system mass and
  <math|\<b-e\><rsub|z>> is a unit vector in the vertical <math|+z>
  direction. For this example, Eq. <reference|w-w(lab)=int> becomes

  <\equation>
    <label|w-w(lab)=-mg del z(loc)>w-w<lab>=-m*g<Del>z<subs|loc>
  </equation>

  where <math|z<subs|loc>> is the elevation in the lab frame of the origin of
  the local frame.

  <section|Center-of-mass Local Frame><label|app-forces com-frame>

  <index-complex|<tuple|center-of-mass frame>||app-forces com-frame
  idx1|<tuple|Center-of-mass frame>><index-complex|<tuple|frame|center-of-mass>||app-forces
  com-frame idx2|<tuple|Frame|center-of-mass>>If we use a <em|center-of-mass
  frame> (cm frame) for the local frame, the internal energy change during a
  process is related in a particularly simple way to the system energy change
  measured in a lab frame. A cm frame has its origin at the center of mass of
  the system and its Cartesian axes parallel to the Cartesian axes of a lab
  frame. This is a special case of the <subsubindex|Frame|local|nonrotating>nonrotating
  local frame discussed in Sec. <reference|A-nonrotating frame>. Since the
  center of mass may accelerate in the lab frame, a cm frame is not
  necessarily inertial.

  The indices <math|i> and <math|j> in this section refer only to the
  particles in the <em|system>.

  The <index|Center of mass><newterm|center of mass> of the system is a point
  whose position in the lab frame is defined by

  <\equation>
    <label|R=sum(m(i)r(i)/m>\<b-R\><cm><defn><frac|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rsub|i>|m>
  </equation>

  where <math|m> is the system mass: <math|m=<big|sum><rsub|i>m<rsub|i>>. The
  position vector of particle <math|i> in the <index|Lab
  frame><subindex|Frame|lab>lab frame is equal to the sum of the vector
  <math|\<b-R\><cm>> from the origin of the lab frame to the center of mass
  and the vector <math|\<b-r\><rprime|'><rsub|i>> from the center of mass to
  the particle (see Fig. <reference|fig:A-cmframe>):<\float|float|thb>
    <\framed>
      <\big-figure|<image|../src/BACK-SUP/cmframe.eps|143pt|114pt||>>
        <label|fig:A-cmframe>Position vectors in a lab frame and a
        center-of-mass frame. Open circle: origin of lab frame; open
        triangle: center of mass; filled circle: particle <math|i>. The thin
        lines represent the Cartesian axes of the two frames.
      </big-figure>
    </framed>
  </float>

  <\equation>
    <label|r(i)=R+r'(i)>\<b-r\><rsub|i>=\<b-R\><cm>+\<b-r\><rprime|'><rsub|i>
  </equation>

  We can use Eqs. <reference|R=sum(m(i)r(i)/m> and <reference|r(i)=R+r'(i)>
  to derive several relations that will be needed presently. Because the
  Cartesian axes of the lab frame and cm frame are parallel to one another
  (that is, the cm frame does not rotate), we can add vectors or form scalar
  products using the vector components measured in either frame. The time
  derivative of Eq. <reference|r(i)=R+r'(i)> is
  <math|<dif>\<b-r\><rsub|i>/<dt>=<dif>\<b-R\><cm>/<dt>+<dif>\<b-r\><rprime|'><rsub|i>/<dt>>,
  or

  <\equation>
    <label|v(i)=v+v'(i)>\<b-v\><rsub|i>=\<b-v\><cm>+\<b-v\><rprime|'><rsub|i>
  </equation>

  where the vector <math|\<b-v\><cm>> gives the velocity of the center of
  mass in the lab frame. Substitution from Eq. <reference|r(i)=R+r'(i)> into
  the sum <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rsub|i>> gives
  <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rsub|i>=m*\<b-R\><cm>+<big|sum><rsub|i>m<rsub|i>*\<b-r\><rprime|'><rsub|i>>,
  and a rearrangement of Eq. <reference|R=sum(m(i)r(i)/m> gives
  <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rsub|i>=m*\<b-R\><cm>>. Comparing
  these two equalities, we see the sum <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rprime|'><rsub|i>>
  must be zero. Therefore the first and second derivatives of
  <math|<big|sum><rsub|i>m<rsub|i>*\<b-r\><rprime|'><rsub|i>> with respect to
  time must also be zero:

  <\equation>
    <label|sum m(i)v'(i)=0><big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>=0*<space|2em><big|sum><rsub|i>m<rsub|i>*<frac|<dif>\<b-v\><rprime|'><rsub|i>|<dt>>=0
  </equation>

  From Eqs. <reference|Fi=mdv(i)/dt>, <reference|F_i(eff)=>,
  <reference|F_i(eff)=F_i+F_i(accel)>, and <reference|v(i)=v+v'(i)> we obtain

  <\equation>
    <label|F_i(accel)=>\<b-F\><sups|accel><rsub|i>=m<rsub|i>*<frac|<dif><around|(|\<b-v\><rprime|'><rsub|i>-\<b-v\><rsub|i>|)>|<dt>>=-m<rsub|i>*<frac|<dif>\<b-v\><cm>|<dt>>
  </equation>

  Equation <reference|F_i(accel)=> is valid only for a <subindex|Local
  frame|nonrotating><subsubindex|Frame|local|nonrotating>nonrotating cm
  frame.

  The difference between the energy changes of the system in the cm frame and
  the lab frame during a process is given, from Eqs. <reference|E(sys)=> and
  <reference|U=4 terms>, by

  <\equation>
    <label|delU-delE(sys)=>

    <\eqsplit>
      <tformat|<table|<row|<cell|<Del>U-<Del>E<sys>>|<cell|=<Del><around*|[|<big|sum><rsub|i><onehalf>m<rsub|i>*<around|(|v<rprime|'><rsub|i>|)><rsup|2>-<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>|]>>>|<row|<cell|>|<cell|<space|1em>+<Del><around*|(|<big|sum><rsub|i><varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i>-<big|sum><rsub|i><varPhi><sups|field><rsub|i>|)>+<Del><around*|(|<big|sum><rsub|i><varPhi><sups|accel><rsub|i>|)>>>>>
    </eqsplit>
  </equation>

  We will find new expressions for the three terms on the right side of this
  equation.

  The first term is the difference between the total kinetic energy changes
  measured in the cm frame and lab frame. We can derive an important
  relation, well known in classical mechanics, for the kinetic energy in the
  lab frame:

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>>|<cell|=<big|sum><rsub|i><onehalf>m<rsub|i>*<around|(|\<b-v\><cm>+\<b-v\><rprime|'><rsub|i>|)><dotprod><around|(|\<b-v\><cm>+\<b-v\><rprime|'><rsub|i>|)>>>|<row|<cell|>|<cell|=<onehalf>m*v<rsup|2><cm>+<big|sum><rsub|i><onehalf>m<rsub|i>*<around*|(|v<rprime|'><rsub|i>|)><rsup|2>+\<b-v\><cm><dotprod><around*|(|<big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>|)>>>>>
    </eqsplit>
  </equation>

  The quantity <math|<onehalf>m*v<rsup|2><cm>> is the bulk kinetic energy of
  the system in the lab frame\Vthat is, the translational energy of a body
  having the same mass as the system and moving with the center of mass. The
  sum <math|<big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>> is zero
  (Eq. <reference|sum m(i)v'(i)=0>). Therefore the first term on the right
  side of Eq. <reference|delU-delE(sys)=> is

  <\equation>
    <Del><around*|[|<big|sum><rsub|i><onehalf>m<rsub|i>*<around|(|v<rprime|'><rsub|i>|)><rsup|2>-<big|sum><rsub|i><onehalf>m<rsub|i>*v<rsub|i><rsup|2>|]>=-<Del><around*|(|<onehalf>m*v<rsup|2><cm>|)>
  </equation>

  Only by using a <subindex|Local frame|nonrotating><subsubindex|Frame|local|nonrotating>nonrotating
  local frame moving with the center of mass is it possible to derive such a
  simple relation among these kinetic energy quantities.

  The second term on the right side of Eq. <reference|delU-delE(sys)=>, with
  the help of Eqs. <reference|del phi_i(field)>, <reference|del
  phi'(field)_1=>, and <reference|r(i)=R+r'(i)> becomes

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<Del><around*|(|<big|sum><rsub|i><varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i>-<big|sum><rsub|i><varPhi><sups|field><rsub|i>|)>>|<cell|=-<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sups|field><rsub|i><dotprod><dif><around|(|\<b-r\>*'<rsub|i>-\<b-r\><rsub|i>|)>>>|<row|<cell|>|<cell|=<big|int><space|-0.17em><around*|(|<big|sum><rsub|i>\<b-F\><sups|field><rsub|i>|)><dotprod><dif>\<b-R\><cm>>>>>
    </eqsplit>
  </equation>

  Suppose the only external field is gravitational:
  <math|\<b-F\><sups|field><rsub|i>=\<b-F\><sups|grav><rsub|i>=-m<rsub|i>*g*\<b-e\><rsub|z>>
  where <math|\<b-e\><rsub|z>> is a unit vector in the vertical (upward)
  <math|+z> direction. In this case we obtain

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<Del><around*|(|<big|sum><rsub|i><varPhi><sups|<math|<rprime|'>><space|0.17em>field><rsub|i>-<big|sum><rsub|i><varPhi><sups|field><rsub|i>|)>>|<cell|=-<big|int><space|-0.17em><around*|(|<big|sum><rsub|i>m<rsub|i>|)>*g*\<b-e\><rsub|z><dotprod><dif>\<b-R\><cm>>>|<row|<cell|>|<cell|=-m*g*<big|int><space|-0.17em>\<b-e\><rsub|z><dotprod><dif>\<b-R\><cm>=-m*g*<big|int><space|-0.17em><dif>z<cm>>>|<row|<cell|>|<cell|=-m*g*<Del>z<cm>>>>>
    </eqsplit>
  </equation>

  where <math|z<cm>> is the elevation of the center of mass in the lab frame.
  The quantity <math|m*g<Del>z<cm>> is the change in the system's bulk
  gravitational potential energy in the lab frame\Vthe change in the
  potential energy of a body of mass <math|m> undergoing the same change in
  elevation as the system's center of mass.

  The third term on the right side of Eq. <reference|delU-delE(sys)=> can be
  shown to be zero when the local frame is a cm frame. The derivation uses
  Eqs. <reference|del phi(accel)_i=> and <reference|F_i(accel)=> and is as
  follows:

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<Del><around*|(|<big|sum><rsub|i><varPhi><sups|accel><rsub|i>|)>>|<cell|=-<big|sum><rsub|i><big|int><space|-0.17em>\<b-F\><sups|accel><rsub|i><dotprod><dif>\<b-r\><rprime|'><rsub|i>=<big|sum><rsub|i><big|int><space|-0.17em>m<rsub|i>*<frac|<dif>\<b-v\><cm>|<dt>><dotprod><dif>\<b-r\><rprime|'><rsub|i>>>|<row|<cell|>|<cell|=<big|int><space|-0.17em><around*|(|<big|sum><rsub|i>m<rsub|i>*<frac|<dif>\<b-r\><rprime|'><rsub|i>|<dt>>|)><dotprod><dif>\<b-v\><cm>=<big|int><space|-0.17em><around*|(|<big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>|)><dotprod><dif>\<b-v\><cm>>>>>
    </eqsplit>
  </equation>

  The sum <math|<big|sum><rsub|i>m<rsub|i>*\<b-v\><rprime|'><rsub|i>> in the
  integrand of the last integral on the right side is zero (Eq.
  <reference|sum m(i)v'(i)=0>) so the integral is also zero.

  With these substitutions, Eq. <reference|delU-delE(sys)=> becomes
  <math|<Del>U-<Del>E<sys>=-<onehalf>m*<Del><space|-0.17em><around*|(|v<rsup|2><cm>|)>-m*g*<Del>z<cm>>.
  Since <math|<Del>U-<Del>E<sys>> is equal to <math|w-w<lab>> when the local
  frame is <subindex|Local frame|nonrotating><subsubindex|Frame|local|nonrotating>nonrotating
  (Eq. <reference|delU-delE(sys)=w-w(lab)>), we have

  <\equation>
    <label|w-w(lab) (cm)>w-w<lab>=-<onehalf>m*<Del><space|-0.17em><around*|(|v<rsup|2><cm>|)>-m*g*<Del>z<cm>
  </equation>

  <index-complex|<tuple|center-of-mass frame>||app-forces com-frame
  idx1|<tuple|Center-of-mass frame>><index-complex|<tuple|frame|center-of-mass>||app-forces
  com-frame idx2|<tuple|Frame|center-of-mass>>

  <section|Rotating Local Frame><label|app-forces
  rotating-frame><label|A-rotating frame>

  <index-complex|<tuple|rotating local frame>||app-forces rotating-frame
  idx1|<tuple|Rotating local frame>><index-complex|<tuple|frame|local|rotating>||app-forces
  rotating-frame idx2|<tuple|Frame|local|rotating>>A rotating local frame is
  the most convenient to use in treating the thermodynamics of a system with
  rotational motion in a lab frame. A good example of such a system is a
  solution in a sample cell of a spinning ultracentrifuge (Sec.
  <reference|9-centrifuge>).

  We will make several simplifying assumptions. The rotating local frame has
  the same origin and the same <math|z> axis as the lab frame, as shown in
  Fig. <reference|fig:A-rotating frame>.<\float|float|thb>
    <\framed>
      <\big-figure|<image|../src/BACK-SUP/rotation.eps|151pt|132pt||Relation
      between the Cartesian axes <math|x>, <math|y>, <math|z> of a lab frame
      and the axes <math|x<rprime|'>>, <math|y<rprime|'>>, <math|z> of a
      rotating local frame. The filled circle represents particle
      <math|i>.<label|fig:A-rotating frame>>>
        <label|fig:A-rotating frame>Relation between the Cartesian axes
        <math|x>, <math|y>, <math|z> of a lab frame and the axes
        <math|x<rprime|'>>, <math|y<rprime|'>>, <math|z> of a rotating local
        frame. The filled circle represents particle <math|i>.
      </big-figure>
    </framed>
  </float> The <math|z> axis is vertical and is the axis of rotation for the
  local frame. The local frame rotates with constant angular velocity
  <math|\<omega\>=<dif>\<vartheta\>/<dt>>, where <math|\<vartheta\>> is the
  angle between the <math|x> axis of the lab frame and the <math|x<rprime|'>>
  axis of the local frame. There is a gravitational force in the <math|-z>
  direction; this force is responsible for the only external field, whose
  potential energy change in the local frame during a process is
  <math|<Del><varPhi><rsub|i><sups|<math|<rprime|'>><space|0.17em>grav>=m<rsub|i>*g*<Del>z<rsub|i>>
  (Eq. <reference|delPhi_i(grav)=m_i g del z_i>).

  The contribution to the effective force acting on particle <math|i> due to
  acceleration when <math|\<omega\>> is constant can be shown to be given
  by<footnote|The derivation, using a different notation, can be found in
  Ref. <cite|marion-95>, Chap. 10.>

  <\equation>
    \<b-F\><sups|accel><rsub|i>=\<b-F\><sups|centr><rsub|i>+\<b-F\><sups|Cor><rsub|i>
  </equation>

  where <math|\<b-F\><sups|centr><rsub|i>> is the so-called
  <subindex|Force|centrifugal><index|Centrifugal force><em|centrifugal force>
  and <math|\<b-F\><sups|Cor><rsub|i>> is called the
  <subindex|Force|Coriolis><index|Coriolis force><em|Coriolis force>.

  The centrifugal force acting on particle <math|i> is given by

  <\equation>
    \<b-F\><sups|centr><rsub|i>=m<rsub|i>*\<omega\><rsup|2>*r<rsub|i>*\<b-e\><rsub|i>
  </equation>

  Here <math|r<rsub|i>> is the radial distance of the particle from the axis
  of rotation, and <math|\<b-e\><rsub|i>> is a unit vector pointing from the
  particle in the direction away from the axis of rotation (see Fig.
  <reference|fig:A-rotating frame>). The direction of <math|\<b-e\><rsub|i>>
  in the local frame changes as the particle moves in this frame.

  The Coriolis force acting on particle <math|i> arises only when the
  particle is moving relative to the rotating frame. This force has magnitude
  <math|2*m<rsub|i>*\<omega\>*v<rprime|'><rsub|i>> and is directed
  perpendicular to both <math|\<b-v\><rprime|'><rsub|i>> and the axis of
  rotation.

  In a rotating local frame, the work during a process is not the same as
  that measured in a lab frame. The heats <math|q> and <math|q<lab>> are not
  equal to one another as they are when the local frame is nonrotating, nor
  can general expressions using macroscopic quantities be written for
  <math|<Del>U-<Del>E<sys>> and <math|w-w<lab>>.
  <index-complex|<tuple|rotating local frame>||app-forces rotating-frame
  idx1|<tuple|Rotating local frame>><index-complex|<tuple|frame|local|rotating>||app-forces
  rotating-frame idx2|<tuple|Frame|local|rotating>>

  <section|Earth-Fixed Reference Frame><label|app-forces
  earth-fixed-frame><label|A-earth-fixed frame>

  <index-complex|<tuple|reference frame|earth fixed>||app-forces
  earth-fixed-frame idx1|<tuple|Reference
  frame|earth-fixed>><index-complex|<tuple|Frame|reference|earth
  fixed>||app-forces earth-fixed-frame idx2|<tuple|Frame|reference|earth-fixed>>In
  the preceding sections of Appendix <reference|app:forces>, we assumed that
  a lab frame whose coordinate axes are fixed relative to the earth's surface
  is an inertial frame. This is not exactly true, because the earth spins
  about its axis and circles the sun. Small correction terms, a centrifugal
  force <subindex|Force|centrifugal><index|Centrifugal force>and a Coriolis
  force, <subindex|Force|Coriolis><index|Coriolis force>are needed to obtain
  the effective net force acting on particle <math|i> that allows Newton's
  second law to be obeyed exactly in the lab frame.<footnote|Ref.
  <cite|goldstein-50>, Sec. 4--9.>

  The earth's movement around the sun makes only a minor contribution to
  these correction terms. The Coriolis force, which occurs only if the
  particle is moving in the lab frame, is usually so small that it can be
  neglected.

  This leaves as the only significant correction the centrifugal force on the
  particle from the earth's spin about its axis. This force is directed
  perpendicular to the earth's axis and has magnitude
  <math|m<rsub|i>*\<omega\><rsup|2>*r<rsub|i>>, where <math|\<omega\>> is the
  earth's angular velocity, <math|m<rsub|i>> is the particle's mass, and
  <math|r<rsub|i>> is the radial distance of the particle from the earth's
  axis. The correction can be treated as a small modification of the
  gravitational force <subindex|Force|gravitational><subindex|Gravitational|force>acting
  on the particle that is at most, at the equator, only about 0.3% of the
  actual gravitational force. Not only is the correction small, but it is
  completely taken into account in the lab frame when we calculate the
  effective gravitational force from <math|\<b-F\><sups|grav><rsub|i>=-m<rsub|i>*g*\<b-e\><rsub|z>>,
  where <math|g> is the acceleration of free fall and <math|\<b-e\><rsub|z>>
  is a unit vector in the <math|+z> (upward) direction. The value of <math|g>
  is an experimental quantity that includes the effect of
  <math|\<b-F\><sups|centr><rsub|i>>, and thus depends on latitude as well as
  elevation above the earth's surface. Since
  <math|\<b-F\><sups|grav><rsub|i>> depends only on position, we can treat
  gravity as a conservative force field in the earth-fixed lab
  frame.<index-complex|<tuple|reference frame|earth fixed>||app-forces
  earth-fixed-frame idx1|<tuple|Reference
  frame|earth-fixed>><index-complex|<tuple|Frame|reference|earth
  fixed>||app-forces earth-fixed-frame idx2|<tuple|Frame|reference|earth-fixed>>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|A-earth-fixed frame|<tuple|A.10|?>>
    <associate|A-forces between particles|<tuple|A.1|?>>
    <associate|A-local frame|<tuple|A.6|?>>
    <associate|A-macr work|<tuple|A.4|?>>
    <associate|A-nonrotating frame|<tuple|A.7|?>>
    <associate|A-rotating frame|<tuple|A.9|?>>
    <associate|A-work done|<tuple|A.5|?>>
    <associate|Del E(sys)=2 terms|<tuple|A.4.1|?>>
    <associate|Del U=q+w|<tuple|A.6.7|?>>
    <associate|DelE(sys)=|<tuple|A.3.7|?>>
    <associate|E(sys)=|<tuple|A.2.6|?>>
    <associate|E(tot)(grouped)|<tuple|A.2.5|?>>
    <associate|E(tot)2=E(tot)1|<tuple|A.1.11|?>>
    <associate|E(tot)=E(sys)+...|<tuple|A.2.7|?>>
    <associate|E(tot)=KE+PE|<tuple|A.1.12|?>>
    <associate|E(tot)=sums|<tuple|A.2.1|?>>
    <associate|F(ij)dr(i)+F(ji)dr(j)|<tuple|A.1.7|?>>
    <associate|F_i(accel)=|<tuple|A.8.5|?>>
    <associate|F_i(eff)=|<tuple|A.6.1|?>>
    <associate|F_i(eff)=F_i+F_i(accel)|<tuple|A.6.2|?>>
    <associate|F_i=sum...|<tuple|A.3.1|?>>
    <associate|Fi=mdv(i)/dt|<tuple|A.1.2|?>>
    <associate|Fi=sum(Fij)|<tuple|A.1.1|?>>
    <associate|R=sum(m(i)r(i)/m|<tuple|A.8.1|?>>
    <associate|U=4 terms|<tuple|A.6.5|?>>
    <associate|W(tot)=-Del(PE)|<tuple|A.1.10|?>>
    <associate|W(tot)=Del(KE)|<tuple|A.1.5|?>>
    <associate|W(tot)=sum(i)sum(j)int|<tuple|A.1.6|?>>
    <associate|W(tot)=sum(i)sum(j\<gtr\>i)|<tuple|A.1.8|?>>
    <associate|Wi=int F(i)dr(i)|<tuple|A.1.3|?>>
    <associate|app-forces between-part|<tuple|A.1|?>>
    <associate|app-forces com-frame|<tuple|A.8|?>>
    <associate|app-forces earth-fixed-frame|<tuple|A.10|?>>
    <associate|app-forces local-frame|<tuple|A.6|?>>
    <associate|app-forces macro-work|<tuple|A.4|?>>
    <associate|app-forces nonrotating-frame|<tuple|A.7|?>>
    <associate|app-forces rotating-frame|<tuple|A.9|?>>
    <associate|app-forces sys-echange|<tuple|A.3|?>>
    <associate|app-forces sys-surround|<tuple|A.2|?>>
    <associate|app-forces work-done|<tuple|A.5|?>>
    <associate|app:forces|<tuple|A|?>>
    <associate|auto-1|<tuple|A|?>>
    <associate|auto-10|<tuple|Newton's second law of motion|?>>
    <associate|auto-100|<tuple|Local frame|?>>
    <associate|auto-101|<tuple|Frame|?>>
    <associate|auto-102|<tuple|Local frame|?>>
    <associate|auto-103|<tuple|Frame|?>>
    <associate|auto-104|<tuple|Local frame|?>>
    <associate|auto-105|<tuple|Frame|?>>
    <associate|auto-106|<tuple|<tuple|center-of-mass frame>|?>>
    <associate|auto-107|<tuple|<tuple|frame|center-of-mass>|?>>
    <associate|auto-108|<tuple|A.9|?>>
    <associate|auto-109|<tuple|<tuple|rotating local frame>|?>>
    <associate|auto-11|<tuple|Lab frame|?>>
    <associate|auto-110|<tuple|<tuple|frame|local|rotating>|?>>
    <associate|auto-111|<tuple|A.9.1|?>>
    <associate|auto-112|<tuple|Force|?>>
    <associate|auto-113|<tuple|Centrifugal force|?>>
    <associate|auto-114|<tuple|Force|?>>
    <associate|auto-115|<tuple|Coriolis force|?>>
    <associate|auto-116|<tuple|<tuple|rotating local frame>|?>>
    <associate|auto-117|<tuple|<tuple|frame|local|rotating>|?>>
    <associate|auto-118|<tuple|A.10|?>>
    <associate|auto-119|<tuple|<tuple|reference frame|earth fixed>|?>>
    <associate|auto-12|<tuple|Frame|?>>
    <associate|auto-120|<tuple|<tuple|Frame|reference|earth fixed>|?>>
    <associate|auto-121|<tuple|Force|?>>
    <associate|auto-122|<tuple|Centrifugal force|?>>
    <associate|auto-123|<tuple|Force|?>>
    <associate|auto-124|<tuple|Coriolis force|?>>
    <associate|auto-125|<tuple|Force|?>>
    <associate|auto-126|<tuple|Gravitational|?>>
    <associate|auto-127|<tuple|<tuple|reference frame|earth fixed>|?>>
    <associate|auto-128|<tuple|<tuple|Frame|reference|earth fixed>|?>>
    <associate|auto-13|<tuple|<tuple|work>|?>>
    <associate|auto-14|<tuple|Lab frame|?>>
    <associate|auto-15|<tuple|Frame|?>>
    <associate|auto-16|<tuple|<tuple|energy>|?>>
    <associate|auto-17|<tuple|Energy|?>>
    <associate|auto-18|<tuple|Kinetic energy|?>>
    <associate|auto-19|<tuple|Newton's third law of action and reaction|?>>
    <associate|auto-2|<tuple|<tuple|force>|?>>
    <associate|auto-20|<tuple|Newton's law of universal gravitation|?>>
    <associate|auto-21|<tuple|Force|?>>
    <associate|auto-22|<tuple|Gravitational|?>>
    <associate|auto-23|<tuple|Coulomb's law|?>>
    <associate|auto-24|<tuple|Force|?>>
    <associate|auto-25|<tuple|Electrical|?>>
    <associate|auto-26|<tuple|Newton's third law of action and reaction|?>>
    <associate|auto-27|<tuple|Potential|?>>
    <associate|auto-28|<tuple|Energy|?>>
    <associate|auto-29|<tuple|Potential|?>>
    <associate|auto-3|<tuple|Reference frame|?>>
    <associate|auto-30|<tuple|Inertial reference frame|?>>
    <associate|auto-31|<tuple|Frame|?>>
    <associate|auto-32|<tuple|A.2|?>>
    <associate|auto-33|<tuple|A.2.1|?>>
    <associate|auto-34|<tuple|Lab frame|?>>
    <associate|auto-35|<tuple|Frame|?>>
    <associate|auto-36|<tuple|Lab frame|?>>
    <associate|auto-37|<tuple|Frame|?>>
    <associate|auto-38|<tuple|Lab frame|?>>
    <associate|auto-39|<tuple|Frame|?>>
    <associate|auto-4|<tuple|Frame|?>>
    <associate|auto-40|<tuple|A.3|?>>
    <associate|auto-41|<tuple|Lab frame|?>>
    <associate|auto-42|<tuple|Frame|?>>
    <associate|auto-43|<tuple|Lab frame|?>>
    <associate|auto-44|<tuple|Frame|?>>
    <associate|auto-45|<tuple|A.4|?>>
    <associate|auto-46|<tuple|Contact|?>>
    <associate|auto-47|<tuple|Force|?>>
    <associate|auto-48|<tuple|Lab frame|?>>
    <associate|auto-49|<tuple|Frame|?>>
    <associate|auto-5|<tuple|A.1|?>>
    <associate|auto-50|<tuple|A.4.1|?>>
    <associate|auto-51|<tuple|Heat|?>>
    <associate|auto-52|<tuple|A.5|?>>
    <associate|auto-53|<tuple|Bridgman, Percy|?>>
    <associate|auto-54|<tuple|Friction|?>>
    <associate|auto-55|<tuple|Energy|?>>
    <associate|auto-56|<tuple|Thermal|?>>
    <associate|auto-57|<tuple|Friction|?>>
    <associate|auto-58|<tuple|<tuple|force>|?>>
    <associate|auto-59|<tuple|<tuple|work>|?>>
    <associate|auto-6|<tuple|Inertial reference frame|?>>
    <associate|auto-60|<tuple|<tuple|energy>|?>>
    <associate|auto-61|<tuple|A.6|?>>
    <associate|auto-62|<tuple|Local frame|?>>
    <associate|auto-63|<tuple|Frame|?>>
    <associate|auto-64|<tuple|Lab frame|?>>
    <associate|auto-65|<tuple|Frame|?>>
    <associate|auto-66|<tuple|A.6.1|?>>
    <associate|auto-67|<tuple|Local frame|?>>
    <associate|auto-68|<tuple|Frame|?>>
    <associate|auto-69|<tuple|Lab frame|?>>
    <associate|auto-7|<tuple|Frame|?>>
    <associate|auto-70|<tuple|Frame|?>>
    <associate|auto-71|<tuple|Force|?>>
    <associate|auto-72|<tuple|Force|?>>
    <associate|auto-73|<tuple|Force|?>>
    <associate|auto-74|<tuple|Local frame|?>>
    <associate|auto-75|<tuple|Frame|?>>
    <associate|auto-76|<tuple|Local frame|?>>
    <associate|auto-77|<tuple|Frame|?>>
    <associate|auto-78|<tuple|Local frame|?>>
    <associate|auto-79|<tuple|Frame|?>>
    <associate|auto-8|<tuple|Lab frame|?>>
    <associate|auto-80|<tuple|Local frame|?>>
    <associate|auto-81|<tuple|Frame|?>>
    <associate|auto-82|<tuple|Newton's third law of action and reaction|?>>
    <associate|auto-83|<tuple|Local frame|?>>
    <associate|auto-84|<tuple|Frame|?>>
    <associate|auto-85|<tuple|Force|?>>
    <associate|auto-86|<tuple|A.7|?>>
    <associate|auto-87|<tuple|Local frame|?>>
    <associate|auto-88|<tuple|Frame|?>>
    <associate|auto-89|<tuple|Lab frame|?>>
    <associate|auto-9|<tuple|Frame|?>>
    <associate|auto-90|<tuple|Frame|?>>
    <associate|auto-91|<tuple|A.8|?>>
    <associate|auto-92|<tuple|<tuple|center-of-mass frame>|?>>
    <associate|auto-93|<tuple|<tuple|frame|center-of-mass>|?>>
    <associate|auto-94|<tuple|Frame|?>>
    <associate|auto-95|<tuple|Center of mass|?>>
    <associate|auto-96|<tuple|center of mass|?>>
    <associate|auto-97|<tuple|Lab frame|?>>
    <associate|auto-98|<tuple|Frame|?>>
    <associate|auto-99|<tuple|A.8.1|?>>
    <associate|del phi|<tuple|A.1.9|?>>
    <associate|del phi'(field)_1=|<tuple|A.6.3|?>>
    <associate|del phi(accel)_i=|<tuple|A.6.4|?>>
    <associate|del phi(field)|<tuple|A.3.5|?>>
    <associate|del phi_i(field)|<tuple|A.2.2|?>>
    <associate|delE(sys)=q(lab)+w(lab)|<tuple|A.4.3|?>>
    <associate|delPhi_i(grav)=m_i g del z_i|<tuple|A.2.3|?>>
    <associate|delU-delE(sys)=|<tuple|A.8.6|?>>
    <associate|delU-delE(sys)=w-w(lab)|<tuple|A.7.1|?>>
    <associate|fig:A-boundary|<tuple|A.4.1|?>>
    <associate|fig:A-cmframe|<tuple|A.8.1|?>>
    <associate|fig:A-forces|<tuple|A.2.1|?>>
    <associate|fig:A-localframe|<tuple|A.6.1|?>>
    <associate|fig:A-rotating frame|<tuple|A.9.1|?>>
    <associate|footnote-A.1.1|<tuple|A.1.1|?>>
    <associate|footnote-A.1.2|<tuple|A.1.2|?>>
    <associate|footnote-A.1.3|<tuple|A.1.3|?>>
    <associate|footnote-A.1.4|<tuple|A.1.4|?>>
    <associate|footnote-A.1.5|<tuple|A.1.5|?>>
    <associate|footnote-A.10.1|<tuple|A.10.1|?>>
    <associate|footnote-A.5.1|<tuple|A.5.1|?>>
    <associate|footnote-A.5.2|<tuple|A.5.2|?>>
    <associate|footnote-A.9.1|<tuple|A.9.1|?>>
    <associate|footnr-A.1.1|<tuple|A.1.1|?>>
    <associate|footnr-A.1.2|<tuple|A.1.2|?>>
    <associate|footnr-A.1.3|<tuple|A.1.3|?>>
    <associate|footnr-A.1.4|<tuple|A.1.4|?>>
    <associate|footnr-A.1.5|<tuple|A.1.5|?>>
    <associate|footnr-A.10.1|<tuple|A.10.1|?>>
    <associate|footnr-A.5.1|<tuple|A.5.1|?>>
    <associate|footnr-A.5.2|<tuple|A.5.2|?>>
    <associate|footnr-A.9.1|<tuple|A.9.1|?>>
    <associate|intF(i)dr(i)=Del(KE)|<tuple|A.1.4|?>>
    <associate|q(lab)=sum int|<tuple|A.4.4|?>>
    <associate|q=sum int|<tuple|A.6.8|?>>
    <associate|r(i)=R+r'(i)|<tuple|A.8.2|?>>
    <associate|sum intF(i)dr(i)|<tuple|A.3.2|?>>
    <associate|sum m(i)v'(i)=0|<tuple|A.8.4|?>>
    <associate|sum=del(KE)|<tuple|A.3.3|?>>
    <associate|sum=del(PE)|<tuple|A.3.4|?>>
    <associate|sum_i sum_k phi(ik)|<tuple|A.2.4|?>>
    <associate|thermo-noninertial frame|<tuple|Frame|?>>
    <associate|v(i)=v+v'(i)|<tuple|A.8.3|?>>
    <associate|w(lab)=sum int|<tuple|A.4.2|?>>
    <associate|w-w(lab) (cm)|<tuple|A.8.12|?>>
    <associate|w-w(lab)=-mg del z(loc)|<tuple|A.7.3|?>>
    <associate|w-w(lab)=int|<tuple|A.7.2|?>>
    <associate|w=int F(sur)dx' dw=|<tuple|A.6.11|?>>
    <associate|w=sum int|<tuple|A.6.9|?>>
    <associate|w=sum int F cos(alpha)ds dw=|<tuple|A.6.10|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      devoe-07

      sears-70

      bridgman-41

      marion-95

      goldstein-50
    </associate>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|A.2.1>|>
        Assignment of particles to groups, and some representative
        particle--particle potential functions (schematic). The closed dashed
        curve represents the system boundary.

        <\with|current-item|<quote|<macro|name|<aligned-item|<arg|name><with|font-shape|right|)><item-spc>>>>|transform-item|<quote|<macro|name|<number|<arg|name>|alpha>>>|item-nr|<quote|0>>
          <\surround|<vspace*|0.5fn><no-indent>|<specific|texmacs|<htab|0fn|first>><vspace|0.5fn>>
            <\with|par-left|<quote|<tmlen|26912|53823.9|80736>>>
              <\surround|<no-page-break*>|<no-indent*>>
                <assign|item-nr|1><hidden-binding|<tuple>|a><assign|last-item-nr|1><assign|last-item|a><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|a<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>The
                open circles represent particles in the system, and the
                filled circles are particles in the surroundings.

                <assign|item-nr|2><hidden-binding|<tuple>|b><assign|last-item-nr|2><assign|last-item|b><vspace*|0.5fn><with|par-first|<quote|<tmlen|-26912|-53823.9|-80736>>|<yes-indent>><resize|b<with|font-shape|<quote|right>|)>|<minus|1r|<minus|<item-hsep>|0.5fn>>||<plus|1r|0.5fn>|>The
                filled triangles are particles in the surroundings that are
                the source of a conservative force field for particles in the
                system.
              </surround>
            </with>
          </surround>
        </with>
      </surround>|<pageref|auto-33>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|A.4.1>|>
        Position vectors within the system. Segment
        <with|mode|<quote|math>|\<tau\>> of the interaction layer lies within
        the heavy curve (representing the system boundary) and the dashed
        lines. Open circle: origin of lab frame; open square: point fixed in
        system boundary at segment <with|mode|<quote|math>|\<tau\>>; filled
        circle: particle <with|mode|<quote|math>|i>.
      </surround>|<pageref|auto-50>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|A.6.1>|>
        Vectors in the lab and local reference frames. Open circle: origin of
        lab frame; open triangle: origin of local frame; open square: point
        fixed in system boundary at segment <with|mode|<quote|math>|\<tau\>>;
        filled circle: particle <with|mode|<quote|math>|i>. The thin lines
        are the Cartesian axes of the reference frames.
      </surround>|<pageref|auto-66>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|A.8.1>|>
        Position vectors in a lab frame and a center-of-mass frame. Open
        circle: origin of lab frame; open triangle: center of mass; filled
        circle: particle <with|mode|<quote|math>|i>. The thin lines represent
        the Cartesian axes of the two frames.
      </surround>|<pageref|auto-99>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|A.9.1>|>
        Relation between the Cartesian axes <with|mode|<quote|math>|x>,
        <with|mode|<quote|math>|y>, <with|mode|<quote|math>|z> of a lab frame
        and the axes <with|mode|<quote|math>|x<rprime|'>>,
        <with|mode|<quote|math>|y<rprime|'>>, <with|mode|<quote|math>|z> of a
        rotating local frame. The filled circle represents particle
        <with|mode|<quote|math>|i>.
      </surround>|<pageref|auto-111>>
    </associate>
    <\associate|gly>
      <tuple|normal|center of mass|<pageref|auto-96>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|force>||app:forces idx1|<tuple|Force>|<pageref|auto-2>>

      <tuple|<tuple|Reference frame|inertial>|<pageref|auto-3>>

      <tuple|<tuple|Frame|reference|inertial>|<pageref|auto-4>>

      <tuple|<tuple|Inertial reference frame>|<pageref|auto-6>>

      <tuple|<tuple|Frame|reference|inertial>|<pageref|auto-7>>

      <tuple|<tuple|Lab frame>|<pageref|auto-8>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-9>>

      <tuple|<tuple|Newton's second law of motion>|<pageref|auto-10>>

      <tuple|<tuple|Lab frame>|<pageref|auto-11>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-12>>

      <tuple|<tuple|work>||app-forces between-part
      idx1|<tuple|Work>|<pageref|auto-13>>

      <tuple|<tuple|Lab frame>|<pageref|auto-14>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-15>>

      <tuple|<tuple|energy>||app-forces between-part
      idx2|<tuple|Energy>|<pageref|auto-16>>

      <tuple|<tuple|Energy|kinetic>|<pageref|auto-17>>

      <tuple|<tuple|Kinetic energy>|<pageref|auto-18>>

      <tuple|<tuple|Newton's third law of action and
      reaction>|<pageref|auto-19>>

      <tuple|<tuple|Newton's law of universal gravitation>|<pageref|auto-20>>

      <tuple|<tuple|Force|gravitational>|<pageref|auto-21>>

      <tuple|<tuple|Gravitational|force>|<pageref|auto-22>>

      <tuple|<tuple|Coulomb's law>|<pageref|auto-23>>

      <tuple|<tuple|Force|electrical>|<pageref|auto-24>>

      <tuple|<tuple|Electrical|force>|<pageref|auto-25>>

      <tuple|<tuple|Newton's third law of action and
      reaction>|<pageref|auto-26>>

      <tuple|<tuple|Potential|function>|<pageref|auto-27>>

      <tuple|<tuple|Energy|potential>|<pageref|auto-28>>

      <tuple|<tuple|Potential|energy>|<pageref|auto-29>>

      <tuple|<tuple|Inertial reference frame>|<pageref|auto-30>>

      <tuple|<tuple|Frame|reference|inertial>|<pageref|auto-31>>

      <tuple|<tuple|Lab frame>|<pageref|auto-34>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-35>>

      <tuple|<tuple|Lab frame>|<pageref|auto-36>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-37>>

      <tuple|<tuple|Lab frame>|<pageref|auto-38>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-39>>

      <tuple|<tuple|Lab frame>|<pageref|auto-41>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-42>>

      <tuple|<tuple|Lab frame>|<pageref|auto-43>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-44>>

      <tuple|<tuple|Contact|force>|<pageref|auto-46>>

      <tuple|<tuple|Force|contact>|<pageref|auto-47>>

      <tuple|<tuple|Lab frame>|<pageref|auto-48>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-49>>

      <tuple|<tuple|Heat>|<pageref|auto-51>>

      <tuple|<tuple|Bridgman, Percy>|<pageref|auto-53>>

      <tuple|<tuple|Friction|sliding>|<pageref|auto-54>>

      <tuple|<tuple|Energy|thermal>|<pageref|auto-55>>

      <tuple|<tuple|Thermal|energy>|<pageref|auto-56>>

      <tuple|<tuple|Friction|sliding>|<pageref|auto-57>>

      <tuple|<tuple|force>||app:forces idx1|<tuple|Force>|<pageref|auto-58>>

      <tuple|<tuple|work>||app-forces between-part
      idx1|<tuple|Work>|<pageref|auto-59>>

      <tuple|<tuple|energy>||app-forces between-part
      idx2|<tuple|Energy>|<pageref|auto-60>>

      <tuple|<tuple|Local frame>|<pageref|auto-62>>

      <tuple|<tuple|Frame|local>|<pageref|auto-63>>

      <tuple|<tuple|Lab frame>|<pageref|auto-64>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-65>>

      <tuple|<tuple|Local frame|rotating>|<pageref|auto-67>>

      <tuple|<tuple|Frame|local|rotating>|<pageref|auto-68>>

      <tuple|<tuple|Lab frame>|<pageref|auto-69>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-70>>

      <tuple|<tuple|Force|effective>|<pageref|auto-71>>

      <tuple|<tuple|Force|apparent>|<pageref|auto-72>>

      <tuple|<tuple|Force|fictitious>|<pageref|auto-73>>

      <tuple|<tuple|Local frame>|<pageref|auto-74>>

      <tuple|<tuple|Frame|local>|<pageref|auto-75>>

      <tuple|<tuple|Local frame>|<pageref|auto-76>>

      <tuple|<tuple|Frame|local>|<pageref|auto-77>>

      <tuple|<tuple|Local frame>|<pageref|auto-78>>

      <tuple|<tuple|Frame|local>|<pageref|auto-79>>

      <tuple|<tuple|Local frame>|<pageref|auto-80>>

      <tuple|<tuple|Frame|local>|<pageref|auto-81>>

      <tuple|<tuple|Newton's third law of action and
      reaction>|<pageref|auto-82>>

      <tuple|<tuple|Local frame>|<pageref|auto-83>>

      <tuple|<tuple|Frame|local>|<pageref|auto-84>>

      <tuple|<tuple|Force|fictitious>|<pageref|auto-85>>

      <tuple|<tuple|Local frame|nonrotating>|<pageref|auto-87>>

      <tuple|<tuple|Frame|local|nonrotating>|<pageref|auto-88>>

      <tuple|<tuple|Lab frame>|<pageref|auto-89>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-90>>

      <tuple|<tuple|center-of-mass frame>||app-forces com-frame
      idx1|<tuple|Center-of-mass frame>|<pageref|auto-92>>

      <tuple|<tuple|frame|center-of-mass>||app-forces com-frame
      idx2|<tuple|Frame|center-of-mass>|<pageref|auto-93>>

      <tuple|<tuple|Frame|local|nonrotating>|<pageref|auto-94>>

      <tuple|<tuple|Center of mass>|<pageref|auto-95>>

      <tuple|<tuple|Lab frame>|<pageref|auto-97>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-98>>

      <tuple|<tuple|Local frame|nonrotating>|<pageref|auto-100>>

      <tuple|<tuple|Frame|local|nonrotating>|<pageref|auto-101>>

      <tuple|<tuple|Local frame|nonrotating>|<pageref|auto-102>>

      <tuple|<tuple|Frame|local|nonrotating>|<pageref|auto-103>>

      <tuple|<tuple|Local frame|nonrotating>|<pageref|auto-104>>

      <tuple|<tuple|Frame|local|nonrotating>|<pageref|auto-105>>

      <tuple|<tuple|center-of-mass frame>||app-forces com-frame
      idx1|<tuple|Center-of-mass frame>|<pageref|auto-106>>

      <tuple|<tuple|frame|center-of-mass>||app-forces com-frame
      idx2|<tuple|Frame|center-of-mass>|<pageref|auto-107>>

      <tuple|<tuple|rotating local frame>||app-forces rotating-frame
      idx1|<tuple|Rotating local frame>|<pageref|auto-109>>

      <tuple|<tuple|frame|local|rotating>||app-forces rotating-frame
      idx2|<tuple|Frame|local|rotating>|<pageref|auto-110>>

      <tuple|<tuple|Force|centrifugal>|<pageref|auto-112>>

      <tuple|<tuple|Centrifugal force>|<pageref|auto-113>>

      <tuple|<tuple|Force|Coriolis>|<pageref|auto-114>>

      <tuple|<tuple|Coriolis force>|<pageref|auto-115>>

      <tuple|<tuple|rotating local frame>||app-forces rotating-frame
      idx1|<tuple|Rotating local frame>|<pageref|auto-116>>

      <tuple|<tuple|frame|local|rotating>||app-forces rotating-frame
      idx2|<tuple|Frame|local|rotating>|<pageref|auto-117>>

      <tuple|<tuple|reference frame|earth fixed>||app-forces
      earth-fixed-frame idx1|<tuple|Reference
      frame|earth-fixed>|<pageref|auto-119>>

      <tuple|<tuple|Frame|reference|earth fixed>||app-forces
      earth-fixed-frame idx2|<tuple|Frame|reference|earth-fixed>|<pageref|auto-120>>

      <tuple|<tuple|Force|centrifugal>|<pageref|auto-121>>

      <tuple|<tuple|Centrifugal force>|<pageref|auto-122>>

      <tuple|<tuple|Force|Coriolis>|<pageref|auto-123>>

      <tuple|<tuple|Coriolis force>|<pageref|auto-124>>

      <tuple|<tuple|Force|gravitational>|<pageref|auto-125>>

      <tuple|<tuple|Gravitational|force>|<pageref|auto-126>>

      <tuple|<tuple|reference frame|earth fixed>||app-forces
      earth-fixed-frame idx1|<tuple|Reference
      frame|earth-fixed>|<pageref|auto-127>>

      <tuple|<tuple|Frame|reference|earth fixed>||app-forces
      earth-fixed-frame idx2|<tuple|Frame|reference|earth-fixed>|<pageref|auto-128>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix
      A<space|2spc>Forces, Energy, and Work>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      A.1<space|2spc>Forces between Particles
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>

      A.2<space|2spc>The System and Surroundings
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-32>

      A.3<space|2spc>System Energy Change
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-40>

      A.4<space|2spc>Macroscopic Work <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-45>

      A.5<space|2spc>The Work Done on the System and Surroundings
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-52>

      A.6<space|2spc>The Local Frame and Internal Energy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-61>

      A.7<space|2spc>Nonrotating Local Frame
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-86>

      A.8<space|2spc>Center-of-mass Local Frame
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-91>

      A.9<space|2spc>Rotating Local Frame
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-108>

      A.10<space|2spc>Earth-Fixed Reference Frame
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-118>
    </associate>
  </collection>
</auxiliary>