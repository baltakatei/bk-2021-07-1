<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <appendix|Mathematical Properties of State Functions><label|app:state>

  A state function is a property of a thermodynamic system whose value at any
  given instant depends only on the state of the system at that instant (Sec.
  <reference|2-state of the system>).

  <section|Differentials><label|app-state diff>

  The <index|Differential><newterm|differential> <math|<df>> of a state
  function <math|f> is an infinitesimal change of <math|f>. Since the value
  of a state function by definition depends only on the state of the system,
  integrating <math|<df>> between an initial state <math|1> and a final state
  <math|2> yields the change in <math|f>, and this change is independent of
  the path:

  <\equation>
    <big|int><rsub|f<rsub|1>><rsup|f<rsub|2>><space|-0.17em><df>=f<rsub|2>-f<rsub|1>=<Del>f
  </equation>

  A differential with this property is called an
  <subindex|Differential|exact><index|Exact differential><em|exact>
  differential. The differential of a state function is always exact.

  <section|Total Differential><label|app-state tot-diff><label|app:total
  diff>

  A state function <math|f> treated as a dependent variable is a function of
  a certain number of independent variables that are also state functions.
  The <subindex|Differential|total><index|Total differential><newterm|total
  differential> of <math|f> is <math|<df>> expressed in terms of the
  differentials of the independent variables and has the form

  <\equation>
    <label|df=(df/dx)dx+.+.+...><df>=<Pd|f|x|><dx>+<Pd|f|y|><dif>y+<Pd|f|z|><dif>z+\<ldots\>
  </equation>

  There are as many terms in the expression on the right side as there are
  independent variables. Each partial derivative in the expression has all
  independent variables held constant except the variable shown in the
  denominator.

  Figure <vpageref|fig:F-total differential> interprets this expression for a
  function <math|f> of the two independent variables <math|x> and <math|y>.
  The shaded plane represents a small element of the surface
  <math|f=f<around|(|x,y|)>>.

  <\big-figure|<image|../src/BACK-SUP/totaldif.eps|364pt|188pt||>>
    <label|fig:F-total differential>
  </big-figure>

  \;

  Consider a system with three independent variables. If we choose these
  independent variables to be <math|x>, <math|y>, and <math|z>, the total
  differential of the dependent state function <math|f> takes the form

  <\equation>
    <label|df=adx+bdy+cdz><df>=a*<dx>+b*<dif>y+c*<dif>z
  </equation>

  where we can identify the coefficients as

  <\equation>
    <label|a=(df/dx)_y,z etc.>a=<Pd|f|x|y,z><space|2em>b=<Pd|f|y|x,z><space|2em>c=<Pd|f|z|x,y>
  </equation>

  These coefficients are themselves, in general, functions of the independent
  variables and may be differentiated to give mixed second partial
  derivatives; for example:

  <\equation>
    <Pd|a|y|x,z>=<frac|\<partial\><rsup|2>*<space|-.08cm>f|\<partial\>*y*\<partial\>*x><space|2em><Pd|b|x|y,z>=<frac|\<partial\><rsup|2>*<space|-.08cm>f|\<partial\>*x*\<partial\>*y>
  </equation>

  The second partial derivative <math|\<partial\><rsup|2>*<space|-.08cm>f/\<partial\>*y*\<partial\>*x>,
  for instance, is the partial derivative with respect to <math|y> of the
  partial derivative of <math|f> with respect to <math|x>. It is a theorem of
  calculus that if a function <math|f> is single valued and has continuous
  derivatives, the order of differentiation in a mixed derivative is
  immaterial. Therefore the mixed derivatives
  <math|\<partial\><rsup|2>*<space|-.08cm>f/\<partial\>*y*\<partial\>*x> and
  <math|\<partial\><rsup|2>*<space|-.08cm>f/\<partial\>*x*\<partial\>*y>,
  evaluated for the system in any given state, are equal:

  <\equation>
    <Pd|a|y|x,z>=<Pd|b|x|y,z>
  </equation>

  The general relation that applies to a function of any number of
  independent variables is

  <\equation>
    <Pd|X|y|>=<Pd|Y|x|>
  </equation>

  where <math|x> and <math|y> are <em|any> two of the independent variables,
  <math|X> is <math|\<partial\>*<space|-.04cm>f/\<partial\>*x>, <math|Y> is
  <math|\<partial\>*<space|-.04cm>f/\<partial\>*y>, and each partial
  derivative has all independent variables held constant except the variable
  shown in the denominator. This general relation is the <index|Euler
  reciprocity relation>Euler reciprocity relation, or <index|Reciprocity
  relation><newterm|reciprocity relation> for short. A necessary and
  sufficient condition for <math|<df>> to be an exact differential is that
  the reciprocity relation is satisfied for each pair of independent
  variables.

  <section|Integration of a Total Differential><label|app-state
  int-tdif><label|app:int of tot diff>

  If the coefficients of the total differential of a dependent variable are
  known as functions of the independent variables, the expression for the
  total differential may be integrated to obtain an expression for the
  dependent variable as a function of the independent variables.

  For example, suppose the total differential of the state function
  <math|f<around|(|x,y,z|)>> is given by Eq. <reference|df=adx+bdy+cdz> and
  the coefficients are known functions <math|a*<around|(|x,y,z|)>>,
  <math|b*<around|(|x,y,z|)>>, and <math|c*<around|(|x,y,z|)>>. Because
  <math|f> is a state function, its change between <math|f<around|(|0,0,0|)>>
  and <math|f<around|(|x<rprime|'>,y<rprime|'>,z<rprime|'>|)>> is independent
  of the integration path taken between these two states. A convenient path
  would be one with the following three segments:

  <\enumerate>
    <item>integration from <math|<around|(|0,0,0|)>> to
    <math|<around|(|x<rprime|'>,0,0|)>>: <math|<big|int><rsub|0><rsup|x<rprime|'>><space|-0.17em>a*<around|(|x,0,0|)><dx>>

    <item>integration from <math|<around|(|x<rprime|'>,0,0|)>> to
    <math|<around|(|x<rprime|'>,y<rprime|'>,0|)>>:
    <math|<big|int><rsub|0><rsup|y<rprime|'>><space|-0.17em>b*<around|(|x<rprime|'>,y,0|)><dif>y>

    <item>integration from <math|<around|(|x<rprime|'>,y<rprime|'>,0|)>> to
    <math|<around|(|x<rprime|'>,y<rprime|'>,z<rprime|'>|)>>:
    <math|<big|int><rsub|0><rsup|z<rprime|'>><space|-0.17em>c*<around|(|x<rprime|'>,y<rprime|'>,z|)><dif>z>
  </enumerate>

  The expression for <math|f<around|(|x,y,z|)>> is then the sum of the three
  integrals and a constant of integration.

  Here is an example of this procedure applied to the total differential

  <\equation>
    <label|df=(2xy)dx+...><df>=<around|(|2*x*y|)>*<dx>+<around|(|x<rsup|2>+z|)>*<dif>y+<around|(|y-9*z<rsup|2>|)>*<dif>z
  </equation>

  An expression for the function <math|f> in this example is given by the sum

  <\eqnarray*>
    <tformat|<table|<row|<cell|f>|<cell|=>|<cell|<big|int><rsub|0><rsup|x<rprime|'>><space|-0.17em><around|(|2*x\<cdot\>0|)><dx>+<big|int><rsub|0><rsup|y<rprime|'>><space|-0.17em><around|[|<around|(|x<rprime|'>|)><rsup|2>+0|]><dif>y+<big|int><rsub|0><rsup|z<rprime|'>><space|-0.17em><around|(|y<rprime|'>-9*z<rsup|2>|)><dif>z+C>>|<row|<cell|>|<cell|=>|<cell|0+x<rsup|2>*y+<around|(|y*z-9*z<rsup|3>/3|)>+C>>|<row|<cell|>|<cell|=>|<cell|x<rsup|2>*y+y*z-3*z<rsup|3>+C<eq-number><label|f=>>>>>
  </eqnarray*>

  where primes are omitted on the second and third lines because the
  expressions are supposed to apply to any values of <math|x>, <math|y>, and
  <math|z>. <math|C> is an integration constant. You can verify that the
  third line of Eq. <reference|f=> gives the correct expression for <math|f>
  by taking partial derivatives with respect to <math|x>, <math|y>, and
  <math|z> and comparing with Eq. <reference|df=(2xy)dx+...>.

  In chemical thermodynamics, there is not likely to be occasion to perform
  this kind of integration. The fact that it can be done, however, shows that
  if we stick to one set of independent variables, the expression for the
  total differential of an independent variable contains the same information
  as the independent variable itself.

  A different kind of integration can be used to express a dependent
  extensive property in terms of independent extensive properties. An
  <em|extensive> property of a thermodynamic system is one that is additive,
  and an <em|intensive> property is one that is not additive and has the same
  value everywhere in a homogeneous region (Sec. <reference|2-extensive
  \ intensive>). Suppose we have a state function <math|f> that is an
  extensive property with the total differential

  <\equation>
    <df>=a*<dx>+b*<dif>y+c*<dif>z+\<ldots\>
  </equation>

  where the independent variables <math|x,y,z,\<ldots\>> are extensive and
  the coefficients <math|a,b,c,\<ldots\>> are intensive. If the independent
  variables include those needed to describe an open system (for example, the
  amounts of the substances), then it is possible to integrate both sides of
  the equation from a lower limit of zero for each of the extensive functions
  while holding the intensive functions constant:

  <\equation>
    <big|int><rsub|0><rsup|f<rprime|'>><space|-0.17em><space|-0.17em><df>=a*<big|int><rsub|0><rsup|x<rprime|'>><space|-0.17em><space|-0.17em><dx>+b*<big|int><rsub|0><rsup|y<rprime|'>><space|-0.17em><space|-0.17em><dif>y+c*<big|int><rsub|0><rsup|z<rprime|'>><space|-0.17em><space|-0.17em><dif>z+\<ldots\>
  </equation>

  <\equation>
    f<rprime|'>=a*x<rprime|'>+b*y<rprime|'>+c*z<rprime|'>+\<ldots\>
  </equation>

  Note that a term of the form <math|c<dif>u> where <math|u> is
  <em|intensive> becomes <em|zero> when integrated with intensive functions
  held constant, because <math|<dif>u> is this case is zero.

  <section|Legendre Transforms><label|app-state lt><label|app:Legendre>

  <index-complex|<tuple|legendre transform>||app-state lt
  idx1|<tuple|Legendre transform>>A <index|Legendre
  transform><newterm|Legendre transform> of a state function is a linear
  change of one or more of the independent variables made by subtracting
  products of conjugate variables.

  To understand how this works, consider a state function <math|f> whose
  total differential is given by

  <\equation>
    <label|df(x,y,x)=adx+bdy+cdz><df>=a*<dx>+b*<dif>y+c*<dif>z
  </equation>

  In the expression on the right side, <math|x>, <math|y>, and <math|z> are
  being treated as the independent variables. The pairs <math|a> and
  <math|x>, <math|b> and <math|y>, and <math|c> and <math|z> are
  <em|conjugate pairs>. That is, <math|a> and <math|x> are conjugates,
  <math|b> and <math|y> are conjugates, and <math|c> and <math|z> are
  conjugates.

  For the first example of a Legendre transform, we define a new state
  function <math|f<rsub|1>> by subtracting the product of the conjugate
  variables <math|a> and <math|x>:

  <\equation>
    <label|f1=f-ax>f<rsub|1><defn>f-a*x
  </equation>

  The function <math|f<rsub|1>> is a Legendre transform of <math|f>. We take
  the differential of Eq. <reference|f1=f-ax>

  <\equation>
    <df><rsub|1>=<df>-a*<dx>-x*<dif>a
  </equation>

  and substitute for <math|<df>> from Eq. <reference|df(x,y,x)=adx+bdy+cdz>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<df><rsub|1>>|<cell|=>|<cell|<around|(|a*<dx>+b*<dif>y+c*<dif>z|)>-a*<dx>-x*<dif>a>>|<row|<cell|>|<cell|=>|<cell|-x*<dif>a+b*<dif>y+c*<dif>z<eq-number><label|df1=-xda+bdy+cdz>>>>>
  </eqnarray*>

  Equation <reference|df1=-xda+bdy+cdz> gives the total differential of
  <math|f<rsub|1>> with <math|a>, <math|y>, and <math|z> as the independent
  variables. The functions <math|x> and <math|a> have switched places as
  independent variables. What we did in order to let <math|a> replace
  <math|x> as an independent variable was to subtract from <math|f> the
  product of the conjugate variables <math|a> and <math|x>.

  Because the right side of Eq. <reference|df1=-xda+bdy+cdz> is an expression
  for the total differential of the state function <math|f<rsub|1>>, we can
  use the expression to identify the coefficients as partial derivatives of
  <math|f<rsub|1>> with respect to the new set of independent variables:

  <\equation>
    -x=<Pd|f<rsub|1>|a|y,z><space|2em>b=<Pd|f<rsub|1>|y|a,z><space|2em>c=<Pd|f<rsub|1>|z|a,y>
  </equation>

  We can also use Eq. <reference|df1=-xda+bdy+cdz> to write new reciprocity
  relations, such as

  <\equation>
    -<Pd|x|y|a,z>=<Pd|b|a|y,z>
  </equation>

  We can make other Legendre transforms of <math|f> by subtracting one or
  more products of conjugate variables. A second example of a Legendre
  transform is

  <\equation>
    <label|f2=f-by-cz>f<rsub|2><defn>f-b*y-c*z
  </equation>

  whose total differential is

  <\eqnarray*>
    <tformat|<table|<row|<cell|<df><rsub|2>>|<cell|=>|<cell|<df>-b*<dif>y-y*<dif>b-c*<dif>z-z*<dif>c>>|<row|<cell|>|<cell|=>|<cell|a*<dx>-y*<dif>b-z*<dif>c<eq-number><label|df2=>>>>>
  </eqnarray*>

  Here <math|b> has replaced <math|y> and <math|c> has replaced <math|z> as
  independent variables. Again, we can identify the coefficients as partial
  derivatives and write new reciprocity relations.

  If we have an algebraic expression for a state function as a function of
  independent variables, then a Legendre transform preserves all the
  information contained in that expression. To illustrate this, we can use
  the state function <math|f> and its Legendre transform <math|f<rsub|2>>
  described above. Suppose we have an expression for
  <math|f<around|(|x,y,z|)>>\Vthis is <math|f> expressed as a function of the
  independent variables <math|x>, <math|y>, and <math|z>. Then by taking
  partial derivatives of this expression, we can find according to Eq.
  <reference|a=(df/dx)_y,z etc.> expressions for the functions
  <math|a*<around|(|x,y,z|)>>, <math|b*<around|(|x,y,z|)>>, and
  <math|c*<around|(|x,y,z|)>>.

  Now we perform the Legendre transform of Eq. <reference|f2=f-by-cz>:
  <hgroup|<math|f<rsub|2>=f-b*y-c*z>> with total differential
  <hgroup|<math|<df><rsub|2>=a<dx>-y<dif>b-z<dif>c>> (Eq. <reference|df2=>).
  The independent variables have been changed from <math|x>, <math|y>, and
  <math|z> to <math|x>, <math|b>, and <math|c>.

  We want to find an expression for <math|f<rsub|2>> as a function of these
  new variables, using the information available from the original function
  <math|f<around|(|x,y,z|)>>. To do this, we eliminate <math|z> from the
  known functions <math|b*<around|(|x,y,z|)>> and <math|c*<around|(|x,y,z|)>>
  and solve for <math|y> as a function of <math|x>, <math|b>, and <math|c>.
  We also eliminate <math|y> from <math|b*<around|(|x,y,z|)>> and
  <math|c*<around|(|x,y,z|)>> and solve for <math|z> as a function of
  <math|x>, <math|b>, and <math|c>. This gives us expressions for
  <math|y*<around|(|x,b,c|)>> and <math|z<around|(|x,b,c|)>> which we
  substitute into the expression for <math|f<around|(|x,y,z|)>>, turning it
  into the function <math|f<around|(|x,b,c|)>>. Finally, we use the functions
  of the new variables to obtain an expression for
  <hgroup|<math|f<rsub|2><around|(|x,b,c|)>=f<around|(|x,b,c|)>-b*y*<around|(|x,b,c|)>-c*z<around|(|x,b,c|)>>>.

  The original expression for <math|f<around|(|x,y,z|)>> and the new
  expression for <math|f<rsub|2><around|(|x,b,c|)>> contain the same
  information. We could take the expression for
  <math|f<rsub|2><around|(|x,b,c|)>> and, by following the same procedure
  with the Legendre transform <math|f=f<rsub|2>+b*y+c*z>, retrieve the
  expression for <math|f<around|(|x,y,z|)>>. Thus no information is lost
  during a Legendre transform.<index-complex|<tuple|legendre
  transform>||app-state lt idx1|<tuple|Legendre transform>>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|a=(df/dx)_y,z etc.|<tuple|A.2.3|?>>
    <associate|app-state diff|<tuple|A.1|?>>
    <associate|app-state int-tdif|<tuple|A.3|?>>
    <associate|app-state lt|<tuple|A.4|?>>
    <associate|app-state tot-diff|<tuple|A.2|?>>
    <associate|app:Legendre|<tuple|A.4|?>>
    <associate|app:int of tot diff|<tuple|A.3|?>>
    <associate|app:state|<tuple|A|?>>
    <associate|app:total diff|<tuple|A.2|?>>
    <associate|auto-1|<tuple|A|?>>
    <associate|auto-10|<tuple|total differential|?>>
    <associate|auto-11|<tuple|A.2.1|?>>
    <associate|auto-12|<tuple|Euler reciprocity relation|?>>
    <associate|auto-13|<tuple|Reciprocity relation|?>>
    <associate|auto-14|<tuple|reciprocity relation|?>>
    <associate|auto-15|<tuple|A.3|?>>
    <associate|auto-16|<tuple|A.4|?>>
    <associate|auto-17|<tuple|<tuple|legendre transform>|?>>
    <associate|auto-18|<tuple|Legendre transform|?>>
    <associate|auto-19|<tuple|Legendre transform|?>>
    <associate|auto-2|<tuple|A.1|?>>
    <associate|auto-20|<tuple|<tuple|legendre transform>|?>>
    <associate|auto-3|<tuple|Differential|?>>
    <associate|auto-4|<tuple|differential|?>>
    <associate|auto-5|<tuple|Differential|?>>
    <associate|auto-6|<tuple|Exact differential|?>>
    <associate|auto-7|<tuple|A.2|?>>
    <associate|auto-8|<tuple|Differential|?>>
    <associate|auto-9|<tuple|Total differential|?>>
    <associate|df(x,y,x)=adx+bdy+cdz|<tuple|A.4.1|?>>
    <associate|df1=-xda+bdy+cdz|<tuple|A.4.4|?>>
    <associate|df2=|<tuple|A.4.8|?>>
    <associate|df=(2xy)dx+...|<tuple|A.3.1|?>>
    <associate|df=(df/dx)dx+.+.+...|<tuple|A.2.1|?>>
    <associate|df=adx+bdy+cdz|<tuple|A.2.2|?>>
    <associate|f1=f-ax|<tuple|A.4.2|?>>
    <associate|f2=f-by-cz|<tuple|A.4.7|?>>
    <associate|f=|<tuple|A.3.2|?>>
    <associate|fig:F-total differential|<tuple|A.2.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|A.2.1>|>
        \;
      </surround>|<pageref|auto-11>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|A.2.2>||>|<pageref|auto-12>>
    </associate>
    <\associate|gly>
      <tuple|normal|differential|<pageref|auto-4>>

      <tuple|normal|total differential|<pageref|auto-10>>

      <tuple|normal|reciprocity relation|<pageref|auto-15>>

      <tuple|normal|Legendre transform|<pageref|auto-19>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Differential>|<pageref|auto-3>>

      <tuple|<tuple|Differential|exact>|<pageref|auto-5>>

      <tuple|<tuple|Exact differential>|<pageref|auto-6>>

      <tuple|<tuple|Differential|total>|<pageref|auto-8>>

      <tuple|<tuple|Total differential>|<pageref|auto-9>>

      <tuple|<tuple|Euler reciprocity relation>|<pageref|auto-13>>

      <tuple|<tuple|Reciprocity relation>|<pageref|auto-14>>

      <tuple|<tuple|Legendre transform>|<pageref|auto-18>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix
      A<space|2spc>Mathematical Properties of State Functions>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      A.1<space|2spc>Differentials <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      A.2<space|2spc>Total Differential <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>

      A.3<space|2spc>Integration of a Total Differential
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>

      A.4<space|2spc>Legendre Transforms <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>
    </associate>
  </collection>
</auxiliary>