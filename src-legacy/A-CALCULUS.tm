<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <appendix|Calculus Review><label|app:calc>

  <section|Derivatives>

  Let <math|f> be a function of the variable <math|x>, and let <math|<Del>f>
  be the change in <math|f> when <math|x> changes by <math|<Del>x>. Then the
  <index|Derivative><newterm|derivative> <math|<df>/<dx>> is the ratio
  <math|<Del>f/<Del>x> in the limit as <math|<Del>x> approaches zero. The
  derivative <math|<df>/<dx>> can also be described as the rate at which
  <math|f> changes with <math|x>, and as the slope of a curve of <math|f>
  plotted as a function of <math|x>.

  The following is a short list of formulas likely to be needed. In these
  formulas, <math|u> and <math|v> are arbitrary functions of <math|x>, and
  <math|a> is a constant. <subindex|Derivative|formulas>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<frac|<dif><around|(|u<rsup|a>|)>|<dx>>>|<cell|=>|<cell|a*u<rsup|a-1>*<frac|<dif>u|<dx>>>>|<row|<cell|<frac|<dif><around|(|u*v|)>|<dx>>>|<cell|=>|<cell|u*<frac|<dif>v|<dx>>+v*<frac|<dif>u|<dx>>>>|<row|<cell|<frac|<dif><around|(|u/v|)>|<dx>>>|<cell|=>|<cell|<around*|(|<frac|1|v<rsup|2>>|)>*<around*|(|v*<frac|<dif>u|<dx>>-u*<frac|<dif>v|<dx>>|)>>>|<row|<cell|<frac|<dif>ln
    <around|(|a*x|)>|<dx>>>|<cell|=>|<cell|<frac|1|x>>>|<row|<cell|<frac|<dif><around|(|e<rsup|a*x>|)>|<dx>>>|<cell|=>|<cell|a*e<rsup|a*x>>>|<row|<cell|<frac|<df><around|(|u|)>|<dx>>>|<cell|=>|<cell|<frac|<df><around|(|u|)>|<dif>u>\<cdot\><frac|<dif>u|<dx>>>>>>
  </eqnarray*>

  <section|Partial Derivatives>

  If <math|f> is a function of the independent variables <math|x>, <math|y>,
  and <math|z>, the <index|Partial derivative><newterm|partial derivative>
  <math|<pd|f|x|y,z>> is the derivative <math|<df>/<dx>> with <math|y> and
  <math|z> held constant. It is important in thermodynamics to indicate the
  variables that are held constant, as <math|<pd|f|x|y,z>> is not necessarily
  equal to <math|<pd|f|x|a,b>> where <math|a> and <math|b> are variables
  different from <math|y> and <math|z>.

  The variables shown at the bottom of a partial derivative should tell you
  which variables are being used as the independent variables. For example,
  if the partial derivative is <math|<Pd|f|y|a,b>> then <math|f> is being
  treated as a function of <math|y>, <math|a>, and <math|b>.

  <new-page>

  <section|Integrals>

  Let <math|f> be a function of the variable <math|x>. Imagine the range of
  <math|x> between the limits <math|x<rprime|'>> and <math|x<rprime|''>> to
  be divided into many small increments of size
  <math|<Del>x<rsub|i><around|(|i=1,2,\<ldots\>|)>>. Let <math|f<rsub|i>> be
  the value of <math|f> when <math|x> is in the middle of the range of the
  <math|i>th increment. Then the <index|Integral><newterm|integral>

  <\equation*>
    <big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><space|-0.17em><space|-0.17em>f<dx>
  </equation*>

  is the sum <math|<big|sum><rsub|i>f<rsub|i><Del>x<rsub|i>> in the limit as
  each <math|<Del>x<rsub|i>> approaches zero and the number of terms in the
  sum approaches infinity. The integral is also the area under a curve of
  <math|f> plotted as a function of <math|x>, measured from
  <math|x=x<rprime|'>> to <math|x=x<rprime|''>>. The function <math|f> is the
  <index|Integrand><newterm|integrand>, which is integrated over the
  integration variable <math|x>.

  This book uses the following integrals: <subindex|Integral|formulas>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><space|-0.17em><space|-0.17em><dx>>|<cell|=>|<cell|x<rprime|''>-x<rprime|'>>>|<row|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><frac|<dx>|x>>|<cell|=>|<cell|<text|ln>
    <around*|\||<dfrac|x<rprime|''>|x<rprime|'>>|\|>>>|<row|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><space|-0.17em>x<rsup|a>*<dx>>|<cell|=>|<cell|<frac|1|a+1>*<around*|[|<around|(|x<rprime|''>|)><rsup|a+1>-<around|(|x<rprime|'>|)><rsup|a+1>|]><htab|5mm><around*|(|<text|<math|a>
    is a constant other than <math|-1>>|)>>>|<row|<cell|<big|int><rsub|x*'><rsup|x*'*'><space|-0.17em><frac|<dx>|a*x+b>>|<cell|=>|<cell|<frac|1|a>*ln
    <around*|\||<frac|a*x*'*'+b|a*x*'+b>|\|><htab|5mm><around*|(|<text|<math|a>
    is a constant>|)>>>>>
  </eqnarray*>

  Here are examples of the use of the expression for the third integral with
  <math|a> set equal to <math|1> and to <math|-2>:

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><space|-0.17em>x*<dx>>|<cell|=>|<cell|<frac|1|2>*<around*|[|<around|(|x<rprime|''>|)><rsup|2>-<around|(|x<rprime|'>|)><rsup|2>|]>>>|<row|<cell|<big|int><rsub|x<rprime|'>><rsup|x<rprime|''>><frac|<dx>|x<rsup|2>>>|<cell|=>|<cell|-<around*|(|<frac|1|x<rprime|''>>-<frac|1|x<rprime|'>>|)>>>>>
  </eqnarray*>

  <section|Line Integrals><label|app-line integrals>

  A <index|Line integral><subindex|Integral|line><newterm|line integral> is
  an integral with an implicit single integration variable that constraints
  the integration to a path.

  The most frequently-seen line integral in this book,
  <math|<big|int><space|-0.17em>p*<dif>V>, will serve as an example. The
  integral can be evaluated in three different ways:

  <\enumerate>
    <item>The integrand <math|p> can be expressed as a function of the
    integration variable <math|V>, so that there is only one variable. For
    example, if <math|p> equals <math|c/V> where <math|c> is a constant, the
    line integral is given by <math|<big|int><space|-0.17em>p*<dif>V=c*<big|int><rsub|V<rsub|1>><rsup|V<rsub|2>><around|(|1/V|)>*<dif>V=c*ln
    <around|(|V<rsub|2>/V<rsub|1>|)>>.

    <item>If <math|p> and <math|V> can be written as functions of another
    variable, such as time, that coordinates their values so that they follow
    the desired path, this new variable becomes the integration variable.

    <item>The desired path can be drawn as a curve on a plot of <math|p>
    versus <math|V>; then <math|<big|int><space|-0.17em>p*<dif>V> is equal in
    value to the area under the curve.
  </enumerate>
</body>

<\initial>
  <\collection>
    <associate|font-base-size|10>
    <associate|page-height|auto>
    <associate|page-type|letter>
    <associate|page-width|auto>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|app-line integrals|<tuple|A.4|?>>
    <associate|app:calc|<tuple|A|?>>
    <associate|auto-1|<tuple|A|?>>
    <associate|auto-10|<tuple|Integral|?>>
    <associate|auto-11|<tuple|integral|?>>
    <associate|auto-12|<tuple|Integrand|?>>
    <associate|auto-13|<tuple|integrand|?>>
    <associate|auto-14|<tuple|Integral|?>>
    <associate|auto-15|<tuple|A.4|?>>
    <associate|auto-16|<tuple|Line integral|?>>
    <associate|auto-17|<tuple|Integral|?>>
    <associate|auto-18|<tuple|line integral|?>>
    <associate|auto-2|<tuple|A.1|?>>
    <associate|auto-3|<tuple|Derivative|?>>
    <associate|auto-4|<tuple|derivative|?>>
    <associate|auto-5|<tuple|Derivative|?>>
    <associate|auto-6|<tuple|A.2|?>>
    <associate|auto-7|<tuple|Partial derivative|?>>
    <associate|auto-8|<tuple|partial derivative|?>>
    <associate|auto-9|<tuple|A.3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|gly>
      <tuple|normal|derivative|<pageref|auto-4>>

      <tuple|normal|partial derivative|<pageref|auto-8>>

      <tuple|normal|integral|<pageref|auto-11>>

      <tuple|normal|integrand|<pageref|auto-13>>

      <tuple|normal|line integral|<pageref|auto-18>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Derivative>|<pageref|auto-3>>

      <tuple|<tuple|Derivative|formulas>|<pageref|auto-5>>

      <tuple|<tuple|Partial derivative>|<pageref|auto-7>>

      <tuple|<tuple|Integral>|<pageref|auto-10>>

      <tuple|<tuple|Integrand>|<pageref|auto-12>>

      <tuple|<tuple|Integral|formulas>|<pageref|auto-14>>

      <tuple|<tuple|Line integral>|<pageref|auto-16>>

      <tuple|<tuple|Integral|line>|<pageref|auto-17>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix
      A<space|2spc>Calculus Review> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      A.1<space|2spc>Derivatives <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      A.2<space|2spc>Partial Derivatives <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>

      A.3<space|2spc>Integrals <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>

      A.4<space|2spc>Line Integrals <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>
    </associate>
  </collection>
</auxiliary>