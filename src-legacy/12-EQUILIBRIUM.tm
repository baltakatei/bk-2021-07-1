<TeXmacs|1.99.20>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    <assign|C|<macro|\<bbb-C\>>>
  </hide-preamble>

  <chapter|Equilibrium Conditions in Multicomponent Systems>

  <paragraphfootnotes><label|Chap. 12>

  This chapter applies equilibrium theory to a variety of chemical systems of
  more than one component. Two different approaches will be used as
  appropriate: one based on the relation <math|\<mu\><rsub|i><aph>=\<mu\><rsub|i><bph>>
  for transfer equilibrium, the other based on
  <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i>=0> or
  <math|K=<big|prod><rsub|i>a<rsub|i><rsup|\<nu\><rsub|i>>> for reaction
  equilibrium.

  <section|Effects of Temperature><label|12-effects of T>

  For some of the derivations in this chapter, we will need an expression for
  the rate at which the ratio <math|\<mu\><rsub|i>/T> varies with temperature
  in a phase of fixed composition maintained at constant pressure. This
  expression leads, among other things, to an important relation between the
  temperature dependence of an equilibrium constant and the standard molar
  reaction enthalpy.

  <subsection|Variation of <math|<mathbold|\<mu\><rsub|i>/T>> with
  temperature><label|12-variation of mu/T with T>

  In a phase containing species <math|i>, either pure or in a mixture, the
  partial derivative of <math|\<mu\><rsub|i>/T> with respect to <math|T> at
  constant <math|p> and a fixed amount of each species is given
  by<footnote|This relation is obtained from the formula
  <math|<dif><around|(|u*v|)>/<dx>=u*<around|(|<dif>v/<dx>|)>+v*<around|(|<dif>u/<dx>|)>>
  (Appendix <reference|app:calc>), where <math|u> is <math|1/T>, <math|v> is
  <math|\<mu\><rsub|i>>, and <math|x> is <math|T>.>

  <\equation>
    <label|d(mu/T)/dT=><bPd|<around*|(|\<mu\><rsub|i>/T|)>|T|p,<allni>>=<frac|1|T><Pd|\<mu\><rsub|i>|T|<space|-0.17em><space|-0.17em>p,<allni>>-<frac|\<mu\><rsub|i>|T<rsup|2>>
  </equation>

  This equality comes from a purely mathematical operation; no thermodynamics
  is involved. The partial derivative <math|<pd|\<mu\><rsub|i>|T|p,<allni>>>
  is equal to <math|-S<rsub|i>> (Eq. <reference|d(mu_i)/dT=-S_i>), so that
  Eq. <reference|d(mu/T)/dT=> becomes

  <\equation>
    <bPd|<around*|(|\<mu\><rsub|i>/T|)>|T|p,<allni>>=-<frac|S<rsub|i>|T>-<frac|\<mu\><rsub|i>|T<rsup|2>>=-<frac|T*S<rsub|i>+\<mu\><rsub|i>|T<rsup|2>>
  </equation>

  The further substitution <math|\<mu\><rsub|i>=H<rsub|i>-T*S<rsub|i>> (Eq.
  <reference|mu_i=H_i-TS_i>) gives finally

  <\equation>
    <label|d(mu_i/T)/dT=-H_i/T^2><bPd|<around*|(|\<mu\><rsub|i>/T|)>|T|p,<allni>>=-<frac|H<rsub|i>|T<rsup|2>>
  </equation>

  <\quote-env>
    \ For a pure substance in a closed system, Eq.
    <reference|d(mu_i/T)/dT=-H_i/T^2> when multiplied by the amount <math|n>
    becomes

    <\equation>
      <bPd|<around*|(|G/T|)>|T|p>=-<frac|H|T<rsup|2>>
    </equation>

    This is the <index|Gibbs--Helmholtz equation><em|Gibbs--Helmholtz
    equation>.
  </quote-env>

  <subsection|Variation of <math|<mathbold|\<mu\><rsub|i><st>/T>> with
  temperature><label|12-variation of muo/T with T>

  If we make the substitution <math|\<mu\><rsub|i>=\<mu\><rsub|i><st>+R*T*ln
  a<rsub|i>> in Eq. <reference|d(mu_i/T)/dT=-H_i/T^2> and rearrange, we
  obtain

  <\equation>
    <label|d(mu(i)o)/dT=><frac|<dif><around|(|\<mu\><rsub|i><st>/T|)>|<dif>T>=-<frac|H<rsub|i>|T<rsup|2>>-R<Pd|ln
    a<rsub|i>|T|<space|-0.17em><space|-0.17em>p,<allni>>
  </equation>

  Because <math|\<mu\><rsub|i><st>/T> is a function only of <math|T>, its
  derivative with respect to <math|T> is itself a function only of <math|T>.
  We can therefore use any convenient combination of pressure and composition
  in the expression on the right side of Eq. <reference|d(mu(i)o)/dT=> in
  order to evaluate <math|<dif><around|(|\<mu\><rsub|i><st>/T|)>/<dif>T> at a
  given temperature.

  If species <math|i> is a constituent of a gas mixture, we take a constant
  pressure of the gas that is low enough for the gas to behave ideally. Under
  these conditions <math|H<rsub|i>> is the standard molar enthalpy
  <math|H<rsub|i><st>> (Eq. <reference|H_i=H_io>). In the expression for
  activity, <math|a<rsub|i><gas>=<G><rsub|i><gas><space|0.17em>\<phi\><rsub|i>*p<rsub|i>/p>
  (Table <reference|tbl:9-activities>), the pressure factor
  <math|<G><rsub|i><gas>> is constant when <math|p> is constant, the fugacity
  coefficient <math|\<phi\><rsub|i>> for the ideal gas is unity, and
  <math|p<rsub|i>/p=y<rsub|i>> is constant at constant <math|<allni>>, so
  that the partial derivative <math|<bpd|ln a<rsub|i><gas>|T|p,<allni>>> is
  zero.

  For component <math|i> of a condensed-phase mixture, we take a constant
  pressure equal to the standard pressure <math|p<st>>, and a mixture
  composition in the limit given by Eqs. <reference|ac_i -\<gtr\>
  1>\U<reference|gamma(mB)-\<gtr\>1> in which the activity coefficient is
  unity. <math|H<rsub|i>> is then the standard molar enthalpy
  <math|H<rsub|i><st>>, and the activity is given by an expression in Table
  <reference|tbl:9-activities> with the pressure factor and activity
  coefficient set equal to 1: <math|a<rsub|i>=x<rsub|i>>, <math|a<A|=>x<A>>,
  <math|a<xbB|=>x<B>>, <math|a<cbB|=>c<B>/c<st>>, or
  <math|a<mbB|=>m<B>/m<st>>.<footnote|If solute B is an electrolyte,
  <math|a<mbB>> is given instead by Eq. <reference|a(mB),general>; like
  <math|a<mbB>> for a nonelectrolyte, it is constant as <math|T> changes at
  constant <math|p> and <math|<allni>>. > With the exception of
  <math|a<cbB>>, these activities are constant as <math|T> changes at
  constant <math|p> and <math|<allni>>.

  Thus for a gas-phase species, or a species with a standard state based on
  mole fraction or molality, <math|<bpd|ln a<rsub|i><gas>|T|p,<allni>>> is
  zero and Eq. <reference|d(mu(i)o)/dT=> becomes

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif><around|(|\<mu\><rsub|i><st>/T|)>|<dif>T>=-<frac|H<rsub|i><st>|T<rsup|2>><cond|(s*t*a*n*d*a*r*d*s*t*a*t*e*n*o*t*b*a*s*e*d><nextcond|o*n*c*o*n*c*e*n*t*r*a*t*i*o*n)><eq-number><label|d(mu_io/T)/dT=-H_io/T^2>>>>>
  </gather>

  Equation <reference|d(mu_io/T)/dT=-H_io/T^2>, as the conditions of validity
  indicate, does not apply to a solute standard state based on concentration,
  except as an approximation. The reason is the volume change that
  accompanies an isobaric temperature change. We can treat this case by
  considering the following behavior of <math|ln <around|(|c<B>/c<st>|)>>:

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|<bPd|ln <around|(|c<B>/c<st>|)>|T|p,<allni>>>|<cell|=<frac|1|c<B>><Pd|c<B>|T|<space|-0.17em><space|-0.17em>p,<allni>>=<frac|1|n<B>/V><bPd|<around|(|n<B>/V|)>|T|p,<allni>>>>|<row|<cell|>|<cell|=V<bPd|<around|(|1/V|)>|T|p,<allni>>=-<frac|1|V><Pd|V|T|<space|-0.17em><space|-0.17em>p,<allni>>>>|<row|<cell|>|<cell|=-\<alpha\>>>>>
    </eqsplit>
  </equation>

  Here <math|\<alpha\>> is the cubic expansion coefficient of the solution
  (Eq. <reference|alpha def>). If the activity coefficient is to be unity,
  the solution must be an ideal-dilute solution, and <math|\<alpha\>> is then
  <math|\<alpha\><A><rsup|\<ast\>>>, the cubic expansion coefficient of the
  pure solvent. Eq. <reference|d(mu(i)o)/dT=> for a nonelectrolyte becomes

  <\equation>
    <label|d(mu_Bo/T)/dT=-H_Bo/T^2+alphaA><frac|<dif><around|(|\<mu\><cbB><st>/T|)>|<dif>T>=-<frac|H<B><st>|T<rsup|2>>+R*\<alpha\><A><rsup|\<ast\>>
  </equation>

  <subsection|Variation of ln <space|-0.17em><space|-0.17em><space|-0.17em>
  <math|<mathbold|K>> <space|-0.17em><space|-0.17em><space|-0.17em> with
  temperature><label|12-variation of lnK with T>

  <subsubindex|Equilibrium constant|thermodynamic|temperature dependence>The
  thermodynamic equilibrium constant <math|K>, for a given reaction equation
  and a given choice of reactant and product standard states, is a function
  of <math|T> and <em|only> of <math|T>. By equating two expressions for the
  standard molar reaction Gibbs energy, <math|\<Delta\><rsub|<text|r>>*G<st>=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i><st>>
  and <math|\<Delta\><rsub|<text|r>>*G<st>=-R*T*ln K> (Eqs.
  <reference|del(r)Gmo=sum(nu_i)(mu_io)> and
  <reference|del(r)Gmo=-RT*ln(K)>), we obtain

  <\equation>
    ln K=-<frac|1|R*T>*<big|sum><rsub|i>\<nu\><rsub|i>*\<mu\><rsub|i><st>
  </equation>

  The rate at which <math|ln K> varies with <math|T> is then given by

  <\equation>
    <label|d(lnK)/dT=-(1/R)sum nu_i d(mu_io/T)/dT><frac|<dif>ln
    K|<dif>T>=-<frac|1|R>*<big|sum><rsub|i>\<nu\><rsub|i>*<frac|<dif><around|(|\<mu\><rsub|i><st>/T|)>|<dif>T>
  </equation>

  Combining Eq. <reference|d(lnK)/dT=-(1/R)sum nu_i d(mu_io/T)/dT> with Eqs.
  <reference|d(mu_io/T)/dT=-H_io/T^2> or <reference|d(mu_Bo/T)/dT=-H_Bo/T^2+alphaA>,
  and recognizing that <math|<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*H<rsub|i><st>>
  is the <subsubindex|Enthalpy|reaction|standard molar>standard molar
  reaction enthalpy <math|\<Delta\><rsub|<text|r>>*H<st>>, we obtain the
  final expression for the temperature dependence of <math|ln K>:

  <\equation>
    <label|d(lnK)/dT=del(r)Hmo/RT2-...><frac|<dif>ln
    K|<dif>T>=<frac|\<Delta\><rsub|<text|r>>*H<st>|R*T<rsup|2>>-\<alpha\><A><rsup|\<ast\>><space|-0.17em><space|-0.17em><space|-0.17em><space|-0.17em><big|sum><rsub|<above|<tx|<with|font-size|0.71|c*o*n*c.*b*a*s*i*s>>|<tx|<with|font-size|0.71|s*o*l*u*t*e*s,>>>><space|-0.17em><space|-0.17em><space|-0.17em><space|-0.17em>\<nu\><rsub|i>
  </equation>

  The sum on the right side includes only solute species whose standard
  states are based on concentration. The expression is simpler if all solute
  standard states are based on mole fraction or molality:

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif>ln
    K|<dif>T>=<frac|\<Delta\><rsub|<text|r>>*H<st>|R*T<rsup|2>><cond|(n*o*s*o*l*u*t*e*s*t*a*n*d*a*r*d*s*t*a*t*e*s><nextcond|b*a*s*e*d*o*n*c*o*n*c*e*n*t*r*a*t*i*o*n)><eq-number><label|d(lnK)/dT=del(r)Hmo/RT2>>>>>
  </gather>

  We can rearrange Eq. <reference|d(lnK)/dT=del(r)Hmo/RT2> to

  <\gather>
    <tformat|<table|<row|<cell|\<Delta\><rsub|<text|r>>*H<st>=R*T<rsup|2>*<frac|<dif>ln
    K|<dif>T><cond|(n*o*s*o*l*u*t*e*s*t*a*n*d*a*r*d*s*t*a*t*e*s><nextcond|b*a*s*e*d*o*n*c*o*n*c*e*n*t*r*a*t*i*o*n)><eq-number><label|del(r)Hmo=(RT^2)dln(K)/dT>>>>>
  </gather>

  We can convert this expression for <math|\<Delta\><rsub|<text|r>>*H<st>> to
  an equivalent form by using the mathematical identity
  <math|<dif><around|(|1/T|)>=-<around|(|1/T<rsup|2>|)><dif>T>:

  <\gather>
    <tformat|<table|<row|<cell|\<Delta\><rsub|<text|r>>*H<st>=-R*<frac|<dif>ln
    K|<dif><around|(|1/T|)>><cond|(n*o*s*o*l*u*t*e*s*t*a*n*d*a*r*d*s*t*a*t*e*s><nextcond|b*a*s*e*d*o*n*c*o*n*c*e*n*t*r*a*t*i*o*n)><eq-number><label|del(r)Hmo=-R*dln(K)/d(1/T)>>>>>
  </gather>

  Equations <reference|del(r)Hmo=(RT^2)dln(K)/dT> and
  <reference|del(r)Hmo=-R*dln(K)/d(1/T)> are two forms of the <I|Vant Hoff
  equation@van't Hoff equation\|reg><newterm|van't Hoff equation>. They allow
  us to evaluate the <subsubindex|Enthalpy|reaction|standard molar>standard
  molar reaction enthalpy of a reaction by a noncalorimetric method from the
  temperature dependence of <math|ln K>. For example, we can plot <math|ln K>
  versus <math|1/T>; then according to Eq.
  <reference|del(r)Hmo=-R*dln(K)/d(1/T)>, the slope of the curve at any value
  of <math|1/T> is equal to <math|-\<Delta\><rsub|<text|r>>*H<st>/R> at the
  corresponding temperature <math|T>.

  <\quote-env>
    \ A simple way to derive the equation for this last procedure is to
    substitute <math|\<Delta\><rsub|<text|r>>*G<st>=\<Delta\><rsub|<text|r>>*H<st>-T\<Delta\><rsub|<text|r>>*S<st>>
    in <math|\<Delta\><rsub|<text|r>>*G<st>=-R*T*ln K> and rearrange to

    <\equation>
      <label|lnK=.(1/T)+.>ln K=-<frac|\<Delta\><rsub|<text|r>>*H<st>|R><around*|(|<frac|1|T>|)>+<frac|\<Delta\><rsub|<text|r>>*S<st>|R>
    </equation>

    Suppose we plot <math|ln K> versus <math|1/T>. In a small temperature
    interval in which <math|\<Delta\><rsub|<text|r>>*H<st>> and
    <math|\<Delta\><rsub|<text|r>>*S<st>> are practically constant, the curve
    will appear linear. According to Eq. <reference|lnK=.(1/T)+.>, the curve
    in this interval has a slope of <math|-\<Delta\><rsub|<text|r>>*H<st>/R>,
    and the tangent to a point on the curve has its intercept at <math|1/T=0>
    equal to <math|\<Delta\><rsub|<text|r>>*S<st>/R>.
  </quote-env>

  When we apply Eq. <reference|del(r)Hmo=-R*dln(K)/d(1/T)> to the
  <index|Vaporization><em|vaporization process> A(l)<math|<ra>>A(g) of pure
  A, it resembles the <index|Clausius--Clapeyron equation>Clausius\UClapeyron
  equation for the same process (Eq. <reference|dln(p/po)/d(1/T)=-delHm/R><vpageref|dln(p/po)/d(1/T)=-delHm/R>).
  These equations are not exactly equivalent, however, as the comparison in
  Table <reference|tbl:12-CC vs VH><vpageref|tbl:12-CC vs VH> shows.

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-valign|top>|<cwith|2|2|1|-1|cell-vmode|exact>|<cwith|2|2|1|-1|cell-height|<plus|1fn|-2ex>>|<cwith|4|4|1|-1|cell-valign|top>|<cwith|4|4|1|-1|cell-vmode|exact>|<cwith|4|4|1|-1|cell-height|<plus|1fn|-.3cm>>|<cwith|6|6|1|-1|cell-valign|top>|<cwith|6|6|1|-1|cell-vmode|exact>|<cwith|6|6|1|-1|cell-height|<plus|1fn|-.3cm>>|<cwith|8|8|1|-1|cell-valign|top>|<cwith|8|8|1|-1|cell-vmode|exact>|<cwith|8|8|1|-1|cell-height|<plus|1fn|-.3cm>>|<cwith|10|10|1|-1|cell-valign|top>|<cwith|10|10|1|-1|cell-vmode|exact>|<cwith|10|10|1|-1|cell-height|<plus|1fn|-2ex>>|<cwith|10|10|1|-1|cell-bborder|1ln>|<table|<row|<cell|Clausius--Clapeyron
    equation>|<cell|van't Hoff equation>>|<row|<cell|>|<cell|>>|<row|<cell|<math|<D><Delsub|v*a*p>H\<approx\>-R*<frac|<D><dif>ln
    <around|(|p/p<st>|)>|<D><dif><around|(|1/T|)>>>>|<cell|<math|<D><Delsub|v*a*p>H<st>=-R*<frac|<D><dif>ln
    K|<D><dif><around|(|1/T|)>>>>>|<row|<cell|>|<cell|>>|<row|<cell|Derivation
    assumes <math|V<m><gas>\<gg\>V<m><liquid>> and ideal-gas
    behavior.>|<cell|An exact relation.>>|<row|<cell|>|<cell|>>|<row|<cell|<math|<Delsub|v*a*p>H>
    is the difference of the molar enthalpies of the real gas and the liquid
    at the saturation vapor pressure of the
    liquid.>|<cell|<math|<Delsub|v*a*p>H<st>> is the difference of the molar
    enthalpies of the ideal gas and the liquid at pressure
    <math|p<st>>.>>|<row|<cell|>|<cell|>>|<row|<cell|<math|p> is the
    saturation vapor pressure of the liquid.>|<cell|<math|K> is equal to
    <math|a<gas>/a<liquid>=<around|(|<fug>/p<st>|)>/<G><liquid>>, and is only
    approximately equal to <math|p/p<st>>.>>|<row|<cell|>|<cell|>>|<row|<cell|>|<cell|>>>>>
  </big-table|<label|tbl:12-CC vs VH>Comparison of the Clausius--Clapeyron
  and van't Hoff equations for vaporization of a liquid.>

  <section|Solvent Chemical Potentials from Phase
  Equilibria><label|12-solvent mu from phase eq>

  Section <reference|9-act coeffs from osmotic coeffs> explained how we can
  evaluate the activity coefficient <math|<g><mbB>> of a nonelectrolyte
  solute of a binary solution if we know the variation of the osmotic
  coefficient of the solution from infinite dilution to the molality of
  interest. A similar procedure for the mean ionic activity coefficient of an
  electrolyte solute was described in Sec. <reference|10-ionic act coeffs
  from osmotic coeffs>.

  The physical measurements needed to find the osmotic coefficient
  <math|\<phi\><rsub|m>> of a binary solution must be directed to the
  calculation of the quantity <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>>, the
  difference between the <I|Chemical potential!solvent@of a solvent!osmotic
  coefficient@from the osmotic coefficient\|reg>chemical potentials of the
  pure solvent and the solvent in the solution at the temperature and
  pressure of interest. This difference is positive, because the presence of
  the solute reduces the solvent's chemical potential.

  To calculate <math|\<phi\><rsub|m>> from
  <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>>, we use Eq.
  <reference|phi(m)=(muA*-muA)/RTM(A)mB><vpageref|phi(m)=(muA*-muA)/RTM(A)mB>
  for a nonelectrolyte solute, or Eq. <reference|phi(m)
  electrolyte><vpageref|phi(m) electrolyte> for an electrolyte solute. Both
  equations are represented by

  <\equation>
    <label|phi_m (general)>\<phi\><rsub|m>=<frac|\<mu\><A><rsup|\<ast\>>-\<mu\><A>|R*T*M<A>\<nu\>*m<B>>
  </equation>

  where <math|\<nu\>> for a nonelectrolyte is <math|1> and for an electrolyte
  is the number of ions per formula unit.

  The sequence of steps, then, is (1) the determination of
  <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>> over a range of molality at
  constant <math|T> and <math|p>, (2) the conversion of these values to
  <math|\<phi\><rsub|m>> using Eq. <reference|phi_m (general)>, and (3) the
  evaluation of the solute activity coefficient<footnote|A measurement of
  <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>> also gives us the <I|Activity
  coefficient!solvent@of a solvent\|n><subindex|Solvent|activity coefficient
  of><em|solvent> activity coefficient, based on the pure-solvent reference
  state, through the relation <math|\<mu\><A>=\<mu\><A><rsup|\<ast\>>+R*T*ln
  <around|(|<g><A>x<A>|)>> (Eq. <reference|act coeff, solvent>).> by a
  suitable integration from infinite dilution to the molality of interest.

  Sections <reference|12-freezing-point measurements> and
  <reference|12-osmotic p measurements> will describe freezing-point and
  osmotic-pressure measurements, two much-used methods for evaluating
  <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>> in a binary solution at a given
  <math|T> and <math|p>. The isopiestic vapor-pressure method was described
  in Sec. <reference|9-fugacity measurements>. The freezing-point and
  isopiestic vapor-pressure methods are often used for electrolyte solutions,
  and osmotic pressure is especially useful for solutions of macromolecules.

  <subsection|Freezing-point measurements><label|12-freezing-point
  measurements>

  <I|Freezing point!evaluate solvent chemical potential@to evaluate solvent
  chemical potential\|(><I|Chemical potential!solvent@of a solvent!freezing
  point@from the freezing point\|(>This section explains how we can evaluate
  <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>> for a solution of a given
  composition at a given <math|T> and <math|p> from the freezing point of the
  solution combined with additional data obtained from calorimetric
  measurements.

  Consider a binary solution of solvent A and solute B. We assume that when
  this solution is cooled at constant pressure and composition, the solid
  that first appears is pure A. For example, for a dilute aqueous solution
  the solid would be ice. The temperature at which solid A first appears is
  <math|T<f>>, the freezing point of the solution. This temperature is lower
  than the freezing point <math|T<f><rsup|\<ast\>>> of the pure solvent, a
  consequence of the lowering of <math|\<mu\><A>> by the presence of the
  solute. Both <math|T<f>> and <math|T<f><rsup|\<ast\>>> can be measured
  experimentally.

  Let <math|T<rprime|'>> be a temperature of interest that is equal to or
  greater than <math|T<f><rsup|\<ast\>>>. We wish to determine the value of
  <math|\<mu\><A><rsup|\<ast\>><around|(|<tx|l>,T<rprime|'>|)>-\<mu\><A><around|(|<tx|s*l*n>,T<rprime|'>|)>>,
  where <math|\<mu\><A><rsup|\<ast\>><around|(|<tx|l>,T<rprime|'>|)>> refers
  to pure liquid solvent and <math|\<mu\><A><around|(|<tx|s*l*n>,T<rprime|'>|)>>
  refers to the solution.

  Figure <reference|fig:12-freezing pt><vpageref|fig:12-freezing pt>

  <\big-figure>
    <boxedfigure|<image|./12-SUP/fpt.eps||||> <capt|Integration path abcde at
    constant pressure for determining <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>>
    at temperature <math|T<rprime|'>> from the freezing point <math|T<f>> of
    a solution (schematic). The dashed extensions of the curves represent
    unstable states.<label|fig:12-freezing pt>>>
  </big-figure|>

  explains the principle of the procedure. The figure shows
  <math|\<mu\><A>/T> for the solvent in the pure solid phase, in the pure
  liquid phase, and in the fixed-composition solution, plotted as functions
  of <math|T> at constant <math|p>. Since <math|\<mu\><A>> is the same in the
  solution and solid phases at temperature <math|T<f>>, and is the same in
  the pure liquid and solid phases at temperature <math|T<f><rsup|\<ast\>>>,
  the curves intersect at these temperatures as shown.

  Formulas for the slopes of the three curves, from Eq.
  <reference|d(mu_i/T)/dT=-H_i/T^2><vpageref|d(mu<rsub|i>/T)/dT=-H<rsub|i>/T<rsup|2>>,
  are included in the figure. The desired value of
  <math|\<mu\><A><rsup|\<ast\>><around|(|<tx|l>,T<rprime|'>|)>-\<mu\><A><around|(|<tx|s*l*n>,T<rprime|'>|)>>
  is the product of <math|T<rprime|'>> and the difference of the values of
  <math|\<mu\><A>/T> at points e and a. To find this difference, we integrate
  the slope <math|<dif><around|(|\<mu\><A>/T|)>/<dif>T> over <math|T> along
  the path abcde:

  <\equation>
    <label|slope diff>

    <\eqsplit>
      <tformat|<table|<row|<cell|<frac|\<mu\><A><rsup|\<ast\>><around|(|<tx|l>,T<rprime|'>|)>|T<rprime|'>>-<frac|\<mu\><A><around|(|<tx|s*l*n>,T<rprime|'>|)>|T<rprime|'>>>|<cell|=-<big|int><rsub|T<rprime|'>><rsup|T<f><rsup|\<ast\>>><frac|H<A><tx|<around|(|s*l*n|)>>|T<rsup|2>><dif>T-<big|int><rsub|T<f><rsup|\<ast\>>><rsup|T<f>><frac|H<A><tx|<around|(|s*l*n|)>>|T<rsup|2>><dif>T>>|<row|<cell|>|<cell|<space|1em>-<big|int><rsub|T<f>><rsup|T<f><rsup|\<ast\>>><frac|H<A><rsup|\<ast\>><solid>|T<rsup|2>><dif>T-<big|int><rsub|T<f><rsup|\<ast\>>><rsup|T<rprime|'>><frac|H<A><rsup|\<ast\>><liquid>|T<rsup|2>><dif>T>>>>
    </eqsplit>
  </equation>

  By combining integrals that have the same range of integration, we turn Eq.
  <reference|slope diff> into

  <\equation>
    <label|muA*/T'-muA/T'=>

    <\eqsplit>
      <tformat|<table|<row|<cell|<frac|\<mu\><A><rsup|\<ast\>><around|(|<tx|l>,T<rprime|'>|)>|T<rprime|'>>-<frac|\<mu\><A><around|(|<tx|s*l*n>,T<rprime|'>|)>|T<rprime|'>>>|<cell|=<big|int><rsub|T<f>><rsup|T<f><rsup|\<ast\>>><frac|H<A><tx|<around|(|s*l*n|)>>-H<A><rsup|\<ast\>><solid>|T<rsup|2>><dif>T>>|<row|<cell|>|<cell|<space|1em>+<big|int><rsub|T<f><rsup|\<ast\>>><rsup|T<rprime|'>><frac|H<A><tx|<around|(|s*l*n|)>>-H<A><rsup|\<ast\>><liquid>|T<rsup|2>><dif>T>>>>
    </eqsplit>
  </equation>

  For convenience of notation, this book will use <math|<Delsub|s*o*l,A>H> to
  denote the molar enthalpy difference <math|H<A><tx|<around|(|s*l*n|)>>-H<A><rsup|\<ast\>><solid>>.
  <math|<Delsub|s*o*l,A>H> is the molar differential enthalpy of solution of
  solid A in the solution at constant <math|T> and <math|p>. The first
  integral on the right side of Eq. <reference|muA*/T'-muA/T'=> requires
  knowledge of <math|<Delsub|s*o*l,A>H> over a temperature range, but the
  only temperature at which it is practical to measure this quantity
  calorimetrically is at the equilibrium transition temperature <math|T<f>>.
  It is usually sufficient to assume <math|<Delsub|s*o*l,A>H> is a linear
  function of <math|T>:

  <\equation>
    <label|del(sol)H=del(sol)C(T-Tf)><Delsub|s*o*l,A>H<around|(|T|)>=<Delsub|s*o*l,A>H<around|(|T<f>|)>+<Delsub|s*o*l,A>C<rsub|p>*<around|(|T-T<f>|)>
  </equation>

  The molar differential heat capacity of solution
  <math|<Delsub|s*o*l,A>C<rsub|p>=C<rsub|p,<tx|A>><tx|<around|(|s*l*n|)>>-C<rsub|p,<tx|A>><solid>>
  is treated as a constant that can be determined from calorimetric
  measurements.

  The quantity <math|H<A><tx|<around|(|s*l*n|)>>-H<A><rsup|\<ast\>><liquid>>
  in the second integral on the right side of Eq. <reference|muA*/T'-muA/T'=>
  is the molar differential enthalpy of dilution of the solvent in the
  solution, <math|<Delsub|d*i*l>H> (see Eq. <reference|del(dil)Hm=HA-HmA*>).
  This quantity can be measured calorimetrically at any temperature higher
  than <math|T<f><rsup|\<ast\>>>. Making this substitution in Eq.
  <reference|muA*/T'-muA/T'=> together with that of Eq.
  <reference|del(sol)H=del(sol)C(T-Tf)>, carrying out the integration of the
  first integral and rearranging, we obtain finally

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|\<mu\><A><rsup|\<ast\>><around|(|<tx|l>,T<rprime|'>|)>-\<mu\><A><around|(|<tx|s*l*n>,T<rprime|'>|)>>|<cell|=T<rprime|'>*<around*|[|<Delsub|s*o*l,A>H<around|(|T<f>|)>-T<f><Delsub|s*o*l,A>C<rsub|p>|]>*<around*|(|<frac|1|T<f>>-<frac|1|T<f><rsup|\<ast\>>>|)>>>|<row|<cell|>|<cell|<space|1em>+T<rprime|'><Delsub|s*o*l,A>C<rsub|p>*ln
      <frac|T<f><rsup|\<ast\>>|T<f>>+T<rprime|'>*<space|-0.17em><big|int><rsub|T<f><rsup|\<ast\>>><rsup|T<rprime|'>><frac|<Delsub|d*i*l>H|T<rsup|2>><dif>T>>>>
    </eqsplit>
  </equation>

  <I|Freezing point!evaluate solvent chemical potential@to evaluate solvent
  chemical potential\|)><I|Chemical potential!solvent@of a solvent!freezing
  point@from the freezing point\|)>

  <subsection|Osmotic-pressure measurements><label|12-osmotic p measurements>

  <I|Osmotic pressure!evaluate solvent chemical potential@to evaluate solvent
  chemical potential\|(><I|Chemical potential!solvent@of a solvent!osmotic
  pressure@from osmotic pressure\|(>A second method for evaluating
  <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>> uses the solution property called
  <em|osmotic pressure>. A simple apparatus to measure the osmotic pressure
  of a binary solution is shown schematically in Fig.
  <reference|fig:12-osmotic pressure>.

  <\big-figure>
    <boxedfigure|<image|./12-SUP/osmotic.eps||||> <capt|Apparatus to measure
    osmotic pressure (schematic). The dashed line represents a membrane
    permeable only to the solvent A. The cross-hatched rectangles represent
    moveable pistons.<label|fig:12-osmotic pressure>>>
  </big-figure|>

  The system consists of two liquid phases separated by a <index|Membrane,
  semipermeable>semipermeable membrane. Phase <math|<pha>> is pure solvent
  and phase <math|<phb>> is a solution with the same solvent at the same
  temperature. The semipermeable membrane is permeable to the solvent and
  impermeable to the solute.

  The presence of the membrane makes this system different from the
  multiphase, multicomponent system of Sec. <reference|9-eqm conditions>,
  used there to derive conditions for transfer equilibrium. By a modification
  of that procedure, we can derive the conditions of equilibrium for the
  present system. We take phase <math|<phb>> as the reference phase because
  it includes both solvent and solute. In order to prevent expansion work in
  the isolated system, both pistons shown in the figure must be fixed in
  stationary positions. This keeps the volume of each phase constant:
  <math|<dif>V<aph>=<dif>V<bph>=0>. Equation <reference|dS=sum(alpha' ne
  alpha)...><vpageref|dS=sum(alpha' ne alpha)...>, expressing the total
  differential of the entropy in an isolated multiphase, multicomponent
  system, becomes

  <\equation>
    <dif>S=<frac|T<bph>-T<aph>|T<bph>><dif>S<aph>+<frac|\<mu\><A><bph>-\<mu\><A><aph>|T<bph>><dif>n<A><aph>
  </equation>

  In an equilibrium state, the coefficients
  <math|<around|(|T<bph>-T<aph>|)>/T<bph>> and
  <math|<around|(|\<mu\><A><bph>-\<mu\><A><aph>|)>/T<bph>> must be zero.
  Therefore, in an equilibrium state the temperature is the same in both
  phases and the solvent has the same chemical potential in both phases. The
  presence of the membrane, however, allows the pressures of the two phases
  to be unequal in the equilibrium state.

  Suppose we start with both phases shown in Fig. <reference|fig:12-osmotic
  pressure> at the same temperature and pressure. Under these conditions, the
  value of <math|\<mu\><A>> is less in the solution than in the pure liquid,
  and a spontaneous flow of solvent will occur through the membrane from the
  pure solvent to the solution. This phenomenon is called
  <index|Osmosis><em|osmosis>.<footnote|Greek for <em|push>.> If we move the
  right-hand piston down slightly in order to increase the pressure
  <math|p<rprime|''>> of the solution in phase <math|<phb>>, <math|\<mu\><A>>
  increases in this phase. The <index|Osmotic pressure><newterm|osmotic
  pressure> of the solution, <math|<varPi>>, is defined as the additional
  pressure the solution must have, compared to the pressure
  <math|p<rprime|'>> of the pure solvent at the same temperature, to
  establish an equilibrium state with no flow of solvent in either direction
  through the membrane: <math|p<rprime|''>=p<rprime|'>+<varPi>>.

  <\quote-env>
    <label|not completely impermeable>In practice, the membrane may not be
    completely impermeable to a solute. All that is required for the
    establishment of an equilibrium state with different pressures on either
    side of the membrane is that solvent transfer equilibrium be established
    on a short time scale compared to the period of observation, and that the
    amount of solute transferred during this period be negligible.
  </quote-env>

  The osmotic pressure <math|<varPi>> is an intensive property of a solution
  whose value depends on the solution's temperature, pressure, and
  composition. Strictly speaking, <math|<varPi>> in an equilibrium state of
  the system shown in Fig. <reference|fig:12-osmotic pressure> refers to the
  osmotic pressure of the solution at pressure <math|p<rprime|'>>, the
  pressure of the pure solvent.<label|osmotic pressure defn>In other words,
  the osmotic pressure of a solution at temperature <math|T> and pressure
  <math|p<rprime|'>> is the additional pressure that would have to be exerted
  on the solution to establish transfer equilibrium with pure solvent that
  has temperature <math|T> and pressure <math|p<rprime|'>>. A solution has
  the property called osmotic pressure regardless of whether this additional
  pressure is actually present, just as a solution has a freezing point even
  when its actual temperature is different from the freezing point.

  Because in an equilibrium state the solvent chemical potential must be the
  same on both sides of the semipermeable membrane, there is a relation
  between chemical potentials and osmotic pressure given by

  <\gather>
    <tformat|<table|<row|<cell|\<mu\><A><around|(|p<rprime|''>|)>=\<mu\><A><around|(|p<rprime|'>+<varPi>|)>=\<mu\><A><rsup|\<ast\>><around|(|p<rprime|'>|)><cond|<around|(|e*q*u*i*l*i*b*r*i*u*m*s*t*a*t*e|)>><eq-number><label|muA(p+Pi)=muA*(p)>>>>>
  </gather>

  We can use this relation to derive an expression for
  <math|\<mu\><A><rsup|\<ast\>><around|(|p<rprime|'>|)>-\<mu\><A><around|(|p<rprime|'>|)>>
  as a function of <math|<varPi>>. The dependence of <math|\<mu\><A>> on
  pressure is given according to Eq. <reference|d(mu_i)/dp=V_i> by

  <\equation>
    <Pd|\<mu\><A>|p|T,<allni>>=V<A>
  </equation>

  where <math|V<A>> is the partial molar volume of the solvent in the
  solution. Rewriting this equation in the form
  <math|<dif>\<mu\><A>=V<A><difp>> and integrating at constant temperature
  and composition from <math|p<rprime|'>> to <math|p<rprime|'>+<varPi>>, we
  obtain

  <\equation>
    \<mu\><A><around|(|p<rprime|'>+<varPi>|)>-\<mu\><A><around|(|p<rprime|'>|)>=<big|int><rsub|p<rprime|'>><rsup|p<rprime|'>+<varPi>><space|-0.17em>V<A><difp>
  </equation>

  Substitution from Eq. <reference|muA(p+Pi)=muA*(p)> changes this to

  <\gather>
    <tformat|<table|<row|<cell|\<mu\><A><rsup|\<ast\>><around|(|p<rprime|'>|)>-\<mu\><A><around|(|p<rprime|'>|)>=<big|int><rsub|p<rprime|'>><rsup|p<rprime|'>+<varPi>><space|-0.17em>V<A><difp><cond|<around|(|c*o*n*s*t*a*n*t<math|T>|)>><eq-number><label|muA*-muA=int(VA)dp>>>>>
  </gather>

  which is the desired expression for <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>>
  at a single temperature and pressure. To evaluate the integral, we need an
  experimental value of the osmotic pressure <math|<varPi>> of the solution.
  If we assume <math|V<A>> is constant in the pressure range from
  <math|p<rprime|'>> to <math|p<rprime|'>+<varPi>>, Eq.
  <reference|muA*-muA=int(VA)dp> becomes simply

  <\equation>
    <label|muA*-muA=VA Pi>\<mu\><A><rsup|\<ast\>><around|(|p<rprime|'>|)>-\<mu\><A><around|(|p<rprime|'>|)>=V<A><varPi>
  </equation>

  <I|Osmotic pressure!evaluate solvent chemical potential@to evaluate solvent
  chemical potential\|)><I|Chemical potential!solvent@of a solvent!osmotic
  pressure@from osmotic pressure\|)>

  <section|Binary Mixture in Equilibrium with a Pure Phase><label|12-binary
  mixt in eqm with pure phase>

  <I|Binary mixture!equilibrium with a pure phase@in equilibrium with a pure
  phase\|reg>This section considers a binary liquid mixture of components A
  and B in equilibrium with either pure solid A or pure gaseous A. The aim is
  to find general relations among changes of temperature, pressure, and
  mixture composition in the two-phase equilibrium system that can be applied
  to specific situations in later sections.

  In this section, <math|\<mu\><A>> is the chemical potential of component A
  in the mixture and <math|\<mu\><A><rsup|\<ast\>>> is for the pure solid or
  gaseous phase. We begin by writing the total differential of
  <math|\<mu\><A>/T> with <math|T>, <math|p>, and <math|x<A>> as the
  independent variables. These quantities refer to the binary liquid mixture,
  and we have not yet imposed a condition of equilibrium with another phase.
  The general expression for the total differential is

  <\equation>
    <dif><around|(|\<mu\><A>/T|)>=<bPd|<around|(|\<mu\><A>/T|)>|T|p,x<A>><space|-0.17em><dif>T+<bPd|<around|(|\<mu\><A>/T|)>|p|T,x<A>><space|-0.17em><difp>+<bPd|<around|(|\<mu\><A>/T|)>|x<A>|T,p><space|-0.17em><dx><A>
  </equation>

  With substitutions from Eqs. <reference|d(mu_i)/dp=V_i> and
  <reference|d(mu_i/T)/dT=-H_i/T^2>, this becomes

  <\equation>
    <label|dmuA/T=><dif><around|(|\<mu\><A>/T|)>=-<frac|H<A>|T<rsup|2>><dif>T+<frac|V<A>|T><difp>+<bPd|<around|(|\<mu\><A>/T|)>|x<A>|T,p><dx><A>
  </equation>

  Next we write the total differential of <math|\<mu\><A><rsup|\<ast\>>/T>
  for pure solid or gaseous A. The independent variables are <math|T> and
  <math|p>; the expression is like Eq. <reference|dmuA/T=> with the last term
  missing:

  <\equation>
    <label|dmuA*/T=><dif><around|(|\<mu\><A><rsup|\<ast\>>/T|)>=-<frac|H<A><rsup|\<ast\>>|T<rsup|2>><dif>T+<frac|V<A><rsup|\<ast\>>|T><difp>
  </equation>

  When the two phases are in transfer equilibrium, <math|\<mu\><A>> and
  <math|\<mu\><A><rsup|\<ast\>>> are equal. If changes occur in <math|T>,
  <math|p>, or <math|x<A>> while the phases remain in equilibrium, the
  condition <math|<dif><around|(|\<mu\><A>/T|)>=<dif><around|(|\<mu\><A><rsup|\<ast\>>/T|)>>
  must be satisfied. Equating the expressions on the right sides of Eqs.
  <reference|dmuA/T=> and <reference|dmuA*/T=> and combining terms, we obtain
  the equation

  <\equation>
    <frac|H<A>-H<A><rsup|\<ast\>>|T<rsup|2>><dif>T-<frac|V<A>-V<A><rsup|\<ast\>>|T><difp>=<bPd|<around|(|\<mu\><A>/T|)>|x<A>|T,p><dx><A>
  </equation>

  which we can rewrite as

  <\gather>
    <tformat|<table|<row|<cell|<frac|<Delsub|s*o*l,A>H|T<rsup|2>><dif>T-<frac|<Delsub|s*o*l,A>V|T><difp>=<bPd|<around|(|\<mu\><A>/T|)>|x<A>|T,p><dx><A><cond|(p*h*a*s*e*s*i*n><nextcond|e*q*u*i*l*i*b*r*i*u*m)><eq-number><label|-del(sol)HmA/T^2
    dT=>>>>>
  </gather>

  Here <math|<Delsub|s*o*l,A>H> is the molar differential enthalpy of
  solution of solid or gaseous A in the liquid mixture, and
  <math|<Delsub|s*o*l,A>V> is the molar differential volume of solution.
  Equation <reference|-del(sol)HmA/T^2 dT=> is a relation between changes in
  the variables <math|T>, <math|p>, and <math|x<A>>, only two of which are
  independent in the equilibrium system.

  Suppose we set <math|<difp>> equal to zero in Eq.
  <reference|-del(sol)HmA/T^2 dT=> and solve for <math|<dif>T/<dx><A>>. This
  gives us the rate at which <math|T> changes with <math|x<A>> at constant
  <math|p>:

  <\gather>
    <tformat|<table|<row|<cell|<Pd|T|x<A>|<space|-0.17em>p>=<frac|T<rsup|2>|<Delsub|s*o*l,A>H><bPd|<around|(|\<mu\><A>/T|)>|x<A>|T,p><cond|(p*h*a*s*e*s*i*n><nextcond|e*q*u*i*l*i*b*r*i*u*m)><eq-number><label|dT/dxA
    l-solid eqm>>>>>
  </gather>

  We can also set <math|<dif>T> equal to zero in Eq.
  <reference|-del(sol)HmA/T^2 dT=> and find the rate at which <math|p>
  changes with <math|x<A>> at constant <math|T>:

  <\gather>
    <tformat|<table|<row|<cell|<Pd|p|x<A>|T>=-<frac|T|<Delsub|s*o*l,A>V><bPd|<around|(|\<mu\><A>/T|)>|x<A>|T,p><cond|(p*h*a*s*e*s*i*n><nextcond|e*q*u*i*l*i*b*r*i*u*m)><eq-number><label|dp/dxA
    l-solid eqm>>>>>
  </gather>

  Equations <reference|dT/dxA l-solid eqm> and <reference|dp/dxA l-solid eqm>
  will be needed in Secs. <reference|12-colligative properties> and
  <reference|12-solid l eqm>.

  <section|Colligative Properties of a Dilute Solution><label|12-colligative
  properties>

  The <index|Colligative property><newterm|colligative properties> of a
  solution are usually considered to be:

  <\enumerate>
    <label|colligative properties>

    <item><I|Freezing point!depression in a solution\|(><em|Freezing-point
    depression>: the decrease in the freezing point of the solution, compared
    to pure solvent at the same pressure.

    <item><subindex|Boiling point|elevation in a solution><em|Boiling-point
    elevation>: the increase in the boiling point of a solution containing
    nonvolatile solutes, compared to pure solvent at the same pressure.

    <item><subindex|Vapor pressure|lowering in a solution><em|Vapor-pressure
    lowering>: the decrease in the vapor pressure of a solution containing
    nonvolatile solutes, compared to the vapor pressure of the pure solvent
    at the same temperature.

    <item><index|Osmotic pressure><em|Osmotic pressure>: the increase in the
    pressure of the solution that places the solvent in transfer equilibrium
    with pure solvent at the same temperature and pressure as the original
    solution (page <pageref|osmotic pressure defn>).
  </enumerate>

  Note that all four properties are defined by an equilibrium between the
  liquid solution and a solid, liquid, or gas phase of the pure solvent. The
  properties called colligative (Latin: <em|tied together>) have in common a
  dependence on the concentration of solute particles that affects the
  solvent chemical potential.

  Figure <reference|fig:12-ideal-dil aq soln><vpageref|fig:12-ideal-dil aq
  soln> illustrates the freezing-point depression and boiling-point elevation
  of an aqueous solution.

  <\big-figure>
    <boxedfigure|<image|./12-SUP/sln-mu-T.eps||||> <capt|Freezing-point
    depression and boiling-point elevation of an aqueous solution. Solid
    curves: dependence on temperature of the chemical potential of
    H<rsub|<math|2>>O (A) in pure phases and in an aqueous solution at
    <math|1<br>>. Dashed curves: unstable states. The <math|\<mu\><A>> values
    have an arbitrary zero. The solution curve is calculated for an
    ideal-dilute solution of composition <math|x<A>=0.9>.<label|fig:12-ideal-dil
    aq soln>>>
  </big-figure|>

  At a fixed pressure, pure liquid water is in equilibrium with ice at the
  freezing point and with steam at the boiling point. These are the
  temperatures at which H<rsub|<math|2>>O has the same chemical potential in
  both phases at this pressure. At these temperatures, the chemical potential
  curves for the phases intersect, as indicated by open circles in the
  figure. The presence of dissolved solute in the solution causes a lowering
  of the H<rsub|<math|2>>O chemical potential compared to pure water at the
  same temperature. Consequently, the curve for the chemical potential of
  H<rsub|<math|2>>O in the solution intersects the curve for ice at a lower
  temperature, and the curve for steam at a higher temperature, as indicated
  by open triangles. The freezing point is depressed by
  <math|<Del>T<rsub|<text|f>>>, and the boiling point (if the solute is
  nonvolatile) is elevated by <math|<Del>T<bd>>.

  Sections <reference|12-freezing-point depression>\U<reference|12-osmotic
  pressure> will derive theoretical relations between each of the four
  colligative properties and solute composition variables in the limit of
  infinite dilution. The expressions show that the colligative properties of
  a dilute binary solution depend on properties of the solvent, are
  proportional to the solute concentration and molality, but do not depend on
  the kind of solute.

  Although these expressions provide no information about the activity
  coefficient of a solute, they are useful for estimating the solute molar
  mass. <I|Molar!mass!colligative property@from a colligative
  property\|reg><I|Colligative property!estimate solute molar mass@to
  estimate solute molar mass\|reg>For example, from a measurement of any of
  the colligative properties of a dilute solution and the appropriate
  theoretical relation, we can obtain an approximate value of the solute
  molality <math|m<B>>. (It is only approximate because, for a measurement of
  reasonable precision, the solution cannot be extremely dilute.) If we
  prepare the solution with a known amount <math|n<A>> of solvent and a known
  mass of solute, we can calculate the amount of solute from
  <math|n<B>=n<A>M<A>m<B>>; then the solute molar mass is the solute mass
  divided by <math|n<B>>.

  <subsection|Freezing-point depression><label|12-freezing-point depression>

  As in Sec. <reference|12-freezing-point measurements>, we assume the solid
  that forms when a dilute solution is cooled to its freezing point is pure
  component A.

  Equation <reference|dT/dxA l-solid eqm><vpageref|dT/dxA l-solid eqm> gives
  the general dependence of temperature on the composition of a binary liquid
  mixture of A and B that is in equilibrium with pure solid A. We treat the
  mixture as a solution. The solvent is component A, the solute is B, and the
  temperature is the freezing point <math|T<f>>:

  <\equation>
    <label|dT(f)/dxA=><Pd|T<f>|x<A>|<space|-0.17em>p>=<frac|T<f><rsup|2>|<Delsub|s*o*l,A>H><bPd|<around|(|\<mu\><A>/T|)>|x<A>|T,p>
  </equation>

  Consider the expression on the right side of this equation in the limit of
  infinite dilution. In this limit, <math|T<f>> becomes
  <math|T<f><rsup|\<ast\>>>, the freezing point of the pure solvent, and
  <math|<Delsub|s*o*l,A>H> becomes <math|<Delsub|f*u*s,A>H>, the molar
  enthalpy of fusion of the pure solvent.

  To deal with the partial derivative on the right side of Eq.
  <reference|dT(f)/dxA=> in the limit of infinite dilution, we use the fact
  that the solvent activity coefficient <math|<g><A>> approaches <math|1> in
  this limit. Then the solvent chemical potential is given by the Raoult's
  law relation

  <\gather>
    <tformat|<table|<row|<cell|\<mu\><A>=\<mu\><A><rsup|\<ast\>>+R*T*ln
    x<A><cond|<around|(|s*o*l*u*t*i*o*n*a*t*i*n*f*i*n*i*t*e*d*i*l*u*t*i*o*n|)>><eq-number><label|muA(sln)=muA*(l)+RTln(xA)>>>>>
  </gather>

  where <math|\<mu\><A><rsup|\<ast\>>> is the chemical potential of A in a
  pure-liquid reference state at the same <math|T> and <math|p> as the
  mixture.<footnote|At the freezing point of the mixture, the reference state
  is an unstable supercooled liquid.>

  If the solute is an electrolyte, Eq. <reference|muA(sln)=muA*(l)+RTln(xA)>
  can be derived by the same procedure as described in Sec.
  <reference|9-Solvent in ideal-dilute soln> for an ideal-dilute binary
  solution of a nonelectrolyte. We must calculate <math|x<A>> from the
  amounts of all species present at infinite dilution. In the limit of
  infinite dilution, any electrolyte solute is completely dissociated to its
  constituent ions: ion pairs and weak electrolytes are completely
  dissociated in this limit. Thus, for a binary solution of electrolyte B
  with <math|\<nu\>> ions per formula unit, we should calculate <math|x<A>>
  from

  <\equation>
    <label|xA=nA/(nA+nu*nB)>x<A>=<frac|n<A>|n<A>+\<nu\>*n<B>>
  </equation>

  where <math|n<B>> is the amount of solute formula unit. (If the solute is a
  nonelectrolyte, we simply set <math|\<nu\>> equal to <math|1> in this
  equation.)

  From Eq. <reference|muA(sln)=muA*(l)+RTln(xA)>, we can write

  <\equation>
    <label|d(mu/T)/dxA-\<gtr\>R><bPd|<around|(|\<mu\><A>/T|)>|x<A>|T,p><ra>R<space|1em><tx|a*s><space|1em>x<A><ra>1
  </equation>

  In the limit of infinite dilution, then, Eq. <reference|dT(f)/dxA=> becomes

  <\equation>
    <label|dT(f)/dxA=RT2/del(fus)Hma>lim<rsub|x<A>\<rightarrow\>1><Pd|T<f>|x<A>|<space|-0.17em>p>=<frac|R<around|(|T<f><rsup|\<ast\>>|)><rsup|2>|<Delsub|f*u*s,A>H>
  </equation>

  It is customary to relate freezing-point depression to the solute
  concentration <math|c<B>> or molality <math|m<B>>. From Eq.
  <reference|xA=nA/(nA+nu*nB)>, we obtain

  <\equation>
    1-x<A>=<frac|\<nu\>*n<B>|n<A>+\<nu\>*n<B>>
  </equation>

  In the limit of infinite dilution, when <math|\<nu\>*n<B>> is much smaller
  than <math|n<A>>, <math|1-x<A>> approaches the value
  <math|\<nu\>*n<B>/n<A>>. Then, using expressions in Eq. <reference|nB/nA
  (dilute)><vpageref|nB/nA (dilute)>, we obtain the relations

  <\gather>
    <tformat|<table|<row|<\cell>
      \;

      <\s>
        <\eqsplit>
          <tformat|<table|<row|<cell|<dx><A>>|<cell|=-<dif><around|(|1-x<A>|)>=-\<nu\><dif><around|(|n<B>/n<A>|)>>>|<row|<cell|>|<cell|=-\<nu\>*V<A><rsup|\<ast\>><dif>c<B>>>|<row|<cell|>|<cell|=-\<nu\>*M<A><dif>m<B>>>>>
        </eqsplit>
      </s>

      <cond|(b*i*n*a*r*y*s*o*l*u*t*i*o*n*a*t><nextcond|i*n*f*i*n*i*t*e*d*i*l*u*t*i*o*n)>

      <eq-number><label|dxA, inf diln>
    </cell>>>>
  </gather>

  which transform Eq. <reference|dT(f)/dxA=RT2/del(fus)Hma> into the
  following:<footnote|A small dependence of <math|V<A><rsup|\<ast\>>> on
  <math|T> has been ignored.>

  <\gather>
    <tformat|<table|<row|<cell|lim<rsub|c<B>\<rightarrow\>0><Pd|T<f>|c<B>|<space|-0.17em>p>=-<frac|\<nu\>*V<A><rsup|\<ast\>>R<around|(|T<f><rsup|\<ast\>>|)><rsup|2>|<Delsub|f*u*s,A>H><eq-number>>>|<row|<cell|lim<rsub|m<B>\<rightarrow\>0><Pd|T<f>|m<B>|<space|-0.17em>p>=-<frac|\<nu\>*M<A>R<around|(|T<f><rsup|\<ast\>>|)><rsup|2>|<Delsub|f*u*s,A>H><eq-number><label|lim(Tf/mB)=>>>>>
  </gather>

  We can apply these equations to a nonelectrolyte solute by setting
  <math|\<nu\>> equal to <math|1>.

  As <math|c<B>> or <math|m<B>> approaches zero, <math|T<f>> approaches
  <math|T<f><rsup|\<ast\>>>. The freezing-point depression (a negative
  quantity) is <math|<Del>T<rsub|<text|f>>=T<f>-T<f><rsup|\<ast\>>>. In the
  range of molalities of a dilute solution in which <math|<pd|T<f>|m<B>|p>>
  is given by the expression on the right side of Eq.
  <reference|lim(Tf/mB)=>, we can write

  <\equation>
    <label|del T(f) =><Del>T<rsub|<text|f>>=-<frac|\<nu\>*M<A>R<around|(|T<f><rsup|\<ast\>>|)><rsup|2>|<Delsub|f*u*s,A>H>*m<B>
  </equation>

  The <index|Molal freezing-point depression constant><newterm|molal
  freezing-point depression constant> or <index|Cryoscopic
  constant>cryoscopic constant, <math|K<rsub|<text|f>>>, is defined for a
  binary solution by

  <\equation>
    K<rsub|<text|f>><defn>-lim<rsub|m<B>\<rightarrow\>0>
    <frac|<Del>T<rsub|<text|f>>|\<nu\>*m<B>>
  </equation>

  and, from Eq. <reference|del T(f) =>, has a value given by

  <\equation>
    K<rsub|<text|f>>=<frac|M<A>R<around|(|T<f><rsup|\<ast\>>|)><rsup|2>|<Delsub|f*u*s,A>H>
  </equation>

  The value of <math|K<rsub|<text|f>>> calculated from this formula depends
  only on the kind of solvent and the pressure. For H<rsub|<math|2>>O at
  <math|1<br>>, the calculated value is <math|K<bd>=1.860<units|K*<space|0.17em>k*g*<space|0.17em>m*o*l<per>>>
  (Prob. 12.<reference|prb:12-Kf Kb>).

  In the dilute binary solution, we have the relation

  <\gather>
    <tformat|<table|<row|<cell|<Del>T<rsub|<text|f>>=-\<nu\>*K<rsub|<text|f>><space|0.17em>m<B><cond|<around|(|d*i*l*u*t*e*b*i*n*a*r*y*s*o*l*u*t*i*o*n|)>><eq-number><label|del(Tf)
    = -nu Kf mB>>>>>
  </gather>

  This relation is useful for estimating the molality of a dilute
  nonelectrolyte solution (<math|\<nu\>=1>) from a measurement of the
  freezing point. The relation is of little utility for an electrolyte
  solute, because at any electrolyte molality that is high enough to give a
  measurable depression of the freezing point, the mean ionic activity
  coefficient deviates greatly from unity and the relation is not accurate.
  <I|Freezing point!depression in a solution\|)>

  <input|./bio/raoult>

  <subsection|Boiling-point elevation><label|12-boiling-point elevation>

  <I|Boiling point!elevation in a solution\|(>We can apply Eq.
  <reference|dT/dxA l-solid eqm> to the boiling point <math|T<bd>> of a
  dilute binary solution. The pure phase of A in equilibrium with the
  solution is now a gas instead of a solid.<footnote|We must assume the
  solute is nonvolatile or has negligible partial pressure in the gas phase.>
  Following the procedure of Sec. <reference|12-freezing-point depression>,
  we obtain

  <\equation>
    lim<rsub|m<B>\<rightarrow\>0><Pd|T<bd>|m<B>|<space|-0.17em>p>=<frac|\<nu\>*M<A>R<around|(|T<bd><rsup|\<ast\>>|)><rsup|2>|<Delsub|v*a*p,A>H>
  </equation>

  where <math|<Delsub|v*a*p,A>H> is the molar enthalpy of vaporization of
  pure solvent at its boiling point <math|T<bd><rsup|\<ast\>>>.

  The <index|Molal boiling-point elevation constant><newterm|molal
  boiling-point elevation constant> or <index|Ebullioscopic
  constant>ebullioscopic constant, <math|K<bd>>, is defined for a binary
  solution by

  <\equation>
    K<bd><defn>lim<rsub|m<B>\<rightarrow\>0> <frac|<Del>T<bd>|\<nu\>*m<B>>
  </equation>

  where <math|<Del>T<bd>=T<bd>-T<bd><rsup|\<ast\>>> is the boiling-point
  elevation. Accordingly, <math|K<bd>> has a value given by

  <\equation>
    K<bd>=<frac|M<A>R<around|(|T<bd><rsup|\<ast\>>|)><rsup|2>|<Delsub|v*a*p,A>H>
  </equation>

  For the boiling point of a dilute solution, the analogy of Eq.
  <reference|del(Tf) = -nu Kf mB> is

  <\gather>
    <tformat|<table|<row|<cell|<Del>T<bd>=\<nu\>*K<bd><space|0.17em>m<B><cond|<around|(|d*i*l*u*t*e*b*i*n*a*r*y*s*o*l*u*t*i*o*n|)>><eq-number><label|del(Tb)
    = Kb mB>>>>>
  </gather>

  Since <math|K<rsub|<text|f>>> has a larger value than <math|K<bd>> (because
  <math|<Delsub|f*u*s,A>H> is smaller than <math|<Delsub|v*a*p,A>H>), the
  measurement of freezing-point depression is more useful than that of
  boiling-point elevation for estimating the molality of a dilute solution.
  <I|Boiling point!elevation in a solution\|)>

  <subsection|Vapor-pressure lowering><label|12-vapor-pressure lowering>

  <I|Vapor pressure!lowering in a solution\|(>In a binary two-phase system in
  which a solution of volatile solvent A and nonvolatile solute B is in
  equilibrium with gaseous A, the vapor pressure of the solution is equal to
  the system pressure <math|p>.

  Equation <reference|dp/dxA l-solid eqm><vpageref|dp/dxA l-solid eqm> gives
  the general dependence of <math|p> on <math|x<A>> for a binary liquid
  mixture in equilibrium with pure gaseous A. In this equation,
  <math|<Delsub|s*o*l,A>V> is the molar differential volume change for the
  dissolution of the gas in the solution. In the limit of infinite dilution,
  <math|-<Delsub|s*o*l,A>V> becomes <math|<Delsub|v*a*p,A>V>, the molar
  volume change for the vaporization of pure solvent. We also apply the
  limiting expressions of Eqs. <reference|d(mu/T)/dxA-\<gtr\>R> and
  <reference|dxA, inf diln>. The result is

  <\equation>
    lim<rsub|c<B>\<rightarrow\>0><Pd|p|c<B>|T>=-<frac|\<nu\>*V<A><rsup|\<ast\>>R*T|<Delsub|v*a*p,A>V>*<space|2em>lim<rsub|m<B>\<rightarrow\>0><Pd|p|m<B>|T>=-<frac|\<nu\>*M<A>R*T|<Delsub|v*a*p,A>V>
  </equation>

  If we neglect the molar volume of the liquid solvent compared to that of
  the gas, and assume the gas is ideal, then we can replace
  <math|<Delsub|v*a*p,A>V> in the expressions above by
  <math|V<A><rsup|\<ast\>><gas>=R*T/p<A><rsup|\<ast\>>> and obtain

  <\equation>
    lim<rsub|c<B>\<rightarrow\>0><Pd|p|c<B>|T>\<approx\>-\<nu\>*V<A><rsup|\<ast\>>p<A><rsup|\<ast\>><space|2em>lim<rsub|m<B>\<rightarrow\>0><Pd|p|m<B>|T>\<approx\>-\<nu\>*M<A>p<A><rsup|\<ast\>>
  </equation>

  where <math|p<A><rsup|\<ast\>>> is the vapor pressure of the pure solvent
  at the temperature of the solution.

  Thus, approximate expressions for vapor-pressure lowering in the limit of
  infinite dilution are

  <\equation>
    <Del>p\<approx\>-\<nu\>*V<A><rsup|\<ast\>>p<A><rsup|\<ast\>>c<B><space|2em><tx|a*n*d><space|2em><Del>p\<approx\>-\<nu\>*M<A>p<A><rsup|\<ast\>>m<B>
  </equation>

  We see that the lowering in this limit depends on the kind of solvent and
  the solution composition, but not on the kind of solute. <I|Vapor
  pressure!lowering in a solution\|)>

  <subsection|Osmotic pressure><label|12-osmotic pressure>

  <I|Osmotic pressure\|(>The osmotic pressure <math|<varPi>> is an intensive
  property of a solution and was defined in Sec. <reference|12-osmotic p
  measurements>. In a dilute solution of low <math|<varPi>>, the
  approximation used to derive Eq. <reference|muA*-muA=VA Pi> (that the
  partial molar volume <math|V<A>> of the solvent is constant in the pressure
  range from <math|p> to <math|p+<varPi>>) becomes valid, and we can write

  <\equation>
    <label|Pi=./.><varPi>=<frac|\<mu\><A><rsup|\<ast\>>-\<mu\><A>|V<A>>
  </equation>

  In the limit of infinite dilution, <math|\<mu\><A><rsup|\<ast\>>-\<mu\><A>>
  approaches <math|-R*T*ln x<A>> (Eq. <reference|muA(sln)=muA*(l)+RTln(xA)>)
  and <math|V<A>> becomes the molar volume <math|V<A><rsup|\<ast\>>> of the
  pure solvent. In this limit, Eq. <reference|Pi=./.> becomes

  <\equation>
    <varPi>=-<frac|R*T*ln x<A>|V<A><rsup|\<ast\>>>
  </equation>

  from which we obtain the equation

  <\equation>
    <label|lim dPi/dxA=>lim<rsub|x<A>\<rightarrow\>1><Pd|<varPi>|x<A>|T,p>=-<frac|R*T|V<A><rsup|\<ast\>>>
  </equation>

  The relations in Eq. <reference|dxA, inf diln> transform Eq. <reference|lim
  dPi/dxA=> into

  <\equation>
    <label|Pi/cB=RT>lim<rsub|c<B>\<rightarrow\>0><Pd|<varPi>|c<B>|T,p>=\<nu\>*R*T
  </equation>

  <\equation>
    <label|Pi/mB=RT*MA/VA>lim<rsub|m<B>\<rightarrow\>0><Pd|<varPi>|m<B>|T,p>=<frac|\<nu\>*R*T*M<A>|V<A><rsup|\<ast\>>>=\<nu\>*\<rho\><A><rsup|\<ast\>>R*T
  </equation>

  Equations <reference|Pi/cB=RT> and <reference|Pi/mB=RT*MA/VA> show that the
  osmotic pressure becomes independent of the kind of solute as the solution
  approaches infinite dilution. The integrated forms of these equations are

  <\gather>
    <tformat|<table|<row|<cell|<varPi>=\<nu\>*c<B>R*T<cond|<around|(|d*i*l*u*t*e*b*i*n*a*r*y*s*o*l*u*t*i*o*n|)>><eq-number><label|Pi=()cB>>>>>
  </gather>

  <\gather>
    <tformat|<table|<row|<cell|<varPi>=<frac|R*T*M<A>|V<A><rsup|\<ast\>>>*\<nu\>*m<B>=\<rho\><A><rsup|\<ast\>>R*T*\<nu\>*m<B><cond|<around|(|d*i*l*u*t*e*b*i*n*a*r*y*s*o*l*u*t*i*o*n|)>><eq-number><label|Pi=()mB>>>>>
  </gather>

  Equation <reference|Pi=()cB> is <I|vant Hoffs equation@van't Hoff's
  equation for osmotic pressure\|reg><I|Osmotic pressure!van't Hoff's
  equation for\|reg><newterm|van't Hoff's equation> for osmotic pressure. If
  there is more than one solute species, <math|\<nu\>*c<B>> can be replaced
  by <math|<big|sum><rsub|i\<ne\><tx|A>>c<rsub|i>> and <math|\<nu\>*m<B>> by
  <math|<big|sum><rsub|i\<ne\><tx|A>>m<rsub|i>> in these expressions.

  <quote-env| In Sec. <reference|9-act coeffs from osmotic coeffs>, it was
  stated that <math|<varPi>/m<B>> is equal to the product of
  <math|\<phi\><rsub|m>> and the limiting value of <math|<varPi>/m<B>> at
  infinite dilution, where <math|\<phi\><rsub|m>=<around|(|\<mu\><A><rsup|\<ast\>>-\<mu\><A>|)>/R*T*M<A><big|sum><rsub|i\<ne\><text|A>>m<rsub|i>>
  is the <index|Osmotic coefficient>osmotic coefficient. This relation
  follows directly from Eqs. <reference|muA*-muA=VA Pi> and
  <reference|Pi=()mB>.> <I|Osmotic pressure\|)>

  <input|./bio/vanthoff>

  <section|Solid\ULiquid Equilibria><label|12-solid l eqm>

  <I|Equilibrium!solid--liquid\|(>A <subindex|Freezing
  point|curve><em|freezing-point curve> (freezing point as a function of
  liquid composition) and a <subindex|Solubility|curve><em|solubility curve>
  (composition of a solution in equilibrium with a pure solid as a function
  of temperature) are different ways of describing the same physical
  situation. Thus, strange as it may sound, the composition <math|x<A>> of an
  aqueous solution at the freezing point is the mole fraction solubility of
  ice in the solution.

  <subsection|Freezing points of ideal binary liquid mixtures><label|12-fr
  pts, ideal l mixts>

  <I|Freezing point!ideal binary mixture@of an ideal binary
  mixture\|(>Section <reference|12-freezing-point measurements> described the
  use of freezing-point measurements to determine the solvent chemical
  potential in a solution of arbitrary composition relative to the chemical
  potential of the pure solvent. The way in which freezing point varies with
  solution composition in the limit of infinite dilution was derived in Sec.
  <reference|12-freezing-point depression>. Now let us consider the freezing
  behavior over the entire composition range of an <em|ideal> liquid mixture.

  The general relation between temperature and the composition of a binary
  liquid mixture, when the mixture is in equilibrium with pure solid A, is
  given by Eq. <reference|dT/dxA l-solid eqm>:

  <\equation>
    <label|dT/dxA repeat><Pd|T|x<A>|<space|-0.17em>p>=<frac|T<rsup|2>|<Delsub|s*o*l,A>H><bPd|<around|(|\<mu\><A>/T|)>|x<A>|T,p>
  </equation>

  We can replace <math|T> by <math|T<fA>> to indicate this is the temperature
  at which the mixture freezes to form solid A. From the expression for the
  chemical potential of component A in an ideal liquid mixture,
  <math|\<mu\><A>=\<mu\><A><rsup|\<ast\>>+R*T*ln x<A>>, we have
  <math|<bpd|<around|(|\<mu\><A>/T|)>|x<A>|T,p>=R/x<A>>. With these
  substitutions, Eq. <reference|dT/dxA repeat> becomes

  <\gather>
    <tformat|<table|<row|<cell|<Pd|T<fA>|x<A>|<space|-0.17em>p>=<frac|R*T<fA><rsup|2>|x<A><Delsub|s*o*l,A>H><cond|<around|(|i*d*e*a*l*l*i*q*u*i*d*m*i*x*t*u*r*e|)>><eq-number><label|dTf/dxA=RTf^2/xA
    del(sol)Hm>>>>>
  </gather>

  Figure <reference|fig:12-benz-sol><vpageref|fig:12-benz-sol>

  <\big-figure>
    <boxedfigure|<image|./12-SUP/benz-sol.eps||||> <capt|Dependence on
    composition of the freezing point of binary liquid mixtures with benzene
    as component A.<footnote|Experimental data from Ref. <cite|negishi-41>.>
    Solid curve: calculated for an ideal liquid mixture (Eq.
    <reference|dTf/dxA=RTf^2/xA del(sol)Hm>), taking the temperature
    variation of <math|<Delsub|s*o*l,A>H> into account. Open circles: B =
    toluene. Open triangles: B = cyclohexane.<label|fig:12-benz-sol>>>
  </big-figure|>

  compares the freezing behavior of benzene predicted by this equation with
  experimental freezing-point data for mixtures of benzene\Utoluene and
  benzene\Ucyclohexane. Any constituent that forms an ideal liquid mixture
  with benzene should give freezing points for the formation of solid benzene
  that fall on the curve in this figure. The agreement is good over a wide
  range of compositions for benzene\Utoluene mixtures (open circles), which
  are known to closely approximate ideal liquid mixtures. The agreement for
  benzene\Ucyclohexane mixtures (open triangles), which are not ideal liquid
  mixtures, is confined to the ideal-dilute region.

  If we make the approximation that <math|<Delsub|s*o*l,A>H> is constant over
  the entire range of mixture composition, we can replace it by
  <math|<Delsub|f*u*s,A>H>, the molar enthalpy of fusion of pure solid A at
  its melting point. This approximation allows us to separate the variables
  in Eq. <reference|dTf/dxA=RTf^2/xA del(sol)Hm> and integrate as follows
  from an arbitrary mixture composition <math|x<rprime|'><A>> at the freezing
  point <math|T<rprime|'><fA>> to pure liquid A at its freezing point
  <math|T<fA><rsup|\<ast\>>>:

  <\equation>
    <big|int><rsub|T<rprime|'><fA>><rsup|T<fA><rsup|\<ast\>>><frac|<dif>T|T<rsup|2>>=<frac|R|<Delsub|f*u*s,A>H>*<big|int><rsub|x<rprime|'><A>><rsup|1><frac|<dx><A>|x<A>>
  </equation>

  The result, after some rearrangement, is

  <\gather>
    <tformat|<table|<row|<cell|ln x<A>=<frac|<Delsub|f*u*s,A>H|R>*<around*|(|<frac|1|T<fA><rsup|\<ast\>>>-<frac|1|T<fA>>|)><cond|(i*d*e*a*l*l*i*q*u*i*d*m*i*x*t*u*r*e,><nextcond|<math|<Delsub|s*o*l,A>H=<Delsub|f*u*s,A>H>)><eq-number><label|ln(xA)
    vs T, l-sol eqm>>>>>
  </gather>

  This equation was used to generate the <I|Freezing point!curve!ideal binary
  mixture@of an ideal binary mixture\|reg>curves shown in Fig.
  <reference|fig:12-Tf-xB><vpageref|fig:12-Tf-xB>.

  <\big-figure>
    <boxedfigure|<image|./12-SUP/Tf-xB.eps||||> <capt|Freezing-point curves
    of ideal binary liquid mixtures. The solid is component A. Each curve is
    calculated from Eq. <reference|ln(xA) vs T, l-sol eqm> and is labeled
    with the value of <math|<Delsub|f*u*s,A>H/R*T<fA><rsup|\<ast\>>>.<label|fig:12-Tf-xB>>>
  </big-figure|>

  Although the shape of the freezing-point curve (<math|T<fA>> versus
  <math|x<B>>) shown in Fig. <reference|fig:12-benz-sol> is concave downward,
  Fig. <reference|fig:12-Tf-xB> shows this is not always the case. When
  <math|<Delsub|f*u*s,A>H/R*T<fA><rsup|\<ast\>>> is less than <math|2>, the
  freezing-point curve at low <math|x<B>> is concave <em|upward>. <I|Freezing
  point!ideal binary mixture@of an ideal binary mixture\|)>

  <subsection|Solubility of a solid nonelectrolyte>

  <I|Solubility!solid nonelectrolyte@of a solid nonelectrolyte\|(>Suppose we
  find that a solution containing solute B at a particular combination of
  temperature, pressure, and composition can exist in transfer equilibrium
  with pure solid B at the same temperature and pressure. This solution is
  said to be <index|Saturated solution><subindex|Solution|saturated><newterm|saturated>
  with respect to the solid. We can express the <I|Solubility!solid@of a
  solid\|reg><newterm|solubility> of the solid in the solvent by the value of
  the mole fraction, concentration, or molality of B in the saturated
  solution. We can also define solubility as the maximum value of the solute
  mole fraction, concentration, or molality that can exist in the solution
  without the possibility of spontaneous precipitation.

  This section considers the solubility of a solid nonelectrolyte. For the
  solution process B(s)<space|0.17em><ra><space|0.17em>B(sln), the general
  expression for the thermodynamic equilibrium constant is
  <math|K=a<B><sln>/a<B><solid>>.<footnote|In this and other expressions for
  equilibrium constants in this chapter, activities will be assumed to be for
  equilibrium states, although not indicated by the ``eq'' subscripts used in
  Chap. <reference|Chap. 11>.> The activity of the pure solid is
  <math|a<B><solid>=<G><B><solid>>. Let us use a solute standard state based
  on mole fraction; then the solute activity is
  <math|a<B><sln>=<G><xbB><space|0.17em><g><xbB><space|0.17em>x<B>>. From
  these relations, the solubility expressed as a mole fraction is

  <\equation>
    x<B>=<frac|<G><B><solid><space|0.17em>K|<G><xbB><space|0.17em><g><xbB>>
  </equation>

  If we measure the solubility at the standard pressure, the pressure factors
  <math|<G><B><solid>> and <math|<G><xbB>> are unity and the solubility is
  given by

  <\gather>
    <tformat|<table|<row|<cell|x<B>=<frac|K|<g><xbB>><cond|<around|(|s*o*l*u*b*i*l*i*t*y*o*f*s*o*l*i*d*B,<math|p=p<st>>|)>><eq-number><label|solid
    solubility>>>>>
  </gather>

  If the pressure is not exactly equal to <math|p<st>>, but is not very much
  greater, the values of the pressure factors are close to unity and Eq.
  <reference|solid solubility> is a good approximation.

  We can find the standard molar enthalpy of solution of B from the
  temperature dependence of the solubility. Combining Eqs.
  <reference|d(lnK)/dT=del(r)Hmo/RT2> and <reference|solid solubility>, we
  obtain

  <\gather>
    <tformat|<table|<row|<cell|<Delsub|s*o*l,B>H<st>=R*T<rsup|2>*<frac|<dif>ln
    <around|(|<g><xbB><space|0.17em>x<B>|)>|<dif>T><cond|<around|(|<math|p=p<st>>|)>><eq-number><label|del(sol)Hmo(solid)>>>>>
  </gather>

  The solubility may be small enough for us to be able to set the solute
  activity coefficient equal to <math|1>, in which case Eq.
  <reference|del(sol)Hmo(solid)> becomes

  <\gather>
    <tformat|<table|<row|<cell|<Delsub|s*o*l,B>H<st>=R*T<rsup|2>*<space|0.17em><frac|<dif>ln
    x<B>|<dif>T><cond|<around|(|<math|p=p<st>>,<math|<g><xbB|=>1>|)>><eq-number><label|del(sol)Hmo(solid,
    gamma(x,B)=1)>>>>>
  </gather>

  If the solubility <math|x<B>> increases with increasing temperature,
  <math|<Delsub|s*o*l,B>H<st>> must be positive and the solution process is
  endothermic. A decrease of solubility with increasing temperature implies
  an exothermic solution process. These statements refer to a solid of low
  solubility; see page <pageref|d xi(sol),eq)/dT> for a discussion of the
  general relation between the temperature dependence of solubility and the
  sign of the <I|Enthalpy!solution@of solution!molar differential\|reg>molar
  differential enthalpy of solution at saturation.

  For a solute standard state based on <em|molality>, we can derive equations
  like Eqs. <reference|del(sol)Hmo(solid)> and <reference|del(sol)Hmo(solid,
  gamma(x,B)=1)> with <math|<g><xbB>> replaced by <math|<g><mbB>> and
  <math|x<B>> replaced by <math|m<B>/m<st>>. If we use a solute standard
  state based on <em|concentration>, the expressions become slightly more
  complicated. The solubility in this case is given by

  <\equation>
    c<B>=<frac|<G><B><solid><space|0.17em>K*c<st>|<G><cbB><space|0.17em><g><cbB>>
  </equation>

  From Eq. <reference|d(lnK)/dT=del(r)Hmo/RT2-...>, we obtain, for a
  nonelectrolyte solid of low solubility, the relation

  <\gather>
    <tformat|<table|<row|<cell|<Delsub|s*o*l,B>H<st>=R*T<rsup|2>*<around*|(|<frac|<dif>ln
    <around|(|c<B>/c<st>|)>|<dif>T>+\<alpha\><A><rsup|\<ast\>>|)><cond|<around|(|<math|p=p<st>>,<math|<g><cbB|=>1>|)>><eq-number><label|del(sol)Hmo(solid,
    gamma(c,B)=1)>>>>>
  </gather>

  <I|Solubility!solid nonelectrolyte@of a solid nonelectrolyte\|)>

  <subsection|Ideal solubility of a solid>

  The <I|Ideal solubility!solid@of a solid\|reg><newterm|ideal solubility> of
  a solid at a given temperature and pressure is the solubility calculated on
  the assumptions that (1) the liquid is an ideal liquid mixture, and (2) the
  molar differential enthalpy of solution equals the molar enthalpy of fusion
  of the solid (<math|<Delsub|s*o*l,B>H=<Delsub|f*u*s,B>H>). These were the
  assumptions used to derive Eq. <reference|ln(xA) vs T, l-sol eqm> for the
  freezing-point curve of an ideal liquid mixture. In Eq. <reference|ln(xA)
  vs T, l-sol eqm>, we exchange the constituent labels A and B so that the
  solid phase is now component B:

  <\gather>
    <tformat|<table|<row|<cell|ln x<B>=<frac|<Delsub|f*u*s,B>H|R>*<around*|(|<frac|1|T<fB><rsup|\<ast\>>>-<frac|1|T>|)><cond|<around|(|i*d*e*a*l*s*o*l*u*b*i*l*i*t*y*o*f*s*o*l*i*d*B|)>><eq-number><label|id
    s solubility>>>>>
  </gather>

  Here <math|T<fB><rsup|\<ast\>>> is the melting point of solid B.

  According to Eq. <reference|id s solubility>, the ideal solubility of a
  solid is independent of the kind of solvent and increases with increasing
  temperature. For solids with similar molar enthalpies of fusion, the ideal
  solubility is less at a given temperature the higher is the melting point.
  This behavior is shown in Fig. <reference|fig:12-ideal
  solubility><vpageref|fig:12-ideal solubility>.

  <\big-figure>
    <boxedfigure|<image|./12-SUP/id-sol.eps||||> <capt|Ideal solubility of
    solid B as a function of <math|T>. The curves are calculated for two
    solids having the same molar enthalpy of fusion
    (<math|<Delsub|f*u*s,B>H=20<units|k*J*<space|0.17em>m*o*l<per>>>) and the
    values of <math|T<fB><rsup|\<ast\>>> indicated.<label|fig:12-ideal
    solubility>>>
  </big-figure|>

  In order for the experimental solubility of a solid to agree even
  approximately with the ideal value, the solvent and solute must be
  chemically similar, and the temperature must be close to the melting point
  of the solid so that <math|<Delsub|s*o*l,B>H> is close in value to
  <math|<Delsub|f*u*s,B>H>.

  <\quote-env>
    \ From the freezing behavior of benzene--toluene mixtures shown by the
    open circles in Fig. <reference|fig:12-benz-sol><vpageref|fig:12-benz-sol>,
    we can see that solid benzene has close to ideal solubility in liquid
    toluene at temperatures not lower than about <math|20<K>> below the
    melting point of benzene.
  </quote-env>

  <subsection|Solid compound of mixture components><label|12-solid cmpds>

  <I|Solid compound!mixture components@of mixture components\|(>Binary liquid
  mixtures are known in which the solid that appears when the mixture is
  cooled is a compound containing both components in a fixed proportion. This
  kind of solid is called a <index|Solid compound><newterm|solid compound>,
  or <I|Stoichiometric!addition compound\|seeSolid compound>stoichiometric
  addition compound. Examples are salt hydrates (salts with fixed numbers of
  waters of hydration in the formula unit) and certain metal alloys.

  The composition of the liquid mixture in this kind of system is variable,
  whereas the composition of the solid compound is fixed. Suppose the
  components are A and B, present in the liquid mixture at mole fractions
  <math|x<A>> and <math|x<B>>, and the solid compound has the formula
  A<rsub|<math|a>>B<rsub|<math|b>>. We assume that in the liquid phase the
  compound is completely dissociated with respect to the components; that is,
  that no molecules of formula A<rsub|<math|a>>B<rsub|<math|b>> exist in the
  liquid. The reaction equation for the freezing process is

  <\equation*>
    a<tx|A*<around|(|m*i*x*t|)>>+b<tx|B*<around|(|m*i*x*t|)>><arrow><tx|A<rsub|<math|a>>*B<rsub|<math|b>><around|(|s|)>>
  </equation*>

  When equilibrium exists between the liquid and solid phases, the
  temperature is the freezing point <math|T<rsub|<text|f>>> of the liquid. At
  equilibrium, the molar reaction Gibbs energy defined by
  <math|\<Delta\><rsub|<text|r>>*G=<big|sum><rsub|i><space|-0.17em>\<nu\><rsub|i>*\<mu\><rsub|i>>
  is zero:

  <\equation>
    <label|-a muA=>-a*\<mu\><A>-b*\<mu\><B>+\<mu\><solid>=0
  </equation>

  Here <math|\<mu\><A>> and <math|\<mu\><B>> refer to chemical potentials in
  the liquid mixture, and <math|\<mu\><solid>> refers to the solid compound.

  How does the freezing point of the liquid mixture vary with composition? We
  divide both sides of Eq. <reference|-a muA=> by <math|T> and take
  differentials:

  <\gather>
    <tformat|<table|<row|<cell|-a<dif><around|(|\<mu\><A>/T|)>-b<dif><around|(|\<mu\><B>/T|)>+<dif*|\<mu\><solid>/T>=0<cond|<around|(|p*h*a*s*e*e*q*u*i*l*i*b*r*i*u*m|)>><eq-number><label|-a*d(muA/T)-b*d(muB/T)+d(mu/T)=0>>>>>
  </gather>

  The pressure is constant. Then <math|\<mu\><A>/T> and <math|\<mu\><B>/T>
  are functions of <math|T> and <math|x<A>>, and <math|\<mu\><solid>/T> is a
  function only of <math|T>. We find expressions for the total differentials
  of these quantities at constant <math|p> with the help of Eq.
  <reference|d(mu_i/T)/dT=-H_i/T^2><vpageref|d(mu<rsub|i>/T)/dT=-H<rsub|i>/T<rsup|2>>:

  <\align>
    <tformat|<table|<row|<cell|<dif><around|(|\<mu\><A>/T|)>>|<cell|=-<frac|H<A>|T<rsup|2>><dif>T+<frac|1|T><Pd|\<mu\><A>|x<A>|T,p><dx><A><eq-number>>>|<row|<cell|<dif><around|(|\<mu\><B>/T|)>>|<cell|=-<frac|H<B>|T<rsup|2>><dif>T+<frac|1|T><Pd|\<mu\><B>|x<A>|T,p><dx><A><eq-number>>>|<row|<cell|<dvar|<around*|[|\<mu\><solid>/T|]>>>|<cell|=-<frac|H<m><solid>|T<rsup|2>><dif>T<eq-number>>>>>
  </align>

  When we substitute these expressions in Eq.
  <reference|-a*d(muA/T)-b*d(muB/T)+d(mu/T)=0> and solve for
  <math|<dif>T/<dx><A>>, setting <math|T> equal to <math|T<rsub|<text|f>>>,
  we obtain

  <\equation>
    <label|dT/dxA (solid cmpd,1)><frac|<dif>T<rsub|<text|f>>|<dx><A>>=<frac|T<rsub|<text|f>>|a*H<A>+b*H<B>-H<m><solid>>*<around*|[|a<Pd|\<mu\><A>|x<A>|T,p>+b<Pd|\<mu\><B>|x<A>|T,p>|]>
  </equation>

  The quantity <math|a*H<A>+b*H<B>-H<m><solid>> in the denominator on the
  right side of Eq. <reference|dT/dxA (solid cmpd,1)> is
  <math|<Delsub|s*o*l>H>, the molar differential enthalpy of solution of the
  solid compound in the liquid mixture. The two partial derivatives on the
  right side are related through the <index|Gibbs--Duhem
  equation>Gibbs\UDuhem equation <math|x<A><dif>\<mu\><A>+x<B><dif>\<mu\><B>=0>
  (Eq. <reference|sum(x_i)dX_i=0><vpageref|sum(x<rsub|i>)dX<rsub|i>=0>),
  which applies to changes at constant <math|T> and <math|p>. We rearrange
  the Gibbs\UDuhem equation to <math|<dif>\<mu\><B>=-<around|(|x<A>/x<B>|)><dif>\<mu\><A>>
  and divide by <math|<dx><A>>:

  <\equation>
    <Pd|\<mu\><B>|x<A>|T,p>=-<frac|x<A>|x<B>><Pd|\<mu\><A>|x<A>|T,p>
  </equation>

  Making this substitution in Eq. <reference|dT/dxA (solid cmpd,1)>, we
  obtain the equation

  <\equation>
    <label|dT/dxA (solid cmpd,2)><frac|<dif>T<rsub|<text|f>>|<dx><A>>=<frac|x<A>T<rsub|<text|f>>|<Delsub|s*o*l>H>*<around*|(|<frac|a|x<A>>-<frac|b|x<B>>|)><Pd|\<mu\><A>|x<A>|T,p>
  </equation>

  which can also be written in the slightly rearranged form

  <\equation>
    <label|dT/dxA (solid cmpd,3)><frac|<dif>T<rsub|<text|f>>|<dx><A>>=<frac|b*T<rsub|<text|f>>|<Delsub|s*o*l>H>*<around*|(|<frac|a|b>-<frac|x<A>|1-x<A>>|)><Pd|\<mu\><A>|x<A>|T,p>
  </equation>

  Suppose we heat a sample of the solid compound to its melting point to form
  a liquid mixture of the same composition as the solid. The molar enthalpy
  change of the fusion process is the molar enthalpy of fusion of the solid
  compound, <math|<Delsub|f*u*s>H>, a <em|positive> quantity. When the liquid
  has the same composition as the solid, the dissolution and fusion processes
  are identical; under these conditions, <math|<Delsub|s*o*l>H> is equal to
  <math|<Delsub|f*u*s>H> and is positive.

  Equation <reference|dT/dxA (solid cmpd,3)> shows that the slope of the
  <I|Freezing point!curve!solid compound formation@for solid compound
  formation\|reg>freezing-point curve, <math|T<rsub|<text|f>>> versus
  <math|x<A>>, is zero when <math|x<A>/<around|(|1-x<A>|)>> is equal to
  <math|a/b>, or <math|x<A>=a/<around|(|a+b|)>>;<label|zero slope>that is,
  when the liquid and solid have the same composition. Because
  <math|<pd|\<mu\><A>|x<A>|T,p>> is positive, and <math|<Delsub|s*o*l>H> at
  this composition is also positive, we see from the equation that the slope
  decreases as <math|x<A>> increases. Thus, the freezing-point curve has a
  maximum at the mixture composition that is the same as the composition of
  the solid compound. This conclusion applies when both components of the
  liquid mixture are nonelectrolytes, and also when one component is an
  electrolyte that dissociates into ions.

  Now let us assume the liquid mixture is an ideal liquid mixture of
  nonelectrolytes in which <math|\<mu\><A>> obeys Raoult's law for fugacity,
  <math|\<mu\><A>=\<mu\><A><rsup|\<ast\>>+R*T*ln x<A>>. The partial
  derivative <math|<pd|\<mu\><A>|x<A>|T,p>> then equals <math|R*T/x<A>>, and
  Eq. <reference|dT/dxA (solid cmpd,2)> becomes

  <\equation>
    <frac|<dif>T<rsub|<text|f>>|<dx><A>>=<frac|R*T<rsub|<text|f>><rsup|2>|<Delsub|s*o*l>H>*<around*|(|<frac|a|x<A>>-<frac|b|x<B>>|)>
  </equation>

  By making the approximations that <math|<Delsub|s*o*l>H> is independent of
  <math|T> and <math|x<A>>, and is equal to <math|<Delsub|f*u*s>H>, we can
  separate the variables and integrate as follows:

  <\equation>
    <big|int><rsub|T<rprime|'><rsub|<text|f>>><rsup|T<rprime|''><rsub|<text|f>>><frac|<dif>T<rsub|<text|f>>|T<rsub|<text|f>><rsup|2>>=<frac|R|<Delsub|f*u*s>H>*<around*|(|<big|int><rsub|x<rprime|'><A>><rsup|x<rprime|''><A>><frac|a|x<A>><dx><A>+<big|int><rsub|x<rprime|'><B>><rsup|x<rprime|''><B>><frac|b|x<B>><dx><B>|)>
  </equation>

  (The second integral on the right side comes from changing <math|<dx><A>>
  to <math|-<dx><B>>.) The result of the integration is

  <\gather>
    <tformat|<table|<row|<cell|<frac|1|T<rprime|'><rsub|<text|f>>>=<frac|1|T<rprime|''><rsub|<text|f>>>+<frac|R|<Delsub|f*u*s>H>*<around*|(|a*ln
    <frac|x<rprime|''><A>|x<rprime|'><A>>+b*ln
    <frac|x<rprime|''><B>|x<rprime|'><B>>|)><cond|(i*d*e*a*l*l*i*q*u*i*d*m*i*x*t*u*r*e*i*n><nextcond|e*q*u*i*l*i*b*r*i*u*m*w*i*t*h*s*o*l*i*d><nextcond|c*o*m*p*o*u*n*d,<math|<Delsub|s*o*l>H=<Delsub|f*u*s>H>)><eq-number><label|solid
    cmpd, id l mixt>>>>>
  </gather>

  Let <math|T<rprime|'><rsub|<text|f>>> be the freezing point of a liquid
  mixture of composition <math|x<rprime|'><A>> and
  <math|x<rprime|'><B>=1-x<rprime|'><A>>, and let
  <math|T<rprime|''><rsub|<text|f>>> be the melting point of the solid
  compound of composition <math|x<rprime|''><A>=a/<around|(|a+b|)>> and
  <math|x<rprime|''><B>=b/<around|(|a+b|)>>. Figure <reference|fig:12-solid
  compound><vpageref|fig:12-solid compound>

  <\big-figure>
    <boxedfigure|<image|./12-SUP/sol-cmpd.eps||||> <capt|Solid curve:
    freezing-point curve of a liquid melt of Zn and Mg that solidifies to the
    solid compound Zn<rsub|<math|2>>Mg.<space|.15em><footnote|Ref.
    <cite|elliot-65>, p. 603.> The curve maximum (open circle) is at the
    compound composition <math|x<rprime|''><subs|Z*n>=2/3> and the solid
    compound melting point <math|T<rprime|''><rsub|<text|f>>=861<K>>. Dashed
    curve: calculated using Eq. <reference|solid cmpd, id l mixt> with
    <math|<Delsub|f*u*s>H=15.8<units|k*J*<space|0.17em>m*o*l<per>>>.<label|fig:12-solid
    compound>>>
  </big-figure|>

  shows an example of a molten metal mixture that solidifies to an alloy of
  fixed composition. The freezing-point curve of this system is closely
  approximated by Eq. <reference|solid cmpd, id l mixt>. <I|Solid
  compound!mixture components@of mixture components\|)>

  <subsection|Solubility of a solid electrolyte><label|12-electrolyte
  solubility>

  <I|Solubility!solid electrolyte@of a solid electrolyte\|(>Consider an
  equilibrium between a crystalline salt (or other kind of ionic solid) and a
  solution containing the solvated ions:

  <\equation*>
    <tx|M<rsub|<math|\<nu\><rsub|+>>>*X<rsub|<math|\<nu\><rsub|->>><around|(|s|)>><arrows>\<nu\><rsub|+><tx|M<rsup|<math|z<rsub|+>>><around|(|aq|)>>+\<nu\><rsub|-><tx|X<rsup|<math|z<rsub|->>><around|(|aq|)>>
  </equation*>

  Here <math|\<nu\><rsub|+>> and <math|\<nu\><rsub|->> are the numbers of
  cations and anions in the formula unit of the salt, and <math|z<rsub|+>>
  and <math|z<rsub|->> are the charge numbers of these ions. The solution in
  equilibrium with the solid salt is a saturated solution. The thermodynamic
  equilibrium constant for this kind of equilibrium is called a
  <index|Solubility product><newterm|solubility product>, <math|K<subs|s>>.

  We can readily derive a relation between <math|K<subs|s>> and the
  molalities of the ions in the saturated solution by treating the dissolved
  salt as a single solute substance, B. We write the equilibrium in the form
  B<rsup|<math|\<ast\>>>(s)<math|<arrows>>B(sln), and write the expression
  for the solubility product as a proper quotient of activities:

  <\equation>
    K<subs|s>=<frac|a<mbB>|a<B><rsup|\<ast\>>>
  </equation>

  From Eq. <reference|a(B)(multisolute)><vpageref|a(B)(multisolute)>, we have
  <math|a<mbB>=<G><mbB><space|0.17em><g><rsub|\<pm\>><rsup|\<nu\>><around|(|m<rsub|+>/m<st>|)><rsup|\<nu\><rsub|+>>*<around|(|m<rsub|->/m<st>|)><rsup|\<nu\><rsub|->>>.
  This expression is valid whether or not the ions M<rsup|<math|z<rsub|+>>>
  and X<rsup|<math|z<rsub|->>> are present in solution in the same ratio as
  in the solid salt. When we replace <math|a<mbB>> with this expression, and
  replace <math|a<B><rsup|\<ast\>>> with <math|<G><B><rsup|\<ast\>>> (Table
  <reference|tbl:9-activities>), we obtain

  <\equation>
    <label|Ks=>K<subs|s>=<around*|(|<frac|<G><mbB>|<G><B><rsup|\<ast\>>>|)><g><rsub|\<pm\>><rsup|\<nu\>><around*|(|<frac|m<rsub|+>|m<st>>|)><rsup|\<nu\><rsub|+>><around*|(|<frac|m<rsub|->|m<st>>|)><rsup|\<nu\><rsub|->>
  </equation>

  where <math|\<nu\>=\<nu\><rsub|+>+\<nu\><rsub|->> is the total number of
  ions per formula unit. <math|<g><rsub|\<pm\>>> is the mean ionic activity
  coefficient of the dissolved salt in the saturated solution, and the
  molalities <math|m<rsub|+>> and <math|m<rsub|->> refer to the ions
  M<rsup|<math|z<rsub|+>>> and X<rsup|<math|z<rsub|->>> in this solution.

  The first factor on the right side of Eq. <reference|Ks=>, the proper
  quotient of pressure factors for the reaction
  B<rsup|<math|\<ast\>>>(s)<math|<ra>>B(sln), will be denoted
  <math|<G><subs|r>> (the subscript \Pr\Q stands for reaction). The value of
  <math|<G><subs|r>> is exactly <math|1> if the system is at the standard
  pressure, and is otherwise approximately <math|1> unless the pressure is
  very high.

  If the aqueous solution is produced by allowing the salt to dissolve in
  pure water, or in a solution of a second solute containing no ions in
  common with the salt, then the ion molalities in the saturated solution are
  <math|m<rsub|+>=\<nu\><rsub|+>*m<B>> and
  <math|m<rsub|->=\<nu\><rsub|->*m<B>> where <math|m<B>> is the solubility of
  the salt expressed as a molality. Under these conditions, Eq.
  <reference|Ks=> becomes<footnote|We could also have obtained this equation
  by using the expression of Eq. <reference|a(mB),general> for
  <math|a<mbB>>.>

  <\gather>
    <tformat|<table|<row|<cell|K<subs|s>=<G><subs|r><space|0.17em><g><rsub|\<pm\>><rsup|\<nu\>><around*|(|\<nu\><rsub|+><rsup|\<nu\><rsub|+>>*\<nu\><rsub|-><rsup|\<nu\><rsub|->>|)><around*|(|<frac|m<B>|m<st>>|)><rsup|\<nu\>><cond|<around|(|n*o*c*o*m*m*o*n*i*o*n|)>><eq-number><label|Ks,
    no common ion>>>>>
  </gather>

  If the ionic strength of the saturated salt solution is sufficiently low
  (i.e., the solubility is sufficiently low), it may be practical to evaluate
  the solubility product with Eq. <reference|Ks, no common ion> and an
  estimate of <math|<g><rsub|\<pm\>>> from the <I|Debye--H�ckel!limiting
  law>Debye\UH�ckel limiting law (see Prob. 12.<reference|prb:12-Ks(AgCl)>).
  The most accurate method of measuring a solubility product, however, is
  through the standard cell potential of an appropriate galvanic cell (Sec.
  <reference|14-st molar rxn quantities>).

  Since <math|K<subs|s>> is a thermodynamic equilibrium constant that depends
  only on <math|T>, and <math|<G><subs|r>> depends only on <math|T> and
  <math|p>, Eq. <reference|Ks, no common ion> shows that any change in the
  solution composition at constant <math|T> and <math|p> that decreases
  <math|<g><rsub|\<pm\>>> must increase the solubility. For example, the
  solubility of a sparingly-soluble salt increases when a second salt,
  lacking a common ion, is dissolved in the solution; this is a
  <em|salting-in effect>.

  Equation <reference|Ks=> is a general equation that applies even if the
  solution saturated with one salt contains a second salt with a common ion.
  For instance, consider the sparingly-soluble salt
  M<rsub|<math|\<nu\><rsub|+>>>X<rsub|<math|\<nu\><rsub|->>> in transfer
  equilibrium with a solution containing the more soluble salt
  M<rsub|<math|\<nu\><rprime|'><rsub|+>>>Y<rsub|<math|\<nu\><rprime|'><rsub|->>>
  at molality <math|m<C>>. The common ion in this example is the cation
  M<rsup|<math|z<rsub|+>>>. The expression for the solubility product is now

  <\gather>
    <tformat|<table|<row|<cell|K<subs|s>=<G><subs|r><space|0.17em><g><rsub|\<pm\>><rsup|\<nu\>><space|0.17em><around|(|\<nu\><rsub|+>*m<B>+\<nu\><rprime|'><rsub|+>*m<C>|)><rsup|\<nu\><rsub|+>>*<around|(|\<nu\><rsub|->*m<B>|)><rsup|\<nu\><rsub|->>/<around|(|m<st>|)><rsup|\<nu\>><cond|<around|(|c*o*m*m*o*n*c*a*t*i*o*n|)>><eq-number><label|Ks,
    common cation>>>>>
  </gather>

  where <math|m<B>> again is the solubility of the sparingly-soluble salt,
  and <math|m<C>> is the molality of the second salt. <math|K<subs|s>> and
  <math|<G><subs|r>> are constant if <math|T> and <math|p> do not change, so
  any increase in <math|m<C>> at constant <math|T> and <math|p> must cause a
  decrease in the solubility <math|m<B>>. This is called the <index|Common
  ion effect><em|common ion effect>.

  From the measured solubility of a salt in pure solvent, or in an
  electrolyte solution with a common cation, and a known value of
  <math|K<subs|s>>, we can evaluate the <I|Activity coefficient!mean
  ionic!solubility@from solubility measurement\|reg>mean ionic activity
  coefficient <math|<g><rsub|\<pm\>>> through Eq. <reference|Ks, no common
  ion> or <reference|Ks, common cation>. This procedure has the disadvantage
  of being limited to the value of <math|m<B>> existing in the saturated
  solution.

  We find the temperature dependence of <subindex|Solubility
  product|temperature dependence><math|K<subs|s>> by applying Eq.
  <reference|d(lnK)/dT=del(r)Hmo/RT2>:

  <\equation>
    <frac|<dif>ln K<subs|s>|<dif>T>=<frac|<Delsub|s*o*l,B>H<st>|R*T<rsup|2>>
  </equation>

  At the standard pressure, <math|<Delsub|s*o*l,B>H<st>> is the same as the
  molar enthalpy of solution at infinite dilution,
  <math|<Delsub|s*o*l,B>H<rsup|\<infty\>>>.
  <I|Equilibrium!solid--liquid\|)><I|Solubility!solid electrolyte@of a solid
  electrolyte\|)>

  <section|Liquid\ULiquid Equilibria><label|12-l-l eqm>

  <I|Equilibrium!liquid--liquid\|(>

  <subsection|Miscibility in binary liquid systems>

  When two different pure liquids are unable to mix in all proportions, they
  are said to be <em|partially miscible>. When these liquids are placed in
  contact with one another and allowed to come to thermal, mechanical, and
  transfer equilibrium, the result is two coexisting liquid mixtures of
  different compositions.

  Liquids are never actually completely <em|immiscible>. To take an extreme
  case, liquid mercury, when equilibrated with water, has some
  H<rsub|<math|2>>O dissolved in it, and some mercury dissolves in the water,
  although the amounts may be too small to measure.

  The Gibbs phase rule for a multicomponent system to be described in Sec.
  <reference|13-phase rule> shows that a two-component, two-phase system at
  equilibrium has only two independent intensive variables. Thus at a given
  temperature and pressure, the mole fraction compositions of both phases are
  fixed; the compositions depend only on the identity of the substances and
  the temperature and pressure.

  Figure <reference|fig:13-liqliq><vpageref|fig:13-liqliq> shows a phase
  diagram for a typical binary liquid mixture that spontaneously separates
  into two phases when the temperature is lowered. The thermodynamic
  conditions for <subindex|Phase|separation of a liquid mixture>phase
  separation of this kind were discussed in Sec. <reference|11-phase sep>.
  The phase separation is usually the result of positive deviations from
  Raoult's law. Typically, when phase separation occurs, one of the
  substances is polar and the other nonpolar.

  <subsection|Solubility of one liquid in another><label|12-sol of l>

  Suppose substances A and B are both liquids when pure. In discussing the
  solubility of liquid B in liquid A, we can treat B as either a solute or as
  a constituent of a liquid mixture. The difference lies in the choice of the
  standard state or reference state of B.

  We can define the solubility of B in A as the maximum amount of B that can
  dissolve without phase separation in a given amount of A at the given
  temperature and pressure. Treating B as a solute, we can express its
  solubility as the mole fraction of B in the phase at the point of phase
  separation. The addition of any more B to the system will result in two
  coexisting liquid phases of fixed composition, one of which will have mole
  fraction <math|x<B>> equal to its solubility.<footnote|Experimentally, the
  solubility of B in A can be determined from the <em|cloud point>, the point
  during titration of A with B at which persistent turbidity is observed.>

  Consider a system with two coexisting liquid phases <math|<pha>> and
  <math|<phb>> containing components A and B. Let <math|<pha>> be the A-rich
  phase and <math|<phb>> be the B-rich phase. For example, A could be water
  and B could be benzene, a hydrophobic substance. Phase <math|<pha>> would
  then be an aqueous phase polluted with a low concentration of dissolved
  benzene, and phase <math|<phb>> would be wet benzene. <math|x<B><aph>>
  would be the solubility of the benzene in water, expressed as a mole
  fraction.

  Below, relations are derived for this kind of system using both choices of
  standard state or reference state.

  <subsubsection|Solute standard state>

  Assume that the two components have low mutual solubilities, so that B has
  a low mole fraction in phase <math|<pha>> and a mole fraction close to 1 in
  phase <math|<phb>>. It is then appropriate to treat B as a solute in phase
  <math|<pha>> and as a constituent of a liquid mixture in phase
  <math|<phb>>. The value of <math|x<B><aph>> is the solubility of liquid B
  in liquid A.

  The equilibrium when two liquid phases are present is
  B(<math|<phb>>)<math|<arrows>>B(<math|<pha>>), and the expression for the
  thermodynamic equilibrium constant, with the solute standard state based on
  mole fraction, is

  <\equation>
    K=<frac|a<xbB><aph>|a<B><bph>>=<frac|<G><xbB><aph><space|0.17em><g><xbB><aph><space|0.17em>x<B><aph>|<G><B><bph><space|0.17em><g><B><bph><space|0.17em>x<B><bph>>
  </equation>

  The solubility of B is then given by

  <\equation>
    x<B><aph>=<frac|<G><B><bph><g><B><bph><space|0.17em>x<B><bph>|<G><xbB><aph><g><xbB><aph>>*K
  </equation>

  The values of the pressure factors and activity coefficients are all close
  to <math|1>, so that the solubility of B in A is given by
  <math|x<B><aph>\<approx\>K>. The temperature dependence of the solubility
  is given by

  <\equation>
    <label|d ln x(B)alpha/dT=><frac|<dif>ln
    x<B><aph>|<dif>T>\<approx\><frac|<dif>ln
    K|<dif>T>=<frac|<Delsub|s*o*l,B>H<st>|R*T<rsup|2>>
  </equation>

  where <math|<Delsub|s*o*l,B>H<st>> is the molar enthalpy change for the
  transfer at pressure <math|p<st>> of pure liquid solute to the solution at
  infinite dilution.

  H<rsub|<math|2>>O and <em|n>-butylbenzene are two liquids with very small
  mutual solubilities. Figure <reference|fig:12-butylbenzene><vpageref|fig:12-butylbenzene>

  <\big-figure>
    <boxedfigure|<image|./12-SUP/BuBenz.eps||||> <capt|Aqueous solubility of
    liquid <em|n>-butylbenzene as a function of temperature (Ref.
    <cite|owens-86>).<label|fig:12-butylbenzene>>>
  </big-figure|>

  shows that the solubility of <em|n>-butylbenzene in water exhibits a
  minimum at about <math|12<units|<degC>>>. Equation <reference|d ln
  x(B)alpha/dT=> allows us to deduce from this behavior that
  <math|<Delsub|s*o*l,B>H<st>> is negative below this temperature, and
  positive above.

  <subsubsection|Pure-liquid reference state>

  The condition for transfer equilibrium of component B is
  <math|\<mu\><B><aph>=\<mu\><B><bph>>. If we use a pure-liquid reference
  state for B in both phases, this condition becomes

  <\equation>
    \<mu\><B><rsup|\<ast\>>+R*T*ln <around|(|<g><B><aph>x<B><aph>|)>=\<mu\><B><rsup|\<ast\>>+R*T*ln
    <around|(|<g><B><bph>x<B><bph>|)>
  </equation>

  This results in the following relation between the compositions and
  activity coefficients:

  <\equation>
    <label|x(a)/x(b)=ac(b)/ac(a)><g><B><aph>x<B><aph>=<g><B><bph>x<B><bph>
  </equation>

  As before, we assume the two components have low mutual solubilities, so
  that the B-rich phase is almost pure liquid B. Then <math|x<B><bph>> is
  only slightly less than <math|1>, <math|<g><B><bph>> is close to <math|1>,
  and Eq. <reference|x(a)/x(b)=ac(b)/ac(a)> becomes
  <math|x<B><aph>\<approx\>1/<g><B><aph>>. Since <math|x<B><aph>> is much
  less than <math|1>, <math|<g><B><aph>> must be much greater than <math|1>.

  In environmental chemistry it is common to use a pure-liquid reference
  state for a nonpolar liquid solute that has very low solubility in water,
  so that the aqueous solution is essentially at infinite dilution. Let the
  nonpolar solute be component B, and let the aqueous phase that is
  equilibrated with liquid B be phase <math|<pha>>. The activity coefficient
  <math|<g><B><aph>> is then a <em|limiting activity coefficient> or
  <em|activity coefficient at infinite dilution>. As explained above, the
  aqueous solubility of B in this case is given by
  <math|x<B><aph>\<approx\>1/<g><B><aph>>, and <math|<g><B><aph>> is much
  greater than <math|1>.

  We can also relate the solubility of B to its <I|Solubility!liquid@of a
  liquid, relation to Henry's law constant\|reg>Henry's law constant
  <math|<kHB><aph>>. Suppose the two liquid phases are equilibrated not only
  with one another but also with a gas phase. Since B is equilibrated between
  phase <math|<pha>> and the gas, we have
  <math|<g><xbB><aph>=<fug><B>/<kHB><aph><space|0.17em>x<B><aph>> (Table
  <reference|tbl:9-act coeff-fugacity>). From the equilibration of B between
  phase <math|<phb>> and the gas, we also have
  <math|<g><B><bph>=<fug><B>/x<B><bph><space|0.17em><fug><B><rsup|\<ast\>>>.
  By eliminating the fugacity <math|<fug><B>> from these relations, we obtain
  the general relation

  <\equation>
    x<B><aph>=<frac|<g><B><bph><space|0.17em>x<B><bph><space|0.17em><fug><B><rsup|\<ast\>>|<g><xbB><aph><space|0.17em><kHB><aph>>
  </equation>

  If we assume as before that the activity coefficients and <math|x<B><bph>>
  are close to 1, and that the gas phase behaves ideally, the solubility of B
  is given by <math|x<B><aph>\<approx\>p<B><rsup|\<ast\>>/<kHB><aph>>, where
  <math|p<B><rsup|\<ast\>>> is the vapor pressure of the pure solute.

  <subsection|Solute distribution between two partially-miscible
  solvents><label|12-solute distribution>

  Consider a two-component system of two equilibrated liquid phases,
  <math|<pha>> and <math|<phb>>. If we add a small quantity of a third
  component, C, it will distribute itself between the two phases. It is
  appropriate to treat C as a solute in <em|both> phases. The thermodynamic
  equilibrium constant for the equilibrium
  <math|<chem>C<around|(|<phb>|)><arrows>C<around|(|<pha>|)>>, with solute
  standard states based on mole fraction, is

  <\equation>
    K=<frac|a<xbC><aph>|a<xbC><bph>>=<frac|<G><xbC><aph><space|0.17em><g><xbC><aph><space|0.17em>x<C><aph>|<G><xbC><bph><space|0.17em><g><xbC><bph><space|0.17em>x<C><bph>>
  </equation>

  We define <math|K<rprime|'>> as the ratio of the mole fractions of C in the
  two phases at equilibrium:

  <\equation>
    <label|K'=>K<rprime|'><defn><frac|x<C><aph>|x<C><bph>>=<frac|<G><xbC><bph><space|0.17em><g><xbC><bph>|<G><xbC><aph><space|0.17em><g><xbC><aph>>*K
  </equation>

  At a fixed <math|T> and <math|p>, the pressure factors and equilibrium
  constant are constants. If <math|x<C>> is low enough in both phases for
  <math|<g><xbC><aph>> and <math|<g><xbC><bph>> to be close to unity,
  <math|K<rprime|'>> becomes a constant for the given <math|T> and <math|p>.
  The constancy of <math|K<rprime|'>> over a range of dilute composition is
  the <subindex|Nernst|distribution law><newterm|Nernst distribution law>.

  Since solute molality and concentration are proportional to mole fraction
  in dilute solutions, the ratios <math|m<C><aph>/m<C><bph>> and
  <math|c<C><aph>/c<C><bph>> also approach constant values at a given
  <math|T> and <math|p>. The ratio of concentrations is called the
  <index|Partition coefficient><newterm|partition coefficient> or
  <index|Distribution coefficient><newterm|distribution coefficient>.

  In the limit of infinite dilution of C, the two phases have the
  compositions that exist when only components A and B are present. As C is
  added and <math|x<C><aph>> and <math|x<C><bph>> increase beyond the region
  of dilute solution behavior, the ratios <math|x<B><aph>/x<A><aph>> and
  <math|x<B><bph>/x<A><bph>> may change. Continued addition of C may increase
  the mutual solubilities of A and B, resulting, when enough C has been
  added, in a single liquid phase containing all three components. It is
  easier to understand this behavior with the help of a ternary phase diagram
  such as Fig. <reference|fig:13-EtOH-benz-H2O><vpageref|fig:13-EtOH-benz-H2O>.

  <I|Equilibrium!liquid--liquid\|)>

  <section|Membrane Equilibria>

  A semipermeable membrane used to separate two liquid phases can, in
  principle, be permeable to certain species and impermeable to others. A
  membrane, however, may not be perfect in this respect over a long time
  period (see page <pageref|not completely impermeable>). We will assume that
  during the period of observation, those species to which the membrane is
  supposed to be permeable quickly achieve transfer equilibrium, and only
  negligible amounts of the other species are transferred across the
  membrane.

  Section <reference|12-osmotic p measurements> sketched a derivation of the
  conditions needed for equilibrium in a two-phase system in which a membrane
  permeable only to solvent separates a solution from pure solvent. We can
  generalize the results for any system with two liquid phases separated by a
  semipermeable membrane: in an equilibrium state, both phases must have the
  same temperature, and any species to which the membrane is permeable must
  have the same chemical potential in both phases. The two phases, however,
  need not and usually do not have the same pressure.

  <subsection|Osmotic membrane equilibrium><label|12-osmotic membrane eqm>

  An equilibrium state in a system with two solutions of the same solvent and
  different solute compositions, separated by a membrane permeable only to
  the solvent, is called an <index|Osmotic membrane
  equilibrium><subindex|Membrane equilibrium|osmotic><newterm|osmotic
  membrane equilibrium>. We have already seen this kind of equilibrium in an
  apparatus that measures osmotic pressure (Fig. <reference|fig:12-osmotic
  pressure><vpageref|fig:12-osmotic pressure>).

  Consider a system with transfer equilibrium of the solvent across a
  membrane separating phases <math|<pha>> and <math|<phb>>. The phases have
  equal solvent chemical potentials but different pressures:

  <\equation>
    \<mu\><A><bph><around|(|p<bph>|)>=\<mu\><A><aph><around|(|p<aph>|)>
  </equation>

  The dependence of <math|\<mu\><A>> on pressure in a phase of fixed
  temperature and composition is given by
  <math|<pd|\<mu\><A>|p|T,<allni>>=V<A>> (from Eq.
  <reference|d(mu_i)/dp=V_i>), where <math|V<A>> is the partial molar volume
  of A in the phase. If we apply this relation to the solution of phase
  <math|<phb>>, treat the partial molar volume <math|V<A>> as independent of
  pressure, and integrate at constant temperature and composition from the
  pressure of phase <math|<pha>> to that of phase <math|<phb>>, we obtain

  <\equation>
    <label|muA(p)=>\<mu\><A><bph><around|(|p<bph>|)>=\<mu\><A><bph><around|(|p<aph>|)>+V<A><bph><around|(|p<bph>-p<aph>|)>
  </equation>

  By equating the two expressions for <math|\<mu\><A><bph><around|(|p<bph>|)>>
  and rearranging, we obtain the following expression for the pressure
  difference needed to achieve transfer equilibrium:

  <\equation>
    <label|p(beta)-p(alpha)=>p<bph>-p<aph>=<frac|\<mu\><A><aph><around|(|p<aph>|)>-\<mu\><A><bph><around|(|p<aph>|)>|V<A><bph>>
  </equation>

  The pressure difference can be related to the <index|Osmotic
  pressure>osmotic pressures of the two phases. From Eq.
  <reference|muA*-muA=VA Pi><vpageref|muA*-muA=VA Pi>, the solvent chemical
  potential in a solution phase can be written
  <math|\<mu\><A><around|(|p|)>=\<mu\><A><rsup|\<ast\>><around|(|p|)>-V<A><varPi><around|(|p|)>>.
  Using this to substitute for <math|\<mu\><A><aph><around|(|p<aph>|)>> and
  <math|\<mu\><A><bph><around|(|p<aph>|)>> in Eq.
  <reference|p(beta)-p(alpha)=>, we obtain

  <\equation>
    p<bph>-p<aph>=<varPi><bph><around|(|p<aph>|)>-<around*|(|<frac|V<A><aph>|V<A><bph>>|)><varPi><aph><around|(|p<aph>|)>
  </equation>

  <subsection|Equilibrium dialysis>

  <subindex|Equilibrium|dialysis><index|Dialysis, equilibrium>Equilibrium
  dialysis is a useful technique for studying the binding of a small
  uncharged solute species (a ligand) to a macromolecule. The macromolecule
  solution is placed on one side of a membrane through which it cannot pass,
  with a solution without the macromolecule on the other side, and the ligand
  is allowed to come to transfer equilibrium across the membrane. If the same
  solute standard state is used for the ligand in both solutions, at
  equilibrium the unbound ligand must have the same activity in both
  solutions. Measurements of the total ligand molality in the macromolecule
  solution and the ligand molality in the other solution, combined with
  estimated values of the unbound ligand activity coefficients, allow the
  amount of ligand bound per macromolecule to be calculated.

  <subsection|Donnan membrane equilibrium><label|12-Donnan eqm>

  <I|Donnan!membrane equilibrium\|(><I|Membrane equilibrium!Donnan\|(>

  If one of the solutions in a two-phase membrane equilibrium contains
  certain <em|charged> solute species that are unable to pass through the
  membrane, whereas other ions can pass through, the situation is more
  complicated than the osmotic membrane equilibrium described in Sec.
  <reference|12-osmotic membrane eqm>. Usually if the membrane is impermeable
  to one kind of ion, an ion species to which it is permeable achieves
  transfer equilibrium across the membrane only when the phases have
  different pressures and different electric potentials. The equilibrium
  state in this case is a <subindex|Donnan|membrane
  equilibrium><subindex|Membrane equilibrium|Donnan><newterm|Donnan membrane
  equilibrium>, and the resulting <subindex|Electric|potential
  difference>electric potential difference across the membrane is called the
  <subindex|Donnan|potential><newterm|Donnan potential>. This phenomenon is
  related to the membrane potentials that are important in the functioning of
  nerve and muscle cells (although the cells of a living organism are not, of
  course, in equilibrium states).

  A Donnan potential can be measured electrically, with some uncertainty due
  to unknown liquid junction potentials, by connecting silver-silver chloride
  electrodes (described in Sec. <reference|14-cell rxns>) to both phases
  through salt bridges.

  <subsubsection|General expressions>

  Consider solution phases <math|<pha>> and <math|<phb>> separated by a
  semipermeable membrane. Both phases contain a dissolved salt, designated
  solute B, that has <math|\<nu\><rsub|+>> cations and <math|\<nu\><rsub|->>
  anions in each formula unit. The membrane is permeable to these ions. Phase
  <math|<phb>> also contains a protein or other polyelectrolyte with a net
  positive or negative charge, together with counterions of the opposite
  charge that are the same species as the cation or anion of the salt. The
  presence of the counterions in phase <math|<phb>> prevents the cation and
  anion of the salt from being present in stoichiometric amounts in this
  phase. The membrane is impermeable to the polyelectrolyte, perhaps because
  the membrane pores are too small to allow the polyelectrolyte to pass
  through.

  The condition for transfer equilibrium of solute B is
  <math|\<mu\><B><aph>=\<mu\><B><bph>>, or

  <\equation>
    <around|(|\<mu\><mbB><st>|)><aph>+R*T*ln
    a<mbB><aph>=<around|(|\<mu\><mbB><st>|)><bph>+R*T*ln a<mbB><bph>
  </equation>

  Solute B has the same standard state in the two phases, so that
  <math|<around|(|\<mu\><mbB><st>|)><aph>> and
  <math|<around|(|\<mu\><mbB><st>|)><bph>> are equal. The activities
  <math|a<mbB><aph>> and <math|a<mbB><bph>> are therefore equal at
  equilibrium. Using the expression for solute activity from Eq.
  <reference|a(B)(multisolute)>, which is valid for a multisolute solution,
  we find that at transfer equilibrium the following relation must exist
  between the molalities of the salt ions in the two phases:

  <\equation>
    <label|Donnan product with act coeffs><G><mbB><aph><around*|(|<g><rsub|\<pm\>><aph>|)><rsup|\<nu\>><around*|(|m<rsub|+><aph>|)><rsup|\<nu\><rsub|+>><around*|(|m<rsub|-><aph>|)><rsup|\<nu\><rsub|->>=<G><mbB><bph><around*|(|<g><rsub|\<pm\>><bph>|)><rsup|\<nu\>><around*|(|m<rsub|+><bph>|)><rsup|\<nu\><rsub|+>><around*|(|m<rsub|-><bph>|)><rsup|\<nu\><rsub|->>
  </equation>

  To find an expression for the Donnan potential, we can equate the
  single-ion chemical potentials of the salt cation:
  <math|\<mu\><rsub|+><aph><around|(|\<phi\><aph>|)>=\<mu\><rsub|+><bph><around|(|\<phi\><bph>|)>>.
  When we use the expression of Eq. <reference|mu+(phi)=> for
  <math|\<mu\><rsub|+><around|(|\<phi\>|)>>, we obtain

  <\gather>
    <tformat|<table|<row|<cell|\<phi\><aph>-\<phi\><bph>=<frac|R*T|z<rsub|+>*F>*ln
    <frac|<G><rsub|+><bph><space|0.17em><g><rsub|+><bph><space|0.17em>m<rsub|+><bph>|<G><rsub|+><aph><space|0.17em><g><rsub|+><aph><space|0.17em>m<rsub|+><aph>><cond|<around|(|D*o*n*n*a*n*p*o*t*e*n*t*i*a*l|)>><eq-number><label|Donnan
    pot>>>>>
  </gather>

  The condition needed for an osmotic membrane equilibrium related to the
  solvent can be written

  <\equation>
    <label|muA(beta)-muA(alpha)=0>\<mu\><A><bph><around|(|p<bph>|)>-\<mu\><A><aph><around|(|p<aph>|)>=0
  </equation>

  The chemical potential of the solvent is
  <math|\<mu\><A>=\<mu\><A><st>+R*T*ln a<A>=\<mu\><A><st>+R*T*ln
  <around|(|<G><A><space|0.17em><g><A><space|0.17em>x<A>|)>>. From Table
  <reference|tbl:9-Gamma_i>, we have to a good approximation the expression
  <math|R*T*ln <G><A>=V<A><rsup|\<ast\>><around|(|p-p<st>|)>>. With these
  substitutions, Eq. <reference|muA(beta)-muA(alpha)=0> becomes

  <\equation>
    <label|Donnan eq 1>R*T*ln <frac|<g><A><bph><space|0.17em>x<A><bph>|<g><A><aph><space|0.17em>x<A><aph>>+V<A><rsup|\<ast\>><around*|(|p<bph>-p<aph>|)>=0
  </equation>

  We can use this equation to estimate the pressure difference needed to
  maintain an equilibrium state. For dilute solutions, with
  <math|<g><A><aph>> and <math|<g><A><bph>> set equal to 1, the equation
  becomes

  <\equation>
    p<bph>-p<aph>\<approx\><frac|R*T|V<A><rsup|\<ast\>>>*ln
    <frac|x<A><aph>|x<A><bph>>
  </equation>

  In the limit of infinite dilution, <math|ln x<A>> can be replaced by
  <math|-M<A><big|sum><rsub|i\<ne\><tx|A>>m<rsub|i>> (Eq.
  <reference|ln(xA)=-MAsum(mi)><vpageref|ln(xA)=-MAsum(mi)>), giving the
  relation

  <\equation>
    <label|p(beta)-p(alpha)(Donnan)=>p<bph>-p<aph>\<approx\><frac|M<A>R*T|V<A><rsup|\<ast\>>>*<big|sum><rsub|i\<ne\><tx|A>><around*|(|m<rsub|i><bph>-m<rsub|i><aph>|)>=\<rho\><A><rsup|\<ast\>>R*T*<big|sum><rsub|i\<ne\><tx|A>><around*|(|m<rsub|i><bph>-m<rsub|i><aph>|)>
  </equation>

  <subsubsection|Example>

  As a specific example of a Donnan membrane equilibrium, consider a system
  in which an aqueous solution of a polyelectrolyte with a net negative
  charge, together with a counterion M<rsup|<math|+>> and a salt MX of the
  counterion, is equilibrated with an aqueous solution of the salt across a
  semipermeable membrane. The membrane is permeable to the H<rsub|<math|2>>O
  solvent and to the ions M<rsup|<math|+>> and X<rsup|<math|->>, but is
  impermeable to the polyelectrolyte. The species in phase <math|<pha>> are
  H<rsub|<math|2>>O, M<rsup|<math|+>>, and X<rsup|<math|->>; those in phase
  <math|<phb>> are H<rsub|<math|2>>O, M<rsup|<math|+>>, X<rsup|<math|->>, and
  the polyelectrolyte. In an equilibrium state, the two phases have the same
  temperature but different compositions, electric potentials, and pressures.

  Because the polyelectrolyte in this example has a negative charge, the
  system has more M<rsup|<math|+>> ions than X<rsup|<math|->> ions. Figure
  <reference|fig:11-Donnan>(a)<vpageref|fig:11-Donnan>

  <\big-figure>
    <\boxedfigure>
      <image|./12-SUP/Donnan.eps||||>

      <\capt>
        Process for attainment of a Donnan membrane equilibrium (schematic).
        The dashed ellipse represents a semipermeable membrane.

        \ (a) Initial nonequilibrium state.

        \ (b) Final equilibrium state.<label|fig:11-Donnan>
      </capt>
    </boxedfigure>
  </big-figure|>

  is a schematic representation of an initial state of this kind of system.
  Phase <math|<phb>> is shown as a solution confined to a closed dialysis bag
  immersed in phase <math|<pha>>. The number of cations and anions shown in
  each phase indicate the relative amounts of these ions.

  For simplicity, let us assume the two phases have equal masses of water, so
  that the molality of an ion is proportional to its amount by the same ratio
  in both phases. It is clear that in the initial state shown in the figure,
  the chemical potentials of both M<rsup|<math|+>> and X<rsup|<math|->> are
  greater in phase <math|<phb>> (greater amounts) than in phase <math|<pha>>,
  and this is a nonequilibrium state. A certain quantity of salt MX will
  therefore pass spontaneously through the membrane from phase <math|<phb>>
  to phase <math|<pha>> until equilibrium is attained.

  The equilibrium ion molalities must agree with Eq. <reference|Donnan
  product with act coeffs>. We make the approximation that the pressure
  factors and mean ionic activity coefficients are unity. Then for the
  present example, with <math|\<nu\><rsub|+>=\<nu\><rsub|->=1>, the equation
  becomes

  <\equation>
    <label|Donnan product>m<rsub|+><aph>m<rsub|-><aph>\<approx\>m<rsub|+><bph>m<rsub|-><bph>
  </equation>

  There is furthermore an electroneutrality condition for each phase:

  <\equation>
    m<aph><rsub|+>=m<aph><rsub|-><space|2em>m<bph><rsub|+>=m<bph><rsub|->+<around|\||z<subs|P>|\|>*m<subs|P>
  </equation>

  Here <math|z<subs|P>> is the negative charge of the polyelectrolyte, and
  <math|m<subs|P>> is its molality. Substitution of these expressions into
  Eq. <reference|Donnan product> gives the relation

  <\equation>
    <around*|(|m<aph><rsub|->|)><rsup|2>\<approx\><around*|(|m<bph><rsub|->+<around|\||z<subs|P>|\|>*m<subs|P>|)>*m<bph><rsub|->
  </equation>

  This shows that in the equilibrium state, <math|m<aph><rsub|->> is greater
  than <math|m<bph><rsub|->>. Then Eq. <reference|Donnan product> shows that
  <math|m<aph><rsub|+>> is less than <math|m<bph><rsub|+>>. These equilibrium
  molalities are depicted in Fig. <reference|fig:11-Donnan>(b).

  The chemical potential of a cation, its activity, and the electric
  potential of the phase are related by Eq.
  <reference|mu_i(a_i,phi)=><vpageref|mu<rsub|i>(a<rsub|i>,phi)=>:
  <math|\<mu\><rsub|+>=\<mu\><rsub|+><st>+R*T*ln
  a<rsub|+>+z<rsub|+>*F*\<phi\>>. In order for M<rsup|<math|+>> to have the
  same chemical potential in both phases, despite its lower activity in phase
  <math|<pha>>, the electric potential of phase <math|<pha>> must be greater
  than that of phase <math|<phb>>. Thus the Donnan potential
  <math|\<phi\><aph>-\<phi\><bph>> in the present example is positive. Its
  value can be estimated from Eq. <reference|Donnan pot> with the values of
  the single-ion pressure factors and activity coefficients approximated by 1
  and with <math|z<rsub|+>> for this example set equal to 1:

  <\equation>
    <label|phi(a)-phi(b)(cation)>\<phi\><aph>-\<phi\><bph>\<approx\><frac|R*T|F>*ln
    <frac|m<rsub|+><bph>|m<rsub|+><aph>>
  </equation>

  <\quote-env>
    \ The existence of a Donnan potential in the equilibrium state is the
    result of a very small departure of the phases on both sides of the
    membrane from exact electroneutrality. In the example, phase <math|<pha>>
    has a minute net positive charge and phase <math|<phb>> has a net
    negative charge of equal magnitude. The amount of M<rsup|<math|+>> ion
    transferred across the membrane to achieve equilibrium is slightly
    greater than the amount of X<rsup|<math|->> ion transferred; the
    difference between these two amounts is far too small to be measured
    chemically. At equilibrium, the excess charge on each side of the
    membrane is distributed over the boundary surface of the solution phase
    on that side, and is not part of the bulk phase composition.
  </quote-env>

  The pressure difference <math|p<bph>-p<aph>> at equilibrium can be
  estimated with Eq. <reference|p(beta)-p(alpha)(Donnan)=>, and for the
  present example is found to be positive. Without this pressure difference,
  the solution in phase <math|<pha>> would move spontaneously through the
  membrane into phase <math|<phb>> until phase <math|<pha>> completely
  disappears. With phase <math|<pha>> open to the atmosphere, as in Fig.
  <reference|fig:11-Donnan>, the volume of phase <math|<phb>> must be
  constrained in order to allow its pressure to differ from atmospheric
  pressure. If the volume of phase <math|<phb>> remains practically constant,
  the transfer of a minute quantity of solvent across the membrane is
  sufficient to cause the pressure difference.

  It should be clear that the existence of a Donnan membrane equilibrium
  introduces complications that would make it difficult to use a measured
  pressure difference to estimate the molar mass of the polyelectrolyte by
  the method of Sec. <reference|12-colligative properties>, or to study the
  binding of a charged ligand by equilibrium dialysis.

  <I|Donnan!membrane equilibrium\|)><I|Membrane equilibrium!Donnan\|)>

  <section|Liquid\UGas Equilibria><label|12-l/gas eq>

  <I|Equilibrium!liquid--gas\|(>

  This section describes multicomponent systems in which a liquid phase is
  equilibrated with a gas phase.

  <subsection|Effect of liquid pressure on gas fugacity><label|12-effect of p
  on fug>

  <subindex|Fugacity|effect of liquid pressure on>If we vary the pressure of
  a liquid mixture at constant temperature and composition, there is a small
  effect on the fugacity of each volatile component in an equilibrated gas
  phase. One way to vary the pressure at essentially constant liquid
  composition is to change the partial pressure of a component of the gas
  phase that has negligible solubility in the liquid.

  At transfer equilibrium, component <math|i> has the same chemical potential
  in both phases: <math|\<mu\><rsub|i><liquid>=\<mu\><rsub|i><gas>>.
  Combining the relations <math|<bpd|\<mu\><rsub|i><liquid>|p|T,<allni>>=V<rsub|i><liquid>>
  and <math|\<mu\><rsub|i><gas>=\<mu\><rsub|i><st><gas>+R*T*ln
  <around|(|<fug><rsub|i>/p<st>|)>> (Eqs. <reference|d(mu_i)/dp=V_i> and
  <reference|mu_i=mu_io(g)+RT*(f_i/po)>), we obtain

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif>ln
    <around|(|<fug><rsub|i>/p<st>|)>|<difp>>=<frac|V<rsub|i><liquid>|R*T><cond|(e*q*u*i*l*i*b*r*a*t*e*d*l*i*q*u*i*d*a*n*d><nextcond|g*a*s*m*i*x*t*u*r*e*s,c*o*n*s*t*a*n*t<math|T>><nextcond|a*n*d*l*i*q*u*i*d*c*o*m*p*o*s*i*t*i*o*n)><eq-number><label|dln(f_i/po)/dp=V_i(l)/RT>>>>>
  </gather>

  Equation <reference|dln(f_i/po)/dp=V_i(l)/RT> shows that an increase in
  pressure, at constant temperature and liquid composition, causes an
  increase in the fugacity of each component in the gas phase.

  Integration of Eq. <reference|dln(f_i/po)/dp=V_i(l)/RT> between pressures
  <math|p<rsub|1>> and <math|p<rsub|2>> yields

  <\gather>
    <tformat|<table|<row|<cell|<fug><rsub|i><around|(|p<rsub|2>|)>=<fug><rsub|i><around|(|p<rsub|1>|)>*exp
    <around*|[|<big|int><rsub|p<rsub|1>><rsup|p<rsub|2>><frac|V<rsub|i><liquid>|R*T><difp>|]><cond|(e*q*u*i*l*i*b*r*a*t*e*d*l*i*q*u*i*d*a*n*d><nextcond|g*a*s*m*i*x*t*u*r*e*s,c*o*n*s*t*a*n*t<math|T>><nextcond|a*n*d*l*i*q*u*i*d*c*o*m*p*o*s*i*t*i*o*n)><eq-number><label|f_i(p2)=f_i(p1)exp[int(V_i/RT)dp]>>>>>
  </gather>

  The exponential on the right side is called the <index|Poynting
  factor><newterm|Poynting factor>.

  The integral in the Poynting factor is simplified if we make the
  approximation that <math|V<rsub|i><liquid>> is independent of pressure.
  Then we obtain the approximate relation

  <\gather>
    <tformat|<table|<row|<cell|<fug><rsub|i><around|(|p<rsub|2>|)>\<approx\><fug><rsub|i><around|(|p<rsub|1>|)>*exp
    <around*|[|<frac|V<rsub|i><liquid><around|(|p<rsub|2>-p<rsub|1>|)>|R*T>|]><cond|(e*q*u*i*l*i*b*r*a*t*e*d*l*i*q*u*i*d*a*n*d><nextcond|g*a*s*m*i*x*t*u*r*e*s,c*o*n*s*t*a*n*t<math|T>><nextcond|a*n*d*l*i*q*u*i*d*c*o*m*p*o*s*i*t*i*o*n)><eq-number><label|f_i(p2)
    approx>>>>>
  </gather>

  <\quote-env>
    \ The effect of pressure on fugacity is usually small, and can often be
    neglected. For typical values of the partial molar volume
    <math|V<rsub|i><liquid>>, the exponential factor is close to unity unless
    <math|<around|\||p<rsub|2>-p<rsub|1>|\|>> is very large. For instance,
    for <math|V<rsub|i><liquid|=>100<units|c*m<rsup|<math|3>>*<space|0.17em>mol<per>>>
    and <math|T=300<K>>, we obtain a value for the ratio
    <math|<fug><rsub|i><around|(|p<rsub|2>|)>/<fug><rsub|i><around|(|p<rsub|1>|)>>
    of <math|1.004> if <math|p<rsub|2>-p<rsub|1>> is <math|1<br>>,
    <math|1.04> if <math|p<rsub|2>-p<rsub|1>> is <math|10<br>>, and
    <math|1.5> if <math|p<rsub|2>-p<rsub|1>> is <math|100<br>>. Thus, unless
    the pressure change is large, we can to a good approximation neglect the
    effect of total pressure on fugacity. This statement applies only to the
    fugacity of a substance in a gas phase that is equilibrated with a liquid
    phase of constant composition containing the same substance. If the
    liquid phase is absent, the fugacity of <math|i> in a gas phase of
    constant composition is of course approximately proportional to the total
    gas pressure.
  </quote-env>

  We can apply Eqs. <reference|f_i(p2)=f_i(p1)exp[int(V_i/RT)dp]> and
  <reference|f_i(p2) approx> to <em|pure> liquid A, in which case
  <math|V<rsub|i><liquid>> is the molar volume
  <math|V<A><rsup|\<ast\>><liquid>>. Suppose we have pure liquid A in
  equilibrium with pure gaseous A at a certain temperature. This is a
  one-component, two-phase equilibrium system with one degree of freedom
  (Sec. <reference|8-Gibbs phase rule>), so that at the given temperature the
  value of the pressure is fixed. This pressure is the saturation vapor
  pressure of pure liquid A at this temperature. We can make the pressure
  <math|p> greater than the saturation vapor pressure by adding a second
  substance to the gas phase that is essentially insoluble in the liquid,
  without changing the temperature or volume. The fugacity <math|<fug><A>> is
  greater at this higher pressure than it was at the saturation vapor
  pressure. The vapor pressure <math|p<A>>, which is approximately equal to
  <math|<fug><A>>, has now become greater than the saturation vapor pressure.
  It is, however, safe to say that the difference is negligible unless the
  difference between <math|p> and <math|p<A>> is much greater than
  <math|1<br>>.

  As an application of these relations, consider the effect of the size of a
  liquid droplet on the equilibrium <I|Vapor pressure!liquid droplet@of a
  liquid droplet\|reg>vapor pressure. The calculation of Prob.
  12.<reference|prb:12-droplet>(b) shows that the fugacity of
  H<rsub|<math|2>>O in a gas phase equilibrated with liquid water in a small
  droplet is slightly greater than when the liquid is in a bulk phase. The
  smaller the radius of the droplet, the greater is the fugacity and the
  vapor pressure.

  <subsection|Effect of liquid composition on gas fugacities>

  <plainfootnotes><label|12-partial pressure over a liquid>

  <I|Fugacity!effect of liquid composition on\|(>Consider system 1 in Fig.
  <reference|fig:9-liquid+gas><vpageref|fig:9-liquid+gas>. A binary liquid
  mixture of two volatile components, A and B, is equilibrated with a gas
  mixture containing A, B, and a third gaseous component C of negligible
  solubility used to control the total pressure. In order for A and B to be
  in transfer equilibrium, their chemical potentials must be the same in both
  phases:

  <\equation>
    \<mu\><A><liquid>=\<mu\><A><st><gas>+R*T*ln
    <frac|<fug><A>|p<st>>*<space|2em>\<mu\><B><liquid>=\<mu\><B><st><gas>+R*T*ln
    <frac|<fug><B>|p<st>>
  </equation>

  Suppose we make an infinitesimal change in the liquid composition at
  constant <math|T> and <math|p>. This causes infinitesimal changes in the
  chemical potentials and fugacities:

  <\equation>
    <dif>\<mu\><A><liquid>=R*T*<frac|<dif><fug><A>|<fug><A>><space|2em><dif>\<mu\><B><liquid>=R*T*<frac|<dif><fug><B>|<fug><B>>
  </equation>

  By inserting these expressions in the <index|Gibbs--Duhem
  equation>Gibbs\UDuhem equation <math|x<A><dif>\<mu\><A>=-x<B><dif>\<mu\><B>>
  (Eq. <reference|sum(x_i)dmu_i=0>), we obtain

  <\gather>
    <tformat|<table|<row|<cell|<frac|x<A>|<fug><A>><dif><fug><A>=-<frac|x<B>|<fug><B>><dif><fug><B><cond|(b*i*n*a*r*y*l*i*q*u*i*d*m*i*x*t*u*r*e*e*q*u*i*l*i*b*r*a*t*e*d><nextcond|w*i*t*h*g*a*s,c*o*n*s*t*a*n*t<math|T>
    and <math|p>)><eq-number><label|(xA/fA)dfA=-(xB/fB)dfB>>>>>
  </gather>

  This equation is a relation between changes in gas-phase fugacities caused
  by a change in the liquid-phase composition. It shows that a composition
  change at constant <math|T> and <math|p> that increases the fugacity of A
  in the equilibrated gas phase must decrease the fugacity of B.

  Now let us treat the liquid mixture as a binary solution with component B
  as the solute. In the ideal-dilute region, at constant <math|T> and
  <math|p>, the solute obeys Henry's law for fugacity:

  <\equation>
    <label|fB=k xB><fug><B>=<kHB>x<B>
  </equation>

  For composition changes in the ideal-dilute region, we can write

  <\equation>
    <label|dfB/dxB=k><frac|<dif><fug><B>|<dx><B>>=<kHB>=<frac|<fug><B>|x<B>>
  </equation>

  With the substitution <math|<dx><B>=-<dx><A>> and rearrangement, Eq.
  <reference|dfB/dxB=k> becomes

  <\equation>
    -<frac|x<B>|<fug><B>><dif><fug><B>=<dx><A>
  </equation>

  Combined with Eq. <reference|(xA/fA)dfA=-(xB/fB)dfB>, this is
  <math|<around|(|x<A>/<fug><A>|)><dif><fug><A>=<dx><A>>, which we can
  rearrange and integrate as follows within the ideal-dilute region:

  <\equation>
    <label|int(dfA/fA)=><big|int><rsub|<fug><A><rsup|\<ast\>>><rsup|<fug><rprime|'><A>><frac|<dif><fug><A>|<fug><A>>=<big|int><rsub|1><rsup|x<rprime|'><A>><frac|<dx><A>|x<A>>*<space|2em>ln
    <frac|<fug><rprime|'><A>|<fug><A><rsup|\<ast\>>>=ln x<rprime|'><A>
  </equation>

  The result is

  <\gather>
    <tformat|<table|<row|<cell|<fug><A>=x<A><fug><A><rsup|\<ast\>><cond|<around|(|i*d*e*a*l-d*i*l*u*t*e*b*i*n*a*r*y*s*o*l*u*t*i*o*n|)>><eq-number><label|fA=xA
    fA*>>>>>
  </gather>

  Here <math|<fug><A><rsup|\<ast\>>> is the fugacity of A in a gas phase
  equilibrated with pure liquid A at the same <math|T> and <math|p> as the
  mixture. Equation <reference|fA=xA fA*> is Raoult's law for fugacity
  applied to component A.

  If component B obeys Henry's law at all compositions, then the Henry's law
  constant <math|<kHB>> is equal to <math|<fug><B><rsup|\<ast\>>> and B obeys
  Raoult's law, <math|<fug><B>=x<B><fug><B><rsup|\<ast\>>>, over the entire
  range of <math|x<B>>.

  We can draw two conclusions:

  <\enumerate>
    <item>In the ideal-dilute region of a binary solution, where the solute
    obeys Henry's law, <subindex|Solvent|behavior in an ideal-dilute
    solution><subindex|Ideal-dilute solution|solvent behavior in>the solvent
    must obey Raoult's law. (A similar result was derived in Sec.
    <reference|9-Solvent in ideal-dilute soln> for a solution with any number
    of solutes.)

    <item><I|Raoult's law!fugacity@for fugacity!binary liquid mixture@in a
    binary liquid mixture\|reg>If one component of a binary liquid mixture
    obeys Raoult's law at all compositions, so also must the other component.
    This is the definition of an ideal binary liquid mixture (Sec.
    <reference|9-ideal mixtures>).
  </enumerate>

  <I|Raoult's law!deviations from\|(>Suppose we have a nonideal binary liquid
  mixture in which component B exhibits positive deviations from Raoult's
  law. An example of this behavior for the water\Uethanol system is shown in
  Fig. <reference|fig:12-EtOH-H2O fug><vpageref|fig:12-EtOH-H2O fug>.

  <\big-figure>
    <boxedfigure|<image|./12-SUP/EtOH-H2O.eps||||> <capt|Fugacities in a gas
    phase equilibrated with a binary liquid mixture of H<rsub|<math|2>>O (A)
    and ethanol (B) at <math|25<units|<degC>>> and
    <math|1<br>>.<space|.15em><footnote|Based on data in Ref.
    <cite|dobson-25>.> The dashed lines show Raoult's law behavior. The
    dotted lines illustrate the inequality
    <math|<around|(|<dif><fug><B>/<dx><B>|)>\<less\><around|(|<fug><B>/x<B>|)>>.<label|fig:12-EtOH-H2O
    fug>>>
  </big-figure|>

  At each point on the curve of <math|<fug><B>> versus <math|x<B>>, the slope
  <math|<dif><fug><B>/<dx><B>> is less than the slope <math|<fug><B>/x<B>> of
  a line drawn from the origin to the point (as illustrated by the open
  circles and dotted lines in the figure), except that the two slopes become
  equal at <math|x<B|=>1>:

  <\equation>
    <label|dfB/dxB\<less\>=fB/xB><frac|<dif><fug><B>|<dx><B>>\<le\><frac|<fug><B>|x<B>>
  </equation>

  As we can see from the figure, this relation must apply to any component
  whose fugacity curve exhibits a positive deviation from Raoult's law and
  has only one inflection point.

  Algebraic operations on an inequality must be carried out with care:
  multiplying both sides by a quantity that can be negative may change the
  inequality to one with the wrong sign. In order to simplify manipulation of
  the inequality of Eq. <reference|dfB/dxB\<less\>=fB/xB>, it helps to
  convert it to the following equality:<footnote|This procedure is similar to
  the rectification procedure described on page <pageref|rectification>.>

  <\equation>
    <label|dfB/dxB+a=fB/xB><frac|<dif><fug><B>|<dx><B>>+D=<frac|<fug><B>|x<B>>
  </equation>

  Here <math|D> represents the difference between <math|<fug><B>/x<B>> and
  <math|<dif><fug><B>/<dx><B>>; its value is a function of <math|x<B>> and
  is, according to Eq. <reference|dfB/dxB\<less\>=fB/xB>, either positive or
  zero. We make the substitution <math|<dx><B>=-<dx><A>> and rearrange to

  <\equation>
    <label|xBdfB/(-fB+DxB)=dxA><frac|x<B><dif><fug><B>|-<fug><B>+D*x<B>>=<dx><A>
  </equation>

  When <math|D> is zero, this equation becomes
  <math|-x<B><dif><fug><B>/<fug><B>=<dx><A>>. When <math|D> is positive, the
  left side of the equation is less than <math|-x<B><dif><fug><B>/<fug><B>>
  and is equal to <math|<dx><A>>, so that <math|<dx><A>> is less than
  <math|-x<B><dif><fug><B>/<fug><B>>. Since <math|D> cannot be negative, Eq.
  <reference|xBdfB/(-fB+DxB)=dxA> is equivalent to the following relation:

  <\equation>
    -<frac|x<B>|<fug><B>><dif><fug><B>\<ge\><dx><A>
  </equation>

  A substitution from Eq. <reference|(xA/fA)dfA=-(xB/fB)dfB> gives us

  <\equation>
    <frac|x<A>|<fug><A>><dif><fug><A>\<ge\><dx><A><space|1em><tx|o*r><space|1em><frac|<dif><fug><A>|<fug><A>>\<ge\><frac|<dx><A>|x<A>>
  </equation>

  We can integrate both sides of the second relation as follows:<footnote|The
  equalities are the same as Eqs. <reference|int(dfA/fA)=> and
  <reference|fA=xA fA*>, with the difference that here <math|x<A>> is not
  restricted to the ideal-dilute region.>

  <\equation>
    <big|int><rsup|<fug><rprime|'><A>><rsub|<fug><A><rsup|\<ast\>>><frac|<dif><fug><A>|<fug><A>>\<ge\><big|int><rsup|x<rprime|'><A>><rsub|1><frac|<dx><A>|x<A>>*<space|2em>ln
    <frac|<fug><rprime|'><A>|<fug><A><rsup|\<ast\>>>\<ge\>ln
    x<rprime|'><A><space|2em><fug><A>\<ge\>x<A><fug><A><rsup|\<ast\>>
  </equation>

  Thus, <em|if the curve of fugacity versus mole fraction for one component
  of a binary liquid mixture exhibits only positive deviations from Raoult's
  law, with only one inflection point, so also must the curve of the other
  component>. In the water\Uethanol system shown in Fig.
  <reference|fig:12-EtOH-H2O fug>, both curves have positive deviations from
  Raoult's law, and both have a single inflection point.

  By the same method, we find that if the fugacity curve of one component has
  only <em|negative> deviations from Raoult's law with a single inflection
  point, the same is true of the other component.

  Figure <reference|fig:12-EtOH-CHCl3 fug><vpageref|fig:12-EtOH-CHCl3 fug>

  <\big-figure>
    <boxedfigure|<image|./12-SUP/EtOH-cf.eps||||> <capt|Fugacities in a gas
    phase equilibrated with a binary liquid mixture of chloroform (A) and
    ethanol (B) at <math|35<units|<degC>>> (Ref.
    <cite|scatchard-38>).<label|fig:12-EtOH-CHCl3 fug>>>
  </big-figure|>

  illustrates the case of a binary mixture in which component B has only
  positive deviations from Raoult's law, whereas component A has both
  positive and negative deviations (<math|<fug><A>> is slightly less than
  <math|x<A><fug><A><rsup|\<ast\>>> for <math|x<B>> less than 0.3). This
  unusual behavior is possible because both fugacity curves have two
  inflection points instead of the usual one. Other types of unusual nonideal
  behavior are possible.<footnote|Ref. <cite|mcglashan-63>.>
  <I|Fugacity!effect of liquid composition on\|)><I|Raoult's law!deviations
  from\|)>

  <subsection|The Duhem\UMargules equation>

  <paragraphfootnotes><label|12-Duhem-Margules eqn>

  When we divide both sides of Eq. <reference|(xA/fA)dfA=-(xB/fB)dfB> by
  <math|<dx><A>>, we obtain the <I|Duhem Margules equation@Duhem--Margules
  equation\|reg><newterm|Duhem--Margules equation>:

  <\gather>
    <tformat|<table|<row|<cell|<frac|x<A>|<fug><A>>*<space|0.17em><frac|<dif><fug><A>|<dx><A>>=-<frac|x<B>|<fug><B>>*<space|0.17em><frac|<dif><fug><B>|<dx><A>><cond|(b*i*n*a*r*y*l*i*q*u*i*d*m*i*x*t*u*r*e*e*q*u*i*l*i*b*r*a*t*e*d><nextcond|w*i*t*h*g*a*s,c*o*n*s*t*a*n*t<math|T>
    and <math|p>)><eq-number><label|Duhem-Margules>>>>>
  </gather>

  If we assume the gas mixture is ideal, the fugacities are the same as the
  partial pressures, and the Duhem\UMargules equation then becomes

  <\gather>
    <tformat|<table|<row|<cell|<frac|x<A>|p<A>>*<space|0.17em><frac|<difp><A>|<dx><A>>=-<frac|x<B>|p<B>>*<space|0.17em><frac|<difp><B>|<dx><A>><cond|(b*i*n*a*r*y*l*i*q*u*i*d*m*i*x*t*u*r*e*e*q*u*i*l*i*b*r*a*t*e*d><nextcond|w*i*t*h*i*d*e*a*l*g*a*s,c*o*n*s*t*a*n*t<math|T>
    and <math|p>)><eq-number><label|Duhem-Margules,ig>>>>>
  </gather>

  Solving Eq. <reference|Duhem-Margules,ig> for <math|<difp><B>/<dx><A>>, we
  obtain

  <\equation>
    <label|dpB/dxA=><frac|<difp><B>|<dx><A>>=-<frac|x<A>p<B>|x<B>p<A>>*<space|0.17em><frac|<difp><A>|<dx><A>>
  </equation>

  To a good approximation, by assuming an ideal gas mixture and neglecting
  the effect of total pressure on fugacity, we can apply Eq.
  <reference|dpB/dxA=> to a liquid\Ugas system in which the total pressure is
  <em|not> constant, but instead is the sum of <math|p<A>> and <math|p<B>>.
  Under these conditions, we obtain the following expression for the rate at
  which the total pressure changes with the liquid composition at constant
  <math|T>:

  <\equation>
    <label|dp/dxA=>

    <\eqsplit>
      <tformat|<table|<row|<cell|<frac|<difp>|<dx><A>>>|<cell|=<frac|<dif><around|(|p<A>+p<B>|)>|<dx><A>>=<frac|<difp><A>|<dx><A>>-<frac|x<A>p<B>|x<B>p<A>>*<space|0.17em><frac|<difp><A>|<dx><A>>=<frac|<difp><A>|<dx><A>>*<around*|(|1-<frac|x<A>/x<B>|p<A>/p<B>>|)>>>|<row|<cell|<vspace*|2ex>>|<cell|=<frac|<difp><A>|<dx><A>>*<around*|(|1-<frac|x<A>/x<B>|y<A>/y<B>>|)>>>>>
    </eqsplit>
  </equation>

  Here <math|y<A>> and <math|y<B>> are the mole fractions of A and B in the
  gas phase given by <math|y<A>=p<A>/p> and <math|y<B>=p<B>/p>.

  We can use Eq. <reference|dp/dxA=> to make several predictions for a binary
  liquid\Ugas system at constant <math|T>.

  <\itemize>
    <item>If the ratio <math|y<A>/y<B>> is greater than <math|x<A>/x<B>>
    (meaning that the mole fraction of A is greater in the gas than in the
    liquid), then <math|<around|(|x<A>/x<B>|)>/<around|(|y<A>/y<B>|)>> is
    less than <math|1> and <math|<difp>/<dx><A>> must have the same sign as
    <math|<difp><A>/<dx><A>>, which is positive.

    <item>Conversely, if <math|y<A>/y<B>> is less than <math|x<A>/x<B>>
    (i.e., the mole fraction of B is greater in the gas than in the liquid),
    then <math|<difp>/<dx><A>> must be negative.

    <item>Thus <em|compared to the liquid, the gas phase is richer in the
    component whose addition to the liquid at constant temperature causes the
    total pressure to increase>. This statement is a version of
    <I|Konowaloff's rule\|reg><em|Konowaloff's rule>.
  </itemize>

  In some binary liquid\Ugas systems, the total pressure at constant
  temperature exhibits a maximum or minimum at a particular liquid
  composition.<label|binary azeotrope>At this composition,
  <math|<difp>/<dx><A>> is zero but <math|<difp><A>/<dx><A>> is positive.
  From Eq. <reference|dp/dxA=>, we see that at this composition
  <math|x<A>/x<B>> must equal <math|y<A>/y<B>>, meaning that the liquid and
  gas phases have identical mole fraction compositions. The liquid with this
  composition is called an <index|Azeotrope><em|azeotrope>. The behavior of
  systems with azeotropes will be discussed in Sec. <reference|13-liq-gas
  nonideal>.

  <subsection|Gas solubility><label|12-gas sol>

  <I|Gas!solubility\|(><I|Solubility!gas@of a gas\|(>For the solution process
  B(g)<space|0.17em><ra><space|0.17em>B(sln), the general expression for the
  thermodynamic equilibrium constant is <math|K=a<B><sln>/a<B><gas>>.

  The activity of B in the gas phase is given by
  <math|a<B><gas>=<fug><B>/p<st>>. If the solute is a nonelectrolyte and we
  choose a standard state based on mole fraction, the activity in the
  solution is <math|a<B><sln>=<G><xbB><space|0.17em><g><xbB><space|0.17em>x<B>>.
  The equilibrium constant is then given by

  <\equation>
    K=<frac|<G><xbB><space|0.17em><g><xbB><space|0.17em>x<B>|<fug><B>/p<st>>
  </equation>

  and the solubility, expressed as the equilibrium mole fraction of solute in
  the solution, is given by

  <\gather>
    <tformat|<table|<row|<cell|x<B>=<frac|K<fug><B>/p<st>|<G><xbB><space|0.17em><g><xbB>><cond|(n*o*n*e*l*e*c*t*r*o*l*y*t*e*s*o*l*u*t*e*i*n><nextcond|e*q*u*i*l*i*b*r*i*u*m*w*i*t*h*g*a*s)><eq-number><label|xB=()fB/gB>>>>>
  </gather>

  At a fixed <math|T> and <math|p>, the values of <math|K> and
  <math|<G><xbB>> are constant. Therefore any change in the solution
  composition that increases the value of the activity coefficient
  <math|<g><xbB>> will decrease the solubility for the same gas fugacity.
  This solubility decrease is often what happens when a salt is dissolved in
  an aqueous solution, and is known as the <index|Salting-out effect on gas
  solubility><em|salting-out effect> (Prob. 12.<reference|prb:12-salting
  out>).

  Unless the pressure is much greater than <math|p<st>>, we can with
  negligible error set the pressure factor <math|<G><xbB>> equal to 1. When
  the gas solubility is low and the solution contains no other solutes, the
  activity coefficient <math|<g><xbB>> is close to 1. If furthermore we
  assume ideal gas behavior, then Eq. <reference|xB=()fB/gB> becomes

  <\gather>
    <tformat|<table|<row|<cell|x<B>=K*<frac|p<B>|p<st>><cond|(n*o*n*e*l*e*c*t*r*o*l*y*t*e*s*o*l*u*t*e*i*n*e*q*u*i*l*i*b*r*i*u*m><nextcond|w*i*t*h*i*d*e*a*l*g*a*s,<math|<G><xbB|=>1>,<math|<g><xbB|=>1>)><eq-number><label|xB=p/p^o>>>>>
  </gather>

  The solubility is predicted to be proportional to the partial pressure. The
  solubility of a gas that dissociates into ions in solution has a quite
  different dependence on partial pressure. An example is the solubility of
  gaseous HCl in water to form an electrolyte solution, shown in Fig.
  <reference|fig:10-HCl gas/soln><vpageref|fig:10-HCl gas/soln>.

  If the actual conditions are close to those assumed for Eq.
  <reference|xB=p/p^o>, we can use Eq. <reference|del(r)Hmo=(RT^2)dln(K)/dT>
  to derive an expression for the temperature dependence of the solubility
  for a fixed partial pressure of the gas:

  <\equation>
    <label|dlnxB/dT=DelH^o/RT^2><Pd|ln x<B>|T|<space|-0.17em><space|-0.17em>p<B>>=<frac|<dif>ln
    K|<dif>T>=<frac|<Delsub|s*o*l,B>H<st>|R*T<rsup|2>>
  </equation>

  At the standard pressure, <math|<Delsub|s*o*l,B>H<st>> is the same as the
  molar enthalpy of solution at infinite dilution.

  Since the dissolution of a gas in a liquid is invariably an exothermic
  process, <math|<Delsub|s*o*l,B>H<st>> is negative, and Eq.
  <reference|dlnxB/dT=DelH^o/RT^2> predicts the solubility decreases with
  increasing temperature.

  Note the similarity of Eq. <reference|dlnxB/dT=DelH^o/RT^2> and the
  expressions derived previously for the temperature dependence of the
  solubilities of solids (Eq. <reference|del(sol)Hmo(solid, gamma(x,B)=1)>)
  and liquids (Eq. <reference|d ln x(B)alpha/dT=>). When we substitute the
  mathematical identity <math|<dif>T=-T<rsup|2><dif><around|(|1/T|)>>, Eq.
  <reference|dlnxB/dT=DelH^o/RT^2> becomes

  <\equation>
    <bPd|ln x<B>|<around|(|1/T|)>|p<B>>=-<frac|<Delsub|s*o*l,B>H<st>|R>
  </equation>

  We can use this form to evaluate <math|<Delsub|s*o*l,B>H<st>> from a plot
  of <math|ln x<B>> versus <math|1/T>.

  The <I|Ideal solubility!gas@of a gas\|reg><I|Solubility!gas@of a
  gas!ideal\|reg><newterm|ideal solubility> of a gas is the solubility
  calculated on the assumption that the dissolved gas obeys Raoult's law for
  partial pressure: <math|p<B>=x<B>p<B><rsup|\<ast\>>>. The ideal solubility,
  expressed as a mole fraction, is then given as a function of partial
  pressure by

  <\gather>
    <tformat|<table|<row|<cell|x<B>=<frac|p<B>|p<B><rsup|\<ast\>>><cond|<around|(|i*d*e*a*l*s*o*l*u*b*i*l*i*t*y*o*f*a*g*a*s|)>><eq-number><label|id
    g solubility>>>>>
  </gather>

  Here <math|p<B><rsup|\<ast\>>> is the vapor pressure of pure liquid solute
  at the same temperature and total pressure as the solution. If the pressure
  is too low for pure B to exist as a liquid at this temperature, we can with
  little error replace <math|p<B><rsup|\<ast\>>> with the saturation vapor
  pressure of liquid B at the same temperature, because the effect of total
  pressure on the vapor pressure of a liquid is usually negligible (Sec.
  <reference|12-effect of p on fug>). If the temperature is above the
  critical temperature of pure B, we can estimate a hypothetical vapor
  pressure by extrapolating the liquid\Uvapor coexistence curve beyond the
  critical point.

  We can use Eq. <reference|id g solubility> to make several predictions
  regarding the ideal solubility of a gas at a fixed value of <math|p<B>>.

  <\enumerate>
    <item>The ideal solubility, expressed as a mole fraction, is independent
    of the kind of solvent.

    <item>The solubility expressed as a concentration, <math|c<B>>, is lower
    the greater is the molar volume of the solvent. This is because at
    constant <math|x<B>>, <math|c<B>> decreases as the solution volume
    increases.

    <item>The more volatile is the pure liquid solute at a particular
    temperature (i.e., the greater is <math|p<B><rsup|\<ast\>>>), the lower
    is the solubility.

    <item>The solubility decreases with increasing temperature, since
    <math|p<B><rsup|\<ast\>>> increases.
  </enumerate>

  Of course, these predictions apply only to solutions that behave
  approximately as ideal liquid mixtures, but even for many nonideal mixtures
  the predictions are found to have good agreement with experiment.

  <\quote-env>
    \ As an example of the general validity of prediction 1, Hildebrand and
    Scott<footnote|Ref. <cite|hildebrand-50>, Chap. XV.> list the following
    solubilities of gaseous Cl<rsub|<math|2>> in several dissimilar solvents
    at <math|0<units|<degC>>> and a partial pressure of <math|1.01<br>>:
    <math|x<B>=0.270> in heptane, <math|x<B>=0.288> in SiCl<rsub|<math|4>>,
    and <math|x<B>=0.298> in CCl<rsub|<math|4>>. These values are similar to
    one another and close to the ideal value
    <math|p<B>/p<B><rsup|\<ast\>>=0.273>.
  </quote-env>

  <I|Gas!solubility\|)><I|Solubility!gas@of a gas\|)>

  <subsection|Effect of temperature and pressure on Henry's law
  constants><label|12-effect of T \ p on Henry's law constants>

  Consider the solution process B(g)<space|0.17em><ra><space|0.17em>B(soln)
  for a nonelectrolyte solute B. The expression for the thermodynamic
  equilibrium constant, with a solute standard state based on mole fraction,
  is

  <\equation>
    K=<frac|a<B><sln>|a<B><gas>>=<frac|<G><xbB><space|0.17em><g><xbB><space|0.17em>x<B>|<fug><B>/p<st>>
  </equation>

  The Henry's law constant <math|<kHB>> is related to <math|<fug><B>> and
  <math|x<B>> by

  <\equation>
    <label|kB=fB/gammaB><kHB>=<frac|<fug><B>|<g><xbB><space|0.17em>x<B>>
  </equation>

  (see Table <reference|tbl:9-act coeff-fugacity>), and is therefore related
  to <math|K> as follows:

  <\gather>
    <tformat|<table|<row|<cell|<kHB>=<frac|<G><xbB><space|0.17em>p<st>|K><cond|<around|(|n*o*n*e*l*e*c*t*r*o*l*y*t*e*s*o*l*u*t*e|)>><eq-number><label|K(x,B)=Gamma(x,B)po/K>>>>>
  </gather>

  The pressure factor <math|<G><xbB>> is a function of <math|T> and <math|p>,
  and <math|K> is a function only of <math|T>. The value of <math|<kHB>>
  therefore depends on both <math|T> and <math|p>.

  At the standard pressure <math|p<st>=1<br>>, the value of <math|<G><xbB>>
  is unity, <I|Henry's law constant!effect of temperature on\|reg>and Eqs.
  <reference|del(r)Hmo=(RT^2)dln(K)/dT> and
  <reference|del(r)Hmo=-R*dln(K)/d(1/T)> then give the following expressions
  for the dependence of the dimensionless quantity <math|<kHB>/p<st>> on
  temperature:

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif>ln
    <around|(|<kHB>/p<st>|)>|<dif>T>=-<frac|<dif>ln
    K|<dif>T>=-<frac|<Delsub|s*o*l,B>H<st>|R*T<rsup|2>><cond|<around|(|<math|p=p<st>>|)>><eq-number>>>>>
  </gather>

  <\gather>
    <tformat|<table|<row|<cell|<frac|<dif>ln
    <around|(|<kHB>/p<st>|)>|<dif><around|(|1/T|)>>=-<frac|<dif>ln
    K|<dif><around|(|1/T|)>>=<frac|<Delsub|s*o*l,B>H<st>|R><cond|<around|(|<math|p=p<st>>|)>><eq-number><label|dln(k/po)/d(1/T)=>>>>>
  </gather>

  These expressions can be used with little error at any pressure that is not
  much greater than <math|p<st>>, say up to at least <math|2<br>>, because
  under these conditions <math|<G><xbB>> does not differ appreciably from
  unity (page <pageref|pressure factor omitted>).

  <I|Henry's law constant!effect of pressure on\|reg>To find the dependence
  of <math|<kHB>> on pressure, we substitute <math|<G><xbB>> in Eq.
  <reference|K(x,B)=Gamma(x,B)po/K> with the expression for <math|<G><xbB>>
  at pressure <math|p<rprime|'>> found in Table <reference|tbl:9-Gamma_i>:

  <\equation>
    <label|k_H,B(p')=><kHB><around|(|p<rprime|'>|)>=<frac|<G><xbB><around|(|p<rprime|'>|)>*<space|0.17em>p<st>|K>=<frac|p<st>|K>*exp
    <around*|(|<big|int><rsub|p<st>><rsup|p<rprime|'>><frac|V<B><rsup|\<infty\>>|R*T><difp>|)>
  </equation>

  We can use Eq. <reference|k_H,B(p')=> to compare the values of <math|<kHB>>
  at the same temperature and two different pressures, <math|p<rsub|1>> and
  <math|p<rsub|2>>:

  <\equation>
    <kHB><around|(|p<rsub|2>|)>=<kHB><around|(|p<rsub|1>|)>*exp
    <around*|(|<big|int><rsub|p<rsub|1>><rsup|p<rsub|2>><frac|V<B><rsup|\<infty\>>|R*T><difp>|)>
  </equation>

  An approximate version of this relation, found by treating
  <math|V<B><rsup|\<infty\>>> as independent of pressure, is

  <\equation>
    <label|KmB(p2) approx><kHB><around|(|p<rsub|2>|)>\<approx\><kHB><around|(|p<rsub|1>|)>*exp
    <around*|[|<frac|V<B><rsup|\<infty\>><around|(|p<rsub|2>-p<rsub|1>|)>|R*T>|]>
  </equation>

  Unless <math|<around|\||p<rsub|2>-p<rsub|1>|\|>> is much greater than
  <math|1<br>>, the effect of pressure on <math|<kHB>> is small; see Prob.
  12.<reference|prb:12-p effect> for an example.
  <I|Equilibrium!liquid--gas\|)>

  <section|Reaction Equilibria>

  <I|Equilibrium!reaction\|(><I|Reaction!equilibrium\|(>The definition of the
  thermodynamic equilibrium constant of a reaction or other chemical process
  is given by Eq. <reference|K=prod(a_i)^(nu_i)>:

  <\equation>
    <label|K=>K=<big|prod><rsub|i><around|(|a<rsub|i>|)><eq><rsup|\<nu\><rsub|i>>
  </equation>

  The activity <math|a<rsub|i>> of each reactant or product species is based
  on an appropriate standard state. We can replace each
  <index|Activity>activity on the right side of Eq. <reference|K=> by an
  expression in Table <reference|tbl:12-activities><vpageref|tbl:12-activities>.

  <\big-table>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|2|2|1|-1|cell-valign|top>|<cwith|2|2|1|-1|cell-vmode|exact>|<cwith|2|2|1|-1|cell-height|<plus|1fn|-1.5ex>>|<cwith|3|3|1|-1|cell-valign|top>|<cwith|3|3|1|-1|cell-vmode|exact>|<cwith|3|3|1|-1|cell-height|<plus|1fn|3mm>>|<cwith|5|5|1|-1|cell-valign|top>|<cwith|5|5|1|-1|cell-vmode|exact>|<cwith|5|5|1|-1|cell-height|<plus|1fn|6mm>>|<cwith|6|6|1|-1|cell-valign|top>|<cwith|6|6|1|-1|cell-vmode|exact>|<cwith|6|6|1|-1|cell-height|<plus|1fn|6mm>>|<cwith|7|7|1|-1|cell-valign|top>|<cwith|7|7|1|-1|cell-vmode|exact>|<cwith|7|7|1|-1|cell-height|<plus|1fn|2mm>>|<cwith|8|8|1|-1|cell-valign|top>|<cwith|8|8|1|-1|cell-vmode|exact>|<cwith|8|8|1|-1|cell-height|<plus|1fn|4mm>>|<cwith|9|9|1|-1|cell-valign|top>|<cwith|9|9|1|-1|cell-vmode|exact>|<cwith|9|9|1|-1|cell-height|<plus|1fn|4mm>>|<cwith|10|10|1|-1|cell-valign|top>|<cwith|10|10|1|-1|cell-vmode|exact>|<cwith|10|10|1|-1|cell-height|<plus|1fn|3mm>>|<cwith|11|11|1|-1|cell-valign|top>|<cwith|11|11|1|-1|cell-vmode|exact>|<cwith|11|11|1|-1|cell-height|<plus|1fn|3mm>>|<cwith|12|12|1|-1|cell-valign|top>|<cwith|12|12|1|-1|cell-vmode|exact>|<cwith|12|12|1|-1|cell-height|<plus|1fn|3mm>>|<cwith|12|12|1|-1|cell-bborder|1ln>|<table|<row|<cell|Species>|<cell|Activity>>|<row|<cell|>|<cell|>>|<row|<cell|Pure
    gas>|<cell|<math|<D><s|a<gas>=<frac|<fug>|p<st>>>>>>|<row|<cell|Pure
    liquid or solid>|<cell|<math|<s|a=<G>>>>>|<row|<cell|Substance <math|i>
    in a gas mixture>|<cell|<math|<D><s|a<rsub|i><gas>=<frac|<fug><rsub|i>|p<st>>>>>>|<row|<cell|Substance
    <math|i> in a liquid or solid mixture>|<cell|<math|<D><s|a<rsub|i>=<G><rsub|i><space|0.17em><g><rsub|i><space|0.17em>x<rsub|i>>>>>|<row|<cell|Solvent
    A of a solution>|<cell|<math|<D><s|a<A>=<G><A><space|0.17em><g><A><space|0.17em>x<A>>>>>|<row|<cell|Nonelectrolyte
    solute B, mole fraction basis>|<cell|<math|<D><s|a<xbB>=<G><xbB><space|0.17em><g><xbB><space|0.17em>x<B>>>>>|<row|<cell|Nonelectrolyte
    solute B, concentration basis>|<cell|<math|<D><s|a<cbB>=<G><cbB><space|0.17em><g><cbB><frac|c<B>|c<st>>>>>>|<row|<cell|Nonelectrolyte
    solute B, molality basis>|<cell|<math|<D><s|a<mbB>=<G><mbB><space|0.17em><g><mbB><frac|m<B>|m<st>>>>>>|<row|<cell|Electrolyte
    solute B>|<cell|<math|<D><s|a<mbB>=<G><mbB><space|0.17em><g><rsub|\<pm\>><rsup|\<nu\>><around*|(|<frac|m<rsub|+>|m<st>>|)><rsup|\<nu\><rsub|+>><around*|(|<frac|m<rsub|->|m<st>>|)><rsup|\<nu\><rsub|->>>>>>|<row|<cell|Ion
    in solution>|<cell|<math|<D><s|a<rsub|+>=<G><rsub|+><g><rsub|+><frac|m<rsub|+>|m<st>>*<space|2em>a<rsub|->=<G><rsub|-><g><rsub|-><frac|m<rsub|->|m<st>>>>>>|<row|<cell|>|<cell|>>>>>
  </big-table|<label|tbl:12-activities>Expressions for activities (from Table
  <reference|tbl:9-activities> and Eqs. <reference|a(+)=...> and
  <reference|a(B)(multisolute)>)>

  For example, consider the following heterogeneous equilibrium that is
  important in the formation of limestone caverns:

  <\equation*>
    <chem>C*a*C*O<rsub|3><around|(|c*r,<space|0.17em>c*a*l*c*i*t*e|)>+C*O<rsub|2><around|(|g|)>+H<rsub|2>*O<around|(|s*l*n|)><arrows>C*a<rsup|2+>*<around|(|a*q|)>+2*H*C*O<rsub|3><rsup|-><around|(|a*q|)>
  </equation*>

  If we treat H<rsub|<math|2>>O as a solvent and Ca<rsup|<math|2+>> and
  HCO<math|<rsub|3><rsup|->> as the solute species, then we write the
  thermodynamic equilibrium constant as follows:

  <\equation>
    <label|K(cave)>K=<frac|a<rsub|+>*<space|0.17em>a<rsub|-><rsup|2>|a<subs|C*a*C*O<rsub|<math|3>>><space|0.17em>a<subs|C*O<rsub|<math|2>>><space|0.17em>a<subs|H<rsub|<math|2>>*O>>=<G><subs|r><space|0.17em><frac|<g><rsub|+><g><rsub|-><rsup|2>m<rsub|+>*m<rsub|-><rsup|2>/<around|(|m<st>|)><rsup|3>|<around*|(|<fug><subs|C*O<rsub|<math|2>>>/p<st>|)><g><subs|H<rsub|<math|2>>*O><space|0.17em>x<subs|H<rsub|<math|2>>*O>>
  </equation>

  The subscripts <math|+> and <math|-> refer to the Ca<rsup|<math|2+>> and
  HCO<math|<rsub|3><rsup|->> ions, and all quantities are for the system at
  reaction equilibrium. <math|<G><subs|r>> is the proper quotient of pressure
  factors, given for this reaction by<footnote|The product
  <math|<G><rsub|+><G><rsub|-><rsup|2>> in the numerator of Eq.
  <reference|Gamma_r(cave)> is the pressure factor <math|<G><mbB>> for the
  solute Ca(HCO<rsub|<math|3>>)<rsub|<math|2>> (see Eq.
  <reference|Gamma(m,B)=Gamma+^(nu+)Gamma-^(nu-)><vpageref|Gamma(m,B)=Gamma+<rsup|(>nu+)Gamma-<rsup|(>nu-)>).>

  <\equation>
    <label|Gamma_r(cave)><G><subs|r>=<frac|<G><rsub|+><G><rsub|-><rsup|2>|<G><subs|C*a*C*O<rsub|<math|3>>><G><subs|H<rsub|<math|2>>*O>>
  </equation>

  Unless the pressure is very high, we can with little error set the value of
  <math|<G><subs|r>> equal to unity.

  Equation <reference|K(cave)> is an example of a <subindex|Equilibrium
  constant|mixed>\Pmixed\Q equilibrium constant\Vone using more than one kind
  of standard state. From the definition of the mean ionic activity
  coefficient (Eq. <reference|g(+-)^nu=...>), we can replace the product
  <math|<g><rsub|+><g><rsub|-><rsup|2>> by <math|<g><rsub|\<pm\>><rsup|3>>,
  where <math|<g><rsub|\<pm\>>> is the mean ionic activity coefficient of
  aqueous Ca(HCO<rsub|<math|3>>)<rsub|<math|2>>:

  <\equation>
    <label|K'(cave)>K=<G><subs|r><space|0.17em><frac|<g><rsub|\<pm\>><rsup|3><space|0.17em>m<rsub|+>*m<rsub|-><rsup|2>/<around|(|m<st>|)><rsup|3>|<around*|(|<fug><subs|C*O<rsub|<math|2>>>/p<st>|)><g><subs|H<rsub|<math|2>>*O><space|0.17em>x<subs|H<rsub|<math|2>>*O>>
  </equation>

  Instead of treating the aqueous Ca<rsup|<math|2+>> and
  HCO<math|<rsub|3><rsup|->> ions as solute species, we can regard the
  dissolved Ca(HCO<rsub|<math|3>>)<rsub|<math|2>> electrolyte as the solute
  and write

  <\equation>
    K=<frac|a<mbB>|a<subs|C*a*C*O<rsub|<math|3>>><space|0.17em>a<subs|C*O<rsub|<math|2>>><space|0.17em>a<subs|H<rsub|<math|2>>*O>>
  </equation>

  We then obtain Eq. <reference|K'(cave)> by replacing <math|a<mbB>> with the
  expression in Table <reference|tbl:12-activities> for an electrolyte
  solute.

  The value of <math|K> depends only on <math|T>, and the value of
  <math|<G><subs|r>> depends only on <math|T> and <math|p>. Suppose we
  dissolve some NaCl in the aqueous phase while maintaining the system at
  constant <math|T> and <math|p>. The increase in the <subindex|Ionic
  strength|effect on reaction equilibrium>ionic strength will alter
  <math|<g><rsub|\<pm\>>> and necessarily cause a compensating change in the
  solute molarity in order for the system to remain in reaction equilibrium.

  An example of a different kind of reaction equilibrium is the dissociation
  (ionization) of a weak monoprotic acid such as acetic acid

  <\equation*>
    <chem>H*A*<around|(|a*q|)><arrows>H<rsup|+>*<around|(|a*q|)>+A<rsup|->*<around|(|a*q|)>
  </equation*>

  for which the thermodynamic equilibrium constant (the <index|Acid
  dissociation constant><em|acid dissociation constant>) is

  <\equation>
    K<subs|a>=<G><subs|r><space|0.17em><frac|<g><rsub|+><g><rsub|->m<rsub|+>*m<rsub|->|<g><subs|<math|m>,HA><space|0.17em>m<subs|H*A>m<st>>=<G><subs|r><space|0.17em><frac|<g><rsub|\<pm\>><rsup|2><space|0.17em>m<rsub|+>*m<rsub|->|<g><subs|<math|m>,HA><space|0.17em>m<subs|H*A>m<st>>
  </equation>

  Suppose the solution is prepared from water and the acid, and
  H<rsup|<math|+>> from the dissociation of H<rsub|<math|2>>O is negligible
  compared to H<rsup|<math|+>> from the acid dissociation. We may then write
  <math|m<rsub|+>=m<rsub|->=\<alpha\>*m<B>>, where <math|\<alpha\>> is the
  <index|Degree of dissociation>degree of dissociation and <math|m<B>> is the
  overall molality of the acid. The molality of the undissociated acid is
  <math|m<subs|H*A>=<around|(|1-\<alpha\>|)>*m<B>>, and the dissociation
  constant can be written

  <\equation>
    <label|Ka>K<subs|a>=<G><subs|r><space|0.17em><frac|<g><rsub|\<pm\>><rsup|2><space|0.17em>\<alpha\><rsup|2>*m<B>/m<st>|<g><subs|<math|m>,HA><around|(|1-\<alpha\>|)>>
  </equation>

  From this equation, we see that a change in the ionic strength that
  decreases <math|<g><rsub|\<pm\>>> when <math|T>, <math|p>, and <math|m<B>>
  are held constant must increase the degree of dissociation (Prob.
  12.<reference|prb:12-formic acid>).

  <I|Equilibrium!reaction\|)><I|Reaction!equilibrium\|)>

  <section|Evaluation of Standard Molar Quantities>

  <I|Standard molar!quantity!evaluation of\|(>Some of the most useful
  experimentally-derived data for thermodynamic calculations are values of
  <subsubindex|Enthalpy|reaction|standard molar>standard molar reaction
  enthalpies, <subindex|Gibbs energy|reaction, standard molar>standard molar
  reaction Gibbs energies, and <subsubindex|Entropy|reaction|standard
  molar>standard molar reaction entropies. The values of these quantities for
  a given reaction are related, as we know (Eq.
  <reference|del(r)Gmo=del(r)Hmo-Tdel(r)Smo>), by

  <\equation>
    <label|del(r)G^o=>\<Delta\><rsub|<text|r>>*G<st>=\<Delta\><rsub|<text|r>>*H<st>-T\<Delta\><rsub|<text|r>>*S<st>
  </equation>

  and <math|\<Delta\><rsub|<text|r>>*S<st>> can be calculated from the
  <subindex|Entropy|standard molar>standard molar entropies of the reactants
  and products using Eq. <reference|del(r)Smo=sum(nu_i)Smio>:

  <\equation>
    <label|del(r)S^o=>\<Delta\><rsub|<text|r>>*S<st>=<big|sum><rsub|i>\<nu\><rsub|i>*S<rsub|i><st>
  </equation>

  The standard molar quantities appearing in Eqs. <reference|del(r)G^o=> and
  <reference|del(r)S^o=> can be evaluated through a variety of experimental
  techniques. <subindex|Calorimetry|reaction>Reaction calorimetry can be used
  to evaluate <math|\<Delta\><rsub|<text|r>>*H<st>> for a reaction (Sec.
  <reference|11-rxn calorimetry>). Calorimetric measurements of heat capacity
  and phase-transition enthalpies can be used to obtain the value of
  <math|S<rsub|i><st>> for a solid or liquid (Sec. <reference|6-third law
  molar entropies>). For a gas, spectroscopic measurements can be used to
  evaluate <math|S<rsub|i><st>> (Sec. <reference|6-S of gas from stat mech>).
  Evaluation of a thermodynamic equilibrium constant and its temperature
  derivative, for any of the kinds of equilibria discussed in this chapter
  (vapor pressure, solubility, chemical reaction, etc.), can provide values
  of <math|\<Delta\><rsub|<text|r>>*G<st>> and
  <math|\<Delta\><rsub|<text|r>>*H<st>> through the relations
  <math|\<Delta\><rsub|<text|r>>*G<st>=-R*T*ln K> and
  <math|\<Delta\><rsub|<text|r>>*H<st>=-R<dif>ln K/<dif><around|(|1/T|)>>.

  In addition to these methods, measurements of cell potentials are useful
  for a reaction that can be carried out reversibly in a galvanic cell.
  Section <reference|14-st molar rxn quantities> will describe how the
  standard cell potential and its temperature derivative allow
  <math|\<Delta\><rsub|<text|r>>*H<st>>, <math|\<Delta\><rsub|<text|r>>*G<st>>,
  and <math|\<Delta\><rsub|<text|r>>*S<st>> to be evaluated for such a
  reaction.

  An efficient way of tabulating the results of experimental measurements is
  in the form of standard molar enthalpies and Gibbs energies of
  <em|formation>. These values can be used to generate the values of standard
  molar reaction quantities for reactions not investigated directly. The
  relations between standard molar reaction and formation quantities (Sec.
  <reference|11-st molar enthalpy of formation>) are

  <\equation>
    \<Delta\><rsub|<text|r>>*H<st>=<big|sum><rsub|i>\<nu\><rsub|i><Delsub|f>H<st><around|(|i|)><space|2em>\<Delta\><rsub|<text|r>>*G<st>=<big|sum><rsub|i>\<nu\><rsub|i><Delsub|f>G<st><around|(|i|)>
  </equation>

  and for ions the conventions used are

  <\equation>
    <Delsub|f>H<st><tx|<around|(|H<rsup|<math|+>>,<space|0.17em>aq|)>>=0<space|2em><Delsub|f>G<st><tx|<around|(|H<rsup|<math|+>>,<space|0.17em>aq|)>>=0*<space|2em>S<m><st><tx|<around|(|H<rsup|<math|+>>,<space|0.17em>aq|)>>=0
  </equation>

  Appendix <reference|app:props> gives an abbreviated set of values of
  <math|<Delsub|f>H<st>>, <math|S<m><st>>, and <math|<Delsub|f>G<st>> at
  <math|298.15<K>>.

  For examples of the evaluation of standard molar reaction quantities and
  standard molar formation quantities from measurements made by various
  experimental techniques, see Probs. 12.<reference|prb:12-Cl
  ion>\U12.<reference|prb:12-AgCl pptn>, 14.<reference|prb:14-1/2H2+AgCl-\<gtr\>>,
  and 14.<reference|prb:14-AgCl formation>. <I|Standard
  molar!quantity!evaluation of\|)>

  <new-page><phantomsection><addcontentsline|toc|section|Problems>
  <paragraphfootnotes><problems| <input|12-problems><page-break>>
  <plainfootnotes>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|(xA/fA)dfA=-(xB/fB)dfB|<tuple|1.8.6|?>>
    <associate|-a muA=|<tuple|1.5.12|?>>
    <associate|-a*d(muA/T)-b*d(muB/T)+d(mu/T)=0|<tuple|1.5.13|?>>
    <associate|-del(sol)HmA/T^2 dT=|<tuple|1.3.5|?>>
    <associate|12-Donnan eqm|<tuple|1.7.3|?>>
    <associate|12-Duhem-Margules eqn|<tuple|1.8.3|?>>
    <associate|12-binary mixt in eqm with pure phase|<tuple|1.3|?>>
    <associate|12-boiling-point elevation|<tuple|1.4.2|?>>
    <associate|12-colligative properties|<tuple|1.4|?>>
    <associate|12-effect of T \ p on Henry's law constants|<tuple|1.8.5|?>>
    <associate|12-effect of p on fug|<tuple|1.8.1|?>>
    <associate|12-effects of T|<tuple|1.1|?>>
    <associate|12-electrolyte solubility|<tuple|1.5.5|?>>
    <associate|12-fr pts, ideal l mixts|<tuple|1.5.1|?>>
    <associate|12-freezing-point depression|<tuple|1.4.1|?>>
    <associate|12-freezing-point measurements|<tuple|1.2.1|?>>
    <associate|12-gas sol|<tuple|1.8.4|?>>
    <associate|12-l-l eqm|<tuple|1.6|?>>
    <associate|12-l/gas eq|<tuple|1.8|?>>
    <associate|12-osmotic membrane eqm|<tuple|1.7.1|?>>
    <associate|12-osmotic p measurements|<tuple|1.2.2|?>>
    <associate|12-osmotic pressure|<tuple|1.4.4|?>>
    <associate|12-partial pressure over a liquid|<tuple|1.8.2|?>>
    <associate|12-sol of l|<tuple|1.6.2|?>>
    <associate|12-solid cmpds|<tuple|1.5.4|?>>
    <associate|12-solid l eqm|<tuple|1.5|?>>
    <associate|12-solute distribution|<tuple|1.6.3|?>>
    <associate|12-solvent mu from phase eq|<tuple|1.2|?>>
    <associate|12-vapor-pressure lowering|<tuple|1.4.3|?>>
    <associate|12-variation of lnK with T|<tuple|1.1.3|?>>
    <associate|12-variation of mu/T with T|<tuple|1.1.1|?>>
    <associate|12-variation of muo/T with T|<tuple|1.1.2|?>>
    <associate|Chap. 12|<tuple|1|?>>
    <associate|Donnan eq 1|<tuple|1.7.9|?>>
    <associate|Donnan pot|<tuple|1.7.7|?>>
    <associate|Donnan product|<tuple|1.7.12|?>>
    <associate|Donnan product with act coeffs|<tuple|1.7.6|?>>
    <associate|Duhem-Margules|<tuple|1.8.18|?>>
    <associate|Duhem-Margules,ig|<tuple|1.8.19|?>>
    <associate|Gamma_r(cave)|<tuple|1.9.3|?>>
    <associate|K'(cave)|<tuple|1.9.4|?>>
    <associate|K'=|<tuple|1.6.8|?>>
    <associate|K(cave)|<tuple|1.9.2|?>>
    <associate|K(x,B)=Gamma(x,B)po/K|<tuple|1.8.30|?>>
    <associate|K=|<tuple|1.9.1|?>>
    <associate|Ka|<tuple|1.9.7|?>>
    <associate|KmB(p2) approx|<tuple|1.8.35|?>>
    <associate|Ks, common cation|<tuple|1.5.27|?>>
    <associate|Ks, no common ion|<tuple|1.5.26|?>>
    <associate|Ks=|<tuple|1.5.25|?>>
    <associate|Pi/cB=RT|<tuple|1.4.24|?>>
    <associate|Pi/mB=RT*MA/VA|<tuple|1.4.25|?>>
    <associate|Pi=()cB|<tuple|1.4.26|?>>
    <associate|Pi=()mB|<tuple|1.4.27|?>>
    <associate|Pi=./.|<tuple|1.4.21|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|Enthalpy|?>>
    <associate|auto-100|<tuple|1.7.1|?>>
    <associate|auto-101|<tuple|1.8|?>>
    <associate|auto-102|<tuple|1.8.1|?>>
    <associate|auto-103|<tuple|Fugacity|?>>
    <associate|auto-104|<tuple|Poynting factor|?>>
    <associate|auto-105|<tuple|Poynting factor|?>>
    <associate|auto-106|<tuple|1.8.2|?>>
    <associate|auto-107|<tuple|Gibbs--Duhem equation|?>>
    <associate|auto-108|<tuple|Solvent|?>>
    <associate|auto-109|<tuple|Ideal-dilute solution|?>>
    <associate|auto-11|<tuple|Vaporization|?>>
    <associate|auto-110|<tuple|1.8.1|?>>
    <associate|auto-111|<tuple|1.8.2|?>>
    <associate|auto-112|<tuple|1.8.3|?>>
    <associate|auto-113|<tuple|Duhem--Margules equation|?>>
    <associate|auto-114|<tuple|Azeotrope|?>>
    <associate|auto-115|<tuple|1.8.4|?>>
    <associate|auto-116|<tuple|Salting-out effect on gas solubility|?>>
    <associate|auto-117|<tuple|ideal solubility|?>>
    <associate|auto-118|<tuple|1.8.5|?>>
    <associate|auto-119|<tuple|1.9|?>>
    <associate|auto-12|<tuple|Clausius--Clapeyron equation|?>>
    <associate|auto-120|<tuple|Activity|?>>
    <associate|auto-121|<tuple|1.9.1|?>>
    <associate|auto-122|<tuple|Equilibrium constant|?>>
    <associate|auto-123|<tuple|Ionic strength|?>>
    <associate|auto-124|<tuple|Acid dissociation constant|?>>
    <associate|auto-125|<tuple|Degree of dissociation|?>>
    <associate|auto-126|<tuple|1.10|?>>
    <associate|auto-127|<tuple|Enthalpy|?>>
    <associate|auto-128|<tuple|Gibbs energy|?>>
    <associate|auto-129|<tuple|Entropy|?>>
    <associate|auto-13|<tuple|1.1.1|?>>
    <associate|auto-130|<tuple|Entropy|?>>
    <associate|auto-131|<tuple|Calorimetry|?>>
    <associate|auto-14|<tuple|1.2|?>>
    <associate|auto-15|<tuple|Solvent|?>>
    <associate|auto-16|<tuple|1.2.1|?>>
    <associate|auto-17|<tuple|1.2.1|?>>
    <associate|auto-18|<tuple|1.2.2|?>>
    <associate|auto-19|<tuple|1.2.2|?>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-20|<tuple|Membrane, semipermeable|?>>
    <associate|auto-21|<tuple|Osmosis|?>>
    <associate|auto-22|<tuple|Osmotic pressure|?>>
    <associate|auto-23|<tuple|osmotic pressure|?>>
    <associate|auto-24|<tuple|1.3|?>>
    <associate|auto-25|<tuple|1.4|?>>
    <associate|auto-26|<tuple|Colligative property|?>>
    <associate|auto-27|<tuple|colligative properties|?>>
    <associate|auto-28|<tuple|Boiling point|?>>
    <associate|auto-29|<tuple|Vapor pressure|?>>
    <associate|auto-3|<tuple|1.1.1|?>>
    <associate|auto-30|<tuple|Osmotic pressure|?>>
    <associate|auto-31|<tuple|1.4.1|?>>
    <associate|auto-32|<tuple|1.4.1|?>>
    <associate|auto-33|<tuple|Molal freezing-point depression constant|?>>
    <associate|auto-34|<tuple|molal freezing-point depression constant|?>>
    <associate|auto-35|<tuple|Cryoscopic constant|?>>
    <associate|auto-36|<tuple|1.4.2|?>>
    <associate|auto-37|<tuple|Molal boiling-point elevation constant|?>>
    <associate|auto-38|<tuple|molal boiling-point elevation constant|?>>
    <associate|auto-39|<tuple|Ebullioscopic constant|?>>
    <associate|auto-4|<tuple|Gibbs--Helmholtz equation|?>>
    <associate|auto-40|<tuple|1.4.3|?>>
    <associate|auto-41|<tuple|1.4.4|?>>
    <associate|auto-42|<tuple|van't Hoff's equation|?>>
    <associate|auto-43|<tuple|Osmotic coefficient|?>>
    <associate|auto-44|<tuple|1.5|?>>
    <associate|auto-45|<tuple|Freezing point|?>>
    <associate|auto-46|<tuple|Solubility|?>>
    <associate|auto-47|<tuple|1.5.1|?>>
    <associate|auto-48|<tuple|1.5.1|?>>
    <associate|auto-49|<tuple|1.5.2|?>>
    <associate|auto-5|<tuple|1.1.2|?>>
    <associate|auto-50|<tuple|1.5.2|?>>
    <associate|auto-51|<tuple|Saturated solution|?>>
    <associate|auto-52|<tuple|Solution|?>>
    <associate|auto-53|<tuple|saturated|?>>
    <associate|auto-54|<tuple|solubility|?>>
    <associate|auto-55|<tuple|1.5.3|?>>
    <associate|auto-56|<tuple|ideal solubility|?>>
    <associate|auto-57|<tuple|1.5.3|?>>
    <associate|auto-58|<tuple|1.5.4|?>>
    <associate|auto-59|<tuple|Solid compound|?>>
    <associate|auto-6|<tuple|1.1.3|?>>
    <associate|auto-60|<tuple|solid compound|?>>
    <associate|auto-61|<tuple|Gibbs--Duhem equation|?>>
    <associate|auto-62|<tuple|1.5.3|?>>
    <associate|auto-63|<tuple|1.5.5|?>>
    <associate|auto-64|<tuple|Solubility product|?>>
    <associate|auto-65|<tuple|solubility product|?>>
    <associate|auto-66|<tuple|Common ion effect|?>>
    <associate|auto-67|<tuple|Solubility product|?>>
    <associate|auto-68|<tuple|1.6|?>>
    <associate|auto-69|<tuple|1.6.1|?>>
    <associate|auto-7|<tuple|Equilibrium constant|?>>
    <associate|auto-70|<tuple|Phase|?>>
    <associate|auto-71|<tuple|1.6.2|?>>
    <associate|auto-72|<tuple|1.6.2.1|?>>
    <associate|auto-73|<tuple|1.6.1|?>>
    <associate|auto-74|<tuple|1.6.2.2|?>>
    <associate|auto-75|<tuple|1.6.3|?>>
    <associate|auto-76|<tuple|Nernst|?>>
    <associate|auto-77|<tuple|Nernst distribution law|?>>
    <associate|auto-78|<tuple|Partition coefficient|?>>
    <associate|auto-79|<tuple|partition coefficient|?>>
    <associate|auto-8|<tuple|Enthalpy|?>>
    <associate|auto-80|<tuple|Distribution coefficient|?>>
    <associate|auto-81|<tuple|distribution coefficient|?>>
    <associate|auto-82|<tuple|1.7|?>>
    <associate|auto-83|<tuple|1.7.1|?>>
    <associate|auto-84|<tuple|Osmotic membrane equilibrium|?>>
    <associate|auto-85|<tuple|Membrane equilibrium|?>>
    <associate|auto-86|<tuple|osmotic membrane equilibrium|?>>
    <associate|auto-87|<tuple|Osmotic pressure|?>>
    <associate|auto-88|<tuple|1.7.2|?>>
    <associate|auto-89|<tuple|Equilibrium|?>>
    <associate|auto-9|<tuple|van't Hoff equation|?>>
    <associate|auto-90|<tuple|Dialysis, equilibrium|?>>
    <associate|auto-91|<tuple|1.7.3|?>>
    <associate|auto-92|<tuple|Donnan|?>>
    <associate|auto-93|<tuple|Membrane equilibrium|?>>
    <associate|auto-94|<tuple|Donnan membrane equilibrium|?>>
    <associate|auto-95|<tuple|Electric|?>>
    <associate|auto-96|<tuple|Donnan|?>>
    <associate|auto-97|<tuple|Donnan potential|?>>
    <associate|auto-98|<tuple|1.7.3.1|?>>
    <associate|auto-99|<tuple|1.7.3.2|?>>
    <associate|binary azeotrope|<tuple|<with|mode|<quote|math>|\<bullet\>>|?>>
    <associate|colligative properties|<tuple|colligative properties|?>>
    <associate|d ln x(B)alpha/dT=|<tuple|1.6.3|?>>
    <associate|d(lnK)/dT=-(1/R)sum nu_i d(mu_io/T)/dT|<tuple|1.1.10|?>>
    <associate|d(lnK)/dT=del(r)Hmo/RT2|<tuple|1.1.12|?>>
    <associate|d(lnK)/dT=del(r)Hmo/RT2-...|<tuple|1.1.11|?>>
    <associate|d(mu(i)o)/dT=|<tuple|1.1.5|?>>
    <associate|d(mu/T)/dT=|<tuple|1.1.1|?>>
    <associate|d(mu/T)/dxA-\<gtr\>R|<tuple|1.4.4|?>>
    <associate|d(mu_Bo/T)/dT=-H_Bo/T^2+alphaA|<tuple|1.1.8|?>>
    <associate|d(mu_i/T)/dT=-H_i/T^2|<tuple|1.1.3|?>>
    <associate|d(mu_io/T)/dT=-H_io/T^2|<tuple|1.1.6|?>>
    <associate|dT(f)/dxA=|<tuple|1.4.1|?>>
    <associate|dT(f)/dxA=RT2/del(fus)Hma|<tuple|1.4.5|?>>
    <associate|dT/dxA (solid cmpd,1)|<tuple|1.5.17|?>>
    <associate|dT/dxA (solid cmpd,2)|<tuple|1.5.19|?>>
    <associate|dT/dxA (solid cmpd,3)|<tuple|1.5.20|?>>
    <associate|dT/dxA l-solid eqm|<tuple|1.3.6|?>>
    <associate|dT/dxA repeat|<tuple|1.5.1|?>>
    <associate|dTf/dxA=RTf^2/xA del(sol)Hm|<tuple|1.5.2|?>>
    <associate|del T(f) =|<tuple|1.4.10|?>>
    <associate|del(Tb) = Kb mB|<tuple|1.4.17|?>>
    <associate|del(Tf) = -nu Kf mB|<tuple|1.4.13|?>>
    <associate|del(r)G^o=|<tuple|1.10.1|?>>
    <associate|del(r)Hmo=(RT^2)dln(K)/dT|<tuple|1.1.13|?>>
    <associate|del(r)Hmo=-R*dln(K)/d(1/T)|<tuple|1.1.14|?>>
    <associate|del(r)S^o=|<tuple|1.10.2|?>>
    <associate|del(sol)H=del(sol)C(T-Tf)|<tuple|1.2.4|?>>
    <associate|del(sol)Hmo(solid)|<tuple|1.5.7|?>>
    <associate|del(sol)Hmo(solid, gamma(c,B)=1)|<tuple|1.5.10|?>>
    <associate|del(sol)Hmo(solid, gamma(x,B)=1)|<tuple|1.5.8|?>>
    <associate|dfB/dxB+a=fB/xB|<tuple|1.8.13|?>>
    <associate|dfB/dxB\<less\>=fB/xB|<tuple|1.8.12|?>>
    <associate|dfB/dxB=k|<tuple|1.8.8|?>>
    <associate|dln(f_i/po)/dp=V_i(l)/RT|<tuple|1.8.1|?>>
    <associate|dln(k/po)/d(1/T)=|<tuple|1.8.32|?>>
    <associate|dlnxB/dT=DelH^o/RT^2|<tuple|1.8.25|?>>
    <associate|dmuA*/T=|<tuple|1.3.3|?>>
    <associate|dmuA/T=|<tuple|1.3.2|?>>
    <associate|dp/dxA l-solid eqm|<tuple|1.3.7|?>>
    <associate|dp/dxA=|<tuple|1.8.21|?>>
    <associate|dpB/dxA=|<tuple|1.8.20|?>>
    <associate|dxA, inf diln|<tuple|1.4.7|?>>
    <associate|fA=xA fA*|<tuple|1.8.11|?>>
    <associate|fB=k xB|<tuple|1.8.7|?>>
    <associate|f_i(p2) approx|<tuple|1.8.3|?>>
    <associate|f_i(p2)=f_i(p1)exp[int(V_i/RT)dp]|<tuple|1.8.2|?>>
    <associate|fig:11-Donnan|<tuple|1.7.1|?>>
    <associate|fig:12-EtOH-CHCl3 fug|<tuple|1.8.2|?>>
    <associate|fig:12-EtOH-H2O fug|<tuple|1.8.1|?>>
    <associate|fig:12-Tf-xB|<tuple|1.5.2|?>>
    <associate|fig:12-benz-sol|<tuple|1.5.1|?>>
    <associate|fig:12-butylbenzene|<tuple|1.6.1|?>>
    <associate|fig:12-freezing pt|<tuple|1.2.1|?>>
    <associate|fig:12-ideal solubility|<tuple|1.5.3|?>>
    <associate|fig:12-ideal-dil aq soln|<tuple|1.4.1|?>>
    <associate|fig:12-osmotic pressure|<tuple|1.2.2|?>>
    <associate|fig:12-solid compound|<tuple|1.5.3|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-1.1.1|<tuple|1.1.1|?>>
    <associate|footnote-1.1.2|<tuple|1.1.2|?>>
    <associate|footnote-1.2.1|<tuple|1.2.1|?>>
    <associate|footnote-1.2.2|<tuple|1.2.2|?>>
    <associate|footnote-1.4.1|<tuple|1.4.1|?>>
    <associate|footnote-1.4.2|<tuple|1.4.2|?>>
    <associate|footnote-1.4.3|<tuple|1.4.3|?>>
    <associate|footnote-1.5.1|<tuple|1.5.1|?>>
    <associate|footnote-1.5.2|<tuple|1.5.2|?>>
    <associate|footnote-1.5.3|<tuple|1.5.3|?>>
    <associate|footnote-1.5.4|<tuple|1.5.4|?>>
    <associate|footnote-1.6.1|<tuple|1.6.1|?>>
    <associate|footnote-1.8.1|<tuple|1.8.1|?>>
    <associate|footnote-1.8.2|<tuple|1.8.2|?>>
    <associate|footnote-1.8.3|<tuple|1.8.3|?>>
    <associate|footnote-1.8.4|<tuple|1.8.4|?>>
    <associate|footnote-1.8.5|<tuple|1.8.5|?>>
    <associate|footnote-1.9.1|<tuple|1.9.1|?>>
    <associate|footnr-1.1.1|<tuple|1.1.1|?>>
    <associate|footnr-1.1.2|<tuple|1.1.2|?>>
    <associate|footnr-1.2.1|<tuple|Solvent|?>>
    <associate|footnr-1.2.2|<tuple|1.2.2|?>>
    <associate|footnr-1.4.1|<tuple|1.4.1|?>>
    <associate|footnr-1.4.2|<tuple|1.4.2|?>>
    <associate|footnr-1.4.3|<tuple|1.4.3|?>>
    <associate|footnr-1.5.1|<tuple|1.5.1|?>>
    <associate|footnr-1.5.2|<tuple|1.5.2|?>>
    <associate|footnr-1.5.3|<tuple|1.5.3|?>>
    <associate|footnr-1.5.4|<tuple|1.5.4|?>>
    <associate|footnr-1.6.1|<tuple|1.6.1|?>>
    <associate|footnr-1.8.1|<tuple|1.8.1|?>>
    <associate|footnr-1.8.2|<tuple|1.8.2|?>>
    <associate|footnr-1.8.3|<tuple|1.8.3|?>>
    <associate|footnr-1.8.4|<tuple|1.8.4|?>>
    <associate|footnr-1.8.5|<tuple|1.8.5|?>>
    <associate|footnr-1.9.1|<tuple|1.9.1|?>>
    <associate|id g solubility|<tuple|1.8.27|?>>
    <associate|id s solubility|<tuple|1.5.11|?>>
    <associate|int(dfA/fA)=|<tuple|1.8.10|?>>
    <associate|kB=fB/gammaB|<tuple|1.8.29|?>>
    <associate|k_H,B(p')=|<tuple|1.8.33|?>>
    <associate|lim dPi/dxA=|<tuple|1.4.23|?>>
    <associate|lim(Tf/mB)=|<tuple|1.4.9|?>>
    <associate|ln(xA) vs T, l-sol eqm|<tuple|1.5.4|?>>
    <associate|lnK=.(1/T)+.|<tuple|1.1.15|?>>
    <associate|muA(beta)-muA(alpha)=0|<tuple|1.7.8|?>>
    <associate|muA(p)=|<tuple|1.7.2|?>>
    <associate|muA(p+Pi)=muA*(p)|<tuple|1.2.7|?>>
    <associate|muA(sln)=muA*(l)+RTln(xA)|<tuple|1.4.2|?>>
    <associate|muA*-muA=VA Pi|<tuple|1.2.11|?>>
    <associate|muA*-muA=int(VA)dp|<tuple|1.2.10|?>>
    <associate|muA*/T'-muA/T'=|<tuple|1.2.3|?>>
    <associate|not completely impermeable|<tuple|osmotic pressure|?>>
    <associate|osmotic pressure defn|<tuple|osmotic pressure|?>>
    <associate|p(beta)-p(alpha)(Donnan)=|<tuple|1.7.11|?>>
    <associate|p(beta)-p(alpha)=|<tuple|1.7.3|?>>
    <associate|phi(a)-phi(b)(cation)|<tuple|1.7.15|?>>
    <associate|phi_m (general)|<tuple|1.2.1|?>>
    <associate|slope diff|<tuple|1.2.2|?>>
    <associate|solid cmpd, id l mixt|<tuple|1.5.23|?>>
    <associate|solid solubility|<tuple|1.5.6|?>>
    <associate|tbl:12-CC vs VH|<tuple|1.1.1|?>>
    <associate|tbl:12-activities|<tuple|1.9.1|?>>
    <associate|x(a)/x(b)=ac(b)/ac(a)|<tuple|1.6.5|?>>
    <associate|xA=nA/(nA+nu*nB)|<tuple|1.4.3|?>>
    <associate|xB=()fB/gB|<tuple|1.8.23|?>>
    <associate|xB=p/p^o|<tuple|1.8.24|?>>
    <associate|xBdfB/(-fB+DxB)=dxA|<tuple|1.8.14|?>>
    <associate|zero slope|<tuple|1.5.20|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      negishi-41

      elliot-65

      owens-86

      dobson-25

      scatchard-38

      mcglashan-63

      hildebrand-50
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.1>||>|<pageref|auto-17>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.2>||>|<pageref|auto-19>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.4.1>||>|<pageref|auto-31>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.5.1>||>|<pageref|auto-48>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.5.2>||>|<pageref|auto-49>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.5.3>||>|<pageref|auto-57>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.5.4>||>|<pageref|auto-62>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.6.1>||>|<pageref|auto-73>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.7.1>||>|<pageref|auto-100>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.8.1>||>|<pageref|auto-110>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.8.2>||>|<pageref|auto-111>>
    </associate>
    <\associate|gly>
      <tuple|normal|van't Hoff equation|<pageref|auto-9>>

      <tuple|normal|osmotic pressure|<pageref|auto-23>>

      <tuple|normal|colligative properties|<pageref|auto-27>>

      <tuple|normal|molal freezing-point depression
      constant|<pageref|auto-34>>

      <tuple|normal|molal boiling-point elevation constant|<pageref|auto-38>>

      <tuple|normal|van't Hoff's equation|<pageref|auto-42>>

      <tuple|normal|saturated|<pageref|auto-53>>

      <tuple|normal|solubility|<pageref|auto-54>>

      <tuple|normal|ideal solubility|<pageref|auto-56>>

      <tuple|normal|solid compound|<pageref|auto-60>>

      <tuple|normal|solubility product|<pageref|auto-65>>

      <tuple|normal|Nernst distribution law|<pageref|auto-77>>

      <tuple|normal|partition coefficient|<pageref|auto-79>>

      <tuple|normal|distribution coefficient|<pageref|auto-81>>

      <tuple|normal|osmotic membrane equilibrium|<pageref|auto-86>>

      <tuple|normal|Donnan membrane equilibrium|<pageref|auto-94>>

      <tuple|normal|Donnan potential|<pageref|auto-97>>

      <tuple|normal|Poynting factor|<pageref|auto-105>>

      <tuple|normal|Duhem--Margules equation|<pageref|auto-113>>

      <tuple|normal|ideal solubility|<pageref|auto-117>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|Gibbs--Helmholtz equation>|<pageref|auto-4>>

      <tuple|<tuple|Equilibrium constant|thermodynamic|temperature
      dependence>|<pageref|auto-7>>

      <tuple|<tuple|Enthalpy|reaction|standard molar>|<pageref|auto-8>>

      <tuple|<tuple|Enthalpy|reaction|standard molar>|<pageref|auto-10>>

      <tuple|<tuple|Vaporization>|<pageref|auto-11>>

      <tuple|<tuple|Clausius--Clapeyron equation>|<pageref|auto-12>>

      <tuple|<tuple|Solvent|activity coefficient of>|<pageref|auto-15>>

      <tuple|<tuple|Membrane, semipermeable>|<pageref|auto-20>>

      <tuple|<tuple|Osmosis>|<pageref|auto-21>>

      <tuple|<tuple|Osmotic pressure>|<pageref|auto-22>>

      <tuple|<tuple|Colligative property>|<pageref|auto-26>>

      <tuple|<tuple|Boiling point|elevation in a solution>|<pageref|auto-28>>

      <tuple|<tuple|Vapor pressure|lowering in a solution>|<pageref|auto-29>>

      <tuple|<tuple|Osmotic pressure>|<pageref|auto-30>>

      <tuple|<tuple|Molal freezing-point depression
      constant>|<pageref|auto-33>>

      <tuple|<tuple|Cryoscopic constant>|<pageref|auto-35>>

      <tuple|<tuple|Molal boiling-point elevation
      constant>|<pageref|auto-37>>

      <tuple|<tuple|Ebullioscopic constant>|<pageref|auto-39>>

      <tuple|<tuple|Osmotic coefficient>|<pageref|auto-43>>

      <tuple|<tuple|Freezing point|curve>|<pageref|auto-45>>

      <tuple|<tuple|Solubility|curve>|<pageref|auto-46>>

      <tuple|<tuple|Saturated solution>|<pageref|auto-51>>

      <tuple|<tuple|Solution|saturated>|<pageref|auto-52>>

      <tuple|<tuple|Solid compound>|<pageref|auto-59>>

      <tuple|<tuple|Gibbs--Duhem equation>|<pageref|auto-61>>

      <tuple|<tuple|Solubility product>|<pageref|auto-64>>

      <tuple|<tuple|Common ion effect>|<pageref|auto-66>>

      <tuple|<tuple|Solubility product|temperature
      dependence>|<pageref|auto-67>>

      <tuple|<tuple|Phase|separation of a liquid mixture>|<pageref|auto-70>>

      <tuple|<tuple|Nernst|distribution law>|<pageref|auto-76>>

      <tuple|<tuple|Partition coefficient>|<pageref|auto-78>>

      <tuple|<tuple|Distribution coefficient>|<pageref|auto-80>>

      <tuple|<tuple|Osmotic membrane equilibrium>|<pageref|auto-84>>

      <tuple|<tuple|Membrane equilibrium|osmotic>|<pageref|auto-85>>

      <tuple|<tuple|Osmotic pressure>|<pageref|auto-87>>

      <tuple|<tuple|Equilibrium|dialysis>|<pageref|auto-89>>

      <tuple|<tuple|Dialysis, equilibrium>|<pageref|auto-90>>

      <tuple|<tuple|Donnan|membrane equilibrium>|<pageref|auto-92>>

      <tuple|<tuple|Membrane equilibrium|Donnan>|<pageref|auto-93>>

      <tuple|<tuple|Electric|potential difference>|<pageref|auto-95>>

      <tuple|<tuple|Donnan|potential>|<pageref|auto-96>>

      <tuple|<tuple|Fugacity|effect of liquid pressure
      on>|<pageref|auto-103>>

      <tuple|<tuple|Poynting factor>|<pageref|auto-104>>

      <tuple|<tuple|Gibbs--Duhem equation>|<pageref|auto-107>>

      <tuple|<tuple|Solvent|behavior in an ideal-dilute
      solution>|<pageref|auto-108>>

      <tuple|<tuple|Ideal-dilute solution|solvent behavior
      in>|<pageref|auto-109>>

      <tuple|<tuple|Azeotrope>|<pageref|auto-114>>

      <tuple|<tuple|Salting-out effect on gas solubility>|<pageref|auto-116>>

      <tuple|<tuple|Activity>|<pageref|auto-120>>

      <tuple|<tuple|Equilibrium constant|mixed>|<pageref|auto-122>>

      <tuple|<tuple|Ionic strength|effect on reaction
      equilibrium>|<pageref|auto-123>>

      <tuple|<tuple|Acid dissociation constant>|<pageref|auto-124>>

      <tuple|<tuple|Degree of dissociation>|<pageref|auto-125>>

      <tuple|<tuple|Enthalpy|reaction|standard molar>|<pageref|auto-127>>

      <tuple|<tuple|Gibbs energy|reaction, standard
      molar>|<pageref|auto-128>>

      <tuple|<tuple|Entropy|reaction|standard molar>|<pageref|auto-129>>

      <tuple|<tuple|Entropy|standard molar>|<pageref|auto-130>>

      <tuple|<tuple|Calorimetry|reaction>|<pageref|auto-131>>
    </associate>
    <\associate|table>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1.1.1>||Comparison of
      the Clausius--Clapeyron and van't Hoff equations for vaporization of a
      liquid.>|<pageref|auto-13>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.9.1>||Expressions for
      activities (from Table <reference|tbl:9-activities> and Eqs.
      <reference|a(+)=...> and <reference|a(B)(multisolute)>)>|<pageref|auto-121>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Equilibrium
      Conditions in Multicomponent Systems>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Effects of Temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      <with|par-left|<quote|1tab>|1.1.1<space|2spc>Variation of
      <with|mode|<quote|math>|<error|compound mathbold>> with temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.1.2<space|2spc>Variation of
      <with|mode|<quote|math>|<error|compound mathbold>> with temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|1.1.3<space|2spc>Variation of ln
      <space|-0.17em><space|-0.17em><space|-0.17em>
      <with|mode|<quote|math>|<error|compound mathbold>>
      <space|-0.17em><space|-0.17em><space|-0.17em> with temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      1.2<space|2spc>Solvent Chemical Potentials from Phase Equilibria
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>

      <with|par-left|<quote|1tab>|1.2.1<space|2spc>Freezing-point
      measurements <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>

      <with|par-left|<quote|1tab>|1.2.2<space|2spc>Osmotic-pressure
      measurements <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-18>>

      1.3<space|2spc>Binary Mixture in Equilibrium with a Pure Phase
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-24>

      1.4<space|2spc>Colligative Properties of a Dilute Solution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-25>

      <with|par-left|<quote|1tab>|1.4.1<space|2spc>Freezing-point depression
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-32>>

      <with|par-left|<quote|1tab>|1.4.2<space|2spc>Boiling-point elevation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-36>>

      <with|par-left|<quote|1tab>|1.4.3<space|2spc>Vapor-pressure lowering
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-40>>

      <with|par-left|<quote|1tab>|1.4.4<space|2spc>Osmotic pressure
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-41>>

      1.5<space|2spc>Solid\ULiquid Equilibria
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-44>

      <with|par-left|<quote|1tab>|1.5.1<space|2spc>Freezing points of ideal
      binary liquid mixtures <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-47>>

      <with|par-left|<quote|1tab>|1.5.2<space|2spc>Solubility of a solid
      nonelectrolyte <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-50>>

      <with|par-left|<quote|1tab>|1.5.3<space|2spc>Ideal solubility of a
      solid <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-55>>

      <with|par-left|<quote|1tab>|1.5.4<space|2spc>Solid compound of mixture
      components <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-58>>

      <with|par-left|<quote|1tab>|1.5.5<space|2spc>Solubility of a solid
      electrolyte <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-63>>

      1.6<space|2spc>Liquid\ULiquid Equilibria
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-68>

      <with|par-left|<quote|1tab>|1.6.1<space|2spc>Miscibility in binary
      liquid systems <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-69>>

      <with|par-left|<quote|1tab>|1.6.2<space|2spc>Solubility of one liquid
      in another <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-71>>

      <with|par-left|<quote|2tab>|1.6.2.1<space|2spc>Solute standard state
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-72>>

      <with|par-left|<quote|2tab>|1.6.2.2<space|2spc>Pure-liquid reference
      state <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-74>>

      <with|par-left|<quote|1tab>|1.6.3<space|2spc>Solute distribution
      between two partially-miscible solvents
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-75>>

      1.7<space|2spc>Membrane Equilibria <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-82>

      <with|par-left|<quote|1tab>|1.7.1<space|2spc>Osmotic membrane
      equilibrium <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-83>>

      <with|par-left|<quote|1tab>|1.7.2<space|2spc>Equilibrium dialysis
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-88>>

      <with|par-left|<quote|1tab>|1.7.3<space|2spc>Donnan membrane
      equilibrium <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-91>>

      <with|par-left|<quote|2tab>|1.7.3.1<space|2spc>General expressions
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-98>>

      <with|par-left|<quote|2tab>|1.7.3.2<space|2spc>Example
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-99>>

      1.8<space|2spc>Liquid\UGas Equilibria
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-101>

      <with|par-left|<quote|1tab>|1.8.1<space|2spc>Effect of liquid pressure
      on gas fugacity <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-102>>

      <with|par-left|<quote|1tab>|1.8.2<space|2spc>Effect of liquid
      composition on gas fugacities <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-106>>

      <with|par-left|<quote|1tab>|1.8.3<space|2spc>The Duhem\UMargules
      equation <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-112>>

      <with|par-left|<quote|1tab>|1.8.4<space|2spc>Gas solubility
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-115>>

      <with|par-left|<quote|1tab>|1.8.5<space|2spc>Effect of temperature and
      pressure on Henry's law constants <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-118>>

      1.9<space|2spc>Reaction Equilibria <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-119>

      1.10<space|2spc>Evaluation of Standard Molar Quantities
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-126>
    </associate>
  </collection>
</auxiliary>