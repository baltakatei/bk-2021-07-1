<TeXmacs|2.1.1>

<style|<tuple|book|style-bk>>

<\body>
  <chapter|Systems and Their Properties><label|Chap. 2>

  This chapter begins by explaining some basic terminology of thermodynamics.
  It discusses macroscopic properties of matter in general and properties
  distinguishing different physical states of matter in particular. Virial
  equations of state of a pure gas are introduced. The chapter goes on to
  discuss some basic macroscopic properties and their measurement. Finally,
  several important concepts needed in later chapters are described:
  thermodynamic states and state functions, independent and dependent
  variables, processes, and internal energy.

  <section|The System, Surroundings, and Boundary><label|2-system>

  Chemists are interested in systems containing matter\Vthat which has mass
  and occupies physical space. Classical thermodynamics looks at
  <em|macroscopic> aspects of matter. It deals with the properties of
  aggregates of vast numbers of microscopic particles (molecules, atoms, and
  ions). The macroscopic viewpoint, in fact, treats matter as a
  <em|continuous> material medium rather than as the collection of discrete
  microscopic particles we know are actually present. Although this book is
  an exposition of classical thermodynamics, at times it will point out
  connections between macroscopic properties and molecular structure and
  behavior.

  A thermodynamic <index|System><newterm|system> is any three-dimensional
  region of physical space on which we wish to focus our attention. Usually
  we consider only one system at a time and call it simply \Pthe system.\Q
  The rest of the physical universe constitutes the
  <index|Surroundings><newterm|surroundings> of the system.

  The <index|Boundary><newterm|boundary> is the closed three-dimensional
  surface that encloses the system and separates it from the surroundings.
  The boundary may (and usually does) coincide with real physical surfaces:
  the interface between two phases, the inner or outer surface of the wall of
  a flask or other vessel, and so on. Alternatively, part or all of the
  boundary may be an imagined intangible surface in space, unrelated to any
  physical structure. The size and shape of the system, as defined by its
  boundary, may change in time. In short, our choice of the three-dimensional
  region that constitutes the system is arbitrary\Vbut it is essential that
  we know exactly what this choice is.

  We usually think of the system as a part of the physical universe that we
  are able to influence only indirectly through its interaction with the
  surroundings, and the surroundings as the part of the universe that we are
  able to directly manipulate with various physical devices under our
  control. That is, we (the experimenters) are part of the surroundings, not
  the system.

  For some purposes we may wish to treat the system as being divided into
  <index|Subsystem><em|subsystems>, or to treat the combination of two or
  more systems as a <index|Supersystem><em|supersystem>.

  If over the course of time matter is transferred in either direction across
  the boundary, the system is <subindex|System|open><newterm|open>; otherwise
  it is <subindex|System|closed><newterm|closed>. If the system is open,
  matter may pass through a stationary boundary, or the boundary may move
  through matter that is fixed in space.

  If the boundary allows heat transfer between the system and surroundings,
  the boundary is <index|Diathermal boundary><subindex|Boundary|diathermal><newterm|diathermal>.
  An <subindex|Adiabatic|boundary><subindex|Boundary|adiabatic><newterm|adiabatic><footnote|Greek:
  <em|impassable>.> boundary, on the other hand, is a boundary that does not
  allow heat transfer. We can, in principle, ensure that the boundary is
  adiabatic by surrounding the system with an adiabatic wall\Vone with
  perfect thermal insulation and a perfect radiation shield.

  An <index|Isolated system><subindex|System|isolated><newterm|isolated>
  system is one that exchanges no matter, heat, or work with the
  surroundings, so that the system's mass and total energy remain constant
  over time.<footnote|The energy in this definition of an isolated system is
  measured in a local <index|Reference frame><subindex|Frame|reference>reference
  frame, as will be explained in Sec. <reference|2-internal energy>.> A
  closed system with an adiabatic boundary, constrained to do no work and to
  have no work done on it, is an isolated system. <minor| The constraints
  required to prevent work usually involve forces between the system and
  surroundings. In that sense a system may interact with the surroundings
  even though it is isolated. For instance, a gas contained within rigid,
  thermally-insulated walls is an isolated system; the gas exerts a force on
  each wall, and the wall exerts an equal and opposite force on the gas. An
  isolated system may also experience a constant <index|External
  field><subindex|Field|external>external field, such as a
  <subindex|Gravitational|field><subindex|Field|gravitational>gravitational
  field.>

  The term <index|Body><newterm|body> usually implies a system, or part of a
  system, whose mass and chemical composition are constant over time.

  <subsection|Extensive and intensive properties><label|2-extensive
  \ intensive>

  A quantitative <em|property> of a system describes some macroscopic feature
  that, although it may vary with time, has a particular value at any given
  instant of time.

  Table <reference|tbl:2-symbols><vpageref|tbl:2-symbols> lists the symbols
  of some of the properties discussed in this chapter and the SI units in
  which they may be expressed. A much more complete table is found in
  Appendix <reference|app:sym>.

  <\big-table>
    <minipagetable|7cm|<label|tbl:2-symbols><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|9|9|1|-1|cell-bborder|1ln>|<table|<row|<cell|Symbol>|<cell|Physical
    quantity>|<cell|SI unit>>|<row|<cell|<math|E>>|<cell|energy>|<cell|J>>|<row|<cell|<math|m>>|<cell|mass>|<cell|kg>>|<row|<cell|<math|n>>|<cell|amount
    of substance>|<cell|mol>>|<row|<cell|<math|p>>|<cell|pressure>|<cell|Pa>>|<row|<cell|<math|T>>|<cell|thermodynamic
    temperature>|<cell|K>>|<row|<cell|<math|V>>|<cell|volume>|<cell|m<rsup|<math|3>>>>|<row|<cell|<math|U>>|<cell|internal
    energy>|<cell|J>>|<row|<cell|<math|\<rho\>>>|<cell|density>|<cell|kg<space|0.17em>m<rsup|<math|-3>>>>>>>>
  </big-table|Symbols and SI units for some common properties>

  Most of the properties studied by thermodynamics may be classified as
  either <em|extensive> or <em|intensive>. We can distinguish these two types
  of properties by the following considerations.

  If we imagine the system to be divided by an imaginary surface into two
  parts, any property of the system that is the sum of the property for the
  two parts is an <index|Extensive property><subindex|Property|extensive><newterm|extensive
  property>. That is, an additive property is extensive. Examples are mass,
  volume, amount, energy, and the surface area of a solid.

  <\minor>
    \ Sometimes a more restricted definition of an extensive property is
    used: The property must be not only additive, but also proportional to
    the mass or the amount when intensive properties remain constant.
    According to this definition, mass, volume, amount, and energy are
    extensive, but surface area is not.
  </minor>

  If we imagine a homogeneous region of space to be divided into two or more
  parts of arbitrary size, any property that has the same value in each part
  and the whole is an <index|Intensive property><subindex|Property|intensive><newterm|intensive
  property>; for example density, concentration, pressure (in a fluid), and
  temperature. The value of an intensive property is the same everywhere in a
  homogeneous region, but may vary from point to point in a heterogeneous
  region\Vit is a <em|local> property.

  Since classical thermodynamics treats matter as a continuous medium,
  whereas matter actually contains discrete microscopic particles, the value
  of an intensive property at a point is a statistical average of the
  behavior of many particles. For instance, the <index|Density>density of a
  gas at one point in space is the average mass of a small volume element at
  that point, large enough to contain many molecules, divided by the volume
  of that element.

  Some properties are defined as the ratio of two extensive quantities. If
  both extensive quantities refer to a homogeneous region of the system or to
  a small volume element, the ratio is an <em|intensive> property. For
  example <index|Concentration>concentration, defined as the ratio
  <math|<tx|a*m*o*u*n*t>/<tx|v*o*l*u*m*e>>, is intensive. A mathematical
  derivative of one such extensive quantity with respect to another is also
  intensive.

  A special case is an extensive quantity divided by the mass, giving an
  intensive <subindex|Specific|quantity><subindex|Quantity|specific><newterm|specific
  quantity>; for example <subindex|Specific|volume><subindex|Volume|specific>

  <\equation>
    <tx|S*p*e*c*i*f*i*c*v*o*l*u*m*e>=<frac|V|m>=<frac|1|\<rho\>>
  </equation>

  If the symbol for the extensive quantity is a capital letter, it is
  customary to use the corresponding lower-case letter as the symbol for the
  specific quantity. Thus the symbol for specific volume is <math|v>.

  Another special case encountered frequently in this book is an extensive
  property for a pure, homogeneous substance divided by the amount <math|n>.
  The resulting intensive property is called, in general, a
  <subindex|Molar|quantity><subindex|Quantity|molar><newterm|molar quantity>
  or molar property. To symbolize a molar quantity, this book follows the
  recommendation of the IUPAC: The symbol of the extensive quantity is
  followed by subscript<label|subscript m rule>m, and optionally the identity
  of the substance is indicated either by a subscript or a formula in
  parentheses. Examples are <subindex|Volume|molar>

  <\equation>
    <label|V/n=Vm><tx|M*o*l*a*r*v*o*l*u*m*e>=<frac|V|n>=V<m>
  </equation>

  <\equation>
    <tx|M*o*l*a*r*v*o*l*u*m*e*o*f*s*u*b*s*t*a*n*c*e>i=<frac|V|n<rsub|i>>=V<mi>
  </equation>

  <\equation>
    <tx|M*o*l*a*r*v*o*l*u*m*e*o*f*H<rsub|<math|2>>*O>=V<m><tx|<around|(|H<rsub|<math|2>>*O|)>>
  </equation>

  In the past, especially in the United States, molar quantities were
  commonly denoted with an overbar (e.g., <math|<wide|V|\<bar\>><rsub|i>>).

  <section|Phases and Physical States of Matter>

  A <index|Phase><newterm|phase> is a region of the system in which each
  intensive property (such as temperature and pressure) has at each instant
  either the same value throughout (a <em|uniform> or <index|Homogeneous
  phase><subindex|Phase|homogeneous><em|homogeneous> phase), or else a value
  that varies continuously from one point to another. Whenever this book
  mentions a phase, it is a <em|uniform> phase unless otherwise stated. Two
  different phases meet at an <index|Interface surface><newterm|interface
  surface>, where intensive properties have a discontinuity or change value
  over a small distance.

  Some intensive properties (e.g., refractive index and polarizability) can
  have directional characteristics. A uniform phase may be either
  <index|Isotropic phase><subindex|Phase|isotropic><em|isotropic>, exhibiting
  the same values of these properties in all directions, or
  <index|Anisotropic phase><subindex|Phase|anisotropic><em|anisotropic>, as
  in the case of some solids and liquid crystals. A vacuum is a uniform phase
  of zero density.

  Suppose we have to deal with a <em|nonuniform> region in which intensive
  properties vary continuously in space along one or more directions\Vfor
  example, a tall column of gas in a gravitational field whose density
  decreases with increasing altitude. There are two ways we may treat such a
  nonuniform, continuous region: either as a single nonuniform phase, or else
  as an infinite number of uniform phases, each of infinitesimal size in one
  or more dimensions.

  <subsection|Physical states of matter><label|2-physical states>

  <index|Physical state><subindex|State|physical><I|State!aggregation@of
  aggregation\|reg>We are used to labeling phases by physical state, or state
  of aggregation. It is common to say that a phase is a <em|solid> if it is
  relatively rigid, a <em|liquid> if it is easily deformed and relatively
  incompressible, and a <em|gas> if it is easily deformed and easily
  compressed. Since these descriptions of responses to external forces differ
  only in degree, they are inadequate to classify intermediate cases.

  A more rigorous approach is to make a primary distinction between a
  <em|solid> and a <em|fluid>, based on the phase's response to an applied
  shear stress, and then use additional criteria to classify a fluid as a
  <em|liquid>, <em|gas>, or <em|supercritical fluid>. <index|Shear
  stress><newterm|Shear stress><label|2-shear stress>is a tangential force
  per unit area that is exerted on matter on one side of an interior plane by
  the matter on the other side. We can produce shear stress in a phase by
  applying tangential forces to parallel surfaces of the phase as shown in
  Fig. <reference|fig:2-shear><vpageref|fig:2-shear>.

  <\big-figure>
    <boxedfigure|<image|./02-SUP/shear.eps||||> <capt|Experimental procedure
    for producing shear stress in a phase (shaded). Blocks at the upper and
    lower surfaces of the phase are pushed in opposite directions, dragging
    the adjacent portions of the phase with them.<label|fig:2-shear>>>
  </big-figure|>

  <\plainlist>
    <item>A <index|Solid><newterm|solid> responds to shear stress by
    undergoing momentary relative motion of its parts, resulting in
    <index|Deformation><em|deformation><emdash>a change of shape. If the
    applied shear stress is constant and small (not large enough to cause
    creep or fracture), the solid quickly reaches a certain degree of
    deformation that depends on the magnitude of the stress and maintains
    this deformation without further change as long as the shear stress
    continues to be applied. On the microscopic level, deformation requires
    relative movement of adjacent layers of particles (atoms, molecules, or
    ions). The shape of an unstressed solid is determined by the attractive
    and repulsive forces between the particles; these forces make it
    difficult for adjacent layers to slide past one another, so that the
    solid resists deformation.

    <item>A <index|Fluid><newterm|fluid> responds to shear stress
    differently, by undergoing continuous relative motion (flow) of its
    parts. The flow continues as long as there is any shear stress, no matter
    how small, and stops only when the shear stress is removed.
  </plainlist>

  Thus, a constant applied shear stress causes a fixed deformation in a solid
  and continuous flow in a fluid. We say that a phase under constant shear
  stress is a solid if, after the initial deformation, we are unable to
  detect a further change in shape during the period we observe the phase.

  Usually this criterion allows us to unambiguously classify a phase as
  either a solid or a fluid. Over a sufficiently long time period, however,
  detectable flow is likely to occur in <em|any> material under shear stress
  of <em|any> magnitude. Thus, the distinction between solid and fluid
  actually depends on the time scale of observation. This fact is obvious
  when we observe the behavior of certain materials (such as Silly Putty, or
  a paste of water and cornstarch) that exhibit solid-like behavior over a
  short time period and fluid-like behavior over a longer period. Such
  materials, that resist deformation by a suddenly-applied shear stress but
  undergo flow over a longer time period, are called <index|Viscoelastic
  solid><subindex|Solid|viscoelastic><em|viscoelastic solids>.

  <subsection|Phase coexistence and phase transitions><label|2-phase
  coexistence>

  This section considers some general characteristics of systems containing
  more than one phase.

  Suppose we bring two uniform phases containing the same constituents into
  physical contact at an interface surface. If we find that the phases have
  no tendency to change over time while both have the same temperature and
  the same pressure, but differ in other intensive properties such as density
  and composition, we say that they <subindex|Phase|coexistence><newterm|coexist>
  in equilibrium with one another. The conditions for such phase coexistence
  are the subject of later sections in this book, but they tend to be quite
  restricted. For instance, the liquid and gas phases of pure
  H<rsub|<math|2>>O at a pressure of <math|1<br>> can coexist at only one
  temperature, <math|99.61<units|<degC>>>.

  A <subindex|Phase|transition><newterm|phase transition> of a pure substance
  is a change over time in which there is a continuous transfer of the
  substance from one phase to another. Eventually one phase can completely
  disappear, and the substance has been completely transferred to the other
  phase. If both phases coexist in equilibrium with one another, and the
  temperature and pressure of both phases remain equal and constant during
  the phase transition, the change is an <subsubindex|Phase|transition|equilibrium><em|equilibrium
  phase transition>. For example, H<rsub|<math|2>>O at
  <math|99.61<units|<degC>>> and <math|1<br>> can undergo an equilibrium
  phase transition from liquid to gas (vaporization) or from gas to liquid
  (condensation). During an equilibrium phase transition, there is a transfer
  of energy between the system and its surroundings by means of heat or work.

  <subsection|Fluids><label|2-fluids>

  <I|Fluid\|(>It is usual to classify a fluid as either a <em|liquid> or a
  <em|gas>. The distinction is important for a pure substance because the
  choice determines the treatment of the phase's standard state (see Sec.
  <reference|7-st states of pure substances>). To complicate matters, a fluid
  at high pressure may be a <index|Supercritical
  fluid><subindex|Fluid|supercritical><em|supercritical fluid>. Sometimes a
  <index|Plasma><em|plasma> (a highly ionized, electrically conducting
  medium) is considered a separate kind of fluid state; it is the state found
  in the earth's ionosphere and in stars.

  In general, and provided the pressure is not high enough for supercritical
  phenomena to exist\Vusually true of pressures below <math|25<br>> except in
  the case of He or H<rsub|<math|2>>\Vwe can make the distinction between
  liquid and gas simply on the basis of density. A
  <index|Liquid><newterm|liquid> has a relatively high density that is
  insensitive to changes in temperature and pressure. A
  <index|Gas><newterm|gas>, on the other hand, has a relatively low density
  that is sensitive to temperature and pressure and that approaches zero as
  pressure is reduced at constant temperature.

  This simple distinction between liquids and gases fails at high pressures,
  where liquid and gas phases may have similar densities at the same
  temperature. Figure <reference|fig:2-phases><vpageref|fig:2-phases>

  <\big-figure>
    <boxedfigure|<image|./02-SUP/phases.eps||||> <capt|Pressure--temperature
    phase diagram of a pure substance (schematic). Point cp is the critical
    point, and point tp is the triple point. Each area is labeled with the
    physical state that is stable under the pressure-temperature conditions
    that fall within the area. A solid curve (coexistence curve) separating
    two areas is the locus of pressure-temperature conditions that allow the
    phases of these areas to coexist at equilibrium. Path ABCD illustrates
    <em|continuity of states>.<label|fig:2-phases>>>
  </big-figure|>

  shows how we can classify stable fluid states of a pure substance in
  relation to a liquid\Ugas coexistence curve and a critical point. If
  raising the temperature of a fluid at constant pressure causes a phase
  transition to a second fluid phase, the original fluid was a liquid and the
  transition occurs at the liquid\Ugas <subindex|Coexistence
  curve|liquid--gas><em|coexistence curve>. This curve ends at a
  <I|Critical!point!pure substance@of a pure substance\|reg><newterm|critical
  point>, at which all intensive properties of the coexisting liquid and gas
  phases become identical. The fluid state of a pure substance at a
  temperature greater than the critical temperature and a pressure greater
  than the critical pressure is called a <index|Supercritical
  fluid><subindex|Fluid|supercritical><newterm|supercritical fluid>.

  The term <index|Vapor><newterm|vapor> is sometimes used for a gas that can
  be condensed to a liquid by increasing the pressure at constant
  temperature. By this definition, the vapor state of a substance exists only
  at temperatures below the critical temperature.

  The designation of a supercritical fluid state of a substance is used more
  for convenience than because of any unique properties compared to a liquid
  or gas. If we vary the temperature or pressure in such a way that the
  substance changes from what we call a liquid to what we call a
  supercritical fluid, we observe only a continuous density change of a
  single phase, and no phase transition with two coexisting phases. The same
  is true for a change from a supercritical fluid to a gas. Thus, by making
  the changes described by the path ABCD shown in Fig.
  <reference|fig:2-phases>, we can transform a pure substance from a liquid
  at a certain pressure to a gas at the same pressure without ever observing
  an interface between two coexisting phases! This curious phenomenon is
  called <index|Continuity of states><em|continuity of states>.

  Chapter 6 will take up the discussion of further aspects of the physical
  states of pure substances.

  If we are dealing with a fluid <em|mixture> (instead of a pure substance)
  at a high pressure, it may be difficult to classify the phase as either
  liquid or gas. The complexity of classification at high pressure is
  illustrated by the <index|Barotropic effect><em|barotropic effect>,
  observed in some mixtures, in which a small change of temperature or
  pressure causes what was initially the more dense of two coexisting fluid
  phases to become the less dense phase. In a gravitational field, the two
  phases switch positions. <I|Fluid\|)>

  <subsection|The equation of state of a fluid><label|2-eqn of state>

  Suppose we prepare a uniform fluid phase containing a known amount
  <math|n<rsub|i>> of each constituent substance <math|i>, and adjust the
  temperature <math|T> and pressure <math|p> to definite known values. We
  expect this phase to have a definite, fixed volume <math|V>. If we change
  any one of the properties <math|T>, <math|p>, or <math|n<rsub|i>>, there is
  usually a change in <math|V>. The value of <math|V> is dependent on the
  other properties and cannot be varied independently of them. Thus, for a
  given substance or mixture of substances in a uniform fluid phase, <math|V>
  is a unique function of <math|T>, <math|p>, and <math|<allni>>, where
  <math|<allni>> stands for the set of amounts of all substances in the
  phase. We may be able to express this relation in an explicit equation:
  <math|V=f*<around|(|T,p,<allni>|)>>. This equation (or a rearranged form)
  that gives a relation among <math|V>, <math|T>, <math|p>, and
  <math|<allni>>, is the <I|Equation of state!fluid@of a
  fluid\|reg><newterm|equation of state> of the fluid.

  We may solve the equation of state, implicitly or explicitly, for any one
  of the quantities <math|V>, <math|T>, <math|p>, and <math|n<rsub|i>> in
  terms of the other quantities. Thus, of the <math|3+s> quantities (where
  <math|s> is the number of substances), only <math|2+s> are independent.

  The <subindex|Ideal gas|equation><I|Equation of state!ideal gas@of an ideal
  gas\|reg><em|ideal gas equation>, <math|p=n*R*T/V> (Eq.
  <reference|p=nRT/V><vpageref|p=nRT/V>), is an equation of state. It is
  found experimentally that the behavior of any gas in the limit of low
  pressure, as temperature is held constant, approaches this equation of
  state. This limiting behavior is also predicted by kinetic-molecular
  theory.

  If the fluid has only one constituent (i.e., is a pure substance rather
  than a mixture), then at a fixed <math|T> and <math|p> the volume is
  proportional to the amount. In this case, the equation of state may be
  expressed as a relation among <math|T>, <math|p>, and the molar volume
  <math|V<m>=V/n>. The equation of state for a pure ideal gas may be written
  <math|p=R*T/V<m>>.

  The <index|Redlich--Kwong equation><em|Redlich\UKwong equation> is a
  two-parameter equation of state frequently used to describe, to good
  accuracy, the behavior of a pure gas at a pressure where the ideal gas
  equation fails:

  <\equation>
    p=<frac|R*T|V<m>-b>-<frac|a|V<m><around|(|V<m>+b|)>*T<rsup|1/2>>
  </equation>

  In this equation, <math|a> and <math|b> are constants that are independent
  of temperature and depend on the substance.

  The next section describes features of <em|virial> equations, an important
  class of equations of state for real (nonideal) gases.

  <subsection|Virial equations of state for pure gases><label|2-virial eqn>

  <subindex|Equation of state|virial><I|Virial!equation!pure gas@for a pure
  gas\|reg>In later chapters of this book there will be occasion to apply
  thermodynamic derivations to virial equations of state of a pure gas or gas
  mixture. These formulas accurately describe the gas at low and moderate
  pressures using empirically determined, temperature-dependent parameters.
  The equations may be derived from <subindex|Statistical mechanics|virial
  equations>statistical mechanics, so they have a theoretical as well as
  empirical foundation.

  There are two forms of virial equations for a pure gas: one a series in
  powers of <math|1/V<m>>:

  <\equation>
    <label|pVm=RT(1+B/Vm...)>p*V<m>=R*T*<around*|(|1+<frac|B|V<m>>+<frac|C|V<m><rsup|2>>+\<cdots\>|)>
  </equation>

  and the other a series in powers of <math|p>:

  <\equation>
    <label|pVm=RT(1+B_p p...)>p*V<m>=R*T*<around*|(|1+B<rsub|p>*p+C<rsub|p>*p<rsup|2>+\<cdots\>|)>
  </equation>

  The parameters <math|B>, <math|C>, <math|\<ldots\>> are called the
  <em|second>, <em|third>, <math|\<ldots\>>
  <subindex|Virial|coefficient><em|virial coefficients>, and the parameters
  <math|B<rsub|p>>, <math|C<rsub|p>>, <math|\<ldots\>> are a set of pressure
  virial coefficients. Their values depend on the substance and are functions
  of temperature. (The <em|first> virial coefficient in both power series is
  <math|1>, because <math|p*V<m>> must approach <math|R*T> as <math|1/V<m>>
  or <math|p> approach zero at constant <math|T>.) Coefficients beyond the
  third virial coefficient are small and rarely evaluated.

  The values of the virial coefficients for a gas at a given temperature can
  be determined from the dependence of <math|p> on <math|V<m>> at this
  temperature. The value of the second virial coefficient <math|B> depends on
  pairwise interactions between the atoms or molecules of the gas, and in
  some cases can be calculated to good accuracy from <index|Statistical
  mechanics>statistical mechanics theory and a realistic intermolecular
  potential function.

  To find the relation between the virial coefficients of Eq.
  <reference|pVm=RT(1+B/Vm...)> and the parameters <math|B<rsub|p>>,
  <math|C<rsub|p>>, <math|\<ldots\>> in Eq. <reference|pVm=RT(1+B_p p...)>,
  we solve Eq. <reference|pVm=RT(1+B/Vm...)> for <math|p> in terms of
  <math|V<m>>

  <\equation>
    p=R*T*<around*|(|<frac|1|V<m>>+<frac|B|V<m><rsup|2>>+\<cdots\>|)>
  </equation>

  and substitute in the right side of Eq. <reference|pVm=RT(1+B_p p...)>:

  <\equation>
    <label|pVm=RT(1+B_p RT(1/Vm...)...)>

    <\eqsplit>
      <tformat|<table|<row|<cell|p*V<m>>|<cell|=R*T*<around*|[|<vphantom|<around*|(|<frac|B|V<m><rsup|2>>|)><rsup|2>>1+B<rsub|p>*R*T*<around*|(|<frac|1|V<m>>+<frac|B|V<m><rsup|2>>+\<cdots\>|)>|\<nobracket\>>>>|<row|<cell|>|<cell|<space|2em><space|2em><space|.05cm>+C<rsub|p>*<around|(|R*T|)><rsup|2>*<around*|\<nobracket\>|<around*|(|<frac|1|V<m>>+<frac|B|V<m><rsup|2>>+\<cdots\>|)><rsup|2>+\<cdots\>|]>>>>>
    </eqsplit>
  </equation>

  Then we equate coefficients of equal powers of <math|1/V<m>> in Eqs.

  <reference|pVm=RT(1+B/Vm...)> and <reference|pVm=RT(1+B_p RT(1/Vm...)...)>
  (since both equations must yield the same value of <math|p*V<m>> for any
  value of <math|1/V<m>>):

  <\equation>
    <label|B=RTB_p>B=R*T*B<rsub|p>
  </equation>

  <\equation>
    C=B<rsub|p>*R*T*B+C<rsub|p>*<around|(|R*T|)><rsup|2>=<around|(|R*T|)><rsup|2>*<around|(|B<rsub|p><rsup|2>+C<rsub|p>|)>
  </equation>

  In the last equation, we have substituted for <math|B> from Eq.
  <reference|B=RTB_p>.

  At pressures up to at least one bar, the terms beyond <math|B<rsub|p>*p> in
  the pressure power series of Eq. <reference|pVm=RT(1+B_p p...)> are
  negligible; then <math|p*V<m>> may be approximated by
  <math|R*T*<around|(|1+B<rsub|p>*p|)>>, giving, with the help of Eq.
  <reference|B=RTB_p>, the <I|Equation of state!gas at low pressure@of a gas
  at low pressure\|reg>simple approximate equation of
  state<footnote|Guggenheim (Ref. <cite|guggen-85>) calls a gas with this
  equation of state a <em|slightly imperfect gas>.>

  <\gather>
    <tformat|<table|<row|<cell|<s|V<m>\<approx\><frac|R*T|p>+B><cond|<around|(|p*u*r*e*g*a*s,<math|p\<le\>1<br>>|)>><eq-number><label|Vm=RT/p+B>>>>>
  </gather>

  The <index|Compression factor><newterm|compression factor> (or
  <index|Compressibility factor>compressibility factor) <math|Z> of a gas is
  defined by

  <\gather>
    <tformat|<table|<row|<cell|<s|Z<defn><frac|p*V|n*R*T>=<frac|p*V<m>|R*T>><cond|<around|(|g*a*s|)>><eq-number><label|Z=pV/nRT>>>>>
  </gather>

  When a gas at a particular temperature and pressure satisfies the ideal gas
  equation, the value of <math|Z> is <math|1>. The virial equations rewritten
  using <math|Z> are

  <\gather>
    <tformat|<table|<row|<cell|Z=1+<frac|B|V<m>>+<frac|C|V<m><rsup|2>>+\<cdots\><eq-number>>>|<row|<cell|Z=1+B<rsub|p>*p+C<rsub|p>*p<rsup|2>+\<cdots\><eq-number>>>>>
  </gather>

  These equations show that the second virial coefficient <math|B> is the
  initial slope of the curve of a plot of <math|Z> versus <math|1/V<m>> at
  constant <math|T>, and <math|B<rsub|p>> is the initial slope of <math|Z>
  versus <math|p> at constant <math|T>.

  The way in which <math|Z> varies with <math|p> at different temperatures is
  shown for the case of carbon dioxide in Fig.
  <reference|fig:2-CO2>(a)<vpageref|fig:2-CO2>.

  <\big-figure>
    <\boxedfigure>
      <image|./02-SUP/CO2.eps||||>

      <\capt>
        (a)<nbsp>Compression factor of CO<rsub|<math|2>> as a function of
        pressure at three temperatures. At <math|710<K>>, the Boyle
        temperature, the initial slope is zero.

        \ (b)<nbsp>Second virial coefficient of CO<rsub|<math|2>> as a
        function of temperature.<inactive|<label|fig:2-CO2>>
      </capt>
    </boxedfigure>
  </big-figure|>

  A temperature at which the initial slope is zero is called the <index|Boyle
  temperature><subindex|Temperature|Boyle><newterm|Boyle temperature>, which
  for CO<rsub|<math|2>> is <math|710<K>>. Both <math|B> and <math|B<rsub|p>>
  must be zero at the Boyle temperature. At lower temperatures <math|B> and
  <math|B<rsub|p>> are negative, and at higher temperatures they are
  positive\Vsee Fig. <reference|fig:2-CO2>(b). This kind of temperature
  dependence is typical for other gases. Experimentally, and also according
  to <subindex|Statistical mechanics|Boyle temperature>statistical mechanical
  theory, <math|B> and <math|B<rsub|p>> for a gas can be zero only at a
  single Boyle temperature.

  <\minor>
    \ The fact that at any temperature other than the Boyle temperature
    <math|B> is nonzero is significant since it means that in the limit as
    <math|p> approaches zero at constant <math|T> and the gas approaches
    ideal-gas behavior, the <em|difference> between the actual molar volume
    <math|V<m>> and the ideal-gas molar volume <math|R*T/p> does not approach
    zero. Instead, <math|V<m>-R*T/p> approaches the nonzero value <math|B>
    (see Eq. <reference|Vm=RT/p+B>). However, the <em|ratio> of the actual
    and ideal molar volumes, <math|V<m>/<around|(|R*T/p|)>>, approaches unity
    in this limit.
  </minor>

  Virial equations of gas <em|mixtures> will be discussed in Sec.
  <reference|9-real gas mixtures>.

  <subsection|Solids><label|2-solids>

  <I|Solid\|(>A solid phase responds to a small applied stress by undergoing
  a small <index|Elastic deformation><subindex|Deformation|elastic><em|elastic
  deformation>. When the stress is removed, the solid returns to its initial
  shape and the properties return to those of the unstressed solid. Under
  these conditions of small stress, the solid has an equation of state just
  as a fluid does, in which <math|p> is the pressure of a fluid surrounding
  the solid (the hydrostatic pressure) as explained in Sec.
  <reference|2-pressure>. The stress is an additional independent variable.
  For example, the length of a metal spring that is elastically deformed is a
  unique function of the temperature, the pressure of the surrounding air,
  and the stretching force.

  If, however, the stress applied to the solid exceeds its elastic limit, the
  response is <index|Plastic deformation><subindex|Deformation|plastic><em|plastic
  deformation>. This deformation persists when the stress is removed, and the
  unstressed solid no longer has its original properties. Plastic deformation
  is a kind of hysteresis, and is caused by such microscopic behavior as the
  slipping of crystal planes past one another in a crystal subjected to shear
  stress, and conformational rearrangements about single bonds in a stretched
  macromolecular fiber. Properties of a solid under plastic deformation
  depend on its past history and are not unique functions of a set of
  independent variables; an equation of state does not exist. <I|Solid\|)>

  <section|Some Basic Properties and Their Measurement>

  <paragraphfootnotes>

  This section discusses aspects of the macroscopic properties mass, amount
  of substance, volume, density, pressure, and temperature, with examples of
  how these properties can be measured.

  <subsection|Mass><label|2-mass>

  <I|Mass!measurement of\|(>

  The SI unit of mass is the <index|Kilogram><with|font-series|bold|kilogram>.
  The practical measurement of the mass of a body is with a balance utilizing
  the downward force exerted on the body by the earth's
  <subindex|Gravitational|field><subindex|Field|gravitational>gravitational
  field. The classic balance has a beam and knife-edge arrangement to compare
  the gravitational force on the body with the
  <subindex|Gravitational|force><subindex|Force|gravitational>gravitational
  force on a weight of known mass. A modern balance (strictly speaking a
  <em|scale>) incorporates a strain gauge or comparable device to directly
  measure the gravitational force on the unknown mass; this type must be
  calibrated with known masses. The most accurate measurements take into
  account the effect of the buoyancy of the body and the calibration masses
  in air.

  The accuracy of the calibration masses should be traceable to a national
  standard kilogram (which in the United States is maintained at NIST, the
  National Institute of Standards and Technology, in Gaithersburg, Maryland)
  and ultimately to the <index|International
  prototype><subindex|Kilogram|international prototype>international
  prototype (page <pageref|international prototype>).

  <\minor>
    \ <subindex|SI|2019 revision>The 2019 revision of the SI replaces the
    international prototype with a new definition of the kilogram (Appendix
    <reference|app:SI>). The present method of choice for applying this
    definition to the precise measurement of a mass, with an uncertainty of
    several parts in <math|10<rsup|8>>, uses an elaborate apparatus called a
    <index|Watt balance>watt balance or <index|Kibble balance><em|Kibble
    balance>.<footnote|Ref. <cite|chao-15>><label|2-Kibble balance>By this
    method, the mass of the international prototype is found to be
    1<units|kg> to within <math|1<timesten|-8><units|k*g>>.<footnote|Ref.
    <cite|stock-19>, Appendix 2.>

    The NIST-4 Kibble balance<footnote|Ref. <cite|haddad-16>> at NIST has a
    balance wheel, from one side of which is suspended a coil of wire placed
    in a magnetic field, and from the other side a tare weight. In use, the
    balance position of the wheel is established. The test weight of unknown
    mass <math|m> is added to the coil side and a current passed through the
    coil, generating an upward force on this side due to the magnetic field.
    The current <math|I> is adjusted to reestablish the balance position. The
    balance condition is that the downward gravitational force on the test
    weight be equal in magnitude to the upward electromagnetic force:
    <math|m*g=B*l*I>, where <math|g> is the acceleration of free fall,
    <math|B> is the magnetic flux density, <math|l> is the wire length of the
    coil, and <math|I> is the current carried by the wire.

    <math|B> and <math|I> can't be measured precisely, so in a second
    calibration step the test weight is removed, the current is turned off,
    and the coil is moved vertically through the magnetic field at a constant
    precisely-measured speed <math|v>. This motion induces an electric
    potential difference between the two ends of the coil wire given by
    <math|<Del>\<phi\>=B*l*v>.

    By eliminating the product <math|B*l> from between the two preceding
    equations, the mass of the test weight can be calculated from
    <math|m=I<Del>\<phi\>/g*v>. For this calculation, <math|I> and
    <math|<Del>\<phi\>> are measured to a very high degree of precision
    during the balance operations by instrumental methods (Josephson and
    quantum Hall effects) requiring the defined value of the Planck constant
    <math|h>; the value of <math|g> at the location of the apparatus is
    measured with a gravimeter.
  </minor>

  \ <I|Mass!measurement of\|)>

  <subsection|Amount of substance><label|2-amount>

  <I|Amount!measurement of\|(>The SI unit of <index|Amount>amount of
  substance (called simply the <em|amount> in this book) is the
  <index|Mole><with|font-series|bold|mole> (Sec. <reference|1-amount>).
  Chemists are familiar with the fact that, although the mole is a counting
  unit, an amount in moles is measured not by counting but by weighing. The
  SI revision of 2019 makes a negligible change to calculations involving the
  mole (page <pageref|1-mole revision>), so the previous definition of the
  mole remains valid for most purposes: twelve grams of carbon-12, the most
  abundant isotope of carbon, contains one mole of atoms.

  The <index|Relative atomic mass><index|Atomic mass, relative><em|relative
  atomic mass> or <index|Atomic weight><em|atomic weight> <math|A<subs|r>> of
  an atom is a dimensionless quantity equal to the atomic mass relative to
  <math|A<subs|r|=>12> for carbon-12. The <index|Relative molecular
  mass><index|Molecular mass, relative><em|relative molecular mass> or
  <index|Molecular weight><label|2-molecular weight> <em|molecular weight>
  <math|M<subs|r>> of a molecular substance, also dimensionless, is the
  molecular mass relative to carbon-12. Thus the amount <math|n> of a
  substance of mass <math|m> can be calculated from

  <\equation>
    n=<frac|m|A<subs|r><units|g*m*o*l<per>>><space|1em><tx|o*r><space|1em>n=<frac|m|M<subs|r><units|g*m*o*l<per>>>
  </equation>

  A related quantity is the <subindex|Molar|mass><newterm|molar mass>
  <math|M> of a substance, defined as the mass divided by the amount:

  <\equation>
    <inactive|<label|M=m/n>>M<defn><frac|m|n>
  </equation>

  (The symbol <math|M> for molar mass is an exception to the rule given on
  page<nbsp><pageref|subscript m rule> that a subscript m is used to indicate
  a molar quantity.) The numerical value of the molar mass expressed in units
  of <math|<space|-0.17em><units|g*<space|0.17em>m*o*l<per>>> is equal to the
  relative atomic or molecular mass:

  <\equation>
    M/<tx|g*<space|0.17em>m*o*l<per>>=A<subs|r><space|1em><tx|o*r><space|1em>M/<tx|g*<space|0.17em>m*o*l<per>>=M<subs|r>
  </equation>

  <I|Amount!measurement of\|)>

  <subsection|Volume>

  <I|Volume!meaurement of\|(>Liquid volumes are commonly measured with
  precision volumetric glassware such as burets, pipets, and volumetric
  flasks. The National Institute of Standards and Technology in the United
  States has established specifications for \PClass A\Q glassware; two
  examples are listed in Table <reference|tbl:2-methods><vpageref|tbl:2-methods>.
  The volume of a vessel at one temperature may be accurately determined from
  the mass of a liquid of known density, such as water, that fills the vessel
  at this temperature.

  <\big-table>
    <minipagetable|11.2cm|<label|tbl:2-methods><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|2|2|1|-1|cell-bborder|1ln>|<cwith|15|15|1|-1|cell-bborder|1ln>|<table|<row|<cell|Physical>|<cell|<Lower|Method>>|<cell|Typical>|<cell|Approximate>>|<row|<cell|quantity>|<cell|>|<cell|value>|<cell|uncertainty>>|<row|<cell|Mass>|<cell|analytical
    balance>|<cell|<math|100<units|g>>>|<cell|<math|0.1<units|m*g>>>>|<row|<cell|>|<cell|microbalance>|<cell|<math|20<units|m*g>>>|<cell|<math|0.1<units|<math|\<up-mu\>>g>>>>|<row|<cell|Volume>|<cell|pipet,
    Class A>|<cell|<math|10<units|m*L>>>|<cell|<math|0.02<units|m*L>>>>|<row|<cell|>|<cell|volumetric
    flask, Class A>|<cell|<math|1<units|L>>>|<cell|<math|0.3<units|m*L>>>>|<row|<cell|Density>|<cell|pycnometer,
    25-mL capacity>|<cell|<math|1<units|g*<space|0.17em>m*L<per>>>>|<cell|<math|2<units|m*g*<space|0.17em>m*L<per>>>>>|<row|<cell|>|<cell|magnetic
    float densimeter>|<cell|<math|1<units|g*<space|0.17em>m*L<per>>>>|<cell|<math|0.1<units|m*g*<space|0.17em>m*L<per>>>>>|<row|<cell|>|<cell|vibrating-tube
    densimeter>|<cell|<math|1<units|g*<space|0.17em>m*L<per>>>>|<cell|<math|0.01<units|m*g*<space|0.17em>m*L<per>>>>>|<row|<cell|Pressure>|<cell|mercury
    manometer or barometer>|<cell|<math|760<units|T*o*r*r>>>|<cell|<math|0.001<units|T*o*r*r>>>>|<row|<cell|>|<cell|diaphragm
    gauge>|<cell|<math|100<units|T*o*r*r>>>|<cell|<math|1<units|T*o*r*r>>>>|<row|<cell|Temperature>|<cell|constant-volume
    gas thermometer>|<cell|<math|10<K>>>|<cell|<math|0.001<K>>>>|<row|<cell|>|<cell|mercury-in-glass
    thermometer>|<cell|<math|300<K>>>|<cell|<math|0.01<K>>>>|<row|<cell|>|<cell|platinum
    resistance thermometer>|<cell|<math|300<K>>>|<cell|<math|0.0001<K>>>>|<row|<cell|>|<cell|monochromatic
    optical pyrometer>|<cell|<math|1300<K>>>|<cell|<math|0.03<K>>>>>>>>
  </big-table|Representative measurement methods>

  The SI unit of volume is the cubic meter, but chemists commonly express
  volumes in units of liters and milliliters. The
  <index|Liter><newterm|liter> is defined as one cubic decimeter (Table
  <reference|tbl:1-non-SI units>). One cubic meter is the same as
  <math|10<rsup|3>> liters and <math|10<rsup|6>> milliliters. The
  <index|Milliliter><em|milliliter> is identical to the cubic centimeter.

  <minor| Before 1964, the liter had a different definition: it was the
  volume of <math|1> kilogram of water at <math|3.98<units|<degC>>>, the
  temperature of maximum density. This definition made one liter equal to
  <math|1.000028<units|d*m<rsup|<math|3>>>>. Thus, a numerical value of
  volume (or density) reported before 1964 and based on the liter as then
  defined may need a small correction in order to be consistent with the
  present definition of the liter.> <I|Volume!meaurement of\|)>

  <subsection|Density>

  <I|Density!measurement of\|(>Density, an intensive property, is defined as
  the ratio of the two extensive properties mass and volume:

  <\equation>
    <label|rho=m/V>\<rho\><defn><frac|m|V>
  </equation>

  <subindex|Volume|molar>The molar volume <math|V<m>> of a homogeneous pure
  substance is inversely proportional to its density. From Eqs.
  <reference|V/n=Vm>, <reference|M=m/n>, and <reference|rho=m/V>, we obtain
  the relation

  <\equation>
    <label|Vm=M/rho>V<m>=<frac|M|\<rho\>>
  </equation>

  Various methods are available for determining the density of a phase, many
  of them based on the measurement of the mass of a fixed volume or on a
  buoyancy technique. Three examples are shown in Fig.
  <reference|fig:2-density><vpageref|fig:2-density>.

  <\big-figure>
    <\boxedfigure>
      <image|./02-SUP/density.eps||||>

      <\capt>
        Three methods for measuring liquid density by comparison with samples
        of known density. The liquid is indicated by gray shading.

        \ (a)<nbsp>Glass pycnometer vessel with capillary stopper. The filled
        pycnometer is brought to the desired temperature in a thermostat
        bath, dried, and weighed.

        \ (b)<nbsp>Magnetic float densimeter.<space|.15em><footnote|Ref.
        <cite|greer-74>.> Buoy <math|B>, containing a magnet, is pulled down
        and kept in position with solenoid <math|S> by means of position
        detector <math|D> and servo control system <math|C>. The solenoid
        current required depends on the liquid density.

        \ (c)<nbsp>Vibrating-tube densimeter. The ends of a liquid-filled
        metal <with|font-family|ss|U>-tube are clamped to a stationary block.
        An oscillating magnetic field at the tip of the tube is used to make
        it vibrate in the direction perpendicular to the page. The measured
        resonance frequency is a function of the mass of the liquid in the
        tube.<label|fig:2-density>
      </capt>
    </boxedfigure>
  </big-figure|>

  Similar apparatus may be used for gases. The density of a solid may be
  determined from the volume of a nonreacting liquid (e.g., mercury)
  displaced by a known mass of the solid, or from the loss of weight due to
  buoyancy when the solid is suspended by a thread in a liquid of known
  density. <I|Density!measurement of\|)>

  <subsection|Pressure><label|2-pressure>

  <I|Pressure!measurement of\|(>Pressure is a force per unit area.
  Specifically, it is the normal component of stress exerted by an
  <index|Isotropic fluid><subindex|Fluid|isotropic>isotropic fluid on a
  surface element.<footnote|A liquid crystal and a polar liquid in a electric
  field are examples of fluids that are not isotropic, because they have
  different macroscopic properties in different directions.> The surface can
  be an interface surface between the fluid and another phase, or an
  imaginary dividing plane within the fluid.

  Pressure is usually a positive quantity. Because cohesive forces exist in a
  liquid, it may be possible to place the liquid under tension and create a
  <em|negative><label|neg p>pressure. For instance, the pressure is negative
  at the top of a column of liquid mercury suspended below the closed end of
  a capillary tube that has no vapor bubble. Negative pressure in a liquid is
  an unstable condition that can result in spontaneous vaporization.

  The SI unit of pressure is the <I|Pascal (unit)\|reg><newterm|pascal>. Its
  symbol is Pa. One pascal is a force of one newton per square meter (Table
  <reference|tbl:1-derived units>).

  Chemists are accustomed to using the non-SI units of millimeters of
  mercury, torr, and atmosphere. One millimeter of mercury (symbol mmHg) is
  the pressure exerted by a column exactly <math|1<units|m*m>> high of a
  fluid of density equal to exactly <math|13.5951<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>
  (the density of mercury at <math|0<units|<degC>>>) in a place where the
  acceleration of free fall, <math|g>, has its standard value
  <math|g<subs|n>> (see Appendix <reference|app:const>). One atmosphere is
  defined as exactly <math|1.01325<timesten|5><Pa>> (Table
  <reference|tbl:1-non-SI units>). The torr is defined by letting one
  atmosphere equal exactly <math|760<units|T*o*r*r>>. One atmosphere is
  approximately <math|760<units|m*m*H*g>>. In other words, the millimeter of
  mercury and the torr are practically identical; they differ from one
  another by less than <math|2<timesten|-7><units|T*o*r*r>>.

  Another non-SI pressure unit is the <index|Bar><newterm|bar>, equal to
  exactly <math|10<rsup|5><Pa>>. A pressure of one bar is approximately one
  percent smaller than one atmosphere. This book often refers to a
  <subindex|Standard|pressure><subindex|Pressure|standard><newterm|standard
  pressure>, <math|p<st>>. In the past, the value of <math|p<st>> was usually
  taken to be <math|1<units|a*t*m>>, but since 1982 the IUPAC has recommended
  the value <math|p<st|=>1<br>>.

  A variety of manometers and other devices is available to measure the
  pressure of a fluid, each type useful in a particular pressure range. Some
  devices measure the pressure of the fluid directly. Others measure the
  differential pressure between the fluid and the atmosphere; the fluid
  pressure is obtained by combining this measurement with the atmospheric
  pressure measured with a barometer.

  Within a <em|solid>, pressure cannot be defined simply as a force per unit
  area. Macroscopic forces at a point within a solid are described by the
  nine components of a stress tensor. The statement that a solid <em|has> or
  <em|is at> a certain pressure means that this is the hydrostatic pressure
  exerted on the solid's exterior surface. Thus, a solid immersed in a
  uniform isotropic fluid of pressure <math|p> is at pressure <math|p>; if
  the fluid pressure is constant over time, the solid is at constant
  pressure. <I|Pressure!measurement of\|)>

  <subsection|Temperature><label|2-temperature>

  <I|Temperature!measurement of\|(>

  Temperature and thermometry are of fundamental importance in
  thermodynamics. Unlike the other physical quantities discussed in this
  chapter, temperature does not have a single unique definition. The chosen
  definition, whatever it may be, requires a <index|Temperature
  scale><em|temperature scale> described by an operational method of
  measuring temperature values. For the scale to be useful, the values should
  increase monotonically with the increase of what we experience
  physiologically as the degree of \Photness.\Q We can define a satisfactory
  scale with any measuring method that satisfies this requirement. The values
  on a particular temperature scale correspond to a particular physical
  quantity and a particular temperature unit.

  For example, suppose you construct a simple
  <subindex|Thermometer|liquid-in-glass>liquid-in-glass thermometer with
  equally spaced marks along the stem and number the marks consecutively. To
  define a temperature scale and a temperature unit, you could place the
  thermometer in thermal contact with a body whose temperature is to be
  measured, wait until the indicating liquid reaches a stable position, and
  read the meniscus position by linear interpolation between two marks. Of
  course, placing the thermometer and body in thermal contact may affect the
  body's temperature. The measured temperature is that of the body <em|after>
  thermal equilibrium is achieved.

  Thermometry is based on the principle that the temperatures of different
  bodies may be compared with a thermometer. For example, if you find by
  separate measurements with your thermometer that two bodies give the same
  reading, you know that within experimental error both have the same
  temperature. The significance of two bodies having the same temperature (on
  any scale) is that if they are placed in thermal contact with one another,
  they will prove to be in thermal equilibrium with one another as evidenced
  by the absence of any changes in their properties. This principle is
  sometimes called the <index|Zeroth law of thermodynamics><em|zeroth law of
  thermodynamics>, and was first stated as follows by <index|Maxwell, James
  Clerk>J. C. Maxwell (1872): \PBodies whose temperatures are equal to that
  of the same body have themselves equal temperatures.\Q

  <subsubsection|Equilibrium systems for fixed temperatures>

  <I|Temperature!equilibrium systems for fixed values\|(>

  The <subindex|Ice point|for fixed temperature><em|ice point> is the
  temperature at which ice and air-saturated water coexist in equilibrium at
  a pressure of one atmosphere. The <index|Steam point><em|steam point> is
  the temperature at which liquid and gaseous H<rsub|<math|2>>O coexist in
  equilibrium at one atmosphere. Neither of these temperatures has sufficient
  reproducibility for high-precision work. The temperature of the
  ice-water-air system used to define the ice point is affected by air
  bubbles in the ice and by varying concentrations of air in the water around
  each piece of ice. The steam point is uncertain because the temperature of
  coexisting liquid and gas is a sensitive function of the experimental
  pressure.

  The <subindex|Melting point|for fixed temperature>melting point of the
  solid phase of a pure substance is a more reproducible temperature. When
  the solid and liquid phases of a pure substance coexist at a controlled,
  constant pressure, the temperature has a definite fixed value.

  <subsubindex|Triple|point|for fixed temperature>Triple points of pure
  substances provide the most reproducible temperatures. Both temperature and
  pressure have definite fixed values in a system containing coexisting
  solid, liquid, and gas phases of a pure substance.

  Figure <reference|fig:2-tp cell><vpageref|fig:2-tp cell>

  <\big-figure>
    <boxedfigure|<image|./02-SUP/tp-cell.eps||||> <capt|Cross-section of a
    water triple-point cell. The cell has cylindrical symmetry about a
    vertical axis. Pure water of the same isotopic composition as
    H<rsub|<math|2>>O in ocean water is distilled into the cell. The air is
    pumped out and the cell is sealed. A freezing mixture is placed in the
    inner well to cause a thin layer of ice to form next to the inner wall.
    The freezing mixture is removed, and some of the ice is allowed to melt
    to a film of very pure water between the ice and inner wall. The
    thermometer bulb is placed in the inner well as shown, together with ice
    water (not shown) for good thermal contact.<label|fig:2-tp cell>>>
  </big-figure|>

  illustrates a <subsubindex|Triple|point|cell>triple-point cell for water
  whose temperature is capable of a reproducibility within
  <math|10<rsup|-4><K>>. When ice, liquid water, and water vapor are in
  equilibrium in this cell, the cell is at the triple point of water.
  <I|Temperature!equilibrium systems for fixed values\|)>

  <subsubsection|Temperature scales>

  <I|Temperature!scales\|(>

  Six different temperature scales are described below: the ideal-gas
  temperature scale, the thermodynamic temperature scale, the obsolete
  centigrade scale, the Celsius scale, the International Temperature Scale of
  1990, and the Provisional Low Temperature Scale of 2000.

  The <em|ideal-gas temperature scale> is defined by gas thermometry
  measurements, as described on page <pageref|2-gas thermometry>. The
  <em|thermodynamic temperature scale> is defined by the behavior of a
  theoretical Carnot engine, as explained in Sec. <reference|4-thermodynamic
  temperature>. These temperature scales correspond to the physical
  quantities called <index|Ideal-gas temperature><I|Temperature!ideal
  gas@ideal-gas\|reg>ideal-gas temperature and
  <subindex|Thermodynamic|temperature><subindex|Temperature|thermodynamic>thermodynamic
  temperature, respectively. Although the two scales have different
  definitions, the two temperatures turn out (Sec. <reference|4-thermodynamic
  temperature>) to be proportional to one another. Their values become
  identical when the same unit of temperature is used for both.

  Prior to the 2019 SI revision, the <I|Kelvin (unit)\|reg><newterm|kelvin>
  was defined by specifying that a system containing the solid, liquid, and
  gaseous phases of H<rsub|<math|2>>O coexisting at equilibrium with one
  another (the <I|Triple!point!H2O@of H<rsub|<math|2>>O\|reg><em|triple point
  of water>) has a thermodynamic temperature of exactly <math|273.16>
  kelvins.<inactive|<label|273.16K=water triple pt>>(This value was chosen to
  make the steam point approximately one hundred kelvins greater than the ice
  point.) The ideal-gas temperature of this system was set equal to the same
  value, <math|273.16> kelvins, making temperatures measured on the two
  scales identical.<inactive|<label|identical temperature scales>>

  The <subindex|SI|2019 revision>2019 SI revision treats the triple point
  temperature of water as a value to be determined experimentally by primary
  thermometry (page <pageref|2-primary thermometry>). The result is
  <math|273.16> kelvins to within <math|1<timesten|-7><units|K>>.<footnote|Ref.
  <cite|stock-19>, Appendix 2.> Thus there is no practical difference between
  the old and new definitions of the kelvin.

  Formally, the symbol <math|T> refers to thermodynamic temperature. Strictly
  speaking, a different symbol should be used for ideal-gas temperature.
  Since the two kinds of temperatures have identical values, this book will
  use the symbol <math|T> for both and refer to both physical quantities
  simply as \Ptemperature\Q except when it is necessary to make a
  distinction.

  The obsolete <index|Centigrade scale><em|centigrade scale> was defined to
  give a value of exactly <math|0> degrees centigrade at the ice point and a
  value of exactly <math|100> degrees centigrade at the steam point, and to
  be a linear function of an ideal-gas temperature scale.

  The centigrade scale has been replaced by the
  <subindex|Celsius|scale><em|Celsius scale>, the thermodynamic (or
  ideal-gas) temperature scale shifted by exactly <math|273.15> kelvins. The
  temperature unit is the degree Celsius (<math|<degC>>), identical in size
  to the kelvin. Thus, <subindex|Celsius|temperature>Celsius temperature
  <math|t> is related to thermodynamic temperature <math|T> by

  <\equation>
    <inactive|<label|t/oC=T/K-273.15>>t/<degC>=T/<tx|K>-273.15
  </equation>

  On the Celsius scale, the <I|Triple!point!H2O@of
  H<rsub|<math|2>>O\|reg>triple point of water is exactly
  <math|0.01<units|<degC>>>. The <index|Ice point>ice point is
  <math|0<units|<degC>>> to within <math|0.0001<units|<degC>>>, and the
  <index|Steam point>steam point is <math|99.97<units|<degC>>>.

  The <I|International temperature scale of
  1990\|seeITS-90><I|Temperature!international scale of
  1990\|seeITS-90><em|International Temperature Scale of
  1990><label|2-ITS-90><index|ITS-90>(abbreviated ITS-90) defines the
  physical quantity called international temperature, with symbol
  <math|T<rsub|90>>.<footnote|Refs. <cite|mcglashan-90> and
  <cite|preston-90>.> Each value of <math|T<rsub|90>> is intended to be very
  close to the corresponding thermodynamic temperature <math|T>.

  The ITS-90 scale is defined over a very wide temperature range, from
  <math|0.65<K>> up to at least <math|1358<K>>. There is a specified
  procedure for each measurement of <math|T<rsub|90>>, depending on the range
  in which <math|T> falls: vapor-pressure thermometry
  (<math|0.65>\U<math|5.0<K>>), gas thermometry
  (<math|3.0>\U<math|24.5561<K>>), platinum-resistance thermometry
  (<math|13.8033>\U<math|1234.93<K>>), or optical pyrometry (above
  <math|1234.93<K>>). For vapor-pressure thermometry, the ITS-90 scale
  provides formulas for <math|T<rsub|90>> in terms of the vapor pressure of
  the helium isotopes <rsup|<math|3>>He and <rsup|<math|4>>He. For the other
  methods, it assigns values of fourteen fixed calibration temperatures
  achieved with the reproducible equilibrium systems listed in Table
  <reference|tbl:2-ITS-90><vpageref|tbl:2-ITS-90>, and provides interpolating
  functions for intermediate temperatures.

  <\big-table>
    <minipagetable|5.9cm|<label|tbl:2-ITS-90><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|1|1|cell-halign|c>|<cwith|1|1|1|1|cell-lborder|0ln>|<cwith|1|1|1|1|cell-rborder|0ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|15|15|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|T<rsub|90>/<space|-0.17em><K>>>|<cell|Equilibrium
    system>>|<row|<cell|13.8033>|<cell|H<rsub|<math|2>> triple
    point>>|<row|<cell|24.5561>|<cell|Ne triple
    point>>|<row|<cell|54.3584>|<cell|O<rsub|<math|2>> triple
    point>>|<row|<cell|83.8058>|<cell|Ar triple
    point>>|<row|<cell|234.3156>|<cell|Hg triple
    point>>|<row|<cell|273.16>|<cell|H<rsub|<math|2>>O triple
    point>>|<row|<cell|302.9146>|<cell|Ga melting point at
    <math|1<units|a*t*m>>>>|<row|<cell|429.7485>|<cell|In melting point at
    <math|1<units|a*t*m>>>>|<row|<cell|505.078>|<cell|Sn melting point at
    <math|1<units|a*t*m>>>>|<row|<cell|692.677>|<cell|Zn melting point at
    <math|1<units|a*t*m>>>>|<row|<cell|933.473>|<cell|Al melting point at
    <math|1<units|a*t*m>>>>|<row|<cell|1234.93>|<cell|Ag melting point at
    <math|1<units|a*t*m>>>>|<row|<cell|1337.33>|<cell|Au melting point at
    <math|1<units|a*t*m>>>>|<row|<cell|1357.77>|<cell|Cu melting point at
    <math|1<units|a*t*m>>>>>>>>
  <|big-table>
    Fixed temperatures of the

    \ International Temperature Scale of 1990
  </big-table>

  The <I|Provisional low temperature scale of
  2000\|seePLST-2000><I|Temperature!provisional scale of
  2000\|seePLST-2000><em|Provisional Low Temperature Scale of 2000>
  (PLST-2000) is for temperatures between 0.0009<units|K> and 1<units|K>.
  This scale is based on the melting temperature of solid <rsup|<math|3>>He
  as a function of pressure. For <rsup|<math|3>>He at these temperatures, the
  required pressures are in the range 30\U40<br>.<footnote|Ref.
  <cite|rusby-02>.>

  The temperatures defined by the ITS-90 and PLST-2000 temperature scales are
  exact with respect to the respective scale\Vtheir values remain unchanged
  during the life of the scale.<footnote|Ref. <cite|fellmuth-16>.>

  <I|Temperature!scales\|)>

  <subsubsection|Primary thermometry><inactive|<label|2-primary thermometry>>

  <I|Primary thermometry\|(><I|Thermometry!primary\|(>Primary thermometry is
  the measurement of temperature based on fundamental physical principles.
  Until about 1960, primary measurements of <math|T> involved gas
  thermometry. Other more accurate methods are now being used; they require
  elaborate equipment and are not convenient for routine measurements of
  <math|T>.

  The methods of primary thermometry require the value of the
  <index|Boltzmann constant>Boltzmann constant <math|k> or the <index|Gas
  constant>gas constant <math|R=N<subs|A>k>, where <math|N<subs|A>> is the
  Avogadro constant. <math|k> and <math|N<subs|A>> are defining constants of
  the 2019 revision of the SI. Using these fixed values (Appendix
  <reference|app:const>) in the calculations results in values of <math|T>
  consistent with the definition of the kelvin according to the 2019
  revision.

  <em|Gas thermometry><inactive|<label|2-gas thermometry>>
  <I|Gas!thermometry\|(><I|Thermometry!gas\|(>is based on the ideal gas
  equation <math|T=p*V/n*R>. It is most commonly carried out with a
  <subindex|Thermometer|constant-volume gas><em|constant-volume gas
  thermometer>. This device consists of a bulb or vessel containing a
  thermometric gas and a means of measuring the pressure of this gas. The
  thermometric gas is usually helium, because it has minimal deviations from
  ideal-gas behavior.

  The simple constant-volume gas thermometer depicted in Fig.
  <reference|fig:2-gas thermometer><vpageref|fig:2-gas thermometer>

  <\big-figure>
    <boxedfigure|<image|./02-SUP/gastherm.eps||||> <capt|Simple version of a
    constant-volume gas thermometer. The leveling bulb is raised or lowered
    to place the left-hand meniscus at the level indicator. The gas pressure
    is then determined from <math|<Del>h> and the density of the mercury:
    <math|p=p<subs|a*t*m|+>\<rho\>*g<Del>h>.<label|fig:2-gas thermometer>>>
  </big-figure|>

  uses a mercury manometer to measure the pressure. More sophisticated
  versions have a diaphragm pressure transducer between the gas bulb and the
  pressure measurement system.

  One procedure for determining the value of an unknown temperature involves
  a pair of pressure measurements. The gas is brought successively into
  thermal equilibrium with two different systems: a reference system of known
  temperature <math|T<rsub|1>> (such as one of the systems listed in Table
  <reference|tbl:2-ITS-90>), and the system whose temperature
  <math|T<rsub|2>> is to be measured. The pressures <math|p<rsub|1>> and
  <math|p<rsub|2>> are measured at these temperatures. In the two
  equilibrations the amount of gas is the same and the gas volume is the same
  except for a small change due to effects of <math|T> and <math|p> on the
  gas bulb dimensions.

  If the gas exactly obeyed the ideal gas equation in both measurements, we
  would have <math|n*R=p<rsub|1>*V<rsub|1>/T<rsub|1>=p<rsub|2>*V<rsub|2>/T<rsub|2>>
  or <math|T<rsub|2>=T<rsub|1>*<around|(|p<rsub|2>*V<rsub|2>/p<rsub|1>*V<rsub|1>|)>>.
  Since, however, the gas approaches ideal behavior only in the limit of low
  pressure, it is necessary to make a series of the paired measurements,
  changing the amount of gas in the bulb before each new pair so as to change
  the measured pressures in discrete steps. Thus, the operational equation
  for evaluating the unknown temperature is

  <\gather>
    <tformat|<table|<row|<cell|<s|T<rsub|2>=T<rsub|1>*lim<rsub|p<rsub|1><ra>0>
    <frac|p<rsub|2>*V<rsub|2>|p<rsub|1>*V<rsub|1>>><cond|<around|(|g*a*s|)>><eq-number><inactive|<label|T2(gas
    thermometer)>>>>>>
  </gather>

  (The ratio <math|V<rsub|2>/V<rsub|1>> differs from unity only because of
  any change in the gas bulb volume when <math|T> and <math|p> change.) The
  limiting value of <math|p<rsub|2>*V<rsub|2>/p<rsub|1>*V<rsub|1>> can be
  obtained by plotting this quantity against <math|p<rsub|1>>, <math|1/V<m>>,
  or another appropriate extrapolating function. Note that values of <math|n>
  and <math|R> are not needed.

  Another method is possible if the value of the second virial coefficient at
  the reference temperature <math|T<rsub|1>> is known. This value can be used
  in the virial equation (Eq. <reference|pVm=RT(1+B/Vm...)>) together with
  the values of <math|T<rsub|1>> and <math|p<rsub|1>> to evaluate the molar
  volume <math|V<m>>. Then, assuming <math|V<m>> is the same in both
  equilibrations of a measurement pair, it is possible to evaluate
  <math|p<rsub|2>*V<m>/R> at temperature <math|T<rsub|2>>, and
  <math|T<rsub|2>> can be found from

  <\gather>
    <tformat|<table|<row|<cell|<s|T<rsub|2>=lim<rsub|p<rsub|2><ra>0>
    <frac|p<rsub|2>*V<m>|R>><cond|<around|(|g*a*s|)>><eq-number>>>>>
  </gather>

  Constant-volume gas thermometry can be used to evaluate the second virial
  coefficient of the gas at temperature <math|T<rsub|2>> if the value at
  <math|T<rsub|1>> is known (Prob. 2.<reference|prb:2-He-virial>).

  The principles of measurements with a gas thermometer are simple, but in
  practice great care is needed to obtain adequate precision and accuracy.
  Corrections or precautions are required for such sources of error as
  thermal expansion of the gas bulb, \Pdead volume\Q between the bulb and
  pressure measurement system, adsorption of the thermometric gas on interior
  surfaces, and desorption of surface contaminants.

  <I|Gas!thermometry\|)><I|Thermometry!gas\|)>

  Since 1960 primary methods with lower uncertainty than gas thermometry have
  been developed and improved. <index|Acoustic gas
  thermometry><subindex|Thermometry|acoustic gas><em|Acoustic gas
  thermometry> is based on the speed of sound in an ideal monatomic gas
  (helium or argon).<footnote|Ref. <cite|moldover-14>.> The gas is confined
  in a metal cavity resonator of known internal dimensions. The thermodynamic
  temperature of the gas is calculated from
  <math|T=<around|(|3/5|)>*M*v<rsup|2>/R>, where <math|M> is the average
  molar mass of the gas and <math|v> is the measured speed of sound in the
  limit of zero frequency. To evaluate <math|T> in a phase of interest, small
  thermometers such as platinum resistor thermometers (page <pageref|Pt RT>)
  are moved from thermal contact with the phase to the outside of the metal
  resonator shell, and the readings compared.

  Values of thermodynamic temperatures <math|T> in the range 118<units|K> to
  323<units|K> obtained by acoustic gas thermometry agree with
  <math|T<rsub|90>> on the ITS-90 scale to within
  0.006<units|K>.<footnote|Ref. <cite|underwood-17>.> The agreement becomes
  better the closer <math|T> is to the water triple point 273.16<units|K>.
  <math|T> and <math|T<rsub|90>> are equal at 273.16<units|K>.

  Other kinds of primary thermometry capable of low uncertainty
  include<footnote|Ref. <cite|fischer-16>.>

  <\itemize>
    <item><index|Dielectric constant gas thermometry><subindex|Thermometry|dielectric
    constant gas><em|Dielectric constant gas thermometry>, based on the
    variation of the dielectric constant of an ideal gas with temperature;

    <item><index|Johnson noise thermometry><subindex|Thermometry|Johnson
    noise><em|Johnson noise thermometry>, based on measurements of the
    mean-square noise voltage developed in a resistor;

    <item><index|Doppler broadening thermometry><subindex|Thermometry|Doppler
    broadening><em|Doppler broadening thermometry>, based on measurements of
    the Doppler width of the absorption line when a laser beam passes through
    a gas.
  </itemize>

  <I|Primary thermometry\|)><I|Thermometry!primary\|)>

  <subsubsection|Practical thermometers><label|2-practical thermometers>

  <subindex|Thermometer|liquid-in-glass><em|Liquid-in-glass thermometers> use
  indicating liquids whose volume change with temperature is much greater
  than that of the glass. A mercury-in-glass thermometer can be used in the
  range <math|234<K>> (the freezing point of mercury) to <math|600<K>>, and
  typically can be read to <math|0.01<K>>. A
  <subindex|Thermometer|Beckmann>Beckmann thermometer covers a range of only
  a few kelvins but can be read to <math|0.001<K>>.

  A <subindex|Thermometer|resistance><em|resistance thermometer> is included
  in a circuit that measures the thermometer's electric resistance.
  <subindex|Thermometer|platinum resistance>Platinum resistance
  thermometers<label|Pt RT> are widely used because of their stability and
  high sensitivity (<math|0.0001<K>>). <subindex|Thermometer|thermistor>Thermistors
  use metal oxides and can be made very small; they have greater sensitivity
  than platinum resistance thermometers but are not as stable over time.

  A <subindex|Thermometer|thermocouple><em|thermocouple> consists of wires of
  two dissimilar metals (e.g., constantan alloy and copper) connected in
  series at soldered or welded junctions. A many-junction thermocouple is
  called a <subindex|Thermometer|thermopile><em|thermopile>. When adjacent
  junctions are placed in thermal contact with bodies of different
  temperatures, an electric potential develops that is a function of the two
  temperatures.

  Finally, two other temperature-measuring devices are the
  <subindex|Thermometer|quartz crystal><em|quartz crystal thermometer>,
  incorporating a quartz crystal whose resonance frequency is temperature
  dependent, and <subindex|Thermometer|optical pyrometer><em|optical
  pyrometers>, which are useful above about <math|1300<K>> to measure the
  radiant intensity of a black body emitter.

  The national laboratories of several countries, including the National
  Institute of Standards and Technology in the United States, maintain stable
  secondary thermometers (e.g., platinum resistance thermometers and
  thermocouples) that have been calibrated according to the ITS-90 scale.
  These secondary thermometers are used as working standards to calibrate
  other laboratory and commercial temperature-measuring devices.

  The <index|PLTS-2000>PLTS-2000 scale from 0.9<units|mK> to 1<units|K> and
  the <index|ITS-90>ITS-90 scale from 0.65<units|K> upwards are expected to
  continue to be used for precise, reproducible and practical approximations
  to thermodynamic temperature. In the temperature range
  23<units|K>\U1233<units|K>, the most precise measurements will be traceable
  to platinum resistance thermometers calibrated according to the ITS-90
  scale.<footnote|Ref. <cite|fellmuth-16>.>

  <I|Temperature!measurement of\|)>

  <section|The State of the System>

  <plainfootnotes><label|2-state of the system>

  <I|State!system@of a system\|(><I|System!state of\|(>

  The thermodynamic <I|State!system@of a system\|reg><subindex|System|state
  of><newterm|state> of the system is an important and subtle concept.

  <\minor>
    \ Do not confuse the <em|state> of the system with the kind of
    <em|physical state> or state of aggregation of a phase discussed in Sec.
    <reference|2-physical states>. A <em|change of state> refers to a change
    in the state of the system, not necessarily to a phase transition.
  </minor>

  At each instant of time, the system is in some definite state that we may
  describe with values of the macroscopic properties we consider to be
  relevant for our purposes. The values of these properties at any given
  instant define the state at that instant. Whenever the value of any of
  these properties changes, the state has changed. If we subsequently find
  that each of the relevant properties has the value it had at a certain
  previous instant, then the system has returned to its previous state.

  <subsection|State functions and independent variables><label|2-state fncs
  \ ind variables>

  <I|State function\|(>

  The properties whose values at each instant depend only on the state of the
  system at that instant, and not on the past or future history of the
  system, are called <index|State function><newterm|state functions> (or
  state variables, or state parameters). There may be other system properties
  that we consider to be irrelevant to the state, such as the shape of the
  system, and these are <em|not> state functions.

  Various conditions determine what states of a system are physically
  possible. If a uniform phase has an equation of state, property values must
  be consistent with this equation. The system may have certain built-in or
  externally-imposed conditions or constraints that keep some properties from
  changing with time. For instance, a closed system has constant mass; a
  system with a rigid boundary has constant volume. We may know about other
  conditions that affect the properties during the time the system is under
  observation.

  We can define the state of the system with the values of a certain minimum
  number of state functions which we treat as the <index|Independent
  variables><subindex|Variables|independent><newterm|independent variables>.
  Once we have selected a set of independent variables, consistent with the
  physical nature of the system and any conditions or constraints, we can
  treat all other state functions as <index|Dependent
  variable><subindex|Variables|dependent><newterm|dependent variables> whose
  values depend on the independent variables.

  Whenever we adjust the independent variables to particular values, every
  other state function is a dependent variable that can have only one
  definite, reproducible value. For example, in a single-phase system of a
  pure substance with <math|T>, <math|p>, and <math|n> as the independent
  variables, the volume is determined by an equation of state in terms of
  <math|T>, <math|p>, and <math|n>; the mass is equal to <math|n*M>; the
  molar volume is given by <math|V<m|=>V/n>; and the density is given by
  <math|\<rho\>=n*M/V>.

  <subsection|An example: state functions of a mixture>

  Table <reference|tbl:2-state><vpageref|tbl:2-state>

  <\big-table>
    <minipagetable|7.6cm|<label|tbl:2-state><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|10|10|1|-1|cell-bborder|1ln>|<table|<row|<cell|temperature>|<cell|<math|T=293.15<K>>>>|<row|<cell|pressure>|<cell|<math|p=1.01<br>>>>|<row|<cell|amount
    of water>|<cell|<math|n<A>=39.18<mol>>>>|<row|<cell|amount of
    sucrose>|<cell|<math|n<B>=1.375<mol>>>>|<row|<cell|volume>|<cell|<math|V=1000<units|c*m<rsup|<math|3>>>>>>|<row|<cell|mass>|<cell|<math|m=1176.5<units|g>>>>|<row|<cell|density>|<cell|<math|\<rho\>=1.1765<units|g*<space|0.17em>c*m<rsup|<math|-3>>>>>>|<row|<cell|mole
    fraction of sucrose>|<cell|<math|x<B>=0.03390>>>|<row|<cell|osmotic
    pressure>|<cell|<math|<varPi>=58.2<br>>>>|<row|<cell|refractive index,
    sodium D line>|<cell|<math|n<subs|D>=1.400>>>>>>>
  </big-table|Values of state functions of an aqueous sucrose solution (A =
  water, B = sucrose)>

  lists the values of ten state functions of an aqueous sucrose solution in a
  particular state. The first four properties (<math|T>, <math|p>,
  <math|n<A>>, <math|n<B>>) are ones that we can vary independently, and
  their values suffice to define the state for most purposes. Experimental
  measurements will convince us that, whenever these four properties have
  these particular values, each of the other properties has the one definite
  value listed\Vwe cannot alter any of the other properties without changing
  one or more of the first four variables. Thus we can take <math|T>,
  <math|p>, <math|n<A>>, and <math|n<B>> as the independent variables, and
  the six other properties as dependent variables. The other properties
  include one (<math|V>) that is determined by an <index|Equation of
  state>equation of state; three (<math|m>, <math|\<rho\>>, and <math|x<B>>)
  that can be calculated from the independent variables and the equation of
  state; a solution property (<math|<varPi>>) treated by thermodynamics (Sec.
  <reference|12-osmotic pressure>); and an optical property
  (<math|n<subs|D>>). In addition to these six dependent variables, this
  system has innumerable others: energy, isothermal compressibility, heat
  capacity at constant pressure, and so on.

  We could make other choices of the independent variables for the aqueous
  sucrose system. For instance, we could choose the set <math|T>, <math|p>,
  <math|V>, and <math|x<B>>, or the set <math|p>, <math|V>, <math|\<rho\>>,
  and <math|x<B>>. If there are no imposed conditions, the <em|number> of
  <subindex|Variables|number of independent>independent variables of this
  system is always four. (Note that we could not arbitrarily choose just any
  four variables. For instance, there are only three independent variables in
  the set <math|p>, <math|V>, <math|m>, and <math|\<rho\>> because of the
  relation <math|\<rho\>=m/V>.)

  If the system has imposed conditions, there will be fewer independent
  variables. Suppose the sucrose solution is in a closed system with fixed,
  known values of <math|n<A>> and <math|n<B>>; then there are only two
  independent variables and we could describe the state by the values of just
  <math|T> and <math|p>. <I|State function\|)>

  <subsection|More about independent variables><label|2-more about ind
  variables>

  A closed system containing a single substance in a single phase has two
  independent variables, as we can see by the fact that the state is
  completely defined by values of <math|T> and <math|p> or of <math|T> and
  <math|V>.

  A closed single-phase system containing a mixture of several nonreacting
  substances, or a mixture of reactants and products in reaction equilibrium,
  also has two independent variables. Examples are

  <\itemize>
    <item>air, a mixture of gases in fixed proportions;

    <item>an aqueous ammonia solution containing H<rsub|<math|2>>O,
    NH<rsub|<math|3>>, <math|<chem|N*H<rsub|4>><rsup|+>>, H<rsup|<math|+>>,
    OH<rsup|<math|->>, and probably other species, all in rapid continuous
    equilibrium.
  </itemize>

  The systems in these two examples contain more than one substance, but only
  one <em|component>. The <index|Components, number of>number of components
  of a system is the minimum number of substances or mixtures of fixed
  composition needed to form each phase.<footnote|The concept of the number
  of components is discussed in more detail in Chap. <reference|Chap. 13>.> A
  system of a single pure substance is a special case of a system of one
  component. In an <em|open> system, the amount of each component can be used
  as an independent variable.

  Consider a system with more than one uniform phase. In principle, for each
  phase we could independently vary the temperature, the pressure, and the
  amount of each substance or component. There would then be <math|2+C>
  independent variables for each phase, where <math|C> is the number of
  components in the phase.

  There usually are, however, various equilibria and other conditions that
  reduce the number of independent variables. For instance, each phase may
  have the same temperature and the same pressure; equilibrium may exist with
  respect to chemical reaction and transfer between phases (Sec.
  <reference|2-eq states>); and the system may be closed. (While these
  various conditions do not <em|have> to be present, the relations among
  <math|T>, <math|p>, <math|V>, and amounts described by an <index|Equation
  of state>equation of state of a phase are <em|always> present.) On the
  other hand, additional independent variables are required if we consider
  properties such as the surface area of a liquid to be
  relevant.<footnote|The important topic of the number of independent
  <em|intensive> variables is treated by the Gibbs phase rule, which will be
  discussed in Sec. <reference|8-Gibbs phase rule> for systems of a single
  substance and in Sec. <reference|13-phase rule> for systems of more than
  one substance.>

  <\minor>
    \ We must be careful to choose a set of independent variables that
    defines the state without ambiguity. For a closed system of liquid water,
    the set <math|p> and <math|V> might be a poor choice because the molar
    volume of water passes through a minimum as <math|T> is varied at
    constant <math|p>. Thus, the values <math|p=1.000<br>> and
    <math|V=18.016<units|c*m<rsup|<math|3>>>> would describe one mole of
    water at both <math|2<units|<degC>>> and <math|6<units|<degC>>>, so these
    values would not uniquely define the state. Better choices of independent
    variables in this case would be either <math|T> and <math|p>, or else
    <math|T> and <math|V>.
  </minor>

  How may we describe the state of a system that has nonuniform regions? In
  this case we may imagine the regions to be divided into many small volume
  elements or parcels, each small enough to be essentially uniform but large
  enough to contain many molecules. We then describe the state by specifying
  values of independent variables for each volume element. If there is
  internal macroscopic motion (e.g., flow), then velocity components can be
  included among the independent variables. Obviously, the quantity of
  information needed to describe a complicated state may be enormous.

  We can imagine situations in which classical thermodynamics would be
  completely incapable of describing the state. For instance, turbulent flow
  in a fluid or a shock wave in a gas may involve inhomogeneities all the way
  down to the molecular scale. Macroscopic variables would not suffice to
  define the states in these cases.

  Whatever our choice of independent variables, all we need to know to be
  sure a system is in the same state at two different times is that <em|the
  value of each independent variable is the same at both times>.

  <subsection|Equilibrium states><label|2-eq states>

  <I|Equilibrium state\|(><I|State!equilibrium\|(>

  An <index|Equilibrium state><subindex|State|equilibrium><newterm|equilibrium
  state> is a state that, when present in an <index|Isolated
  system><subindex|System|isolated>isolated system, remains unchanged
  indefinitely as long as the system remains isolated. (Recall that an
  isolated system is one that exchanges no matter or energy with the
  surroundings.) An equilibrium state of an isolated system has no natural
  tendency to change over time. If changes <em|do> occur in an isolated
  system, they continue until an equilibrium state is reached.

  A system in an equilibrium state may have some or all of the following
  kinds of internal equilibria:<label|kinds of internal equilibria>

  <\plainlist>
    <item><with|font-series|bold|Thermal equilibrium>:
    \ <subindex|Equilibrium|thermal>the temperature is uniform throughout.

    <item><with|font-series|bold|Mechanical equilibrium>:
    \ <subindex|Equilibrium|mechanical>the pressure is uniform throughout.

    <item><with|font-series|bold|Transfer equilibrium>:
    \ <subindex|Equilibrium|transfer>there is equilibrium with respect to the
    transfer of each species from one phase to another.

    <item><with|font-series|bold|Reaction equilibrium>:
    \ <subindex|Equilibrium|reaction>every possible chemical reaction is at
    equilibrium.
  </plainlist>

  A <em|homogeneous> system has a single phase of uniform temperature and
  pressure, and so has thermal and mechanical equilibrium. It is in an
  equilibrium state if it also has reaction equilibrium.

  A <em|heterogeneous> system is in an equilibrium state if each of the four
  kinds of internal equilibrium is present.

  The meaning of internal equilibrium in the context of an equilibrium state
  is that no perceptible change of state occurs during the period we keep the
  isolated system under observation. For instance, a system containing a
  homogeneous mixture of gaseous H<rsub|<math|2>> and O<rsub|<math|2>> at
  <math|25<units|<degC>>> and <math|1<br>> is in a state of reaction
  equilibrium on a time scale of hours or days; but if a measurable amount of
  H<rsub|<math|2>>O forms over a longer period, the state is not an
  equilibrium state on this longer time scale. This consideration of time
  scale is similar to the one we apply to the persistence of deformation in
  distinguishing a solid from a fluid (Sec. <reference|2-physical states>).

  Even if a system is not in internal equilibrium, it can be in an
  equilibrium state if a change of state is prevented by an imposed internal
  constraint or the influence of an <index|External
  field><subindex|Field|external>external field. Here are five examples of
  such states:

  <\itemize>
    <item>A system with an internal adiabatic partition separating two phases
    can be in an equilibrium state that is not in thermal equilibrium. The
    adiabatic partition allows the two phases to remain indefinitely at
    different temperatures. If the partition is rigid, it can also allow the
    two phases to have different pressures, so that the equilibrium state
    lacks mechanical equilibrium.

    <item>An experimental system used to measure osmotic pressure
    <index|Osmotic pressure>(Fig. <reference|fig:12-osmotic
    pressure><vpageref|fig:12-osmotic pressure>) has a <index|Membrane,
    semipermeable>semipermeable membrane separating a liquid solution phase
    and a pure solvent phase. The membrane prevents the transfer of solute
    from the solution to the pure solvent. In the equilibrium state of this
    system, the solution has a higher pressure than the pure solvent; the
    system is then in neither transfer equilibrium nor mechanical
    equilibrium.

    <item>In the equilibrium state of a galvanic cell <I|Galvanic
    cell!equilibrium state@in an equilibrium state\|reg>that is not part of a
    closed electrical circuit (Sec. <reference|3-galvanic cell>), the
    separation of the reactants and products and the open circuit are
    constraints that prevent the cell reaction from coming to reaction
    equilibrium.

    <item>A system containing mixed reactants of a reaction can be in an
    equilibrium state without reaction equilibrium if we withhold a catalyst
    or initiator or introduce an inhibitor that prevents reaction. In the
    example above of a mixture of H<rsub|<math|2>> and O<rsub|<math|2>>
    gases, we could consider the high activation energy barrier for the
    formation of H<rsub|<math|2>>O to be an internal constraint. If we remove
    the constraint by adding a catalyst, the reaction will proceed
    explosively.

    <item>An example of a system influenced by an external field is a tall
    column of gas in a <subindex|Gravitational|field><subindex|Field|gravitational>gravitational
    field (Sec. <reference|8-gas in gravity>). In order for an equilibrium
    state to be established in this field, the pressure must decrease
    continuously with increasing elevation.
  </itemize>

  Keep in mind that regardless of the presence or absence of internal
  constraints and external fields, the essential feature of an equilibrium
  state is this: if we isolate the system while it is in this state, <em|the
  state functions do not change over time>.

  Three additional comments can be made regarding the properties of
  equilibrium states.

  <\enumerate>
    <item>It should be apparent that a system with thermal equilibrium has a
    single value of <math|T>, and one with mechanical equilibrium has a
    single value of <math|p>, and this allows the state to be described by a
    minimal number of independent variables. In contrast, the definition of a
    nonequilibrium state with nonuniform intensive properties may require a
    very large number of independent variables.

    <item>Strictly speaking, during a time period in which the system
    exchanges energy with the surroundings its state cannot be an equilibrium
    state. Energy transfer at a finite rate causes nonuniform temperature and
    pressure within the system and prevents internal thermal and mechanical
    equilibrium. If, however, the rate of energy transfer is small, then at
    each instant the state can closely approximate an equilibrium state. This
    topic will be discussed in detail in the next chapter.

    <item><inactive|<label|very slow changes>> The concept of an equilibrium
    state assumes that when the system is in this state and isolated, no
    observable change occurs during the period of experimental observation.
    If the state does, in fact, undergo a slow change that is too small to be
    detected during the experimental observation period
    <math|<Del>t<subs|e*x*p>>, the state is <index|Metastable
    state><subindex|State|metastable><em|metastable>\Vthe relaxation time of
    the slow change is much greater than <math|<Del>t<subs|e*x*p>>. There is
    actually no such thing as a true equilibrium state, because very slow
    changes inevitably take place that we have no way of controlling. One
    example was mentioned above: the slow formation of water from its
    elements in the absence of a catalyst. Atoms of radioactive elements with
    long half-lives slowly change to other elements. More generally, all
    elements are presumably subject to eventual transmutation to iron-58 or
    nickel-62, the nuclei with the greatest binding energy per nucleon. When
    we use the concept of an equilibrium state, we are in effect assuming
    that rapid changes that have come to equilibrium have relaxation times
    much shorter than <math|<Del>t<subs|e*x*p>> and that the relaxation times
    of all other changes are infinite.
  </enumerate>

  <I|Equilibrium state\|)><I|State!equilibrium\|)>

  <subsection|Steady states>

  It is important not to confuse an equilibrium state with a<label|steady
  state><index|Steady state><subindex|State|steady><newterm|steady state>, a
  state that is constant during a time period during which the system
  exchanges matter or energy with the surroundings.

  The heat-conducting metal rod shown in Fig. <reference|fig:2-steady
  state><vpageref|fig:2-steady state>

  <\big-figure>
    <boxedfigure|<image|./02-SUP/rod.eps||||> <capt|Steady state in a metal
    rod (shaded) with heat conduction. The boxes at the ends represent heat
    reservoirs of constant temperature.<label|fig:2-steady state>>>
  </big-figure|>

  is a system in such a steady state. Each end of the rod is in thermal
  contact with a <subindex|Heat|reservoir><newterm|heat reservoir> (or
  <subindex|Thermal|reservoir><newterm|thermal
  reservoir>),<inactive|<label|2-heat reservoir>>which is a body or external
  system whose temperature remains constant and uniform when there is heat
  transfer to or from it.<footnote|A heat reservoir can be a body that is so
  large that its temperature changes only imperceptibly during heat transfer,
  or an external system of coexisting phases of a pure substance (e.g., ice
  and water) at constant pressure.> The two heat reservoirs in the figure
  have different temperatures, causing a temperature gradient to form along
  the length of the rod and energy to be transferred by heat from the warmer
  reservoir to the rod and from the rod to the cooler reservoir. Although the
  properties of the steady state of the rod remain constant, the rod is
  clearly not in an equilibrium state because the temperature gradient will
  quickly disappear when we isolate the rod by removing it from contact with
  the heat reservoirs.

  <I|State!system@of a system\|)><I|System!state of\|)>

  <section|Processes and Paths><label|2-processes and paths>

  A <index|Process><newterm|process> is a change in the state of the system
  over time, starting with a definite initial state and ending with a
  definite final state. The process is defined by a
  <index|Path><newterm|path>, which is the continuous sequence of consecutive
  states through which the system passes, including the initial state, the
  intermediate states, and the final state. The process has a direction along
  the path. The path could be described by a curve in an
  <math|N><space|-.18em>-<space|.05em>dimensional space in which each
  coordinate axis represents one of the <math|N> independent variables.

  This book takes the view that a thermodynamic process is defined by what
  happens <em|within> the system, in the three-dimensional region up to and
  including the boundary, and by the forces exerted on the system by the
  surroundings and any external field. Conditions and changes in the
  surroundings are not part of the process except insofar as they affect
  these forces. For example, consider a process in which the system
  temperature decreases from <math|300<K>> to <math|273<K>>. We could
  accomplish this temperature change by placing the system in thermal contact
  with either a refrigerated thermostat bath or a mixture of ice and water.
  The process is the same in both cases, but the surroundings are different.

  <index|Expansion><subindex|Process|expansion><newterm|Expansion> is a
  process in which the system volume increases; in
  <index|Compression><subindex|Process|compression><newterm|compression>, the
  volume decreases.

  An <subindex|Isothermal|process><subindex|Process|isothermal><newterm|isothermal>
  process is one in which the temperature of the system remains uniform and
  constant. An <index|Isobaric process><subindex|Process|isobaric><newterm|isobaric>
  or <subindex|Isopiestic|process><subindex|Process|isopiestic><newterm|isopiestic>
  process refers to uniform constant pressure, and an <index|Isochoric
  process><subindex|Process|isochoric><newterm|isochoric> process refers to
  constant volume. Paths for these processes of an ideal gas are shown in
  Fig. <reference|fig:2-paths><vpageref|fig:2-paths>.

  <\big-figure>
    <boxedfigure|<image|./02-SUP/3-paths.eps||||> <capt|Paths of three
    processes of a closed ideal-gas system with <math|p> and <math|V> as the
    independent variables. (a)<nbsp>Isothermal expansion. (b)<nbsp>Isobaric
    expansion. (c)<nbsp>Isochoric pressure reduction.<label|fig:2-paths>>>
  </big-figure|>

  An <subindex|Adiabatic|process><subindex|Process|adiabatic><newterm|adiabatic>
  process is one in which there is no heat transfer across any portion of the
  boundary. We may ensure that a process is adiabatic either by using an
  adiabatic boundary or, if the boundary is diathermal, by continuously
  adjusting the external temperature to eliminate a temperature gradient at
  the boundary.

  Recall that a state function is a property whose value at each instant
  depends only on the state of the system at that instant. The
  <subindex|State function|change of>finite change of a state function
  <math|X> in a process is written <math|<Del>X>. The notation <math|<Del>X>
  always has the meaning <math|X<rsub|2>-X<rsub|1>>, where <math|X<rsub|1>>
  is the value in the initial state and <math|X<rsub|2>> is the value in the
  final state. Therefore, the value of <math|<Del>X> depends only on the
  values of <math|X<rsub|1>> and <math|X<rsub|2>>. <em|The change of a state
  function during a process depends only on the initial and final states of
  the system, not on the path of the process.>

  An infinitesimal <subindex|State function|change of>change of the state
  function <math|X> is written <math|<dif>X>. The mathematical operation of
  summing an infinite number of infinitesimal changes is integration, and the
  sum is an integral (see the brief calculus review in Appendix
  <reference|app:calc>). The sum of the infinitesimal changes of <math|X>
  along a path is a definite integral equal to <math|<Del>X>:

  <\equation>
    <big|int><rsub|X<rsub|1>><rsup|X<rsub|2>><space|-0.17em><dif>X=X<rsub|2>-X<rsub|1>=\<Delta\>*X
  </equation>

  If <math|<dif>X> obeys this relation\Vthat is, if its integral for given
  limits has the same value regardless of the path\Vit is called an
  <index|Exact differential><subindex|Differential|exact><newterm|exact
  differential>.<label|exact differential>The differential of a state
  function is always an exact differential.

  A <index|Cyclic process><subindex|Process|cyclic><newterm|cyclic process>
  is a process in which the state of the system changes and then returns to
  the initial state. In this case the integral of <math|<dif>X> is written
  with a cyclic integral sign: <math|<big|oint><dif>X>. Since a state
  function <math|X> has the same initial and final values in a cyclic
  process, <math|X<rsub|2>> is equal to <math|X<rsub|1>> and the cyclic
  integral of <math|<dif>X> is zero:

  <\equation>
    <inactive|<label|oint(dX)=0>><big|oint><dif>X=0
  </equation>

  Heat (<math|q>) and work (<math|w>) are examples of quantities that are
  <em|not> state functions. They are not even properties of the system;
  instead they are quantities of energy transferred across the boundary over
  a period of time. It would therefore be incorrect to write
  \P<math|<Del>q>\Q or \P<math|<Del>w>.\Q Instead, the values of <math|q> and
  <math|w> depend in general on the path and are called <index|Path
  function><newterm|path functions>.

  This book uses the symbol <math|<dBar>> (lowercase letter \Pd\Q with a bar
  through the stem) for an infinitesimal quantity of a path function. Thus,
  <math|<dq>> and <math|<dw>> are infinitesimal quantities of heat and work.
  The sum of many infinitesimal quantities of a path function is the <em|net>
  quantity:

  <\equation>
    <big|int><space|-0.17em><dq>=q*<space|2em><big|int><space|-0.17em><dw>=w<inactive|<label|int
    dq=q, intdw=w>>
  </equation>

  The infinitesimal quantities <math|<dq>> and <math|<dw>>, because the
  values of their integrals depend on the path, are <index|Inexact
  differential><subindex|Differential|inexact><newterm|inexact
  differentials>.<footnote|Chemical thermodynamicists often write these
  quantities as <math|<dif>q> and <math|<dif>w>. Mathematicians, however,
  frown on using the same notation for inexact and exact differentials. Other
  notations sometimes used to indicate that heat and work are path functions
  are <math|<tx|D>q> and <math|<tx|D>w>, and also <math|\<delta\>*q> and
  <math|\<delta\>*w>.>

  There is a fundamental difference between a state function (such as
  temperature or volume) and a path function (such as heat or work): <em|The
  value of a state function refers to one instant of time; the value of a
  path function refers to an interval of time>.

  <\minor>
    \ State function and path function in thermodynamics are analogous to
    elevation and distance in climbing a mountain. Suppose there are several
    trails of different lengths from the trailhead to the summit. The climber
    at each instant is at a definite elevation, and the elevation change
    during the climb depends only on the trailhead and summit elevations and
    not on the trail used. Thus elevation is like a state function. The
    distance traveled by the climber depends on the trail, and is like a path
    function.
  </minor>

  <section|The Energy of the System><inactive|<label|2-energy of the system>>

  <I|Energy!system@of the system\|(>

  A large part of classical thermodynamics is concerned with the energy of
  the system. The total energy of a system is an extensive property whose
  value at any one instant cannot be measured in any practical way, but whose
  change is the focus of the first law of thermodynamics (Chap.
  <reference|Chap. 3>).

  <subsection|Energy and reference frames>

  Classical thermodynamics ignores microscopic properties such as the
  behavior of individual atoms and molecules. Nevertheless, a consideration
  of the classical mechanics of particles will help us to understand the
  sources of the potential and kinetic energy of a thermodynamic system.

  In classical mechanics, the energy <index|Energy>of a collection of
  interacting point particles is the sum of the kinetic energy
  <math|<onehalf>m*v<rsup|2>> of each particle (where <math|m> is the
  particle's mass and <math|v> is its velocity), and of various kinds of
  potential energies. The potential energies are defined in such a way that
  if the particles are isolated from the rest of the universe, as the
  particles move and interact with one another the total energy (kinetic plus
  potential) is constant over time. This principle of the conservation of
  energy also holds for real atoms and molecules whose electronic,
  vibrational, and rotational energies, absent in point particles, are
  additional contributions to the total energy.

  The positions and velocities of particles must be measured in a specified
  system of coordinates called a <index|Reference
  frame><subindex|Frame|reference><em|reference frame>. This book will use
  reference frames with <em|Cartesian> axes. Since the kinetic energy of a
  particle is a function of velocity, the kinetic energy depends on the
  choice of the reference frame. A particularly important kind is an
  <index|Inertial reference frame><subsubindex|Frame|reference|inertial><em|inertial>
  frame, one in which Newton's laws of motion are obeyed (see Sec.
  <reference|A-forces between particles> in Appendix <reference|app:forces>).

  A reference frame whose axes are fixed relative to the earth's surface is
  what this book will call a <index|Lab frame><subindex|Frame|lab><em|lab
  frame>. A lab frame for all practical purposes is inertial (Sec.
  <reference|A-earth-fixed frame><vpageref|A-earth-fixed frame>). It is in
  this kind of stationary frame that the laws of thermodynamics have been
  found by experiment to be valid.

  The energy <math|E> of a thermodynamic system is the sum of the energies of
  the particles contained in it and the potential energies of interaction
  between these particles. Just as for an individual particle, the energy of
  the system depends on the reference frame in which it is measured. The
  energy of the system may change during a process, but the principle of the
  conservation of energy ensures that the sum of the energy of the system,
  the energy of the surroundings, and any energy shared by both, all measured
  in the same reference frame, remains constant over time.

  This book uses the symbol <math|E<sys>> for the energy of the system
  measured in a specified inertial frame. The system could be located in a
  weightless environment in outer space, and the inertial frame could be one
  that is either fixed or moving at constant velocity relative to local
  stars. Usually, however, the system is located in the earth's gravitational
  field, and the appropriate inertial frame is then an earth-fixed lab frame.

  If during a process the system as a whole undergoes motion or rotation
  relative to the inertial frame, then <math|E<sys>> depends in part on
  coordinates that are not properties of the system. In such situations
  <math|E<sys>> is not a state function, and we need the concept of internal
  energy.

  <subsection|Internal energy><label|2-internal energy>

  <I|Internal energy\|(><I|Energy!internal\|(>

  The <index|Internal energy><subindex|Energy|internal><newterm|internal
  energy>, <math|U<space|-0.17em>>, is the energy of the system measured in a
  reference frame that allows <math|U> to be a state function\Vthat is, at
  each instant the value of <math|U> depends only on the state of the system.
  This book will call a reference frame with this property a <index|Local
  frame><subindex|Frame|local><em|local frame>. A local frame may also be,
  but is not necessarily, an earth-fixed lab frame.

  Here is a simple illustration of the distinction between the energy
  <math|E<sys>> of a system measured in a lab frame and the internal energy
  <math|U<space|-0.17em>> measured in a local frame. Let the <em|system> be a
  fixed amount of water of uniform temperature <math|T> and pressure <math|p>
  contained in a glass beaker. (The glass material of the beaker is part of
  the surroundings.) The state of this system is defined by the independent
  variables <math|T> and <math|p>. The most convenient local frame in which
  to measure <math|U> in this case is a frame fixed with respect to the
  beaker.

  <\itemize>
    <item>When the beaker is at rest on the lab bench, the local frame is a
    lab frame; then the energies <math|E<sys>> and <math|U> are equal and
    depend only on <math|T> and <math|p>.

    <item>If we place the beaker on a laboratory hot plate and use the hot
    plate to raise the temperature of the water, the values of <math|E<sys>>
    and <math|U> increase equally.

    <item>Suppose we slide the beaker horizontally along the lab bench while
    <math|T> and <math|p> stay constant. While the system is in motion, its
    kinetic energy is greater in the lab frame than in the local frame. Now
    <math|E<sys>> is greater than when the beaker was at rest, although the
    state of the system and the value of <math|U> are unchanged.

    <item>If we slowly lift the beaker above the bench, the potential energy
    of the water in the earth's <subindex|Gravitational|field><subindex|Field|gravitational>gravitational
    field increases, again with no change in <math|T> and <math|p>. The value
    of <math|E<sys>> has increased, but there has been no change in the state
    of the system or the value of <math|U>.
  </itemize>

  Section <reference|3-thermo work> will show that the relation between
  changes of the system energy and the internal energy in this example is
  <math|<Del>E<sys>=<Del>E<subs|k>+<Del>E<subs|p>+<Del>U>, where
  <math|E<subs|k>> and <math|E<subs|p>> are the kinetic and potential
  energies of the system as a whole measured in the lab frame.

  Our choice of the local frame used to define the internal energy <math|U>
  of any particular system during a given process is to some extent
  arbitrary. Three possible choices are as follows.

  <\itemize>
    <item>If the system as a whole does not move or rotate in the laboratory,
    a lab frame is an appropriate local frame. Then <math|U> is the same as
    the system energy <math|E<sys>> measured in the lab frame.

    <item><label|2-cm frame> If the system's center of mass moves in the lab
    frame during the process, we can let the local frame be a
    <index|Center-of-mass frame><subindex|Frame|center-of-mass><em|center-of-mass
    frame> whose origin moves with the center of mass and whose Cartesian
    axes are parallel to the axes of the lab frame.

    <item>If the system consists of the contents of a rigid container that
    moves or rotates in the lab, as in the illustration above, it may be
    convenient to choose a local frame that has its origin and axes fixed
    with respect to the container.
  </itemize>

  Is it possible to determine a numerical <em|value> for the internal energy
  of a system? The total energy of a body of mass <math|m> when it is at rest
  is given by the <index|Einstein energy relation>Einstein relation
  <math|E=m*c<rsup|2>>, where <math|c> is the speed of light in vacuum. In
  principle, then, we could calculate the internal energy <math|U> of a
  system at rest from its mass, and we could determine <math|<Del>U> for a
  process from the change in mass. In practice, however, an absolute value of
  <math|U> calculated from a measured mass has too much uncertainty to be of
  any practical use. For example, the typical uncertainty of the mass of an
  object measured with a microbalance, about
  <math|0.1<units|<math|\<up-mu\>>g>> (Table <reference|tbl:2-methods>),
  would introduce the enormous uncertainty in energy of about
  <math|10<rsup|10>> joules. Only values of the <em|change> <math|<Del>U> are
  useful, and these values cannot be calculated from <math|<Del>m> because
  the change in mass during an ordinary chemical process is much too small to
  be detected. <I|Internal energy\|)><I|Energy!internal\|)>

  <I|Energy!system@of the system\|)>

  <\comment>
    \ No answers:
  </comment>

  <new-page><phantomsection><addcontentsline|toc|section|Problems>
  <paragraphfootnotes><problemsNoAns| <input|02-problems><page-break>>
  <plainfootnotes>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|2-ITS-90|<tuple|Steam point|?>>
    <associate|2-Kibble balance|<tuple|1.3.1|?>>
    <associate|2-amount|<tuple|1.3.2|?>>
    <associate|2-cm frame|<tuple|<with|mode|<quote|math>|\<bullet\>>|?>>
    <associate|2-eq states|<tuple|1.4.4|?>>
    <associate|2-eqn of state|<tuple|1.2.4|?>>
    <associate|2-extensive \ intensive|<tuple|1.1.1|?>>
    <associate|2-fluids|<tuple|1.2.3|?>>
    <associate|2-internal energy|<tuple|1.6.2|?>>
    <associate|2-mass|<tuple|1.3.1|?>>
    <associate|2-molecular weight|<tuple|Molecular weight|?>>
    <associate|2-more about ind variables|<tuple|1.4.3|?>>
    <associate|2-phase coexistence|<tuple|1.2.2|?>>
    <associate|2-physical states|<tuple|1.2.1|?>>
    <associate|2-practical thermometers|<tuple|1.3.6.4|?>>
    <associate|2-pressure|<tuple|1.3.5|?>>
    <associate|2-processes and paths|<tuple|1.5|?>>
    <associate|2-shear stress|<tuple|Shear stress|?>>
    <associate|2-solids|<tuple|1.2.6|?>>
    <associate|2-state fncs \ ind variables|<tuple|1.4.1|?>>
    <associate|2-state of the system|<tuple|1.4|?>>
    <associate|2-system|<tuple|1.1|?>>
    <associate|2-temperature|<tuple|1.3.6|?>>
    <associate|2-virial eqn|<tuple|1.2.5|?>>
    <associate|B=RTB_p|<tuple|1.2.6|?>>
    <associate|Chap. 2|<tuple|1|?>>
    <associate|Pt RT|<tuple|Thermometer|?>>
    <associate|V/n=Vm|<tuple|1.1.2|?>>
    <associate|Vm=M/rho|<tuple|1.3.5|?>>
    <associate|Vm=RT/p+B|<tuple|1.2.8|?>>
    <associate|Z=pV/nRT|<tuple|1.2.9|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|Supersystem|?>>
    <associate|auto-100|<tuple|equation of state|?>>
    <associate|auto-101|<tuple|Ideal gas|?>>
    <associate|auto-102|<tuple|Redlich--Kwong equation|?>>
    <associate|auto-103|<tuple|1.2.5|?>>
    <associate|auto-104|<tuple|Equation of state|?>>
    <associate|auto-105|<tuple|Statistical mechanics|?>>
    <associate|auto-106|<tuple|Virial|?>>
    <associate|auto-107|<tuple|Statistical mechanics|?>>
    <associate|auto-108|<tuple|Compression factor|?>>
    <associate|auto-109|<tuple|compression factor|?>>
    <associate|auto-11|<tuple|System|?>>
    <associate|auto-110|<tuple|Compressibility factor|?>>
    <associate|auto-111|<tuple|1.2.3|?>>
    <associate|auto-112|<tuple|Boyle temperature|?>>
    <associate|auto-113|<tuple|Temperature|?>>
    <associate|auto-114|<tuple|Boyle temperature|?>>
    <associate|auto-115|<tuple|Statistical mechanics|?>>
    <associate|auto-116|<tuple|1.2.6|?>>
    <associate|auto-117|<tuple|Elastic deformation|?>>
    <associate|auto-118|<tuple|Deformation|?>>
    <associate|auto-119|<tuple|Plastic deformation|?>>
    <associate|auto-12|<tuple|open|?>>
    <associate|auto-120|<tuple|Deformation|?>>
    <associate|auto-121|<tuple|1.3|?>>
    <associate|auto-122|<tuple|1.3.1|?>>
    <associate|auto-123|<tuple|Kilogram|?>>
    <associate|auto-124|<tuple|Gravitational|?>>
    <associate|auto-125|<tuple|Field|?>>
    <associate|auto-126|<tuple|Gravitational|?>>
    <associate|auto-127|<tuple|Force|?>>
    <associate|auto-128|<tuple|International prototype|?>>
    <associate|auto-129|<tuple|Kilogram|?>>
    <associate|auto-13|<tuple|System|?>>
    <associate|auto-130|<tuple|SI|?>>
    <associate|auto-131|<tuple|Watt balance|?>>
    <associate|auto-132|<tuple|Kibble balance|?>>
    <associate|auto-133|<tuple|1.3.2|?>>
    <associate|auto-134|<tuple|Amount|?>>
    <associate|auto-135|<tuple|Mole|?>>
    <associate|auto-136|<tuple|Relative atomic mass|?>>
    <associate|auto-137|<tuple|Atomic mass, relative|?>>
    <associate|auto-138|<tuple|Atomic weight|?>>
    <associate|auto-139|<tuple|Relative molecular mass|?>>
    <associate|auto-14|<tuple|closed|?>>
    <associate|auto-140|<tuple|Molecular mass, relative|?>>
    <associate|auto-141|<tuple|Molecular weight|?>>
    <associate|auto-142|<tuple|Molar|?>>
    <associate|auto-143|<tuple|molar mass|?>>
    <associate|auto-144|<tuple|1.3.3|?>>
    <associate|auto-145|<tuple|1.3.1|?>>
    <associate|auto-146|<tuple|Liter|?>>
    <associate|auto-147|<tuple|liter|?>>
    <associate|auto-148|<tuple|Milliliter|?>>
    <associate|auto-149|<tuple|1.3.4|?>>
    <associate|auto-15|<tuple|Diathermal boundary|?>>
    <associate|auto-150|<tuple|Volume|?>>
    <associate|auto-151|<tuple|1.3.4|?>>
    <associate|auto-152|<tuple|1.3.5|?>>
    <associate|auto-153|<tuple|Isotropic fluid|?>>
    <associate|auto-154|<tuple|Fluid|?>>
    <associate|auto-155|<tuple|pascal|?>>
    <associate|auto-156|<tuple|Bar|?>>
    <associate|auto-157|<tuple|bar|?>>
    <associate|auto-158|<tuple|Standard|?>>
    <associate|auto-159|<tuple|Pressure|?>>
    <associate|auto-16|<tuple|Boundary|?>>
    <associate|auto-160|<tuple|standard pressure|?>>
    <associate|auto-161|<tuple|1.3.6|?>>
    <associate|auto-162|<tuple|Temperature scale|?>>
    <associate|auto-163|<tuple|Thermometer|?>>
    <associate|auto-164|<tuple|Zeroth law of thermodynamics|?>>
    <associate|auto-165|<tuple|Maxwell, James Clerk|?>>
    <associate|auto-166|<tuple|1.3.6.1|?>>
    <associate|auto-167|<tuple|Ice point|?>>
    <associate|auto-168|<tuple|Steam point|?>>
    <associate|auto-169|<tuple|Melting point|?>>
    <associate|auto-17|<tuple|diathermal|?>>
    <associate|auto-170|<tuple|Triple|?>>
    <associate|auto-171|<tuple|1.3.2|?>>
    <associate|auto-172|<tuple|Triple|?>>
    <associate|auto-173|<tuple|1.3.6.2|?>>
    <associate|auto-174|<tuple|Ideal-gas temperature|?>>
    <associate|auto-175|<tuple|Thermodynamic|?>>
    <associate|auto-176|<tuple|Temperature|?>>
    <associate|auto-177|<tuple|kelvin|?>>
    <associate|auto-178|<tuple|SI|?>>
    <associate|auto-179|<tuple|Centigrade scale|?>>
    <associate|auto-18|<tuple|Adiabatic|?>>
    <associate|auto-180|<tuple|Celsius|?>>
    <associate|auto-181|<tuple|Celsius|?>>
    <associate|auto-182|<tuple|Ice point|?>>
    <associate|auto-183|<tuple|Steam point|?>>
    <associate|auto-184|<tuple|ITS-90|?>>
    <associate|auto-185|<tuple|1.3.2|?>>
    <associate|auto-186|<tuple|1.3.6.3|?>>
    <associate|auto-187|<tuple|Boltzmann constant|?>>
    <associate|auto-188|<tuple|Gas constant|?>>
    <associate|auto-189|<tuple|Thermometer|?>>
    <associate|auto-19|<tuple|Boundary|?>>
    <associate|auto-190|<tuple|1.3.3|?>>
    <associate|auto-191|<tuple|Acoustic gas thermometry|?>>
    <associate|auto-192|<tuple|Thermometry|?>>
    <associate|auto-193|<tuple|Dielectric constant gas thermometry|?>>
    <associate|auto-194|<tuple|Thermometry|?>>
    <associate|auto-195|<tuple|Johnson noise thermometry|?>>
    <associate|auto-196|<tuple|Thermometry|?>>
    <associate|auto-197|<tuple|Doppler broadening thermometry|?>>
    <associate|auto-198|<tuple|Thermometry|?>>
    <associate|auto-199|<tuple|1.3.6.4|?>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-20|<tuple|adiabatic|?>>
    <associate|auto-200|<tuple|Thermometer|?>>
    <associate|auto-201|<tuple|Thermometer|?>>
    <associate|auto-202|<tuple|Thermometer|?>>
    <associate|auto-203|<tuple|Thermometer|?>>
    <associate|auto-204|<tuple|Thermometer|?>>
    <associate|auto-205|<tuple|Thermometer|?>>
    <associate|auto-206|<tuple|Thermometer|?>>
    <associate|auto-207|<tuple|Thermometer|?>>
    <associate|auto-208|<tuple|Thermometer|?>>
    <associate|auto-209|<tuple|PLTS-2000|?>>
    <associate|auto-21|<tuple|Isolated system|?>>
    <associate|auto-210|<tuple|ITS-90|?>>
    <associate|auto-211|<tuple|1.4|?>>
    <associate|auto-212|<tuple|System|?>>
    <associate|auto-213|<tuple|state|?>>
    <associate|auto-214|<tuple|1.4.1|?>>
    <associate|auto-215|<tuple|State function|?>>
    <associate|auto-216|<tuple|state functions|?>>
    <associate|auto-217|<tuple|Independent variables|?>>
    <associate|auto-218|<tuple|Variables|?>>
    <associate|auto-219|<tuple|independent variables|?>>
    <associate|auto-22|<tuple|System|?>>
    <associate|auto-220|<tuple|Dependent variable|?>>
    <associate|auto-221|<tuple|Variables|?>>
    <associate|auto-222|<tuple|dependent variables|?>>
    <associate|auto-223|<tuple|1.4.2|?>>
    <associate|auto-224|<tuple|1.4.1|?>>
    <associate|auto-225|<tuple|Equation of state|?>>
    <associate|auto-226|<tuple|Variables|?>>
    <associate|auto-227|<tuple|1.4.3|?>>
    <associate|auto-228|<tuple|Components, number of|?>>
    <associate|auto-229|<tuple|Equation of state|?>>
    <associate|auto-23|<tuple|isolated|?>>
    <associate|auto-230|<tuple|1.4.4|?>>
    <associate|auto-231|<tuple|Equilibrium state|?>>
    <associate|auto-232|<tuple|State|?>>
    <associate|auto-233|<tuple|equilibrium state|?>>
    <associate|auto-234|<tuple|Isolated system|?>>
    <associate|auto-235|<tuple|System|?>>
    <associate|auto-236|<tuple|Equilibrium|?>>
    <associate|auto-237|<tuple|Equilibrium|?>>
    <associate|auto-238|<tuple|Equilibrium|?>>
    <associate|auto-239|<tuple|Equilibrium|?>>
    <associate|auto-24|<tuple|Reference frame|?>>
    <associate|auto-240|<tuple|External field|?>>
    <associate|auto-241|<tuple|Field|?>>
    <associate|auto-242|<tuple|Osmotic pressure|?>>
    <associate|auto-243|<tuple|Membrane, semipermeable|?>>
    <associate|auto-244|<tuple|Gravitational|?>>
    <associate|auto-245|<tuple|Field|?>>
    <associate|auto-246|<tuple|Metastable state|?>>
    <associate|auto-247|<tuple|State|?>>
    <associate|auto-248|<tuple|1.4.5|?>>
    <associate|auto-249|<tuple|Steady state|?>>
    <associate|auto-25|<tuple|Frame|?>>
    <associate|auto-250|<tuple|State|?>>
    <associate|auto-251|<tuple|steady state|?>>
    <associate|auto-252|<tuple|1.4.1|?>>
    <associate|auto-253|<tuple|Heat|?>>
    <associate|auto-254|<tuple|heat reservoir|?>>
    <associate|auto-255|<tuple|Thermal|?>>
    <associate|auto-256|<tuple|thermal reservoir|?>>
    <associate|auto-257|<tuple|1.5|?>>
    <associate|auto-258|<tuple|Process|?>>
    <associate|auto-259|<tuple|process|?>>
    <associate|auto-26|<tuple|External field|?>>
    <associate|auto-260|<tuple|Path|?>>
    <associate|auto-261|<tuple|path|?>>
    <associate|auto-262|<tuple|Expansion|?>>
    <associate|auto-263|<tuple|Process|?>>
    <associate|auto-264|<tuple|Expansion|?>>
    <associate|auto-265|<tuple|Compression|?>>
    <associate|auto-266|<tuple|Process|?>>
    <associate|auto-267|<tuple|compression|?>>
    <associate|auto-268|<tuple|Isothermal|?>>
    <associate|auto-269|<tuple|Process|?>>
    <associate|auto-27|<tuple|Field|?>>
    <associate|auto-270|<tuple|isothermal|?>>
    <associate|auto-271|<tuple|Isobaric process|?>>
    <associate|auto-272|<tuple|Process|?>>
    <associate|auto-273|<tuple|isobaric|?>>
    <associate|auto-274|<tuple|Isopiestic|?>>
    <associate|auto-275|<tuple|Process|?>>
    <associate|auto-276|<tuple|isopiestic|?>>
    <associate|auto-277|<tuple|Isochoric process|?>>
    <associate|auto-278|<tuple|Process|?>>
    <associate|auto-279|<tuple|isochoric|?>>
    <associate|auto-28|<tuple|Gravitational|?>>
    <associate|auto-280|<tuple|1.5.1|?>>
    <associate|auto-281|<tuple|Adiabatic|?>>
    <associate|auto-282|<tuple|Process|?>>
    <associate|auto-283|<tuple|adiabatic|?>>
    <associate|auto-284|<tuple|State function|?>>
    <associate|auto-285|<tuple|State function|?>>
    <associate|auto-286|<tuple|Exact differential|?>>
    <associate|auto-287|<tuple|Differential|?>>
    <associate|auto-288|<tuple|exact differential|?>>
    <associate|auto-289|<tuple|Cyclic process|?>>
    <associate|auto-29|<tuple|Field|?>>
    <associate|auto-290|<tuple|Process|?>>
    <associate|auto-291|<tuple|cyclic process|?>>
    <associate|auto-292|<tuple|Path function|?>>
    <associate|auto-293|<tuple|path functions|?>>
    <associate|auto-294|<tuple|Inexact differential|?>>
    <associate|auto-295|<tuple|Differential|?>>
    <associate|auto-296|<tuple|inexact differentials|?>>
    <associate|auto-297|<tuple|1.6|?>>
    <associate|auto-298|<tuple|1.6.1|?>>
    <associate|auto-299|<tuple|Energy|?>>
    <associate|auto-3|<tuple|System|?>>
    <associate|auto-30|<tuple|Body|?>>
    <associate|auto-300|<tuple|Reference frame|?>>
    <associate|auto-301|<tuple|Frame|?>>
    <associate|auto-302|<tuple|Inertial reference frame|?>>
    <associate|auto-303|<tuple|Frame|?>>
    <associate|auto-304|<tuple|Lab frame|?>>
    <associate|auto-305|<tuple|Frame|?>>
    <associate|auto-306|<tuple|1.6.2|?>>
    <associate|auto-307|<tuple|Internal energy|?>>
    <associate|auto-308|<tuple|Energy|?>>
    <associate|auto-309|<tuple|internal energy|?>>
    <associate|auto-31|<tuple|body|?>>
    <associate|auto-310|<tuple|Local frame|?>>
    <associate|auto-311|<tuple|Frame|?>>
    <associate|auto-312|<tuple|Gravitational|?>>
    <associate|auto-313|<tuple|Field|?>>
    <associate|auto-314|<tuple|Center-of-mass frame|?>>
    <associate|auto-315|<tuple|Frame|?>>
    <associate|auto-316|<tuple|Einstein energy relation|?>>
    <associate|auto-32|<tuple|1.1.1|?>>
    <associate|auto-33|<tuple|1.1.1|?>>
    <associate|auto-34|<tuple|Extensive property|?>>
    <associate|auto-35|<tuple|Property|?>>
    <associate|auto-36|<tuple|extensive property|?>>
    <associate|auto-37|<tuple|Intensive property|?>>
    <associate|auto-38|<tuple|Property|?>>
    <associate|auto-39|<tuple|intensive property|?>>
    <associate|auto-4|<tuple|system|?>>
    <associate|auto-40|<tuple|Density|?>>
    <associate|auto-41|<tuple|Concentration|?>>
    <associate|auto-42|<tuple|Specific|?>>
    <associate|auto-43|<tuple|Quantity|?>>
    <associate|auto-44|<tuple|specific quantity|?>>
    <associate|auto-45|<tuple|Specific|?>>
    <associate|auto-46|<tuple|Volume|?>>
    <associate|auto-47|<tuple|Molar|?>>
    <associate|auto-48|<tuple|Quantity|?>>
    <associate|auto-49|<tuple|molar quantity|?>>
    <associate|auto-5|<tuple|Surroundings|?>>
    <associate|auto-50|<tuple|Volume|?>>
    <associate|auto-51|<tuple|1.2|?>>
    <associate|auto-52|<tuple|Phase|?>>
    <associate|auto-53|<tuple|phase|?>>
    <associate|auto-54|<tuple|Homogeneous phase|?>>
    <associate|auto-55|<tuple|Phase|?>>
    <associate|auto-56|<tuple|Interface surface|?>>
    <associate|auto-57|<tuple|interface surface|?>>
    <associate|auto-58|<tuple|Isotropic phase|?>>
    <associate|auto-59|<tuple|Phase|?>>
    <associate|auto-6|<tuple|surroundings|?>>
    <associate|auto-60|<tuple|Anisotropic phase|?>>
    <associate|auto-61|<tuple|Phase|?>>
    <associate|auto-62|<tuple|1.2.1|?>>
    <associate|auto-63|<tuple|Physical state|?>>
    <associate|auto-64|<tuple|State|?>>
    <associate|auto-65|<tuple|Shear stress|?>>
    <associate|auto-66|<tuple|Shear stress|?>>
    <associate|auto-67|<tuple|1.2.1|?>>
    <associate|auto-68|<tuple|Solid|?>>
    <associate|auto-69|<tuple|solid|?>>
    <associate|auto-7|<tuple|Boundary|?>>
    <associate|auto-70|<tuple|Deformation|?>>
    <associate|auto-71|<tuple|Fluid|?>>
    <associate|auto-72|<tuple|fluid|?>>
    <associate|auto-73|<tuple|Viscoelastic solid|?>>
    <associate|auto-74|<tuple|Solid|?>>
    <associate|auto-75|<tuple|1.2.2|?>>
    <associate|auto-76|<tuple|Phase|?>>
    <associate|auto-77|<tuple|coexist|?>>
    <associate|auto-78|<tuple|Phase|?>>
    <associate|auto-79|<tuple|phase transition|?>>
    <associate|auto-8|<tuple|boundary|?>>
    <associate|auto-80|<tuple|Phase|?>>
    <associate|auto-81|<tuple|1.2.3|?>>
    <associate|auto-82|<tuple|Supercritical fluid|?>>
    <associate|auto-83|<tuple|Fluid|?>>
    <associate|auto-84|<tuple|Plasma|?>>
    <associate|auto-85|<tuple|Liquid|?>>
    <associate|auto-86|<tuple|liquid|?>>
    <associate|auto-87|<tuple|Gas|?>>
    <associate|auto-88|<tuple|gas|?>>
    <associate|auto-89|<tuple|1.2.2|?>>
    <associate|auto-9|<tuple|Subsystem|?>>
    <associate|auto-90|<tuple|Coexistence curve|?>>
    <associate|auto-91|<tuple|critical point|?>>
    <associate|auto-92|<tuple|Supercritical fluid|?>>
    <associate|auto-93|<tuple|Fluid|?>>
    <associate|auto-94|<tuple|supercritical fluid|?>>
    <associate|auto-95|<tuple|Vapor|?>>
    <associate|auto-96|<tuple|vapor|?>>
    <associate|auto-97|<tuple|Continuity of states|?>>
    <associate|auto-98|<tuple|Barotropic effect|?>>
    <associate|auto-99|<tuple|1.2.4|?>>
    <associate|exact differential|<tuple|exact differential|?>>
    <associate|fig:2-density|<tuple|1.3.4|?>>
    <associate|fig:2-gas thermometer|<tuple|1.3.3|?>>
    <associate|fig:2-paths|<tuple|1.5.1|?>>
    <associate|fig:2-phases|<tuple|1.2.2|?>>
    <associate|fig:2-shear|<tuple|1.2.1|?>>
    <associate|fig:2-steady state|<tuple|1.4.1|?>>
    <associate|fig:2-tp cell|<tuple|1.3.2|?>>
    <associate|footnote-1.1.1|<tuple|1.1.1|?>>
    <associate|footnote-1.1.2|<tuple|1.1.2|?>>
    <associate|footnote-1.2.1|<tuple|1.2.1|?>>
    <associate|footnote-1.3.1|<tuple|1.3.1|?>>
    <associate|footnote-1.3.10|<tuple|1.3.10|?>>
    <associate|footnote-1.3.11|<tuple|1.3.11|?>>
    <associate|footnote-1.3.12|<tuple|1.3.12|?>>
    <associate|footnote-1.3.13|<tuple|1.3.13|?>>
    <associate|footnote-1.3.2|<tuple|1.3.2|?>>
    <associate|footnote-1.3.3|<tuple|1.3.3|?>>
    <associate|footnote-1.3.4|<tuple|1.3.4|?>>
    <associate|footnote-1.3.5|<tuple|1.3.5|?>>
    <associate|footnote-1.3.6|<tuple|1.3.6|?>>
    <associate|footnote-1.3.7|<tuple|1.3.7|?>>
    <associate|footnote-1.3.8|<tuple|1.3.8|?>>
    <associate|footnote-1.3.9|<tuple|1.3.9|?>>
    <associate|footnote-1.4.1|<tuple|1.4.1|?>>
    <associate|footnote-1.4.2|<tuple|1.4.2|?>>
    <associate|footnote-1.4.3|<tuple|1.4.3|?>>
    <associate|footnote-1.5.1|<tuple|1.5.1|?>>
    <associate|footnr-1.1.1|<tuple|1.1.1|?>>
    <associate|footnr-1.1.2|<tuple|Frame|?>>
    <associate|footnr-1.2.1|<tuple|1.2.1|?>>
    <associate|footnr-1.3.1|<tuple|1.3.1|?>>
    <associate|footnr-1.3.10|<tuple|1.3.10|?>>
    <associate|footnr-1.3.11|<tuple|1.3.11|?>>
    <associate|footnr-1.3.12|<tuple|1.3.12|?>>
    <associate|footnr-1.3.13|<tuple|1.3.13|?>>
    <associate|footnr-1.3.2|<tuple|1.3.2|?>>
    <associate|footnr-1.3.3|<tuple|1.3.3|?>>
    <associate|footnr-1.3.4|<tuple|1.3.4|?>>
    <associate|footnr-1.3.5|<tuple|1.3.5|?>>
    <associate|footnr-1.3.6|<tuple|1.3.6|?>>
    <associate|footnr-1.3.7|<tuple|1.3.7|?>>
    <associate|footnr-1.3.8|<tuple|1.3.8|?>>
    <associate|footnr-1.3.9|<tuple|1.3.9|?>>
    <associate|footnr-1.4.1|<tuple|1.4.1|?>>
    <associate|footnr-1.4.2|<tuple|1.4.2|?>>
    <associate|footnr-1.4.3|<tuple|1.4.3|?>>
    <associate|footnr-1.5.1|<tuple|1.5.1|?>>
    <associate|kinds of internal equilibria|<tuple|System|?>>
    <associate|neg p|<tuple|1.3.5|?>>
    <associate|pVm=RT(1+B/Vm...)|<tuple|1.2.2|?>>
    <associate|pVm=RT(1+B_p RT(1/Vm...)...)|<tuple|1.2.5|?>>
    <associate|pVm=RT(1+B_p p...)|<tuple|1.2.3|?>>
    <associate|rho=m/V|<tuple|1.3.4|?>>
    <associate|steady state|<tuple|1.4.5|?>>
    <associate|subscript m rule|<tuple|molar quantity|?>>
    <associate|tbl:2-ITS-90|<tuple|1.3.2|?>>
    <associate|tbl:2-methods|<tuple|1.3.1|?>>
    <associate|tbl:2-state|<tuple|1.4.1|?>>
    <associate|tbl:2-symbols|<tuple|1.1.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      guggen-85

      chao-15

      stock-19

      haddad-16

      greer-74

      stock-19

      mcglashan-90

      preston-90

      rusby-02

      fellmuth-16

      moldover-14

      underwood-17

      fischer-16

      fellmuth-16
    </associate>
    <\associate|figure>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.1>||>|<pageref|auto-67>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.2>||>|<pageref|auto-89>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.2.3>||>|<pageref|auto-111>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.1>||>|<pageref|auto-151>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.2>||>|<pageref|auto-171>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.3>||>|<pageref|auto-190>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.4.1>||>|<pageref|auto-252>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.5.1>||>|<pageref|auto-280>>
    </associate>
    <\associate|gly>
      <tuple|normal|system|<pageref|auto-4>>

      <tuple|normal|surroundings|<pageref|auto-6>>

      <tuple|normal|boundary|<pageref|auto-8>>

      <tuple|normal|open|<pageref|auto-12>>

      <tuple|normal|closed|<pageref|auto-14>>

      <tuple|normal|diathermal|<pageref|auto-17>>

      <tuple|normal|adiabatic|<pageref|auto-20>>

      <tuple|normal|isolated|<pageref|auto-23>>

      <tuple|normal|body|<pageref|auto-31>>

      <tuple|normal|extensive property|<pageref|auto-36>>

      <tuple|normal|intensive property|<pageref|auto-39>>

      <tuple|normal|specific quantity|<pageref|auto-44>>

      <tuple|normal|molar quantity|<pageref|auto-49>>

      <tuple|normal|phase|<pageref|auto-53>>

      <tuple|normal|interface surface|<pageref|auto-57>>

      <tuple|normal|Shear stress|<pageref|auto-66>>

      <tuple|normal|solid|<pageref|auto-69>>

      <tuple|normal|fluid|<pageref|auto-72>>

      <tuple|normal|coexist|<pageref|auto-77>>

      <tuple|normal|phase transition|<pageref|auto-79>>

      <tuple|normal|liquid|<pageref|auto-86>>

      <tuple|normal|gas|<pageref|auto-88>>

      <tuple|normal|critical point|<pageref|auto-91>>

      <tuple|normal|supercritical fluid|<pageref|auto-94>>

      <tuple|normal|vapor|<pageref|auto-96>>

      <tuple|normal|equation of state|<pageref|auto-100>>

      <tuple|normal|compression factor|<pageref|auto-109>>

      <tuple|normal|Boyle temperature|<pageref|auto-114>>

      <tuple|normal|molar mass|<pageref|auto-143>>

      <tuple|normal|liter|<pageref|auto-147>>

      <tuple|normal|pascal|<pageref|auto-155>>

      <tuple|normal|bar|<pageref|auto-157>>

      <tuple|normal|standard pressure|<pageref|auto-160>>

      <tuple|normal|kelvin|<pageref|auto-177>>

      <tuple|normal|state|<pageref|auto-213>>

      <tuple|normal|state functions|<pageref|auto-216>>

      <tuple|normal|independent variables|<pageref|auto-219>>

      <tuple|normal|dependent variables|<pageref|auto-222>>

      <tuple|normal|equilibrium state|<pageref|auto-233>>

      <tuple|normal|steady state|<pageref|auto-251>>

      <tuple|normal|heat reservoir|<pageref|auto-254>>

      <tuple|normal|thermal reservoir|<pageref|auto-256>>

      <tuple|normal|process|<pageref|auto-259>>

      <tuple|normal|path|<pageref|auto-261>>

      <tuple|normal|Expansion|<pageref|auto-264>>

      <tuple|normal|compression|<pageref|auto-267>>

      <tuple|normal|isothermal|<pageref|auto-270>>

      <tuple|normal|isobaric|<pageref|auto-273>>

      <tuple|normal|isopiestic|<pageref|auto-276>>

      <tuple|normal|isochoric|<pageref|auto-279>>

      <tuple|normal|adiabatic|<pageref|auto-283>>

      <tuple|normal|exact differential|<pageref|auto-288>>

      <tuple|normal|cyclic process|<pageref|auto-291>>

      <tuple|normal|path functions|<pageref|auto-293>>

      <tuple|normal|inexact differentials|<pageref|auto-296>>

      <tuple|normal|internal energy|<pageref|auto-309>>
    </associate>
    <\associate|idx>
      <tuple|<tuple|System>|<pageref|auto-3>>

      <tuple|<tuple|Surroundings>|<pageref|auto-5>>

      <tuple|<tuple|Boundary>|<pageref|auto-7>>

      <tuple|<tuple|Subsystem>|<pageref|auto-9>>

      <tuple|<tuple|Supersystem>|<pageref|auto-10>>

      <tuple|<tuple|System|open>|<pageref|auto-11>>

      <tuple|<tuple|System|closed>|<pageref|auto-13>>

      <tuple|<tuple|Diathermal boundary>|<pageref|auto-15>>

      <tuple|<tuple|Boundary|diathermal>|<pageref|auto-16>>

      <tuple|<tuple|Adiabatic|boundary>|<pageref|auto-18>>

      <tuple|<tuple|Boundary|adiabatic>|<pageref|auto-19>>

      <tuple|<tuple|Isolated system>|<pageref|auto-21>>

      <tuple|<tuple|System|isolated>|<pageref|auto-22>>

      <tuple|<tuple|Reference frame>|<pageref|auto-24>>

      <tuple|<tuple|Frame|reference>|<pageref|auto-25>>

      <tuple|<tuple|External field>|<pageref|auto-26>>

      <tuple|<tuple|Field|external>|<pageref|auto-27>>

      <tuple|<tuple|Gravitational|field>|<pageref|auto-28>>

      <tuple|<tuple|Field|gravitational>|<pageref|auto-29>>

      <tuple|<tuple|Body>|<pageref|auto-30>>

      <tuple|<tuple|Extensive property>|<pageref|auto-34>>

      <tuple|<tuple|Property|extensive>|<pageref|auto-35>>

      <tuple|<tuple|Intensive property>|<pageref|auto-37>>

      <tuple|<tuple|Property|intensive>|<pageref|auto-38>>

      <tuple|<tuple|Density>|<pageref|auto-40>>

      <tuple|<tuple|Concentration>|<pageref|auto-41>>

      <tuple|<tuple|Specific|quantity>|<pageref|auto-42>>

      <tuple|<tuple|Quantity|specific>|<pageref|auto-43>>

      <tuple|<tuple|Specific|volume>|<pageref|auto-45>>

      <tuple|<tuple|Volume|specific>|<pageref|auto-46>>

      <tuple|<tuple|Molar|quantity>|<pageref|auto-47>>

      <tuple|<tuple|Quantity|molar>|<pageref|auto-48>>

      <tuple|<tuple|Volume|molar>|<pageref|auto-50>>

      <tuple|<tuple|Phase>|<pageref|auto-52>>

      <tuple|<tuple|Homogeneous phase>|<pageref|auto-54>>

      <tuple|<tuple|Phase|homogeneous>|<pageref|auto-55>>

      <tuple|<tuple|Interface surface>|<pageref|auto-56>>

      <tuple|<tuple|Isotropic phase>|<pageref|auto-58>>

      <tuple|<tuple|Phase|isotropic>|<pageref|auto-59>>

      <tuple|<tuple|Anisotropic phase>|<pageref|auto-60>>

      <tuple|<tuple|Phase|anisotropic>|<pageref|auto-61>>

      <tuple|<tuple|Physical state>|<pageref|auto-63>>

      <tuple|<tuple|State|physical>|<pageref|auto-64>>

      <tuple|<tuple|Shear stress>|<pageref|auto-65>>

      <tuple|<tuple|Solid>|<pageref|auto-68>>

      <tuple|<tuple|Deformation>|<pageref|auto-70>>

      <tuple|<tuple|Fluid>|<pageref|auto-71>>

      <tuple|<tuple|Viscoelastic solid>|<pageref|auto-73>>

      <tuple|<tuple|Solid|viscoelastic>|<pageref|auto-74>>

      <tuple|<tuple|Phase|coexistence>|<pageref|auto-76>>

      <tuple|<tuple|Phase|transition>|<pageref|auto-78>>

      <tuple|<tuple|Phase|transition|equilibrium>|<pageref|auto-80>>

      <tuple|<tuple|Supercritical fluid>|<pageref|auto-82>>

      <tuple|<tuple|Fluid|supercritical>|<pageref|auto-83>>

      <tuple|<tuple|Plasma>|<pageref|auto-84>>

      <tuple|<tuple|Liquid>|<pageref|auto-85>>

      <tuple|<tuple|Gas>|<pageref|auto-87>>

      <tuple|<tuple|Coexistence curve|liquid--gas>|<pageref|auto-90>>

      <tuple|<tuple|Supercritical fluid>|<pageref|auto-92>>

      <tuple|<tuple|Fluid|supercritical>|<pageref|auto-93>>

      <tuple|<tuple|Vapor>|<pageref|auto-95>>

      <tuple|<tuple|Continuity of states>|<pageref|auto-97>>

      <tuple|<tuple|Barotropic effect>|<pageref|auto-98>>

      <tuple|<tuple|Ideal gas|equation>|<pageref|auto-101>>

      <tuple|<tuple|Redlich--Kwong equation>|<pageref|auto-102>>

      <tuple|<tuple|Equation of state|virial>|<pageref|auto-104>>

      <tuple|<tuple|Statistical mechanics|virial
      equations>|<pageref|auto-105>>

      <tuple|<tuple|Virial|coefficient>|<pageref|auto-106>>

      <tuple|<tuple|Statistical mechanics>|<pageref|auto-107>>

      <tuple|<tuple|Compression factor>|<pageref|auto-108>>

      <tuple|<tuple|Compressibility factor>|<pageref|auto-110>>

      <tuple|<tuple|Boyle temperature>|<pageref|auto-112>>

      <tuple|<tuple|Temperature|Boyle>|<pageref|auto-113>>

      <tuple|<tuple|Statistical mechanics|Boyle
      temperature>|<pageref|auto-115>>

      <tuple|<tuple|Elastic deformation>|<pageref|auto-117>>

      <tuple|<tuple|Deformation|elastic>|<pageref|auto-118>>

      <tuple|<tuple|Plastic deformation>|<pageref|auto-119>>

      <tuple|<tuple|Deformation|plastic>|<pageref|auto-120>>

      <tuple|<tuple|Kilogram>|<pageref|auto-123>>

      <tuple|<tuple|Gravitational|field>|<pageref|auto-124>>

      <tuple|<tuple|Field|gravitational>|<pageref|auto-125>>

      <tuple|<tuple|Gravitational|force>|<pageref|auto-126>>

      <tuple|<tuple|Force|gravitational>|<pageref|auto-127>>

      <tuple|<tuple|International prototype>|<pageref|auto-128>>

      <tuple|<tuple|Kilogram|international prototype>|<pageref|auto-129>>

      <tuple|<tuple|SI|2019 revision>|<pageref|auto-130>>

      <tuple|<tuple|Watt balance>|<pageref|auto-131>>

      <tuple|<tuple|Kibble balance>|<pageref|auto-132>>

      <tuple|<tuple|Amount>|<pageref|auto-134>>

      <tuple|<tuple|Mole>|<pageref|auto-135>>

      <tuple|<tuple|Relative atomic mass>|<pageref|auto-136>>

      <tuple|<tuple|Atomic mass, relative>|<pageref|auto-137>>

      <tuple|<tuple|Atomic weight>|<pageref|auto-138>>

      <tuple|<tuple|Relative molecular mass>|<pageref|auto-139>>

      <tuple|<tuple|Molecular mass, relative>|<pageref|auto-140>>

      <tuple|<tuple|Molecular weight>|<pageref|auto-141>>

      <tuple|<tuple|Molar|mass>|<pageref|auto-142>>

      <tuple|<tuple|Liter>|<pageref|auto-146>>

      <tuple|<tuple|Milliliter>|<pageref|auto-148>>

      <tuple|<tuple|Volume|molar>|<pageref|auto-150>>

      <tuple|<tuple|Isotropic fluid>|<pageref|auto-153>>

      <tuple|<tuple|Fluid|isotropic>|<pageref|auto-154>>

      <tuple|<tuple|Bar>|<pageref|auto-156>>

      <tuple|<tuple|Standard|pressure>|<pageref|auto-158>>

      <tuple|<tuple|Pressure|standard>|<pageref|auto-159>>

      <tuple|<tuple|Temperature scale>|<pageref|auto-162>>

      <tuple|<tuple|Thermometer|liquid-in-glass>|<pageref|auto-163>>

      <tuple|<tuple|Zeroth law of thermodynamics>|<pageref|auto-164>>

      <tuple|<tuple|Maxwell, James Clerk>|<pageref|auto-165>>

      <tuple|<tuple|Ice point|for fixed temperature>|<pageref|auto-167>>

      <tuple|<tuple|Steam point>|<pageref|auto-168>>

      <tuple|<tuple|Melting point|for fixed temperature>|<pageref|auto-169>>

      <tuple|<tuple|Triple|point|for fixed temperature>|<pageref|auto-170>>

      <tuple|<tuple|Triple|point|cell>|<pageref|auto-172>>

      <tuple|<tuple|Ideal-gas temperature>|<pageref|auto-174>>

      <tuple|<tuple|Thermodynamic|temperature>|<pageref|auto-175>>

      <tuple|<tuple|Temperature|thermodynamic>|<pageref|auto-176>>

      <tuple|<tuple|SI|2019 revision>|<pageref|auto-178>>

      <tuple|<tuple|Centigrade scale>|<pageref|auto-179>>

      <tuple|<tuple|Celsius|scale>|<pageref|auto-180>>

      <tuple|<tuple|Celsius|temperature>|<pageref|auto-181>>

      <tuple|<tuple|Ice point>|<pageref|auto-182>>

      <tuple|<tuple|Steam point>|<pageref|auto-183>>

      <tuple|<tuple|ITS-90>|<pageref|auto-184>>

      <tuple|<tuple|Boltzmann constant>|<pageref|auto-187>>

      <tuple|<tuple|Gas constant>|<pageref|auto-188>>

      <tuple|<tuple|Thermometer|constant-volume gas>|<pageref|auto-189>>

      <tuple|<tuple|Acoustic gas thermometry>|<pageref|auto-191>>

      <tuple|<tuple|Thermometry|acoustic gas>|<pageref|auto-192>>

      <tuple|<tuple|Dielectric constant gas thermometry>|<pageref|auto-193>>

      <tuple|<tuple|Thermometry|dielectric constant gas>|<pageref|auto-194>>

      <tuple|<tuple|Johnson noise thermometry>|<pageref|auto-195>>

      <tuple|<tuple|Thermometry|Johnson noise>|<pageref|auto-196>>

      <tuple|<tuple|Doppler broadening thermometry>|<pageref|auto-197>>

      <tuple|<tuple|Thermometry|Doppler broadening>|<pageref|auto-198>>

      <tuple|<tuple|Thermometer|liquid-in-glass>|<pageref|auto-200>>

      <tuple|<tuple|Thermometer|Beckmann>|<pageref|auto-201>>

      <tuple|<tuple|Thermometer|resistance>|<pageref|auto-202>>

      <tuple|<tuple|Thermometer|platinum resistance>|<pageref|auto-203>>

      <tuple|<tuple|Thermometer|thermistor>|<pageref|auto-204>>

      <tuple|<tuple|Thermometer|thermocouple>|<pageref|auto-205>>

      <tuple|<tuple|Thermometer|thermopile>|<pageref|auto-206>>

      <tuple|<tuple|Thermometer|quartz crystal>|<pageref|auto-207>>

      <tuple|<tuple|Thermometer|optical pyrometer>|<pageref|auto-208>>

      <tuple|<tuple|PLTS-2000>|<pageref|auto-209>>

      <tuple|<tuple|ITS-90>|<pageref|auto-210>>

      <tuple|<tuple|System|state of>|<pageref|auto-212>>

      <tuple|<tuple|State function>|<pageref|auto-215>>

      <tuple|<tuple|Independent variables>|<pageref|auto-217>>

      <tuple|<tuple|Variables|independent>|<pageref|auto-218>>

      <tuple|<tuple|Dependent variable>|<pageref|auto-220>>

      <tuple|<tuple|Variables|dependent>|<pageref|auto-221>>

      <tuple|<tuple|Equation of state>|<pageref|auto-225>>

      <tuple|<tuple|Variables|number of independent>|<pageref|auto-226>>

      <tuple|<tuple|Components, number of>|<pageref|auto-228>>

      <tuple|<tuple|Equation of state>|<pageref|auto-229>>

      <tuple|<tuple|Equilibrium state>|<pageref|auto-231>>

      <tuple|<tuple|State|equilibrium>|<pageref|auto-232>>

      <tuple|<tuple|Isolated system>|<pageref|auto-234>>

      <tuple|<tuple|System|isolated>|<pageref|auto-235>>

      <tuple|<tuple|Equilibrium|thermal>|<pageref|auto-236>>

      <tuple|<tuple|Equilibrium|mechanical>|<pageref|auto-237>>

      <tuple|<tuple|Equilibrium|transfer>|<pageref|auto-238>>

      <tuple|<tuple|Equilibrium|reaction>|<pageref|auto-239>>

      <tuple|<tuple|External field>|<pageref|auto-240>>

      <tuple|<tuple|Field|external>|<pageref|auto-241>>

      <tuple|<tuple|Osmotic pressure>|<pageref|auto-242>>

      <tuple|<tuple|Membrane, semipermeable>|<pageref|auto-243>>

      <tuple|<tuple|Gravitational|field>|<pageref|auto-244>>

      <tuple|<tuple|Field|gravitational>|<pageref|auto-245>>

      <tuple|<tuple|Metastable state>|<pageref|auto-246>>

      <tuple|<tuple|State|metastable>|<pageref|auto-247>>

      <tuple|<tuple|Steady state>|<pageref|auto-249>>

      <tuple|<tuple|State|steady>|<pageref|auto-250>>

      <tuple|<tuple|Heat|reservoir>|<pageref|auto-253>>

      <tuple|<tuple|Thermal|reservoir>|<pageref|auto-255>>

      <tuple|<tuple|Process>|<pageref|auto-258>>

      <tuple|<tuple|Path>|<pageref|auto-260>>

      <tuple|<tuple|Expansion>|<pageref|auto-262>>

      <tuple|<tuple|Process|expansion>|<pageref|auto-263>>

      <tuple|<tuple|Compression>|<pageref|auto-265>>

      <tuple|<tuple|Process|compression>|<pageref|auto-266>>

      <tuple|<tuple|Isothermal|process>|<pageref|auto-268>>

      <tuple|<tuple|Process|isothermal>|<pageref|auto-269>>

      <tuple|<tuple|Isobaric process>|<pageref|auto-271>>

      <tuple|<tuple|Process|isobaric>|<pageref|auto-272>>

      <tuple|<tuple|Isopiestic|process>|<pageref|auto-274>>

      <tuple|<tuple|Process|isopiestic>|<pageref|auto-275>>

      <tuple|<tuple|Isochoric process>|<pageref|auto-277>>

      <tuple|<tuple|Process|isochoric>|<pageref|auto-278>>

      <tuple|<tuple|Adiabatic|process>|<pageref|auto-281>>

      <tuple|<tuple|Process|adiabatic>|<pageref|auto-282>>

      <tuple|<tuple|State function|change of>|<pageref|auto-284>>

      <tuple|<tuple|State function|change of>|<pageref|auto-285>>

      <tuple|<tuple|Exact differential>|<pageref|auto-286>>

      <tuple|<tuple|Differential|exact>|<pageref|auto-287>>

      <tuple|<tuple|Cyclic process>|<pageref|auto-289>>

      <tuple|<tuple|Process|cyclic>|<pageref|auto-290>>

      <tuple|<tuple|Path function>|<pageref|auto-292>>

      <tuple|<tuple|Inexact differential>|<pageref|auto-294>>

      <tuple|<tuple|Differential|inexact>|<pageref|auto-295>>

      <tuple|<tuple|Energy>|<pageref|auto-299>>

      <tuple|<tuple|Reference frame>|<pageref|auto-300>>

      <tuple|<tuple|Frame|reference>|<pageref|auto-301>>

      <tuple|<tuple|Inertial reference frame>|<pageref|auto-302>>

      <tuple|<tuple|Frame|reference|inertial>|<pageref|auto-303>>

      <tuple|<tuple|Lab frame>|<pageref|auto-304>>

      <tuple|<tuple|Frame|lab>|<pageref|auto-305>>

      <tuple|<tuple|Internal energy>|<pageref|auto-307>>

      <tuple|<tuple|Energy|internal>|<pageref|auto-308>>

      <tuple|<tuple|Local frame>|<pageref|auto-310>>

      <tuple|<tuple|Frame|local>|<pageref|auto-311>>

      <tuple|<tuple|Gravitational|field>|<pageref|auto-312>>

      <tuple|<tuple|Field|gravitational>|<pageref|auto-313>>

      <tuple|<tuple|Center-of-mass frame>|<pageref|auto-314>>

      <tuple|<tuple|Frame|center-of-mass>|<pageref|auto-315>>

      <tuple|<tuple|Einstein energy relation>|<pageref|auto-316>>
    </associate>
    <\associate|table>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1.1.1>||Symbols and SI
      units for some common properties>|<pageref|auto-33>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.3.1>||Representative
      measurement methods>|<pageref|auto-145>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|1.3.2>|>
        Fixed temperatures of the

        \ International Temperature Scale of 1990
      </surround>|<pageref|auto-185>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|1.4.1>||Values of state
      functions of an aqueous sucrose solution (A = water, B =
      sucrose)>|<pageref|auto-224>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Systems
      and Their Properties> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>The System, Surroundings, and Boundary
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      <with|par-left|<quote|1tab>|1.1.1<space|2spc>Extensive and intensive
      properties <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-32>>

      1.2<space|2spc>Phases and Physical States of Matter
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-51>

      <with|par-left|<quote|1tab>|1.2.1<space|2spc>Physical states of matter
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-62>>

      <with|par-left|<quote|1tab>|1.2.2<space|2spc>Phase coexistence and
      phase transitions <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-75>>

      <with|par-left|<quote|1tab>|1.2.3<space|2spc>Fluids
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-81>>

      <with|par-left|<quote|1tab>|1.2.4<space|2spc>The equation of state of a
      fluid <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-99>>

      <with|par-left|<quote|1tab>|1.2.5<space|2spc>Virial equations of state
      for pure gases <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-103>>

      <with|par-left|<quote|1tab>|1.2.6<space|2spc>Solids
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-116>>

      1.3<space|2spc>Some Basic Properties and Their Measurement
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-121>

      <with|par-left|<quote|1tab>|1.3.1<space|2spc>Mass
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-122>>

      <with|par-left|<quote|1tab>|1.3.2<space|2spc>Amount of substance
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-133>>

      <with|par-left|<quote|1tab>|1.3.3<space|2spc>Volume
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-144>>

      <with|par-left|<quote|1tab>|1.3.4<space|2spc>Density
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-149>>

      <with|par-left|<quote|1tab>|1.3.5<space|2spc>Pressure
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-152>>

      <with|par-left|<quote|1tab>|1.3.6<space|2spc>Temperature
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-161>>

      <with|par-left|<quote|2tab>|1.3.6.1<space|2spc>Equilibrium systems for
      fixed temperatures <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-166>>

      <with|par-left|<quote|2tab>|1.3.6.2<space|2spc>Temperature scales
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-173>>

      <with|par-left|<quote|2tab>|1.3.6.3<space|2spc>Primary thermometry
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-186>>

      <with|par-left|<quote|2tab>|1.3.6.4<space|2spc>Practical thermometers
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-199>>

      1.4<space|2spc>The State of the System
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-211>

      <with|par-left|<quote|1tab>|1.4.1<space|2spc>State functions and
      independent variables <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-214>>

      <with|par-left|<quote|1tab>|1.4.2<space|2spc>An example: state
      functions of a mixture <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-223>>

      <with|par-left|<quote|1tab>|1.4.3<space|2spc>More about independent
      variables <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-227>>

      <with|par-left|<quote|1tab>|1.4.4<space|2spc>Equilibrium states
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-230>>

      <with|par-left|<quote|1tab>|1.4.5<space|2spc>Steady states
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-248>>

      1.5<space|2spc>Processes and Paths <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-257>

      1.6<space|2spc>The Energy of the System
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-297>

      <with|par-left|<quote|1tab>|1.6.1<space|2spc>Energy and reference
      frames <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-298>>

      <with|par-left|<quote|1tab>|1.6.2<space|2spc>Internal energy
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-306>>
    </associate>
  </collection>
</auxiliary>