<TeXmacs|1.99.20>

<style|<tuple|generic|std-latex>>

<\body>
  <label|Chap5><problem><label|prb:5-H(T)>Show that the enthalpy of a fixed
  amount of an ideal gas depends only on the temperature. <soln| The enthalpy
  <math|H> is defined by <math|H<defn>U+p*V>. For a fixed amount of an ideal
  gas, <math|U> depends only on <math|T> (Sec. <reference|3-U of ideal gas>).
  The product <math|p*V> for an ideal gas also depends only on <math|T> since
  it is equal to <math|n*R*T>. Thus, the enthalpy of a fixed amount of an
  ideal gas, like the internal energy, depends only on <math|T>.>

  <problem><label|prb:5-ht cap of id gas>From concepts in this chapter, show
  that the heat capacities <math|C<rsub|V>> and <math|C<rsub|p>> of a fixed
  amount of an ideal gas are functions only of <math|T>. <soln| Since the
  internal energy and enthalpy of a fixed amount of an ideal gas depend only
  on <math|T>, the derivatives <math|<dif>U/<dif>T> and <math|<dif>H/<dif>T>
  must also depend only on <math|T>. These derivatives are equal to
  <math|C<rsub|V>> and <math|C<rsub|p>> in the case of an ideal gas (Eqs.
  <reference|CV=dU/dT (id gas)> and <reference|Cp=dH/dT (id gas)>).>

  <problem>During the reversible expansion of a fixed amount of an ideal gas,
  each increment of heat is given by the expression
  <math|<dq>=C<rsub|V><dif>T+<around|(|n*R*T/V|)><dif>V> (Eq.
  <reference|dq=CVdT+(nRT/V)dV>). <problemparts| <problempart>A necessary and
  sufficient condition for this expression to be an exact differential is
  that the <I|Reciprocity relation\|p>reciprocity relation must be satisfied
  for the independent variables <math|T> and <math|V> (see Appendix
  <reference|app:state>). Apply this test to show that the expression is
  <em|not> an exact differential, and that heat therefore is not a state
  function. <soln| The reciprocity relation is satisfied if
  <math|<pd|C<rsub|V>|V|T>> and <math|<bpd|<around|(|n*R*T/V|)>|T|V>> are
  equal. For an ideal gas, <math|C<rsub|V>> is a function only of <math|T>
  (Prob. 5.<reference|prb:5-ht cap of id gas>). Thus,
  <math|<pd|C<rsub|V>|V|T>> is zero. The other partial derivative is
  <math|<bpd|<around|(|n*R*T/V|)>|T|V>=n*R/V>, which is not zero. Thus, the
  reciprocity relation is not satisfied, the expression is not an exact
  differential, and <math|q> is not a state function.> <problempart>By the
  same method, show that the entropy increment during the reversible
  expansion, given by the expression <math|<dif>S=<dq>/T>, is an exact
  differential, so that entropy is a state function. <soln| The expression
  for the entropy increment is <math|<dq>/T=<around|(|C<rsub|V>/T|)><dif>T+<around|(|n*R/V|)><dif>V>.
  The reciprocity relation is satisfied if
  <math|<bpd|<around|(|C<rsub|V>/T|)>|V|T>> and
  <math|<bpd|<around|(|n*R/V|)>|T|V>> are equal. Since <math|C<rsub|V>/T> is
  a function only of <math|T>, <math|<bpd|<around|(|C<rsub|V>/T|)>|V|T>> is
  zero. <math|<bpd|<around|(|n*R/V|)>|T|V>> is also zero, so the reciprocity
  relation is satisfied, the expression is an exact differential, and
  <math|S> is a state function.>>

  <problem><label|prb:5-fncs from A>

  This problem illustrates how an expression for one of the thermodynamic
  potentials as a function of its natural variables contains the information
  needed to obtain expressions for the other thermodynamic potentials and
  many other state functions.

  From statistical mechanical theory, a simple model for a hypothetical
  \Phard-sphere\Q liquid (spherical molecules of finite size without
  attractive intermolecular forces) gives the following expression for the
  Helmholtz energy with its natural variables <math|T>, <math|V>, and
  <math|n> as the independent variables:

  <\equation*>
    A=-n*R*T*ln <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>-n*R*T+n*a
  </equation*>

  Here <math|a>, <math|b>, and <math|c> are constants. Derive expressions for
  the following state functions of this hypothetical liquid as functions of
  <math|T>, <math|V>, and <math|n>.

  <\problemparts>
    \ <problempartAns>The entropy, <math|S>

    <answer|<math|<D>S=n*R*ln <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>+<around*|(|<frac|5|2>|)>*n*R>>

    <soln| <math|<D>S=-<Pd|A|T|V,n>=n*R*ln
    <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>+n*R<around*|(|<frac|3|2>|)>+n*R>
    <vs> <math|<D><phantom|S>=n*R*ln <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>+<around*|(|<frac|5|2>|)>*n*R>>
    <problempart>The pressure, <math|p> <soln|
    <math|<D>p=-<Pd|A|V|T,n>=<frac|<D>n*R*T<around*|(|<frac|1|n>|)>|<D><around*|(|<frac|V|n>-b|)>>=<frac|n*R*T|V-n*b>>>
    <problempart>The chemical potential, <math|\<mu\>> <soln|
    <math|<D>\<mu\>=<Pd|A|n|T,V>=-R*T*ln <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>-<frac|n*R*T|<D><around*|(|<frac|V|n>-b|)>>*<around*|(|-<frac|V|n<rsup|2>>|)>-R*T+a>
    <vs> <math|<D><phantom|\<mu\>>=-R*T*ln
    <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>+<frac|V*R*T|V-n*b>-R*T+a>>
    <problempart>The internal energy, <math|U> <soln|
    <math|<D>U=A+T*S=-n*R*T+n*a+<around*|(|<frac|5|2>|)>*n*R*T=<around*|(|<frac|3|2>|)>*n*R*T+n*a>>
    <problempart>The enthalpy, <math|H> <soln|
    <math|<D>H=U+p*V=<around*|(|<frac|3|2>|)>*n*R*T+n*a+<around*|(|<frac|n*R*T|V-n*b>|)>*V>>
    <problempart>The Gibbs energy, <math|G> <soln|
    <math|<D>G=U-T*S+p*V=H-T*S> <vs> <math|<D><phantom|G>=<around*|(|<frac|3|2>|)>*n*R*T+n*a+<around*|(|<frac|n*R*T|V-n*b>|)>*V-n*R*T*ln
    <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>-<around*|(|<frac|5|2>|)>*n*R*T>
    <vs> <math|<D><phantom|G>=-n*R*T*ln <around*|[|c*T<rsup|3/2>*<around*|(|<frac|V|n>-b|)>|]>+<frac|n*V*R*T|V-n*b>-n*R*T+n*a>
    <vs> Since <math|\<mu\>> is the molar Gibbs energy, the same result can
    be obtained using <math|G=n*\<mu\>>.> <problempart>The heat capacity at
    constant volume, <math|C<rsub|V>> <soln|
    <math|<D>C<rsub|V>=<Pd|U|T|V,n>=<around*|(|<frac|3|2>|)>*n*R>>
    <problempart>The heat capacity at constant pressure, <math|C<rsub|p>>
    (hint: use the expression for <math|p> to solve for <math|V> as a
    function of <math|T>, <math|p>, and <math|n>; then use <math|H=U+p*V>)
    <soln| <math|<D>V=<frac|n*R*T|p>+n*b*<space|2em>p*V=n*R*T+n*b*p>
    <vs><math|<D>C<rsub|p>=<Pd|H|T|p,n>=<bPd|<around|(|U+p*V|)>|T|p,n>=<around*|(|<frac|3|2>|)>*n*R+n*R=<around*|(|<frac|5|2>|)>*n*R>>
  </problemparts>

  <\big-figure>
    <boxedfigure|<image|./05-SUP/irrevvap.eps||||><capt|<label|fig:5-irrev
    vaporization>>>
  </big-figure|>

  <problem>Figure <reference|fig:5-irrev vaporization><vpageref|fig:5-irrev
  vaporization> depicts a hypothetical liquid in equilibrium with its vapor.
  The liquid and gas are confined in a cylinder by a piston. An electrical
  resistor is immersed in the liquid. The <em|system> is the contents of the
  cylinder to the left of the piston (the liquid, gas, and resistor). The
  initial state of the system is described by

  <\equation*>
    V<rsub|1>=0.2200<units|m<rsup|<math|3>>><space|2em>T<rsub|1>=300.0<K><space|2em>p<rsub|1>=2.50<timesten|5><Pa>
  </equation*>

  A constant current <math|I=0.5000<units|A>> is passed for
  <math|1600<units|s>> through the resistor, which has electric resistance
  <math|R<el>=50.00<units|<math|<upOmega>>>>. The piston moves slowly to the
  right against a constant external pressure equal to the vapor pressure of
  the liquid, <math|2.50<timesten|5><Pa>>, and some of the liquid vaporizes.
  Assume that the process is adiabatic and that <math|T> and <math|p> remain
  uniform and constant. The final state is described by

  <\equation*>
    V<rsub|2>=0.2400<units|m<rsup|<math|3>>><space|2em>T<rsub|2>=300.0<K><space|2em>p<rsub|2>=2.50<timesten|5><Pa>
  </equation*>

  <\problemparts>
    \ <problempartAns>Calculate <math|q>, <math|w>, <math|<Del>U>, and
    <math|<Del>H>.

    <answer|<math|q=0>, <math|w=1.50<timesten|4><units|J>>,
    <math|<Del>U=1.50<timesten|4><units|J>>,
    <math|<Del>H=2.00<timesten|4><units|J>>>

    <soln| <math|q=0> because the process is adiabatic.
    <vs><math|<D>w=-p<Del>V+I<rsup|2>*R<el><Del>t> <vs>
    <math|<D><phantom|w>=-<around|(|2.50<timesten|5><Pa>|)><around|(|0.0200<units|m<rsup|<math|3>>>|)>+<around|(|0.5000<units|A>|)><rsup|2><around|(|50.00<units|<math|<upOmega>>>|)><around|(|1600<units|s>|)>>
    <vs> <math|<D><phantom|w>=1.50<timesten|4><units|J>>
    <vs><math|<Del>U=q+w=1.50<timesten|4><units|J>>
    <vs><math|<D><Del>H=<Del>U+p<Del>V=1.50<timesten|4><units|J>+<around|(|2.50<timesten|5><Pa>|)><around|(|0.0200<units|m<rsup|<math|3>>>|)>>
    <vs> <math|<D><phantom|<Del>H>=2.00<timesten|4><units|J>>>
    <problempart>Is the process reversible? Explain. <soln| The process is
    not reversible, because of the electrical work.> <problempartAns>Devise a
    reversible process that accomplishes the same change of state, and use it
    to calculate <math|<Del>S>.

    <answer|<math|<Del>S=66.7<units|J*<space|0.17em>K<per>>>>

    <soln| The same change of state can be accomplished by omitting the
    electrical work and allowing energy to be reversibly transferred into the
    system by means of heat. The work is then equal to <math|-p<Del>V>. The
    quantity of heat must cause the same increase of internal energy as in
    the original process: <vs><math|<D>q=<Del>U-w=1.50<timesten|4><units|J>+<around|(|2.50<timesten|5><Pa>|)><around|(|0.0200<units|m<rsup|<math|3>>>|)>=2.00<timesten|4><units|J>>
    <vs>Calculate <math|<Del>S> for this reversible process:
    <vs><math|<D><Del>S=<frac|q|T>=<frac|2.00<timesten|4><units|J>|300.0<K>>=66.7<units|J*<space|0.17em>K<per>>>>
    <problempart>Compare <math|q> for the reversible process with
    <math|<Del>H>. Does your result agree with Eq. <reference|delH=q (dp=0)>?
    <soln| The heat for the isobaric process without nonexpansion work is
    equal to the enthalpy change, in agreement with Eq. <reference|delH=q
    (dp=0)>.>
  </problemparts>

  <\big-table>
    <minipagetable|3.8cm|}<label|tbl:5-H2O surf
    ten><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|1|cell-col-span|1>|<cwith|1|1|2|2|cell-col-span|1>|<cwith|1|1|2|2|cell-halign|c>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|6|6|1|-1|cell-bborder|1ln>|<table|<row|<cell|<math|t/<degC>>>|<cell|<math|<g>/10<rsup|-6><units|J*<space|0.17em>c*m<rsup|<math|-2>>>>>>|<row|<cell|15>|<cell|7.350>>|<row|<cell|20>|<cell|7.275>>|<row|<cell|25>|<cell|7.199>>|<row|<cell|30>|<cell|7.120>>|<row|<cell|35>|<cell|7.041>>>>>>
  </big-table|Surface tension of water at
  <math|1<units|b*a*r>><space|.15em><footnote|Ref. <cite|vargaftik-83>.>>

  <problem>Use the data in Table <reference|tbl:5-H2O surf
  ten><vpageref|tbl:5-H2O surf ten> to evaluate <math|<pd|S|A<subs|s>|T,p>>
  at <math|25<units|<degC>>>, which is the rate at which the entropy changes
  with the area of the air\Uwater interface at this temperature. <soln| From
  Eq. <reference|d(gamma)/dT=-dS/dA(s)>: <vs><math|<D><Pd|S|A<subs|s>|T,p>=-<Pd|<g>|T|<space|-0.17em>p>>
  <vs>At <math|25<units|<degC>>>, <math|<pd|<g>|T|p>> is about
  <math|-1.55<timesten|-8><units|J*<space|0.17em>K<per><space|0.17em>c*m<rsup|<math|-2>>>>;
  thus, <math|<pd|S|A<subs|s>|T,p>> is approximately
  <math|1.55<timesten|-8><units|J*<space|0.17em>K<per><space|0.17em>c*m<rsup|<math|-2>>>>.>

  <problem>When an ordinary <I|Rubber, thermodynamics of\|p>rubber band is
  hung from a clamp and stretched with constant downward force <math|F> by a
  weight attached to the bottom end, gentle heating is observed to cause the
  rubber band to contract in length. To keep the length <math|l> of the
  rubber band constant during heating, <math|F> must be increased. The
  stretching work is given by <math|<dw><rprime|'>=F<dif>l>. From this
  information, find the sign of the partial derivative <math|<pd|T|l|S,p>>;
  then predict whether stretching of the rubber band will cause a heating or
  a cooling effect.

  (Hint: make a Legendre transform of <math|U> whose total differential has
  the independent variables needed for the partial derivative, and write a
  reciprocity relation.)

  You can check your prediction experimentally by touching a rubber band to
  the side of your face before and after you rapidly stretch it.

  <\soln>
    \ The total differential of the internal energy is given by
    <math|<dif>U=T<dif>S-p<dif>V+F<dif>l>. <vs>To change the independent
    variables from <math|S>, <math|V>, and <math|l> to <math|S>, <math|p>,
    and <math|l>, subtract the conjugate pair product
    <math|<around|(|-p|)><around|(|V|)>> from <math|U> to make the Legendre
    transform <math|H>: <math|<D><dif>H=<dif><around|(|U+p*V|)>=<dif>U+p<dif>V+V<difp>=<around|(|T<dif>S-p<dif>V+F<dif>l|)>+p<dif>V+V<difp>>
    <vs> <math|<D><phantom|<dif>H>=T<dif>S+V<difp>+F<dif>l> <vs>Reciprocity
    relation: <vs><math|<D><Pd|T|l|S,p>=<Pd|F|S|p,l>> <vs>The partial
    derivative <math|<pd|F|S|p,l>> is positive because <math|S> increases
    when heat reversibly enters the rubber band and raises the temperature,
    and experimentally <math|F> must be increased if <math|l> is to be
    constant as <math|T> increases. Thus, <math|<pd|T|l|S,p>> is positive,
    which means that as the rubber band is stretched reversibly and
    adiabatically (<math|<dif>S=0>), <math|T> will <em|increase>.
  </soln>
</body>

<\initial>
  <\collection>
    <associate|info-flag|detailed>
    <associate|preamble|true>
  </collection>
</initial>