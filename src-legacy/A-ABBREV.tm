<TeXmacs|2.1>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    \;
  </hide-preamble>

  <chapter|Miscellaneous Abbreviations and Symbols><label|app:abbrev>

  <section|Physical States>

  These abbreviations for <subindex|Physical state|symbols
  for><subindex|State|physical><index-complex|<tuple|state|aggregation>|||<tuple|State|of
  aggregation>>physical states (states of aggregation) may be appended in
  parentheses to chemical formulas or used as superscripts to symbols for
  physical quantities. All but \Pmixt\Q are listed in the IUPAC Green Book
  (Ref. <cite|greenbook-3>, p. 54).

  <\padded-center>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|-1|1|-1|cell-halign|l>|<cwith|1|-1|1|-1|cell-lsep|0.50fn>|<cwith|1|-1|1|-1|cell-rsep|0.50fn>|<table|<row|<cell|g>|<cell|gas
    or vapor>>|<row|<cell|l>|<cell|liquid>>|<row|<cell|f>|<cell|fluid (gas or
    liquid)>>|<row|<cell|s>|<cell|solid>>|<row|<cell|cd>|<cell|condensed
    phase (liquid or solid)>>|<row|<cell|cr>|<cell|crystalline>>|<row|<cell|mixt>|<cell|mixture>>|<row|<cell|sln>|<cell|solution>>|<row|<cell|aq>|<cell|aqueous
    solution>>|<row|<cell|aq<math|,\<infty\>>>|<cell|aqueous solution at
    infinite dilution>>>>>
  </padded-center>

  <new-page>

  <section|Subscripts for Chemical Processes><label|app:abbrev-processes>

  <index|Subscripts for chemical processes><subsubindex|Process|chemical|subscript
  for><subindex|Chemical process|subscript for>These abbreviations are used
  as subscripts to the <math|<Del>> symbol. They are listed in the IUPAC
  Green Book (Ref. <cite|greenbook-3>, p. 59\U60).

  The combination <math|\<Delta\><rsub|<text|p>>>, where \Pp\Q is any one of
  the abbreviations below, can be interpreted as an operator:
  <math|\<Delta\><rsub|<text|p>><defn>\<partial\>/\<partial\>*\<xi\><rsub|<text|p>>>
  where <math|\<xi\><rsub|<text|p>>> is the advancement of the given process
  at constant temperature and pressure. For example,
  <math|<Delsub|c>*H=<pd|H|\<xi\><rsub|<text|c>>|T,p>> is the molar
  differential enthalpy of combustion.

  <\padded-center>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|-1|1|-1|cell-halign|l>|<cwith|1|-1|1|-1|cell-lsep|0.50fn>|<cwith|1|-1|1|-1|cell-rsep|0.50fn>|<table|<row|<cell|vap>|<cell|vaporization,
    evaporation (l <math|<ra>> g)>>|<row|<cell|sub>|<cell|sublimation (s
    <math|<ra>> g)>>|<row|<cell|fus>|<cell|melting, fusion (s <math|<ra>>
    l)>>|<row|<cell|trs>|<cell|transition between two
    phases>>|<row|<cell|mix>|<cell|mixing of
    fluids>>|<row|<cell|sol>|<cell|solution of a solute in
    solvent>>|<row|<cell|dil>|<cell|dilution of a
    solution>>|<row|<cell|ads>|<cell|adsorption>>|<row|<cell|dpl>|<cell|displacement>>|<row|<cell|imm>|<cell|immersion>>|<row|<cell|r>|<cell|reaction
    in general>>|<row|<cell|at>|<cell|atomization>>|<row|<cell|c>|<cell|combustion
    reaction>>|<row|<cell|f>|<cell|formation reaction>>>>>
  </padded-center>

  <new-page>

  <section|Superscripts>

  <index|Superscripts>These abbreviations and symbols are used as
  superscripts to symbols for physical quantities. All but <math|<rprime|'>>,
  int, and ref are listed as recommended superscripts in the IUPAC Green Book
  (Ref. <cite|greenbook-3>, p. 60).

  <\padded-center>
    <tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|-1|1|-1|cell-halign|l>|<cwith|1|-1|1|-1|cell-lsep|0.50fn>|<cwith|1|-1|1|-1|cell-rsep|0.50fn>|<table|<row|<cell|<st>>|<cell|standard>>|<row|<cell|<rsup|<math|\<ast\>>>>|<cell|pure
    substance>>|<row|<cell|<math|<rprime|'>>>|<cell|Legendre transform of a
    thermodynamic potential>>|<row|<cell|<math|\<infty\>>>|<cell|infinite
    dilution>>|<row|<cell|id>|<cell|ideal>>|<row|<cell|int>|<cell|integral>>|<row|<cell|<with|font-family|ss|E>>|<cell|excess
    quantity>>|<row|<cell|ref>|<cell|reference state>>>>>
  </padded-center>
</body>

<\initial>
  <\collection>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|app:abbrev|<tuple|1|?>>
    <associate|app:abbrev-processes|<tuple|1.2|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|1.3|?>>
    <associate|auto-11|<tuple|Superscripts|?>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-3|<tuple|Physical state|?>>
    <associate|auto-4|<tuple|State|?>>
    <associate|auto-5|<tuple|<tuple|state|aggregation>|?>>
    <associate|auto-6|<tuple|1.2|?>>
    <associate|auto-7|<tuple|Subscripts for chemical processes|?>>
    <associate|auto-8|<tuple|Process|?>>
    <associate|auto-9|<tuple|Chemical process|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      greenbook-3

      greenbook-3

      greenbook-3
    </associate>
    <\associate|idx>
      <tuple|<tuple|Physical state|symbols for>|<pageref|auto-3>>

      <tuple|<tuple|State|physical>|<pageref|auto-4>>

      <tuple|<tuple|state|aggregation>|||<tuple|State|of
      aggregation>|<pageref|auto-5>>

      <tuple|<tuple|Subscripts for chemical processes>|<pageref|auto-7>>

      <tuple|<tuple|Process|chemical|subscript for>|<pageref|auto-8>>

      <tuple|<tuple|Chemical process|subscript for>|<pageref|auto-9>>

      <tuple|<tuple|Superscripts>|<pageref|auto-11>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Miscellaneous
      Abbreviations and Symbols> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Physical States <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      1.2<space|2spc>Subscripts for Chemical Processes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>

      1.3<space|2spc>Superscripts <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>
    </associate>
  </collection>
</auxiliary>