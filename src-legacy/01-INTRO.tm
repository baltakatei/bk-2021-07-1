<TeXmacs|1.99.19>

<style|<tuple|book|style-bk>>

<\body>
  <\hide-preamble>
    <assign|R|<macro|\<bbb-R\>>>
  </hide-preamble>

  <chapter|Introduction>

  <paragraphfootnotes><label|Chap. 1>

  Thermodynamics is a quantitative subject. It allows us to derive relations
  between the values of numerous physical quantities. Some physical
  quantities, such as mole fraction, are dimensionless; the value of one of
  these quantities is a pure number. Most quantities, however, are not
  dimensionless and their values must include one or more <em|units>. This
  chapter describes the SI system of units, which are the preferred units in
  science applications. The chapter then discusses some useful mathematical
  manipulations of physical quantities using quantity calculus, and certain
  general aspects of dimensional analysis.

  <section|Physical Quantities, Units, and Symbols>

  <subsection|The International System of Units>

  There is international agreement that the units used for physical
  quantities in science and technology should be those of the
  <I|International System of Units\|seeSI>International System of Units, or
  <index|SI>SI (standing for the French <I|Systeme International
  dUnites@Syst�me International d'Unit�s\|seeSI><newterm|Syst�me
  International d'Unit�s>).

  Physical quantities and units are denoted by symbols. This book will, with
  a few exceptions, use symbols recommended in the third edition of what is
  known, from the color of its cover, as the <index|IUPAC Green Book>IUPAC
  Green Book.<footnote|Ref. <cite|greenbook-3>. The references are listed in
  the Bibliography at the back of the book.> This publication is a manual of
  recommended symbols and terminology based on the SI and produced by the
  <I|International Union of Pure and Applied
  Chemistry\|seeIUPAC>International Union of Pure and Applied Chemistry
  <index|IUPAC>(IUPAC). The symbols for physical quantities are listed for
  convenient reference in Appendices <reference|app:sym> and
  <reference|app:abbrev>.

  The SI includes the seven <index|Base units><newterm|base units> listed in
  Table <reference|tbl:1-SI base units><vpageref|tbl:1-SI base units>.

  <\big-table>
    <subindex|Units|SI><subindex|SI|base units>

    <minipagetable|7.2cm|}<label|tbl:1-SI base
    units><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|3|3|cell-col-span|1>|<cwith|1|1|3|3|cell-halign|l>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|8|8|1|-1|cell-bborder|1ln>|<table|<row|<cell|Physical
    quantity>|<cell|SI unit>|<cell|Symbol>>|<row|<cell|time>|<cell|second>|<cell|s>>|<row|<cell|length>|<cell|meter<space|.15em><footnote|or
    metre>>|<cell|m>>|<row|<cell|mass>|<cell|kilogram>|<cell|kg>>|<row|<cell|thermodynamic
    temperature>|<cell|kelvin>|<cell|K>>|<row|<cell|amount of
    substance>|<cell|mole>|<cell|mol>>|<row|<cell|electric
    current>|<cell|ampere>|<cell|A>>|<row|<cell|luminous
    intensity>|<cell|candela>|<cell|cd>>>>>>
  </big-table|SI base units>

  These base units are for seven independent physical quantities that are
  sufficient to describe all other physical quantities. Definitions of the
  base units are given in Appendix <reference|app:SI>. (The candela, the SI
  unit of luminous intensity, is usually not needed in thermodynamics and is
  not used in this book.)

  <subsection|Amount of substance and amount><label|1-amount>

  The physical quantity formally called <I|Amount of
  substance\|seeAmount><index|Amount><newterm|amount of substance> is a
  counting quantity for specified elementary entities. An <index|Elementary
  entity>elementary entity may be an atom, a molecule, an ion, an
  electron, any other particle or specified group of particles. The SI base
  unit for amount of substance is the <index|Mole><newterm|mole>.

  Before 2019, the mole was defined as the amount of substance containing as
  many elementary entities as the number of atoms in exactly <math|12> grams
  of pure carbon-12 nuclide, <rsup|<math|12>>C<@>. This definition was such
  that one mole of H<rsub|<math|2>>O molecules, for example, has a mass of
  18.02 grams, where 18.02 is the relative molecular mass of
  H<rsub|<math|2>>O, and contains 6.022<timesten|23> molecules, where
  6.022<timesten|23><units|mol<per>> is <math|N<subs|A>>, the <index|Avogadro
  constant><em|Avogadro constant> (values given to four significant
  digits). The same statement can be made for any other substance if 18.02 is
  replaced by the appropriate relative atomic mass or molecular mass value
  (Sec. <reference|2-molecular weight>).

  The SI revision of 2019 (Sec. <reference|1-SI revision>) redefines the mole
  as exactly 6.022,140,76<timesten|23> elementary entities. The mass of this
  number of carbon-12 atoms is 12 grams to within <math|5<timesten|-9>>
  gram,<footnote|Ref. <cite|stock-19>, Appendix 2.> so the revision makes a
  negligible change to calculations involving the mole.<label|1-mole
  revision>

  The symbol for amount of substance is <math|n><@>. It is admittedly awkward
  to refer to <math|n>(H<rsub|<math|2>>O) as \Pthe amount of substance of
  water.\Q This book simply shortens \Pamount of substance\Q to
  <index|Amount><newterm|amount>, a usage condoned by the
  IUPAC<@>.<footnote|Ref. <cite|mills-89>. An alternative name suggested for
  <math|n> is <index|Chemical amount>``chemical amount.''> Thus, \Pthe amount
  of water in the system\Q refers not to the mass or volume of water, but to
  the <em|number> of H<rsub|<math|2>>O molecules expressed in a counting unit
  such as the mole.

  <subsection|The SI revision of 2019><label|1-SI revision>

  <I|SI!2019 revision\|(>

  At a General Conference on Weights and Measures held in Versailles, France
  in November 2018, metrologists from over fifty countries agreed on a major
  revision of the International System of Units. The revision became official
  on 20 May 2019. It redefines the base units for mass, thermodynamic
  temperature, amount of substance, and electric current.

  The SI revision bases the definitions of the base units (Appendix
  <reference|app:SI>) on a set of six <index|Defining constants>defining
  constants with values (listed in Appendix <reference|app:const>) treated as
  exact, with no uncertainty.

  Previously, the kilogram had been defined as the mass of a physical
  artifact, the <index|International prototype><subindex|Kilogram|international
  prototype>international prototype of the kilogram.<label|international
  prototype>The international prototype is a platinum-iridium cylinder
  manufactured in 1879 in England and stored since 1889 in a vault of the
  International Bureau of Weights and Measures in S�vres, near Paris, France.
  As it is subject to surface contamination and other slow changes of mass,
  it is not entirely suitable as a standard.

  The 2019 SI revision instead defines the kilogram in terms of the <index|Planck
  constant>Planck constant <math|h>.<footnote|The manner in which this
  is done using a Kibble balance is described on page <pageref|2-Kibble
  balance>.> As a defining constant, the value of <math|h> was chosen to
  agree with the mass of the international prototype with an uncertainty of
  only several parts in <math|10<rsup|8>>. Thus, as a practical matter, the
  SI revision has a negligible effect on the value of a mass.

  The SI revision defines the kelvin in terms of the <index|Boltzmann
  constant>Boltzmann constant <math|k>, the mole in terms of the
  <index|Avogadro constant>Avogadro constant <math|N<subs|A>>, and the
  ampere in terms of the <index|Elementary charge>elementary charge
  <math|e>. The values of these defining constants were chosen to closely
  agree with the previous base unit definitions. Consequently, the SI
  revision has a negligible effect on values of thermodynamic temperature,
  amount of substance, and electric current. <I|SI!2019 revision\|)>

  <subsection|Derived units and prefixes>

  Table <reference|tbl:1-derived units> lists derived units for some
  additional physical quantities used in thermodynamics. The derived units
  have exact definitions in terms of SI base units, as given in the last
  column of the table.

  <\big-table>
    <subindex|Units|SI derived><subindex|SI|derived units>

    <minipagetable|12.4 cm|<label|tbl:1-derived
    units><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|10|10|1|-1|cell-bborder|1ln>|<table|<row|<cell|Physical
    quantity>|<cell|Unit>|<cell|Symbol>|<cell|Definition of
    unit>>|<row|<cell|force>|<cell|newton>|<cell|N>|<cell|<math|1<units|N>=1<units|m*<space|0.17em>k*g*<space|0.17em>s<rsup|<math|-2>>>>>>|<row|<cell|pressure>|<cell|pascal>|<cell|Pa>|<cell|<math|1<Pa>=1<units|N*<space|0.17em>m<rsup|<math|-2>>>=1<units|k*g*<space|0.17em>m<per><space|0.17em>s<rsup|<math|-2>>>>>>|<row|<cell|Celsius
    temperature>|<cell|degree Celsius>|<cell|<math|<degC>>>|<cell|<math|t/<degC>=T/<tx|K>-273.15>>>|<row|<cell|energy>|<cell|joule>|<cell|J>|<cell|<math|1<units|J>=1<units|N*<space|0.17em>m>=1<units|m<rsup|<math|2>>*<space|0.17em>kg<space|0.17em>s<rsup|<math|-2>>>>>>|<row|<cell|power>|<cell|watt>|<cell|W>|<cell|<math|1<units|W>=1<units|J*<space|0.17em>s<per>>=1<units|m<rsup|<math|2>>*<space|0.17em>kg<space|0.17em>s<rsup|<math|-3>>>>>>|<row|<cell|frequency>|<cell|hertz>|<cell|Hz>|<cell|<math|1<units|H*z>=1<units|s<per>>>>>|<row|<cell|electric
    charge>|<cell|coulomb>|<cell|C>|<cell|<math|1<units|C>=1<units|A*<space|0.17em>s>>>>|<row|<cell|electric
    potential>|<cell|volt>|<cell|V>|<cell|<math|1<units|V>=1<units|J*<space|0.17em>C<per>>=1<units|m<rsup|<math|2>>*<space|0.17em>kg<space|0.17em>s<rsup|<math|-3>>*<space|0.17em>A<per>>>>>|<row|<cell|electric
    resistance>|<cell|ohm>|<cell|<math|<upOmega>>>|<cell|<math|1<space|0.17em><upOmega>=1<units|V*<space|0.17em>A<per>>=1<units|m<rsup|<math|2>>*<space|0.17em>kg<space|0.17em>s<rsup|<math|-3>>*<space|0.17em>A<rsup|<math|-2>>>>>>>>>>
  </big-table|SI derived units>

  The units listed in Table <reference|tbl:1-non-SI units> are sometimes used
  in thermodynamics but are not part of the SI<@>. They do, however, have
  exact definitions in terms of SI units and so offer no problems of
  numerical conversion to or from SI units.

  <\big-table>
    <subindex|Units|Non-SI><paragraphfootnotes>

    <minipagetable|12.5cm|}<label|tbl:1-non-SI
    units><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|6|6|1|-1|cell-bborder|1ln>|<table|<row|<cell|Physical
    quantity>|<cell|Unit>|<cell|Symbol>|<cell|Definition of
    unit>>|<row|<cell|volume>|<cell|liter<space|.15em><footnote|or
    litre>>|<cell|L<space|.15em><footnote|or
    l>>|<cell|<math|1<units|L>=1<units|d*m<rsup|<math|3>>>=10<rsup|-3><units|m<rsup|<math|3>>>>>>|<row|<cell|pressure>|<cell|bar>|<cell|bar>|<cell|<math|1<br>=10<rsup|5><Pa>>>>|<row|<cell|pressure>|<cell|atmosphere>|<cell|atm>|<cell|<math|1<units|a*t*m>=<tx|101,325><Pa>=1.01325<br>>>>|<row|<cell|pressure>|<cell|torr>|<cell|Torr>|<cell|<math|1<units|T*o*r*r>=<around|(|1/760|)><units|a*t*m>=<around|(|<tx|101,325/760>|)><Pa>>>>|<row|<cell|energy>|<cell|calorie<space|.15em><footnote|or
    thermochemical calorie>>|<cell|cal<space|.15em><footnote|or
    cal<math|<subs|t*h>>>>|<cell|<math|1<units|c*a*l>=4.184<units|J>>>>>>>>
  </big-table|Non-SI derived units>

  Any of the symbols for units listed in Tables <reference|tbl:1-SI base
  units>\U<reference|tbl:1-non-SI units>, except kg and <math|<degC>>, may be
  preceded by one of the prefix symbols of Table <reference|tbl:1-SI
  prefixes><vpageref|tbl:1-SI prefixes>

  <\big-table>
    <minipagetable|9.5cm|<label|tbl:1-SI prefixes><tabular*|<tformat|<cwith|1|-1|1|-1|cell-valign|c>|<cwith|1|1|1|-1|cell-tborder|1ln>|<cwith|1|1|1|-1|cell-bborder|1ln>|<cwith|11|11|1|-1|cell-valign|top>|<cwith|11|11|1|-1|cell-vmode|exact>|<cwith|11|11|1|-1|cell-height|<plus|1fn|0.5ex>>|<cwith|11|11|1|-1|cell-bborder|1ln>|<table|<row|<cell|<index|Prefixes><subindex|SI|prefixes>Fraction>|<cell|Prefix>|<cell|Symbol
    <math|<space|2em>>>|<cell|Multiple>|<cell|Prefix>|<cell|Symbol>>|<row|<cell|<math|10<rsup|-1>>>|<cell|deci>|<cell|d>|<cell|<math|10>>|<cell|deka>|<cell|da>>|<row|<cell|<math|10<rsup|-2>>>|<cell|centi>|<cell|c>|<cell|<math|10<rsup|2>>>|<cell|hecto>|<cell|h>>|<row|<cell|<math|10<rsup|-3>>>|<cell|milli>|<cell|m>|<cell|<math|10<rsup|3>>>|<cell|kilo>|<cell|k>>|<row|<cell|<math|10<rsup|-6>>>|<cell|micro>|<cell|<math|\<up-mu\>>>|<cell|<math|10<rsup|6>>>|<cell|mega>|<cell|M>>|<row|<cell|<math|10<rsup|-9>>>|<cell|nano>|<cell|n>|<cell|<math|10<rsup|9>>>|<cell|giga>|<cell|G>>|<row|<cell|<math|10<rsup|-12>>>|<cell|pico>|<cell|p>|<cell|<math|10<rsup|12>>>|<cell|tera>|<cell|T>>|<row|<cell|<math|10<rsup|-15>>>|<cell|femto>|<cell|f>|<cell|<math|10<rsup|15>>>|<cell|peta>|<cell|P>>|<row|<cell|<math|10<rsup|-18>>>|<cell|atto>|<cell|a>|<cell|<math|10<rsup|18>>>|<cell|exa>|<cell|E>>|<row|<cell|<math|10<rsup|-21>>>|<cell|zepto>|<cell|z>|<cell|<math|10<rsup|21>>>|<cell|zetta>|<cell|Z>>|<row|<cell|<math|10<rsup|-24>>>|<cell|yocto>|<cell|y>|<cell|<math|10<rsup|24>>>|<cell|yotta>|<cell|Y>>|<row|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>|<cell|>>>>>>
  </big-table|SI prefixes>

  to construct a decimal fraction or multiple of the unit. (The symbol g may
  be preceded by a prefix symbol to construct a fraction or multiple of the
  gram.) The combination of prefix symbol and unit symbol is taken as a new
  symbol that can be raised to a power without using parentheses, as in the
  following examples:

  <shortpage>

  <\plainlist>
    <item><space|.6cm><math|1<units|m*g>=1<timesten|-3><units|g>>

    <item><space|.6cm><math|1<units|c*m>=1<timesten|-2><units|m>>

    <item><space|.6cm><math|1<units|c*m<rsup|<math|3>>>=<around|(|1<timesten|-2><units|m>|)><rsup|3>=1<timesten|-6><units|m<rsup|<math|3>>>>
  </plainlist>

  <section|Quantity Calculus><label|1-quantity calculus>

  This section gives examples of how we may manipulate physical quantities by
  the rules of algebra. The method is called <index|Quantity
  calculus><em|quantity calculus>, although a better term might be
  \Pquantity algebra.\Q

  Quantity calculus is based on the concept that a physical quantity, unless
  it is dimensionless, has a value equal to the product of a <em|numerical
  value> (a pure number) and one or more <index|Units><em|units>:

  <\equation>
    <tx|p*h*y*s*i*c*a*l*q*u*a*n*t*i*t*y=n*u*m*e*r*i*c*a*l*v*a*l*u*e<math|\<times\>>units>
  </equation>

  (If the quantity is dimensionless, it is equal to a pure number without
  units.) The physical property may be denoted by a symbol, but the symbol
  does <em|not> imply a particular choice of units. For instance, this book
  uses the symbol <math|\<rho\>> for density, but <math|\<rho\>> can be
  expressed in any units having the dimensions of mass divided by volume.

  A simple example illustrates the use of quantity calculus. We may express
  the density of water at <math|25<units|<degC>>> to four significant digits
  in SI base units by the equation

  <\equation>
    \<rho\>=9.970<timesten|2><units|k*g*<space|0.17em>m<rsup|<math|-3>>>
  </equation>

  and in different density units by the equation

  <\equation>
    \<rho\>=0.9970<units|g*<space|0.17em>c*m<rsup|<math|-3>>>
  </equation>

  We may divide both sides of the last equation by
  <math|1<units|g*<space|0.17em>c*m<rsup|<math|-3>>>> to obtain a new
  equation

  <\equation>
    \<rho\>/<tx|g*<space|0.17em>c*m<rsup|<math|-3>>>=0.9970
  </equation>

  Now the pure number <math|0.9970> appearing in this equation is the number
  of grams in one cubic centimeter of water, so we may call the ratio
  <math|\<rho\>/>g<space|0.17em>cm<rsup|<math|-3>> \Pthe number of grams per
  cubic centimeter.\Q By the same reasoning,
  <math|\<rho\>/>kg<space|0.17em>m<rsup|<math|-3>> is the number of kilograms
  per cubic meter. In general, a physical quantity divided by particular
  units for the physical quantity is a pure number representing the number of
  those units.

  <\minor>
    \ Just as it would be incorrect to call <math|\<rho\>> ``the number of
    grams per cubic centimeter,'' because that would refer to a particular
    choice of units for <math|\<rho\>>, the common practice of calling
    <math|n> ``the number of moles'' is also strictly speaking not correct.
    It is actually the ratio <math|n/<tx|m*o*l>> that is the number of moles.
  </minor>

  In a table, the ratio <math|\<rho\>/>g<space|0.17em>cm<rsup|<math|-3>>
  makes a convenient heading for a column of density values because the
  column can then show pure numbers. Likewise, it is convenient to use
  <math|\<rho\>/>g<space|0.17em>cm<rsup|<math|-3>> as the label of a graph
  axis and to show pure numbers at the grid marks of the axis. You will see
  many examples of this usage in the tables and figures of this book.

  A major advantage of using SI base units and SI derived units is that they
  are <em|coherent>. That is, values of a physical quantity expressed in
  different combinations of these units have the same numerical value.

  For example, suppose we wish to evaluate the pressure of a gas according to
  the <subindex|Ideal gas|equation>ideal gas equation<footnote|This is the
  first equation in this book that, like many others to follow, shows
  <index|Conditions of validity><em|conditions of validity> in parentheses
  immediately below the equation number at the right. Thus, Eq.
  <reference|p=nRT/V> is valid for an ideal gas.>

  <\gather>
    <tformat|<table|<row|<cell|<s|p=<frac|n*R*T|V>><cond|<around|(|i*d*e*a*l*g*a*s|)>><eq-number><label|p=nRT/V>>>>>
  </gather>

  In this equation, <math|p>, <math|n>, <math|T>, and <math|V> are the
  symbols for the physical quantities pressure, amount (amount of substance),
  thermodynamic temperature, and volume, respectively, and <math|R> is the
  gas constant.

  The calculation of <math|p> for <math|5.000> moles of an ideal gas at a
  temperature of <math|298.15> kelvins, in a volume of <math|4.000> cubic
  meters, is

  <\equation>
    <label|p=./.>p=<frac|<around|(|5.000<mol>|)><around|(|<R>|)><around|(|298.15<K>|)>|4.000<units|m<rsup|<math|3>>>>=3.099<timesten|3><units|J*<space|0.17em>m<rsup|<math|-3>>>
  </equation>

  The mole and kelvin units cancel, and we are left with units of
  J<space|0.17em>m<rsup|<math|-3>>, a combination of an SI derived unit (the
  joule) and an SI base unit (the meter). The units
  J<space|0.17em>m<rsup|<math|-3>> must have dimensions of pressure, but are
  not commonly used to express pressure.

  To convert J<space|0.17em>m<rsup|<math|-3>> to the SI derived unit of
  pressure, the pascal (Pa), we can use the following relations from Table
  <reference|tbl:1-derived units>:

  <\equation>
    1<units|J>=1<units|N*<space|0.17em>m><space|2em>1<Pa>=1<units|N*<space|0.17em>m<rsup|<math|-2>>>
  </equation>

  When we divide both sides of the first relation by <math|1<units|J>> and
  divide both sides of the second relation by
  <math|1<units|N*<space|0.17em>m<rsup|<math|-2>>>>, we obtain the two new
  relations

  <\equation>
    1=<around|(|1<units|N*<space|0.17em>m>/<tx|J>|)>*<space|2em><around|(|1<units|P*a>/<tx|N*<space|0.17em>m<rsup|<math|-2>>>|)>=1
  </equation>

  The ratios in parentheses are <index|Conversion factor><em|conversion
  factors>. When a physical quantity is multiplied by a conversion factor
  that, like these, is equal to the pure number <math|1>, the physical
  quantity changes its units but not its value. When we multiply Eq.
  <reference|p=./.> by both of these conversion factors, all units cancel
  except Pa:

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|p>|<cell|=<around|(|3.099<timesten|3><units|J*<space|0.17em>m<rsup|<math|-3>>>|)>\<times\><around|(|1<units|N*<space|0.17em>m>/<tx|J>|)>\<times\><around|(|1<Pa>/<tx|N*<space|0.17em>m<rsup|<math|-2>>>|)>>>|<row|<cell|>|<cell|=3.099<timesten|3><Pa>>>>>
    </eqsplit>
  </equation>

  This example illustrates the fact that to calculate a physical quantity, we
  can simply enter into a calculator numerical values expressed in SI units,
  and the result is the numerical value of the calculated quantity expressed
  in SI units. In other words, as long as we use only SI base units and SI
  derived units (without prefixes), <em|all conversion factors are unity>.

  Of course we do not have to limit the calculation to SI units. Suppose we
  wish to express the calculated pressure in torrs, a non-SI unit. In this
  case, using a conversion factor obtained from the definition of the torr in
  Table <reference|tbl:1-non-SI units>, the calculation becomes

  <\equation>
    <\eqsplit>
      <tformat|<table|<row|<cell|p>|<cell|=<around|(|3.099<timesten|3><Pa>|)>\<times\><around|(|760<units|T*o*r*r>/101,325<Pa>|)>>>|<row|<cell|>|<cell|=23.24<units|T*o*r*r>>>>>
    </eqsplit>
  </equation>

  <section|Dimensional Analysis><label|1-dimensional analysis>

  <I|Dimensional analysis\|(>Sometimes you can catch an error in the form of
  an equation or expression, or in the dimensions of a quantity used for a
  calculation, by checking for dimensional consistency. Here are some rules
  that must be satisfied:

  <\itemize>
    <item>both sides of an equation have the same dimensions

    <item>all terms of a sum or difference have the same dimensions

    <item>logarithms and exponentials, and arguments of logarithms and
    exponentials, are dimensionless

    <item>a quantity used as a power is dimensionless
  </itemize>

  In this book the <index|Differential><em|differential> of a function, such
  as <math|<df>>, refers to an <em|infinitesimal> quantity. If one side of an
  equation is an infinitesimal quantity, the other side must also be. Thus,
  the equation <math|<df>=a<dx>+b<dif>y> (where <math|a*x> and <math|b*y>
  have the same dimensions as <math|f>) makes mathematical sense, but
  <math|<df>=a*x+b<dif>y> does not.

  Derivatives, partial derivatives, and integrals have dimensions that we
  must take into account when determining the overall dimensions of an
  expression that includes them. For instance:

  <\itemize>
    <item>the derivative <math|<difp>/<dif>T> and the partial derivative
    <math|<pd|p|T|V>> have the same dimensions as <math|p/T>

    <item>the partial second derivative <math|<around|(|\<partial\><rsup|2>*p/\<partial\>*T<rsup|2>|)><rsub|V>>
    has the same dimensions as <math|p/T<rsup|2>>

    <item>the integral <math|<big|int><space|-0.17em>T<dif>T> has the same
    dimensions as <math|T<rsup|2>>
  </itemize>

  Some examples of applying these principles are given here using symbols
  described in Sec. <reference|1-quantity calculus>.

  <with|font-series|bold|Example 1.> Since the gas constant <math|R> may be
  expressed in units of J<space|0.17em>K<per><space|0.17em>mol<per>, it has
  dimensions of energy divided by thermodynamic temperature and amount. Thus,
  <math|R*T> has dimensions of energy divided by amount, and <math|n*R*T> has
  dimensions of energy. The products <math|R*T> and <math|n*R*T> appear
  frequently in thermodynamic expressions.

  <with|font-series|bold|Example 2.> What are the dimensions of the quantity
  <math|n*R*T*ln <around|(|p/p<st>|)>> and of <math|p<st>> in this
  expression? The quantity has the same dimensions as <math|n*R*T> (or
  energy) because the logarithm is dimensionless. Furthermore, <math|p<st>>
  in this expression has dimensions of pressure in order to make the argument
  of the logarithm, <math|p/p<st>>, dimensionless.

  <with|font-series|bold|Example 3.> Find the dimensions of the constants
  <math|a> and <math|b> in the van der Waals equation

  <\equation*>
    p=<frac|n*R*T|V-n*b>-<frac|n<rsup|2>*a|V<rsup|2>>
  </equation*>

  Dimensional analysis tells us that, because <math|n*b> is subtracted from
  <math|V>, <math|n*b> has dimensions of volume and therefore <math|b> has
  dimensions of volume/amount. Furthermore, since the right side of the
  equation is a difference of two terms, these terms have the same dimensions
  as the left side, which is pressure. Therefore, the second term
  <math|n<rsup|2>*a/V<rsup|2>> has dimensions of pressure, and <math|a> has
  dimensions of pressure<math|<space|0.17em>\<times\><space|0.17em>>volume<math|<rsup|2><space|0.17em>\<times\><space|0.17em>>amount<rsup|<math|-2>>.

  <with|font-series|bold|Example 4.> Consider an equation of the form

  <\equation*>
    <Pd|ln x|T|<space|-0.17em>p>=<frac|y|R>
  </equation*>

  What are the SI units of <math|y>? <math|ln x> is dimensionless, so the
  left side of the equation has the dimensions of <math|1/T>, and its SI
  units are K<per>. The SI units of the right side are therefore also
  K<per><@>. Since <math|R> has the units
  J<space|0.17em>K<per><space|0.17em>mol<per>, the SI units of <math|y> are
  J<space|0.17em>K<rsup|<math|-2>><space|0.17em>mol<per>. <I|Dimensional
  analysis\|)>

  <\comment>
    \ One problem, no answers:
  </comment>

  <new-page><phantomsection><addcontentsline|toc|section|Problem>
  <paragraphfootnotes><oneproblem| <input|01-problems><page-break>>
  <plainfootnotes>
</body>

<initial|<\collection>
</collection>>

<\references>
  <\collection>
    <associate|1-SI revision|<tuple|1.3|?>>
    <associate|1-amount|<tuple|1.2|?>>
    <associate|1-dimensional analysis|<tuple|3|?>>
    <associate|1-mole revision|<tuple|3|?>>
    <associate|1-quantity calculus|<tuple|2|?>>
    <associate|Chap. 1|<tuple|1|?>>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-10|<tuple|4|?>>
    <associate|auto-11|<tuple|2|?>>
    <associate|auto-12|<tuple|3|?>>
    <associate|auto-2|<tuple|1|?>>
    <associate|auto-3|<tuple|1.1|?>>
    <associate|auto-4|<tuple|2|?>>
    <associate|auto-5|<tuple|1.2|?>>
    <associate|auto-6|<tuple|1.3|?>>
    <associate|auto-7|<tuple|1.4|?>>
    <associate|auto-8|<tuple|2|?>>
    <associate|auto-9|<tuple|9|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-10|<tuple|10|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnote-5|<tuple|5|?>>
    <associate|footnote-6|<tuple|6|?>>
    <associate|footnote-7|<tuple|7|?>>
    <associate|footnote-8|<tuple|8|?>>
    <associate|footnote-9|<tuple|9|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-10|<tuple|10|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
    <associate|footnr-5|<tuple|5|?>>
    <associate|footnr-6|<tuple|6|?>>
    <associate|footnr-7|<tuple|7|?>>
    <associate|footnr-8|<tuple|8|?>>
    <associate|footnr-9|<tuple|9|?>>
    <associate|international prototype|<tuple|1.3|?>>
    <associate|p=./.|<tuple|6|?>>
    <associate|p=nRT/V|<tuple|5|?>>
    <associate|tbl:1-SI base units|<tuple|1|?>>
    <associate|tbl:1-SI prefixes|<tuple|4|?>>
    <associate|tbl:1-derived units|<tuple|2|?>>
    <associate|tbl:1-non-SI units|<tuple|3|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|bib>
      greenbook-3

      stock-19

      mills-89
    </associate>
    <\associate|table>
      <tuple|normal|<surround|<hidden-binding|<tuple>|1>||SI base
      units>|<pageref|auto-4>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|2>||SI derived
      units>|<pageref|auto-8>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|3>||Non-SI derived
      units>|<pageref|auto-9>>

      <tuple|normal|<surround|<hidden-binding|<tuple>|4>||SI
      prefixes>|<pageref|auto-10>>
    </associate>
    <\associate|toc>
      <vspace*|2fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|font-size|<quote|1.19>|1<space|2spc>Introduction>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|1fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Physical
      Quantities, Units, and Symbols> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>The International System of
      Units <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>Amount of substance and
      amount <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1tab>|1.3<space|2spc>The SI revision of 2019
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|1tab>|1.4<space|2spc>Derived units and prefixes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>Quantity
      Calculus> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|3<space|2spc>Dimensional
      Analysis> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>