<TeXmacs|1.99.19>

<style|<tuple|generic|std-latex>>

<\body>
  <settrimmedsize|11in|8.5in|*> } } \ <settypeblocksize|8.5in|14cm|*>
  <setulmarginsandblock|1.2in|1in|*> <setlrmargins|*|*|1.0>
  <setheaderspaces|*|.4in|*> <raggedbottom><checkandfixthelayout>

  <maxtocdepth|subsection> <nobibintoc><noindexintoc>}
  <assign|*|<macro|<cftappendixname>>>Appendix

  } } } <epigraphposition|center>

  <makepagestyle|thermobkpagestyle>

  <makepsmarks|thermobkpagestyle|<let><@mkboth><markboth><assign|chaptermark|<macro|1|<markboth><ifnum><c@chapter>\<gtr\>0
  <if@mainmatter><@chapapp> <the-chapter>
  <fi><fi>#<arg|1>>><assign|sectionmark|<macro|1|<markright|<ifnum><c@secnumdepth>\<gtr\>0pt
  <the-section> <fi>#<arg|1>>>>>

  <makeoddhead|thermobkpagestyle|<with|font-size|0.84|<change-case|<leftmark>|UPCASE><next-line><with|font-size|0.84|font-shape|small-caps|<rightmark>>>||<with|font-size|1.19|<page-the-page>>>

  <makeheadrule|thermobkpagestyle|tex-text-width|1pt>

  <copypagestyle|nosectioninheader|thermobkpagestyle>
  <makeoddhead|nosectioninheader|<with|font-shape|small-caps|<leftmark>>||<with|font-size|1.19|<page-the-page>>>

  } }\ 

  <\makechapterstyle|mychapterstyle>
    <assign|*|<macro|<chapnamefont>>> <assign|*|<macro|<chapnumfont>>>
    <assign|*|<macro|<printchaptername>>><chapnamefont><with|par-mode|center|<@chapapp>>
    <assign|*|<macro|<printchapternum>>><chapnumfont><the-chapter>
    <assign|*|<macro|<chaptitlefont>>> <assign|*|<macro|<printchaptertitle>>>[1]-1.0<onelineskip>
    <rule|tex-text-width|1.5pt> -0.2<onelineskip>

    <surround|| |<\with|par-mode|center>
      <chaptitlefont>

      <\with|font-size|1.68|font-series|bold>
        #<1>
      </with>
    </with>>

    <assign|*|<macro|<afterchaptertitle>>>-1.0<onelineskip>
    <rule|tex-text-width|1.5pt> afterchapskip
    <assign|*|<macro|<printchapternonum>>><vphantom|<chapnumfont><with|font-shape|small-caps|9>><afterchapternum>
  </makechapterstyle>

  <setsecheadstyle|> <setsubsecheadstyle|> <setsubsubsecheadstyle|>

  <captionnamefont|> <captiondelim|<space|.1in>> <captiontitlefont|>

  } } <feetbelowfloat><setfootnoterule*|<vfill>|3pt|0.4tex-column-width|<normalrulethickness>>

  } } }

  } } }

  <firmlists>
</body>

<initial|<\collection>
</collection>>