<TeXmacs|1.99.19>

<style|book>

<\body>
  <\hide-preamble>
    \;

    \;

    <assign|equation-lab-cov|<\macro|body|lab|cov>
      <\surround|<set-binding|<arg|lab>>|<space|5mm><tabular|<tformat|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|(<arg|lab>)>>|<row|<cell|(<arg|cov>)>>>>>>
        <\equation*>
          <arg|body>
        </equation*>
      </surround>
    </macro>>

    <assign|equation-cov|<\macro|body|cov>
      <\surround|<next-equation>|>
        <equation-lab-cov|<arg|body>|<the-equation>|<arg|cov>>
      </surround>
    </macro>>
  </hide-preamble>

  <\equation>
    E=m*c<rsup|2>
  </equation>

  <\equation>
    1+3=4
  </equation>

  \;

  \;

  \;

  \;

  \;

  <\equation>
    1+1=2
  </equation>

  \;

  1

  <equation-cov|1+1=2|ideal gas>

  \;

  \;

  Testing. <tabular|<tformat|<cwith|1|1|1|1|cell-halign|r>|<table|<row|<cell|(1)>>|<row|<cell|(ideal
  gas)>>>>>

  \;

  \;

  \;
</body>

<\initial>
  <\collection>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|true>
    <associate|src-special|raw>
  </collection>
</initial>