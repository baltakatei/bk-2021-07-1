;; 2021-06-09T20:47Z; bktei> Removes `*` chars from `<tx|>` tags like so:
;;     `<tx|f*o*o>` becomes `<tx|foo>`
;; Ref/Attrib: How to replace characters within brackets with emacs regular expressions, by lawlist https://stackoverflow.com/a/24938787
;; Ref/Attrib: Manual for emacs regular expressions builder ("re-builder.el") https://web.archive.org/web/20150628073628/http://repo.or.cz/w/emacs.git/blob/HEAD:/lisp/emacs-lisp/re-builder.el
;; Ref/Attrib: How to define a custom emacs interactive function https://github.com/hypernumbers/learn_elisp_the_hard_way/blob/master/contents/lesson-4-2-adding-custom-functions-to-emacs.rst
;; Note: Load this filewith `M-x load-file`

(defun texmacs-remove-asterisk-from-tx ()
  "Remove asterisks from within <tx|> tags."
  (interactive)
  (save-excursion
    (goto-char (point-max))
    (while (re-search-backward "\\(\<\\)\\(tx|[^>]*\\)\\(\>\\)" nil t)
      (when (looking-at "\\(\<\\)\\(tx|[^>]*\\)\\(\>\\)")
	(let* (
               (start (match-beginning 0))
               (end (match-end 0)))
          (replace-regexp "*" "" nil start end)))))
  )

(defun texmacs-remove-asterisk-from-Delsub ()
  "Remove asterisks from within <Delsub|> tags."
  (interactive)
  (save-excursion
    (goto-char (point-max))
    (while (re-search-backward "\\(\<\\)\\(Delsub|[^>]*\\)\\(\>\\)" nil t)
      (when (looking-at "\\(\<\\)\\(Delsub|[^>]*\\)\\(\>\\)")
	(let* (
               (start (match-beginning 0))
               (end (match-end 0)))
          (replace-regexp "*" "" nil start end)))))
  )

(defun texmacs-remove-asterisk-from-subs ()
  "Remove asterisks from within <subs|> tags."
  (interactive)
  (save-excursion
    (goto-char (point-max))
    (while (re-search-backward "\\(\<\\)\\(subs|[^>]*\\)\\(\>\\)" nil t)
      (when (looking-at "\\(\<\\)\\(subs|[^>]*\\)\\(\>\\)")
	(let* (
               (start (match-beginning 0))
               (end (match-end 0)))
          (replace-regexp "*" "" nil start end)))))
  )

(defun texmacs-remove-asterisk-from-sups ()
  "Remove asterisks from within <sups|> tags."
  (interactive)
  (save-excursion
    (goto-char (point-max))
    (while (re-search-backward "\\(\<\\)\\(sups|[^>]*\\)\\(\>\\)" nil t)
      (when (looking-at "\\(\<\\)\\(sups|[^>]*\\)\\(\>\\)")
	(let* (
               (start (match-beginning 0))
               (end (match-end 0)))
          (replace-regexp "*" "" nil start end)))))
  )

