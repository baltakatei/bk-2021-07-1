#!/usr/bin/env bash
# Desc: Identifies dashed path objects in .svg file and converts each dash into a subpath
# Desc_verbose: Checks to see if an .svg (SVG) file contains the
#   string "stroke-dasharray" which may be found in the style
#   attribute of a *path* object that is a dashed line. All such
#   objects are identified and selected by their ID attribute (the ID
#   is pulled using a grep text search command that relies upon
#   Inkscape's formatting of an SVG's XML tree, so beware). An
#   `inkscape` command is then crafted that run the 'Modify Path ->
#   Convert to Dashes' extension on each selected object; this
#   extension runs a python script bundled with Inkscape 1.2 called
#   'convert2dashes.py' which converts each dash of a dashed *path*
#   object into a subpath of the object; in XML tree terms, it shifts
#   the dash appearance information from being dynamically-generated
#   according to the 'stroke-dasharray' *style* attribute to the *d*
#   attribute (where the path's canvas location information is stored,
#   greatly engorging it). Visually, the change is not noticeable in
#   inkscape; however, when exported to PDF or EPS, the resulting
#   postscript code (and subsequent ghostscript conversions) will not
#   attempt to dynamically render dashes (and likely do a bad job at
#   it). The original object is preserved under a different name and
#   hidden; the duplicate retains the original name.
# Usage: ink_convert_dash.sh arg1
# Depends: inkscape 1.2, GNU Coreutils 8.30 (grep, cut, cat)
# Version: 0.0.2

declare -Ag appRollCall # Associative array for storing app status
declare -Ag fileRollCall # Associative array for storing file status
declare -Ag dirRollCall # Associative array for storing dir status

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
try() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
checkapp() {
    # Desc: If arg is a command, save result in assoc array 'appRollCall'
    # Usage: checkapp arg1 arg2 arg3 ...
    # Version: 0.1.1
    # Input: global assoc. array 'appRollCall'
    # Output: adds/updates key(value) to global assoc array 'appRollCall'
    # Depends: bash 5.0.3
    local returnState    

    #===Process Args===
    for arg in "$@"; do
	if command -v "$arg" 1>/dev/null 2>&1; then # Check if arg is a valid command
	    appRollCall[$arg]="true";
	    if ! [ "$returnState" = "false" ]; then returnState="true"; fi;
	else
	    appRollCall[$arg]="false"; returnState="false";
	fi;
    done;

    #===Determine function return code===
    if [ "$returnState" = "true" ]; then
	return 0;
    else
	return 1;
    fi;
} # Check that app exists
checkfile() {
    # Desc: If arg is a file path, save result in assoc array 'fileRollCall'
    # Usage: checkfile arg1 arg2 arg3 ...
    # Version: 0.1.1
    # Input: global assoc. array 'fileRollCall'
    # Output: adds/updates key(value) to global assoc array 'fileRollCall';
    # Output: returns 0 if app found, 1 otherwise
    # Depends: bash 5.0.3
    local returnState

    #===Process Args===
    for arg in "$@"; do
	if [ -f "$arg" ]; then
	    fileRollCall["$arg"]="true";
	    if ! [ "$returnState" = "false" ]; then returnState="true"; fi;
	else
	    fileRollCall["$arg"]="false"; returnState="false";
	fi;
    done;
    
    #===Determine function return code===
    if [ "$returnState" = "true" ]; then
	return 0;
    else
	return 1;
    fi;
} # Check that file exists
checkdir() {
    # Desc: If arg is a dir path, save result in assoc array 'dirRollCall'
    # Usage: checkdir arg1 arg2 arg3 ...
    # Version 0.1.2
    # Input: global assoc. array 'dirRollCall'
    # Output: adds/updates key(value) to global assoc array 'dirRollCall';
    # Output: returns 0 if all args are dirs; 1 otherwise
    # Depends: Bash 5.0.3
    local returnState

    #===Process Args===
    for arg in "$@"; do
	if [ -z "$arg" ]; then
	    dirRollCall["(Unspecified Dirname(s))"]="false"; returnState="false";
	elif [ -d "$arg" ]; then
	    dirRollCall["$arg"]="true";
	    if ! [ "$returnState" = "false" ]; then returnState="true"; fi
	else
	    dirRollCall["$arg"]="false"; returnState="false";
	fi
    done
    
    #===Determine function return code===
    if [ "$returnState" = "true" ]; then
	return 0;
    else
	return 1;
    fi
} # Check that dir exists
displayMissing() {
    # Desc: Displays missing apps, files, and dirs
    # Usage: displayMissing
    # Version 1.0.0
    # Input: associative arrays: appRollCall, fileRollCall, dirRollCall
    # Output: stderr: messages indicating missing apps, file, or dirs
    # Output: returns exit code 0 if nothing missing; 1 otherwise
    # Depends: bash 5, checkAppFileDir()
    local missingApps value appMissing missingFiles fileMissing
    local missingDirs dirMissing

    #==BEGIN Display errors==
    #===BEGIN Display Missing Apps===
    missingApps="Missing apps  :";
    #for key in "${!appRollCall[@]}"; do echo "DEBUG:$key => ${appRollCall[$key]}"; done
    for key in "${!appRollCall[@]}"; do
	value="${appRollCall[$key]}";
	if [ "$value" = "false" ]; then
	    #echo "DEBUG:Missing apps: $key => $value";
	    missingApps="$missingApps""$key ";
	    appMissing="true";
	fi;
    done;
    if [ "$appMissing" = "true" ]; then  # Only indicate if an app is missing.
	echo "$missingApps" 1>&2;
    fi;
    unset value;
    #===END Display Missing Apps===

    #===BEGIN Display Missing Files===
    missingFiles="Missing files:";
    #for key in "${!fileRollCall[@]}"; do echo "DEBUG:$key => ${fileRollCall[$key]}"; done
    for key in "${!fileRollCall[@]}"; do
	value="${fileRollCall[$key]}";
	if [ "$value" = "false" ]; then
	    #echo "DEBUG:Missing files: $key => $value";
	    missingFiles="$missingFiles""$key ";
	    fileMissing="true";
	fi;
    done;
    if [ "$fileMissing" = "true" ]; then  # Only indicate if an app is missing.
	echo "$missingFiles" 1>&2;
    fi;
    unset value;
    #===END Display Missing Files===

    #===BEGIN Display Missing Directories===
    missingDirs="Missing dirs:";
    #for key in "${!dirRollCall[@]}"; do echo "DEBUG:$key => ${dirRollCall[$key]}"; done
    for key in "${!dirRollCall[@]}"; do
	value="${dirRollCall[$key]}";
	if [ "$value" = "false" ]; then
	    #echo "DEBUG:Missing dirs: $key => $value";
	    missingDirs="$missingDirs""$key ";
	    dirMissing="true";
	fi;
    done;
    if [ "$dirMissing" = "true" ]; then  # Only indicate if an dir is missing.
	echo "$missingDirs" 1>&2;
    fi;
    unset value;
    #===END Display Missing Directories===

    #==END Display errors==
    #==BEGIN Determine function return code===
    if [ "$appMissing" == "true" ] || [ "$fileMissing" == "true" ] || [ "$dirMissing" == "true" ]; then
	return 1;
    else
	return 0;
    fi
    #==END Determine function return code===
} # Display missing apps, files, dirs
main() {
    yell "DEBUG:start main()";
    # Check depends
    checkapp inkscape grep;
    
    # Process input
    yell "DEBUG:process input";
    arg1="$1";
    if [[ $# -ne 1 ]]; then die "ERROR:Incorrect number of arguments:$#:$*"; fi;
    ## File name ends in .svg
    pattern_file_in=".svg$";
    if [[ ! $arg1 =~ $pattern_file_in ]]; then die "ERROR:File must end in .svg"; fi;
    ### Is a file
    if checkfile "$arg1"; then
	file_in="$arg1";
	yell "DEBUG:file_in:$file_in";
    else
	die "ERROR:Not a file:$arg1";
    fi;
    displayMissing;

    # Output settings
    file_out="${file_in%.svg}_dashed.svg";
    yell "file_out:$file_out";

    # Search for 'stroke-dasharray'
    yell "DEBUG:search for pattern";
    pattern='stroke-dasharray';
    if ! grep "$pattern" "$file_in" 1>/dev/null 2>&1; then die "ERROR:not found:$pattern"; fi;

    # Get list of IDs
    yell "DEBUG:Get list of IDs";
    ## Fragile grep pattern search (TODO:replace with XML tree walk?)
    ids="$(grep "$pattern" -A4 "$file_in" | grep "id=" | cut -d'"' -f2)";
    id_count="$(echo "$ids" | wc -l)"; # get number of IDs
    #yell "DEBUG:ids:$ids";
    yell "DEBUG:id_count:$id_count";

    # Construct command
    options="--with-gui"; # required for running inkscape extensions (that use python scripts?)
    # open --actions
    options="$options --actions 'about";
    ## Perform actions on each ID
    n=1;
    while read -r line; do
	yell "DEBUG:line:$line";
	### Select object
	options="$options; select-by-id:$line";

	### Duplicate object
	options="$options; duplicate"; # duplicate objects and deselect originals
	
	### Hide duplicate (for future reference)
	options="$options; selection-hide"; # hide dashed line objects

	### Select original object
	options="$options; select-by-id:$line";

	### Convert dashes
	options="$options; org.inkscape.filter.dashit.noprefs";

	### Deselect
	options="$options; select-clear";
	((n++));
    done < <(echo "$ids");

    ## Export options
    #options="$options;export-area-drawing";
    options="$options; export-plain-svg";
    options="$options; export-filename:$file_out";
    options="$options; export-overwrite"; # overwrite $file_out
    options="$options; export-do";

    ## Close Inkscape
    options="$options;quit-immediate";
    
    options="$options'"; # close --actions
    yell "DEBUG:$options";
    cmd="inkscape $file_in $options";
    yell "DEBUG:cmd:$cmd";

    # Run Command
    eval inkscape "$file_in" "$options"
    
    yell "DEBUG:end main()";
}; # Main program

try main "$@";

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
