#!/bin/bash
# Desc: displays whenever the string "reference" appears within 30
#   characters of the string "vpageref".
# usage: regex_find_refvpageref_duplicates.sh [dir]
# Note: May be useful for finding instances of '<reference|foo><vpageref|foo>'.

# check args
if [[ $# -ne 1 ]]; then
    echo "ERROR:Wrong number of args:$#:$*" 1>&2;
    exit 1;
fi;

if [[ -d $1 ]]; then
    dir_in="$1";
else
    echo "ERROR:Not a dir:$1" 1>&2;
    exit 1;
fi;

clear;
for file in "$dir_in"/**.tm; do
    # read each .tm file, strip out newlines, and regex search
    cat "$file" | tr -d '\n' | grep -Eo '(.{50})(reference)(.){1,30}(vpageref)(.{50})';
    if [[ -$? -eq 0 ]]; then
	## display path of file if match found based on exit code of last command (i.e. grep)
	echo "$file"; echo "";
    fi;
done;
