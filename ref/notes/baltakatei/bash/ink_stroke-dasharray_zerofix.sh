#!/usr/bin/env bash
# Desc: In svg file, change '0' in `stroke-dasharray:0,28.1;` into
#   small non-zero (e.g. 0.28100,28.1).
# Input: arg1: path to file to process
# Output: file_out: path to output file (same as input file)
# Depends: GNU Coreutils 8.30, bc 1.07.1
# Version: 0.0.1
# Note: Assumes stroke-dasharray takes form of 'stroke-dasharray:0,28.1'.

yell() { echo "$0: $*" >&2; } # print script path and all args to stderr
die() { yell "$*"; exit 111; } # same as yell() but non-zero exit status
try() { "$@" || die "cannot $*"; } # runs args as command, reports args if command fails
main () {
    # Process inputs
    pattern=".svg$"
    if [[ -f $1 ]] && [[ $1 =~ $pattern ]]; then
	file_in="$1";
    else
	die "ERROR:Not a file:$1";
    fi;
    if [[ $# -ne 1 ]]; then die "ERROR:Wrong number of arguments:$#:$*"; fi;
    file_out="${file_in%.svg}"_new.svg;

    unset output_buffer;
    SAVEIFS=$IFS ;
    IFS=$'\n';
    n=1;
    while read -r line; do
	### Check for pattern
	pattern_line="stroke-dasharray:0,";
	if [[ $line =~ $pattern_line ]]; then
	    yell "DEBUG:match found on line:$n";
	    yell "DEBUG:line   :$line";
	    before="$(echo "${line%%stroke-dasharray:*}")";
	    after="$(echo "${line##*stroke-dasharray:}" | cut -d';' -f2-)";
	    between="$(echo "${line##$before}" | cut -d';' -f1)";
	    yell "DEBUG:before :$before";
	    yell "DEBUG:after  :$after";
	    yell "DEBUG:between:$between";

	    #### Count separators
	    sep_count="$(echo "$between" | tr -dc ', ' | wc -c)";
	    yell "DEBUG:sep_count:$sep_count";
	    if [[ $sep_count -ne 1 ]]; then
		yell "WARNING:skipping because invalid number of separators in stroke-dasharray:$sep_count:$between";
		continue;
	    fi;

	    #### Get stroke-dasharray fields
	    fields="$(echo "$between" | cut -d':' -f2)";
	    yell "DEBUG:fields:$fields";
	    if [[ $between =~ , ]]; then
		field1="$(echo "$fields" | cut -d',' -f1)";
		field2="$(echo "$fields" | cut -d',' -f2)";
	    elif [[ $between =~ ' ' ]]; then
		field1="$(echo "$fields" | cut -d' ' -f1)";
		field2="$(echo "$fields" | cut -d' ' -f2)";
	    fi;
	    yell "DEBUG:field1:$field1";
	    yell "DEBUG:field2:$field2";

	    #### Calculate new field1
	    #### Note: field1 calculated in proportion to field2
	    field1="$(echo "scale=5; $field2 / 100" | bc -l)";
	    if [[ $field1 =~ ^'.' ]]; then field1="0""$field1"; fi; # restore leading zero if field1 starts with `.`.
	    
	    #### Reconstruct line with new field values
	    between="stroke-dasharray:$field1,$field2;";
	    yell "DEBUG:between(new):$between";
	    line="$before$between$after";
	    yell "DEBUG:line(new):$line";
	fi;

	#### Construct output_buffer
	if [[ $n -eq 1 ]]; then
	    output_buffer="$(echo -e "$line")";
	else
	    output_buffer="$(echo -e "$output_buffer"\\n"$line")";
	fi;	
	
	((n++));
    done < <(cat "$file_in");
    IFS=$SAVEIFS;
    echo "$output_buffer" > "$file_out";
    
} # main program

main "$@";

# Author: Steven Baltakatei Sandoval
# License: GPLv3+
