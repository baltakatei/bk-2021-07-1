<TeXmacs|1.99.19>

<style|generic>

<\body>
  <section|How to Build Things>

  <subsection|Automobiles>

  <subsubsection|How to build a truck>

  This is how to build a truck.<index|truck><subindex|Automobiles|truck>

  <new-page>

  \;

  <subsubsection|How to build a passenger car>

  This is how to build a car.<index|car>

  <new-page>

  \;

  <subsubsection|How to build an SUV>

  <subsubsection|How to build a passenger plane>

  This is how to build a plane.<index|plane>

  \;

  <new-page>

  \;

  \;

  \;

  \;

  <\the-index|idx>
    <index+1*|Automobiles>

    <index+2|Automobiles|truck|<pageref|auto-5>>

    <index+1|car|<pageref|auto-7>>

    <index+1|plane|<pageref|auto-10>>

    <index+1|truck|<pageref|auto-4>>
  </the-index>
</body>

<\initial>
  <\collection>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|letter>
    <associate|page-width|auto>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-10|<tuple|plane|3|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-11|<tuple|plane|4|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-2|<tuple|1.1|1|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-3|<tuple|1.1.1|1|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-4|<tuple|truck|1|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-5|<tuple|Automobiles|1|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-6|<tuple|1.1.2|2|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-7|<tuple|car|2|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-8|<tuple|1.1.3|3|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
    <associate|auto-9|<tuple|1.1.4|3|../../../../../.TeXmacs/texts/scratch/no_name_5.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|idx>
      <tuple|<tuple|truck>|<pageref|auto-4>>

      <tuple|<tuple|Automobiles|truck>|<pageref|auto-5>>

      <tuple|<tuple|car>|<pageref|auto-7>>

      <tuple|<tuple|plane>|<pageref|auto-10>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>How
      to Build Things> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Automobiles
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|2tab>|1.1.1<space|2spc>How to build a truck
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|2tab>|1.1.2<space|2spc>How to build a passenger
      car <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|2tab>|1.1.3<space|2spc>How to build an SUV
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|2tab>|1.1.4<space|2spc>How to build a passenger
      plane <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Index>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>