<TeXmacs|2.1>

<style|<tuple|book|termes-font>>

<\body>
  <section|Unicode Glyph Tests>

  This document tests Unicode glyphs used throughout the textbook.

  <subsection|Vertical Bars for Galvanic Cell Diagrams>

  <subsubsection|Plain Unicode>

  <\eqnarray*>
    <tformat|<table|<row|<cell|<text|VERTICAL
    LINE>>|<cell|<text|U+007C>>|<cell|\<#007c\>>>|<row|<cell|<text|BROKEN
    BAR>>|<cell|<text|U+00A6>>|<cell|\<#00a6\>>>|<row|<cell|<text|DOUBLE
    VERTICAL LINE>>|<cell|<text|U+2016>>|<cell|\<#2016\>>>|<row|<cell|<text|VERTICAL
    FOUR DOTS>>|<cell|<text|U+205E>>|<cell|\<#205e\>>>|<row|<cell|<text|VERTICAL
    ELLIPSIS>>|<cell|<text|U+22EE>>|<cell|\<#22ee\>>>|<row|<cell|<text|BOX
    DRAWINGS LIGHT VERTICAL>>|<cell|<text|U+2502>>|<cell|\<#2502\>>>|<row|<cell|<text|BOX
    DRAWINGS HEAVY VERTICAL>>|<cell|<text|U+2503>>|<cell|\<#2503\>>>|<row|<cell|<text|BOX
    DRAWINGS LIGHT TRIPLE DASH VERTICAL>>|<cell|<text|U+2506>>|<cell|\<#2506\>>>|<row|<cell|<text|BOX
    DRAWINGS HEAVY TRIPLE DASH VERTICAL>>|<cell|<text|U+2507>>|<cell|\<#2507\>>>|<row|<cell|<text|BOX
    DRAWINGS LIGHT QUADRUPLE DASH VERTICAL>>|<cell|<text|U+250A>>|<cell|\<#250a\>>>|<row|<cell|<text|BOX
    DRAWINGS HEAVY QUADRUPLE DASH VERTICAL>>|<cell|<text|U+250B>>|<cell|\<#250b\>>>|<row|<cell|<text|MUSICAL
    SYMBOL SINGLE BARLINE>>|<cell|<text|U+1D100>>|<cell|\<#1d100\>>>|<row|<cell|<text|MUSICAL
    SYMBOL DOUBLE BARLINE>>|<cell|<text|U+1D101>>|<cell|\<#1d101\>>>|<row|<cell|<text|MUSICAL
    SYMBOL DASHED BARLINE>>|<cell|<text|U+1D104>>|<cell|\<#1D104\>>>>>
  </eqnarray*>

  \;

  <section|References>

  See Baltakatei article \P<hlink|Hunt for the Dashed Vertical
  Bar|https://reboil.com/texmacs/articles/0020210616T1952Z..hunt_for_dashed_vertical_bar_glyph.xhtml>\Q.
</body>

<\initial>
  <\collection>
    <associate|math-font|math-termes>
    <associate|page-medium|paper>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|1.1|1>>
    <associate|auto-3|<tuple|1.1.1|1>>
    <associate|auto-4|<tuple|2|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      1<space|2spc>Unicode Glyph Tests <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Vertical Bars for Galvanic
      Cell Diagrams <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|2tab>|1.1.1<space|2spc>Plain Unicode
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>