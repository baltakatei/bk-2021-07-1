<TeXmacs|1.99.20>

<style|book>

<\body>
  <\big-table|<tabular|<tformat|<cwith|2|-1|1|1|cell-halign|L.>|<table|<row|<cell|a
  very long column header>>|<row|<cell|03.14>>|<row|<cell|3.1415>>>>>>
    \;
  </big-table>

  <\big-table|<tabular|<tformat|<cwith|2|-1|1|1|cell-halign|C.>|<table|<row|<cell|a
  very long column header>>|<row|<cell|03.14>>|<row|<cell|3.1415>>>>>>
    \;
  </big-table>

  \;

  <\big-table|<tabular|<tformat|<cwith|2|-1|1|1|cell-halign|C.>|<table|<row|<cell|a
  very long column header>>|<row|<cell|3.00000>>|<row|<cell|03.1>>|<row|<cell|00003.14>>|<row|<cell|03.141>>|<row|<cell|3.1415>>>>>>
    \;
  </big-table>

  \;
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../../../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-2|<tuple|2|1|../../../../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|auto-3|<tuple|3|1|../../../../../.TeXmacs/texts/scratch/no_name_8.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|table>
      <tuple|normal|<\surround|<hidden-binding|<tuple>|1>|>
        \;
      </surround>|<pageref|auto-1>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|2>|>
        \;
      </surround>|<pageref|auto-2>>

      <tuple|normal|<\surround|<hidden-binding|<tuple>|3>|>
        \;
      </surround>|<pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>