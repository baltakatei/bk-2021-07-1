<TeXmacs|2.1>

<style|generic>

<\body>
  <\hide-preamble>
    \;

    <assign|B|<macro|<rsub|<text|B>>>>
  </hide-preamble>

  <math|m<B><rprime|'>>

  Text.<\footnote>
    Footnote text.
  </footnote>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|footnote-1|<tuple|1|1>>
    <associate|footnr-1|<tuple|1|1>>
  </collection>
</references>