<TeXmacs|1.99.20>

<project|book.tm>

<style|book>

<\body>
  <chapter|Test Chapter>

  <section|Test section><label|c1 section>

  Blah.

  <subsection|Test Subsection><label|c1 section-subsection>

  Blah.

  <subsection*|Unnumbered Subsection><label|c1 section-subsectionUn>

  Blah.

  <subsection*|Second Unnumbered Subsection><label|c1 section-subsectionUn2>

  Blah.

  \;
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|0>
    <associate|page-first|1>
    <associate|page-medium|paper>
    <associate|section-nr|0>
    <associate|subsection-nr|0>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-2|<tuple|1.1|1>>
    <associate|auto-3|<tuple|1.1.1|1>>
    <associate|auto-4|<tuple|1.1.1|1>>
    <associate|auto-5|<tuple|1.1.1|?>>
    <associate|c1 section|<tuple|1.1|1>>
    <associate|c1 section-subsection|<tuple|1.1.1|1>>
    <associate|c1 section-subsection-Un2|<tuple|1.1.1|?>>
    <associate|c1 section-subsectionUn|<tuple|1.1.1|1>>
    <associate|c1 section-subsectionUn2|<tuple|1.1.1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Test
      Chapter> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Test section <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      <with|par-left|<quote|1tab>|1.1.1<space|2spc>Test Subsection
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1tab>|Test Subsection (unnumbered)
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>
    </associate>
  </collection>
</auxiliary>