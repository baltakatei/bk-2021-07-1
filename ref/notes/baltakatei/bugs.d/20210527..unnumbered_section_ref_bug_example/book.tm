<TeXmacs|1.99.20>

<style|book>

<\body>
  <include|c1.tm>

  <include|c2.tm>
</body>

<\initial>
  <\collection>
    <associate|page-medium|paper>
    <associate|project-flag|true>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|c1.tm>>
    <associate|auto-2|<tuple|1.1|1|c1.tm>>
    <associate|auto-3|<tuple|1.1.1|1|c1.tm>>
    <associate|auto-4|<tuple|2|?|c2.tm>>
    <associate|c1 section|<tuple|1.1|1|c1.tm>>
    <associate|c1 section-subsection|<tuple|1.1.1|1|c1.tm>>
    <associate|part:c1.tm|<tuple|?|1|../../../../../../../.TeXmacs/texts/scratch/no_name_8.tm>>
    <associate|part:c2.tm|<tuple|1.1.1|?|../../../../../../../.TeXmacs/texts/scratch/no_name_8.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|parts>
      <tuple|c1.tm|chapter-nr|0|section-nr|0|subsection-nr|0>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Test
      Chapter> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      1.1<space|2spc>Test section <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>

      <with|par-left|<quote|1tab>|1.1.1<space|2spc>Test Subsection
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>
    </associate>
  </collection>
</auxiliary>