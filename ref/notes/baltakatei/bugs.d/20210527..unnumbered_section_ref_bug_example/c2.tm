<TeXmacs|1.99.20>

<project|book.tm>

<style|book>

<\body>
  <chapter|>

  There is a section chapter 1 (see <reference|c1 section>).

  There is a subsection in Chapter 1 that is numbered (see <reference|c1
  section-subsection>).

  There is a subsection in Chapter 1 that is unnumbered (see <reference|c1
  section-subsectionUn>).

  There is a second subsection in Chapter 1 that is also unnumbered (see
  <reference|c1 section-subsectionUn2>).
</body>

<\initial>
  <\collection>
    <associate|chapter-nr|1>
    <associate|page-first|?>
    <associate|page-medium|paper>
    <associate|section-nr|1>
    <associate|subsection-nr|1>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|2|1>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|2<space|2spc>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>