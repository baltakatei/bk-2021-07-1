<TeXmacs|1.99.19>

<style|generic>

<\body>
  <section|Glossary test>

  <subsection|Transport vehicles><glossary-line|<strong|Transport>>

  <subsubsection|Planes>

  A plane<glossary|plane> permits transport by air.

  <subsubsection|Trains>

  A train permits transport by land-based rail.

  <subsubsection|Automobiles>

  An <glossary|automobile>automobile permits transport by land-based
  pavement.

  <subsection|Storage vessels><glossary-line|<vspace*|0.5fn><strong|Storage>>

  <subsubsection|Lake>

  A lake<glossary|lake> can store water at atmospheric pressure.

  <subsubsection|Tanks>

  A tank<glossary|tank> can store pressurized fluid.

  <subsubsection|Niling D-sink>

  <hrule>

  <\the-glossary|gly>
    <with|font-series|bold|math-font-series|bold|Transport>

    <glossary-1|plane|<pageref|auto-4>>

    <glossary-1|automobile|<pageref|auto-7>>

    <vspace*|0.5fn><with|font-series|bold|math-font-series|bold|Storage>

    <glossary-1|lake|<pageref|auto-10>>

    <glossary-1|tank|<pageref|auto-12>>
  </the-glossary>
</body>

<\initial>
  <\collection>
    <associate|page-height|auto>
    <associate|page-medium|paper>
    <associate|page-type|letter>
    <associate|page-width|auto>
    <associate|preamble|false>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-10|<tuple|lake|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-11|<tuple|1.2.2|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-12|<tuple|tank|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-13|<tuple|1.2.3|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-14|<tuple|1.2.3|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-2|<tuple|1.1|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-3|<tuple|1.1.1|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-4|<tuple|plane|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-5|<tuple|1.1.2|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-6|<tuple|1.1.3|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-7|<tuple|automobile|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-8|<tuple|1.2|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
    <associate|auto-9|<tuple|1.2.1|1|../../../../../.TeXmacs/texts/scratch/no_name_7.tm>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|gly>
      <tuple|<with|font-series|<quote|bold>|math-font-series|<quote|bold>|Transport>>

      <tuple|normal|plane|<pageref|auto-4>>

      <tuple|normal|automobile|<pageref|auto-7>>

      <tuple|<vspace*|0.5fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Storage>>

      <tuple|normal|lake|<pageref|auto-10>>

      <tuple|normal|tank|<pageref|auto-12>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|1<space|2spc>Glossary
      test> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <with|par-left|<quote|1tab>|1.1<space|2spc>Transport vehicles
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2>>

      <with|par-left|<quote|2tab>|1.1.1<space|2spc>Planes
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|2tab>|1.1.2<space|2spc>Trains
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|2tab>|1.1.3<space|2spc>Automobiles
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|1tab>|1.2<space|2spc>Storage vessels
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|2tab>|1.2.1<space|2spc>Lake
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|2tab>|1.2.2<space|2spc>Tanks
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|2tab>|1.2.3<space|2spc>Niling D-sink
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Glossary>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>