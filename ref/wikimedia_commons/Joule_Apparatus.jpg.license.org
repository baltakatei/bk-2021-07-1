#+TITLE: File license
#+AUTHOR:Steven Baltakatei Sandoval
#+EMAIL: baltakatei@gmail.com
#+DATE: 2021-04-24
#+LANGUAGE: en
#+OPTIONS: html-postamble:nil toc:nil

"[[https://commons.wikimedia.org/wiki/File:Joule_Apparatus.jpg][Joule Apparatus.jpg]]" by [[https://commons.wikimedia.org/wiki/User:DrJunge][Dr. Mirko Junge]] is licensed under [[https://creativecommons.org/licenses/by-sa/3.0/][CC BY-SA 3.0]].

A local copy of the legal text of CC BY-SA 3.0 may be found [[file:../cc-by-sa-3.0..legalcode.txt][here]].
